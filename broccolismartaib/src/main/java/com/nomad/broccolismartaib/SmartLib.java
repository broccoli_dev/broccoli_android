package com.nomad.broccolismartaib;

import android.content.Context;

import com.kwic.saib.pub.SmartAIB;
import com.nomad.broccolismartaib.data.BankRequestData;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by YelloHyunminJang on 16. 1. 11..
 */
public class SmartLib {

    public static void sendJsonData(Context context, BankRequestData data, SmartAIB.OnCompleteListener listener) throws IllegalStateException, JSONException {
        String raw = null;
        if (data != null) {
            raw = data.getJSONObject().toString();
        }
        if (raw != null) {
            new SmartAIB(context, listener).AIB_Execute(raw);
        }
    }

    private JSONObject sendJsonDataByLoginID(Context context, String _module, String _id, String _pw, String _code,
                                             String startDate, String endDate, SmartAIB.OnCompleteListener _CompleteListener){
        final JSONObject obj = new JSONObject();
        try {

            obj.put("MODULE",_module);
            obj.put("FCODE", AiBUtils.getFSByCode(_code.trim()));  			// 기관코드
            obj.put("LOGINMETHOD","1");
            obj.put("LOGINID",_id.trim());  			// 로그인아이a
            obj.put("LOGINPWD",_pw.trim());  		         // 로그인비번

            obj.put("STARTDATE", startDate);  			// 조회일자
            obj.put("ENDDATE", endDate);       			// 조회일자
            if (_code.equals("504") || _code.equals("505") || _code.equals("506")) obj.put("APPHISTORYVIEW","1");       			// 가맹정보
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new SmartAIB(context, _CompleteListener).AIB_Execute(obj.toString());

        return obj;
    }

}
