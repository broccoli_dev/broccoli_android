package com.nomad.broccolismartaib.data;

/**
 * Created by YelloHyunminJang on 16. 2. 11..
 */
public class CommonCode {

    public static final String KEY_RESULT = "RESULT";
    public static final String KEY_ERRMSG = "ERRMSG";
    public static final String KEY_ERRDOC = "ERRDOC";

    public static final String KEY_BANK1 = "BANK_1";
    public static final String KEY_BANK2 = "BANK_2";
    public static final String KEY_BANK3 = "BANK_3";
    public static final String KEY_ACCNAME = "ACCTNM";
    public static final String KEY_LIST = "LIST";

    public static final String KEY_CARD1 = "CARD_1";
    public static final String KEY_CARD2 = "CARD_2";
    public static final String KEY_CARD3 = "CARD_3";
    public static final String KEY_CARD4 = "CARD_4";

    public static final String VALUE_FAIL = "FAIL";
    public static final String VALUE_SUCCESS = "SUCCESS";
    public static final String ERROR_RETRY   = "retry";     //이전 라이브러리가 스크래핑이 타임아웃으로 실패 시에 동일한 Callback으로 retry 에러메시지를 여러번 전달해 주고 있어서 callback에서 다음 순번 호출 시 문제가 되어 예외처리하기 위한 것
    public static final String ERROR_TIMEOUT = "timeout";
    public static final String ERROR_NETWORK = "[SYS002]네트워크 연결 실패";
    public static final String ERROR_NOT_REG = "인증서";
    public static final String ERROR_RENEWAL = "CHA001";
}
