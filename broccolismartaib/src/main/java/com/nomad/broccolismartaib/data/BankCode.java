package com.nomad.broccolismartaib.data;

/**
 * Created by YelloHyunminJang on 16. 1. 11..
 */
public class BankCode {

    /**
     * 경남
     */
    public static final String KYUNGNAM     = "039";

    /**
     * 광주
     */
    public static final String KWANGJU      = "034";

    /**
     * 국민
     */
    public static final String KB           = "004";

    /**
     * 기업은행
     */
    public static final String IBK          = "003";

    /**
     * 농협
     */
    public static final String NH           = "011";

    /**
     * 대구
     */
    public static final String DAEGU        = "031";

    /**
     * 부산
     */
    public static final String BUSAN        = "032";

    /**
     * 산업은행
     */
    public static final String KDB          = "002";

    /**
     * 새마을
     */
    public static final String SAEMAUL      = "045";

    /**
     * 수협
     */
    public static final String SUHYUP       = "007";

    /**
     * 신한
     */
    public static final String SHINHAN      = "088";

    /**
     * 신협
     */
    public static final String SINHYUP      = "048";

    /**
     * 시티
     */
    public static final String CITY         = "027";
    /**
     * 외환
     */
    public static final String KEB          = "005";

    /**
     * 우리
     */
    public static final String WOORI        = "020";

    /**
     * 전북
     */
    public static final String JEONBUK      = "037";

    /**
     * SC
     */
    public static final String SC           = "023";

    /**
     * 제주
     */
    public static final String JEJU         = "035";

    /**
     * 하나
     */
    public static final String HANA         = "081";

    /**
     * 우체국 - 서비스대상아님
     */
    public static final String POST         = "071";

}
