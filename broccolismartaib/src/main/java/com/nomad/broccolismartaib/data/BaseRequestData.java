package com.nomad.broccolismartaib.data;

/**
 * Created by YelloHyunminJang on 16. 1. 8..
 */
public class BaseRequestData {

    /**
     * 서비스 분류 코드
     */
    public static final String ORG = "ORG";

    /**
     * 인증키 (라이센스키)
     */
    public static final String CERTKEY = "CERTKEY";

    /**
     * 서비스구분코드
     */
    public static final String MODULE = "MODULE";

    /**
     * 기관코드
     */
    public static final String FCODE = "FCODE";

    /**
     * 로그인방법
     */
    public static final String LOGIN = "LOGINMETHOD";

    /**
     * 고객구분
     */
    public static final String CUS_KIND = "CUS_KIND";

    /**
     * 인증서명
     */
    public static final String CERTNAME = "CERTNAME";

    /**
     * 인증서비밀번호
     */
    public static final String CERTPWD = "CERTPWD";

    /**
     * 로그인아이디
     */
    public static final String LOGINID = "LOGINID";

    /**
     * 로그인비밀번호
     */
    public static final String LOGINPWD = "LOGINPWD";

    /**
     * 계좌번호
     */
    public static final String NUMBER = "NUMBER";

    /**
     * 계좌비밀번호
     */
    public static final String PASSWORD = "PASSWORD";

    /**
     * 통화코드
     */
    public static final String CURRCD = "CURRCD";

    /**
     * 조회시작일자
     */
    public static final String STARTDATE = "STARTDATE";

    /**
     * 조회종료일자
     */
    public static final String ENDDATE = "ENDDATE";

    /**
     * 주민/사업자번호
     */
    public static final String REGNUMBER = "REGNUMBER";

    /**
     * 카드 CVC
     */
    public static final String CVC = "CVC";

    /**
     * 카드유효기간
     */
    public static final String VAILDDATE = "YYYYMM";

    /**
     * 가맹점 정보 - 카드?
     */
    public static final String APPHISTORYVIEW = "AppHistoryView";

    /**
     *
     */
    public static final String BILLDATE = "BILLDATE";

    /**
     * 인증서
     */
    public static final String VALUE_METHOD_AUTH = "0";

    /**
     * ID/PW
     */
    public static final String VALUE_METHOD_ID = "1";


    public static final String VALUE_MODULE_BANK_ALL = "1";
    public static final String VALUE_MODULE_BANK_LEFT = "2";
    public static final String VALUE_MODULE_BANK_LIST = "3";

    public static final String VALUE_MODULE_CARD_DUE = "1";
    public static final String VALUE_MODULE_CARD_APPROVAL = "2";
    public static final String VALUE_MODULE_CARD_BILL = "3";
    public static final String VALUE_MODULE_CARD_LIMIT = "4";

    public static final String VALUE_MODULE_CASH = "2";
}
