package com.nomad.broccolismartaib.data;

/**
 * Created by YelloHyunminJang on 16. 1. 12..
 */
public class CardCode {

    public static final String KB       = "502";
    public static final String NH       = "514";
    public static final String LOTTE    = "508";
    public static final String SAMSUNG  = "508";
    public static final String SHINHAN  = "506";
    public static final String CITY     = "501";
    public static final String WOORI    = "511";
    public static final String HANA     = "509";
    public static final String HYUNDAE  = "505";
    public static final String BC       = "503";

}
