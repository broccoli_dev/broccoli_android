package com.nomadconnection.broccoli.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.io.IOException;

public class ImageUtil {
	/**
	 * 비트맵의 모서리를 라운드 처리 한 후 Bitmap을 리턴
	 *
	 * @param bitmap
	 *       bitmap handle
	 * @return Bitmap
	 */
	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
		int image_width = 288;
		int image_height= 288;
		Bitmap output = Bitmap.createBitmap(image_width, image_height, Bitmap.Config.ARGB_8888);
		//Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, output.getWidth(), output.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = 200;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		bitmap.recycle();
		bitmap = output;

		return bitmap;
	}

	/**
	 * 지정한 패스의 파일을 화면 크기에 맞게 읽어서 Bitmap을 리턴
	 *
	 * @param context
	 *       application context
	 * @param imgFilePath
	 *       bitmap file path
	 * @return Bitmap
	 * @throws IOException
	 */
	public static Bitmap loadBackgroundBitmap(Context context, String imgFilePath) {
		File file = new File(imgFilePath);
		if (file.exists() == false) {
			return null;
		}

		// 폰의 화면 사이즈를 구한다.
		Display display = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int displayWidth = size.x;
		int displayHeight = size.y;

		// 읽어들일 이미지의 사이즈를 구한다.
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(imgFilePath, options);

		// 화면 사이즈에 가장 근접하는 이미지의 스케일 팩터를 구한다.
		// 스케일 팩터는 이미지 손실을 최소화하기 위해 짝수로 한다.
		float widthScale = options.outWidth / displayWidth;
		float heightScale = options.outHeight / 609;
		float scale = widthScale > heightScale ? widthScale : heightScale;

		if (scale >= 8)
			options.inSampleSize = 8;
		else if (scale >= 6)
			options.inSampleSize = 6;
		else if (scale >= 4)
			options.inSampleSize = 4;
		else if (scale >= 2)
			options.inSampleSize = 2;
		else
			options.inSampleSize = 1;
		options.inJustDecodeBounds = false;

		return BitmapFactory.decodeFile(imgFilePath, options);
	}

	/**
	 * 지정한 패스의 파일의 EXIF 정보를 읽어서 회전시킬 각도 구하기
	 *
	 * @param filepath
	 *       bitmap file path
	 * @return degree
	 */
	public synchronized static int GetExifOrientation(String filepath) {
		int degree = 0;
		ExifInterface exif = null;

		try {
			exif = new ExifInterface(filepath);
		}
		catch (IOException e) {
			Log.e("TAG", "cannot read exif");
			e.printStackTrace();
		}

		if (exif != null) {
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

			if (orientation != -1) {
				// We only recognize a subset of orientation tag values.
				switch(orientation) {
					case ExifInterface.ORIENTATION_ROTATE_90:
						degree = 90;
						break;

					case ExifInterface.ORIENTATION_ROTATE_180:
						degree = 180;
						break;

					case ExifInterface.ORIENTATION_ROTATE_270:
						degree = 270;
						break;
				}
			}
		}

		return degree;
	}

	/**
	 * 지정한 패스의 파일을 EXIF 정보에 맞춰 회전시키기
	 *
	 * @param bitmap
	 *       bitmap handle
	 * @return Bitmap
	 */
	public synchronized static Bitmap GetRotatedBitmap(Bitmap bitmap, int degrees) {
		if (degrees != 0 && bitmap != null) {
			Matrix m = new Matrix();
			m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2 );
			try {
				Bitmap b2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
				if (bitmap != b2) {
					bitmap.recycle();
					bitmap = b2;
				}
			}
			catch (OutOfMemoryError ex) {
				Log.e("TAG", ex.toString());
				// We have no memory to rotate. Return the original bitmap.
			}
		}

		return bitmap;
	}

	/**
	  *
	  * 이미지를 주어진 너비와 높이에 맞게 리사이즈 하는 코드.
	  * 원본 이미지를 크롭 하는게 아니라 리사이즈 하는 것이어서,
	  * 주어진 너비:높이 의 비율이 원본 bitmap 의 비율과 다르다면 변환 후의 너비:높이의 비율도 주어진 비율과는 다를 수 있다.
	  *
	  * 가로가 넓거나 세로가 긴 이미지를 정사각형이나 원형의 view 에 맞추려 할 때,
	  * 이 메쏘드를 호출한 후 반환된 bitmap 을 crop 하면 찌그러지지 않는 이미지를 얻을 수 있다.
	  *
	  * @param Bitmap bitmap 원본 비트맵
	  * @param int width 뷰의 가로 길이
	  * @param int height 뷰의 세로 길이
	  *
	  * @return Bitmap 리사이즈 된 bitmap
	  */
	public static Bitmap resizeBitmap(Bitmap bitmap, int width, int height) {
		if (bitmap.getWidth() != width || bitmap.getHeight() != height){
			float ratio = 1.0f;
			if (width > height) {
				ratio = (float)width / (float)bitmap.getWidth();
			} else {
				ratio = (float)height / (float)bitmap.getHeight();
			}
			bitmap = Bitmap.createScaledBitmap(bitmap,
					(int)(((float)bitmap.getWidth()) * ratio), // Width
					(int)(((float)bitmap.getHeight()) * ratio), // Height
					false);
		}
		return bitmap;
	}

	/**
	 * Bitmap 이미지를 가운데를 기준으로 w, h 크기 만큼 crop한다.
	 *
	 * @param src 원본
	 * @param w 넓이
	 * @param h 높이
	 * @return
	 */
	public static Bitmap cropCenterBitmap(Bitmap src, int w, int h) {
		if(src == null)
			return null;

		int width = src.getWidth();
		int height = src.getHeight();

		if(width < w && height < h)
			return src;

		int x = 0;
		int y = 0;

		if(width > w)
			x = (width - w)/2;

		if(height > h)
			y = (height - h)/2;

		int cw = w; // crop width
		int ch = h; // crop height

		if(w > width)
			cw = width;

		if(h > height)
			ch = height;

		return Bitmap.createBitmap(src, x, y, cw, ch);
	}

	public static boolean fileCheck(String filePath){
		boolean result = false;
		File files = new File(filePath);
		if(files.exists()==true) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}
}
