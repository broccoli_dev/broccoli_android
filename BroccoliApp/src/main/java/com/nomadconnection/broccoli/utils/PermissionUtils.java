package com.nomadconnection.broccoli.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by YelloHyunminJang on 2016. 11. 7..
 */

public class PermissionUtils {

    public static class PermissionState {

        public static final int STATE_ENABLE = 0;
        public static final int STATE_DISABLE = 1;
        public static final int STATE_DISABLE_NOT_SHOW = 2;

        private String permissionName;
        private int permissionState = STATE_DISABLE;

        public int getPermissionState() {
            return permissionState;
        }

        public void setPermissionState(int permissionState) {
            this.permissionState = permissionState;
        }

        public String getPermissionName() {
            return permissionName;
        }

        public void setPermissionName(String permissionName) {
            this.permissionName = permissionName;
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_BROCCOLI = 5000;

    public interface OnDeterminePermissionListener {
        void onEnabled();
        void onShowRequestDialog(HashMap<String, PermissionState> map);
        void onShowPermissionSettingDialog(HashMap<String, PermissionState> map);
    }

    public static boolean checkPermission(Activity activity, @NonNull HashMap<String, PermissionState> permission) {
        boolean permissionDenied = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Set<String> keySet = permission.keySet();
            Iterator<String> iterator = keySet.iterator();
            int result = PackageManager.PERMISSION_DENIED;
            while (iterator.hasNext()) {
                String key = iterator.next();
                result = ContextCompat.checkSelfPermission(activity, key);
                PermissionState state = new PermissionState();
                state.setPermissionName(key);
                if (result == PackageManager.PERMISSION_DENIED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, key)) {
                        state.setPermissionState(PermissionState.STATE_DISABLE);
                        permission.put(key, state);
                    } else {
                        state.setPermissionState(PermissionState.STATE_DISABLE_NOT_SHOW);
                        permission.put(key, state);
                    }
                    permissionDenied = true;
                } else {
                    state.setPermissionState(PermissionState.STATE_ENABLE);
                    permission.put(key, state);
                }
            }
        }

        return permissionDenied;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void showPermissionDialog(Activity activity, HashMap<String, PermissionState> permission, int requestCode) {
        Set<String> keySet = permission.keySet();
        Iterator<String> iterator = keySet.iterator();
        String[] kesArray = new String[keySet.size()];
        int i = 0;
        while (iterator.hasNext()) {
            kesArray[i] = iterator.next();
            i++;
        }
        activity.requestPermissions(kesArray, requestCode);
    }

    public static void determineWhichPermissionDialog(HashMap<String, PermissionState> permission, OnDeterminePermissionListener listener) {
        Set<String> keySet = permission.keySet();
        Iterator<String> iterator = keySet.iterator();
        int notShowCount = 0;
        int disableCount = 0;
        int enableCount = 0;
        while (iterator.hasNext()) {
            String key = iterator.next();
            PermissionState state = permission.get(key);
            if (state != null) {
                switch (state.getPermissionState()) {
                    case PermissionState.STATE_ENABLE:
                        enableCount++;
                        break;
                    case PermissionState.STATE_DISABLE:
                        disableCount++;
                        break;
                    case PermissionState.STATE_DISABLE_NOT_SHOW:
                        notShowCount++;
                        break;
                }
            }
        }
        if (notShowCount > 0) {
            if (listener != null)
                listener.onShowPermissionSettingDialog(permission);
        } else if (disableCount > 0) {
            if (listener != null)
                listener.onShowRequestDialog(permission);
        } else if (enableCount > 0) {
            if (listener != null)
                listener.onEnabled();
        }
    }

    public static void showSettingPermission(Activity activity, int requestCode) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:"+activity.getPackageName()));
        activity.startActivityForResult(intent, requestCode);
    }
}
