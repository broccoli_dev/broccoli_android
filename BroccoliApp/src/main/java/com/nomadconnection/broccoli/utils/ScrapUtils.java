package com.nomadconnection.broccoli.utils;

import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoliespider.data.BankRequestData;

/**
 * Created by YelloHyunminJang on 16. 4. 8..
 */
public class ScrapUtils {

    public static BankRequestData convertBankRequestData(BankData bank, String start, String end, boolean isDaily) {
        BankRequestData.Builder builder = new BankRequestData.Builder()
                .setModule("")
                .setStartDate(start)
                .setEndDate(end);
        if (BankData.LOGIN_ID.equalsIgnoreCase(bank.getLoginMethod())) {
            builder.setIDAndPW(bank.getCertSerial(), bank.getPassWord());
        } else {
            builder.setAuth(bank.getCertSerial(), bank.getPassWord());
        }
        BankRequestData data = builder.build();
        data.setOriginalBankCode(bank.getBankCode());
        data.setOriginalBankType(bank.getType());
        data.setOriginalBankName(bank.getBankName());
        data.setIsDaily(isDaily);
        return data;
    }

    public static BankRequestData convertCardRequestData(String module, BankData bank, String start, String end, String billdate, boolean isDaily) {
        BankRequestData.Builder builder = new BankRequestData.Builder()
                .setModule(module)
                        //.setAppHistoryView("1")
                .setStartDate(start)
                .setEndDate(end);
        if (BankData.LOGIN_ID.equalsIgnoreCase(bank.getLoginMethod())) {
            builder.setIDAndPW(bank.getCertSerial(), bank.getPassWord());
        } else {
            builder.setAuth(bank.getCertSerial(), bank.getPassWord());
        }
        if (billdate != null) {
            builder.setBillDate(billdate);
        }
        BankRequestData data = builder.build();
        data.setOriginalBankCode(bank.getBankCode());
        data.setOriginalBankType(bank.getType());
        data.setOriginalBankName(bank.getBankName());
        data.setIsDaily(isDaily);
        return data;
    }

    public static BankRequestData convertCashRequestData(String module, BankData bank, String start, String end, String billdate, boolean isDaily) {
        BankRequestData.Builder builder = new BankRequestData.Builder()
                .setModule(module)
                .setStartDate(start)
                .setEndDate(end);
        if (BankData.LOGIN_ID.equalsIgnoreCase(bank.getLoginMethod())) {
            builder.setIDAndPW(bank.getCertSerial(), bank.getPassWord());
        } else {
            builder.setAuth(bank.getCertSerial(), bank.getPassWord());
        }
        if (billdate != null) {
            builder.setBillDate(billdate);
        }
        BankRequestData data = builder.build();
        data.setOriginalBankCode(bank.getBankCode());
        data.setOriginalBankType(bank.getType());
        data.setOriginalBankName(bank.getBankName());
        data.setIsDaily(isDaily);
        return data;
    }

}
