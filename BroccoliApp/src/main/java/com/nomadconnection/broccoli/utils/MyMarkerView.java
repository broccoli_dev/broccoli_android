
package com.nomadconnection.broccoli.utils;

import android.content.Context;
import android.graphics.RectF;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.nomadconnection.broccoli.R;

/**
 * Custom implementation of the MarkerView.
 * 
 * @author Philipp Jahoda
 */
public class MyMarkerView extends MarkerView {

    private TextView tvContent;
    private int mLayoutResource;
    private int mContentWidth = 0;
    private float offsetX = 2;
    private Chart mChart;
    private int mLeft = 0;
    private int mRight = 0;

    public MyMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);
        mLayoutResource = layoutResource;

        tvContent = (TextView) findViewById(R.id.tvContent);
        mContentWidth = tvContent.getWidth();
    }

    public void setChart(View view) {
        mChart = (Chart) view;
        mChart.post(new Runnable() {
            @Override
            public void run() {
                ViewPortHandler handler = mChart.getViewPortHandler();
                RectF rect = handler.getContentRect();
                mLeft = (int) rect.left;
                mRight = (int) rect.right;
            }
        });
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        if (e instanceof CandleEntry) {

            CandleEntry ce = (CandleEntry) e;

            tvContent.setText("" + Utils.formatNumber(ce.getHigh(), 0, true, ','));
        } else {
            Object obj = e.getData();
            String date = null;
            if (obj != null && obj instanceof String) {
                date = obj.toString();
            }
            if (mLayoutResource == R.layout.custom_marker_view && highlight.getXIndex() >= 0/* <= ChartUtils.main_asset_value.size()*/){
                long val = (long) e.getVal();//ChartUtils.main_asset_value.get(highlight.getXIndex());
                tvContent.setText("" + ElseUtils.getDecimalFormat(val));
            }
            else {
                if (date != null) {
                    tvContent.setText(""+ date + " : " + Utils.formatNumber(e.getVal(), 0, true, ','));
                } else {
                    tvContent.setText("" + Utils.formatNumber(e.getVal(), 0, true, ','));
                }
            }
            mContentWidth = tvContent.getWidth();
        }
    }

    @Override
    public int getXOffset(float xpos) {
        // this will center the marker-view horizontally
        if (xpos < mContentWidth/2) {
            float temp = mContentWidth/2 - xpos;
            float value = temp/(mContentWidth/2);
            offsetX = 2f + 8f*(value);
        } else if (xpos >= mContentWidth/2 && xpos <= mRight-mContentWidth/2) {
            offsetX = 2f;
        } else {
            float temp = xpos - (mRight - mContentWidth/2);
            float value = temp/(mContentWidth/2);
            offsetX = 1f + (1f - value);
        }
        return (int) -(getWidth() / offsetX);
    }

    @Override
    public int getYOffset(float ypos) {
        // this will cause the marker-view to be above the selected value
        return -getHeight();
    }
}
