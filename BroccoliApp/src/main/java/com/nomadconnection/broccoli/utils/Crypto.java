package com.nomadconnection.broccoli.utils;

import android.util.Base64;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {

	private final static String HEX = "0123456789ABCDEF";
	private final static String SHA1 = "SHA1PRNG";
	private final static String AES = "AES";
    private final static String UTF8 = "UTF-8";
	
    private final static int JELLY_BEAN_4_2 = 17;
    
   
    // 암호화에 사용할 키. 원하는 값으로 바꿔주자.
    private final static byte[] key = {(byte)0x01, (byte)0x09, (byte)0x09, (byte)0x05, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x03, (byte)0x01, (byte)0x06, (byte)0x07, (byte)0x04, (byte)0x07, (byte)0x00, (byte)0x00, (byte)0x00 };

    /**
     * AES 암호화
     * @param seed 암호화에 사용할 seed
     * @param cleartext 암호화 할 문자열
     * @return 암호화된 문자열
     * @throws Exception 암호화 key 사이즈가 192bit 또는 128bit로 감소함.
     */
    public static String encrypt(String seed, String cleartext) throws Exception {
    	if (cleartext == null || cleartext == "") 
    		return null;
    	byte[] rawKey = getRawKey(seed.getBytes());
    	byte[] result = encrypt(rawKey, cleartext.getBytes(UTF8));
    	String fromHex = toHex(result);
    	String base64 = new String(Base64.encodeToString(fromHex.getBytes(), Base64.DEFAULT));
        return base64;
    }

    /**
     * AES 복호화
     * @param seed 복호화에 사용할 seed
     * @param encrypted 복호화 할 문자열
     * @return 복호화한 문자열
     * @throws Exception 암호화 key 사이즈가 192bit 또는 128bit로 감소함.
     */
    public static String decrypt(String seed, String encrypted) throws Exception {
        if (encrypted == null || encrypted == "")
        	return null;
        byte[] seedByte = seed.getBytes("UTF8");
        System.arraycopy(seedByte, 0, key, 0, ((seedByte.length < 16) ? seedByte.length : 16));
        String base64 = new String(Base64.decode(encrypted, Base64.DEFAULT));
        byte[] rawKey = getRawKey(seedByte);
        byte[] enc = toByte(base64);
        byte[] result = decrypt(rawKey, enc);
        return new String(result, UTF8);
    }


    public static byte[] encryptBytes(String seed, byte[] cleartext) throws Exception {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] result = encrypt(rawKey, cleartext);
        return result;
    }

    public static byte[] decryptBytes(String seed, byte[] encrypted) throws Exception {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] result = decrypt(rawKey, encrypted);
        return result;
    }

    private static byte[] getRawKey(byte[] seed) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance(AES);
        SecureRandom sr = null;
        if (android.os.Build.VERSION.SDK_INT >= JELLY_BEAN_4_2) {
            sr = SecureRandom.getInstance(SHA1, "Crypto");
        } else {
            sr = SecureRandom.getInstance(SHA1);
        }
        sr.setSeed(seed);
        try {
            kgen.init(256, sr);
        } catch (Exception e) {
        	//This device doesn't suppor 256bits, trying 192bits.
            try {
                kgen.init(192, sr);
            } catch (Exception e1) {
                //This device doesn't suppor 192bits, trying 128bits.
                kgen.init(128, sr);
            }
        }
        SecretKey skey = kgen.generateKey();
        byte[] raw = skey.getEncoded();
        return raw;
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, AES);
        Cipher cipher = Cipher.getInstance(AES);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, AES);
        Cipher cipher = Cipher.getInstance(AES);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public static String toHex(String txt) {
            return toHex(txt.getBytes());
    }
   
    public static String fromHex(String hex) {
            return new String(toByte(hex));
    }
   
    public static byte[] toByte(String hexString) {
            int len = hexString.length()/2;
            byte[] result = new byte[len];
            for (int i = 0; i < len; i++)
                    result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
            return result;
    }

    public static String toHex(byte[] buf) {
            if (buf == null)
            	return "";
            StringBuffer result = new StringBuffer(2*buf.length);
            for (int i = 0; i < buf.length; i++) {
                    appendHex(result, buf[i]);
            }
            return result.toString();
    }

    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }

    public static Key getCDNAESKey() throws Exception {
        Key keySpec;

        String key = "hmVfFzdhdeoKIM1s";
        byte[] keyBytes = new byte[16];
        byte[] b = key.getBytes("UTF-8");

        int len = b.length;
        if (len > keyBytes.length) {
            len = keyBytes.length;
        }

        System.arraycopy(b, 0, keyBytes, 0, len);
        keySpec = new SecretKeySpec(keyBytes, "AES");

        return keySpec;
    }

    // 암호화
    public static String encCDNAES(String str) throws Exception {
        Key keySpec = getCDNAESKey();
        String iv = "7T29pXIhIsFAaqmD";
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes("UTF-8")));
        byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
        String enStr = new String(Base64.encodeToString(encrypted, Base64.DEFAULT));

        return enStr;
    }

    // 복호화
    public static String decCDNAES(String enStr) throws Exception {
        Key keySpec = getCDNAESKey();
        String iv = "7T29pXIhIsFAaqmD";
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes("UTF-8")));
        byte[] byteStr = Base64.decode(enStr.getBytes("UTF-8"), Base64.DEFAULT);
        String decStr = new String(c.doFinal(byteStr), "UTF-8");

        return decStr;
    }
	
}
