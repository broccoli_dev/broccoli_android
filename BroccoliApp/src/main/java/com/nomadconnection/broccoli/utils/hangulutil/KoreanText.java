package com.nomadconnection.broccoli.utils.hangulutil;

/*
 * Copyright 2014 Bang Jun-young
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Created by YelloHyunminJang on 2017. 1. 19..
 */

public class KoreanText {

    public enum KoreanCategory {
        HANGUL_COMPAT,
        HANGUL_JAMO,
        HANGUL_CHO,
        HANGUL_NOT
    }

    private static final int CHOSEONG_COUNT = 19;
    private static final int JUNGSEONG_COUNT = 21;
    private static final int JONGSEONG_COUNT = 28;
    private static final int HANGUL_SYLLABLE_COUNT = CHOSEONG_COUNT * JUNGSEONG_COUNT * JONGSEONG_COUNT;
    private static final int HANGUL_SYLLABLES_BASE = 0xAC00;
    private static final int HANGUL_SYLLABLES_END = HANGUL_SYLLABLES_BASE + HANGUL_SYLLABLE_COUNT;

    // ㄱ ㄲ ㄴ ㄷ ㄸ ㄹ ㅁ ㅂ ㅃ ㅅ ㅆ ㅇ ㅈ ㅉ ㅊ ㅋ ㅌ ㅍ ㅎ
    private static final char[] CHO =
            {0x3131, 0x3132, 0x3134, 0x3137, 0x3138, 0x3139, 0x3141,
                    0x3142, 0x3143, 0x3145, 0x3146, 0x3147, 0x3148,
                    0x3149, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e};

    //ㅏㅐㅑㅒㅓㅔㅕㅖ ㅗ ㅘ ㅙ ㅚ ㅛ ㅜ ㅝ ㅞ ㅟ ㅠ ㅡ ㅢ ㅣ
    private static final char[] JUN =
            {0x314f, 0x3150, 0x3151, 0x3152, 0x3153, 0x3154, 0x3155,
                    0x3156, 0x3157, 0x3158, 0x3159, 0x315a, 0x315b,
                    0x315c, 0x315d, 0x315e, 0x315f, 0x3160,    0x3161,
                    0x3162, 0x3163};

    // ㄱㄲㄳㄴㄵㄶㄷㄹㄺ ㄻ ㄼ ㄽ ㄾ ㄿ ㅀ ㅁ ㅂ ㅄ ㅅ ㅆ ㅇ ㅈ ㅊ ㅋ ㅌ ㅍ ㅎ
    private static final char[] JON =
            {0x0000, 0x3131, 0x3132, 0x3133, 0x3134, 0x3135, 0x3136,
                    0x3137, 0x3139, 0x313a, 0x313b, 0x313c, 0x313d,
                    0x313e, 0x313f, 0x3140, 0x3141, 0x3142, 0x3144,
                    0x3145, 0x3146, 0x3147, 0x3148, 0x314a, 0x314b,
                    0x314c, 0x314d, 0x314e};

    public static boolean isChoseong(char c) {
        return 0x1100 <= c && c <= 0x1112;
    }

    public static boolean isCompatChoseong(char c) {
        return 0x3131 <= c && c <= 0x314E;
    }

    public static boolean isKoreanChar(char value) {
        boolean ret = isSyllable(value);
        if (!ret) {
            ret = isCompatChoseong(value);
        }
        if (!ret) {
            ret = isChoseong(value);
        }

        return ret;
    }

    public static KoreanCategory getKoreanCategory(char value) {
        KoreanCategory category = KoreanCategory.HANGUL_NOT;
        boolean ret = isSyllable(value);
        if (ret) {
            category = KoreanCategory.HANGUL_COMPAT;
        }
        if (!ret) {
            ret = isCompatChoseong(value);
            if (ret) {
                category = KoreanCategory.HANGUL_COMPAT;
            }
        }
        if (!ret) {
            ret = isChoseong(value);
            if (ret) {
                category = KoreanCategory.HANGUL_CHO;
            }
        }
        if (!ret) {
            if ((value >= 0x3131 && value <= 0x318f)
                    || (value >= 0x1100 && value <= 0x11ff)) {
                category = KoreanCategory.HANGUL_JAMO;
                ret = true;
            }
        }

        return category;
    }

    public static boolean isSyllable(char c) {
        return HANGUL_SYLLABLES_BASE <= c && c < HANGUL_SYLLABLES_END;
    }

    public static char getChoseong(char value) {
        if (!isSyllable(value))
            return '\0';

        final int choseongIndex = getChoseongIndex(value);
        return (char)(0x1100 + choseongIndex);
    }

    public static char getCompatChoseong(char value) {
        if (!isSyllable(value))
            return '\0';

        final int choseongIndex = getChoseongIndex(value);
        return CHO[choseongIndex];
    }

    private static int getChoseongIndex(char syllable) {
        final int syllableIndex = syllable - HANGUL_SYLLABLES_BASE;
        final int choseongIndex = syllableIndex / (JUNGSEONG_COUNT * JONGSEONG_COUNT);
        return choseongIndex;
    }

    public static boolean isEnglish(char value) {
        return (value >= 0x0041 && value <= 0x005A)
                || (value >= 0x0061 && value <= 0x007A);
    }

    public static boolean isNumber(char value) {
        return (value >= 0x0030 && value <= 0x0039);
    }
}
