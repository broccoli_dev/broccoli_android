package com.nomadconnection.broccoli.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Splash;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoliespider.data.CardCode;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ElseUtils {

	private static DecimalFormat mDecimalFormat = new DecimalFormat("###,###");


	//==========================================================================//
	// View 좌표 구하기
	//==========================================================================//
	/** View 좌표 구하기 **/
	public static Rect getRect(View _View){
		Rect mRect = new Rect();
		_View.getGlobalVisibleRect(mRect); //RootView 레이아웃을 기준으로한 좌표.
		return mRect;
	}

	/** Status Bar 좌표 구하기 **/
	public static Rect getStatusBar(Activity _Activity){
		Rect mRect = new Rect();
		Window window= _Activity.getWindow();
		window.getDecorView().getWindowVisibleDisplayFrame(mRect);
		return mRect;
	}

	//===============================================
	// email 패턴확인
	//===============================================
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
			Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	public static boolean validateEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}


	//==========================================================================//
	// Value 변환
	//==========================================================================//
	/** dip -> px 구하기 **/
	public static float getDiptoPx(int _dip){
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _dip, MainApplication.mAPPResources.getDisplayMetrics());
		return px;
	}

	//==========================================================================//
	// 일반 숫자를 화폐단위 문자포맷으로 변경해서 돌려줌
	//==========================================================================//
	/** 10000 -> 10,000 **/
	public static String getDecimalFormat(long price) {
		String priceStr = mDecimalFormat.format(price);
		return priceStr;
	}


	//==========================================================================//
	// 문자 숫자를 화폐단위 문자포맷으로 변경해서 돌려줌
	//==========================================================================//
	/** 10000 -> 10,000 **/
	public static String getStringDecimalFormat(String price) {
		long mPrice = Long.parseLong(price);
		String priceStr = mDecimalFormat.format(mPrice);
		return priceStr;
	}


	//==========================================================================//
	// Date 문자포맷으로 변경해서 돌려줌 yyyyMMddHHmmss => yyyy.MM.dd a hh:mm
	//==========================================================================//
	private static final SimpleDateFormat InputTimeFormat = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
	private static final SimpleDateFormat InputTimeFormatSpend = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
	private static final SimpleDateFormat OutputDateFormat = new SimpleDateFormat("yyyy.MM.dd  a hh:mm");
	private static final SimpleDateFormat OutputDateFormatSpend = new SimpleDateFormat("yyyy.MM.dd");

	public static String getDate(String str) {
		String date = null;
		try {
			date = OutputDateFormat.format(InputTimeFormat.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static String getSpendDate(String str) {
		String date = null;
		try {
			if(str.length() == 8){
				date = OutputDateFormatSpend.format(InputTimeFormatSpend.parse(str));
			}
			else {
				date = OutputDateFormat.format(InputTimeFormat.parse(str));
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static String getSpendDateForHeader(String str) {
		String date = null;
		try {
			if(str.length() == 8){
				date = MonthDayDateFormat.format(InputTimeFormatSpend.parse(str));
			}
			else {
				date = MonthDayDateFormat.format(InputTimeFormat.parse(str));
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	private static final SimpleDateFormat InputTimeFormatNotice = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);

	public static String getDateNotice(String str) {
		String date = null;
		try {
			date = OutputDateFormat.format(InputTimeFormatNotice.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static String getDateSettingsNotice(String str) {
		String date = null;
		try {
			date = OutputDateFormatSpend.format(InputTimeFormatNotice.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}


	//private static final SimpleDateFormat CalendarTimeFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy");
	private static final SimpleDateFormat YearMonthDateFormat = new SimpleDateFormat("yyyyMM");

	public static String getYearMonth(Date targetDate) {
		String date = null;
		try {
			date = YearMonthDateFormat.format(targetDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	private static final SimpleDateFormat YearMonthDayDateFormat = new SimpleDateFormat("yyyyMMdd");

	public static String getYearMonthDay(Date targetDate) {
		String date = null;
		try {
			date = YearMonthDayDateFormat.format(targetDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	private static final SimpleDateFormat MonthDayStringFormat = new SimpleDateFormat("M월 d일");

	/**
	 * date : yyyyMMdd
	 * @param targetDate
	 * @return
	 */
	public static String getYearMonthDayString(String targetDate) {
		String date = null;
		try {
			date = MonthDayStringFormat.format(YearMonthDayDateFormat.parse(targetDate));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	private static final SimpleDateFormat BirthDayDateFormat = new SimpleDateFormat("yyyy.MM.dd");

	public static String getBirthDay(String str) {
		String date = null;
		try {
			date = BirthDayDateFormat.format(YearMonthDayDateFormat.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	private static final SimpleDateFormat MonthDayDateFormat = new SimpleDateFormat("MM.dd");
	public static String getMonthDay(String str) {
		String date = null;
		try {
			date = MonthDayDateFormat.format(YearMonthDayDateFormat.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static String getBillDate(String str) {
		String date = null;
		try {
			date = YearMonthDateFormat.format(YearMonthDayDateFormat.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static String getMonthDayByCalendar(Date targetDate) {
		String date = null;
		try {
			date = MonthDayDateFormat.format(targetDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static String dateUploadFormat(String str) {
		String date = null;
		try {
			date = YearMonthDayDateFormat.format(BirthDayDateFormat.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static boolean isExpireDate(String date) {
		boolean ret = false;
		Date day = new Date();
		Date expire = null;
		if (date == null) {
			return ret;
		}
		try {
			expire = EditTimeFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (null != expire) {
			if (day.after(expire)) {
				ret = true;
			}
		}
		return ret;
	}

	private static final SimpleDateFormat ServerDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");

	public static String getSimpleDate(Date targetDate) {
		String date = null;
		try {
			date = ServerDateFormat.format(targetDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static String getSimpleDateToYearMonthDay(String str) {
		String date = null;
		try {
			date = YearMonthDayDateFormat.format(ServerDateFormat.parse(str));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}


	private static final SimpleDateFormat InitTimeFormat = new SimpleDateFormat("yyyyMMddhhmm", Locale.KOREA);
	private static final SimpleDateFormat EditTimeFormat = new SimpleDateFormat("yyyy.MM.dd");

	public static String getEditDate(String str) {
		String date = null;
		try {
			date = EditTimeFormat.format(InitTimeFormat.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	private static final SimpleDateFormat CardLoanFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);

	public static String getCardLoanDate(String str) {
		String date = null;
		if (str == null)
		{
			return "";
		}
		try {
			date = EditTimeFormat.format(CardLoanFormat.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	private static final SimpleDateFormat SpendHistoryFormat = new SimpleDateFormat("yyyy.MM.dd  a hh:mm");

	public static String getServerDate(String str) {
		String date = null;
		try {
			date = InputTimeFormat.format(SpendHistoryFormat.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	private static final SimpleDateFormat HourMinuteFixFormat = new SimpleDateFormat("  a hh:mm");
	private static final SimpleDateFormat HourMinuteFormat = new SimpleDateFormat("hhmm");

	public static String getHourMinute(String str) {
		String hour = null;
		try {
			hour = HourMinuteFixFormat.format(HourMinuteFormat.parse(str));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return hour;
	}

	/**
	 * service가 실행중인지 체크
	 *
	 * @param classNme
	 * @return
	 */
	public static boolean isServiceRunning(Context ctx, String classNme) {
		ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> info;
		info = am.getRunningServices(100);

		boolean isCheck = false;
		for (Iterator<ActivityManager.RunningServiceInfo> iterator = info.iterator(); iterator.hasNext(); ) {

			ActivityManager.RunningServiceInfo runningTaskInfo = (ActivityManager.RunningServiceInfo) iterator.next();

			if (runningTaskInfo.service.getClassName().equals(classNme)) {
				isCheck = true;
				break;
			}
		}

		return isCheck;
	}

	/**
	 * 소수점 자르기
	 * @param num
	 * @return
	 */
	public static String getPointCut(double num) {
		String ret = null;

		ret = String.format("%.2f", num);

		return ret;
	}

	/* 키보드 막기 - 최초 화면 뜰때 */
	public static void hideSoftKeyboardAtFirstLayoutOpen(Activity _Activity) {
		_Activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	/* 키보드 막기 */
	public static void hideSoftKeyboard(final EditText editText) {
		((InputMethodManager) MainApplication.mAPPContext.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
				editText.getWindowToken(), 0);
	}

	/* 키보드 보여주기 */
	public static void showSoftKeyboard(final EditText editText) {
		((InputMethodManager) MainApplication.mAPPContext.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(editText, InputMethodManager.SHOW_FORCED);
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static void showNotification(Context context, String sender, String msg) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
		builder.setPriority(NotificationCompat.PRIORITY_MAX);
		builder.setVibrate(new long[]{1, 1, 1});
		builder.setCategory(NotificationCompat.CATEGORY_STATUS);
		builder.setSmallIcon(getNotificationIcon());//required
		builder.setColor(context.getResources().getColor(R.color.common_rgb_56_136_255));
		builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.notification_large));
		builder.setContentTitle(sender);//required
		builder.setContentText(msg);//required
		builder.setTicker(sender);//optional
		builder.setAutoCancel(true);
		Intent resultIntent = openApp(context, context.getPackageName());
		resultIntent.setAction(Intent.ACTION_MAIN);
		resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		//stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
		builder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(1, builder.build());
	}

	public static Intent openApp(Context context, String packageName) {
		PackageManager manager = context.getPackageManager();
		Intent i = manager.getLaunchIntentForPackage(packageName);
		if (i == null) {
            return null;
            //throw new PackageManager.NameNotFoundException();
        }
		i.addCategory(Intent.CATEGORY_LAUNCHER);
		return i;
	}

	private static Point mScreenSize = null;

	public static Point getScreenSize(Context context) {
		if (mScreenSize == null) {
			mScreenSize = new Point();
			WindowManager winmgr = (WindowManager) context.getSystemService(Activity.WINDOW_SERVICE);
			Display dis = winmgr.getDefaultDisplay();
			DisplayMetrics dm = new DisplayMetrics();
			dis.getMetrics(dm);
			mScreenSize.set(dm.widthPixels, dm.heightPixels);
		}
		return mScreenSize;
	}

	public static void network_error(Activity activity){
		if (activity != null && !activity.isFinishing()) {
			Toast.makeText(activity, activity.getResources().getString(R.string.common_network_error), Toast.LENGTH_SHORT).show();
		}
	}


	public static int getNotificationIcon() {
		boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
		return useWhiteIcon ? R.drawable.notification : R.drawable.notification_color;
	}

	public static void showPlayStore(Context context) {
		final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
		try {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
		}
	}

	private final static int NUMBER_ZERO = 48;
	private final static int NUMBER_NINE = 57;
	public static boolean isNumeric(String str){
		boolean isIncludeNumber = true;

		if (str == null) {
			str = "";
		}

		if(str.equals("")){
			isIncludeNumber = false;
		}

		char check;
		for(int i=0; i < str.length(); i++){
			check = str.charAt(i);
			if( check < NUMBER_ZERO || check > NUMBER_NINE) {
				isIncludeNumber = false;
				break;
			}
		}
		return isIncludeNumber;
	}

	public static int convertDp2Px(Context context, float dp) {
		Resources r = context.getResources();
		int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
		return px;
	}

	public static int convertPx2Dp(Context context, float dp) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float logicalDensity = metrics.density;
		int px = (int) Math.ceil(dp * logicalDensity);
		return px;
	}

	public static String convertHangul(String money){
		String[] han1 = {"","일","이","삼","사","오","육","칠","팔","구"};
		String[] han2 = {"","십","백","천"};
		String[] han3 = {"","만","억","조","경"};

		StringBuffer result = new StringBuffer();
		int len = money.length();
		for(int i=len-1; i>=0; i--){
			result.append(han1[Integer.parseInt(money.substring(len-i-1, len-i))]);
			if(Integer.parseInt(money.substring(len-i-1, len-i)) > 0)
				result.append(han2[i%4]);
			if(i%4 == 0)
				result.append(han3[i/4]);
		}

		return result.toString();
	}

	public static String makePhoneNumber(String phoneNumber) {
		String regEx = "(\\d{3})(\\d{3,4})(\\d{4})";

		if(!Pattern.matches(regEx, phoneNumber)) return null;

		return phoneNumber.replaceAll(regEx, "$1-$2-$3");

	}

	/**
	 * input : (881122-1)****** 괄호 안에 하이픈 제외한 숫자만
	 * output : 태어난년도, 성별
	 * @param str
	 * @return
     */
	public static String[] getBirthYear(String str) {
		String birth[] = null;

		if (str.length() >= 7) {
			String year = str.substring(0, 6);
			String gene = str.substring(6, 7);
			String birthYear = null;
			String gender = null;
			if (gene != null) {
				birth = new String[2];
				int generation = Integer.parseInt(gene);
				switch (generation) {
					case 9:
					case 0:
						birthYear = "18"+year;
						if (generation == 9) {
							gender = "M";
						} else {
							gender = "F";
						}
						break;
					case 1:
					case 2:
						birthYear = "19"+year;
						if (generation == 1) {
							gender = "M";
						} else {
							gender = "F";
						}
						break;
					case 3:
					case 4:
						birthYear = "20"+year;
						if (generation == 3) {
							gender = "M";
						} else {
							gender = "F";
						}
						break;
					case 5:
					case 6:
						birthYear = "19"+year;
						if (generation == 5) {
							gender = "M";
						} else {
							gender = "F";
						}
						break;
					case 7:
					case 8:
						birthYear = "20"+year;
						if (generation == 7) {
							gender = "M";
						} else {
							gender = "F";
						}
						break;
				}
				birth[0] = birthYear;
				birth[1] = gender;
			}
		}

		return birth;
	}

	public static String getLockDownTime(Context context) {
		return APPSharedPrefer.getInstance(context).getPrefString(APP.SETTING_PASSOWORD_DELAY_TIME);
	}

	public static void setLockDownTime(Context context, String time) {
		APPSharedPrefer.getInstance(context).setPrefString(APP.SETTING_PASSOWORD_DELAY_TIME, time);
	}

	public static void setLockOverCount(Context context, int count) {
		APPSharedPrefer.getInstance(context).setPrefInt(APP.SETTING_PASSOWORD_CHECK_COUNT, count);
	}

	public static int getLockOverCount(Context context) {
		return APPSharedPrefer.getInstance(context).getPrefInt(APP.SETTING_PASSOWORD_CHECK_COUNT);
	}

	public static void setCertCount(Context context, int count) {
		APPSharedPrefer.getInstance(context).setPrefInt(APP.SETTING_CERT_CHECK_COUNT, count);
	}

	public static int getCertCount(Context context) {
		return APPSharedPrefer.getInstance(context).getPrefInt(APP.SETTING_CERT_CHECK_COUNT);
	}

	public static void setCertOverTime(Context context, String time) {
		APPSharedPrefer.getInstance(context).setPrefString(APP.SETTING_CERT_OVER_TIME, time);
	}

	public static String getCertOverTime(Context context) {
		return APPSharedPrefer.getInstance(context).getPrefString(APP.SETTING_CERT_OVER_TIME);
	}

	public static void setCertOverCount(Context context, int count) {
		APPSharedPrefer.getInstance(context).setPrefInt(APP.SETTING_CERT_OVER_COUNT, count);
	}

	public static int getCertOverCount(Context context) {
		return APPSharedPrefer.getInstance(context).getPrefInt(APP.SETTING_CERT_OVER_COUNT);
	}

	public static String getDeviceUUID(final Context context) {
		if (Splash.ISTEST) {
			return "96fcc346-8fe8-33fc-9720-17aeebfffd9d";
		}
		final APPSharedPrefer prefer = APPSharedPrefer.getInstance(context);
		final String id = prefer.getPrefString(APP.PROPERTY_DEVICE_ID);

		UUID uuid = null;
		if (id != null && !id.isEmpty()) {
			uuid = UUID.fromString(id);
		} else {
			final String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
			try {
				if (!"9774d56d682e549c".equals(androidId)) {
					uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
				} else {
					final String deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
					uuid = deviceId != null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")) : UUID.randomUUID();
				}
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}

			prefer.setPrefString(APP.PROPERTY_DEVICE_ID, uuid.toString());
		}

		return uuid.toString();
	}

	public static boolean isRunningProcess(Context context, String packageName) {

		boolean isRunning = false;

		ActivityManager actMng = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);

		List<ActivityManager.RunningTaskInfo> list = actMng.getRunningTasks(100);

		for(ActivityManager.RunningTaskInfo rap : list) {
			if(rap.baseActivity.getPackageName().equals(packageName)) {
				isRunning = true;
				break;
			}
			rap.topActivity.getClass();
		}

		return isRunning;
	}

	// 킷캣에서 추가된  document식 Path
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri.getAuthority());
	}

	// 기본 경로 ( 킷캣 이전버전 )
	public static boolean isPathSDCardType(Uri uri) {
		// Path : external/images/media/ID(1234...)
		return "external".equals(uri.getPathSegments().get(0));
	}

	// 구글 드라이브를 통한 업로드 여부 체크.
	public static boolean isGooglePhotoUri(Uri uri) {
		return "com.google.android.apps.photos.content".equals(uri.getAuthority());
	}

	// URI 를 받아서 Column 데이터 접근.
	public static String getDataColumn(Context context, Uri uri, String selection,
									   String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = {
				column
		};

		try {

			cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs ,null);
			if (cursor != null && cursor.moveToFirst()) {
				final int column_index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(column_index);
			}

		} finally {
			if (cursor != null)
				cursor.close();
		}

		return null;
	}

	public static String getPath(Context ctx, Uri uri) {

		final boolean isAndroidVersionKitKat = Build.VERSION.SDK_INT >=  19; // ( == Build.VERSION_CODE.KITKAT )

		// Check Google Drive.
		if(isGooglePhotoUri(uri)) {
			return uri.getLastPathSegment();
		}

		// 1. 안드로이드 버전 체크
		// com.android.providers.media.documents/document/image :: uri로 전달 받는 경로가 킷캣으로 업데이트 되면서 변경 됨.
		if(isAndroidVersionKitKat && DocumentsContract.isDocumentUri(ctx, uri)) {

			//com.android.providers.media.documents/document/image:1234 ...
			//
			if(isMediaDocument(uri) && DocumentsContract.isDocumentUri(ctx, uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;

				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

				} else if ("video".equals(type)) {
					return null; // 필자는 이미지만 처리할 예정이므로 비디오 및 오디오를 불러오는 부분은 작성하지 않음.

				} else if ("audio".equals(type)) {
					return null;
				}

				final String selection = MediaStore.Images.Media._ID + "=?";
				final String[] selectionArgs = new String[] {
						split[1]
				};

				return getDataColumn(ctx, contentUri, selection, selectionArgs);
			}

		}

		// content://media/external/images/media/....
		// 안드로이드 버전에 관계없이 경로가 com.android... 형식으로 집히지 않을 수 도 있음. [ 겔럭시S4 테스트 확인 ]
		if(isPathSDCardType(uri)) {

			final String selection = MediaStore.Images.Media._ID + "=?";
			final String[] selectionArgs = new String[] {
					uri.getLastPathSegment()
			};

			return getDataColumn(ctx,  MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection, selectionArgs);
		}

		// File 접근일 경우
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
											int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			inSampleSize = heightRatio > widthRatio ? heightRatio : widthRatio;

			// This offers some additional logic in case the image has a strange
			// aspect ratio. For example, a panorama may have a much larger
			// width than height. In these cases the total pixels might still
			// end up being too large to fit comfortably in memory, so we should
			// be more aggressive with sample down the image (=larger inSampleSize).

			final float totalPixels = width * height;

			// Anything more than 2x the requested pixels we'll sample down further
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;

			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}
		}
		return inSampleSize;
	}

	public static int getExifOrientation(String filepath) {
		int degree = 0;
		ExifInterface exif = null;

		try {
			exif = new ExifInterface(filepath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (exif != null) {
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
			if (orientation != -1) {
				switch(orientation) {
					case ExifInterface.ORIENTATION_ROTATE_90:
						degree = 90;
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						degree = 180;
						break;
					case ExifInterface.ORIENTATION_ROTATE_270:
						degree = 270;
						break;
				}
			}
		}
		return degree;
	}

	public static Bitmap GetRotatedBitmap(Bitmap bitmap, int degrees) {
		if (degrees != 0 && bitmap != null) {
			Matrix m = new Matrix();
			m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

			try {
				Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);

				if (bitmap != rotate) {
					bitmap.recycle();
					bitmap = rotate;
				}
			} catch (OutOfMemoryError e) {
				// 메모리 부족에러시, 원본을 반환
			}
		}

		return bitmap;
	}

	public static Uri saveImage(Context context, Bitmap bitmap, String filename) {
		File resizedFile = new File(context.getCacheDir(), filename);

		OutputStream fOut=null;
		try {
			fOut = new BufferedOutputStream(new FileOutputStream(resizedFile));
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
			fOut.flush();
			fOut.close();
			bitmap.recycle();
		} catch (Exception e) { // TODO
		}
		return Uri.fromFile(resizedFile);
	}

	public static int isHigherVersion(String[] appVersion, String[] serverVersion) {
		int length = Math.max(appVersion.length, serverVersion.length);
		for (int i=0; i < length; i++) {
			int app = i < appVersion.length ? Integer.parseInt(appVersion[i]) : 0;
			int server = i < serverVersion.length ? Integer.parseInt(serverVersion[i]) : 0;
			if (app < server)
				return -1;
			if (app > server)
				return 1;
		}
		return 0;
	}

	public static int createCardLogoItem(String code) {
		int drawable = 0;
		if (CardCode.BC.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_010;
		} else if (CardCode.CITY.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_009;
		} else if (CardCode.HANA.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_006;
		} else if (CardCode.HYUNDAE.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_004;
		} else if (CardCode.KB.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_002;
		} else if (CardCode.LOTTE.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_003;
		} else if (CardCode.NH.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_008;
		} else if (CardCode.SAMSUNG.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_003;
		} else if (CardCode.SHINHAN.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_001;
		} else if (CardCode.WOORI.equalsIgnoreCase(code)) {
			drawable = R.drawable.cd_005;
		} else {
			drawable = R.drawable.cd_default;
		}
		return drawable;
	}
}
