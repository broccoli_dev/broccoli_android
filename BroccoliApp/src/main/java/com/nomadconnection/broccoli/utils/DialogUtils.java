package com.nomadconnection.broccoli.utils;

import android.animation.Animator;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.AlertDialogActivity;
import com.nomadconnection.broccoli.activity.CloudMessageActivity;
import com.nomadconnection.broccoli.activity.membership.LoginActivity;
import com.nomadconnection.broccoli.activity.remit.RemitReceiveMoneyActivity;
import com.nomadconnection.broccoli.adapter.spend.AdtExpListViewSpendCardSelect;
import com.nomadconnection.broccoli.common.CustomExpandableListView;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogCustomType;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeBank;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendPhone;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitChargeMoney;
import com.nomadconnection.broccoli.data.Scrap.ServiceRecord;
import com.nomadconnection.broccoli.data.Spend.ResponseCardList;
import com.nomadconnection.broccoli.data.Spend.SpendCardDetailAnnualFee;
import com.nomadconnection.broccoli.dialog.DialogBaseGridOrList;
import com.nomadconnection.broccoli.dialog.DialogBaseList;
import com.nomadconnection.broccoli.dialog.DialogBaseListBank;
import com.nomadconnection.broccoli.dialog.DialogBaseLoading;
import com.nomadconnection.broccoli.dialog.DialogBaseOneButton;
import com.nomadconnection.broccoli.dialog.DialogBaseOneButtonNoTitle;
import com.nomadconnection.broccoli.dialog.DialogBaseTwoButton;
import com.nomadconnection.broccoli.dialog.DialogNoticeTwoButton;
import com.nomadconnection.broccoli.dialog.DialogSpinner;
import com.nomadconnection.broccoli.dialog.DialogSpinner220;
import com.nomadconnection.broccoli.dialog.DialogTutorialNoticeImage;
import com.nomadconnection.broccoli.dialog.remit.DialogBaseRemitChargeMoney;
import com.nomadconnection.broccoli.dialog.remit.DialogBaseRemitEditButton;
import com.nomadconnection.broccoli.dialog.remit.DialogBaseRemitList;
import com.nomadconnection.broccoli.dialog.remit.DialogBaseRemitSendAccount;
import com.nomadconnection.broccoli.dialog.remit.DialogBaseRemitSendPhone;
import com.nomadconnection.broccoli.dialog.remit.DialogBaseRemitTwoButton;
import com.nomadconnection.broccoli.view.SideSelector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DialogUtils {

	public static DialogBaseLoading mLoading;
	/** show Loading **/
	public static void showLoading(Activity _activity){
		dismissLoading();
		if (_activity != null && !_activity.isFinishing()) {
			mLoading = new DialogBaseLoading(_activity);
			mLoading.setOwnerActivity(_activity);
			mLoading.setCanceledOnTouchOutside(false);
			mLoading.setCancelable(false);
			mLoading.show();
		}
	}

	/** dismiss Loading **/
	public static void dismissLoading(){
		if (mLoading != null) {
			if (mLoading.getOwnerActivity() != null && !mLoading.getOwnerActivity().isFinishing()) {
				mLoading.dismiss();
			}
		}
		else Log.e("broccoli", "else dismissLoading");
	}

	/** Base One Button **/
	public static void showDlgBaseOneButton(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog){
		DialogBaseOneButton mPopup = new DialogBaseOneButton(_activity, _popupBtnClickListener, _infoDataDialog);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
	}

	/** Base One Button **/
	public static void showDlgBaseOneButton(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, DialogInterface.OnDismissListener _dissmissListener){
		DialogBaseOneButton mPopup = new DialogBaseOneButton(_activity, _popupBtnClickListener, _infoDataDialog);
		mPopup.setCanceledOnTouchOutside(false);
		mPopup.setOnDismissListener(_dissmissListener);
//		mPopup.setCancelable(false);
		mPopup.show();
	}


	/** Base One Button No Title **/
	public static void showDlgBaseOneButtonNoTitle(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog){
		DialogBaseOneButtonNoTitle mPopup = new DialogBaseOneButtonNoTitle(_activity, _popupBtnClickListener, _infoDataDialog);
		mPopup.setCanceledOnTouchOutside(false);
		mPopup.setCancelable(false);
		mPopup.show();
	}

	public static void showDialogBaseOneButton(Activity activity, final OnClickListener listener, String title, String text) {
		CommonDialogTextType dialogTextType = new CommonDialogTextType(activity, title, new int[] {R.string.common_confirm});
		dialogTextType.addTextView(text);
		dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				switch (id) {
					case R.string.common_confirm:
						listener.onClick(dialog, id);
						break;
					default:
						dialog.dismiss();
						break;
				}
			}
		});
		dialogTextType.show();
	}

	public static void showDialogBaseOneButtonNoTitle(Activity activity, final OnClickListener listener, String text) {
		CommonDialogTextType dialogTextType = new CommonDialogTextType(activity, new int[] {R.string.common_confirm});
		dialogTextType.addTextView(text);
		dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				switch (id) {
					case R.string.common_confirm:
						listener.onClick(dialog, id);
						break;
					default:
						dialog.dismiss();
						break;
				}
			}
		});
		dialogTextType.show();
	}

	public static void showDialogBaseTwoButton(Activity activity, final OnClickListener listener, String text) {
		CommonDialogTextType dialogTextType = new CommonDialogTextType(activity, new int[] {R.string.common_confirm, R.string.common_cancel});
		dialogTextType.addTextView(text);
		dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				switch (id) {
					case R.string.common_confirm:
						listener.onClick(dialog, id);
						break;
					default:
						listener.onClick(dialog, id);
						dialog.dismiss();
						break;
				}
			}
		});
		dialogTextType.show();
	}

	/** Base Two Button **/
	public static DialogBaseTwoButton showDlgBaseTwoButton(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog){
		DialogBaseTwoButton mPopup = new DialogBaseTwoButton(_activity, _popupBtnClickListener, _infoDataDialog);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
		return mPopup;
	}
	
	
	/** Dlg Grid or List **/
	public static void showDlgBaseGridOrList(Activity _activity, AdapterView.OnItemClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, int _selectValue){
		DialogBaseGridOrList mPopup = new DialogBaseGridOrList(_activity, _popupBtnClickListener, _infoDataDialog, _selectValue);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
	}

	/** Dlg Grid or List **/
	public static void showDlgBaseGridOrList(Activity _activity, AdapterView.OnItemClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, int _selectValue, String _selectStr1, String _selectStr2){
		DialogBaseGridOrList mPopup = new DialogBaseGridOrList(_activity, _popupBtnClickListener, _infoDataDialog, _selectValue, _selectStr1, _selectStr2);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
	}

	/** Dlg Radio Group **/
	public static void showDlgBaseList(Activity _activity, AdapterView.OnItemClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, int _selectValue){
		DialogBaseList mPopup = new DialogBaseList(_activity, _popupBtnClickListener, _infoDataDialog, _selectValue);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
	}

	/** Dlg Radio Group Bank **/
	public static void showDlgBaseListBank(Activity _activity, AdapterView.OnItemClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, int _selectValue, List<EtcChallengeBank> _etcChallengeBankList){
		DialogBaseListBank mPopup = new DialogBaseListBank(_activity, _popupBtnClickListener, _infoDataDialog, _selectValue, _etcChallengeBankList);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
	}
	
	/** Spinner Dialog 
	 * @param _pos **/
	public static void showDlgSpinnerList(Activity _activity, OnClickListener _popupBtnClickListener, ArrayList<String> _data, int _TempTop, int _TempLeft, int _pos){
		DialogSpinner mPopup = new DialogSpinner(_activity, _popupBtnClickListener, _data, _pos);
		mPopup.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		WindowManager.LayoutParams params = new WindowManager.LayoutParams();
//		params.y = _TempTop-20; params.x = _TempLeft-20;	params.gravity = Gravity.TOP | Gravity.LEFT;
		params.y = _TempTop; params.x = _TempLeft;	params.gravity = Gravity.TOP | Gravity.LEFT;
		mPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		mPopup.getWindow().setAttributes(params);
		mPopup.setCanceledOnTouchOutside(true);
//		mPopup.setCancelable(false);
		mPopup.show();
	}

	/** Spinner Dialog
	 * @param _pos **/
	public static void showDlgSpinnerList(Activity _activity, OnClickListener _popupBtnClickListener, ArrayList<String> _data, int _TempTop, int _TempLeft, int _pos, DialogInterface.OnDismissListener _dissmissListener){
		DialogSpinner mPopup = new DialogSpinner(_activity, _popupBtnClickListener, _data, _pos);
		mPopup.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		WindowManager.LayoutParams params = new WindowManager.LayoutParams();
//		params.y = _TempTop-20; params.x = _TempLeft-20;	params.gravity = Gravity.TOP | Gravity.LEFT;
		params.y = _TempTop; params.x = _TempLeft;	params.gravity = Gravity.TOP | Gravity.LEFT;
		mPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		mPopup.getWindow().setAttributes(params);
		mPopup.setCanceledOnTouchOutside(true);
		mPopup.setOnDismissListener(_dissmissListener);
//		mPopup.setCancelable(false);
		mPopup.show();
	}

	/** Spinner Dialog
	 * @param _pos **/
	public static void showDlgSpinnerList_220(Activity _activity, OnClickListener _popupBtnClickListener, ArrayList<String> _data, int _TempTop, int _TempLeft, int _pos, DialogInterface.OnDismissListener _dissmissListener){
		DialogSpinner220 mPopup = new DialogSpinner220(_activity, _popupBtnClickListener, _data, _pos);
		mPopup.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		WindowManager.LayoutParams params = new WindowManager.LayoutParams();
//		params.y = _TempTop-20; params.x = _TempLeft-20;	params.gravity = Gravity.TOP | Gravity.LEFT;
		params.y = _TempTop; params.x = _TempLeft;	params.gravity = Gravity.TOP | Gravity.LEFT;
		mPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		mPopup.getWindow().setAttributes(params);
		mPopup.setCanceledOnTouchOutside(true);
		mPopup.setOnDismissListener(_dissmissListener);
//		mPopup.setCancelable(false);
		mPopup.show();
	}

	public static void showScrapingDialog(Context context, int requestCode, String title, String msg, String type, int mode, ArrayList<ServiceRecord> list){
		Bundle bun = new Bundle();
		bun.putString(AlertDialogActivity.PARAMETER_TITLE, title);
		bun.putString(AlertDialogActivity.PARAMETER_MESSAGE, msg);
		bun.putString(AlertDialogActivity.PARAMETER_TYPE, type);
		bun.putInt(AlertDialogActivity.PARAMETER_RETRY_MODE, mode);
		bun.putSerializable(AlertDialogActivity.PARAMETER_ERROR_LIST, list);

		Intent popupIntent = new Intent(context, AlertDialogActivity.class);
		popupIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

		popupIntent.putExtras(bun);
		context.startActivity(popupIntent);
	}

	public static void ServiceDialogShow(Context context, int requestCode, String title, String msg, String type, int mode, ArrayList<ServiceRecord> list){
		Bundle bun = new Bundle();
		bun.putString(AlertDialogActivity.PARAMETER_TITLE, title);
		bun.putString(AlertDialogActivity.PARAMETER_MESSAGE, msg);
		bun.putString(AlertDialogActivity.PARAMETER_TYPE, type);
		bun.putInt(AlertDialogActivity.PARAMETER_RETRY_MODE, mode);
		bun.putSerializable(AlertDialogActivity.PARAMETER_ERROR_LIST, list);

		Intent popupIntent = new Intent(context, AlertDialogActivity.class);
		popupIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		popupIntent.putExtras(bun);
		PendingIntent pie= PendingIntent.getActivity(context, requestCode, popupIntent, PendingIntent.FLAG_ONE_SHOT);
		try {
			pie.send();
		} catch (PendingIntent.CanceledException e) {
			Log.e("broccoli", "alert_dialog_show() : " + e.toString());
		}
	}

	public static void showReceivedMoneyDialog(Context context, int requestCode, String msg) {
		Bundle bun = new Bundle();
		bun.putString(AlertDialogActivity.PARAMETER_MESSAGE, msg);

		Intent popupIntent = new Intent(context, RemitReceiveMoneyActivity.class);
		popupIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

		popupIntent.putExtras(bun);
		PendingIntent pie= PendingIntent.getActivity(context, requestCode, popupIntent, PendingIntent.FLAG_ONE_SHOT);
		try {
			pie.send();
		} catch (PendingIntent.CanceledException e) {
			Log.e("broccoli", "showReceivedMoneyDialog_show() : " + e.toString());
		}
	}

	public static void showGcmDialog(Context context, int requestCode, String msg) {
		Bundle bun = new Bundle();
		bun.putString(CloudMessageActivity.PARAMETER_MESSAGE, msg);

		Intent popupIntent = new Intent(context, CloudMessageActivity.class);
		popupIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

		popupIntent.putExtras(bun);
		PendingIntent pie= PendingIntent.getActivity(context, requestCode, popupIntent, PendingIntent.FLAG_ONE_SHOT);
		try {
			pie.send();
		} catch (PendingIntent.CanceledException e) {
			Log.e("broccoli", "showReceivedMoneyDialog_show() : " + e.toString());
		}
	}


	/** Remit Edit Two Button **/
	public static DialogBaseRemitEditButton showDlgRemitEditTwoButton(Activity _activity, DialogBaseRemitEditButton.DialogInputTypeListener _popupBtnClickListener, InfoDataDialog _infoDataDialog){
		DialogBaseRemitEditButton mPopup = new DialogBaseRemitEditButton(_activity, _popupBtnClickListener, _infoDataDialog);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
		return mPopup;
	}


	/** Remit Dlg Radio Group Bank **/
	public static void showDlgBaseRemitList(Activity _activity, AdapterView.OnItemClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, int _selectValue, ArrayList<String> _remitBankNameList){
		DialogBaseRemitList mPopup = new DialogBaseRemitList(_activity, _popupBtnClickListener, _infoDataDialog, _selectValue, _remitBankNameList);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
	}

	/** Remit Two Button **/
	public static DialogBaseRemitTwoButton showDlgBaseRemitTwoButton(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog){
		DialogBaseRemitTwoButton mPopup = new DialogBaseRemitTwoButton(_activity, _popupBtnClickListener, _infoDataDialog);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
		return mPopup;
	}

	/** Remit Send Phone **/
	public static DialogBaseRemitSendPhone showDlgBaseRemitSendPhone(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, RequestDataSendPhone _requestDataSendPhone){
		DialogBaseRemitSendPhone mPopup = new DialogBaseRemitSendPhone(_activity, _popupBtnClickListener, _infoDataDialog, _requestDataSendPhone);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
		return mPopup;
	}

	/** Remit Send Account **/
	public static DialogBaseRemitSendAccount showDlgBaseRemitSendAccount(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, RequestDataSendAccount _requestDataSendAccount){
		DialogBaseRemitSendAccount mPopup = new DialogBaseRemitSendAccount(_activity, _popupBtnClickListener, _infoDataDialog, _requestDataSendAccount);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
		return mPopup;
	}

	/** Remit Send Account **/
	public static DialogBaseRemitChargeMoney showDlgBaseRemitChargeMoney(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog, ResponseRemitChargeMoney _responseRemitChargeMoney){
		DialogBaseRemitChargeMoney mPopup = new DialogBaseRemitChargeMoney(_activity, _popupBtnClickListener, _infoDataDialog, _responseRemitChargeMoney);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
		return mPopup;
	}

	/** Main Notice Two Button **/
	public static DialogNoticeTwoButton showDlgMainNotieceTwoButton(Activity _activity, OnClickListener _popupBtnClickListener, InfoDataDialog _infoDataDialog){
		DialogNoticeTwoButton mPopup = new DialogNoticeTwoButton(_activity, _popupBtnClickListener, _infoDataDialog);
		mPopup.setCanceledOnTouchOutside(false);
//		mPopup.setCancelable(false);
		mPopup.show();
		return mPopup;
	}


	/** Tutorial Image **/
	public static DialogTutorialNoticeImage showDlgTutorialImage(Activity _activity, OnClickListener _popupBtnClickListener){
		DialogTutorialNoticeImage mPopup = new DialogTutorialNoticeImage(_activity, _popupBtnClickListener);
		mPopup.setCanceledOnTouchOutside(true);
//		mPopup.setCancelable(false);
		mPopup.show();
		return mPopup;
	}

	public static void showDialogDemoFinish(final Activity activity) {
		CommonDialogTextType dialogTextType = new CommonDialogTextType(activity, new int[]{R.string.demo_finish_dialog_cancel, R.string.demo_finish_dialog_ok});
		dialogTextType.addTextView(R.string.demo_finish_dialog, R.color.common_maintext_color, 13);
		dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				switch (id) {
					case R.string.demo_finish_dialog_ok:
						Intent intent = new Intent(activity, LoginActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
						activity.startActivity(intent);
						activity.finish();
						break;
				}
				dialog.dismiss();
			}
		});
		dialogTextType.show();
	}

	public static BaseVariableDialog showDialogCardAnnualFee(Activity activity, List<SpendCardDetailAnnualFee> list) {
		CommonDialogTextType commonDialogTextType = new CommonDialogTextType(activity, R.string.spend_card_annual_fee_detail_title, new int[] {R.string.common_confirm});
		if (list != null) {
			ArrayList<SpendCardDetailAnnualFee> sortList = new ArrayList<>();
			sortList.addAll(list);
			Collections.sort(sortList, new Comparator<SpendCardDetailAnnualFee>() {
				@Override
				public int compare(SpendCardDetailAnnualFee lhs, SpendCardDetailAnnualFee rhs) {
					int result = 0;
					if (lhs.type < rhs.type) {
						result = -1;
					} else if (lhs.type > rhs.type) {
						result = 1;
					} else {
						result = 0;
					}
					return result;
				}
			});
			int headerCount = 0;
			for (int i=0; i < sortList.size(); i++) {
				SpendCardDetailAnnualFee annualFee = sortList.get(i);
				if (headerCount != annualFee.type) {
					switch (annualFee.type) {
						case 1:
							commonDialogTextType.addAdjustMarginTextView(R.string.spend_card_annual_fee_detail_type_internal,
									R.color.common_maintext_color, 15, Typeface.BOLD, 0, Gravity.LEFT);
							break;
						case 2:
							commonDialogTextType.addAdjustMarginTextView(R.string.spend_card_annual_fee_detail_type_foreign,
									R.color.common_maintext_color, 15, Typeface.BOLD, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, activity.getResources().getDisplayMetrics()), Gravity.LEFT);
							break;
						case 3:
							commonDialogTextType.addAdjustMarginTextView(R.string.spend_card_annual_fee_detail_type_mobile,
									R.color.common_maintext_color, 15, Typeface.BOLD, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, activity.getResources().getDisplayMetrics()), Gravity.LEFT);
							break;
					}
					headerCount = annualFee.type;
				}
				commonDialogTextType.addAdjustMarginTextView(annualFee.name+" "+ElseUtils.getDecimalFormat(annualFee.annualFee)+"원", R.color.common_subtext_color, 15, 0, 0, Gravity.LEFT);
			}
		}
		return commonDialogTextType;
	}

	public static BaseVariableDialog showDialogCardSelectList(final Activity activity, final List<ResponseCardList> list, String selectedCode, final ExpandableListView.OnChildClickListener listener) {
		final CommonDialogCustomType customType = new CommonDialogCustomType(activity, R.string.spend_card_select, null);
		View title = activity.getLayoutInflater().inflate(R.layout.navi_img_txt, null);
		((TextView)title.findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.spend_card_select);
		title.findViewById(R.id.rl_navi_img_bg).setBackgroundColor(activity.getResources().getColor(R.color.main_2_layout_bg));
		title.findViewById(R.id.ll_navi_img_txt_back).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				customType.dismiss();
			}
		});
		customType.addCustomTitle(title);
		customType.setBodyMargin(0, 0, 0, 0);
		customType.setFullScreenMode(true);
		customType.setButtonLayerVisibility(View.GONE);
		final View body = activity.getLayoutInflater().inflate(R.layout.custom_dialog_card_select, null);
		final CustomExpandableListView listView = (CustomExpandableListView) body.findViewById(R.id.lv_custom_dialog_list_result);
		LinearLayout headerView = (LinearLayout) activity.getLayoutInflater().inflate(R.layout.row_grid_item_card_header, null);
		headerView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listener.onChildClick(listView, v, 0, 0, -1);
				customType.dismiss();
			}
		});
		final RelativeLayout topView = (RelativeLayout) headerView.findViewById(R.id.ll_row_grid_item_card_layout);
		listView.addHeaderView(headerView);
		final AdtExpListViewSpendCardSelect adapter = new AdtExpListViewSpendCardSelect(activity);
		adapter.setData((ArrayList<ResponseCardList>) list, selectedCode);
		WrapperExpandableListAdapter wrapAdapter = new WrapperExpandableListAdapter(adapter);
		listView.setAdapter(wrapAdapter);
		listView.setGroupIndicator(null);
		listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				listener.onChildClick(parent, v, groupPosition, childPosition, id);
				customType.dismiss();
				return false;
			}
		});
		final EditText input = (EditText) body.findViewById(R.id.et_custom_dialog_input);
		body.findViewById(R.id.iv_custom_dialog_search).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
			}
		});
		input.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() > 0) {
					topView.setVisibility(View.GONE);
				} else {
					topView.setVisibility(View.VISIBLE);
				}
				adapter.setFilter(s.toString());
			}
		});
		input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				switch (actionId) {
					case EditorInfo.IME_ACTION_DONE:
						InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
						break;
				}
				return false;
			}
		});
		customType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int idOrWhich) {
				dialog.dismiss();
			}
		});
		listView.setSelectionAfterHeaderView();
		final SideSelector sideIndex = (SideSelector) body.findViewById(R.id.custom_dialog_index);
		sideIndex.setListView(listView, adapter);
		sideIndex.setVisibility(View.INVISIBLE);
		sideIndex.setOnSideSelectorListener(new SideSelector.OnSideSelectorListener() {
			@Override
			public void onPressed() {
				ViewPropertyAnimator animator = sideIndex.animate();
				if (animator != null) {
					animator.cancel();
					sideIndex.setAlpha(1.0f);
					sideIndex.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onMoved() {
				ViewPropertyAnimator animator = sideIndex.animate();
				if (animator != null) {
					animator.cancel();
				}
			}

			@Override
			public void onRelease() {
				ViewPropertyAnimator animator = sideIndex.animate();
				if (animator != null) {
					animator.cancel();
					sideIndex.animate().alpha(0.0f).setDuration(1000).setListener(disappearAnimation).start();
				}
			}

			private Animator.AnimatorListener disappearAnimation = new Animator.AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) {

				}

				@Override
				public void onAnimationEnd(Animator animation) {
					sideIndex.setVisibility(View.GONE);
				}

				@Override
				public void onAnimationCancel(Animator animation) {
					sideIndex.setAlpha(1.0f);
				}

				@Override
				public void onAnimationRepeat(Animator animation) {

				}
			};
		});
		listView.setOnScrollListener(new AbsListView.OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				switch (scrollState) {
					case SCROLL_STATE_IDLE:
						if (!sideIndex.isDragging()) {
							sideIndex.animate().alpha(0.0f).setDuration(1000).setListener(disappearAnimation).start();
						}
						break;
					case SCROLL_STATE_TOUCH_SCROLL:
						InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
					default:
						sideIndex.animate().alpha(1.0f).setDuration(500).setListener(appearAnimation).start();
						break;
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

			}

			private Animator.AnimatorListener disappearAnimation = new Animator.AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) {

				}

				@Override
				public void onAnimationEnd(Animator animation) {
					sideIndex.setVisibility(View.GONE);
				}

				@Override
				public void onAnimationCancel(Animator animation) {
					sideIndex.setAlpha(1.0f);
				}

				@Override
				public void onAnimationRepeat(Animator animation) {

				}
			};

			private Animator.AnimatorListener appearAnimation = new Animator.AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) {

				}

				@Override
				public void onAnimationEnd(Animator animation) {
					sideIndex.setVisibility(View.VISIBLE);
				}

				@Override
				public void onAnimationCancel(Animator animation) {
					sideIndex.setAlpha(1.0f);
				}

				@Override
				public void onAnimationRepeat(Animator animation) {

				}
			};
		});
		customType.addCustomBodyTypeList(body);
		listView.post(new Runnable() {
			@Override
			public void run() {
				ArrayList<String> headerList = adapter.getHeaderList();
				for (int i = 0; i < headerList.size(); i++) {
					listView.expandGroup(i);
				}
				adapter.showSelected(listView);
			}
		});
		return customType;
	}

}
