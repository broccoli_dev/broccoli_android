package com.nomadconnection.broccoli.utils;

import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;

/**
 * Created by YelloHyunminJang on 2016. 12. 15..
 */

public class BroccoliLoginFilter implements InputFilter {

    public enum InputResult {
        ALLOWED,
        NEED_INCLUDE_NUMBER,
        NOT_ALLOWED_CHARACTER,
        NOT_ENOUGH_LENGTH,
        TOO_LONG_LENGTH,
        NEED_INCLUDE_CHAR
    }

    public interface OnInputListener {
        void isAllowed(InputResult allowed);
    }

    private static final String mAllowed = "!@#$%^&*()";
    private boolean mAppendInvalid = true;
    private OnInputListener mListener;

    public BroccoliLoginFilter(OnInputListener listener) {
        mListener = listener;
    }

    public boolean isAllowed(char c) {
        if ('0' <= c && c <= '9')
            return true;
        if ('a' <= c && c <= 'z')
            return true;
        if ('A' <= c && c <= 'Z')
            return true;
        if (mAllowed.indexOf(c) != -1)
            return true;
        return false;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

        // Scan through changed characters rejecting disallowed chars
        SpannableStringBuilder modification = null;
        int modoff = 0;

        String expected = new String();
        expected += dest.subSequence(0, dstart);
        expected += source.subSequence(start, end);
        expected += dest.subSequence(dend, dest.length());


        boolean isAllowed = true;
        boolean isContainChar = false;
        boolean isContainNumber = false;
        boolean isNotAllowedChar = false;

        for (int i=0; i < expected.length(); i++) {
            char c = expected.charAt(i);
            if ('0' <= c && c <= '9') {
                isContainNumber = true;
            }
            if ('a' <= c && c <= 'z') {
                isContainChar = true;
            }
            if ('A' <= c && c <= 'Z') {
                isContainChar = true;
            }
            if (!isAllowed(c)) {
                isAllowed = false;
                isNotAllowedChar = true;
            }
        }

        for (int i = start; i < end; i++) {
            char c = source.charAt(i);
            if ('0' <= c && c <= '9') {
                isContainNumber = true;
            }
            if (isAllowed(c)) {
                // Character allowed.
                modoff++;
            } else {
                isAllowed = false;
                isNotAllowedChar = true;
                if (mAppendInvalid) {
                    modoff++;
                } else {
                    if (modification == null) {
                        modification = new SpannableStringBuilder(source, start, end);
                        modoff = i - start;
                    }

                    modification.delete(modoff, modoff + 1);
                }
            }
        }

        if (mListener != null) {
            if (isAllowed && isContainNumber && isContainChar) {
                if (expected.length() < 8) {
                    mListener.isAllowed(InputResult.NOT_ENOUGH_LENGTH);
                } else if (expected.length() > 20) {
                    mListener.isAllowed(InputResult.TOO_LONG_LENGTH);
                } else {
                    mListener.isAllowed(InputResult.ALLOWED);
                }
            } else if (isNotAllowedChar) {
                mListener.isAllowed(InputResult.NOT_ALLOWED_CHARACTER);
            } else if (!isContainNumber) {
                mListener.isAllowed(InputResult.NEED_INCLUDE_NUMBER);
            } else if (!isContainChar) {
                mListener.isAllowed(InputResult.NEED_INCLUDE_CHAR);
            }
        }

        return modification;
    }
}
