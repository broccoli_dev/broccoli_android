package com.nomadconnection.broccoli.utils;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;

import java.text.DecimalFormat;

public class CustomYAxisValueFormatter implements YAxisValueFormatter {

    private DecimalFormat mFormat;

    public CustomYAxisValueFormatter () {
        mFormat = new DecimalFormat("###,###");
    }


    @Override
    public String getFormattedValue(float value, YAxis yAxis) {
        return mFormat.format(value);
    }
}














