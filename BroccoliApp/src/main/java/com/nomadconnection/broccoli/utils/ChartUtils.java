package com.nomadconnection.broccoli.utils;

import android.graphics.Color;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.BubbleData;
import com.github.mikephil.charting.data.BubbleDataSet;
import com.github.mikephil.charting.data.BubbleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.nomadconnection.broccoli.config.APP;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ChartUtils {


	//==========================================================================//
	// Common
	//==========================================================================//

	public static CombinedChart.DrawOrder[] mDrawOrder = new CombinedChart.DrawOrder[]{
			CombinedChart.DrawOrder.BAR, CombinedChart.DrawOrder.BUBBLE, CombinedChart.DrawOrder.CANDLE, CombinedChart.DrawOrder.LINE, CombinedChart.DrawOrder.SCATTER
	};
	public static List<Long> main_asset_value = new ArrayList<>();
	private static String mWhereToUse ="";


	/** Fragment1 Line Chart Circle Colors **/
//	public static final int[] FRAGMENT_MAIN_1_LINE_CHART_CIRCLE_COLORS = {
//			Color.argb((int)(255*0), 255, 255, 255), Color.argb((int)(255*0), 255, 255, 255), Color.argb((int)(255*0), 255, 255, 255),
//			Color.argb((int)(255*0), 255, 255, 255), Color.argb((int)(255*0), 255, 255, 255), Color.argb((int)(255*1), 255, 255, 255)
//	};

	/** Fragment1 Line Chart Fill Colors **/
//	public static final int[] FRAGMENT_MAIN_1_LINE_CHART_FILL_COLORS = {
//			Color.argb((int)(255*1), 240, 238, 70), Color.argb((int)(255*1), 240, 238, 70), Color.argb((int)(255*1), 240, 238, 70),
//			Color.argb((int)(255*1), 240, 238, 70), Color.argb((int)(255*1), 240, 238, 70), Color.argb((int)(255*0), 240, 238, 70)
//	};

	/** Fragment1 Bar Chart Colors **/
//	public static final int[] FRAGMENT_MAIN_1_BAR_CHART_COLORS = {
//			Color.argb((int)(255*0.4), 255, 255, 255), Color.argb((int)(255*0.4), 255, 255, 255), Color.argb((int)(255*0.4), 255, 255, 255),
//			Color.argb((int)(255*0.4), 255, 255, 255), Color.argb((int)(255*0.4), 255, 255, 255), Color.argb((int)(255*0.8), 255, 255, 255)
//	};

	/** 최근 6개월 구하기 **/
	public static String[] getMonth(int _CurrentMonth, int _Range){
		String[] mResult = new String[_Range];
		SimpleDateFormat sdformat = new SimpleDateFormat("MM");

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();

		for (int i = 0; i <_Range ; i++) {
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -5 + i);
			mResult[i] = sdformat.format(calendar.getTime());
		}
		return mResult;
	}

	/** 해당월 마지막날 구하기 **/
	public static String[] DayOfMonthly(int lastDay){
		String[] mResult = new String[lastDay];
		Calendar calendar = Calendar.getInstance();

		int year = calendar.get(calendar.YEAR);
		int month = calendar.get(calendar.MONTH);
		calendar.set(year, month, 1);

		int DayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		for(int j = 0; j < DayOfMonth; j ++){
			mResult[j] = Integer.toString(j + 1);
		}
		return mResult;
	}


	//==========================================================================//
	// Line
	//==========================================================================//
	public static LineData generateLineData(ArrayList<Entry> _entries, String _WhereToUse) {
		mWhereToUse = _WhereToUse;
		LineData d = new LineData();
		LineDataSet set = new LineDataSet(_entries, "Line DataSet");

		if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_1)){
			set.setColor(Color.argb((int) (255 * 0.4), 255, 255, 255));
			set.setLineWidth(1.5f);
//			set.setCircleColors(FRAGMENT_MAIN_1_LINE_CHART_CIRCLE_COLORS);
			set.setCircleSize(5f);
			set.setCircleColor(Color.argb((int) (255 * 0), 255, 255, 255));
			set.setFillColor(Color.rgb(255, 255, 255));
			set.setCircleColorHole(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setDrawHighlightIndicators(false);
//			set.setFillColor(Color.rgb(240, 238, 70));
//			set.setDrawCubic(true);
			set.setDrawValues(false);
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_2)){
			set.setColor(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setLineWidth(1.5f);
			set.setCircleSize(5f);
			set.setCircleColor(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setFillColor(Color.rgb(255, 255, 255));
			set.setCircleColorHole(Color.argb((int) (255 * 1), 203, 124, 229));
			set.setDrawCircleHole(false);
			set.setDrawCircles(false);
			set.setDrawHighlightIndicators(false);
			set.setDrawValues(false);
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_3)){
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_4)){
		}
		else{
			// Base
			set.setColor(Color.rgb(255, 255, 255));
			set.setLineWidth(1.5f);
			set.setCircleColor(Color.rgb(240, 238, 70));
			set.setCircleSize(5f);
			set.setFillColor(Color.rgb(240, 238, 70));
			set.setDrawCubic(true);
			set.setDrawValues(true);
			set.setValueTextSize(10f);
			set.setValueTextColor(Color.rgb(240, 238, 70));

			set.setAxisDependency(YAxis.AxisDependency.LEFT);
		}

		d.addDataSet(set);
		return d;
	}


	public static LineData generateLineDataCircle(ArrayList<Entry> _entries, String _WhereToUse, int _Pos) {
		mWhereToUse = _WhereToUse;
		LineData d = new LineData();
		LineDataSet set = new LineDataSet(_entries, "Line DataSet");

		if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_1)){
			int[] mTempCirlcleColor = new int[_entries.size()];
			for (int i = 0; i < _entries.size(); i++) {
				if (_Pos == i) mTempCirlcleColor[i] = Color.argb((int)(255*1), 255, 255, 255);
				else mTempCirlcleColor[i] = Color.argb((int)(255*0), 255, 255, 255);
			}
			set.setColor(Color.argb((int) (255 * 0.4), 255, 255, 255));
			set.setLineWidth(1.5f);
			set.setCircleColors(mTempCirlcleColor);
			set.setCircleSize(5f);
			set.setFillColor(Color.rgb(255, 255, 255));
			set.setCircleColorHole(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setDrawHighlightIndicators(false);
//			set.setFillColor(Color.rgb(240, 238, 70));
//			set.setDrawCubic(true);
			set.setDrawValues(false);
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_2)){
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_3)){
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_4)){
		}
		else{
			// Base
			set.setColor(Color.rgb(255, 255, 255));
			set.setLineWidth(1.5f);
			set.setCircleColor(Color.rgb(240, 238, 70));
			set.setCircleSize(5f);
			set.setFillColor(Color.rgb(240, 238, 70));
			set.setDrawCubic(true);
			set.setDrawValues(true);
			set.setValueTextSize(10f);
			set.setValueTextColor(Color.rgb(240, 238, 70));

			set.setAxisDependency(YAxis.AxisDependency.LEFT);
		}

		d.addDataSet(set);

		return d;
	}

	public static LineData generateLineDataFill(ArrayList<String> _Xentries, ArrayList<Entry> _Yentries, String _WhereToUse) {
		mWhereToUse = _WhereToUse;
		LineData d = new LineData(_Xentries);
		LineDataSet set = new LineDataSet(_Yentries, "Line DataSet");

		if (mWhereToUse.equals(APP.ASSET_STOCK_HISTORY_CHART_USE)){
//			set.setAxisDependency(YAxis.AxisDependency.RIGHT);
			set.setDrawFilled(true);
			set.setColor(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setLineWidth(1.5f);
			set.setFillColor(Color.argb((int) (255 * 0.2), 255, 255, 255));
//			set.setCircleColorHole(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setDrawHighlightIndicators(true);
			set.setHighLightColor(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setDrawVerticalHighlightIndicator(true);
			set.setDrawHorizontalHighlightIndicator(false);
			set.setDrawCircles(false);
//			set.setDrawCubic(true);
			set.setDrawValues(false);
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_3)){
//			set.setAxisDependency(YAxis.AxisDependency.RIGHT);
			set.setDrawFilled(true);
			set.setColor(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setLineWidth(1.5f);
			set.setFillColor(Color.argb((int) (255 * 0.2), 255, 255, 255));
//			set.setCircleColorHole(Color.argb((int) (255 * 1), 255, 255, 255));
			set.setDrawHighlightIndicators(false);
			set.setDrawCircles(false);
//			set.setDrawCubic(true);
			set.setDrawValues(false);
		}
		else{
			// Base
			set.enableDashedLine(10f, 5f, 0f);
			set.enableDashedHighlightLine(10f, 5f, 0f);
			set.setColor(Color.BLACK);
			set.setCircleColor(Color.BLACK);
			set.setLineWidth(1f);
			set.setCircleSize(3f);
			set.setDrawCircleHole(false);
			set.setValueTextSize(9f);
			set.setFillAlpha(65);
			set.setFillColor(Color.BLACK);
		}

		d.addDataSet(set);

		return d;
	}



	//==========================================================================//
	// Bar
	//==========================================================================//
	public static BarData generateBarData(ArrayList<BarEntry> _entries, String _WhereToUse) {
		mWhereToUse = _WhereToUse;
		BarData d = new BarData();
		BarDataSet set = new BarDataSet(_entries, "Bar DataSet");
		ArrayList<BarDataSet> arrDataSet = new ArrayList<BarDataSet>();

		if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_1)){
			set.setColor(Color.argb((int)(255*0.4), 255, 255, 255));
//			set.setColors(FRAGMENT_MAIN_1_BAR_CHART_COLORS);
			set.setBarSpacePercent(35f);
//			set.setValueTextColor(Color.rgb(255, 255, 255));
//			set.setValueTextSize(11f);
//			set.setAxisDependency(YAxis.AxisDependency.LEFT);
			set.setHighLightColor(Color.argb((int) (255 * 0.8), 255, 255, 255));
			set.setDrawValues(false);
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_2)){
			List<Integer> colors = new ArrayList<Integer>();
			Calendar cal = java.util.Calendar.getInstance();
			int date = cal.get(cal.DATE) - 1;
			for(int h = 0; h < _entries.size(); h++){
				if(h > date){
					colors.add(Color.argb((int) (255 * 0.1), 0, 0, 0));
				}
				else if(h == date){
					colors.add(Color.argb((int) (255 * 0.6), 255, 255, 255));
				}
				else{
					colors.add(Color.argb((int) (255 * 0.2), 255, 255, 255));
				}
				set.setColors(colors);
				//set.setBarSpacePercent(8f);
				set.setDrawValues(false);
				//set.setAxisDependency(YAxis.AxisDependency.LEFT);
			}
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_3)){

		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_4)){

		}
		else{
			// Base
			set.setColor(Color.rgb(60, 220, 78));
			set.setValueTextColor(Color.rgb(60, 220, 78));
			set.setValueTextSize(10f);
			set.setAxisDependency(YAxis.AxisDependency.LEFT);
		}

		d.addDataSet(set);

		return d;
	}



//==========================================================================//
// Bubble
//==========================================================================//
	public static BubbleData generateBubbleData(ArrayList<BubbleEntry> _entries, String _WhereToUse, int _Pos) {
		mWhereToUse = _WhereToUse;
		BubbleData d = new BubbleData();
		BubbleDataSet set = new BubbleDataSet(_entries, "Bubble DataSet");

		if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_1)){
			int[] mTempCirlcleColor = new int[_entries.size()];
			for (int i = 0; i < _entries.size(); i++) {
				if (_Pos == i) mTempCirlcleColor[i] = Color.argb((int)(255*1), 0, 0, 0);
				else mTempCirlcleColor[i] = Color.argb((int)(255*0), 125, 147, 255);
			}
			set.setColors(mTempCirlcleColor);
//			set.setColor(Color.argb((int) (255 * 1), 125, 147, 255));
			set.setDrawValues(false);
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_2)){
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_3)){
		}
		else if (mWhereToUse.equals(APP.MAIN_LIST_CHART_USE_4)){
		}
		else{
			// Base
			set.setColors(ColorTemplate.COLORFUL_COLORS);
			set.setDrawValues(false);
		}

		d.addDataSet(set);

		return d;

	}



}
