package com.nomadconnection.broccoli.utils.hangulutil;

/*
 * Copyright 2014 Bang Jun-young
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * Created by YelloHyunminJang on 2017. 1. 19..
 */

public class KoreanTextMatch {

    /**
     * 검색이 실패했을 때 결과로 리턴되는 인스턴스.
     * <p>
     * 이 인스턴스의 success()는 항상 <code>false</code>다. index(),
     * length(), value() 등 다른 프로퍼티들의 값은 미정이다.
     */
    public static final KoreanTextMatch EMPTY = new KoreanTextMatch();

    private final KoreanTextMatcher mTextMatcher;
    private final String mText;
    private final String mValue;
    private final int mIndex;
    private final boolean isSuccess;

    private KoreanTextMatch() {
        mTextMatcher = null;
        mText = null;
        mValue = "";
        mIndex = 0;
        isSuccess = false;
    }

    KoreanTextMatch(KoreanTextMatcher matcher, String text, int startIndex, int length) {
        mTextMatcher = matcher;
        mText = text;
        mValue = text.substring(startIndex, startIndex + length);
        mIndex = startIndex;
        isSuccess = true;
    }

    /**
     * 매치가 성공했는지 여부를 조사한다.
     *
     * @return 성공했으면 <code>true</code>, 아니면 <code>false</code>.
     */
    public boolean success() {
        return isSuccess;
    }

    /**
     * 매치의 시작 위치를 구한다.
     *
     * @return 검색 대상 문자열 내 패턴의 시작 위치
     */
    public int index() {
        return mIndex;
    }

    /**
     * 매치의 길이를 구한다.
     *
     * @return 검색 대상 문자열 내 매치의 길이
     */
    public int length() {
        return mValue.length();
    }

    /**
     * 매치 문자열을 구한다.
     *
     * @return 검색 대상 문자열 내 실제 매치
     */
    public String value() {
        return mValue;
    }

    /**
     * 마지막 매치가 끝나는 위치의 뒷문자부터 시작해서 다음 매치를 찾는다.
     *
     * @return 검색 결과를 담은 {@link KoreanTextMatch} 인스턴스. success()
     *         가 <code>true</code>일 때만 유효하며, 검색이 실패하면
     *         {@link KoreanTextMatch#EMPTY}가 리턴된다.
     */
    public KoreanTextMatch nextMatch() {
        if (mText == null)
            return EMPTY;

        KoreanTextMatch match = mTextMatcher.match(mText, mIndex + mValue.length());
        return match;
    }

}
