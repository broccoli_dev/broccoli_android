package com.nomadconnection.broccoli.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class APPSharedPrefer {
	private SharedPreferences mSharedPreferences;
	private static APPSharedPrefer mInstance;


	public APPSharedPrefer(Context _context){
		super();
		mSharedPreferences  = _context.getSharedPreferences(this.getClass().getSimpleName(), Activity.MODE_PRIVATE);
	}
	
	public static APPSharedPrefer getInstance(Context context){
		if(mInstance == null){
			mInstance = new APPSharedPrefer(context);
		}
		return mInstance;
	}

	
//=========================================================================================//
// Set
//=========================================================================================//
	public void setPrefInt(String _strName, int _count) {
		Editor edit = mSharedPreferences.edit();
		edit.putInt(_strName, _count);
		edit.commit();
	}
	
	public void setPrefBool(String _strName, boolean _flag) {
		Editor edit = mSharedPreferences.edit();
		edit.putBoolean(_strName, _flag);
		edit.commit();
	}
	
	public void setPrefString(String strName, String strValue) {
		Editor edit = mSharedPreferences.edit();
		edit.putString(strName, strValue);
		edit.commit();
	}
	
	
//=========================================================================================//
// Get
//=========================================================================================//
	public int getPrefInt(String _strName) {
		int count = mSharedPreferences.getInt(_strName, 0);
		return count;
	}
	
	public boolean getPrefBool(String _strName , boolean _flag) {
		boolean bRet = mSharedPreferences.getBoolean(_strName, _flag);
		return bRet;
	}

	public String getPrefString(String strName) {
		String strRet = mSharedPreferences.getString(strName, "");
		return strRet;
	}
	
	
	
//=========================================================================================//
// Remove
//=========================================================================================//

	// 값(DISCOUNT_POINT Data) 삭제하기
	public void removeAllPreferences(){
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.clear();
		editor.commit();
	}
	
}
