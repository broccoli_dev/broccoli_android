package com.nomadconnection.broccoli.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;

public class CustomToast extends Toast {

	private TextView mContent = null;
	private static CustomToast mInstance = null;

	public static CustomToast getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new CustomToast(context);
		}
		return mInstance;
	}

	public CustomToast(Context context) {
		super(context);

		View layout = LayoutInflater.from(context).inflate(R.layout.custom_toast_layout, null, true);
		mContent = (TextView) layout.findViewById(R.id.tv_custom_toast_layout_msg);

		// this.setGravity(Gravity.BOTTOM, 0, 0);
		this.setView(layout);
		this.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
	}

	public void showShort(String txt, int bgResId) {
		if (bgResId > 0) {
			mContent.setBackgroundResource(bgResId);
		}
		this.setDuration(Toast.LENGTH_SHORT);
		ToastTextSize(12);
		mContent.setText(txt);
		this.show();
	}

	public void showLong(String txt, int bgResId) {
		if (bgResId > 0) {
			LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) mContent
					.getLayoutParams();

			mContent.setBackgroundResource(bgResId);
		}
		this.setDuration(Toast.LENGTH_LONG);
		mContent.setText(txt);
		ToastTextSize(12);
		this.show();
	}

	public void showShort(String txt) {
		this.setDuration(Toast.LENGTH_SHORT);
		mContent.setText(txt);
		this.show();
	}

	public void showLong(String txt) {
		this.setDuration(Toast.LENGTH_LONG);
		ToastTextSize(12);
		mContent.setText(txt);
		this.show();
	}

	/**
	 * 디바이스의 글자 크기를 최대로 했을 경우 Toast 글씨가 커짐 현상으로 인해 Toast의 글자 크기를 dp로 해줌
	 * */
	public void ToastTextSize(int txtsize) {
		mContent.setTextSize(txtsize);
	}
}
