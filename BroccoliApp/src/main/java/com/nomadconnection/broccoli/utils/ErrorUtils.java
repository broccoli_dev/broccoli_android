package com.nomadconnection.broccoli.utils;

import android.text.TextUtils;

import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.APIError;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.activity.base.BaseActivity;

import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 1. 5..
 */

public class ErrorUtils {

    public static ErrorMessage parseError(Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                ServiceGenerator.getRetrofit().responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error = null;
        try {
            if (response.errorBody() == null) {
                Result result = (Result) response.body();
                if (result == null) {
                    error = new APIError();
                } else {
                    error = new APIError();
                    error.setCode(result.getCode());
                    error.setDesc(result.getDesc());
                }
            } else {
                error = converter.convert(response.errorBody());
            }
        } catch (Exception e) {
            return ErrorMessage.ERROR_CODE_INTERNAL_ERROR;
        }

        String code = error.getCode();
        ErrorMessage pickMessage = null;
        ErrorMessage[] errorMessages = ErrorMessage.values();
        if (code == null || TextUtils.isEmpty(code)) {
            pickMessage = ErrorMessage.ERROR_CODE_INTERNAL_ERROR;
        } else {
            for (ErrorMessage message : errorMessages) {
                if (message.code().equalsIgnoreCase(code)) {
                    pickMessage = message;
                    break;
                }
            }
        }

        if (pickMessage == null) {
            pickMessage = ErrorMessage.ERROR_CODE_INTERNAL_ERROR;
        }

        return pickMessage;
    }

    public static ErrorMessage parseError(BaseActivity activity, Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                ServiceGenerator.getRetrofit().responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error = null;
        try {
            if (response.errorBody() == null) {
                Result result = (Result) response.body();
                if (result == null) {
                    error = new APIError();
                } else {
                    error = new APIError();
                    error.setCode(result.getCode());
                    error.setDesc(result.getDesc());
                }
            } else {
                error = converter.convert(response.errorBody());
            }
        } catch (Exception e) {
            return ErrorMessage.ERROR_CODE_INTERNAL_ERROR;
        }

        String code = error.getCode();
        ErrorMessage pickMessage = null;
        ErrorMessage[] errorMessages = ErrorMessage.values();
        if (code == null || TextUtils.isEmpty(code)) {
            pickMessage = ErrorMessage.ERROR_CODE_INTERNAL_ERROR;
        } else {
            if (ErrorMessage.ERROR_716_TOKEN_EXPIRE.code().equalsIgnoreCase(code)) {
                pickMessage = ErrorMessage.ERROR_716_TOKEN_EXPIRE;
            } else {
                for (ErrorMessage message : errorMessages) {
                    if (message.code().equalsIgnoreCase(code)) {
                        pickMessage = message;
                        break;
                    }
                }
            }
        }

        if (pickMessage == null) {
            pickMessage = ErrorMessage.ERROR_CODE_INTERNAL_ERROR;
        }

        if (activity != null) {
            switch (pickMessage) {
                case ERROR_713_NO_SUCH_USER:
                case ERROR_20004_NO_SUCH_USER:
                    activity.showLoginActivity();
                case ERROR_716_TOKEN_EXPIRE:
                case ERROR_10007_SESSION_NOT_FOUND:
                case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    activity.showTokenExpire();
                    break;
                default:
                    ElseUtils.network_error(activity);
                    break;
            }
        }

        return pickMessage;
    }

}
