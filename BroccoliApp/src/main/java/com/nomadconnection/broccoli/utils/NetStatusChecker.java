package com.nomadconnection.broccoli.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;

public class NetStatusChecker {
	
	public static final int NET_TYPE_NONE = 0x00;
	public static final int NET_TYPE_WIFI = 0x01;
	public static final int NET_TYPE_3G  = 0x02; //LTE도 포함 
	
	private static NetStatusChecker mInstance = null;
	
	public static NetStatusChecker getInstance() {
		if(mInstance == null){
			mInstance = new NetStatusChecker();
		}
		
		return mInstance;
	}
	
	private boolean getWifiState(Context context){
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		boolean isConn = ni.isConnected();
		return isConn;
	}
	
	private boolean get3GState(Context context){
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		boolean isConn = ni.isConnected();
		return isConn;
	}
	
	public int getNetType(Context context){
		int netType = NetStatusChecker.NET_TYPE_NONE;
		
		if(getWifiState(context)){
			netType = NetStatusChecker.NET_TYPE_WIFI;
		}else{
			netType = NetStatusChecker.NET_TYPE_3G;
		}
		
		return netType;
	}
	
	public boolean getNetworkConnectStatus(Context context){
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			boolean mobileNetwork = false;
			boolean wifiNetwork = false;

			NetworkInfo mobileInfo = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);                
			NetworkInfo wifiInfo = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		
			if (mobileInfo != null || wifiInfo != null) {			
				if(mobileInfo != null){
					mobileNetwork = mobileInfo.isAvailable(); 
				}
				if(wifiInfo != null){
					wifiNetwork = wifiInfo.isAvailable();
				}
				if(mobileNetwork || wifiNetwork){
					if((mobileInfo != null && mobileInfo.getState() == NetworkInfo.State.CONNECTED) || 
							(wifiInfo != null && wifiInfo.getState() == NetworkInfo.State.CONNECTED)) {//TelephonyManager.NETWORK_TYPE_LTE) {
						return true;
					}
					else
					{
						return false;						
					}					
				}			
			}		
		}
		return false;
	}

	public NetStatusChecker() {
		// TODO Auto-generated constructor stub
	}

	public static boolean isNetworkEnable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		try {
			NetworkInfo ni = cm.getActiveNetworkInfo();
			return ni.isAvailable() && ni.isConnected();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static Boolean isAirPlane(Context context) {
		Boolean isAirplaneMode;
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1){
			isAirplaneMode = Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
		}else{
			isAirplaneMode = Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
		}
		return isAirplaneMode;
	}
}
