package com.nomadconnection.broccoli.utils;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TransFormatUtils {


	public static DecimalFormat mDecimalFormat = new DecimalFormat("###,###");

	private static SimpleDateFormat mSDF_YMD_CHART_BASE = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
	private static SimpleDateFormat mSDF_YMD_CHART_WEAK = new SimpleDateFormat("MM.dd", Locale.KOREA);
	private static SimpleDateFormat mSDF_YMD_CHART_3MONTH = new SimpleDateFormat("MM.dd", Locale.KOREA);
	private static SimpleDateFormat mSDF_YMD_CHART_1YEAR = new SimpleDateFormat("MM.dd", Locale.KOREA);
	private static SimpleDateFormat mSDF_YMD_CHART_3YEAR = new SimpleDateFormat("yyyy.MM", Locale.KOREA);
	private static SimpleDateFormat mSDF_YMD = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
	private static SimpleDateFormat mSDF_YMDHM = new SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
	private static SimpleDateFormat mSDF_YMDHMS = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
	private static SimpleDateFormat mSDF_Y_M_D = new SimpleDateFormat("yyyy.MM.dd", Locale.KOREA);
	private static SimpleDateFormat mSDF_Y_M_D_H_M_aa = new SimpleDateFormat("yyyy.MM.dd aa hh:mm", Locale.KOREA);
	private static SimpleDateFormat mSDF_Y_M_D_H_M_aa_count_1 = new SimpleDateFormat("yyyy.MM.dd aa hh:mm", Locale.KOREA); //오늘
	private static SimpleDateFormat mSDF_Y_M_D_H_M_aa_count_2 = new SimpleDateFormat("yyyy.MM.dd aa hh:mm", Locale.KOREA); //어제



//==========================================================================//
// 금일날자 받기
//==========================================================================//
	/** 현재 날자 구하기 (형식별) **/
	public static String getDataFormatWhich(int _which) {
		String mResult = "";
		long now = System.currentTimeMillis();
		Date date = new Date(now);

		switch (_which) {
			case 0:
				mResult = new SimpleDateFormat("yyyyMMdd").format(date);
				break;

			case 1:
				mResult = new SimpleDateFormat("yyyy.MM.dd.").format(date);
				break;

			case 2:
				mResult = new SimpleDateFormat("yyyyMMddHHmm").format(date);
				break;

			case 3:
				mResult = new SimpleDateFormat("yyyyMM").format(date);
				break;

			default:
				break;
			}

		return mResult;
	}

	/** 최근 Range 만큼 개월수 구하기 **/
	public static String[] getRangeMonth(String _Now, int _Range){
		DateFormat sdFormat = new SimpleDateFormat("yyyyMMdd");
		Date nowdate = null;
		try {
			nowdate = sdFormat.parse(_Now);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String[] mResult = new String[_Range];
		if (nowdate == null) return mResult;

		Calendar calendar = Calendar.getInstance();
		for (int i = 0; i <_Range ; i++) {
			calendar.setTime(nowdate);
			calendar.add(Calendar.MONTH, -_Range + i + 1);
			mResult[i] = sdFormat.format(calendar.getTime());
		}
		return mResult;
	}

	/** 최근 Range 만큼 개월수 구하기 **/
	public static String getAfterDate(String _Base, String _Dvider, int _Range){
		String mResult = "";
		DateFormat sdFormat = new SimpleDateFormat("yyyyMMdd");
		Date mBasedate = null;
		try {
			mBasedate = sdFormat.parse(_Base);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (mBasedate == null) return mResult;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mBasedate);
		if (_Dvider.equalsIgnoreCase("y")){
			calendar.add(Calendar.YEAR, +_Range);
		}
		else if (_Dvider.equalsIgnoreCase("m")){
			calendar.add(Calendar.MONTH, +_Range);
		}
		else if (_Dvider.equalsIgnoreCase("d")){
			calendar.add(Calendar.DATE, +_Range);
		}

		mResult = sdFormat.format(calendar.getTime());
		return mResult;
	}

	/** 날자비교 (A,B)**/
	public static boolean getCompareDate(String _Base, String _Compare, int _Range){
		Calendar calBase = Calendar.getInstance ( );
		Calendar calCompare = Calendar.getInstance ( );
		Date mDateBase = null;
		Date mDateCompare = null;
		try {
			if (_Base.length() == 8){
				mDateBase = mSDF_YMD.parse(_Base);
				mDateCompare = mSDF_YMD.parse(_Compare);
			}
			else if (_Base.length() == 14){
				mDateBase = mSDF_YMDHMS.parse(_Base);
				mDateCompare = mSDF_YMDHMS.parse(_Compare);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (mDateBase == null || mDateCompare == null) return false;
		calBase.setTime(mDateBase);
		if (_Range == 30) calBase.add(Calendar.MONTH, -1);
		else calBase.add(Calendar.DATE, -_Range);
		calCompare.setTime(mDateCompare);

		if (calCompare.compareTo(calBase) < 0) return false;
		else return true;

	}

	/** 날자비교 (A,B)**/
	public static boolean getCompareDate(String _Base, String _Compare){
		Calendar calBase = Calendar.getInstance ( );
		Calendar calCompare = Calendar.getInstance ( );
		Date mDateBase = null;
		Date mDateCompare = null;
		try {
			mDateBase = mSDF_Y_M_D.parse(_Base);
			mDateCompare = mSDF_Y_M_D.parse(_Compare);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (mDateBase == null || mDateCompare == null) return false;
		calBase.setTime(mDateBase);
		calCompare.setTime(mDateCompare);

		if (calCompare.compareTo(calBase) < 0) return false;
		else return true;

	}


	/** 날자비교 후 기간 반환 (머니캘린더용) **/
	public static String getCompareDateMoneyCalendar(String _Base, String _Compare){
		String mResult = "";
		Calendar calBase = Calendar.getInstance ( );
		Calendar calCompare = Calendar.getInstance ( );
		Date mDateBase = null;
		Date mDateCompare = null;
		List<String> dayList = new ArrayList<String>();
		try {
			if (_Base.length() == 8){
				mDateBase = mSDF_YMD.parse(_Base);
				mDateCompare = mSDF_YMD.parse(_Compare);
			}
			else if (_Base.length() == 14){
				mDateBase = mSDF_YMDHMS.parse(_Base);
				mDateCompare = mSDF_YMDHMS.parse(_Compare);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (mDateBase == null || mDateCompare == null) return mResult;
		calBase.setTime(mDateBase);
		calCompare.setTime(mDateCompare);

		long mGap = ((calCompare.getTimeInMillis() - calBase.getTimeInMillis())/1000)/(60*60*24);

		if (mGap == 1){
			mResult = "내일";

		}
		else{
			dayList.add(mSDF_YMD.format(calBase.getTime()));
			while(calBase.get(Calendar.DAY_OF_WEEK) != 1){
				calBase.add(Calendar.DATE, +1);
				dayList.add(mSDF_YMD.format(calBase.getTime()));
			}

			mResult = "다음 주 이후";
			for (String str : dayList) {
				if (str.equals(_Compare)){
					mResult = "이번 주";
					break;
				}
			}
		}

		return mResult;

	}

	/** 날자비교 차이 (머니캘린더용) **/
	public static String getGapDateAtoB(String _Base, String _Compare){
		String mResult = "";
		Calendar calBase = Calendar.getInstance ( );
		Calendar calCompare = Calendar.getInstance ( );
		Date mDateBase = null;
		Date mDateCompare = null;
		List<String> dayList = new ArrayList<String>();
		try {
			if (_Base.length() == 8){
				mDateBase = mSDF_YMD.parse(_Base);
				mDateCompare = mSDF_YMD.parse(_Compare);
			}
			else if (_Base.length() == 14){
				mDateBase = mSDF_YMDHMS.parse(_Base);
				mDateCompare = mSDF_YMDHMS.parse(_Compare);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (mDateBase == null || mDateCompare == null) return mResult;
		calBase.setTime(mDateBase);
		calCompare.setTime(mDateCompare);

		long mGap = ((calCompare.getTimeInMillis() - calBase.getTimeInMillis())/1000)/(60*60*24);
		mResult = String.valueOf(mGap);

		return transZeroPadding2(mResult);
	}

	/** 다음달 마지막 날 구하기 (머니캘린더용) **/
	public static String getNextMonthDay(){
		String mResult = "";
		Calendar calBase = Calendar.getInstance ( );
		calBase.add(Calendar.MONTH, 1);
		calBase.set(calBase.get(Calendar.YEAR), calBase.get(Calendar.MONTH), calBase.getActualMaximum(Calendar.DAY_OF_MONTH));
		mResult = mSDF_Y_M_D.format(calBase.getTime());

		return mResult;
	}



//==========================================================================//
// 일반 숫자를 화폐단위 문자포맷으로 변경해서 돌려줌
//==========================================================================//
	/** 10000 -> 10,000 **/
	public static String getDecimalFormatRecvString(String price) {
		if (price == null || price.equalsIgnoreCase("null") || price.equalsIgnoreCase("")) return "0";
		String priceStr = mDecimalFormat.format(Long.valueOf(price));
		return priceStr;
	}

	public static String transNullToCost(String price) {
		if (price == null || price.equalsIgnoreCase("null") || price.equalsIgnoreCase("")) return "0";
		else return price;
	}

	/** 10000 -> 10,000 **/
	public static String getDecimalFormat(long price) {
		String priceStr = mDecimalFormat.format(price);
		return priceStr;
	}


//==========================================================================//
// Code <-> Name
//==========================================================================//
	/** Bank Code to Name **/
	public static String transBankCodeToName(String _Code) {
		return MainApplication.mAPPResources.getStringArray(R.array.bank_and_card_name)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.bank_and_card_code)).indexOf(_Code)];
//		return MainApplication.mAPPResources.getStringArray(R.array.bank_name)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.bank_code)).indexOf(_Code)];
	}

	/** Bank Name to Code **/
	public static String transBankNameToCode(String _Name) {
		return MainApplication.mAPPResources.getStringArray(R.array.bank_and_card_code)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.bank_and_card_name)).indexOf(_Name)];
//		return MainApplication.mAPPResources.getStringArray(R.array.bank_code)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.bank_name)).indexOf(_Name)];
	}

	/** Card Code to Name **/
	public static String transCardCodeToName(String _Code) {
		return MainApplication.mAPPResources.getStringArray(R.array.bank_and_card_name)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.bank_and_card_code)).indexOf(_Code)];
//		return MainApplication.mAPPResources.getStringArray(R.array.card_name)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.card_code)).indexOf(_Code)];
	}

	/** Card Name to Code **/
	public static String transCardNameToCode(String _Name) {
		return MainApplication.mAPPResources.getStringArray(R.array.bank_and_card_code)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.bank_and_card_name)).indexOf(_Name)];
//		return MainApplication.mAPPResources.getStringArray(R.array.card_code)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.card_name)).indexOf(_Name)];
	}

	/** Stock Code to Name **/
	public static String transStockCodeToName(String _Pos) {
		return MainApplication.mAPPResources.getStringArray(R.array.spinner_asset_stock_1)[Integer.valueOf(_Pos)-1];
	}

	/** Stock Name to Code **/
	public static String transStockNameToCode(String _Name) {
		return String.valueOf(Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.spinner_asset_stock_1)).indexOf(_Name)+1);
	}


	/** Estate Type Pos to Name **/
	public static String transEstateTypePosToName(String _Pos) {
		return MainApplication.mAPPResources.getStringArray(R.array.estate_type_name)[Integer.valueOf(_Pos)-1];
	}

	/** Estate Type Name to Pos **/
	public static String transEstateTypeNameToPos(String _Name) {
		return transZeroPadding2(String.valueOf(Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.estate_type_name)).indexOf(_Name) + 1));
	}

	/** Deal Type Pos to Name **/
	public static String transDealTypePosToName(String _Pos) {
		return MainApplication.mAPPResources.getStringArray(R.array.deal_type_name)[Integer.valueOf(_Pos)-1];
	}

	/** Deal Type Name to Pos **/
	public static String transDealTypeNameToPos(String _Name) {
		return transZeroPadding2(String.valueOf(Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.deal_type_name)).indexOf(_Name)+1));
	}

	/** Dialog radio alarm Pos to Name **/
	public static String transDialogRadioAlarmPosToName(String _Pos) {
		return MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_period_name)[Integer.valueOf(_Pos)];
	}

	/** Dialog radio alarm Name to Pos **/
	public static String transDialogRadioAlarmNameToPos(String _Name) {
		return String.valueOf(Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_period_name)).indexOf(_Name));
	}


	/** Dialog radio alarm Code to Name **/
	public static String transDialogRadioAlarmCodeToName(String _Code) {
		return MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_period_name)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_period_code)).indexOf(_Code)];
	}

	/** Dialog radio alarm Name to Code **/
	public static String transDialogRadioAlarmNameToCode(String _Name) {
		return MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_period_code)[Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_period_name)).indexOf(_Name)];
	}




	/** Dialog radio type Pos to Name **/
	public static String transDialogRadioTypePosToName(String _Pos) {
		return MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_type)[Integer.valueOf(_Pos)];
	}

	/** Dialog radio type Name to Pos **/
	public static String transDialogRadioTypeNameToPos(String _Name) {
		return String.valueOf(Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_type)).indexOf(_Name));
	}

	/** Dialog radio type Name to Code **/
	public static String transDialogRadioTypeNameToCode(String _Name) {
		return transZeroPadding2(String.valueOf(Arrays.asList(MainApplication.mAPPResources.getStringArray(R.array.dlg_radio_type)).indexOf(_Name) + 1));
	}

	/** 자리수 채우기  ex) 1 -> 02 **/
	public static String transZeroPadding2(String _Num) {
		return String.format("%02d", Integer.valueOf(_Num));
	}

	/**
	 * 소수점 자르기
	 * @param num
	 * @return
	 */
	public static String getPointCut(double num) {
		String ret = null;

		ret = String.format("%.2f", num);

		return ret;
	}

	/** Date 형식 .추가  ex) 20150101 -> 2015.01.01 **/
	public static String transAddDot(String _Date) {
		String mResult = "";
		try {
			mResult = mSDF_Y_M_D.format(mSDF_YMD.parse(_Date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return mResult;
	}

	/** Date 형식 .제거  ex) 2015.01.01 -> 20150101 **/
	public static String transRemoveDot(String _Date) {
		return _Date.replace(".", "");
	}

	/** Date 형식 ,제거  ex) 1,000,000 -> 1000000 **/
	public static String transRemoveComma(String _Num) {
		return _Num.replaceAll(",", "");
	}



	/** Date 형식 변형 ex) 20160201 -> 2016.02.01 **/
	public static String transDateForm(String _Date){
		Calendar calToday = Calendar.getInstance ( );
		calToday.setTime(new Date());

		Calendar calInday = Calendar.getInstance ( );

		String mResult = "";
		Date mDate;

		try {
			if (_Date.length() == 8){
				mDate = mSDF_YMD.parse(_Date);
				mResult = mSDF_Y_M_D.format(mSDF_YMD.parse(_Date));
			}
			else if (_Date.length() == 12){
				mDate = mSDF_YMDHM.parse(_Date);
				calInday.setTime(mDate);
				int count = 0;
				while ( !calInday.after ( calToday ) ) {
					count++;
					calInday.add(Calendar.DATE, 1);
					if (count > 2) break;
				}
				if (count == 0){
					mResult = mSDF_Y_M_D_H_M_aa_count_1.format(mSDF_YMDHM.parse(_Date));
				}
				else if (count == 1){
					mResult = mSDF_Y_M_D_H_M_aa_count_2.format(mSDF_YMDHM.parse(_Date));
				}
				else{
					mResult = mSDF_Y_M_D_H_M_aa.format(mSDF_YMDHM.parse(_Date));
				}
			}
			else if (_Date.length() == 14){
				mDate = mSDF_YMDHMS.parse(_Date);
				calInday.setTime(mDate);
				int count = 0;
				while ( !calInday.after ( calToday ) ) {
					count++;
					calInday.add(Calendar.DATE, 1);
					if (count > 2) break;
				}
				if (count == 0){
					//mResult = mSDF_Y_M_D_H_M_aa_count_1.format(mSDF_YMDHMS.parse(_Date));
					mResult = mSDF_Y_M_D_H_M_aa.format(mSDF_YMDHMS.parse(_Date));
				}
				else if (count == 1){
					//mResult = mSDF_Y_M_D_H_M_aa_count_2.format(mSDF_YMDHMS.parse(_Date));
					mResult = mSDF_Y_M_D_H_M_aa.format(mSDF_YMDHMS.parse(_Date));
				}
				else{
					mResult = mSDF_Y_M_D_H_M_aa.format(mSDF_YMDHMS.parse(_Date));
				}
			}
			else if (_Date.length() == 0){
				return "";
			}
			else return "Date Form Error";	// Date Form Not Correct

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return mResult;
	}


	/** Date 형식 변형 ex) 2016-02-01 -> 02.01 or 2016.02 **/
	public static String transDateFormChart(String _Date, String _Type){

		String mResult = "";
		try {
			if (_Type.equalsIgnoreCase("week")){
				mResult = mSDF_YMD_CHART_WEAK.format(mSDF_YMD_CHART_BASE.parse(_Date));
			}
			else if (_Type.equalsIgnoreCase("3month")){
				mResult = mSDF_YMD_CHART_3MONTH.format(mSDF_YMD_CHART_BASE.parse(_Date));
			}
			else if (_Type.equalsIgnoreCase("1year")){
				mResult = mSDF_YMD_CHART_1YEAR.format(mSDF_YMD_CHART_BASE.parse(_Date));
			}
			else if (_Type.equalsIgnoreCase("3year")){
				mResult = mSDF_YMD_CHART_3YEAR.format(mSDF_YMD_CHART_BASE.parse(_Date));
			}
			else return "Date Form Error";	// Date Form Not Correct

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return mResult;
	}


	/** Date after 비교 **/
	public static Boolean compareDate(String _Date1, String _Date2){
		String mResult = "";
		Calendar cal1 = Calendar.getInstance ( );
		Calendar cal2 = Calendar.getInstance ( );
		Date mDate1;
		Date mDate2;

		try {
			mDate1 = mSDF_YMD.parse(_Date1);
			mDate2 = mSDF_YMD.parse(_Date2);

			cal1.setTime(mDate1);
			cal2.setTime(mDate2);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return cal1.after(cal2);
	}

}
