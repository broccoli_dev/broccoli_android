package com.nomadconnection.broccoli.adapter.remit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Remit.RemitContent;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.Locale;

public class AdtExpListViewRemitSendPhone extends BaseExpandableListAdapter {

    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<RemitContent>> mChildList = null;
    private ArrayList<ArrayList<RemitContent>> mDataList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;

    public AdtExpListViewRemitSendPhone(Context c, ArrayList<String> groupList, ArrayList<ArrayList<RemitContent>> childList){
        super();
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_group_remit_send, parent, false);
            mViewHolder.mGroupTitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_remit_send_title);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }
        mViewHolder.mGroupTitle.setText(getGroup(groupPosition));
        return v;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public RemitContent getChild(int groupPosition, int childPosition){
//    	public String getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_remit_send, null);
            mViewHolder.mName = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_remit_send_name);
            mViewHolder.mNumber = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_remit_send_number);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        String tempNumber = getChild(groupPosition, childPosition).getTelNo().replaceAll("-", "");
        mViewHolder.mName.setText(getChild(groupPosition, childPosition).getHostName());
        mViewHolder.mNumber.setText(ElseUtils.makePhoneNumber(tempNumber));

        if (isLastChild) ((View)v.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)v.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return v;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}


    public void filterByName(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        ArrayList<RemitContent> temp = new ArrayList<RemitContent>();
        mChildList.clear();
        if (charText.length() == 0) {
            mChildList.addAll(mDataList);
        } else {
            String number = charText.replaceAll("-", "");
            for(int i = 0; i < mDataList.size(); i++){
                for (RemitContent contact : mDataList.get(i)) {

                    if (contact.getHostName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        temp.add(contact);
                    } else if (contact.getTelNo().replaceAll("-", "").contains(number)) {
                        temp.add(contact);
                    }
                }
                mChildList.add(temp);
            }

        }
        notifyDataSetChanged();
    }
 
    class ViewHolder{
    	/* group */
        public TextView mGroupTitle;

        /* child */
        public TextView mName;
        public TextView mNumber;
    }

    public void setData(ArrayList<String> groupList, ArrayList<ArrayList<RemitContent>> childList) {
        this.mGroupList = groupList;
        this.mChildList = childList;

        mDataList = new ArrayList<ArrayList<RemitContent>>();
        if(mChildList != null){
            mDataList.addAll(mChildList);
        }
        notifyDataSetChanged();
    }

//====================================================================//
// Sort
//====================================================================//
    String mSort = "all";
    public void sortData(String _sort){
        mSort = _sort;
    }
}
