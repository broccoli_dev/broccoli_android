package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Etc.BodyOrgInfo;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;


public class AdtGridViewDlgOrg extends BaseAdapter {

	private Activity mActivity;
	private ViewHolder mViewHolder;

	private ArrayList<BodyOrgInfo> mArrBodyOrgInfo;
//	private int mSelectValue;
	private String mSelectStr1;
	private String mSelectStr2;
	private boolean mBSelectStr;

	public AdtGridViewDlgOrg(Activity _Activity, ArrayList<BodyOrgInfo> _ArrBodyOrgInfo){
		mActivity = _Activity;
		mArrBodyOrgInfo = _ArrBodyOrgInfo;
	}

	@Override
	public int getCount() {
		return mArrBodyOrgInfo.size();
	}

	@Override
	public BodyOrgInfo getItem(int position) {
		return mArrBodyOrgInfo.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View mView = convertView;
		LayoutInflater inflator = mActivity.getLayoutInflater();
		
		if(mView == null){
			mViewHolder = new ViewHolder();
			mView = inflator.inflate(R.layout.row_grid_item_org, null);
			
			mViewHolder.mLayout = (LinearLayout)mView.findViewById(R.id.ll_row_grid_item_org_layout);
			mViewHolder.mLayoutDim = (LinearLayout)mView.findViewById(R.id.ll_row_grid_item_org_layout_dim);
			mViewHolder.mRadioButton = (RadioButton)mView.findViewById(R.id.rb_row_grid_item_org_icon);
			mViewHolder.mTitle = (TextView)mView.findViewById(R.id.tv_row_grid_item_org_info_title);
			mViewHolder.mTag = (TextView)mView.findViewById(R.id.tv_row_grid_item_org_info_tag);
			mViewHolder.mDate = (TextView)mView.findViewById(R.id.tv_row_grid_item_org_info_date);
			mViewHolder.mCost = (TextView)mView.findViewById(R.id.tv_row_grid_item_org_info_cost);

			mViewHolder.mRadioButton.setClickable(false);
			mView.setTag(mViewHolder);
		}
		else{
			mViewHolder = (ViewHolder)mView.getTag();
		}

		/* 값 입력 */
		if (mSelectStr1.equalsIgnoreCase("선택")){
			if (position == 0){
				mViewHolder.mRadioButton.setChecked(true);
			}
			else{
				mViewHolder.mRadioButton.setChecked(false);
			}
		}
		else{
			if (mSelectStr2.length() != 0){
				if(mArrBodyOrgInfo.get(position).mBodyOrgId.equals(mSelectStr2)){
					mViewHolder.mRadioButton.setChecked(true);
				}else{
					mViewHolder.mRadioButton.setChecked(false);
				}
			}
			else{
				if(mArrBodyOrgInfo.get(position).mBodyOrgOpponent.equals(mSelectStr1)){
					mViewHolder.mRadioButton.setChecked(true);
					mSelectStr2 = mArrBodyOrgInfo.get(position).mBodyOrgId;
				}
				else{
					mViewHolder.mRadioButton.setChecked(false);
				}
			}
		}

		mViewHolder.mTag.setVisibility(mArrBodyOrgInfo.get(position).mBodyOrgOpponent.equals("없음")?View.GONE:View.VISIBLE);
		mViewHolder.mDate.setVisibility(mArrBodyOrgInfo.get(position).mBodyOrgOpponent.equals("없음")?View.GONE:View.VISIBLE);
		mViewHolder.mCost.setVisibility(mArrBodyOrgInfo.get(position).mBodyOrgOpponent.equals("없음")?View.GONE:View.VISIBLE);

		//mViewHolder.mLayoutDim.setVisibility(mArrBodyOrgInfo.get(position).mBodyOrgAddedYn != null && mArrBodyOrgInfo.get(position).mBodyOrgAddedYn.equalsIgnoreCase("Y") ? View.VISIBLE : View.GONE);
		if(mArrBodyOrgInfo.get(position).mBodyOrgAddedYn != null && mArrBodyOrgInfo.get(position).mBodyOrgAddedYn.equalsIgnoreCase("Y")){
			mViewHolder.mDate.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_191_191_191));
			mViewHolder.mTag.setBackgroundResource(R.drawable.shape_tag_a_bg_5);
			if (Build.VERSION.SDK_INT < 23) {
				mViewHolder.mTitle.setTextAppearance(mActivity, R.style.CS_Text_15_191191191);
				mViewHolder.mTag.setTextAppearance(mActivity, R.style.CS_TagA5);
				mViewHolder.mCost.setTextAppearance(mActivity, R.style.CS_Text_15_191191191);
			} else {
				mViewHolder.mTitle.setTextAppearance(R.style.CS_Text_15_191191191);
				mViewHolder.mTag.setTextAppearance(R.style.CS_TagA5);
				mViewHolder.mCost.setTextAppearance(R.style.CS_Text_15_191191191);
			}
		}
		else {
			mViewHolder.mDate.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_89_89_89));
			mViewHolder.mTag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
			if (Build.VERSION.SDK_INT < 23) {
				mViewHolder.mTitle.setTextAppearance(mActivity, R.style.CS_Text_15_898989);
				mViewHolder.mTag.setTextAppearance(mActivity, R.style.CS_TagA1);
				mViewHolder.mCost.setTextAppearance(mActivity, R.style.CS_Text_15_898989);
			} else {
				mViewHolder.mTitle.setTextAppearance(R.style.CS_Text_15_898989);
				mViewHolder.mTag.setTextAppearance(R.style.CS_TagA1);
				mViewHolder.mCost.setTextAppearance(R.style.CS_Text_15_898989);
			}
		}

		mViewHolder.mTitle.setText(mArrBodyOrgInfo.get(position).mBodyOrgOpponent);
		mViewHolder.mTag.setText(mArrBodyOrgInfo.get(position).mBodyOrgMethod);
		mViewHolder.mDate.setText(TransFormatUtils.transDateForm(mArrBodyOrgInfo.get(position).mBodyOrgDate));
		mViewHolder.mCost.setText(ElseUtils.getDecimalFormat(mArrBodyOrgInfo.get(position).mBodyOrgSum));

		return mView;
	}

//	public void setSelectValue(int _selectValue){
//		mSelectValue = _selectValue;
//	}

	public void setSelectStr(String _selectStr1, String _selectStr2){
		mSelectStr1 = _selectStr1;
		mSelectStr2 = _selectStr2;
	}


//====================================================================//
// ViewHolder
//====================================================================//   
	    private class ViewHolder {
	    	private LinearLayout mLayout;
	    	private LinearLayout mLayoutDim;
	    	private RadioButton mRadioButton;
	    	private TextView  mTitle;
	    	private TextView  mTag;
	    	private TextView  mDate;
	    	private TextView  mCost;
	    }
	    

}
