package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.GuideAuthRegisterActivity;
import com.nomadconnection.broccoli.activity.GuideAuthTaxActivity;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import static com.nomadconnection.broccoli.data.Scrap.SelectBankData.CERT_FINISH;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class AdtListViewFinancialList extends BaseExpandableListAdapter {

    private Context mContext;
    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<SelectBankData>> mChildList = null;
    private LayoutInflater mInflater = null;
    private boolean isShowHeaderGuide = true;

    public AdtListViewFinancialList(Context context, ArrayList<String> group, ArrayList<ArrayList<SelectBankData>> child) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mGroupList = group;
        mChildList = child;
    }

    public void setData(ArrayList<String> group, ArrayList<ArrayList<SelectBankData>> child) {
        mGroupList = group;
        mChildList = child;
    }

    public void setVisibleHeaderGuide(boolean bool) {
        isShowHeaderGuide = bool;
    }

    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }

    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }

    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;
        GroupHolder groupHolder = null;
        if(v == null){
            groupHolder = new GroupHolder();
            v = mInflater.inflate(R.layout.row_expandable_listview_item_group_asset_stock, parent, false);
            groupHolder.mGroupTitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_asset_stock_title);
            v.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) v.getTag();
        }

        String title = getGroup(groupPosition);
        groupHolder.mGroupTitle.setText(title);

        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
        //return mChildList.get(groupPosition).size();
    }

    @Override
    public SelectBankData getChild(int groupPosition, int childPosition) {
        return mChildList.get(groupPosition).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){

        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            viewHolder.id = groupPosition;
            convertView = mInflater.inflate(R.layout.row_expandable_listview_item_child_financial, null);
            viewHolder.mGridView = (GridView) convertView.findViewById(R.id.gv_row_expandable_list_item_financial);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                viewHolder.mGridView.setNestedScrollingEnabled(true);
            }
            viewHolder.mAdapter = new AdtGridViewFinancialList(mContext, onItemClickListener);
            viewHolder.mAdapter.setData(mChildList.get(groupPosition));
            viewHolder.mGridView.setAdapter(viewHolder.mAdapter);
            viewHolder.mItemClickListener = new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    SelectBankData mTemp = null;
                    mTemp = (SelectBankData) adapterView.getAdapter().getItem(position);

                    if (mTemp == null) return;
                    int status = mTemp.getStatus();
                    switch (status) {
                        case SelectBankData.SELECTED:
                            mTemp.setStatus(SelectBankData.NONE);
                            break;
                        case SelectBankData.NONE:
                            mTemp.setStatus(SelectBankData.SELECTED);
                            break;
                        case CERT_FINISH:
                            mTemp.setStatus(SelectBankData.NONE);
                            mTemp.clear();
                            break;
                        case SelectBankData.LOGIN_FINISH:
                            mTemp.setStatus(SelectBankData.NONE);
                            mTemp.clear();
                            break;
                    }
                    ((BaseAdapter)adapterView.getAdapter()).notifyDataSetChanged();
                    notifyDataSetChanged();
                }
            };
            convertView.setTag(viewHolder);
            int totalHeight = 0;
            int count = (int) Math.ceil((float)viewHolder.mAdapter.getCount()/(float)3);
            int height = (int) ElseUtils.getDiptoPx(60);
            totalHeight = count * height;
            if (count >= 1) {
                totalHeight += height/2;
            }
            LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) viewHolder.mGridView.getLayoutParams();
            if (param == null) {
                param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            }
            param.height = totalHeight;
            viewHolder.mGridView.setLayoutParams(param);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
            if (viewHolder.id != groupPosition) {
                viewHolder.id = groupPosition;
                viewHolder.mAdapter = new AdtGridViewFinancialList(mContext, onItemClickListener);
                viewHolder.mAdapter.setData(mChildList.get(groupPosition));
                viewHolder.mGridView.setAdapter(viewHolder.mAdapter);
                convertView.setTag(viewHolder);
            }
            int totalHeight = 0;
            int count = (int) Math.ceil((float)viewHolder.mAdapter.getCount()/(float)3);
            int height = (int) ElseUtils.getDiptoPx(60);
            totalHeight = count * height;
            if (count >= 1) {
                totalHeight += height/2;
            }
            LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) viewHolder.mGridView.getLayoutParams();
            if (param == null) {
                param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            }
            param.height = totalHeight;
            viewHolder.mGridView.setLayoutParams(param);
        }

        viewHolder.mGridView.setOnItemClickListener(viewHolder.mItemClickListener);

        return convertView;
    }

    class GroupHolder {
        /* group */
        TextView mGroupTitle;
    }

    class ViewHolder {
        int id;
        GridView mGridView;
        AdtGridViewFinancialList mAdapter;
        GridView.OnItemClickListener mItemClickListener;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String type = (String) v.getTag();
            if (SelectBankData.TYPE_BANK.equalsIgnoreCase(type)) {

            } else {
                startGuideActivity(type);
            }
        }
    };

    private void startGuideActivity(String type) {
        if (SelectBankData.TYPE_CARD.equalsIgnoreCase(type)) {
            Intent intent = new Intent(mContext, GuideAuthRegisterActivity.class);
            mContext.startActivity(intent);
        } else {
            Intent intent = new Intent(mContext, GuideAuthTaxActivity.class);
            mContext.startActivity(intent);
        }
    }

    GridView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            SelectBankData mTemp = null;
            mTemp = (SelectBankData) adapterView.getAdapter().getItem(i);

            if (mTemp == null) return;
            int status = mTemp.getStatus();
            switch (status) {
                case SelectBankData.SELECTED:
                    mTemp.setStatus(SelectBankData.NONE);
                    break;
                case SelectBankData.NONE:
                    mTemp.setStatus(SelectBankData.SELECTED);
                    break;
                case CERT_FINISH:
                    mTemp.setStatus(SelectBankData.NONE);
                    mTemp.clear();
                    break;
                case SelectBankData.LOGIN_FINISH:
                    mTemp.setStatus(SelectBankData.NONE);
                    mTemp.clear();
                    break;
            }
            notifyDataSetChanged();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SelectBankData mTemp = null;
            mTemp = (SelectBankData) view.getTag();

            if (mTemp == null) return;
            int status = mTemp.getStatus();
            switch (status) {
                case SelectBankData.SELECTED:
                    mTemp.setStatus(SelectBankData.NONE);
                    break;
                case SelectBankData.NONE:
                    mTemp.setStatus(SelectBankData.SELECTED);
                    break;
                case CERT_FINISH:
                    mTemp.setStatus(SelectBankData.NONE);
                    mTemp.clear();
                    break;
                case SelectBankData.LOGIN_FINISH:
                    mTemp.setStatus(SelectBankData.NONE);
                    mTemp.clear();
                    break;
            }
            notifyDataSetChanged();
        }
    };

}
