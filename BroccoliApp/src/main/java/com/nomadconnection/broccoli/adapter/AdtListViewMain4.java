package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.InfoDataMain4;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

/*
 * Card Use : g, h
 */

public class AdtListViewMain4 extends BaseAdapter {
	
	private ArrayList<InfoDataMain4> 	mAlDataList;
	private Activity			  		mActivity;

	private static int 			mTotalPageNum 	= 0;
	private static int 			mCurrentPageIdx	= 0;
	
//==========================================================================//
// constructor	
//==========================================================================//
	public AdtListViewMain4(Activity _activity, ArrayList<InfoDataMain4> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public InfoDataMain4 getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_main_4, parent, false);
            init(listRow, convertView);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }

        
        /* Data 적용 */
        dataSet(listRow, position);
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// Init
//====================================================================//
    private void init(ViewHolder _listRow, View _convertView){
    	/* Card A */
//    	_listRow.mCardALayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_a);
//    	_listRow.mCardATitle = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_title);
//    	_listRow.mCardATitleCount = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_title_count);
//    	_listRow.mCardAIcon = (ImageView)_listRow.mCardALayout.findViewById(R.id.iv_row_item_inc_home_card_a_icon);
//    	_listRow.mCardACost = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_cost);
//    	_listRow.mCardASubInfo1 = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_sub_info_1);
//    	_listRow.mCardASubInfo2 = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_sub_info_2);
    	
    	/* Card B */
//    	_listRow.mCardBLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_b);
//    	_listRow.mCardBTitle = (TextView)_listRow.mCardBLayout.findViewById(R.id.tv_row_item_inc_home_card_b_title);
//    	_listRow.mCardBCost = (TextView)_listRow.mCardBLayout.findViewById(R.id.tv_row_item_inc_home_card_b_cost);
//    	_listRow.mCardBSubInfo1 = (TextView)_listRow.mCardBLayout.findViewById(R.id.tv_row_item_inc_home_card_b_sub_info_1);
//    	_listRow.mCardBSubInfo2 = (TextView)_listRow.mCardBLayout.findViewById(R.id.tv_row_item_inc_home_card_b_sub_info_2);
    	
    	/* Card C */
//    	_listRow.mCardCLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_c);
//    	_listRow.mCardCTitle = (TextView)_listRow.mCardCLayout.findViewById(R.id.tv_row_item_inc_home_card_c_title);
//    	_listRow.mCardCIcon = (ImageView)_listRow.mCardCLayout.findViewById(R.id.iv_row_item_inc_home_card_c_icon);
//    	_listRow.mCardCCost = (TextView)_listRow.mCardCLayout.findViewById(R.id.tv_row_item_inc_home_card_c_cost);
    	
    	/* Card D */
//    	_listRow.mCardDLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_d);
//    	_listRow.mCardDTitle = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_title);
//    	_listRow.mCardDIcon = (ImageView)_listRow.mCardDLayout.findViewById(R.id.iv_row_item_inc_home_card_d_icon);
//    	_listRow.mCardDSubInfo1Cost = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_1_title_cost);
//    	_listRow.mCardDSubInfo1Title = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_1_title);
//    	_listRow.mCardDSubInfo2Cost = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_2_title_cost);
//    	_listRow.mCardDSubInfo2Title = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_2_title);

		_listRow.mMain_4_Margin = (FrameLayout)_convertView.findViewById(R.id.fl_row_item_main_4_margin);
    	
    	
    	/* Card E */
    	_listRow.mCardELayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_e);
    	_listRow.mCardETitle = (TextView)_listRow.mCardELayout.findViewById(R.id.tv_row_item_inc_home_card_e_title);
    	_listRow.mCardEIcon = (ImageView)_listRow.mCardELayout.findViewById(R.id.iv_row_item_inc_home_card_e_icon);
    	
    	/* Card F */
//    	_listRow.mCardFLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_f);
//    	_listRow.mCardFTitle = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_title);
//    	_listRow.mCardFIcon = (ImageView)_listRow.mCardFLayout.findViewById(R.id.iv_row_item_inc_home_card_f_icon);
//    	_listRow.mCardFSubInfo1Title = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_1_txt_1);
//    	_listRow.mCardFSubInfo1Tag = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_1_txt_1_tag);
//    	_listRow.mCardFSubInfo1Txt = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_1_txt_2);
//    	_listRow.mCardFSubInfo1Cost = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_1_cost);
//    	_listRow.mCardFSubInfo2Title = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_2_txt_1);
//    	_listRow.mCardFSubInfo2Tag = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_2_txt_1_tag);
//    	_listRow.mCardFSubInfo2Txt = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_2_txt_2);
//    	_listRow.mCardFSubInfo2Cost = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_2_cost);
//    	_listRow.mCardFSubInfo3Title = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_3_txt_1);
//    	_listRow.mCardFSubInfo3Tag = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_3_txt_1_tag);
//    	_listRow.mCardFSubInfo3Txt = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_3_txt_2);
//    	_listRow.mCardFSubInfo3Cost = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_3_cost);
    	
    	
    	/* Card G */
    	_listRow.mCardGLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_g);
    	_listRow.mCardGTitle = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_title);
		_listRow.mCardGTitleCount = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_title_count);
    	_listRow.mCardGIcon = (ImageView)_listRow.mCardGLayout.findViewById(R.id.iv_row_item_inc_home_card_g_icon);
		_listRow.mCardGSubLayout1 = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_g_sub_layout_1);
    	_listRow.mCardGSubInfo1date1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_date_1);
    	_listRow.mCardGSubInfo1date2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_date_2);
    	_listRow.mCardGSubInfo1Txt1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_txt_1);
    	_listRow.mCardGSubInfo1Txt1Tag = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_txt_1_tag);
    	_listRow.mCardGSubInfo1Txt2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_txt_2);
    	_listRow.mCardGSubInfo1Txt3 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_txt_3);
    	_listRow.mCardGSubInfo1Cost = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_cost);
		_listRow.mCardGSubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardGSubLayout2 = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_g_sub_layout_2);
    	_listRow.mCardGSubInfo2date1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_date_1);
    	_listRow.mCardGSubInfo2date2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_date_2);
    	_listRow.mCardGSubInfo2Txt1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_txt_1);
    	_listRow.mCardGSubInfo2Txt1Tag = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_txt_1_tag);
    	_listRow.mCardGSubInfo2Txt2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_txt_2);
    	_listRow.mCardGSubInfo2Txt3 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_txt_3);
    	_listRow.mCardGSubInfo2Cost = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_cost);
		_listRow.mCardGSubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardGSubLayout3 = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_g_sub_layout_3);
    	_listRow.mCardGSubInfo3date1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_date_1);
    	_listRow.mCardGSubInfo3date2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_date_2);
    	_listRow.mCardGSubInfo3Txt1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_txt_1);
    	_listRow.mCardGSubInfo3Txt1Tag = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_txt_1_tag);
    	_listRow.mCardGSubInfo3Txt2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_txt_2);
    	_listRow.mCardGSubInfo3Txt3 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_txt_3);
    	_listRow.mCardGSubInfo3Cost = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_cost);
		_listRow.mCardGSubInfo3Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardGSubLayout4 = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_g_sub_layout_4);
    	_listRow.mCardGSubInfo4date1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_date_1);
    	_listRow.mCardGSubInfo4date2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_date_2);
    	_listRow.mCardGSubInfo4Txt1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_txt_1);
    	_listRow.mCardGSubInfo4Txt1Tag = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_txt_1_tag);
    	_listRow.mCardGSubInfo4Txt2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_txt_2);
    	_listRow.mCardGSubInfo4Txt3 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_txt_3);
    	_listRow.mCardGSubInfo4Cost = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_cost);
		_listRow.mCardGSubInfo4Cost.setTypeface(FontClass.setFont(mActivity));
    	
    	
    	/* Card H */
		_listRow.mCardHLayout = (RelativeLayout)_convertView.findViewById(R.id.inc_card_h);
		_listRow.mCardHPagerLayout = (ViewPager)_convertView.findViewById(R.id.row_viewpager_220);
		_listRow.mCardHPagerIndex = (LinearLayout)_convertView.findViewById(R.id.row_page_index_220);
//    	_listRow.mCardHLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_h);
//    	_listRow.mCardHTitle = (TextView)_listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_title);
//    	_listRow.mCardHTitleCount = (TextView)_listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_title_count);
//    	_listRow.mCardHIcon = (ImageView)_listRow.mCardHLayout.findViewById(R.id.iv_row_item_inc_home_card_h_icon);
//    	_listRow.mCardHSubInfoTitle = (TextView)_listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_sub_info_title);
//    	_listRow.mCardHSubInfoCost = (TextView)_listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_sub_info_cost);
//    	_listRow.mCardHPageIndex = (LinearLayout)_listRow.mCardHLayout.findViewById(R.id.ly_row_item_inc_home_card_h_page_index);
//
    	/* Card I */
//    	_listRow.mCardILayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_i);
//    	_listRow.mCardITitle = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_title);
//    	_listRow.mCardITitleCount = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_title_count);
//    	_listRow.mCardIIcon = (ImageView)_listRow.mCardILayout.findViewById(R.id.iv_row_item_inc_home_card_i_icon);
//    	_listRow.mCardISubInfo1Title = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_sub_info_1_title);
//    	_listRow.mCardISubInfo1Cost = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_sub_info_1_cost);
//    	_listRow.mCardISubInfo2Title = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_sub_info_2_title);
//    	_listRow.mCardISubInfo2Cost = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_sub_info_2_cost);
    	
    	/* Card J */
//    	_listRow.mCardJLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_j);
//    	_listRow.mCardJTitle = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_title);
//    	_listRow.mCardJTitleCount = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_title_count);
//    	_listRow.mCardJIcon = (ImageView)_listRow.mCardJLayout.findViewById(R.id.iv_row_item_inc_home_card_j_icon);
//    	_listRow.mCardJSubInfo1Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_1_title);
//    	_listRow.mCardJSubInfo1Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_1_cost);
//    	_listRow.mCardJSubInfo2Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_2_title);
//    	_listRow.mCardJSubInfo2Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_2_cost);
//    	_listRow.mCardJSubInfo3Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_3_title);
//    	_listRow.mCardJSubInfo3Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_3_cost);
    	
    	/* Card K */
//    	_listRow.mCardKLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_k);
//    	_listRow.mCardKTitle = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_title);
//    	_listRow.mCardKIcon = (ImageView)_listRow.mCardKLayout.findViewById(R.id.iv_row_item_inc_home_card_k_icon);
//    	_listRow.mCardKSubInfo1Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_1_title);
//    	_listRow.mCardKSubInfo1Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_1_cost);
//    	_listRow.mCardKSubInfo2Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_2_title);
//    	_listRow.mCardKSubInfo2Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_2_cost);
//    	_listRow.mCardKSubInfo3Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_3_title);
//    	_listRow.mCardKSubInfo3Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_3_cost);
    }
    
    
    
    
//====================================================================//
// Data Set
//====================================================================//
    private void dataSet(final ViewHolder _listRow, int _position){
    	/* Main 4 */
//		_listRow.mCardALayout.setVisibility(View.GONE);
//		_listRow.mCardBLayout.setVisibility(View.GONE);
//		_listRow.mCardCLayout.setVisibility(View.GONE);
//		_listRow.mCardDLayout.setVisibility(View.GONE);
//		_listRow.mCardELayout.setVisibility(View.GONE);
//		_listRow.mCardFLayout.setVisibility(View.GONE);
//		_listRow.mCardGLayout.setVisibility(View.VISIBLE);
//		_listRow.mCardHLayout.setVisibility(View.VISIBLE);
//		_listRow.mCardILayout.setVisibility(View.GONE);
//		_listRow.mCardJLayout.setVisibility(View.GONE);
//		_listRow.mCardKLayout.setVisibility(View.GONE);
		
		if (mAlDataList.get(_position).mTxt1.equals("챌린지")){
			if (mAlDataList.get(_position).mTxt2.equals("y")) {
				_listRow.mCardGLayout.setVisibility(View.GONE);
				_listRow.mMain_4_Margin.setVisibility(View.VISIBLE);
				_listRow.mCardHLayout.setVisibility(View.VISIBLE);
				_listRow.mCardELayout.setVisibility(View.GONE);
//			_listRow.mCardHTitle.setText(mAlDataList.get(_position).mTxt1);
//			_listRow.mCardHSubInfoTitle.setText(mAlDataList.get(_position).mTxt3);
//			_listRow.mCardHSubInfoCost.setText(mAlDataList.get(_position).mTxt4);
				ArrayList<EtcChallengeInfo> pager = new ArrayList<EtcChallengeInfo>();
				int size = mAlDataList.get(_position).mEtcMainData.mMainEtcChallengeInfo.size();
				for(int i = 0; i < size ; i++){
					pager.add(mAlDataList.get(_position).mEtcMainData.mMainEtcChallengeInfo.get(i));
				}
				ViewPager viewPager = _listRow.mCardHPagerLayout;
				ChallengeRowPagerAdapter mChallengeRowPagerAdapter = new ChallengeRowPagerAdapter(mActivity, pager);
				mTotalPageNum = mChallengeRowPagerAdapter.getCount();
				viewPager.setAdapter(mChallengeRowPagerAdapter);
				viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
					@Override
					public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
						mCurrentPageIdx = 0;
					}

					@Override
					public void onPageSelected(int position) {
						mCurrentPageIdx = position;
						initPageIndex(mActivity, _listRow);
					}

					@Override
					public void onPageScrollStateChanged(int state) {

					}
				});
				initPageIndex(mActivity, _listRow);
			}
			else{
				_listRow.mCardGLayout.setVisibility(View.GONE);
				_listRow.mCardHLayout.setVisibility(View.GONE);
				_listRow.mMain_4_Margin.setVisibility(View.VISIBLE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
			}
		}
		else if (mAlDataList.get(_position).mTxt1.equals("머니 캘린더")){
			if (mAlDataList.get(_position).mTxt2.equals("y")) {
				_listRow.mCardGLayout.setVisibility(View.VISIBLE);
				_listRow.mMain_4_Margin.setVisibility(View.GONE);
				_listRow.mCardHLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardGTitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardGTitleCount.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyTotalCount);
				_listRow.mCardGIcon.setBackgroundResource(R.drawable.home_icon_12);

				if (Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyTotalCount) == 1){
					_listRow.mCardGSubLayout1.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout2.setVisibility(View.GONE);
					_listRow.mCardGSubLayout3.setVisibility(View.GONE);
					_listRow.mCardGSubLayout4.setVisibility(View.GONE);

					String mTempGapDate1 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate);
					if (mTempGapDate1.equals("00")){
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo1date1.setText("오늘");
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					else if (mTempGapDate1.equals("01")){
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo1date1.setText("내일");
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					else{
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
						_listRow.mCardGSubInfo1date1.setText("D-"+mTempGapDate1);
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					_listRow.mCardGSubInfo1Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyTitle);
					if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayType.equalsIgnoreCase("01")) {
						_listRow.mCardGSubInfo1Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
						_listRow.mCardGSubInfo1Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
					}
					else {
						_listRow.mCardGSubInfo1Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
						_listRow.mCardGSubInfo1Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
					}
					_listRow.mCardGSubInfo1Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayType) - 1)));
					String mTempPayDate1 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate;
					_listRow.mCardGSubInfo1Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate1.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate1));
//					_listRow.mCardGSubInfo1Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate);
					_listRow.mCardGSubInfo1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyAmt));

					String expect_or_paid_0 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyExpectOrPaid;
					switch (expect_or_paid_0) {
						case "EY":
							_listRow.mCardGSubInfo1Txt3.setText("예상");
							break;
						case "EN":
							_listRow.mCardGSubInfo1Txt3.setText("확정");
							break;
						case "PY":
							_listRow.mCardGSubInfo1Txt3.setText("납부완료");
							break;
						case "PN":
							_listRow.mCardGSubInfo1Txt3.setText("미납");
							break;
					}
				}
				else if (Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyTotalCount) == 2){
					_listRow.mCardGSubLayout1.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout2.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout3.setVisibility(View.GONE);
					_listRow.mCardGSubLayout4.setVisibility(View.GONE);
					String mTempGapDate1 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate);
					if (mTempGapDate1.equals("00")){
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo1date1.setText("오늘");
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					else if (mTempGapDate1.equals("01")){
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo1date1.setText("내일");
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					else{
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
						_listRow.mCardGSubInfo1date1.setText("D-"+mTempGapDate1);
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					_listRow.mCardGSubInfo1Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyTitle);
					if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayType.equalsIgnoreCase("01")) {
						_listRow.mCardGSubInfo1Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
						_listRow.mCardGSubInfo1Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
					}
					else {
						_listRow.mCardGSubInfo1Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
						_listRow.mCardGSubInfo1Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
					}
					_listRow.mCardGSubInfo1Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayType) - 1)));
					String mTempPayDate1 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate;
					_listRow.mCardGSubInfo1Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate1.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate1));
//					_listRow.mCardGSubInfo1Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate);
					_listRow.mCardGSubInfo1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyAmt));

					String expect_or_paid_0 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyExpectOrPaid;
					switch (expect_or_paid_0) {
						case "EY":
							_listRow.mCardGSubInfo1Txt3.setText("예상");
							break;
						case "EN":
							_listRow.mCardGSubInfo1Txt3.setText("확정");
							break;
						case "PY":
							_listRow.mCardGSubInfo1Txt3.setText("납부완료");
							break;
						case "PN":
							_listRow.mCardGSubInfo1Txt3.setText("미납");
							break;
					}

					String mTempGapDate2 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate);
					if (mTempGapDate2.equals("00")){
						_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo2date1.setText("오늘");
						_listRow.mCardGSubInfo2date2.setText("");
						_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
					}
					else if (mTempGapDate2.equals("01")){
						_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo2date1.setText("내일");
						_listRow.mCardGSubInfo2date2.setText("");
						_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
					}
					else{
						_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
						_listRow.mCardGSubInfo2date1.setText("D-"+mTempGapDate2);
						_listRow.mCardGSubInfo2date2.setText("");
						_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
					}
					_listRow.mCardGSubInfo2Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyTitle);
					if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayType.equalsIgnoreCase("01")) {
						_listRow.mCardGSubInfo2Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
						_listRow.mCardGSubInfo2Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
					}
					else {
						_listRow.mCardGSubInfo2Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
						_listRow.mCardGSubInfo2Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
					}
					_listRow.mCardGSubInfo2Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayType) - 1)));
					String mTempPayDate2 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate;
					_listRow.mCardGSubInfo2Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate2.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate2));
//					_listRow.mCardGSubInfo2Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate);
					_listRow.mCardGSubInfo2Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyAmt));

					String expect_or_paid_1 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyExpectOrPaid;
					switch (expect_or_paid_1) {
						case "EY":
							_listRow.mCardGSubInfo2Txt3.setText("예상");
							break;
						case "EN":
							_listRow.mCardGSubInfo2Txt3.setText("확정");
							break;
						case "PY":
							_listRow.mCardGSubInfo2Txt3.setText("납부완료");
							break;
						case "PN":
							_listRow.mCardGSubInfo2Txt3.setText("미납");
							break;
					}
				}
				else if (Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyTotalCount) == 3){
					_listRow.mCardGSubLayout1.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout2.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout3.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout4.setVisibility(View.GONE);
					String mTempGapDate1 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate);
					if (mTempGapDate1.equals("00")){
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo1date1.setText("오늘");
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					else if (mTempGapDate1.equals("01")){
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo1date1.setText("내일");
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					else{
						_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
						_listRow.mCardGSubInfo1date1.setText("D-"+mTempGapDate1);
						_listRow.mCardGSubInfo1date2.setText("");
						_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
					}
					_listRow.mCardGSubInfo1Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyTitle);
					if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayType.equalsIgnoreCase("01")) {
						_listRow.mCardGSubInfo1Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
						_listRow.mCardGSubInfo1Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
					}
					else {
						_listRow.mCardGSubInfo1Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
						_listRow.mCardGSubInfo1Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
					}
					_listRow.mCardGSubInfo1Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayType) - 1)));
					String mTempPayDate1 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate;
					_listRow.mCardGSubInfo1Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate1.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate1));
//					_listRow.mCardGSubInfo1Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate);
					_listRow.mCardGSubInfo1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyAmt));

					String expect_or_paid_0 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyExpectOrPaid;
					switch (expect_or_paid_0) {
						case "EY":
							_listRow.mCardGSubInfo1Txt3.setText("예상");
							break;
						case "EN":
							_listRow.mCardGSubInfo1Txt3.setText("확정");
							break;
						case "PY":
							_listRow.mCardGSubInfo1Txt3.setText("납부완료");
							break;
						case "PN":
							_listRow.mCardGSubInfo1Txt3.setText("미납");
							break;
					}

					String mTempGapDate2 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate);
					if (mTempGapDate2.equals("00")){
						_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo2date1.setText("오늘");
						_listRow.mCardGSubInfo2date2.setText("");
						_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
					}
					else if (mTempGapDate2.equals("01")){
						_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo2date1.setText("내일");
						_listRow.mCardGSubInfo2date2.setText("");
						_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
					}
					else{
						_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
						_listRow.mCardGSubInfo2date1.setText("D-"+mTempGapDate2);
						_listRow.mCardGSubInfo2date2.setText("");
						_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
					}
					_listRow.mCardGSubInfo2Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyTitle);
					if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayType.equalsIgnoreCase("01")) {
						_listRow.mCardGSubInfo2Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
						_listRow.mCardGSubInfo2Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
					}
					else {
						_listRow.mCardGSubInfo2Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
						_listRow.mCardGSubInfo2Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
					}
					_listRow.mCardGSubInfo2Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayType) - 1)));
					String mTempPayDate2 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate;
					_listRow.mCardGSubInfo2Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate2.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate2));
//					_listRow.mCardGSubInfo2Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate);
					_listRow.mCardGSubInfo2Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyAmt));

					String expect_or_paid_1 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyExpectOrPaid;
					switch (expect_or_paid_1) {
						case "EY":
							_listRow.mCardGSubInfo2Txt3.setText("예상");
							break;
						case "EN":
							_listRow.mCardGSubInfo2Txt3.setText("확정");
							break;
						case "PY":
							_listRow.mCardGSubInfo2Txt3.setText("납부완료");
							break;
						case "PN":
							_listRow.mCardGSubInfo2Txt3.setText("미납");
							break;
					}

					String mTempGapDate3 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayDate);
					if (mTempGapDate3.equals("00")){
						_listRow.mCardGSubInfo3date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo3date1.setText("오늘");
						_listRow.mCardGSubInfo3date2.setText("");
						_listRow.mCardGSubInfo3date2.setVisibility(View.GONE);
					}
					else if (mTempGapDate3.equals("01")){
						_listRow.mCardGSubInfo3date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
						_listRow.mCardGSubInfo3date1.setText("내일");
						_listRow.mCardGSubInfo3date2.setText("");
						_listRow.mCardGSubInfo3date2.setVisibility(View.GONE);
					}
					else{
						_listRow.mCardGSubInfo3date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
						_listRow.mCardGSubInfo3date1.setText("D-"+mTempGapDate3);
						_listRow.mCardGSubInfo3date2.setText("");
						_listRow.mCardGSubInfo3date2.setVisibility(View.GONE);
					}
					_listRow.mCardGSubInfo3Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyTitle);
					if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayType.equalsIgnoreCase("01")) {
						_listRow.mCardGSubInfo3Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
						_listRow.mCardGSubInfo3Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
					}
					else {
						_listRow.mCardGSubInfo3Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
						_listRow.mCardGSubInfo3Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
					}
					_listRow.mCardGSubInfo3Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayType) - 1)));
					String mTempPayDate3 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayDate;
					_listRow.mCardGSubInfo3Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate3.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate3));
//					_listRow.mCardGSubInfo3Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayDate);
					_listRow.mCardGSubInfo3Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyAmt));

					String expect_or_paid_2 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyExpectOrPaid;
					switch (expect_or_paid_2) {
						case "EY":
							_listRow.mCardGSubInfo3Txt3.setText("예상");
							break;
						case "EN":
							_listRow.mCardGSubInfo3Txt3.setText("확정");
							break;
						case "PY":
							_listRow.mCardGSubInfo3Txt3.setText("납부완료");
							break;
						case "PN":
							_listRow.mCardGSubInfo3Txt3.setText("미납");
							break;
					}
				}
				else{
					_listRow.mCardGSubLayout1.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout2.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout3.setVisibility(View.VISIBLE);
					_listRow.mCardGSubLayout4.setVisibility(View.VISIBLE);

//					if(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.size() != 0){ // 임시방편으로 작업함 수정해야함
						String mTempGapDate1 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate);
						if (mTempGapDate1.equals("00")){
							_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
							_listRow.mCardGSubInfo1date1.setText("오늘");
							_listRow.mCardGSubInfo1date2.setText("");
							_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
						}
						else if (mTempGapDate1.equals("01")){
							_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
							_listRow.mCardGSubInfo1date1.setText("내일");
							_listRow.mCardGSubInfo1date2.setText("");
							_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
						}
						else{
							_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
							_listRow.mCardGSubInfo1date1.setText("D-"+mTempGapDate1);
							_listRow.mCardGSubInfo1date2.setText("");
							_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
						}
						_listRow.mCardGSubInfo1Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyTitle);
						if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayType.equalsIgnoreCase("01")) {
							_listRow.mCardGSubInfo1Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
							_listRow.mCardGSubInfo1Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
						}
						else {
							_listRow.mCardGSubInfo1Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
							_listRow.mCardGSubInfo1Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
						}
						_listRow.mCardGSubInfo1Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayType) - 1)));
						String mTempPayDate1 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate;
						_listRow.mCardGSubInfo1Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate1.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate1));
//					_listRow.mCardGSubInfo1Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate);
						_listRow.mCardGSubInfo1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyAmt));

						String expect_or_paid_0 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyExpectOrPaid;
						switch (expect_or_paid_0) {
							case "EY":
								_listRow.mCardGSubInfo1Txt3.setText("예상");
								break;
							case "EN":
								_listRow.mCardGSubInfo1Txt3.setText("확정");
								break;
							case "PY":
								_listRow.mCardGSubInfo1Txt3.setText("납부완료");
								break;
							case "PN":
								_listRow.mCardGSubInfo1Txt3.setText("미납");
								break;
						}

						String mTempGapDate2 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate);
						if (mTempGapDate2.equals("00")){
							_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
							_listRow.mCardGSubInfo2date1.setText("오늘");
							_listRow.mCardGSubInfo2date2.setText("");
							_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
						}
						else if (mTempGapDate2.equals("01")){
							_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
							_listRow.mCardGSubInfo2date1.setText("내일");
							_listRow.mCardGSubInfo2date2.setText("");
							_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
						}
						else{
							_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
							_listRow.mCardGSubInfo2date1.setText("D-"+mTempGapDate2);
							_listRow.mCardGSubInfo2date2.setText("");
							_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
						}
						_listRow.mCardGSubInfo2Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyTitle);
						if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayType.equalsIgnoreCase("01")) {
							_listRow.mCardGSubInfo2Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
							_listRow.mCardGSubInfo2Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
						}
						else {
							_listRow.mCardGSubInfo2Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
							_listRow.mCardGSubInfo2Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
						}
						_listRow.mCardGSubInfo2Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayType) - 1)));
						String mTempPayDate2 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate;
						_listRow.mCardGSubInfo2Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate2.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate2));
//					_listRow.mCardGSubInfo2Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyPayDate);
						_listRow.mCardGSubInfo2Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyAmt));

						String expect_or_paid_1 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(1).mBodyMoneyExpectOrPaid;
						switch (expect_or_paid_1) {
							case "EY":
								_listRow.mCardGSubInfo2Txt3.setText("예상");
								break;
							case "EN":
								_listRow.mCardGSubInfo2Txt3.setText("확정");
								break;
							case "PY":
								_listRow.mCardGSubInfo2Txt3.setText("납부완료");
								break;
							case "PN":
								_listRow.mCardGSubInfo2Txt3.setText("미납");
								break;
						}

						String mTempGapDate3 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayDate);
						if (mTempGapDate3.equals("00")){
							_listRow.mCardGSubInfo3date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
							_listRow.mCardGSubInfo3date1.setText("오늘");
							_listRow.mCardGSubInfo3date2.setText("");
							_listRow.mCardGSubInfo3date2.setVisibility(View.GONE);
						}
						else if (mTempGapDate3.equals("01")){
							_listRow.mCardGSubInfo3date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
							_listRow.mCardGSubInfo3date1.setText("내일");
							_listRow.mCardGSubInfo3date2.setText("");
							_listRow.mCardGSubInfo3date2.setVisibility(View.GONE);
						}
						else{
							_listRow.mCardGSubInfo3date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
							_listRow.mCardGSubInfo3date1.setText("D-"+mTempGapDate3);
							_listRow.mCardGSubInfo3date2.setText("");
							_listRow.mCardGSubInfo3date2.setVisibility(View.GONE);
						}
						_listRow.mCardGSubInfo3Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyTitle);
						if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayType.equalsIgnoreCase("01")) {
							_listRow.mCardGSubInfo3Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
							_listRow.mCardGSubInfo3Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
						}
						else {
							_listRow.mCardGSubInfo3Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
							_listRow.mCardGSubInfo3Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
						}
						_listRow.mCardGSubInfo3Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayType) - 1)));
						String mTempPayDate3 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayDate;
						_listRow.mCardGSubInfo3Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate3.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate3));
//					_listRow.mCardGSubInfo3Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyPayDate);
						_listRow.mCardGSubInfo3Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyAmt));

						String expect_or_paid_2 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(2).mBodyMoneyExpectOrPaid;
						switch (expect_or_paid_2) {
							case "EY":
								_listRow.mCardGSubInfo3Txt3.setText("예상");
								break;
							case "EN":
								_listRow.mCardGSubInfo3Txt3.setText("확정");
								break;
							case "PY":
								_listRow.mCardGSubInfo3Txt3.setText("납부완료");
								break;
							case "PN":
								_listRow.mCardGSubInfo3Txt3.setText("미납");
								break;
						}

						String mTempGapDate4 = TransFormatUtils.getGapDateAtoB(TransFormatUtils.getDataFormatWhich(0), mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyPayDate);
						if (mTempGapDate4.equals("00")){
							_listRow.mCardGSubInfo4date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
							_listRow.mCardGSubInfo4date1.setText("오늘");
							_listRow.mCardGSubInfo4date2.setText("");
							_listRow.mCardGSubInfo4date2.setVisibility(View.GONE);
						}
						else if (mTempGapDate4.equals("01")){
							_listRow.mCardGSubInfo4date1.setTextAppearance(mActivity, R.style.CS_Text_18_898989);
							_listRow.mCardGSubInfo4date1.setText("내일");
							_listRow.mCardGSubInfo4date2.setText("");
							_listRow.mCardGSubInfo4date2.setVisibility(View.GONE);
						}
						else{
							_listRow.mCardGSubInfo4date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
							_listRow.mCardGSubInfo4date1.setText("D-"+mTempGapDate4);
							_listRow.mCardGSubInfo4date2.setText("");
							_listRow.mCardGSubInfo4date2.setVisibility(View.GONE);
						}
						_listRow.mCardGSubInfo4Txt1.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyTitle);
						if (mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyPayType.equalsIgnoreCase("01")) {
							_listRow.mCardGSubInfo4Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA3);
							_listRow.mCardGSubInfo4Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
						} else {
							_listRow.mCardGSubInfo4Txt1Tag.setTextAppearance(mActivity, R.style.CS_TagA1);
							_listRow.mCardGSubInfo4Txt1Tag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
						}
						_listRow.mCardGSubInfo4Txt1Tag.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyPayType) - 1)));
						String mTempPayDate4 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyPayDate;
						_listRow.mCardGSubInfo4Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyExpectOrPaid.equalsIgnoreCase("PY")?("매월 "+mTempPayDate4.substring(6) +"일"):TransFormatUtils.transDateForm(mTempPayDate4));
//					_listRow.mCardGSubInfo4Txt2.setText(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyPayDate);
						_listRow.mCardGSubInfo4Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyAmt));

					String expect_or_paid_3 = mAlDataList.get(_position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(3).mBodyMoneyExpectOrPaid;
					switch (expect_or_paid_3) {
						case "EY":
							_listRow.mCardGSubInfo4Txt3.setText("예상");
							break;
						case "EN":
							_listRow.mCardGSubInfo4Txt3.setText("확정");
							break;
						case "PY":
							_listRow.mCardGSubInfo4Txt3.setText("납부완료");
							break;
						case "PN":
							_listRow.mCardGSubInfo4Txt3.setText("미납");
							break;
					}
//					}

				}

			}
			else{
				_listRow.mMain_4_Margin.setVisibility(View.GONE);
				_listRow.mCardGLayout.setVisibility(View.GONE);
				_listRow.mCardHLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
			}
		}
    }
    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {

		private FrameLayout mMain_4_Margin;
    	
    	/* Card A */
//    	private LinearLayout 	mCardALayout;
//    	private TextView		mCardATitle;
//    	private TextView		mCardATitleCount;
//    	private ImageView 		mCardAIcon;
//    	private TextView		mCardACost;
//    	private TextView		mCardASubInfo1;
//    	private TextView		mCardASubInfo2;
    	
    	/* Card B */
//    	private LinearLayout 	mCardBLayout;
//    	private TextView		mCardBTitle;
//    	private TextView		mCardBCost;
//    	private TextView		mCardBSubInfo1;
//    	private TextView		mCardBSubInfo2;
    	
    	/* Card C */
//    	private LinearLayout 	mCardCLayout;
//    	private TextView		mCardCTitle;
//    	private ImageView 		mCardCIcon;
//    	private TextView		mCardCCost;
    	
    	/* Card D */
//    	private LinearLayout 	mCardDLayout;
//    	private TextView		mCardDTitle;
//    	private ImageView 		mCardDIcon;
//    	private TextView		mCardDSubInfo1Cost;
//    	private TextView		mCardDSubInfo1Title;
//    	private TextView		mCardDSubInfo2Cost;
//    	private TextView		mCardDSubInfo2Title;
    	
    	/* Card E */
    	private LinearLayout 	mCardELayout;
    	private TextView		mCardETitle;
    	private ImageView 		mCardEIcon;
    	
    	/* Card F */
//    	private LinearLayout 	mCardFLayout;
//    	private TextView		mCardFTitle;
//    	private ImageView 		mCardFIcon;
//    	private TextView		mCardFSubInfo1Title;
//    	private TextView		mCardFSubInfo1Tag;
//    	private TextView		mCardFSubInfo1Txt;
//    	private TextView		mCardFSubInfo1Cost;
//    	private TextView		mCardFSubInfo2Title;
//    	private TextView		mCardFSubInfo2Tag;
//    	private TextView		mCardFSubInfo2Txt;
//    	private TextView		mCardFSubInfo2Cost;
//    	private TextView		mCardFSubInfo3Title;
//    	private TextView		mCardFSubInfo3Tag;
//    	private TextView		mCardFSubInfo3Txt;
//    	private TextView		mCardFSubInfo3Cost;
    	
    	/* Card G */
    	private LinearLayout 	mCardGLayout;
    	private TextView		mCardGTitle;
		private TextView		mCardGTitleCount;
    	private ImageView 		mCardGIcon;
		private LinearLayout 	mCardGSubLayout1;
    	private TextView		mCardGSubInfo1date1;
    	private TextView		mCardGSubInfo1date2;
    	private TextView		mCardGSubInfo1Txt1;
    	private TextView		mCardGSubInfo1Txt1Tag;
    	private TextView		mCardGSubInfo1Txt2;
    	private TextView		mCardGSubInfo1Txt3;
    	private TextView		mCardGSubInfo1Cost;
		private LinearLayout 	mCardGSubLayout2;
    	private TextView		mCardGSubInfo2date1;
    	private TextView		mCardGSubInfo2date2;
    	private TextView		mCardGSubInfo2Txt1;
    	private TextView		mCardGSubInfo2Txt1Tag;
    	private TextView		mCardGSubInfo2Txt2;
    	private TextView		mCardGSubInfo2Txt3;
    	private TextView		mCardGSubInfo2Cost;
		private LinearLayout 	mCardGSubLayout3;
    	private TextView		mCardGSubInfo3date1;
    	private TextView		mCardGSubInfo3date2;
    	private TextView		mCardGSubInfo3Txt1;
    	private TextView		mCardGSubInfo3Txt1Tag;
    	private TextView		mCardGSubInfo3Txt2;
    	private TextView		mCardGSubInfo3Txt3;
    	private TextView		mCardGSubInfo3Cost;
		private LinearLayout 	mCardGSubLayout4;
    	private TextView		mCardGSubInfo4date1;
    	private TextView		mCardGSubInfo4date2;
    	private TextView		mCardGSubInfo4Txt1;
    	private TextView		mCardGSubInfo4Txt1Tag;
    	private TextView		mCardGSubInfo4Txt2;
    	private TextView		mCardGSubInfo4Txt3;
    	private TextView		mCardGSubInfo4Cost;
    	
    	/* Card H */
		private RelativeLayout mCardHLayout;
		private ViewPager mCardHPagerLayout;
		private LinearLayout mCardHPagerIndex;
//    	private LinearLayout 	mCardHLayout;
//    	private TextView		mCardHTitle;
//    	private TextView		mCardHTitleCount;
//    	private ImageView 		mCardHIcon;
//    	private TextView		mCardHSubInfoTitle;
//    	private TextView		mCardHSubInfoCost;
//    	private LinearLayout	mCardHPageIndex;
    	
    	/* Card I */
//    	private LinearLayout 	mCardILayout;
//    	private TextView		mCardITitle;
//    	private TextView		mCardITitleCount;
//    	private ImageView 		mCardIIcon;
//    	private TextView		mCardISubInfo1Title;
//    	private TextView		mCardISubInfo1Cost;
//    	private TextView		mCardISubInfo2Title;
//    	private TextView		mCardISubInfo2Cost;
    	
    	/* Card J */
//    	private LinearLayout 	mCardJLayout;
//    	private TextView		mCardJTitle;
//    	private TextView		mCardJTitleCount;
//    	private ImageView 		mCardJIcon;
//    	private TextView		mCardJSubInfo1Title;
//    	private TextView		mCardJSubInfo1Cost;
//    	private TextView		mCardJSubInfo2Title;
//    	private TextView		mCardJSubInfo2Cost;
//    	private TextView		mCardJSubInfo3Title;
//    	private TextView		mCardJSubInfo3Cost;
    	
    	/* Card K */
//    	private LinearLayout 	mCardKLayout;
//    	private TextView		mCardKTitle;
//    	private ImageView 		mCardKIcon;
//    	private TextView		mCardKSubInfo1Title;
//    	private TextView		mCardKSubInfo1Cost;
//    	private TextView		mCardKSubInfo2Title;
//    	private TextView		mCardKSubInfo2Cost;
//    	private TextView		mCardKSubInfo3Title;
//    	private TextView		mCardKSubInfo3Cost;
    	
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<InfoDataMain4> array){
    	mAlDataList = array;
	}


//=======================================================================================//
// Page Index
//=======================================================================================//
	/** Page Index **/
	private void initPageIndex(Context context, ViewHolder _listRow){
		try
		{

			_listRow.mCardHPagerIndex.removeAllViews();
			if(mTotalPageNum > 1){
				for(int i=0; i<mTotalPageNum; i++) {
					ImageView iv = new ImageView(context);	//페이지 표시 이미지 뷰 생성
					iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//				iv.setImageResource(getResources().obtainTypedArray(R.array.page_index_items).getResourceId(i, -1));

					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(18, 18);
					lp.setMargins( 0 , 0 , 15 , 0 ); // Left , Top, Right, Bottom

//				if(i==mCurrentPageIdx) iv.setStatus(true);
//				else iv.setStatus(false);
					if(i==mCurrentPageIdx) iv.setBackgroundResource(R.drawable.shape_row_indicator_on);
					else iv.setBackgroundResource(R.drawable.shape_row_indicator_off);

					_listRow.mCardHPagerIndex.addView(iv, lp);
				}
			}
		}
		catch(Exception e){
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initPageIndex() : " + e.toString());
		}
	}

     
}
