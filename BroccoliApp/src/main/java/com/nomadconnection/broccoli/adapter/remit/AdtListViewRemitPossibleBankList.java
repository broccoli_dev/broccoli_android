package com.nomadconnection.broccoli.adapter.remit;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Remit.RemitBank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class AdtListViewRemitPossibleBankList extends BaseAdapter {

    private Context mContext;
    private ArrayList<RemitBank> mAlDataList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;
    private int mSelectItem = 0;

    public AdtListViewRemitPossibleBankList(Context context, ArrayList<RemitBank> list) {
        mContext = context;
        inflater = LayoutInflater.from(context);
        mAlDataList = list;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }

    @Override
    public RemitBank getItem(int position) {
        RemitBank info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_listview_item_remit_possible_bank, null);
            mViewHolder.mPossibleBank = (LinearLayout) v.findViewById(R.id.ll_row_list_item_remit_possible_bank);
            mViewHolder.mBankNameIv = (ImageView) v.findViewById(R.id.iv_row_list_item_remit_possible_bank);
            mViewHolder.mRemitBankStatus = (TextView) v.findViewById(R.id.tv_row_list_item_remit_possible_bank_number);
            mViewHolder.mNonePossibleBank= (LinearLayout)v.findViewById(R.id.ll_row_list_item_remit_possible_none_bank);
            mViewHolder.mNoneBankNameList = (TextView) v.findViewById(R.id.tv_row_list_item_remit_possible_none_bank_list);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        RemitBank mBankItem = getItem(position);

        if("Y".equals(mBankItem.getPossibleSend())){
            mViewHolder.mPossibleBank.setVisibility(View.VISIBLE);
            mViewHolder.mNonePossibleBank.setVisibility(View.GONE);

            List<String> mBankCode = Arrays.asList(mContext.getResources().getStringArray(R.array.bank_code));
            for(int i = 0; i < mBankCode.size(); i++){
                if(mBankItem.getCompanyCode().equals(mBankCode.get(i))){
                    TypedArray imgs = mContext.getResources().obtainTypedArray(R.array.bank_name_image);

                    // get resource ID by index
                    imgs.getResourceId(i, -1);

                    // or set you ImageView's resource to the id
                    mViewHolder.mBankNameIv.setImageResource(imgs.getResourceId(i, -1));

                    // recycle the array
                    imgs.recycle();
                }
            }

            if("Y".equals(mBankItem.getHaveAccount())){
                mViewHolder.mRemitBankStatus.setText(mContext.getResources().getString(R.string.remit_link_account));
                mViewHolder.mRemitBankStatus.setTextColor(mContext.getResources().getColor(R.color.tag_d_txt_color));
            }
            else {
                mViewHolder.mRemitBankStatus.setText(mContext.getResources().getString(R.string.remit_link_account_none));
                mViewHolder.mRemitBankStatus.setTextColor(mContext.getResources().getColor(R.color.subitem_left_txt_dim_color));
            }
        }
        else{
            String getBank = mViewHolder.mNoneBankNameList.getText().toString();
            String tempBank = mBankItem.getCompanyName() + ",";
            mViewHolder.mPossibleBank.setVisibility(View.GONE);
            mViewHolder.mNonePossibleBank.setVisibility(View.VISIBLE);
            mViewHolder.mNoneBankNameList.setText(getBank + tempBank);
        }

        return v;
    }

    class ViewHolder {
        LinearLayout mPossibleBank;
        ImageView mBankNameIv;
        TextView mRemitBankStatus;
        LinearLayout mNonePossibleBank;
        TextView mNoneBankNameList;
    }

//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<RemitBank> array){
        mAlDataList = array;
    }

}
