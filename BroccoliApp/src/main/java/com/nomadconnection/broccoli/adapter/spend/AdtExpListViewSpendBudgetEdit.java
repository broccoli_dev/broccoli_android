package com.nomadconnection.broccoli.adapter.spend;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetCategoryEdit;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetDetailEdit;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class AdtExpListViewSpendBudgetEdit extends BaseExpandableListAdapter {

    private static final int MAX_GROUP_COUNT = 2;

    private TreeMap<Integer, SpendBudgetCategoryEdit> mGroupList = null;
    private HashMap<Integer, ArrayList<SpendBudgetDetailEdit>> mChildList = null;
    private LayoutInflater inflater = null;
    private String[] mCategoryGroup;
    private String[] mDetailArray;
    private Object[] groupArray;
    private Object[] childArray;
    private DataSetObserver mObserver;

    public AdtExpListViewSpendBudgetEdit(Context context, TreeMap<Integer, SpendBudgetCategoryEdit> groupList, HashMap<Integer, ArrayList<SpendBudgetDetailEdit>> childList, DataSetObserver observer){
        super();
        this.inflater = LayoutInflater.from(context);
        this.mGroupList = groupList;
        this.mChildList = childList;
        this.mCategoryGroup = context.getResources().getStringArray(R.array.spend_budget_group_category);
        this.mDetailArray = context.getResources().getStringArray(R.array.spend_category);
        this.mObserver = observer;
        this.groupArray = groupList.keySet().toArray();
        this.childArray = childList.keySet().toArray();
    }

    public TreeMap<Integer, SpendBudgetCategoryEdit> getGroupMap() {
        return mGroupList;
    }

    public HashMap<Integer, ArrayList<SpendBudgetDetailEdit>> getDetailMap() {
        return mChildList;
    }
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public SpendBudgetCategoryEdit getGroup(int groupPosition){
        return mGroupList.get((int)groupArray[groupPosition]);
    }
 
    @Override
    public int getGroupCount(){
        int count = 0;
        if (mGroupList != null) {
            count = mGroupList.size();
        }
        return count;
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        GroupHolder groupHolder = null;
        int groupType = getGroupType(groupPosition);

        if(convertView == null){
            if (groupType == SpendBudgetCategoryEdit.EditType.GROUP.ordinal()) {
                groupHolder = new GroupHolder();
                convertView = inflater.inflate(R.layout.row_item_group_budget_edit, parent, false);
                groupHolder.mGroupTitle = (TextView)convertView.findViewById(R.id.tv_row_spend_budget_title);
                groupHolder.mGroupTitle.setTypeface(Typeface.DEFAULT_BOLD);
                groupHolder.mGroupBudget = (EditText)convertView.findViewById(R.id.et_row_spend_budget_category);
                groupHolder.mGroupUnit = (TextView) convertView.findViewById(R.id.tv_row_spend_budget_unit);
                groupHolder.mWatcher = new IndexTextWatcher(getGroup(groupPosition));
                groupHolder.mGroupBudget.addTextChangedListener(groupHolder.mWatcher);
                groupHolder.mId = groupType;
                convertView.setTag(groupHolder);
            } else {
                groupHolder = new GroupHolder();
                convertView = inflater.inflate(R.layout.row_item_group_budget_edit, parent, false);
                groupHolder.mGroupTitle = (TextView)convertView.findViewById(R.id.tv_row_spend_budget_title);
                groupHolder.mGroupBudget = (EditText)convertView.findViewById(R.id.et_row_spend_budget_category);
                groupHolder.mGroupUnit = (TextView) convertView.findViewById(R.id.tv_row_spend_budget_unit);
                groupHolder.mWatcher = new IndexTextWatcher(getGroup(groupPosition));
                groupHolder.mGroupBudget.addTextChangedListener(groupHolder.mWatcher);
                groupHolder.mId = groupType;
                convertView.setTag(groupHolder);
            }
        }else {
            groupHolder = (GroupHolder)convertView.getTag();
        }

        if (groupType == SpendBudgetCategoryEdit.EditType.TOTAL.ordinal()) {
            SpendBudgetCategoryEdit category = getGroup(groupPosition);
            String title = "총 예산";
            groupHolder.mGroupTitle.setText(title);
            groupHolder.mWatcher.setDataSet(category);
//            groupHolder.mGroupBudget.addTextChangedListener(groupHolder.mWatcher);
            String budget = null;
            if (category.mEditText != null && !TextUtils.isEmpty(category.mEditText)) {
                budget = category.mEditText.replaceAll(",", "");
            } else {
                budget = String.valueOf(category.sum);
            }
            groupHolder.mGroupBudget.setText(ElseUtils.getStringDecimalFormat(budget));
        } else {
            SpendBudgetCategoryEdit category = getGroup(groupPosition);
            String title = null;
            try {
                title = mCategoryGroup[category.categoryGroup];
            } catch (Exception e) {
                e.printStackTrace();
            }
            groupHolder.mGroupTitle.setText(title);
            groupHolder.mWatcher.setDataSet(category);
//            groupHolder.mGroupBudget.addTextChangedListener(groupHolder.mWatcher);
            String budget = null;
            if (category.mEditText != null && !TextUtils.isEmpty(category.mEditText)) {
                budget = category.mEditText.replaceAll(",", "");
            } else {
                budget = String.valueOf(category.sum);
            }
            groupHolder.mGroupBudget.setText(ElseUtils.getStringDecimalFormat(budget));
        }

        return convertView;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public SpendBudgetDetailEdit getChild(int groupPosition, int childPosition){
        return mChildList.get(groupArray[groupPosition]).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        int count = 0;
        if (mChildList != null) {
            ArrayList<SpendBudgetDetailEdit> list = mChildList.get(groupArray[groupPosition]);
            if (list != null) {
                count = list.size();
            }
        }
        return count;
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_item_child_budget_edit, null);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_budget_title);
            viewHolder.mEditText = (EditText) convertView.findViewById(R.id.et_row_spend_budget_sub_category);
            viewHolder.mWatcher = new IndexTextWatcher(getChild(groupPosition, childPosition));
            viewHolder.mEditText.addTextChangedListener(viewHolder.mWatcher);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        SpendBudgetDetailEdit detailEdit = getChild(groupPosition, childPosition);
        String title = mDetailArray[detailEdit.largeCategoryId];
        viewHolder.mTitle.setText(title);


        String budget = null;
        viewHolder.mWatcher.setDataSet(detailEdit);
        if (detailEdit.mEditText != null && !TextUtils.isEmpty(detailEdit.mEditText)) {
            budget = detailEdit.mEditText.replaceAll(",", "");
        } else {
            budget = "0";
        }
        viewHolder.mEditText.setText(ElseUtils.getStringDecimalFormat(budget));

        return convertView;
    }

    @Override
    public int getGroupType(int groupPosition) {
        return mGroupList.get(groupPosition).editType.ordinal();
    }

    @Override
    public int getGroupTypeCount() {
        return MAX_GROUP_COUNT;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setData(TreeMap<Integer, SpendBudgetCategoryEdit> mGroupData, HashMap<Integer, ArrayList<SpendBudgetDetailEdit>> mChildData) {
        this.mGroupList = mGroupData;
        this.mChildList = mChildData;
        groupArray = mGroupList.keySet().toArray();
        childArray = mChildList.keySet().toArray();
    }


    class GroupHolder {
        public int mId;
        public TextView mGroupTitle;
        public EditText mGroupBudget;
        public TextView mGroupUnit;
        public IndexTextWatcher mWatcher;
    }

    class ViewHolder{
        /* child */
        public int mId;
        public TextView mTitle;
        public EditText mEditText;
        public TextView mUnit;
        public IndexTextWatcher mWatcher;
    }

    private class IndexTextWatcher implements TextWatcher {
        private int index = 0;

        Object mObj;

        public IndexTextWatcher(Object object) {
            mObj = object;
        }

        public void setDataSet(Object object) {
            mObj = object;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mObj instanceof SpendBudgetCategoryEdit) {
                SpendBudgetCategoryEdit edit = ((SpendBudgetCategoryEdit) mObj);
                if (edit.mEditText != null){
                    if (!edit.mEditText.equalsIgnoreCase(s.toString())) {
                        edit.mEditText = s.toString();
                        notifyDataSetChanged();
                    }
                } else {
                    edit.mEditText = s.toString();
                    notifyDataSetChanged();
                }
            } else if (mObj instanceof SpendBudgetDetailEdit) {
                SpendBudgetDetailEdit edit = ((SpendBudgetDetailEdit) mObj);
                if (edit.mEditText != null) {
                    if (!edit.mEditText.equalsIgnoreCase(s.toString())) {
                        String str = s.toString();
                        if (TextUtils.isEmpty(str)) {
                            str = "0";
                        }
                        edit.mEditText = str;
                        notifyDataSetChanged();
                    }
                } else {
                    edit.mEditText = s.toString();
                    notifyDataSetChanged();
                }
            }
            if (s.length() > 0) {
                Selection.setSelection(s, s.length());
            }
            mObserver.onChanged();
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }

    class CustomTextWatcher implements TextWatcher {

        private EditText mEditText;
        String strAmount = ""; // 임시저장값 (콤마)
        Object mObj;

        public CustomTextWatcher(EditText e, Object object) {
            mEditText = e;
            mObj = object;
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
            if (!s.toString().equals(strAmount)) { // StackOverflow를 막기위해,
                strAmount = makeStringComma(s.toString().replace(",", ""));
                mEditText.setText(strAmount);
                Editable e = mEditText.getText();
                if (mObj instanceof SpendBudgetCategoryEdit) {
                    ((SpendBudgetCategoryEdit) mObj).mEditText = strAmount;
                } else if (mObj instanceof SpendBudgetDetailEdit) {
                    ((SpendBudgetDetailEdit) mObj).mEditText = strAmount;
                }
                Selection.setSelection(e, strAmount.length());
                mObserver.onChanged();
                notifyDataSetChanged();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        protected String makeStringComma(String str) {
            if (str.length() == 0)
                return "";
            long value = Long.parseLong(str);
            DecimalFormat format = new DecimalFormat("###,###");
            return format.format(value);
        }
    }
}
