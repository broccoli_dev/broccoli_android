package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

import java.util.List;


public class AdtListViewDlgChallengeType extends BaseAdapter {

	private Activity mActivity;
	private ViewHolder mViewHolder;

	private List<String> mChallengeType;
	private int mSelectValue = 0;

	public AdtListViewDlgChallengeType(Activity _Activity, List<String> _ChallengeTypeList){
		mActivity = _Activity;
		mChallengeType = _ChallengeTypeList;
	}

	@Override
	public int getCount() {
		return mChallengeType.size();
	}

	@Override
	public String  getItem(int position) {
		return mChallengeType.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View mView = convertView;
		LayoutInflater inflator = mActivity.getLayoutInflater();
		
		if(mView == null){
			mViewHolder = new ViewHolder();
			mView = inflator.inflate(R.layout.row_grid_item_card, null);
			
			mViewHolder.mLayout = (RelativeLayout)mView.findViewById(R.id.ll_row_grid_item_card_layout);
			mViewHolder.mRadioButton = (RadioButton)mView.findViewById(R.id.rb_row_grid_item_card_icon);
			mViewHolder.mRadioButton.setFocusable(false);
			mViewHolder.mRadioButton.setClickable(false);
			mViewHolder.mRadioButton.setChecked(((ListView)parent).isItemChecked(position));

			mViewHolder.mTitle = (TextView)mView.findViewById(R.id.tv_row_grid_item_card_text);

			mView.setTag(mViewHolder);
		}
		else{
			mViewHolder = (ViewHolder)mView.getTag();
		}

		/* 값 입력 */
		mViewHolder.mRadioButton.setChecked(mSelectValue == position);

		mViewHolder.mTitle.setText(mChallengeType.get(position));

		return mView;
	}

	public void setSelectValue(int _selectValue){
		mSelectValue = _selectValue;
	}


//====================================================================//
// ViewHolder
//====================================================================//   
	private class ViewHolder {
		private RelativeLayout mLayout;
		private RadioButton mRadioButton;
		private TextView  mTitle;
	}
}
