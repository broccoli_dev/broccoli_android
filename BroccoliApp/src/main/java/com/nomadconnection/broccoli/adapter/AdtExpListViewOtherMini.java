package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.InfoDataOtherMini;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

public class AdtExpListViewOtherMini extends BaseExpandableListAdapter {

    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<InfoDataOtherMini>> mChildList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;

    public AdtExpListViewOtherMini(Context c, ArrayList<String> groupList, ArrayList<ArrayList<InfoDataOtherMini>> childList){
        super();
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_group_other_mini, parent, false);
            mViewHolder.mGroupA = (LinearLayout)v.findViewById(R.id.ll_row_expandable_listview_item_group_other_mini_group_a_layout);
            mViewHolder.mGroupB = (LinearLayout)v.findViewById(R.id.ll_row_expandable_listview_item_group_other_mini_group_b_layout);
            mViewHolder.mGroupATitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_other_mini_group_a_title);
            mViewHolder.mGroupACost = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_other_mini_group_a_cost);
            mViewHolder.mGroupBTitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_other_mini_group_b_title);
            mViewHolder.mGroupBCount = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_other_mini_title_group_b_count);
            mViewHolder.mGroupIndicator = (ImageView) v.findViewById(R.id.iv_row_expandable_listview_item_group_other_mini_image);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        if (getGroup(groupPosition).equals("미납") || getGroup(groupPosition).equals("완료")){
            mViewHolder.mGroupA.setVisibility(View.GONE);
            mViewHolder.mGroupB.setVisibility(View.VISIBLE);
            mViewHolder.mGroupBTitle.setText(getGroup(groupPosition));
            mViewHolder.mGroupBCount.setText(String.valueOf(getChildrenCount(groupPosition)));
            if (isExpanded) {
                mViewHolder.mGroupIndicator.setImageResource(R.drawable.group_list_arrow_up);
            } else {
                mViewHolder.mGroupIndicator.setImageResource(R.drawable.group_list_arrow_down);
            }
        }
        else{
            mViewHolder.mGroupA.setVisibility(View.VISIBLE);
            mViewHolder.mGroupB.setVisibility(View.GONE);
            mViewHolder.mGroupATitle.setText(getGroup(groupPosition));
            long mTempCalc = 0;
            for (int i = 0; i < getChildrenCount(groupPosition); i++) {
                mTempCalc += Long.valueOf(getChild(groupPosition, i).mTxt5);
            }
            mViewHolder.mGroupACost.setText(TransFormatUtils.getDecimalFormatRecvString(String.valueOf(mTempCalc)));
        }

        return v;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public InfoDataOtherMini getChild(int groupPosition, int childPosition){
//    	public String getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_other_mini, null);
            mViewHolder.mInfo1 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_other_mini_info_1);
            mViewHolder.mInfo2 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_other_mini_info_2);
            mViewHolder.mInfoMethod = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_other_mini_info_method);
            mViewHolder.mInfoCost = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_other_mini_cost);
            mViewHolder.mInfo3 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_other_mini_info_3);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }
 
        mViewHolder.mInfo1.setText(getChild(groupPosition, childPosition).mTxt2);
        if (getChild(groupPosition, childPosition).mTxt3.equalsIgnoreCase("01")) {
            mViewHolder.mInfoMethod.setTextAppearance(MainApplication.mAPPContext, R.style.CS_TagA3);
            mViewHolder.mInfoMethod.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
        }
        else {
            mViewHolder.mInfoMethod.setTextAppearance(MainApplication.mAPPContext, R.style.CS_TagA1);
            mViewHolder.mInfoMethod.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
        }
        mViewHolder.mInfoMethod.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(getChild(groupPosition, childPosition).mTxt3)-1)));
        String mTempPayDate = getChild(groupPosition, childPosition).mTxt4;
        String mRepeatType = getChild(groupPosition, childPosition).mTxt9;
        String dateRepeat = "매월 ";
        if ("NO".equalsIgnoreCase(mRepeatType)) {
            dateRepeat = "";
        }

        String time = TransFormatUtils.transDateForm(mTempPayDate);

        mViewHolder.mInfoCost.setText(TransFormatUtils.getDecimalFormatRecvString(getChild(groupPosition, childPosition).mTxt5));
       // mViewHolder.mInfo3.setText(getChild(groupPosition, childPosition).mTxt6.equalsIgnoreCase("y")?"확정":"예상");

        String mTempExpect = getChild(groupPosition, childPosition).mTxt6;
        switch (mTempExpect) {
            case "EY":
                mViewHolder.mInfo3.setText("예상");
                if (!"NO".equalsIgnoreCase(mRepeatType)) {
                    time = dateRepeat + mTempPayDate.substring(6) +"일";
                }
                break;
            case "EN":
                mViewHolder.mInfo3.setText("확정");
                break;
            case "PY":
                mViewHolder.mInfo3.setText("납부완료");
                time = dateRepeat + mTempPayDate.substring(6) +"일";
                break;
            case "PN":
                mViewHolder.mInfo3.setText("미납");
                break;
        }
        mViewHolder.mInfo2.setText(time);

        if (isLastChild) ((View)v.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)v.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return v;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
 
 
    class ViewHolder{
    	/* group */
        public LinearLayout mGroupA;
        public LinearLayout mGroupB;
        public TextView mGroupATitle;
        public TextView mGroupACost;
        public TextView mGroupBTitle;
        public TextView mGroupBCount;
        public ImageView mGroupIndicator;

        /* child */
        public TextView mInfo1;
        public TextView mInfo2;
        public TextView mInfoMethod;
        public TextView mInfoCost;
        public TextView mInfo3;
    }
}
