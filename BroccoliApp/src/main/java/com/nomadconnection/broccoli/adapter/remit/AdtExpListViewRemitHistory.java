package com.nomadconnection.broccoli.adapter.remit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.InfoDataAssetOther;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

public class AdtExpListViewRemitHistory extends BaseExpandableListAdapter {

    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<String>> mChildList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;

    public AdtExpListViewRemitHistory(Context c, ArrayList<String> groupList, ArrayList<ArrayList<String>> childList){
        super();
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_group_remit_history, parent, false);
            mViewHolder.mGroupName = (TextView)v.findViewById(R.id.tv_row_expandable_list_item_group_remit_history_name);
            mViewHolder.mGroupMethod = (TextView)v.findViewById(R.id.tv_row_expandable_list_item_group_remit_history_method);
            mViewHolder.mGroupTime = (TextView)v.findViewById(R.id.tv_row_expandable_list_item_group_remit_history_time);
            mViewHolder.mGroupCost = (TextView)v.findViewById(R.id.tv_row_expandable_list_item_group_remit_history_cost);
            mViewHolder.mGroupImage = (ImageView)v.findViewById(R.id.iv_row_expandable_list_item_group_remit_history);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        if(isExpanded){
            mViewHolder.mGroupImage.setImageResource(R.drawable.list_up_ic_p);
        }
        else {
            mViewHolder.mGroupImage.setImageResource(R.drawable.list_down_ic_p);
        }

        mViewHolder.mGroupName.setText(getGroup(groupPosition));
        return v;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public String getChild(int groupPosition, int childPosition){
//    	public String getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_remit_history, null);
            mViewHolder.mButton = (LinearLayout) v.findViewById(R.id.ll__row_expandable_list_item_child_remit_history_stand);
            mViewHolder.mText = (LinearLayout) v.findViewById(R.id.ll__row_expandable_list_item_child_remit_history_complete);
            mViewHolder.mRequest = (Button) v.findViewById(R.id.btn_row_expandable_list_item_child_remit_history_request);
            mViewHolder.mCancel = (Button) v.findViewById(R.id.btn_row_expandable_list_item_child_remit_history_cancel);
            mViewHolder.mInfo1 = (TextView) v.findViewById(R.id.tv_row_expandable_list_item_child_remit_history_account_remit);
            mViewHolder.mInfo2 = (TextView) v.findViewById(R.id.tv_row_expandable_list_item_child_remit_history_account_draw);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        if (isLastChild) ((View)v.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)v.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return v;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
 
 
    class ViewHolder{
    	/* group */
        public TextView mGroupName;
        public TextView mGroupMethod;
        public TextView mGroupTime;
        public TextView mGroupCost;
        public ImageView mGroupImage;

        /* child */
        public LinearLayout mButton;
        public LinearLayout mText;
        public Button mRequest;
        public Button mCancel;
        public TextView mInfo1;
        public TextView mInfo2;
    }

    public void setData(ArrayList<String> groupList, ArrayList<ArrayList<String>> childList) {
        this.mGroupList = groupList;
        this.mChildList = childList;
        notifyDataSetChanged();
    }

//====================================================================//
// Sort
//====================================================================//
    String mSort = "all";
    public void sortData(String _sort){
        mSort = _sort;
    }
}
