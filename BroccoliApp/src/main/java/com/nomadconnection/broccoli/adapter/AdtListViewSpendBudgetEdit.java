package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.BodySpendBudgetCategory;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;


public class AdtListViewSpendBudgetEdit extends BaseAdapter {

    private int lastFocussedPosition = -1;
    private Handler handler = new Handler();

	private List<BodySpendBudgetCategory>   mAlDataList;
    private int mSpendBudgetSum             = 0;
	private Activity			  		    mActivity;
    private boolean                         mEditMode = false;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewSpendBudgetEdit(Activity _activity, List<BodySpendBudgetCategory> _AlDataList) {
        mAlDataList = new ArrayList<BodySpendBudgetCategory>();
		mActivity 		= _activity;
        mAlDataList.clear();
        mAlDataList.addAll(_AlDataList);

//        mSpendBudgetDataList = new ArrayList<BodySpendBudgetDetail>();
//        for(int i= 0; i <mAlDataList.size(); i++){
//            int value = Integer.valueOf(mAlDataList.get(i).getSum());
//            BodySpendBudgetDetail mBudget = new BodySpendBudgetDetail();
//            mBudget.setCategoryId(mAlDataList.get(i).getCategoryId());
//            mBudget.setSum(mAlDataList.get(i).getSum());
//            mSpendBudgetDataList.add(mBudget);
//            mSpendBudgetSum += value;
//        }
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public BodySpendBudgetCategory getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	
    	final ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_spend_budget_edit, parent, false);
            listRow.mBudgetName = (TextView) convertView.findViewById(R.id.tv_row_list_item_spend_budget_name);
            listRow.mBudgetSum	= (TextView) convertView.findViewById(R.id.tv_row_list_item_spend_budget_sum);
            listRow.mBudgetCost = (EditText) convertView.findViewById(R.id.et_row_list_item_spend_budget_sum_value);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        String[] category = mActivity.getResources().getStringArray(R.array.spend_category);
        long last_month_cost = 0;
        int categoryType = 0;

        last_month_cost = Long.valueOf(mAlDataList.get(position).getExpense());

        if(mAlDataList.get(position).getCategoryId() != null){
            categoryType = Integer.valueOf(mAlDataList.get(position).getCategoryId());
        }

        listRow.mBudgetName.setText(category[categoryType]);
        listRow.mBudgetSum.setText(mActivity.getString(R.string.spend_last_month) + "/" + ElseUtils.getDecimalFormat(last_month_cost));

        if(mAlDataList.get(position).getBudget() == null){
            //mAlDataList.get(position).setSum("0");
            listRow.mBudgetCost.setHint("0");
        }
        else {
            listRow.mBudgetCost.setText(ElseUtils.getStringDecimalFormat(mAlDataList.get(position).getBudget()));
        }

        textWatcher watcher = null;
        if(listRow.mBudgetCost.getTag() != null){
            //watcher = (textWatcher)listRow.mBudgetCost.getTag();
            //watcher.setData(listRow.mBudgetCost, position);
            watcher = new textWatcher(listRow.mBudgetCost, position);
            listRow.mBudgetCost.addTextChangedListener(watcher);
        }
        else {
            watcher = new textWatcher(listRow.mBudgetCost, position);
            listRow.mBudgetCost.addTextChangedListener(watcher);
        }
        listRow.mBudgetCost.setTag(watcher);


//        listRow.mBudgetCost.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                final int _position = (int)v.getTag();
//                if (hasFocus) {
//                    handler.postDelayed(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            if (lastFocussedPosition == -1 || lastFocussedPosition == _position) {
//                                lastFocussedPosition = _position;
//                                listRow.mBudgetCost.requestFocus();
//                            }
//                        }
//                    }, 200);
//
//                } else {
//                    lastFocussedPosition = -1;
//                }
//
//            }
//        });
//
//        listRow.mBudgetCost.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mEditMode = true;
//                Toast.makeText(mActivity, "눌러요", Toast.LENGTH_SHORT).show();
//
//                listRow.mBudgetCost.setFocusable(true);
//                listRow.mBudgetCost.setInputType(InputType.TYPE_CLASS_NUMBER);
//                listRow.mBudgetCost.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_191_191_191));
//                listRow.mBudgetCost.setBackgroundResource(R.drawable.selector_edittext_bg);
//                //notifyDataSetChanged();
//            }
//        });
//
//        if(mEditMode == false){
//            if(cost == 0){
//                listRow.mBudgetCost.setFocusable(false);
//                listRow.mBudgetCost.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_191_191_191));
//                listRow.mBudgetCost.setBackgroundResource(R.drawable.selector_edittext_bg);
//            }
//            else {
//                listRow.mBudgetCost.setFocusable(false);
//                listRow.mBudgetCost.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_89_89_89));
//                listRow.mBudgetCost.setBackgroundResource(0);
//            }
//        }


        return convertView;
    }





//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private TextView       mBudgetName;
    	private TextView       mBudgetSum;
        private EditText       mBudgetCost;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setDBData(List<BodySpendBudgetCategory> array){
        mAlDataList = array;
    }

    public List<BodySpendBudgetCategory> getData(){
        return mAlDataList;
    }


//    public int getmSpendBudgetSum(){
//        return mSpendBudgetSum;
//    }



    private class textWatcher implements TextWatcher {
        private View view;
        private int position;

        private textWatcher(View view, int position) {
            this.view = view;
            this.position = position;
        }

        public void setData(View view, int position){
            this.view = view;
            this.position = position;
        }

        @Override
        public void afterTextChanged(Editable s) {
            mAlDataList.get(position).setSum(s.toString());
//                setDBData(mAlDataList);
//                mSpendBudgetSum += Integer.valueOf(s.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO Auto-generated method stub
        }
    }

}
