package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.SpendMonthlyInfo;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;


public class AdtListViewSpendCategoryDetail extends BaseAdapter {

	private List<SpendMonthlyInfo> 	mAlDataList = null;
	private Activity			  	mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewSpendCategoryDetail(Activity _activity, List<SpendMonthlyInfo> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }
 
    @Override
    public SpendMonthlyInfo getItem(int position) {
        SpendMonthlyInfo info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_spend_category_detail, parent, false);
            listRow.mSpendName = (TextView) convertView.findViewById(R.id.tv_row_item_spend_category_detail_title);
            listRow.mSpendTag	= (TextView) convertView.findViewById(R.id.tv_row_item_spend_category_detail_tag);
            listRow.mSpendTime = (TextView) convertView.findViewById(R.id.tv_row_item_spend_category_detail_sub_text);
            listRow.mSpendCost = (TextView) convertView.findViewById(R.id.tv_row_item_spend_category_detail_cost);
            listRow.mDetailMemo = (ImageView) convertView.findViewById(R.id.iv_row_list_item_spend_category_detail_memo);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        String[] payType = mActivity.getResources().getStringArray(R.array.spend_pay_type);
        int payTypeInt = 0;
        if(Integer.parseInt(mAlDataList.get(position).getPayType()) > 5){
            payTypeInt = 5;
        }
        else {
            payTypeInt = Integer.parseInt(mAlDataList.get(position).getPayType());
        }

        listRow.mSpendName.setText(mAlDataList.get(position).getOpponent());
        switch (payTypeInt){
            case 1: //신용카드
                listRow.mSpendTag.setTextAppearance(mActivity, R.style.CS_TagA1);
                listRow.mSpendTag.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
                break;
            case 2: //체크카드
                listRow.mSpendTag.setTextAppearance(mActivity, R.style.CS_TagA2);
                listRow.mSpendTag.setBackgroundResource(R.drawable.shape_tag_a_bg_2);
                break;
            case 3: //현금
                listRow.mSpendTag.setTextAppearance(mActivity, R.style.CS_TagA4);
                listRow.mSpendTag.setBackgroundResource(R.drawable.shape_tag_a_bg_4);
                break;
            case 4: //계좌이체
                listRow.mSpendTag.setTextAppearance(mActivity, R.style.CS_TagA3);
                listRow.mSpendTag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
                break;
            case 5: //기타
                listRow.mSpendTag.setTextAppearance(mActivity, R.style.CS_TagA3);
                listRow.mSpendTag.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
                break;
        }
        listRow.mSpendTag.setText(payType[payTypeInt]);
        listRow.mSpendTime.setText(ElseUtils.getSpendDate(mAlDataList.get(position).getExpenseDate()));
        listRow.mSpendCost.setText(ElseUtils.getStringDecimalFormat(mAlDataList.get(position).getSum()));

        if (mAlDataList.get(position).getMemo() != null && !TextUtils.isEmpty(mAlDataList.get(position).getMemo())) {
            listRow.mDetailMemo.setVisibility(View.VISIBLE);
        } else {
            listRow.mDetailMemo.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }





//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private TextView       mSpendName;
    	private TextView       mSpendTag;
        private TextView       mSpendTime;
        private TextView       mSpendCost;
        private ImageView       mDetailMemo;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<SpendMonthlyInfo> array){
    	mAlDataList = array;
	}
    
     
}
