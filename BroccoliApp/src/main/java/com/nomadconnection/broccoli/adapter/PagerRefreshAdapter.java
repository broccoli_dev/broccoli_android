package com.nomadconnection.broccoli.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

//public class MainPagerAdapter extends FragmentStatePagerAdapter  {
public class PagerRefreshAdapter extends FragmentStatePagerAdapter {

	private String[] mPagerTitle;
	private ArrayList<Fragment> mFragments;

	public PagerRefreshAdapter(FragmentManager fm, ArrayList<Fragment> _alFragment, String[] _PagerTitle) {
		super(fm);
		mFragments = _alFragment;
		mPagerTitle = _PagerTitle;
	}

	public void refreshData(ArrayList<Fragment> _alFragment){
		mFragments = _alFragment;
		notifyDataSetChanged();
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	@Override
	public Fragment getItem(int i) {
		if(i < mFragments.size())
			return mFragments.get(i);
		else
			return null;
	}

	@Override
	public int getCount() {
		return mFragments.size();
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		return mPagerTitle[position];
	}

}
