package com.nomadconnection.broccoli.adapter.spend;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.constant.BenefitInternalType;
import com.nomadconnection.broccoli.data.Spend.SpendCardBenefitDetailRowData;
import com.nomadconnection.broccoli.data.Spend.SpendCardBenefitInternalBody;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created by YelloHyunminJang on 2017. 3. 30..
 */

public class AdtSpendCardBenefitRow extends BaseAdapter {

    private LayoutInflater mInflater;
    private TreeMap<Integer, HashMap<String, SpendCardBenefitDetailRowData>> map;
    private HashMap<String, SpendCardBenefitDetailRowData> mRealMap;
    private Object[] mFirstCategoryArray;
    private Object[] mKeyArray;
    private int mFirstCategoryIndex;

    public AdtSpendCardBenefitRow(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public void setData(int firstCategoryIndex, TreeMap<Integer, HashMap<String, SpendCardBenefitDetailRowData>> data) {
        map = data;
        mFirstCategoryArray = data.keySet().toArray();
        mFirstCategoryIndex = firstCategoryIndex;
        mRealMap = map.get(mFirstCategoryArray[firstCategoryIndex]);
        mKeyArray = mRealMap.keySet().toArray();
    }

    public void setFirstCategoryId(int firstCategoryIndex) {
        mFirstCategoryIndex = firstCategoryIndex;
        mRealMap = map.get(mFirstCategoryArray[mFirstCategoryIndex]);
        mKeyArray = mRealMap.keySet().toArray();
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mRealMap != null) {
            count = mRealMap.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        if (mRealMap != null) {
            object = mRealMap.get(mKeyArray[position]);
        }
        return object;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_list_item_spend_card_benefit, null);
            holder = new ViewHolder();
            holder.mTitle = (TextView) convertView.findViewById(R.id.tv_spend_card_benefit_name);
            holder.mDiscount = (TextView) convertView.findViewById(R.id.tv_spend_card_benefit_discount);
            holder.mPoint = (TextView) convertView.findViewById(R.id.tv_spend_card_benefit_point);
            holder.mMileage = (TextView) convertView.findViewById(R.id.tv_spend_card_benefit_mileage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            holder.mDiscount.setVisibility(View.GONE);
            holder.mPoint.setVisibility(View.GONE);
            holder.mMileage.setVisibility(View.GONE);
        }

        SpendCardBenefitDetailRowData row = mRealMap.get(mKeyArray[position]);
        holder.mTitle.setText(row.benefitName);
        if (row.benefitTypeMap != null) {
            Object[] benefitType = row.benefitTypeMap.keySet().toArray();
            for (int i=0; i < benefitType.length; i++) {
                Integer type = (Integer) benefitType[i];
                HashMap<BenefitInternalType, SpendCardBenefitInternalBody> benefitMap = row.benefitTypeMap.get(type);
                Object[] internalTypes = benefitMap.keySet().toArray();
                String content = "";
                for (int j=0; j < internalTypes.length; j++) {
                    String pre = "";
                    if (j > 0) {
                        pre = "\n";
                    }
                    BenefitInternalType temp = (BenefitInternalType) internalTypes[j];
                    SpendCardBenefitInternalBody body = benefitMap.get(temp);
                    String max = "";
                    String amount = "";
                    String benefit = "";
                    if (body.isMultipleData) {
                        max = "최대 ";
                    }
                    if (body.isDefaultBenefitExist) {
                        if (j == 0 && body.affiliateYn == 1 && !TextUtils.isEmpty(body.defaultBenefitContent)) {
                            pre += body.defaultBenefitContent+"\n+";
                        }
                    }
                    switch (type) {
                        case 1:
                            switch (temp) {
                                case RATE:
                                    benefit = "할인";
                                    break;
                                case UNIT:
                                    benefit = "원 할인";
                                    break;
                                case AMOUNT:
                                    benefit = "원 할인";
                                    break;
                            }
                            break;
                        case 2:
                            benefit = "적립";
                            break;
                        case 3:
                            benefit = "마일 적립";
                            break;
                    }

                    switch (temp) {
                        case RATE:
                            String rate = String.format("%.2f", body.benefitRate);
                            String split[] = rate.split("\\.");
                            if (split.length > 1) {
                                float value = Float.valueOf(split[1]);
                                if (value == 0f) {
                                    rate = split[0];
                                }
                            }
                            amount = max + rate +"% "+benefit;
                            break;
                        case UNIT:
                            amount = ElseUtils.getDecimalFormat(body.benefitAmountUnit) +"원당 " + max + ElseUtils.getDecimalFormat((long)((float)body.benefitAmount)) + benefit;
                            break;
                        case AMOUNT:
                            amount = max + ElseUtils.getDecimalFormat((long)((float)body.benefitAmount)) + benefit;
                            break;
                    }

                    content = content + pre + amount;
                }
                switch (type) {
                    case 1: //discount
                        holder.mDiscount.setText(content);
                        holder.mDiscount.setVisibility(View.VISIBLE);
                        break;
                    case 2: // point
                        holder.mPoint.setText(content);
                        holder.mPoint.setVisibility(View.VISIBLE);
                        break;
                    case 3: // mileage
                        holder.mMileage.setText(content);
                        holder.mMileage.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }

        return convertView;
    }

    private class ViewHolder {
        private TextView mTitle;
        private TextView mDiscount;
        private TextView mPoint;
        private TextView mMileage;
    }
}
