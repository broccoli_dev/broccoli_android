package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetCredit;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCardBill;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.HashMap;


public class AdtExpListViewAssetCredit extends BaseExpandableListAdapter {

    public static final int CARD_NORMAL = 0;
    public static final int CARD_LOAN = 1;
    final int MAX_VIEW_TYPE = 2;

    private ArrayList<Level2ActivityAssetCredit.GroupData> mGroupList;
	private HashMap<String, ArrayList<ResponseAssetCardBill>> mChildList;
	private Activity mActivity;
    private LayoutInflater mInflater;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtExpListViewAssetCredit(Activity _activity, ArrayList<Level2ActivityAssetCredit.GroupData> groupList, HashMap<String, ArrayList<ResponseAssetCardBill>> childList) {
		mActivity 		= _activity;
        mGroupList = groupList;
		mChildList = childList;
        mInflater = mActivity.getLayoutInflater();
	}

    @Override
    public Level2ActivityAssetCredit.GroupData getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mGroupList.size();
    }

    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){

        int groupType = getGroupType(groupPosition);

        GroupHolder groupHolder = null;
        if(convertView == null){
            groupHolder = new GroupHolder();
            if (groupType == CARD_NORMAL) {
                convertView = mInflater.inflate(R.layout.row_expandable_listview_item_group_asset_credit, parent, false);
                groupHolder.mGroupTitle = (TextView)convertView.findViewById(R.id.tv_row_expandable_listview_item_group_credit_title);
                groupHolder.mTotalValue = (TextView)convertView.findViewById(R.id.tv_row_expandable_listview_item_group_credit_total_value);
                groupHolder.mId = groupType;
                convertView.setTag(groupHolder);
            } else {
                convertView = mInflater.inflate(R.layout.row_list_item_honest_fund, parent, false);
                groupHolder.mId = groupType;
                convertView.setTag(groupHolder);
            }
        }else{
            groupHolder = (GroupHolder)convertView.getTag();
            if (groupHolder.mId != groupType) {
                if (groupType == CARD_NORMAL) {
                    convertView = mInflater.inflate(R.layout.row_expandable_listview_item_group_asset_credit, parent, false);
                    groupHolder.mGroupTitle = (TextView)convertView.findViewById(R.id.tv_row_expandable_listview_item_group_credit_title);
                    groupHolder.mTotalValue = (TextView)convertView.findViewById(R.id.tv_row_expandable_listview_item_group_credit_total_value);
                    groupHolder.mId = groupType;
                    convertView.setTag(groupHolder);
                } else {
                    convertView = mInflater.inflate(R.layout.row_list_item_honest_fund, parent, false);
                    groupHolder.mId = groupType;
                    convertView.setTag(groupHolder);
                }
            }
        }

        if (groupType == CARD_NORMAL) {
            long value = 0;
            ArrayList<ResponseAssetCardBill> list = mChildList.get(getGroup(groupPosition).title);
            if (list != null && list.size() > 0) {
                for (ResponseAssetCardBill cardBill : list) {
                    value += cardBill.payAmount;
                }
            }

            groupHolder.mGroupTitle.setText(ElseUtils.getYearMonthDayString(getGroup(groupPosition).title));
            groupHolder.mTotalValue.setText(ElseUtils.getDecimalFormat(value));

        }

        return convertView;
    }

    @Override
    public int getGroupType(int groupPosition) {
        return getGroup(groupPosition).mType;
    }

    @Override
    public int getGroupTypeCount() {
        return MAX_VIEW_TYPE;
    }

    //===============================================================================//
// Child
//===============================================================================//

    @Override
    public ResponseAssetCardBill getChild(int groupPosition, int childPosition){
        return mChildList.get(getGroup(groupPosition).title).get(childPosition);
    }

    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(getGroup(groupPosition).title).size();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        ViewHolder viewHolder;

        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_list_item_asset_credit, parent, false);
            viewHolder.mCreditCardName = (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_credit_card_name);
            viewHolder.mCreditCost = (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_credit_cost);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        ResponseAssetCardBill cardBill = getChild(groupPosition, childPosition);

        if(cardBill.billType != null && !cardBill.billType.equals("")){
            String cardName = TransFormatUtils.transCardCodeToName(cardBill.companyCode)
                    + "(" + cardBill.billType + ")";
            viewHolder.mCreditCardName.setText(cardName);
        }
        else {
            viewHolder.mCreditCardName.setText(TransFormatUtils.transCardCodeToName(cardBill.companyCode));
        }

        viewHolder.mCreditCost.setText(ElseUtils.getDecimalFormat(cardBill.payAmount));
        if (childPosition == mChildList.size()-1) ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return convertView;
    }

    @Override
    public boolean hasStableIds(){return true;}

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition){return true;}
    
    
//====================================================================//
// ViewHolder
//====================================================================//

    private class GroupHolder {
        private TextView    mGroupTitle;
        private TextView    mTotalValue;
        private int         mId;
    }

    private class ViewHolder {
    	private TextView        mCreditCardName;
    	private TextView        mCreditCost;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<Level2ActivityAssetCredit.GroupData> groupList, HashMap<String, ArrayList<ResponseAssetCardBill>> array){
        mGroupList = groupList;
    	mChildList = array;
	}
    
     
}
