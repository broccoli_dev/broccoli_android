package com.nomadconnection.broccoli.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 4. 18..
 */

public class CommonPagerAdapter extends PagerAdapter {

    private List<?> mList;
    private PagerItemCreateListener mListener;

    public interface PagerItemCreateListener {
        Object instantiateItem(ViewGroup container, int position);
        void destroyItem(ViewGroup container, int position, Object object);
    }

    public CommonPagerAdapter(PagerItemCreateListener listener) {
        mListener = listener;
    }

    public void setData(List<?> list) {
        mList = list;
    }

    public List<?> getData() {
        return mList;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mList != null) {
            count = mList.size();
        }
        return count;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if (mListener != null) {
            return mListener.instantiateItem(container, position);
        } else {
            return super.instantiateItem(container, position);
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (mListener != null) {
            mListener.destroyItem(container, position, object);
        } else {
            super.destroyItem(container, position, object);
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View pager, Object obj) {
        return pager == obj;
    }
}
