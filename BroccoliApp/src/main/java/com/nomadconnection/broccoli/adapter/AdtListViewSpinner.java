package com.nomadconnection.broccoli.adapter;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

public class AdtListViewSpinner extends BaseAdapter{
	private LayoutInflater							mInflater;
	private ArrayList<String>						mData;
	private int										mPosition;
	private Dialog									mDialog;
	
	public AdtListViewSpinner(Dialog _dialog, ArrayList<String> _data, int _pos) {
		mDialog			= _dialog;
		mData			= _data;
		mPosition		= _pos;
		mInflater 		= (LayoutInflater)mDialog.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;

		if(convertView == null){
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_list_item_dlg_spinner, parent, false);

			/*위젯생성*/
			viewHolder.holderTv = (TextView)convertView.findViewById(R.id.tv_row_list_item_spinner_text);

			convertView.setTag(viewHolder);

		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}

		viewHolder.holderTv.setText(mData.get(position));
		if (position == mPosition) {
			viewHolder.holderTv.setSelected(true);
		}
		else{
			viewHolder.holderTv.setSelected(false);
		}
		
		return convertView;

	}
	
	private class ViewHolder{
		public TextView			holderTv;
	}

}
