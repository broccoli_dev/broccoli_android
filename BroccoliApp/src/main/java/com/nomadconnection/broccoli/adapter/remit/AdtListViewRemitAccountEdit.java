package com.nomadconnection.broccoli.adapter.remit;

import android.app.Activity;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Spend.SpendMonthlyInfo;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.List;


public class AdtListViewRemitAccountEdit extends BaseAdapter {

	private ArrayList<RemitAccount> mAlDataList;
	private Activity			  		mActivity;
    private View.OnClickListener    mOnClickListener;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewRemitAccountEdit(Activity _activity, ArrayList<RemitAccount> _AlDataList, View.OnClickListener _OnClickListener) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
        mOnClickListener = _OnClickListener;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }
 
    @Override
    public RemitAccount getItem(int position) {
        RemitAccount info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_remit_edit, parent, false);

            listRow.mRemitAccountName       = (TextView) convertView.findViewById(R.id.tv_row_list_item_remit_edit_bank_name);
            listRow.mRemitAccountSubName	= (TextView) convertView.findViewById(R.id.tv_row_list_item_remit_edit_bank_name_sub);
            listRow.mRemitAccountNumber	    = (TextView) convertView.findViewById(R.id.tv_row_list_item_remit_edit_account_number);
            listRow.mRemitAccountCost		= (TextView) convertView.findViewById(R.id.tv_row_list_item_remit_edit_bank_cost);
            listRow.mRemitAccountTime		= (TextView) convertView.findViewById(R.id.tv_row_list_item_remit_edit_time);
            listRow.mRemitAccountDel        = (Button) convertView.findViewById(R.id.btn_row_list_item_remit_edit_del);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        RemitAccount mRemitAccount = mAlDataList.get(position);
        listRow.mRemitAccountName.setText(TransFormatUtils.transBankCodeToName(mRemitAccount.getCompanyCode()));
        listRow.mRemitAccountSubName.setText(mRemitAccount.getAccountName());
        listRow.mRemitAccountNumber.setText(mRemitAccount.getAccountNumber());
        if(mRemitAccount.getBalance() != null){
            listRow.mRemitAccountCost.setText(ElseUtils.getStringDecimalFormat(mRemitAccount.getBalance()));
        }

        listRow.mRemitAccountTime.setText(ElseUtils.getDate(mRemitAccount.getStandardDate()));
        listRow.mRemitAccountDel.setTag(mRemitAccount);
        listRow.mRemitAccountDel.setOnClickListener(mOnClickListener);
        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
        private TextView        mRemitAccountName;
    	private TextView		mRemitAccountSubName;
    	private TextView		mRemitAccountNumber;
    	private TextView		mRemitAccountCost;
    	private TextView		mRemitAccountTime;
        private Button          mRemitAccountDel;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<RemitAccount> array){
    	mAlDataList = array;
	}
    

}
