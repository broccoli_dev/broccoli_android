package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level4ActivityAssetStockNewsy;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockInfo;
import com.nomadconnection.broccoli.data.Investment.InvestNewsyStockData;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

public class AdtExpListViewAssetStock extends BaseExpandableListAdapter {

    private Activity mContext;
    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<BodyAssetStockInfo>> mChildList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;
    private boolean isStockPageState = false;

    public AdtExpListViewAssetStock(Activity c, ArrayList<String> groupList, ArrayList<ArrayList<BodyAssetStockInfo>> childList, boolean stockPageState){
        super();
        mContext = c;
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
        this.isStockPageState = stockPageState;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_group_asset_stock, parent, false);
            mViewHolder.mGroupTitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_asset_stock_title);
            mViewHolder.mGroupTitleLayout = (LinearLayout)v.findViewById(R.id.ll_row_expandable_listview_item_group_asset_stock_title_layout);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        mViewHolder.mGroupTitle.setText(getGroup(groupPosition));
        if (getChildrenCount(groupPosition) == 0) mViewHolder.mGroupTitleLayout.setVisibility(View.GONE);

        return v;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public BodyAssetStockInfo getChild(int groupPosition, int childPosition){
//    	public String getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_asset_stock, null);
            mViewHolder.mLayout1 = (LinearLayout) v.findViewById(R.id.ll_row_expandable_listview_item_child_asset_stock_layout_1);
            mViewHolder.mLayout2 = (LinearLayout) v.findViewById(R.id.ll_row_expandable_listview_item_child_asset_stock_layout_2);
            mViewHolder.mLayout1Info1 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_stock_layout_1_info_1);
            mViewHolder.mLayout1Info2 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_stock_layout_1_info_2);
            mViewHolder.mLayout1Cost = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_stock_layout_1_cost);
            mViewHolder.mLayout2Info1 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_stock_layout_2_info_1);
            mViewHolder.mLayout2Info2 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_stock_layout_2_info_2);
            mViewHolder.mLayout1Newsy = (ImageView) v.findViewById(R.id.iv_row_expandable_list_item_child_layout1_newsy);
            mViewHolder.mLayout2Newsy = (ImageView) v.findViewById(R.id.iv_row_expandable_list_item_child_layout2_newsy);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        if (!getChild(groupPosition, childPosition).mBodyAssetStockSum.toString().equals("0")){
            mViewHolder.mLayout1.setVisibility(View.VISIBLE);
            mViewHolder.mLayout2.setVisibility(View.GONE);
            mViewHolder.mLayout1Info1.setText(getChild(groupPosition, childPosition).mBodyAssetStockName);
            mViewHolder.mLayout1Info2.setText("총 평가액");
            mViewHolder.mLayout1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(getChild(groupPosition, childPosition).mBodyAssetStockSum));
            InvestNewsyStockData data = getChild(groupPosition, childPosition).mBodyNewsy;
            if (data != null && data.getTotal() != null) {
                String total = data.getTotal();
                int value = Integer.parseInt(total);
                if (value <= 19) {
                    mViewHolder.mLayout1Newsy.setImageResource(R.drawable.selector_button_weather_btn_01);
                } else if (value <= 39) {
                    mViewHolder.mLayout1Newsy.setImageResource(R.drawable.selector_button_weather_btn_02);
                } else if (value <= 59) {
                    mViewHolder.mLayout1Newsy.setImageResource(R.drawable.selector_button_weather_btn_03);
                } else if (value <= 79) {
                    mViewHolder.mLayout1Newsy.setImageResource(R.drawable.selector_button_weather_btn_04);
                } else {
                    mViewHolder.mLayout1Newsy.setImageResource(R.drawable.selector_button_weather_btn_05);
                }
                mViewHolder.mLayout1Newsy.setVisibility(View.VISIBLE);
            } else {
                mViewHolder.mLayout1Newsy.setVisibility(View.GONE);
            }
            mViewHolder.mLayout1Newsy.setTag(getChild(groupPosition, childPosition));
            mViewHolder.mLayout1Newsy.setOnClickListener(onNewsyClickListener);
        } else {
            mViewHolder.mLayout1.setVisibility(View.GONE);
            mViewHolder.mLayout2.setVisibility(View.VISIBLE);
            mViewHolder.mLayout2Info1.setText(getChild(groupPosition, childPosition).mBodyAssetStockName);
            mViewHolder.mLayout2Info2.setText("전량매도");
            InvestNewsyStockData data = getChild(groupPosition, childPosition).mBodyNewsy;
            if (data != null && data.getTotal() != null) {
                String total = data.getTotal();
                int value = Integer.parseInt(total);
                if (value <= 19) {
                    mViewHolder.mLayout2Newsy.setImageResource(R.drawable.selector_button_weather_btn_01);
                } else if (value <= 39) {
                    mViewHolder.mLayout2Newsy.setImageResource(R.drawable.selector_button_weather_btn_02);
                } else if (value <= 59) {
                    mViewHolder.mLayout2Newsy.setImageResource(R.drawable.selector_button_weather_btn_03);
                } else if (value <= 79) {
                    mViewHolder.mLayout2Newsy.setImageResource(R.drawable.selector_button_weather_btn_04);
                } else {
                    mViewHolder.mLayout2Newsy.setImageResource(R.drawable.selector_button_weather_btn_05);
                }
                mViewHolder.mLayout2Newsy.setVisibility(View.VISIBLE);
            } else {
                mViewHolder.mLayout2Newsy.setVisibility(View.GONE);
            }
            mViewHolder.mLayout2Newsy.setTag(getChild(groupPosition, childPosition));
            mViewHolder.mLayout2Newsy.setOnClickListener(onNewsyClickListener);
        }

        if (isLastChild) ((View)v.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)v.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return v;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
    private View.OnClickListener onNewsyClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((BaseActivity)mContext).sendTracker(AnalyticsName.AssetStockListNewsy);
            BodyAssetStockInfo info = (BodyAssetStockInfo) v.getTag();
            Intent intent = new Intent(mContext, Level4ActivityAssetStockNewsy.class);
            intent.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER, info);
            if (isStockPageState) {
                intent.putExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND, isStockPageState);
            }
            mContext.startActivity(intent);
        }
    };
 
    class ViewHolder{
    	/* group */
        public LinearLayout mGroupTitleLayout;
        public TextView mGroupTitle;

        /* child */
        public LinearLayout mLayout1;
        public LinearLayout mLayout2;

        public TextView mLayout1Info1;
        public TextView mLayout1Info2;
        public TextView mLayout1Cost;
        public TextView mLayout2Info1;
        public TextView mLayout2Info2;
        public ImageView mLayout1Newsy;
        public ImageView mLayout2Newsy;
    }


    public void setData(ArrayList<String> groupList, ArrayList<ArrayList<BodyAssetStockInfo>> childList) {
        this.mGroupList = groupList;
        this.mChildList = childList;
        notifyDataSetChanged();
    }
}
