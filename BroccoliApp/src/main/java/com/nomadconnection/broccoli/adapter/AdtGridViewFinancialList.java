package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2017. 5. 18..
 */

public class AdtGridViewFinancialList extends BaseAdapter {

    private Context mContext;
    private ArrayList<SelectBankData> mList;
    private LayoutInflater mInflater;
    private View.OnClickListener mListener;

    private TypedArray mBankRes;
    private TypedArray mCardRes;
    private TypedArray mBankDisableRes;
    private TypedArray mCardDisableRes;

    private int normalEdge = 0;
    private int duplicateEdge = 0;

    public AdtGridViewFinancialList(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        init(context);
    }

    public AdtGridViewFinancialList(Context context, View.OnClickListener onItemClickListener) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mListener = onItemClickListener;
        init(context);
    }

    private void init(Context context) {
        mBankRes = context.getResources().obtainTypedArray(R.array.bank_name_image);
        mCardRes = context.getResources().obtainTypedArray(R.array.card_name_image);
        mBankDisableRes = context.getResources().obtainTypedArray(R.array.bank_name_image_dim_20);
        mCardDisableRes = context.getResources().obtainTypedArray(R.array.card_name_image_dim_20);
        normalEdge = (int) ElseUtils.getDiptoPx(1);
        duplicateEdge = (int) ElseUtils.getDiptoPx(1)/2;
        if (normalEdge == 1) {
            normalEdge = 2;
            duplicateEdge = 1;
        }
    }

    public void setData(ArrayList<SelectBankData> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mList != null) {
            count = mList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int i) {
        Object object = null;
        if (mList != null) {
            object = mList.get(i);
        }
        return object;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_grid_item_financial, null);
            viewHolder.mBankImage = (ImageView) convertView.findViewById(R.id.iv_row_grid_item_bank_image);
            viewHolder.mCash = (TextView) convertView.findViewById(R.id.tv_row_grid_item_cash);
            viewHolder.mSelectImage = (ImageView) convertView.findViewById(R.id.iv_row_grid_item_selector);
            viewHolder.mEdge = (ImageView) convertView.findViewById(R.id.iv_row_grid_item_edge);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String type = mList.get(position).getType();

        if (SelectBankData.TYPE_BANK.equalsIgnoreCase(type) ||
                SelectBankData.TYPE_CARD.equalsIgnoreCase(type)) {
            int currentPosition = position % 3;
            int lastCount = mList.size() - mList.size()%3;
            int left = normalEdge;
            int right = normalEdge;
            int top = normalEdge;
            int bottom = normalEdge;
            switch (currentPosition) {
                case 0:
                    if (position < 3) {
                        right = duplicateEdge;
                        bottom = duplicateEdge;
                    } else if (position < lastCount) {
                        top = duplicateEdge;
                        right = duplicateEdge;
                        bottom = duplicateEdge;
                    } else if (lastCount < position && position < mList.size()-1) {
                        top = duplicateEdge;
                        right = duplicateEdge;
                    } else {
                        top = duplicateEdge;
                    }
                    break;
                case 1:
                    if (position < 3) {
                        left = duplicateEdge;
                        right = duplicateEdge;
                        bottom = duplicateEdge;
                    } else if (position < lastCount) {
                        left = duplicateEdge;
                        top = duplicateEdge;
                        right = duplicateEdge;
                        bottom = duplicateEdge;
                    } else if (lastCount < position && position < mList.size()-1) {
                        left = duplicateEdge;
                        top = duplicateEdge;
                        right = duplicateEdge;
                    } else {
                        left = duplicateEdge;
                        top = duplicateEdge;
                    }
                    break;
                case 2:
                    if (position < 3) {
                        left = duplicateEdge;
                        bottom = duplicateEdge;
                    } else if (position < lastCount) {
                        left = duplicateEdge;
                        top = duplicateEdge;
                        bottom = duplicateEdge;
                    } else if (lastCount < position && position < mList.size()-1) {
                        left = duplicateEdge;
                        top = duplicateEdge;
                    } else {
                        left = duplicateEdge;
                        top = duplicateEdge;
                    }
                    break;
            }
            viewHolder.mEdge.setPadding(left, top, right, bottom);
        }

        if(SelectBankData.TYPE_BANK.equalsIgnoreCase(type)){
            viewHolder.mBankImage.setVisibility(View.VISIBLE);
            viewHolder.mCash.setVisibility(View.GONE);
            int imageId = mList.get(position).getNameId();

            // or set you ImageView's resource to the id
            viewHolder.mBankImage.setImageResource(mBankRes.getResourceId(imageId, -1));
        }
        else if(SelectBankData.TYPE_CARD.equalsIgnoreCase(type)){
            viewHolder.mBankImage.setVisibility(View.VISIBLE);
            viewHolder.mCash.setVisibility(View.GONE);
            int imageId = mList.get(position).getNameId();

            // or set you ImageView's resource to the id
            viewHolder.mBankImage.setImageResource(mCardRes.getResourceId(imageId, -1));
        }
        else {
            viewHolder.mBankImage.setVisibility(View.GONE);
            viewHolder.mCash.setVisibility(View.VISIBLE);
            viewHolder.mCash.setText(mList.get(position).getName());
        }

        int status = mList.get(position).getStatus();
        switch (status) {
            case SelectBankData.NONE:
                viewHolder.mSelectImage.setVisibility(View.INVISIBLE);
                break;
            case SelectBankData.SELECTED:
                viewHolder.mSelectImage.setVisibility(View.VISIBLE);
                break;
            case SelectBankData.COMPLETE:
                if (SelectBankData.TYPE_BANK.equalsIgnoreCase(type)) {
                    viewHolder.mBankImage.setImageResource(mBankDisableRes.getResourceId(mList.get(position).getNameId(), -1));
                } else if (SelectBankData.TYPE_CARD.equalsIgnoreCase(type)){
                    viewHolder.mBankImage.setImageResource(mCardDisableRes.getResourceId(mList.get(position).getNameId(), -1));
                } else {
                    viewHolder.mCash.setTextColor(viewHolder.mCash.getResources().getColor(R.color.subitem_left_txt_dim_color));
                }
                break;
        }
        return convertView;
    }

    public class ViewHolder {
        ImageView mBankImage;
        TextView mCash;
        ImageView mSelectImage;
        ImageView mEdge;
    }
}
