package com.nomadconnection.broccoli.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 3. 10..
 */

public class CommonFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private List<?> mList;
    private FragmentPagerItemCreateListener mListener;

    public interface FragmentPagerItemCreateListener {
        Fragment getItem(List<?> list, int position);
        CharSequence getPageTitle(int position);
    }

    public CommonFragmentPagerAdapter(FragmentManager fm, FragmentPagerItemCreateListener listener) {
        super(fm);
        mListener = listener;
    }

    public void setData(List<?> list) {
        mList = list;
    }

    public List<?> getData() {
        return mList;
    }

    @Override
    public Fragment getItem(int position) {
        return mListener.getItem(mList, position);
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mList != null) {
            count = mList.size();
        }
        return count;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mListener.getPageTitle(position);
    }
}
