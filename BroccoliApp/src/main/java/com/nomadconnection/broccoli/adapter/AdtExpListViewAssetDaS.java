package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.AssetBankInfo;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

public class AdtExpListViewAssetDaS extends BaseExpandableListAdapter {

    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<AssetBankInfo>> mChildList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;

    public AdtExpListViewAssetDaS(Context c, ArrayList<String> groupList, ArrayList<ArrayList<AssetBankInfo>> childList){
        super();
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;

        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_group_asset_das, parent, false);
            mViewHolder.mGroupTitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_asset_das_title);
            mViewHolder.mGroupCount = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_asset_das_title_count);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        mViewHolder.mGroupTitle.setText(TransFormatUtils.transBankCodeToName(getGroup(groupPosition)));
        mViewHolder.mGroupCount.setText(String.valueOf(getChildrenCount(groupPosition)));

        return v;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public AssetBankInfo getChild(int groupPosition, int childPosition){
//    	public String getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;

        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_asset_das, null);

            mViewHolder.mBankName			= (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_das_bank_name);
//            mViewHolder.mBankAccountName	= (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_das_bank_account_name);
            mViewHolder.mBankAccountNum		= (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_das_bank_account_num);
            mViewHolder.mAlertIcon			= (ImageView) v.findViewById(R.id.iv_row_expandable_listview_item_child_asset_das_alert_icon);
            mViewHolder.mBankCost		= (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_das_bank_cost);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        AssetBankInfo bankInfo = getChild(groupPosition, childPosition);

        mViewHolder.mBankName.setText(bankInfo.mAssetBankAccountName);
        mViewHolder.mBankAccountNum.setText(bankInfo.mAssetBankAccountNumber);
        mViewHolder.mAlertIcon.setVisibility(View.GONE);
        mViewHolder.mBankCost.setText(TransFormatUtils.getDecimalFormatRecvString(bankInfo.mAssetBankSum));

        if (isLastChild) ((View)v.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)v.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return v;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
 
 
    class ViewHolder{
    	/* group */
        public LinearLayout mGroup;
        public TextView mGroupTitle;
        public TextView mGroupCount;

        /* child */
        private TextView		mBankName;
//    	private TextView		mBankAccountName;
        private TextView		mBankAccountNum;
        private ImageView      mAlertIcon;
        private TextView		mBankCost;
    }
}
