package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.InfoDataMain4;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

/*
 * Card Use : g, h
 */

public class AdtListViewDemoEtc extends BaseAdapter {

	private ArrayList<InfoDataMain4> 	mAlDataList;
	private Activity			  		mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewDemoEtc(Activity activity, ArrayList<InfoDataMain4> arrayList) {
		mActivity 		= activity;
		mAlDataList		= arrayList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public InfoDataMain4 getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_main_4, parent, false);
            init(listRow, convertView);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }

        
        /* Data 적용 */
        dataSet(listRow, position);
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// Init
//====================================================================//
    private void init(ViewHolder _listRow, View _convertView){

		_convertView.findViewById(R.id.inc_card_h).setVisibility(View.GONE);
		_listRow.mMain_4_Margin = (FrameLayout)_convertView.findViewById(R.id.fl_row_item_main_4_margin);
    	
    	
    	/* Card E */
    	_listRow.mCardELayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_e);
    	_listRow.mCardETitle = (TextView)_listRow.mCardELayout.findViewById(R.id.tv_row_item_inc_home_card_e_title);
    	_listRow.mCardEIcon = (ImageView)_listRow.mCardELayout.findViewById(R.id.iv_row_item_inc_home_card_e_icon);

    	
    	/* Card G */
    	_listRow.mCardGLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_g);
    	_listRow.mCardGTitle = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_title);
		_listRow.mCardGTitleCount = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_title_count);
    	_listRow.mCardGIcon = (ImageView)_listRow.mCardGLayout.findViewById(R.id.iv_row_item_inc_home_card_g_icon);
		_listRow.mCardGSubLayout1 = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_g_sub_layout_1);
    	_listRow.mCardGSubInfo1date1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_date_1);
    	_listRow.mCardGSubInfo1date2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_date_2);
    	_listRow.mCardGSubInfo1Txt1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_txt_1);
    	_listRow.mCardGSubInfo1Txt1Tag = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_txt_1_tag);
    	_listRow.mCardGSubInfo1Txt2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_txt_2);
    	_listRow.mCardGSubInfo1Txt3 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_txt_3);
    	_listRow.mCardGSubInfo1Cost = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_1_cost);
		_listRow.mCardGSubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardGSubLayout2 = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_g_sub_layout_2);
    	_listRow.mCardGSubInfo2date1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_date_1);
    	_listRow.mCardGSubInfo2date2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_date_2);
    	_listRow.mCardGSubInfo2Txt1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_txt_1);
    	_listRow.mCardGSubInfo2Txt1Tag = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_txt_1_tag);
    	_listRow.mCardGSubInfo2Txt2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_txt_2);
    	_listRow.mCardGSubInfo2Txt3 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_txt_3);
    	_listRow.mCardGSubInfo2Cost = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_2_cost);
		_listRow.mCardGSubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardGSubLayout3 = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_g_sub_layout_3);
    	_listRow.mCardGSubInfo3date1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_date_1);
    	_listRow.mCardGSubInfo3date2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_date_2);
    	_listRow.mCardGSubInfo3Txt1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_txt_1);
    	_listRow.mCardGSubInfo3Txt1Tag = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_txt_1_tag);
    	_listRow.mCardGSubInfo3Txt2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_txt_2);
    	_listRow.mCardGSubInfo3Txt3 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_txt_3);
    	_listRow.mCardGSubInfo3Cost = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_3_cost);
		_listRow.mCardGSubInfo3Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardGSubLayout4 = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_g_sub_layout_4);
    	_listRow.mCardGSubInfo4date1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_date_1);
    	_listRow.mCardGSubInfo4date2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_date_2);
    	_listRow.mCardGSubInfo4Txt1 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_txt_1);
    	_listRow.mCardGSubInfo4Txt1Tag = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_txt_1_tag);
    	_listRow.mCardGSubInfo4Txt2 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_txt_2);
    	_listRow.mCardGSubInfo4Txt3 = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_txt_3);
    	_listRow.mCardGSubInfo4Cost = (TextView)_listRow.mCardGLayout.findViewById(R.id.tv_row_item_inc_home_card_g_sub_info_4_cost);
		_listRow.mCardGSubInfo4Cost.setTypeface(FontClass.setFont(mActivity));

		_listRow.mCardHLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_f);
		_listRow.mChallengeTitle= (TextView) _listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_title);
		_listRow.mChallengeLTitleCount = (TextView) _listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_title_count);
		_listRow.mChallengeProgress = (ProgressBar) _listRow.mCardHLayout.findViewById(R.id.progressbar_row_item_inc_home_card_h);
		_listRow.mChallengePercentValue = (TextView) _listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_percent_value);
		_listRow.mChallengePercent = (TextView) _listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_percent);
		_listRow.mChallengeAccount = (TextView) _listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_sub_info_title);
		_listRow.mChallengeCost = (TextView) _listRow.mCardHLayout.findViewById(R.id.tv_row_item_inc_home_card_h_sub_info_cost);
		_listRow.mChallengeCost.setTypeface(FontClass.setFont(mActivity));
    }
    
    
    
    
//====================================================================//
// Data Set
//====================================================================//
    private void dataSet(final ViewHolder _listRow, int _position){
		
		if (mAlDataList.get(_position).mTxt1.equals("가장 인기있는 챌린지")){
			if (mAlDataList.get(_position).mTxt2.equals("y")) {
				_listRow.mCardGLayout.setVisibility(View.GONE);
				_listRow.mMain_4_Margin.setVisibility(View.VISIBLE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardHLayout.setVisibility(View.VISIBLE);

				long current_value = Long.valueOf(mAlDataList.get(_position).mTxt5);
				long need_value = Long.valueOf(mAlDataList.get(_position).mTxt4);
				int value = (int)( (double)current_value / (double)need_value * 100.0 );

				if (current_value < 0) {
					value = 0;
				}

				if(value > 100){
					value = 100;
				}

				if(value < 31){
					_listRow.mChallengePercentValue.setText(Integer.toString(value));
					_listRow.mChallengePercentValue.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_one_section));
					_listRow.mChallengePercent.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_one_section));
					_listRow.mChallengeProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_one_section_circle));
					_listRow.mChallengeProgress.setProgress(value);
				}
				else if(value < 81){
					_listRow.mChallengePercentValue.setText(Integer.toString(value));
					_listRow.mChallengePercentValue.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_two_section));
					_listRow.mChallengePercent.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_two_section));
					_listRow.mChallengeProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_two_section_circle));
					_listRow.mChallengeProgress.setProgress(value);
				}
				else {
					_listRow.mChallengePercentValue.setText(Integer.toString(value));
					_listRow.mChallengePercentValue.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_three_section));
					_listRow.mChallengePercent.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_three_section));
					_listRow.mChallengeProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_three_section_circle));
					_listRow.mChallengeProgress.setProgress(value);
				}
				_listRow.mChallengeTitle.setText("가장 인기있는 챌린지");
				_listRow.mChallengeLTitleCount.setVisibility(View.GONE);
				_listRow.mChallengeAccount.setText(mAlDataList.get(_position).mTxt3);
				_listRow.mChallengeCost.setText(ElseUtils.getDecimalFormat(current_value));
			}
		}
		else if (mAlDataList.get(_position).mTxt1.equals("머니 캘린더")){
			if (mAlDataList.get(_position).mTxt2.equals("y")) {
				_listRow.mCardGLayout.setVisibility(View.VISIBLE);
				_listRow.mMain_4_Margin.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardGTitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardGTitleCount.setVisibility(View.GONE);
				_listRow.mCardGIcon.setBackgroundResource(R.drawable.home_icon_12);
				_listRow.mCardHLayout.setVisibility(View.GONE);


				_listRow.mCardGSubLayout1.setVisibility(View.VISIBLE);
				_listRow.mCardGSubLayout2.setVisibility(View.VISIBLE);
				_listRow.mCardGSubLayout3.setVisibility(View.VISIBLE);
				_listRow.mCardGSubLayout4.setVisibility(View.GONE);
				_listRow.mCardGSubInfo1date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
				_listRow.mCardGSubInfo1date1.setText("D-"+"01");
				_listRow.mCardGSubInfo1date2.setText("");
				_listRow.mCardGSubInfo1date2.setVisibility(View.GONE);
				_listRow.mCardGSubInfo1Txt1.setText(mAlDataList.get(_position).mTxt3);
				_listRow.mCardGSubInfo1Txt1Tag.setVisibility(View.GONE);
				_listRow.mCardGSubInfo1Txt2.setVisibility(View.GONE);
				_listRow.mCardGSubInfo1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt4));
				_listRow.mCardGSubInfo1Txt3.setText("예상");

				_listRow.mCardGSubInfo2date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
				_listRow.mCardGSubInfo2date1.setText("D-"+"02");
				_listRow.mCardGSubInfo2date2.setText("");
				_listRow.mCardGSubInfo2date2.setVisibility(View.GONE);
				_listRow.mCardGSubInfo2Txt1.setText(mAlDataList.get(_position).mTxt5);
				_listRow.mCardGSubInfo2Txt1Tag.setVisibility(View.GONE);
				_listRow.mCardGSubInfo2Txt2.setVisibility(View.GONE);
				_listRow.mCardGSubInfo2Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt6));
				_listRow.mCardGSubInfo2Txt3.setText("예상");

				_listRow.mCardGSubInfo3date1.setTextAppearance(mActivity, R.style.CS_Text_18_153153153);
				_listRow.mCardGSubInfo3date1.setText("D-"+"21");
				_listRow.mCardGSubInfo3date2.setText("");
				_listRow.mCardGSubInfo3date2.setVisibility(View.GONE);
				_listRow.mCardGSubInfo3Txt1.setText(mAlDataList.get(_position).mTxt7);
				_listRow.mCardGSubInfo3Txt1Tag.setVisibility(View.GONE);
				_listRow.mCardGSubInfo3Txt2.setVisibility(View.GONE);
				_listRow.mCardGSubInfo3Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt8));
				_listRow.mCardGSubInfo3Txt3.setText("예상");
			}
			else{
				_listRow.mMain_4_Margin.setVisibility(View.GONE);
				_listRow.mCardGLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
			}
		}
    }
    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {

		private FrameLayout mMain_4_Margin;
    	
    	/* Card E */
    	private LinearLayout 	mCardELayout;
    	private TextView		mCardETitle;
    	private ImageView 		mCardEIcon;

    	
    	/* Card G */
    	private LinearLayout 	mCardGLayout;
    	private TextView		mCardGTitle;
		private TextView		mCardGTitleCount;
    	private ImageView 		mCardGIcon;
		private LinearLayout 	mCardGSubLayout1;
    	private TextView		mCardGSubInfo1date1;
    	private TextView		mCardGSubInfo1date2;
    	private TextView		mCardGSubInfo1Txt1;
    	private TextView		mCardGSubInfo1Txt1Tag;
    	private TextView		mCardGSubInfo1Txt2;
    	private TextView		mCardGSubInfo1Txt3;
    	private TextView		mCardGSubInfo1Cost;
		private LinearLayout 	mCardGSubLayout2;
    	private TextView		mCardGSubInfo2date1;
    	private TextView		mCardGSubInfo2date2;
    	private TextView		mCardGSubInfo2Txt1;
    	private TextView		mCardGSubInfo2Txt1Tag;
    	private TextView		mCardGSubInfo2Txt2;
    	private TextView		mCardGSubInfo2Txt3;
    	private TextView		mCardGSubInfo2Cost;
		private LinearLayout 	mCardGSubLayout3;
    	private TextView		mCardGSubInfo3date1;
    	private TextView		mCardGSubInfo3date2;
    	private TextView		mCardGSubInfo3Txt1;
    	private TextView		mCardGSubInfo3Txt1Tag;
    	private TextView		mCardGSubInfo3Txt2;
    	private TextView		mCardGSubInfo3Txt3;
    	private TextView		mCardGSubInfo3Cost;
		private LinearLayout 	mCardGSubLayout4;
    	private TextView		mCardGSubInfo4date1;
    	private TextView		mCardGSubInfo4date2;
    	private TextView		mCardGSubInfo4Txt1;
    	private TextView		mCardGSubInfo4Txt1Tag;
    	private TextView		mCardGSubInfo4Txt2;
    	private TextView		mCardGSubInfo4Txt3;
    	private TextView		mCardGSubInfo4Cost;

		private LinearLayout	mCardHLayout;
		private TextView mChallengeTitle;
		private TextView mChallengeLTitleCount;
		private ProgressBar mChallengeProgress;
		private TextView mChallengePercentValue;
		private TextView mChallengePercent;
		private TextView mChallengeAccount;
		private TextView mChallengeCost;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<InfoDataMain4> array){
    	mAlDataList = array;
	}

     
}
