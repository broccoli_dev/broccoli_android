package com.nomadconnection.broccoli.adapter.remit;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeBank;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.Remit.RemitBank;
import com.nomadconnection.broccoli.data.Remit.RemitBankDetail;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoli.data.Spend.BodySpendBudgetCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class AdtListViewRemitBankList extends BaseAdapter {

    private Context mContext;
    private ArrayList<RemitBankDetail> mAlDataList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;
    private int mSelectItem = -1;

    public AdtListViewRemitBankList(Context context, ArrayList<RemitBankDetail> list) {
        mContext = context;
        inflater = LayoutInflater.from(context);
        mAlDataList = list;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }

    @Override
    public RemitBankDetail getItem(int position) {
        RemitBankDetail info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_listview_item_remit_register, null);
            mViewHolder.mRadioButton = (RadioButton) v.findViewById(R.id.rb_row_list_item_remit_register_icon);
            mViewHolder.mRadioButton.setFocusable(false);
            mViewHolder.mRadioButton.setClickable(false);
            mViewHolder.mRadioButton.setChecked(((ListView)parent).isItemChecked(position));
            mViewHolder.mBankNameIv = (ImageView) v.findViewById(R.id.iv_row_list_item_remit_register_bank);
            mViewHolder.mReady = (TextView) v.findViewById(R.id.tv_row_list_item_remit_register_ready);
            mViewHolder.mBankNumber = (TextView)v.findViewById(R.id.tv_row_list_item_remit_register_bank_number);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        RemitBankDetail mBankItem = getItem(position);

        if(mSelectItem == position){
            if(mViewHolder.mRadioButton.isChecked()){
                mViewHolder.mRadioButton.setChecked(false);
            }
            else {
                mViewHolder.mRadioButton.setChecked(true);
            }
        }
        else {
            mViewHolder.mRadioButton.setChecked(false);
        }

        mViewHolder.mBankNumber.setText(mBankItem.getAccountNo());

        if("N".equals(mBankItem.getPossibleSend())){
            mViewHolder.mRadioButton.setEnabled(false);
            List<String> mBankCode = Arrays.asList(mContext.getResources().getStringArray(R.array.bank_code));
            for(int i = 0; i < mBankCode.size(); i++){
                if(mBankItem.getCompanyCode().equals(mBankCode.get(i))){
                    TypedArray imgs = mContext.getResources().obtainTypedArray(R.array.bank_name_image_dim);
                    // get resource ID by index
                    imgs.getResourceId(i, -1);
                    // or set you ImageView's resource to the id
                    mViewHolder.mBankNameIv.setImageResource(imgs.getResourceId(i, -1));
                    // recycle the array
                    imgs.recycle();
                }
            }
            if (Build.VERSION.SDK_INT < 23) {
                mViewHolder.mBankNumber.setTextAppearance(mContext, R.style.CS_Text_15_191191191);
            } else {
                mViewHolder.mBankNumber.setTextAppearance(R.style.CS_Text_15_191191191);
            }
            mViewHolder.mReady.setVisibility(View.VISIBLE);
        }
        else {
            mViewHolder.mRadioButton.setEnabled(true);
            List<String> mBankCode = Arrays.asList(mContext.getResources().getStringArray(R.array.bank_code));
            for(int i = 0; i < mBankCode.size(); i++){
                if(mBankItem.getCompanyCode().equals(mBankCode.get(i))){
                    TypedArray imgs = mContext.getResources().obtainTypedArray(R.array.bank_name_image);
                    // get resource ID by index
                    imgs.getResourceId(i, -1);
                    // or set you ImageView's resource to the id
                    mViewHolder.mBankNameIv.setImageResource(imgs.getResourceId(i, -1));
                    // recycle the array
                    imgs.recycle();
                }
            }
            if (Build.VERSION.SDK_INT < 23) {
                mViewHolder.mBankNumber.setTextAppearance(mContext, R.style.CS_Text_15_898989);
            } else {
                mViewHolder.mBankNumber.setTextAppearance(R.style.CS_Text_15_898989);
            }
            mViewHolder.mReady.setVisibility(View.GONE);
        }

        return v;
    }

    class ViewHolder {
        RadioButton mRadioButton;
        ImageView mBankNameIv;
        TextView mReady;
        TextView mBankNumber;
    }

//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<RemitBankDetail> array){
        mAlDataList = array;
    }

    public void setSelectItem(int _position){
        mSelectItem = _position;
    }

}
