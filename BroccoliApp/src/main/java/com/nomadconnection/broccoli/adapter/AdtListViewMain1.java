package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.AssetCardInfo;
import com.nomadconnection.broccoli.data.InfoDataMain1;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 * Card Use : a, c, d ,e
 */

public class AdtListViewMain1 extends BaseAdapter {

	private ArrayList<InfoDataMain1> 	mAlDataList;
	private Activity			  		mActivity;
	private View.OnClickListener mBtnClickListener;
	private AdapterView.OnItemClickListener mItemClickListener;
	private boolean isDemo = false;

//==========================================================================//
// constructor	
//==========================================================================//
	public AdtListViewMain1(Activity _activity, ArrayList<InfoDataMain1> _AlDataList, View.OnClickListener btnClickListener, AdapterView.OnItemClickListener _ItemClickListener) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
		mBtnClickListener = btnClickListener;
		mItemClickListener = _ItemClickListener;
	}

	public AdtListViewMain1(Activity _activity, ArrayList<InfoDataMain1> _AlDataList, AdapterView.OnItemClickListener _ItemClickListener, boolean isDemo) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
		mItemClickListener = _ItemClickListener;
		this.isDemo = true;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public InfoDataMain1 getItem(int position) {
        return mAlDataList.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_main_1, parent, false);
            init(listRow, convertView);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        dataSet(listRow, position);
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// Init
//====================================================================//
    private void init(ViewHolder _listRow, View _convertView){
    	/* Card A */
    	_listRow.mCardALayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_a);
    	_listRow.mCardATitle = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_title);
    	_listRow.mCardATitleCount = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_title_count);
    	_listRow.mCardAIcon = (ImageView)_listRow.mCardALayout.findViewById(R.id.iv_row_item_inc_home_card_a_icon);
    	_listRow.mCardACost = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_cost);
		_listRow.mCardACost.setTypeface(FontClass.setFont(mActivity));
    	_listRow.mCardASubInfo1 = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_sub_info_1);
    	_listRow.mCardASubInfo2 = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_sub_info_2);
    	
    	/* Card B */
    	_listRow.mCardBLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_b);
    	_listRow.mCardBTitle = (TextView)_listRow.mCardBLayout.findViewById(R.id.tv_row_item_inc_home_card_b_title);
		_listRow.mCardBIcon = (ImageView) _listRow.mCardBLayout.findViewById(R.id.iv_row_item_inc_home_card_b_icon);
    	_listRow.mCardBCost = (TextView)_listRow.mCardBLayout.findViewById(R.id.tv_row_item_inc_home_card_b_cost);
		_listRow.mCardBCost.setTypeface(FontClass.setFont(mActivity));
    	_listRow.mCardBList = (LinearLayout) _listRow.mCardBLayout.findViewById(R.id.tv_row_item_asset_card_bill_list);
		_listRow.mCardBContentLayout = _listRow.mCardBLayout.findViewById(R.id.ll_row_item_credit_layout);
		_listRow.mCardBEmptyLayout = _listRow.mCardBLayout.findViewById(R.id.ll_row_item_credit_empty_layout);
    	
    	/* Card C */
    	_listRow.mCardCLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_c);
    	_listRow.mCardCTitle = (TextView)_listRow.mCardCLayout.findViewById(R.id.tv_row_item_inc_home_card_c_title);
		_listRow.mCardCTitleCount = (TextView)_listRow.mCardCLayout.findViewById(R.id.tv_row_item_inc_home_card_c_title_count);
    	_listRow.mCardCIcon = (ImageView)_listRow.mCardCLayout.findViewById(R.id.iv_row_item_inc_home_card_c_icon);
    	_listRow.mCardCCost = (TextView)_listRow.mCardCLayout.findViewById(R.id.tv_row_item_inc_home_card_c_cost);
		_listRow.mCardCCost.setTypeface(FontClass.setFont(mActivity));
    	
    	/* Card D */
    	_listRow.mCardDLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_d);
    	_listRow.mCardDTitle = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_title);
		_listRow.mCardDTitleCount = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_title_count);
    	_listRow.mCardDIcon = (ImageView)_listRow.mCardDLayout.findViewById(R.id.iv_row_item_inc_home_card_d_icon);
    	_listRow.mCardDSubInfo1Cost = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_1_title_cost);
		_listRow.mCardDSubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
    	_listRow.mCardDSubInfo1Title = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_1_title);
    	_listRow.mCardDSubInfo2Cost = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_2_title_cost);
		_listRow.mCardDSubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
    	_listRow.mCardDSubInfo2Title = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_2_title);
		_listRow.mCardDSubInfo3Cost = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_3_title_cost);
		_listRow.mCardDSubInfo3Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardDSubInfo3Title = (TextView)_listRow.mCardDLayout.findViewById(R.id.tv_row_item_inc_home_card_d_sub_info_3_title);
    	
    	
    	/* Card E */
    	_listRow.mCardELayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_e);
    	_listRow.mCardETitle = (TextView)_listRow.mCardELayout.findViewById(R.id.tv_row_item_inc_home_card_e_title);
		_listRow.mCardESubTitle = (TextView) _listRow.mCardELayout.findViewById(R.id.tv_row_item_inc_home_card_e_subtitle);
    	_listRow.mCardEIcon = (ImageView)_listRow.mCardELayout.findViewById(R.id.iv_row_item_inc_home_card_e_icon);

    }
    
    
    
    
//====================================================================//
// Data Set
//====================================================================//
    private void dataSet(ViewHolder _listRow, final int _position){

		_listRow.mCardESubTitle.setVisibility(View.GONE);

		if (!isDemo) {
			if (mAlDataList.get(_position).mTxt1.equals("예금/적금")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardALayout.setVisibility(View.VISIBLE);
					_listRow.mCardBLayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardATitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardATitleCount.setText(mAlDataList.get(_position).mTxt8);
					_listRow.mCardAIcon.setBackgroundResource(R.drawable.home_icon_01);
					_listRow.mCardACost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt3));
					_listRow.mCardASubInfo1.setText(TransFormatUtils.transDateForm(mAlDataList.get(_position).mTxt4) + " " + mAlDataList.get(_position).mSubInfo1);
					_listRow.mCardASubInfo2.setTextColor(mActivity.getResources().getColor(R.color.home_card_common_sub_info_2_txt_color));
					if (mAlDataList.get(_position).mTxt5.length() != 0) _listRow.mCardASubInfo2.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt5));
					else _listRow.mCardASubInfo2.setText("-");
					_listRow.mCardALayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position+1, 0);
						}
					});
				}
				else{
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardBLayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.VISIBLE);
					_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
					_listRow.mCardELayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position+1, 0);
						}
					});
				}
			}
			else if (mAlDataList.get(_position).mTxt1.equals("신용카드")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					AssetCardInfo info = (AssetCardInfo) mAlDataList.get(_position).getData();
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardBLayout.setVisibility(View.VISIBLE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardBTitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardBIcon.setBackgroundResource(R.drawable.home_icon_02);
					_listRow.mCardBCost.setText(ElseUtils.getDecimalFormat(info.mAssetCardSum));
					_listRow.mCardBContentLayout.setVisibility(View.VISIBLE);
					_listRow.mCardBEmptyLayout.setVisibility(View.GONE);
					if (info.mAssetCardBillList == null || info.mAssetCardBillList.size() == 0) {
						_listRow.mCardBList.setVisibility(View.GONE);
					} else {
						_listRow.mCardBList.setVisibility(View.VISIBLE);
						List<HashMap<String, Long>> list = info.mAssetCardBillList;
						_listRow.mCardBList.removeAllViews();
						for (int i = 0; i < list.size(); i++) {
							HashMap<String, Long> recent = list.get(i);
							Object[] key = recent.keySet().toArray();
							View view = mActivity.getLayoutInflater().inflate(R.layout.row_item_asset_card_recent, null);
							LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
							if (i == 0) {
								params.bottomMargin = (int) ElseUtils.getDiptoPx(12);
							}
							view.setLayoutParams(params);
							TextView titleView = (TextView) view.findViewById(R.id.tv_row_item_asset_card_title);
							TextView costView = (TextView) view.findViewById(R.id.tv_row_item_asset_card_cost);
							titleView.setText("결제예정일 "+ElseUtils.getYearMonthDayString(key[0].toString()));
							costView.setText(ElseUtils.getDecimalFormat(recent.get(key[0].toString())));
							_listRow.mCardBList.addView(view);
						}
					}
                    _listRow.mCardBLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mItemClickListener.onItemClick(null, v, _position + 1, 0);
                        }
                    });
				} else {
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardBLayout.setVisibility(View.VISIBLE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardBContentLayout.setVisibility(View.GONE);
					_listRow.mCardBEmptyLayout.setVisibility(View.VISIBLE);
					_listRow.mCardBLayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
			}
			else if (mAlDataList.get(_position).mTxt1.equals("주식")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardALayout.setVisibility(View.VISIBLE);
					_listRow.mCardBLayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardATitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardATitleCount.setText(mAlDataList.get(_position).mTxt8);
					_listRow.mCardAIcon.setBackgroundResource(R.drawable.home_icon_03);
					_listRow.mCardACost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt3));
	//    			_listRow.mCardASubInfo1.setText(TransFormatUtils.transDateForm(mAlDataList.get(_position).mTxt4));
					_listRow.mCardASubInfo1.setText("전일대비 수익  (수익률)");
					_listRow.mCardASubInfo2.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt5) +" ("+ mAlDataList.get(_position).mSubInfo1+"%)");
					if(mAlDataList.get(_position).mSubInfo1.contains("-")){
						_listRow.mCardASubInfo2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stock_down, 0, 0, 0);
						_listRow.mCardASubInfo2.setCompoundDrawablePadding(10);
						_listRow.mCardASubInfo2.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_80_129_229));
					}
					else {
						_listRow.mCardASubInfo2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stock_up, 0, 0, 0);
						_listRow.mCardASubInfo2.setCompoundDrawablePadding(10);
						_listRow.mCardASubInfo2.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_230_77_00));
					}
					_listRow.mCardALayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
				else{
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardBLayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.VISIBLE);
					_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
					_listRow.mCardELayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
			}
			else if (mAlDataList.get(_position).mTxt1.equals("펀드")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardALayout.setVisibility(View.GONE);									// 펀드 일단 막으라고 요청(내부회의) 2016-01-22
	//    			_listRow.mCardALayout.setVisibility(View.VISIBLE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardATitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardATitleCount.setText(mAlDataList.get(_position).mTxt8);
					_listRow.mCardAIcon.setBackgroundResource(R.drawable.home_icon_04);
					_listRow.mCardACost.setText(mAlDataList.get(_position).mTxt3);
					_listRow.mCardASubInfo1.setText(TransFormatUtils.transDateForm(mAlDataList.get(_position).mTxt4));
					_listRow.mCardASubInfo2.setText(mAlDataList.get(_position).mTxt5);
					_listRow.mCardALayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
				else{
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.VISIBLE);
					_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
					_listRow.mCardELayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
			}
			else if (mAlDataList.get(_position).mTxt1.equals("대출")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardBLayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.VISIBLE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardCTitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardCTitleCount.setText(mAlDataList.get(_position).mTxt8);
					_listRow.mCardCIcon.setBackgroundResource(R.drawable.home_icon_05);
					_listRow.mCardCCost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt3));
					_listRow.mCardCLayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
				else{
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardBLayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.VISIBLE);
					_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
					_listRow.mCardELayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
			}
			else if (mAlDataList.get(_position).mTxt1.equals("기타재산")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardBLayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.VISIBLE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardDTitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardDTitleCount.setText(mAlDataList.get(_position).mTxt8);
					_listRow.mCardDIcon.setBackgroundResource(R.drawable.home_icon_06);
					_listRow.mCardDSubInfo1Title.setText("부동산");
					_listRow.mCardDSubInfo1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt5));
					_listRow.mCardDSubInfo2Title.setText("차량");
					_listRow.mCardDSubInfo2Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt6));
					_listRow.mCardDSubInfo3Title.setText("기타");
					_listRow.mCardDSubInfo3Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt7));
					_listRow.mCardDLayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
				else{
					_listRow.mCardALayout.setVisibility(View.GONE);
					_listRow.mCardBLayout.setVisibility(View.GONE);
					_listRow.mCardCLayout.setVisibility(View.GONE);
					_listRow.mCardDLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.VISIBLE);
					_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
					_listRow.mCardELayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
			}
			else{
				/* 기타 */
				_listRow.mCardALayout.setVisibility(View.GONE);
				_listRow.mCardBLayout.setVisibility(View.GONE);
				_listRow.mCardCLayout.setVisibility(View.GONE);
				_listRow.mCardDLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
			}

		} else {
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			_listRow.mCardALayout.setVisibility(View.GONE);
			_listRow.mCardBLayout.setVisibility(View.GONE);
			_listRow.mCardCLayout.setVisibility(View.GONE);
			_listRow.mCardDLayout.setVisibility(View.GONE);
			_listRow.mCardELayout.setVisibility(View.GONE);
			if (mAlDataList.get(_position).mTxt1.equals("예금/적금")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardALayout.setVisibility(View.VISIBLE);
					_listRow.mCardATitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardATitleCount.setVisibility(View.GONE);
					_listRow.mCardAIcon.setBackgroundResource(R.drawable.home_icon_01);
					_listRow.mCardACost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt3));
					_listRow.mCardASubInfo1.setText("평균 은행계좌 보유 수 : "+mAlDataList.get(_position).mTxt4 + "개");
					_listRow.mCardASubInfo2.setVisibility(View.GONE);
					_listRow.mCardALayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position+1, 0);
						}
					});
				}
			}
			else if (mAlDataList.get(_position).mTxt1.equals("월 평균 카드 사용액")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardALayout.setVisibility(View.VISIBLE);
					_listRow.mCardATitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardATitleCount.setVisibility(View.GONE);
					_listRow.mCardAIcon.setBackgroundResource(R.drawable.home_icon_02);
					_listRow.mCardACost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt3));
					_listRow.mCardASubInfo1.setText("평균 카드 보유 수 : "+mAlDataList.get(_position).mTxt4 + "개");
					_listRow.mCardASubInfo2.setVisibility(View.GONE);
					_listRow.mCardALayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position+1, 0);
						}
					});
				}
			}
			else if (mAlDataList.get(_position).mTxt1.equals("평균 대출액")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardCLayout.setVisibility(View.VISIBLE);
					_listRow.mCardCTitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardCTitleCount.setVisibility(View.GONE);
					_listRow.mCardCIcon.setBackgroundResource(R.drawable.home_icon_05);
					_listRow.mCardCCost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt3));
					_listRow.mCardCLayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
			}
			else if (mAlDataList.get(_position).mTxt1.equals("평균 기타재산 항목")){
				if (mAlDataList.get(_position).mTxt2.equals("y")) {
					_listRow.mCardDLayout.setVisibility(View.VISIBLE);
					_listRow.mCardDTitle.setText(mAlDataList.get(_position).mTxt1);
					_listRow.mCardDTitleCount.setVisibility(View.GONE);
					_listRow.mCardDIcon.setBackgroundResource(R.drawable.home_icon_06);
					_listRow.mCardDSubInfo1Title.setText("부동산");
					_listRow.mCardDSubInfo1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt5));
					_listRow.mCardDSubInfo2Title.setText("차량");
					_listRow.mCardDSubInfo2Cost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(_position).mTxt6));
					_listRow.mCardDSubInfo3Title.setVisibility(View.GONE);
					_listRow.mCardDSubInfo3Cost.setVisibility(View.GONE);
					_listRow.mCardDLayout.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mItemClickListener.onItemClick(null, v, _position + 1, 0);
						}
					});
				}
			}
			else{
				/* 기타 */
				_listRow.mCardALayout.setVisibility(View.GONE);
				_listRow.mCardBLayout.setVisibility(View.GONE);
				_listRow.mCardCLayout.setVisibility(View.GONE);
				_listRow.mCardDLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
			}



		}
    }
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<InfoDataMain1> array){
    	mAlDataList = array;
	}
    
//====================================================================//
// ViewHolder
//====================================================================//
    private class ViewHolder {

    	/* Card A */
    	private LinearLayout 	mCardALayout;
    	private TextView		mCardATitle;
    	private TextView		mCardATitleCount;
    	private ImageView 		mCardAIcon;
    	private TextView		mCardACost;
    	private TextView		mCardASubInfo1;
    	private TextView		mCardASubInfo2;

    	/* Card B */
    	private LinearLayout 	mCardBLayout;
    	private TextView		mCardBTitle;
		private ImageView 		mCardBIcon;
    	private TextView		mCardBCost;
		private LinearLayout	mCardBList;
		private View			mCardBContentLayout;
		private View			mCardBEmptyLayout;

    	/* Card C */
    	private LinearLayout 	mCardCLayout;
    	private TextView		mCardCTitle;
		private TextView		mCardCTitleCount;
    	private ImageView 		mCardCIcon;
    	private TextView		mCardCCost;

    	/* Card D */
    	private LinearLayout 	mCardDLayout;
    	private TextView		mCardDTitle;
		private TextView		mCardDTitleCount;
    	private ImageView 		mCardDIcon;
    	private TextView		mCardDSubInfo1Cost;
    	private TextView		mCardDSubInfo1Title;
    	private TextView		mCardDSubInfo2Cost;
    	private TextView		mCardDSubInfo2Title;
		private TextView		mCardDSubInfo3Cost;
		private TextView		mCardDSubInfo3Title;

    	/* Card E */
    	private LinearLayout 	mCardELayout;
    	private TextView		mCardETitle;
		private TextView		mCardESubTitle;
    	private ImageView 		mCardEIcon;

    	/* Card F */
//    	private LinearLayout 	mCardFLayout;
//    	private TextView		mCardFTitle;
//    	private ImageView 		mCardFIcon;
//    	private TextView		mCardFSubInfo1Title;
//    	private TextView		mCardFSubInfo1Tag;
//    	private TextView		mCardFSubInfo1Txt;
//    	private TextView		mCardFSubInfo1Cost;
//    	private TextView		mCardFSubInfo2Title;
//    	private TextView		mCardFSubInfo2Tag;
//    	private TextView		mCardFSubInfo2Txt;
//    	private TextView		mCardFSubInfo2Cost;
//    	private TextView		mCardFSubInfo3Title;
//    	private TextView		mCardFSubInfo3Tag;
//    	private TextView		mCardFSubInfo3Txt;
//    	private TextView		mCardFSubInfo3Cost;

    	/* Card G */
//    	private LinearLayout 	mCardGLayout;
//    	private TextView		mCardGTitle;
//    	private ImageView 		mCardGIcon;
//    	private TextView		mCardGSubInfo1date;
//    	private TextView		mCardGSubInfo1Txt1;
//    	private TextView		mCardGSubInfo1Txt1Tag;
//    	private TextView		mCardGSubInfo1Txt2;
//    	private TextView		mCardGSubInfo1Txt3;
//    	private TextView		mCardGSubInfo1Cost;
//    	private TextView		mCardGSubInfo2date;
//    	private TextView		mCardGSubInfo2Txt1;
//    	private TextView		mCardGSubInfo2Txt1Tag;
//    	private TextView		mCardGSubInfo2Txt2;
//    	private TextView		mCardGSubInfo2Txt3;
//    	private TextView		mCardGSubInfo2Cost;
//    	private TextView		mCardGSubInfo3date;
//    	private TextView		mCardGSubInfo3Txt1;
//    	private TextView		mCardGSubInfo3Txt1Tag;
//    	private TextView		mCardGSubInfo3Txt2;
//    	private TextView		mCardGSubInfo3Txt3;
//    	private TextView		mCardGSubInfo3Cost;
//    	private TextView		mCardGSubInfo4date;
//    	private TextView		mCardGSubInfo4Txt1;
//    	private TextView		mCardGSubInfo4Txt1Tag;
//    	private TextView		mCardGSubInfo4Txt2;
//    	private TextView		mCardGSubInfo4Txt3;
//    	private TextView		mCardGSubInfo4Cost;

    	/* Card H */
//    	private LinearLayout 	mCardHLayout;
//    	private TextView		mCardHTitle;
//    	private TextView		mCardHTitleCount;
//    	private ImageView 		mCardHIcon;
//    	private TextView		mCardHSubInfoTitle;
//    	private TextView		mCardHSubInfoCost;
//    	private LinearLayout	mCardHPageIndex;

    	/* Card I */
//    	private LinearLayout 	mCardILayout;
//    	private TextView		mCardITitle;
//    	private TextView		mCardITitleCount;
//    	private ImageView 		mCardIIcon;
//    	private TextView		mCardISubInfo1Title;
//    	private TextView		mCardISubInfo1Cost;
//    	private TextView		mCardISubInfo2Title;
//    	private TextView		mCardISubInfo2Cost;

    	/* Card J */
//    	private LinearLayout 	mCardJLayout;
//    	private TextView		mCardJTitle;
//    	private TextView		mCardJTitleCount;
//    	private ImageView 		mCardJIcon;
//    	private TextView		mCardJSubInfo1Title;
//    	private TextView		mCardJSubInfo1Cost;
//    	private TextView		mCardJSubInfo2Title;
//    	private TextView		mCardJSubInfo2Cost;
//    	private TextView		mCardJSubInfo3Title;
//    	private TextView		mCardJSubInfo3Cost;

    	/* Card K */
//    	private LinearLayout 	mCardKLayout;
//    	private TextView		mCardKTitle;
//    	private ImageView 		mCardKIcon;
//    	private TextView		mCardKSubInfo1Title;
//    	private TextView		mCardKSubInfo1Cost;
//    	private TextView		mCardKSubInfo2Title;
//    	private TextView		mCardKSubInfo2Cost;
//    	private TextView		mCardKSubInfo3Title;
//    	private TextView		mCardKSubInfo3Cost;

    }
    
     
}
