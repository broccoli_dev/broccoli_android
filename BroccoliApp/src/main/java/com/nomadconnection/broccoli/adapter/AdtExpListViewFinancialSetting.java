package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.financial.LinkageFinancialSettingActivity;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoliraonsecure.KSW_CertItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AdtExpListViewFinancialSetting extends BaseExpandableListAdapter {
	 
    private ArrayList<LinkageFinancialSettingActivity.LinkageData> mGroupList = new ArrayList<>();
    private HashMap<String, ArrayList<BankData>> mChildList = new HashMap<>();
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;
    private View.OnClickListener mClickListener;

    private TypedArray mBankRes;
    private TypedArray mCardRes;
    private TypedArray mBankDisableRes;
    private TypedArray mCardDisableRes;
    private List<String> mBankCodeList;
    private List<String> mCardCodeList;
 
    public AdtExpListViewFinancialSetting(Context c, View.OnClickListener onClickListener){
        super();
        this.inflater = LayoutInflater.from(c);
        mClickListener = onClickListener;
        mBankRes = c.getResources().obtainTypedArray(R.array.bank_name_image);
        mCardRes = c.getResources().obtainTypedArray(R.array.card_name_image);
        mBankDisableRes = c.getResources().obtainTypedArray(R.array.bank_name_image_dim_20);
        mCardDisableRes = c.getResources().obtainTypedArray(R.array.card_name_image_dim_20);
        mBankCodeList = Arrays.asList(c.getResources().getStringArray(R.array.bank_code));
        mCardCodeList = Arrays.asList(c.getResources().getStringArray(R.array.card_code));
    }

    public void setData(ArrayList<LinkageFinancialSettingActivity.LinkageData> groupList, HashMap<String, ArrayList<BankData>> childList) {
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public LinkageFinancialSettingActivity.LinkageData getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        int count = 0;
        if (mGroupList != null) {
            count = mGroupList.size();
        }
        return count;
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        GroupHolder holder = null;
 
        if(convertView == null){
            holder = new GroupHolder();
            convertView = inflater.inflate(R.layout.row_expandable_listview_item_group_financial_setting, parent, false);
            holder.mStatus = (ImageView)convertView.findViewById(R.id.iv_cert_status);
            holder.mSubjectName = (TextView)convertView.findViewById(R.id.tv_certname);
            holder.mMessage = (TextView)convertView.findViewById(R.id.tv_message);
            holder.mIssuerName = (TextView)convertView.findViewById(R.id.tv_cert_issuername);
            holder.mPolicy = (TextView)convertView.findViewById(R.id.tv_cert_policy);
            holder.mExpireTime = (TextView)convertView.findViewById(R.id.tv_cert_expire_time);
            holder.mSetting = convertView.findViewById(R.id.tv_row_item_financial_setting);
            holder.mSubLayout = convertView.findViewById(R.id.middle_layout);
            holder.mStatus.setScaleType(ImageView.ScaleType.FIT_CENTER);
            holder.mDeleteMessage = (TextView) convertView.findViewById(R.id.tv_under_message);
            convertView.setTag(holder);
        }else{
            holder = (GroupHolder)convertView.getTag();
            holder.mStatus.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }

        LinkageFinancialSettingActivity.LinkageData data = getGroup(groupPosition);

        if (data.type == LinkageFinancialSettingActivity.LinkageData.LinkType.CERTIFICATE) {
            String name = (String) data.item.get(KSW_CertItem.SUBJECTNAME);
            String[] names = name.split("\\(");
            if (names != null && names.length > 0) {
                holder.mSubjectName.setText(names[0]);
            } else {
                holder.mSubjectName.setText(name);
            }

            holder.mPolicy.setText((String)data.item.get(KSW_CertItem.POLICY));
            holder.mIssuerName.setText((String)data.item.get(KSW_CertItem.ISSUERNAME));
            holder.mExpireTime.setText((String)data.item.get(KSW_CertItem.EXPIREDTIME));
            holder.mSetting.setTag(data.item);
            holder.mSetting.setOnClickListener(mClickListener);
            holder.mSetting.setVisibility(View.VISIBLE);
            holder.mSubLayout.setVisibility(View.VISIBLE);
            holder.mMessage.setVisibility(View.VISIBLE);
            holder.mDeleteMessage.setVisibility(View.GONE);

            if (data.item.isAvailable()) {
                holder.mStatus.setImageResource(R.drawable.financial_cert);
                holder.mMessage.setText(null);
                String expireDate = (String)data.item.get(KSW_CertItem.EXPIREDTIME);
                if (expireDate != null && !TextUtils.isEmpty(expireDate)) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    try {
                        String clearStr = expireDate.replaceAll("[\\D]", "");
                        Calendar calendar = Calendar.getInstance();
                        Calendar certDate = Calendar.getInstance();
                        Date date = simpleDateFormat.parse(clearStr);
                        certDate.setTime(date);
                        long dffSec = (calendar.getTimeInMillis() - certDate.getTimeInMillis())/1000;
                        long dffDay = dffSec/(60*60*24);
                        if (dffDay >= -30) {
                            holder.mStatus.setImageResource(R.drawable.financial_cert_excl);
                            holder.mMessage.setText("D-"+String.valueOf(dffDay)+" "+holder.mMessage.getResources().getString(R.string.setting_financial_reg_link_setting_cert_expire));
                        } else {
                            holder.mStatus.setImageResource(R.drawable.financial_cert);
                            holder.mMessage.setText(null);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                        holder.mStatus.setImageResource(R.drawable.financial_cert);
                    }
                }
            } else {
                holder.mStatus.setImageResource(R.drawable.financial_cert_warn);
                if (data.item.isExpired()) {
                    holder.mMessage.setText(R.string.setting_financial_reg_link_setting_cert_expire);
                } else {
                    holder.mMessage.setText(R.string.setting_financial_reg_link_setting_cert_error);
                }
            }
        } else if (data.type == LinkageFinancialSettingActivity.LinkageData.LinkType.ID_PASS) {
            holder.mStatus.setImageResource(R.drawable.financial_login);
            holder.mSubjectName.setText(R.string.setting_financial_reg_link_setting_id_pwd_title);
            holder.mPolicy.setText(null);
            holder.mIssuerName.setText(null);
            holder.mExpireTime.setText(null);
            holder.mSetting.setTag(null);
            holder.mSetting.setOnClickListener(null);
            holder.mSubLayout.setVisibility(View.GONE);
            holder.mSetting.setVisibility(View.GONE);
            holder.mMessage.setVisibility(View.GONE);
            holder.mDeleteMessage.setVisibility(View.GONE);
        } else {
            String name = (String) data.item.get(KSW_CertItem.SUBJECTNAME);
            if (name != null && !TextUtils.isEmpty(name)) {
                String[] names = name.split("\\(");
                if (names != null && names.length > 0) {
                    holder.mSubjectName.setText(names[0]);
                } else {
                    holder.mSubjectName.setText(name);
                }
            } else {
                name = (String) data.item.get(KSW_CertItem.PATH);
                boolean isLowerCase = false;
                if (name != null) {
                    isLowerCase = name.contains("cn=");
                }
                String[] splitTemp = isLowerCase ? name.split("cn=") : name.split("CN=");
                if (splitTemp.length > 0) {
                    String[] names = splitTemp[1].split("\\(");
                    if (names != null && names.length > 0) {
                        holder.mSubjectName.setText(names[0]);
                    } else {
                        holder.mSubjectName.setText(name);
                    }
                }
            }

            holder.mPolicy.setText(null);
            holder.mIssuerName.setText(null);
            holder.mExpireTime.setText(null);
            holder.mSetting.setTag(data.item);
            holder.mSetting.setOnClickListener(mClickListener);
            holder.mSetting.setVisibility(View.VISIBLE);
            holder.mSubLayout.setVisibility(View.GONE);
            holder.mStatus.setImageResource(R.drawable.financial_cert_warn);
            holder.mMessage.setVisibility(View.GONE);
            holder.mDeleteMessage.setVisibility(View.VISIBLE);
            holder.mDeleteMessage.setText(R.string.setting_financial_reg_link_setting_cert_missing);
        }
 
        return convertView;
    }


//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public BankData getChild(int groupPosition, int childPosition){
        return mChildList.get(getGroup(groupPosition).key).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(getGroup(groupPosition).key).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_financial_setting, null);
            mViewHolder.mChildImage = (ImageView)v.findViewById(R.id.iv_row_expandable_listview_item_child_financial_bank);
            mViewHolder.mChildTextView = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_financial_bank);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }


        BankData data = getChild(groupPosition, childPosition);
        String code = data.getBankCode();
        int index = 0;
        mViewHolder.mChildImage.setVisibility(View.VISIBLE);
        mViewHolder.mChildTextView.setVisibility(View.GONE);
        if (BankData.TYPE_BANK.equalsIgnoreCase(data.getType())) {
            index = mBankCodeList.indexOf(code);
            if (data.getStatus() == null || BankData.NONE.equalsIgnoreCase(data.getStatus())) {
                mViewHolder.mChildImage.setImageResource(mBankRes.getResourceId(index, -1));
            } else {
                mViewHolder.mChildImage.setImageResource(mBankDisableRes.getResourceId(index, -1));
            }
        } else if (BankData.TYPE_CARD.equalsIgnoreCase(data.getType())) {
            index = mCardCodeList.indexOf(code);
            if (data.getStatus() == null || BankData.NONE.equalsIgnoreCase(data.getStatus())) {
                mViewHolder.mChildImage.setImageResource(mCardRes.getResourceId(index, -1));
            } else {
                mViewHolder.mChildImage.setImageResource(mCardDisableRes.getResourceId(index, -1));
            }
        } else {
            mViewHolder.mChildImage.setVisibility(View.GONE);
            mViewHolder.mChildTextView.setVisibility(View.VISIBLE);
            mViewHolder.mChildTextView.setText("현금영수증");
            if (data.getStatus() == null || BankData.NONE.equalsIgnoreCase(data.getStatus())) {
                mViewHolder.mChildTextView.setTextColor(mViewHolder.mChildTextView.getResources().getColor(R.color.common_maintext_color));
            } else {
                mViewHolder.mChildTextView.setTextColor(mViewHolder.mChildTextView.getResources().getColor(R.color.subitem_left_txt_dim_color));
            }
        }
 
        return v;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}

    class GroupHolder {
        private ImageView mStatus;
        private TextView mSubjectName;
        private TextView mMessage;
        private TextView mPolicy;
        private TextView mIssuerName;
        private TextView mExpireTime;
        private View mSetting;
        private View mSubLayout;
        private TextView mDeleteMessage;
    }
 
    class ViewHolder{
        
        /* child */
        public ImageView mChildImage;
        public TextView mChildTextView;
    }
}
