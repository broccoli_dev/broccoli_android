package com.nomadconnection.broccoli.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 3. 10..
 */

public class CommonAdapter extends BaseAdapter {

    private List<?> mList;
    private AdapterItemCreateListener mListener;

    public interface AdapterItemCreateListener {
        View getView(List<?> list, int position, View convertView, ViewGroup parent);
    }

    public CommonAdapter(AdapterItemCreateListener listener) {
        mListener = listener;
    }

    public void setData(List<?> list) {
        mList = list;
    }

    public List<?> getData() {
        return mList;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mList != null) {
            count = mList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        if (mList != null) {
            object = mList.get(position);
        }
        return object;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return mListener.getView(mList, position, convertView, parent);
    }
}
