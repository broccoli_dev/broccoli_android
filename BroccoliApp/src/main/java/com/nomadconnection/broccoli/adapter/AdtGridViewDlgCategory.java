package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

import java.util.List;


public class AdtGridViewDlgCategory extends BaseAdapter {

	private Activity mActivity;
	private List<String> mListCategory;
//	private ArrayList<Integer> mArrayList;
	private ViewHolder mViewHolder;
	private int mSelectValue;
	
	public AdtGridViewDlgCategory(Activity _Activity, List<String> _ListCategory){
		mActivity = _Activity;
		mListCategory = _ListCategory;
	}

	@Override
	public int getCount() {
		return mListCategory.size();
	}

	@Override
	public String getItem(int position) {
//		public Object getItem(int position) {
		return mListCategory.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View mView = convertView;
		LayoutInflater inflator = mActivity.getLayoutInflater();
		
		if(mView == null){
			mViewHolder = new ViewHolder();
			mView = inflator.inflate(R.layout.row_grid_item_category, null);
			
			mViewHolder.mImage = (ImageView)mView.findViewById(R.id.iv_row_grid_item_category_img);
			mViewHolder.mImageBg = (ImageView)mView.findViewById(R.id.iv_row_grid_item_category_bg);
			mViewHolder.mName = (TextView)mView.findViewById(R.id.tv_row_grid_item_category_name);
			
			mView.setTag(mViewHolder);
		}
		else{
			mViewHolder = (ViewHolder)mView.getTag();
		}
		
		/* 값 입력 */
		mViewHolder.mImage.setTag(position);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			mViewHolder.mImage.setBackground(img_select_set(position));
		}

		if(position == mSelectValue){
			mViewHolder.mImageBg.setSelected(true);
			mViewHolder.mName.setTextColor(mActivity.getResources().getColor(R.color.tag_d_txt_color));
		}
		else {
			mViewHolder.mImageBg.setSelected(false);
			mViewHolder.mName.setTextColor(mActivity.getResources().getColor(R.color.tag_a_4_txt_color));
		}
		mViewHolder.mName.setText(getItem(position));

		
		return mView;
	}

	public void setSelectValue(int _selectValue){
		mSelectValue = _selectValue;
	}

	private StateListDrawable img_select_set(int _position){
		StateListDrawable background = new StateListDrawable();
		Drawable normal = null, press = null, select = null;
		switch (_position){
			case 0:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_01);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_01);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_01);
				break;
			case 1:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_02);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_02);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_02);
				break;
			case 2:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_03);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_03);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_03);
				break;
			case 3:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_04);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_04);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_04);
				break;
			case 4:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_05);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_05);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_05);
				break;
			case 5:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_06);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_06);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_06);
				break;
			case 6:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_07);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_07);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_07);
				break;
			case 7:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_08);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_08);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_08);
				break;
			case 8:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_09);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_09);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_09);
				break;
			case 9:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_10);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_10);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_10);
				break;
			case 10:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_11);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_11);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_11);
				break;
			case 11:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_12);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_12);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_12);
				break;
			case 12:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_13);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_13);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_13);
				break;
			case 13:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_14);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_14);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_14);
				break;
			case 14:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_15);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_15);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_15);
				break;
			case 15:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_16);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_16);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_16);
				break;
			case 16:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_17);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_17);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_17);
				break;
			case 17:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_18);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_18);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_18);
				break;
			case 18:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_19);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_19);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_19);
				break;
			case 19:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_20);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_20);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_20);
				break;
			case 20:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_21);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_21);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_21);
				break;
			case 21:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_22);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_22);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_22);
				break;
			case 22:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_23);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_23);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_23);
				break;
			case 23:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_24);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_24);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_24);
				break;
			case 24:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_25);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_25);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_25);
				break;
			case 25:
				normal = mActivity.getResources().getDrawable(R.drawable.popup_icon_25);
				press = mActivity.getResources().getDrawable(R.drawable.popup_icon_p_25);
				select = mActivity.getResources().getDrawable(R.drawable.popup_icon_25);
				break;
			default:
				normal = null;
				press = null;
				break;
		}
		background.addState(new int[]{android.R.attr.state_pressed}, press);
		background.addState(new int[]{android.R.attr.state_selected}, select);
		background.addState(new int[]{}, normal);
		return background;
	}
	
	
	
	
	
//====================================================================//
// ViewHolder
//====================================================================//   
	    private class ViewHolder {
	    	private ImageView mImage;
			private ImageView mImageBg;
	    	private TextView  mName;
	    }
	    

}
