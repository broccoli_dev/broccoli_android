package com.nomadconnection.broccoli.adapter;

import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

public class AdtListDlgRadio extends BaseAdapter implements OnClickListener{

	private Context mContext;
	private int mSelectPosition = 0;
	private Handler mHandler;
	private List<String> mListRadio;
	
	public AdtListDlgRadio(Context context , int position , Handler handler, List<String> _ListRadio) {
		mContext = context;
		mSelectPosition = position;
		mHandler = handler;
		mListRadio = _ListRadio;
	}

	@Override
	public String getItem(int position) {
		return mListRadio.get(position).toString();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View v = convertView;
		
		if(v == null){
			v = View.inflate(mContext, R.layout.row_list_item_radio_period, null);
		}
		
		String info = getItem(position);
		
		if(info != null){
			
			TextView tv_Menu = (TextView) v.findViewById(R.id.tv_row_list_item_radio_period_text);
			RadioButton rb_Menu = (RadioButton) v.findViewById(R.id.rb_row_list_item_radio_period_icon);
			
			v.setOnClickListener(this);
			rb_Menu.setOnClickListener(this);
			
			tv_Menu.setText(info);
			
			if(mSelectPosition == position){
				rb_Menu.setChecked(true);
			}else{
				rb_Menu.setChecked(false);
			}
			
			v.setTag(position);
			rb_Menu.setTag(position);
		}
		
		return v;
	}

	@Override
	public void onClick(View v) {
		int index = (Integer) v.getTag();
		mSelectPosition = index;
		notifyDataSetChanged();
		mHandler.sendEmptyMessageDelayed(index, 100);
//		mHandler.sendEmptyMessage(index);
		
	}

	@Override
	public int getCount() {
		if(mListRadio == null)
			return  0;
		return mListRadio.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
}
