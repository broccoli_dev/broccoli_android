package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.ResponseSearchAssetStock;

import java.util.ArrayList;


public class AdtListViewAssetStockSearch extends BaseAdapter {

	private ArrayList<ResponseSearchAssetStock> 	mAlDataList;
	private Activity			  		mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewAssetStockSearch(Activity _activity, ArrayList<ResponseSearchAssetStock> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public ResponseSearchAssetStock getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_asset_stock_search, parent, false);
            listRow.mStockName			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_stock_search_info_1);
            listRow.mStockNum			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_stock_search_info_2);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        listRow.mStockName.setText(mAlDataList.get(position).mStockName);
        listRow.mStockNum.setText(mAlDataList.get(position).mStockCode);
        if (position == mAlDataList.size()-1) ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private TextView		mStockName;
    	private TextView		mStockNum;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<ResponseSearchAssetStock> array){
    	mAlDataList = array;
	}
    
     
}
