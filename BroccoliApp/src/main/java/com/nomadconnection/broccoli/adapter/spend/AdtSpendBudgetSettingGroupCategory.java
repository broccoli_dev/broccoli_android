package com.nomadconnection.broccoli.adapter.spend;

import android.content.Context;
import android.database.DataSetObserver;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetSettingData;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 3. 7..
 */

public class AdtSpendBudgetSettingGroupCategory extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<SpendBudgetSettingData> mList;
    private DataSetObserver mObserver;

    public AdtSpendBudgetSettingGroupCategory(Context context, DataSetObserver observer) {
        mInflater = LayoutInflater.from(context);
        mObserver = observer;
    }

    public void setData(List<SpendBudgetSettingData> list) {
        mList = list;
    }

    public List<SpendBudgetSettingData> getDataList() {
        return mList;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mList != null) {
            count = mList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        Object item = null;
        if (mList != null) {
            item = mList.get(position);
        }
        return item;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_list_item_spend_budget_setting_large_category, parent, false);

            holder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_budget_title);
            holder.mValue = (EditText) convertView.findViewById(R.id.et_row_spend_budget_category);
            holder.mWatcher = new IndexTextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    SpendBudgetSettingData data = mList.get(getIndex());
                    String target = s.toString();
                    if (target != null) {
                        data.budget = target.replaceAll(",", "");
                        mObserver.onChanged();
                    }
                }
            };
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.mWatcher.setIndex(position);
        holder.mTitle.setText(mList.get(position).title);
        String expense = mList.get(position).expense;
        long expenseValue = Long.valueOf(expense);
        expenseValue = Math.round(expenseValue/10000.0);
        holder.mValue.setHint(ElseUtils.getDecimalFormat(expenseValue));
        holder.mValue.setText(mList.get(position).budget);
        holder.mValue.addTextChangedListener(holder.mWatcher);

        return convertView;
    }

    private class ViewHolder {
        private TextView mTitle;
        private EditText mValue;
        private IndexTextWatcher mWatcher;
    }

    private class IndexTextWatcher implements TextWatcher {
        private int index = 0;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }

}
