package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level2ActivityOtherChall;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;

import java.io.Serializable;
import java.util.ArrayList;

public class ChallengeRowPagerAdapter extends android.support.v4.view.PagerAdapter {

	private ArrayList<EtcChallengeInfo> pagerItems;
	private LayoutInflater inflater;
	private Context context;

	private TextView mChallengeTitle;
	private TextView mChallengeLTitleCount;
	private ProgressBar mChallengeProgress;
	private TextView mChallengePercentValue;
	private TextView mChallengePercent;
	private TextView mChallengeAccount;
	private TextView mChallengeCost;

	public ChallengeRowPagerAdapter(Context context, ArrayList<EtcChallengeInfo> pagerItems) {
		super();
		this.pagerItems = pagerItems;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		LinearLayout layout = null;
		layout = (LinearLayout) inflater.inflate(R.layout.row_item_inc_home_card_h, null);

		int count = pagerItems.size();
		long current_value = 0;
		long need_value = 0;
		if("".equals(pagerItems.get(position).getAmt())){
			need_value = Integer.parseInt(pagerItems.get(position).getGoalAmt());
		}
		else {
			current_value = Integer.parseInt(pagerItems.get(position).getAmt());
			need_value = Integer.parseInt(pagerItems.get(position).getGoalAmt());
		}

		Init(layout);
		progressInit(current_value, need_value);
		mChallengeTitle.setText(context.getResources().getString(R.string.other_challenge));
		mChallengeLTitleCount.setText(String.valueOf(count));
		mChallengeAccount.setText(pagerItems.get(position).getTitle());
		mChallengeCost.setText(ElseUtils.getDecimalFormat(current_value));

//		LinearLayout main = (LinearLayout) layout.findViewById(R.id.main);
//		main.setBackgroundColor(context.getResources().getColor(pagerItems.get(position).getColor()));
		container.addView(layout);
		layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, Level2ActivityOtherChall.class);
				intent.putExtra(Const.DATA, (Serializable) pagerItems);
				context.startActivity(intent);
			}
		});
		return layout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
	}

	@Override
	public int getCount() {
		return pagerItems.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object obj) {
		return view.equals(obj);
	}

	private void Init(LinearLayout layout){
		mChallengeTitle= (TextView) layout.findViewById(R.id.tv_row_item_inc_home_card_h_title);
		mChallengeLTitleCount = (TextView) layout.findViewById(R.id.tv_row_item_inc_home_card_h_title_count);
		mChallengeProgress = (ProgressBar) layout.findViewById(R.id.progressbar_row_item_inc_home_card_h);
		mChallengePercentValue = (TextView) layout.findViewById(R.id.tv_row_item_inc_home_card_h_percent_value);
		mChallengePercent = (TextView) layout.findViewById(R.id.tv_row_item_inc_home_card_h_percent);
		mChallengeAccount = (TextView) layout.findViewById(R.id.tv_row_item_inc_home_card_h_sub_info_title);
		mChallengeCost = (TextView) layout.findViewById(R.id.tv_row_item_inc_home_card_h_sub_info_cost);
		mChallengeCost.setTypeface(FontClass.setFont(context));
	}

	private void progressInit(long use_cost, long need_cost){
		int value = (int)( (double)use_cost / (double)need_cost * 100.0 );

		if (use_cost < 0) {
			value = 0;
		}

		if(value > 100){
			value = 100;
		}

		if(value < 31){
			mChallengePercentValue.setText(Integer.toString(value));
			mChallengePercentValue.setTextColor(context.getResources().getColor(R.color.home_card_i_progress_one_section));
			mChallengePercent.setTextColor(context.getResources().getColor(R.color.home_card_i_progress_one_section));
			mChallengeProgress.setProgressDrawable(context.getResources().getDrawable(R.drawable.custom_progress_one_section_circle));
			mChallengeProgress.setProgress(value);
		}
		else if(value < 81){
			mChallengePercentValue.setText(Integer.toString(value));
			mChallengePercentValue.setTextColor(context.getResources().getColor(R.color.home_card_i_progress_two_section));
			mChallengePercent.setTextColor(context.getResources().getColor(R.color.home_card_i_progress_two_section));
			mChallengeProgress.setProgressDrawable(context.getResources().getDrawable(R.drawable.custom_progress_two_section_circle));
			mChallengeProgress.setProgress(value);
		}
		else {
			mChallengePercentValue.setText(Integer.toString(value));
			mChallengePercentValue.setTextColor(context.getResources().getColor(R.color.home_card_i_progress_three_section));
			mChallengePercent.setTextColor(context.getResources().getColor(R.color.home_card_i_progress_three_section));
			mChallengeProgress.setProgressDrawable(context.getResources().getDrawable(R.drawable.custom_progress_three_section_circle));
			mChallengeProgress.setProgress(value);
		}

	}
}
