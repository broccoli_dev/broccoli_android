package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockList;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;


public class AdtListViewAssetStockHistory extends BaseAdapter {

	private ArrayList<BodyAssetStockList> 	mAlDataList;
	private Activity			  		mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewAssetStockHistory(Activity _activity, ArrayList<BodyAssetStockList> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public BodyAssetStockList getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_asset_stock_history, parent, false);
			listRow.mSubInfo1			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_stock_history_info_1);
			listRow.mSubInfo2			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_stock_history_info_2);
			listRow.mSubInfo3			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_stock_history_info_3);
			listRow.mSubInfo4			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_stock_history_info_4);
            convertView.setTag(listRow);
        } else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
		listRow.mSubInfo1.setText(TransFormatUtils.transDateForm(mAlDataList.get(position).mBodyAssetStockDetailTransDate));
		listRow.mSubInfo2.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(position).mBodyAssetStockDetailPrice));
		listRow.mSubInfo3.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(position).mBodyAssetStockDetailAmount));
		listRow.mSubInfo4.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(position).mBodyAssetStockDetailSum));
        if (position == mAlDataList.size()-1) ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return convertView;
    }    
    
    

    
    

    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	
    	private TextView		mSubInfo1;
    	private TextView		mSubInfo2;
    	private TextView		mSubInfo3;
    	private TextView		mSubInfo4;

    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<BodyAssetStockList> array){
    	mAlDataList = array;
	}
    
     
}
