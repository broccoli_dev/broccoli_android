package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.AssetDebtInfo;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

public class AdtExpListViewAssetLoan extends BaseExpandableListAdapter {

    public static final int ITEM_NORMAL = 0;
    public static final int ITEM_ADVERTISE = 1;

    final int MAX_CHILD_COUNT = 2;

    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<AssetDebtInfo>> mChildList = null;
    private LayoutInflater inflater = null;

    public AdtExpListViewAssetLoan(Context c, ArrayList<String> groupList, ArrayList<ArrayList<AssetDebtInfo>> childList){
        super();
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_expandable_listview_item_group_asset_other, parent, false);
            viewHolder.mGroupTitle = (TextView)convertView.findViewById(R.id.tv_row_expandable_listview_item_group_asset_other_title);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.mGroupTitle.setText(getGroup(groupPosition));

        return convertView;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public AssetDebtInfo getChild(int groupPosition, int childPosition){
//    	public String getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        int viewType = getChildType(groupPosition, childPosition);

        if(convertView == null){
            viewHolder = new ViewHolder();

            if (viewType == ITEM_NORMAL) {
                convertView = inflater.inflate(R.layout.row_expandable_listview_item_child_asset_other, null);
                viewHolder.mInfo1 = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_info_1);
                viewHolder.mInfo2 = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_info_2);
                viewHolder.mInfoCost = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_cost);
                viewHolder.mInfoRemain = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_quantity);
                viewHolder.mInfoImage = (RelativeLayout) convertView.findViewById(R.id.rl_row_expandable_listview_item_child_asset_other_image);
            } else {
                convertView = inflater.inflate(R.layout.row_list_item_honest_fund, parent, false);
            }
            viewHolder.mId = viewType;
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
            if (viewType != viewHolder.mId) {
                if (viewType == ITEM_NORMAL) {
                    convertView = inflater.inflate(R.layout.row_expandable_listview_item_child_asset_other, null);
                    viewHolder.mInfo1 = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_info_1);
                    viewHolder.mInfo2 = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_info_2);
                    viewHolder.mInfoCost = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_cost);
                    viewHolder.mInfoRemain = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_quantity);
                    viewHolder.mInfoImage = (RelativeLayout) convertView.findViewById(R.id.rl_row_expandable_listview_item_child_asset_other_image);
                } else {
                    convertView = inflater.inflate(R.layout.row_list_item_honest_fund, parent, false);
                }
                viewHolder.mId = viewType;
                convertView.setTag(viewHolder);
            }
        }

        if (viewType == ITEM_NORMAL) {
            viewHolder.mInfoRemain.setVisibility(View.VISIBLE);
            viewHolder.mInfoRemain.setText("잔여 대출액");
            viewHolder.mInfo1.setText(TransFormatUtils.transBankCodeToName(getChild(groupPosition, childPosition).mAssetDebtCode));
            if (getChild(groupPosition, childPosition).mAssetDebtName == null || getChild(groupPosition, childPosition).mAssetDebtName.isEmpty()) {
                viewHolder.mInfo2.setVisibility(View.GONE);
            } else {
                viewHolder.mInfo2.setText(getChild(groupPosition, childPosition).mAssetDebtName);
                viewHolder.mInfo2.setVisibility(View.VISIBLE);
            }
            String sum = getChild(groupPosition, childPosition).mAssetDebtSum;
            if (sum == null || sum.isEmpty() || sum.equalsIgnoreCase("0")) {
                viewHolder.mInfoCost.setText("미지원");
            } else {
                viewHolder.mInfoCost.setText(TransFormatUtils.getDecimalFormatRecvString(sum));
            }
            viewHolder.mInfoImage.setVisibility(View.INVISIBLE);

            if (isLastChild) ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.GONE);
            else ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public int getChildTypeCount() {
        return MAX_CHILD_COUNT;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).mAdvertise;
    }

    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
 
 
    class ViewHolder{
    	/* group */
        public TextView mGroupTitle;

        /* child */
        public int mId;
        public TextView mInfo1;
        public TextView mInfo2;
        public TextView mInfoCost;
        public TextView mInfoRemain;
        public RelativeLayout mInfoImage;
    }
}
