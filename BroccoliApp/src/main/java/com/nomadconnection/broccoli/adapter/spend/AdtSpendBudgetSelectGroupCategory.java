package com.nomadconnection.broccoli.adapter.spend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetSettingData;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 3. 6..
 */

public class AdtSpendBudgetSelectGroupCategory extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<SpendBudgetSettingData> mList;

    public AdtSpendBudgetSelectGroupCategory(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public AdtSpendBudgetSelectGroupCategory(Context context, List<SpendBudgetSettingData> list) {
        mInflater = LayoutInflater.from(context);
        mList = list;
    }

    public void setData(List<SpendBudgetSettingData> list) {
        mList = list;
    }

    public List<SpendBudgetSettingData> getDataList() {
        return mList;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mList != null) {
            count = mList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        Object item = null;
        if (mList != null) {
            item = mList.get(position);
        }
        return item;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_list_item_spend_budget_select_large_category, parent, false);

            holder.mCheck = (ImageView) convertView.findViewById(R.id.imv_row_spend_budget_check);
            holder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_budget_title);
            holder.mSubTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_budget_subtitle);
            holder.mExpense	= (TextView) convertView.findViewById(R.id.tv_row_spend_budget_expense);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.mTitle.setText(mList.get(position).title);
        holder.mSubTitle.setText(mList.get(position).subTitle);
        holder.mCheck.setSelected(mList.get(position).isChecked);
        holder.mExpense.setText(ElseUtils.getStringDecimalFormat(mList.get(position).expense));

        return convertView;
    }

    private class ViewHolder {
        private ImageView mCheck;
        private TextView mTitle;
        private TextView mSubTitle;
        private TextView mExpense;
    }
}
