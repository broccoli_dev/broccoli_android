package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetBankDetail;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;


public class AdtListViewAssetDaSA extends BaseAdapter {

    private ArrayList<ResponseAssetBankDetail> mOriginalDataList;
	private ArrayList<ResponseAssetBankDetail> 	mAlDataList = new ArrayList<>();
	private Activity			  		mActivity;
    private LayoutInflater              mInflater;
	
//==========================================================================//
// constructor	
//==========================================================================//
	public AdtListViewAssetDaSA(Activity _activity, ArrayList<ResponseAssetBankDetail> _AlDataList) {
		mActivity 		= _activity;
        mOriginalDataList	= _AlDataList;
        mInflater = mActivity.getLayoutInflater();
        sortData(mSort);
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public ResponseAssetBankDetail getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_list_item_asset_das_a, parent, false);
            
            listRow.mDetailLayout		= (LinearLayout) convertView.findViewById(R.id.tv_row_list_item_asset_das_a_detail_layout);
            listRow.mLastLayout		= (LinearLayout) convertView.findViewById(R.id.ll_row_list_item_asset_das_a_last_layout);
            listRow.mDetailName			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_das_a_detail_name);
            listRow.mDetailMethod	= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_das_a_detail_method);
            listRow.mDetailTime		= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_das_a_detail_time);
            listRow.mDetailCost		= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_das_a_detail_cost);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        if(mAlDataList.get(position).mAssetBankDetailOpponent.equals("") && mAlDataList.get(position).mAssetBankDetailSum.equals("")
                && mAlDataList.get(position).mAssetBankDetailTransDate.equals("")){
            listRow.mDetailLayout.setVisibility(View.GONE);
            listRow.mLastLayout.setVisibility(View.VISIBLE);
        }
        else {
            listRow.mDetailLayout.setVisibility(View.VISIBLE);
            listRow.mLastLayout.setVisibility(View.GONE);

            /* sort */
            if (mSort.equals("in")) {
                if (mAlDataList.get(position).mAssetBankDetailSum.contains("-")) {
                    listRow.mDetailLayout.setVisibility(View.GONE);
                }
                else{
                    listRow.mDetailLayout.setVisibility(View.VISIBLE);
                }
            }
            else if (mSort.equals("out")) {
                if (mAlDataList.get(position).mAssetBankDetailSum.contains("-")) {
                    listRow.mDetailLayout.setVisibility(View.VISIBLE);
                }
                else{
                    listRow.mDetailLayout.setVisibility(View.GONE);
                }
            }
            else{
                listRow.mDetailLayout.setVisibility(View.VISIBLE);
            }

            listRow.mDetailName.setText(mAlDataList.get(position).mAssetBankDetailOpponent);
            listRow.mDetailMethod.setText(mAlDataList.get(position).mAssetBankDetailTransMethod);
            listRow.mDetailTime.setText(TransFormatUtils.transDateForm(mAlDataList.get(position).mAssetBankDetailTransDate));

            listRow.mDetailCost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(position).mAssetBankDetailSum));
            if (mAlDataList.get(position).mAssetBankDetailSum.contains("-")) {
                listRow.mDetailCost.setTextColor(Color.rgb(229, 105, 80));
            }
            else{
                listRow.mDetailCost.setTextColor(Color.rgb(80, 129, 229));
            }
        }

        if (position == mAlDataList.size()-1) ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private LinearLayout	mDetailLayout;
        private LinearLayout	mLastLayout;
    	private TextView		mDetailName;
    	private TextView		mDetailMethod;
    	private TextView		mDetailTime;
    	private TextView		mDetailCost;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<ResponseAssetBankDetail> array){
        mOriginalDataList = array;
        sortData(mSort);
	}
    
//====================================================================//
// Sort
//====================================================================//
    String mSort = "all";
    public void sortData(String _sort){
    	mSort = _sort;
        if (mAlDataList == null) {
            mAlDataList = new ArrayList<>();
        }
        mAlDataList.clear();
        for (ResponseAssetBankDetail detail : mOriginalDataList) {
            if (mSort.equals("in")) {
                if (!detail.mAssetBankDetailSum.contains("-")) {
                    mAlDataList.add(detail);
                }
            } else if (mSort.equals("out")) {
                if (detail.mAssetBankDetailSum.contains("-") || TextUtils.isEmpty(detail.mAssetBankDetailSum)) {
                    mAlDataList.add(detail);
                }
            } else{
                mAlDataList.add(detail);
            }
        }
    }
    
     
}
