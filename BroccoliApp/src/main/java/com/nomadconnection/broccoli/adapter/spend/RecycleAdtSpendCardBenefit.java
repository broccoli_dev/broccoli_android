package com.nomadconnection.broccoli.adapter.spend;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

import java.util.TreeMap;

/**
 * Created by YelloHyunminJang on 2017. 3. 29..
 */

public class RecycleAdtSpendCardBenefit extends RecyclerView.Adapter<RecycleAdtSpendCardBenefit.BenefitViewHolder> {

    private Object[] mKeyList = null;
    private TreeMap<Integer, String> list;
    private TypedArray mSpendListIcon;
    private View.OnClickListener mClickListener;
    private int mIndex = -1;

    public RecycleAdtSpendCardBenefit(Context context, View.OnClickListener listener) {
        mSpendListIcon = context.getResources().obtainTypedArray(R.array.spend_category_list_img);
        mClickListener = listener;
    }

    public void setData(TreeMap<Integer, String> list) {
        mKeyList = list.keySet().toArray();
        this.list = list;
    }

    public void setSelectIndex(int index) {
        mIndex = index;
    }

    @Override
    public BenefitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_horizontal_list_item_spend_card_benefit, null);
        BenefitViewHolder holder = new BenefitViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final BenefitViewHolder holder, int position) {
        holder.mTitle.setText(list.get(mKeyList[position]));
        int icon = R.drawable.icon_category_default;
        if (((int)mKeyList[position]) != 0) {
            icon = mSpendListIcon.getResourceId((Integer) mKeyList[position], -1);
        }
        if (position == mIndex) {
            if (!holder.mItemView.isSelected()) {
                holder.mItemView.setSelected(true);
            }
        } else {
            if (holder.mItemView.isSelected()) {
                holder.mItemView.setSelected(false);
            }
        }
        holder.mIcon.setImageResource(icon);
        holder.mItemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (list != null) {
            count = list.size();
        }
        return count;
    }

    public class BenefitViewHolder extends RecyclerView.ViewHolder {

        private View mItemView;
        private ImageView mIcon;
        private TextView mTitle;

        public BenefitViewHolder(View itemView) {
            super(itemView);
            mItemView = itemView;
            mIcon = (ImageView) itemView.findViewById(R.id.iv_row_spend_card_benefit_icon);
            mTitle = (TextView) itemView.findViewById(R.id.tv_row_spend_card_benefit_name);
            itemView.setOnClickListener(mClickListener);
        }
    }
}
