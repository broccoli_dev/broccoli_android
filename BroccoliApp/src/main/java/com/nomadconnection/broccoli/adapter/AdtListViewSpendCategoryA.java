package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.BodySpendCategoryList;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;


public class AdtListViewSpendCategoryA extends BaseAdapter {

	private List<BodySpendCategoryList> 	mAlDataList;
	private Activity			  		mActivity;
    private int                        mValue = 0;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewSpendCategoryA(Activity _activity, List<BodySpendCategoryList> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }
 
    @Override
    public BodySpendCategoryList getItem(int position) {
        BodySpendCategoryList info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_spend_category_a, parent, false);

            listRow.mCategoryIcon       = (ImageView) convertView.findViewById(R.id.imv_row_item_spend_category_icon);
            listRow.mCategoryName			= (TextView) convertView.findViewById(R.id.tv_row_list_item_sped_category_name);
            listRow.mCategoryPercent	            = (TextView) convertView.findViewById(R.id.tv_row_list_item_sped_category_percent);
            listRow.mCategoryCost		        = (TextView) convertView.findViewById(R.id.tv_row_list_item_sped_category_cost);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        /* Default Value */
        int typeCategory = 1;
        if(mAlDataList.get(position).getCategoryId() != null){
            typeCategory = Integer.valueOf(mAlDataList.get(position).getCategoryId());
        }

        String[] category = mActivity.getResources().getStringArray(R.array.spend_category);
        TypedArray spend_list_img = mActivity.getResources().obtainTypedArray(R.array.spend_category_list_img);
        spend_list_img.getResourceId(typeCategory, -1);

        listRow.mCategoryIcon.setImageResource(spend_list_img.getResourceId(typeCategory, R.drawable.list_icon_25));
        spend_list_img.recycle();
        listRow.mCategoryName.setText(category[typeCategory]);
        listRow.mCategoryCost.setText(ElseUtils.getStringDecimalFormat(mAlDataList.get(position).getSum()));
//        if("18".equals(mAlDataList.get(position).getCategoryId())){
//            listRow.mCategoryPercent.setVisibility(View.GONE);
//        }
//        else {
        listRow.mCategoryPercent.setVisibility(View.VISIBLE);
        listRow.mCategoryPercent.setText(mAlDataList.get(position).getPercent() + "%");
//        }

        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private RelativeLayout	mCategoryLayout;
        private ImageView      mCategoryIcon;
        private TextView       mCategoryName;
        private TextView		mCategoryPercent;
    	private TextView		mCategoryCost;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<BodySpendCategoryList> array){
    	mAlDataList = array;
	}
}


