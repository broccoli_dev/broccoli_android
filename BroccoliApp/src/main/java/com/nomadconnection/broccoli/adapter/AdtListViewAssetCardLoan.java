package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.ResponseCardLoanDetail;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;


public class AdtListViewAssetCardLoan extends BaseAdapter {

	private ArrayList<ResponseCardLoanDetail> 	mDataList;
	private Context			  		mContext;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewAssetCardLoan(Context context, ArrayList<ResponseCardLoanDetail> list) {
		mContext 		= context;
        mDataList		= list;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mDataList != null) {
            count = mDataList.size();
        }
        return count;
    }
 
    @Override
    public ResponseCardLoanDetail getItem(int position) {
        return mDataList.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.row_item_asset_card_loan, parent, false);
            
            listRow.mDetailLayout = (LinearLayout) convertView.findViewById(R.id.tv_row_list_item_asset_das_a_detail_layout);
            listRow.mDetailPeriod = (TextView) convertView.findViewById(R.id.tv_row_item_asset_card_loan_detail_period);
            listRow.mDetailCode = (TextView) convertView.findViewById(R.id.tv_row_item_asset_card_loan_detail_type);
            listRow.mDetailName	= (TextView) convertView.findViewById(R.id.tv_row_item_asset_card_loan_detail_name);
            listRow.mDetailRate = (TextView) convertView.findViewById(R.id.tv_row_item_asset_card_loan_detail_rate);
            listRow.mDetailTotal = (TextView) convertView.findViewById(R.id.tv_row_item_asset_card_loan_detail_total);
            listRow.mDetailRemain = (TextView) convertView.findViewById(R.id.tv_row_item_asset_card_loan_detail_remain);
            listRow.mDetailInterest = (TextView) convertView.findViewById(R.id.tv_row_item_asset_card_loan_detail_interest);
            listRow.mProgress = (ProgressBar) convertView.findViewById(R.id.progressbar_item_asset_card_loan_detail);

            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        ResponseCardLoanDetail detail = mDataList.get(position);
        String name = "";
        try {
            name = TransFormatUtils.transCardCodeToName(detail.companyCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        listRow.mDetailCode.setText(name+" 장기카드대출");
        listRow.mDetailName.setText(detail.loanNum);
        listRow.mDetailPeriod.setText(ElseUtils.getCardLoanDate(detail.loanDate)+ " ~ "+ElseUtils.getCardLoanDate(detail.endDate));
        float rate = ((float)(detail.loanAmount - detail.loanBalance) / (float)detail.loanAmount);
        listRow.mDetailRate.setText(String.format("%.1f", rate));
        listRow.mProgress.setProgress((int) rate);
        listRow.mDetailTotal.setText(TransFormatUtils.getDecimalFormat(detail.loanAmount));
        if (detail.loanBalance <= 0) {
            listRow.mDetailRemain.setText("미지원");
            listRow.mDetailRemain.setAlpha(0.5f);
        } else {
            listRow.mDetailRemain.setText(TransFormatUtils.getDecimalFormat(detail.loanBalance));
            listRow.mDetailRemain.setAlpha(1f);
        }
        if (detail.interestRate <= 0) {
            listRow.mDetailInterest.setText("미지원");
            listRow.mDetailInterest.setAlpha(0.5f);
        } else {
            listRow.mDetailInterest.setText(String.valueOf(detail.interestRate));
            listRow.mDetailInterest.setAlpha(1f);
        }
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private LinearLayout	mDetailLayout;
        private ProgressBar     mProgress;
        private TextView        mDetailPeriod;
        private TextView        mDetailCode;
    	private TextView		mDetailName;
        private TextView        mDetailRate;
    	private TextView		mDetailTotal;
    	private TextView		mDetailRemain;
    	private TextView		mDetailInterest;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<ResponseCardLoanDetail> array){
    	mDataList = array;
	}
    
     
}
