package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.BodySpendBudgetCategory;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;


public class AdtListViewSpendBudgetA extends BaseAdapter {

	private List<BodySpendBudgetCategory> 	mAlDataList;
	private Activity			  		mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewSpendBudgetA(Activity _activity, List<BodySpendBudgetCategory> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }
 
    @Override
    public BodySpendBudgetCategory getItem(int position) {
        BodySpendBudgetCategory info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_spend_budget_a, parent, false);

            listRow.mBudgetLayout         = (RelativeLayout) convertView.findViewById(R.id.rl_row_list_item_main);
            listRow.mBudgetProgress       = (ProgressBar) convertView.findViewById(R.id.progressbar_row_list_item_spend_budget);
            listRow.mBudgetTitle			= (TextView) convertView.findViewById(R.id.tv_row_list_item_sped_budget_name);
            listRow.mBudgetMax	            = (TextView) convertView.findViewById(R.id.tv_row_list_item_spend_budget_value);
            listRow.mBudgetUse		        = (TextView) convertView.findViewById(R.id.tv_row_list_item_spend_value);
            listRow.mBudgetMaxOver		    = (TextView) convertView.findViewById(R.id.tv_row_list_item_spend_over_value);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        /* Default Value */
        if(!"25".equals(mAlDataList.get(position).getCategoryId())){
            listRow.mBudgetLayout.setVisibility(View.VISIBLE);
            String[] category = mActivity.getResources().getStringArray(R.array.spend_category);
            int categoryType;
            long budget = 0;
            if(mAlDataList.get(position).getBudget() != null){
                budget = Long.valueOf(mAlDataList.get(position).getBudget());
            }

            long spend = Long.valueOf(mAlDataList.get(position).getExpense());
            int value = (int)( (double)spend / (double)budget * 100.0 );

            if(!"26".equals(mAlDataList.get(position).getCategoryId())) {
                categoryType = Integer.valueOf(mAlDataList.get(position).getCategoryId());
                listRow.mBudgetTitle.setText(category[categoryType]);
            }

            if(value == 0){
                listRow.mBudgetProgress.setProgress(value);
                listRow.mBudgetProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_row_item_0_horizontal));
                listRow.mBudgetUse.setVisibility(View.VISIBLE);
                listRow.mBudgetMaxOver.setVisibility(View.GONE);
                listRow.mBudgetUse.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_89_89_89));
                listRow.mBudgetUse.setText(ElseUtils.getDecimalFormat(spend));
                listRow.mBudgetMax.setText(ElseUtils.getDecimalFormat(budget));
                listRow.mBudgetMax.setTextColor(mActivity.getResources().getColor(R.color.common_argb_50p_89_89_89));
            }
            else if(value < 31){
                listRow.mBudgetProgress.setProgress(value);
                listRow.mBudgetProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_row_item_1_horizontal));
                listRow.mBudgetUse.setVisibility(View.VISIBLE);
                listRow.mBudgetMaxOver.setVisibility(View.GONE);
                listRow.mBudgetUse.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_27_178_166));
                listRow.mBudgetUse.setText(ElseUtils.getDecimalFormat(spend));
                listRow.mBudgetMax.setText(ElseUtils.getDecimalFormat(budget));
            }
            else if(value < 71){
                listRow.mBudgetProgress.setProgress(value);
                listRow.mBudgetProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_row_item_2_horizontal));
                listRow.mBudgetUse.setVisibility(View.VISIBLE);
                listRow.mBudgetMaxOver.setVisibility(View.GONE);
                listRow.mBudgetUse.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_57_158_230));
                listRow.mBudgetUse.setText(ElseUtils.getDecimalFormat(spend));
                listRow.mBudgetMax.setText(ElseUtils.getDecimalFormat(budget));
            }
            else if(value < 101){
                listRow.mBudgetProgress.setProgress(value);
                listRow.mBudgetProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_row_item_3_horizontal));
                listRow.mBudgetUse.setVisibility(View.VISIBLE);
                listRow.mBudgetMaxOver.setVisibility(View.GONE);
                listRow.mBudgetUse.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_229_153_46));
                listRow.mBudgetUse.setText(ElseUtils.getDecimalFormat(spend));
                listRow.mBudgetMax.setText(ElseUtils.getDecimalFormat(budget));
            }
            else{
                listRow.mBudgetProgress.setProgress(value);
                listRow.mBudgetProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_row_item_4_horizontal));
                listRow.mBudgetUse.setVisibility(View.GONE);
                listRow.mBudgetMaxOver.setVisibility(View.VISIBLE);
                listRow.mBudgetMaxOver.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_230_57_57));
                listRow.mBudgetMaxOver.setText(ElseUtils.getDecimalFormat(spend));
                listRow.mBudgetMax.setText(ElseUtils.getDecimalFormat(budget));
                listRow.mBudgetMax.setTextColor(mActivity.getResources().getColor(R.color.common_argb_50p_89_89_89));
            }
        }
        else{
            listRow.mBudgetLayout.setVisibility(View.GONE);
        }

        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private RelativeLayout	mBudgetLayout;
        private ProgressBar    mBudgetProgress;
        private TextView       mBudgetTitle;
    	private TextView		mBudgetMax;
    	private TextView		mBudgetUse;
    	private TextView		mBudgetMaxOver;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<BodySpendBudgetCategory> array){
    	mAlDataList = array;
	}
    

}
