package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;

import java.util.ArrayList;
import java.util.List;


public class AdtListViewOtherChallengeInfo extends BaseAdapter {

	private List<EtcChallengeInfo> 	mAlDataList;
	private Activity			  		mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewOtherChallengeInfo(Activity _activity, List<EtcChallengeInfo> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }
 
    @Override
    public EtcChallengeInfo getItem(int position) {
        EtcChallengeInfo info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_challenge_info, parent, false);
            listRow.mChallengeTitle = (TextView) convertView.findViewById(R.id.tv_row_list_item_challenge_info_title);
            listRow.mChallengeBankLink = (TextView) convertView.findViewById(R.id.tv_row_list_item_challenge_info_bank_link);
            listRow.mChallengeDate = (TextView) convertView.findViewById(R.id.tv_row_list_item_challenge_info_date);
            listRow.mChallengeProgress = (ProgressBar) convertView.findViewById(R.id.progressbar_row_list_item_challenge_info);
            listRow.mChallengePercentValue = (TextView) convertView.findViewById(R.id.tv_row_list_item_challenge_percent_value);
            listRow.mChallengePercent	= (TextView) convertView.findViewById(R.id.tv_row_list_item_challenge_percent);
            listRow.mChallengeCurrent = (TextView) convertView.findViewById(R.id.tv_row_list_item_challenge_info_current);
            listRow.mChallengeCurrent.setTypeface(FontClass.setFont(mActivity));
            listRow.mChallengeTarget = (TextView) convertView.findViewById(R.id.tv_row_list_item_challenge_info_need);
            listRow.mChallengeTarget.setTypeface(FontClass.setFont(mActivity));
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        int value;
        long current_cost;
        long target_cost;
        int challengeType;
        String DateAndType;
        String[]typeStr = mActivity.getResources().getStringArray(R.array.challenge_type);
        if("".equals(mAlDataList.get(position).getAmt())){
            current_cost = 0;
            target_cost = Long.valueOf(mAlDataList.get(position).getGoalAmt());
        }
        else {
            current_cost = Long.valueOf(mAlDataList.get(position).getAmt());
            target_cost = Long.valueOf(mAlDataList.get(position).getGoalAmt());
        }

        if("".equals(mAlDataList.get(position).getChallengeType())){
            DateAndType = "" + "/" + ElseUtils.getBirthDay(mAlDataList.get(position).getStartDate())
                    + "~" + ElseUtils.getBirthDay(mAlDataList.get(position).getEndDate());
        }
        else {
            challengeType = Integer.valueOf(mAlDataList.get(position).getChallengeType());
            DateAndType = typeStr[challengeType-1] + "/" + ElseUtils.getBirthDay(mAlDataList.get(position).getStartDate())
                    + "~" + ElseUtils.getBirthDay(mAlDataList.get(position).getEndDate());
        }

        if("".equals(mAlDataList.get(position).getAccountId())){
            listRow.mChallengeBankLink.setVisibility(View.VISIBLE);
            listRow.mChallengeBankLink.setText(mActivity.getResources().getString(R.string.other_challenge_bank_link));
        }
        else{
            listRow.mChallengeBankLink.setVisibility(View.GONE);
        }

        value = (int)( (double)current_cost / (double)target_cost * 100.0 );

        if (current_cost < 0) {
            value = 0;
        }

        if(value > 100){
            value = 100;
        }

        listRow.mChallengeTitle.setText(mAlDataList.get(position).getTitle());
        listRow.mChallengeDate.setText(DateAndType);

        if(value < 31){
            listRow.mChallengePercentValue.setText(Integer.toString(value));
            listRow.mChallengePercentValue.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_one_section));
            listRow.mChallengePercent.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_one_section));
            listRow.mChallengeProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_1_challenge_circle));
            listRow.mChallengeProgress.setProgress(value);
        }
        else if(value < 61){
            listRow.mChallengePercentValue.setText(Integer.toString(value));
            listRow.mChallengePercentValue.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_two_section));
            listRow.mChallengePercent.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_two_section));
            listRow.mChallengeProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_2_challenge_circle));
            listRow.mChallengeProgress.setProgress(value);
        }
        else {
            if(value == 100){
                listRow.mChallengeBankLink.setVisibility(View.VISIBLE);
                listRow.mChallengeBankLink.setText(mActivity.getResources().getString(R.string.other_challenge_detail_complete));
            }
            listRow.mChallengePercentValue.setText(Integer.toString(value));
            listRow.mChallengePercentValue.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_three_section));
            listRow.mChallengePercent.setTextColor(mActivity.getResources().getColor(R.color.home_card_i_progress_three_section));
            listRow.mChallengeProgress.setProgressDrawable(mActivity.getResources().getDrawable(R.drawable.custom_progress_3_challenge_circle));
            listRow.mChallengeProgress.setProgress(value);
        }

        listRow.mChallengeCurrent.setText(ElseUtils.getDecimalFormat(current_cost));
        listRow.mChallengeTarget.setText(ElseUtils.getDecimalFormat(target_cost));

        return convertView;
    }





//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
        private TextView       mChallengeTitle;
        private TextView       mChallengeBankLink;
        private TextView       mChallengeDate;
        private ProgressBar    mChallengeProgress;
        private TextView       mChallengePercentValue;
        private TextView       mChallengePercent;
    	private TextView       mChallengeCurrent;
    	private TextView       mChallengeTarget;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<EtcChallengeInfo> array){
    	mAlDataList = array;
	}
    
     
}
