package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

import java.util.ArrayList;


public class PagerAdapter extends FragmentStatePagerAdapter {
	
	private String[] mPagerTitle;
	private ArrayList<Fragment> mFragments;

	public PagerAdapter(FragmentManager fm, ArrayList<Fragment> _alFragment, String[] _PagerTitle) {
		super(fm);
		mFragments = _alFragment;
		mPagerTitle = _PagerTitle;
	}

	@Override
	public Fragment getItem(int i) {
		if(i < mFragments.size())
			return mFragments.get(i);
		else
			return null;
	}

	@Override
	public int getCount() {
		return mFragments.size();
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		return mPagerTitle[position];
	}

	public View getTabView(Context context, int position) {
		TextView tv = new TextView(context);
		tv.setGravity(Gravity.CENTER);
		tv.setText(mPagerTitle[position]);
		tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f);
		tv.setTextAppearance(context, android.R.style.TextAppearance_DeviceDefault_Medium);
		int[][] states = new int[][] {new int[] {android.R.attr.state_pressed}, new int[] {android.R.attr.state_enabled},
				new int[] {-android.R.attr.state_pressed}, new int[] {-android.R.attr.state_enabled}};
		int[] colors = new int[] {context.getResources().getColor(R.color.tabbar_title_text_color_press),
				context.getResources().getColor(R.color.tabbar_title_text_color_press),
				context.getResources().getColor(R.color.tabbar_title_text_color_nor),
				context.getResources().getColor(R.color.tabbar_title_text_color_nor)};
		ColorStateList colorStateList = new ColorStateList(states, colors);
		tv.setTextColor(colorStateList);
		tv.setEnabled(false);
		return tv;
	}

	@Override
	public Parcelable saveState() {
		return null;
	}
}
