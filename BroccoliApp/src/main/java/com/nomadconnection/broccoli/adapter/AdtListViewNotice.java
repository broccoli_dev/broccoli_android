package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Etc.EtcNoticeInfo;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;


public class AdtListViewNotice extends BaseAdapter {

	private List<EtcNoticeInfo>             mAlDataList;
	private Activity			  			mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewNotice(Activity _activity, List<EtcNoticeInfo> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }
 
    @Override
    public EtcNoticeInfo getItem(int position) {
        EtcNoticeInfo info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_etc_notice, parent, false);
            listRow.mNoticeDepth	= (LinearLayout) convertView.findViewById(R.id.ll_row_list_item_etc_depth);
            listRow.mNotice			= (LinearLayout) convertView.findViewById(R.id.ll_row_list_item_etc_none_depth);
            listRow.mNoticeDepthName			= (TextView) convertView.findViewById(R.id.tv_row_list_item_etc_notice_depth_name);
            listRow.mNoticeDepthIcon			= (ImageView) convertView.findViewById(R.id.iv_row_list_item_etc_notice_depth_alert_icon);
            listRow.mNoticeDepthDate			= (TextView) convertView.findViewById(R.id.tv_row_list_item_etc_notice_depth_date);
            listRow.mNoticeDepthContent			= (TextView) convertView.findViewById(R.id.tv_row_list_item_etc_notice_depth_content);

            listRow.mNoticeName			= (TextView) convertView.findViewById(R.id.tv_row_list_item_etc_notice_name);
            listRow.mNoticeIcon			= (ImageView) convertView.findViewById(R.id.iv_row_list_item_etc_notice_alert_icon);
            listRow.mNoticeDate			= (TextView) convertView.findViewById(R.id.tv_row_list_item_etc_notice_date);
            listRow.mNoticeContent			= (TextView) convertView.findViewById(R.id.tv_row_list_item_etc_notice_content);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        listRow.mNotice.setVisibility(View.GONE);
        String name = null;
        switch (mAlDataList.get(position).getCategoryCode()){
            case "0101":
            case "0102":
            case "0103":
            case "0104":
            case "0105":
            case "0106":
                name = mActivity.getResources().getString(R.string.other_spend);
                break;
            case "0201":
                name = mActivity.getResources().getString(R.string.other_invest);
                break;
            case "0301":
            case "0302":
            case "0303":
            case "0304":
            case "0305":
                name = mActivity.getResources().getString(R.string.other_money);
                break;
            case "0401":
            case "0402":
            case "0403":
                name = mActivity.getResources().getString(R.string.other_challenge);
                break;
            default:
                name = mActivity.getResources().getString(R.string.other_notify);
                break;
        }
        listRow.mNoticeDepthName.setText(name);
        if("Y".equals(mAlDataList.get(position).getReadYn())){
            listRow.mNoticeDepthIcon.setVisibility(View.GONE);
        }
        else {
            listRow.mNoticeDepthIcon.setVisibility(View.VISIBLE);
        }

        listRow.mNoticeDepthDate.setText(ElseUtils.getDateNotice(mAlDataList.get(position).getDate()));
        listRow.mNoticeDepthContent.setText(mAlDataList.get(position).getMessage());
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
        private LinearLayout    mNoticeDepth;
    	private TextView		mNoticeDepthName;
        private ImageView       mNoticeDepthIcon;
    	private TextView		mNoticeDepthDate;
    	private TextView		mNoticeDepthContent;

        private LinearLayout    mNotice;
        private TextView		mNoticeName;
        private ImageView       mNoticeIcon;
        private TextView		mNoticeDate;
        private TextView		mNoticeContent;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<EtcNoticeInfo> array){
    	mAlDataList = array;
	}
    
     
}
