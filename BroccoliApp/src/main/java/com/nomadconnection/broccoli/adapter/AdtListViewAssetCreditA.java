package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetCreditDetail;
import com.nomadconnection.broccoli.data.Asset.BodyAssetCardDetail;
import com.nomadconnection.broccoli.data.Spend.AssetCardPaymentTypeEnum;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;


public class AdtListViewAssetCreditA extends BaseAdapter {

	private ArrayList<BodyAssetCardDetail> mAlDataList;
    private ArrayList<BodyAssetCardDetail> mOriginalDataList;
	private LayoutInflater mInflater;
    private Level3ActivityAssetCreditDetail.SortType mSort = Level3ActivityAssetCreditDetail.SortType.TOTAL;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewAssetCreditA(Activity _activity, ArrayList<BodyAssetCardDetail> _AlDataList) {
        mInflater = _activity.getLayoutInflater();
        mOriginalDataList = _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }
 
    @Override
    public BodyAssetCardDetail getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_list_item_asset_credit_a, parent, false);
            
            listRow.mDetailLayout		= (LinearLayout) convertView.findViewById(R.id.tv_row_list_item_asset_credit_a_detail_layout);
            listRow.mDetailName			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_credit_a_detail_name);
            listRow.mDetailMethod	= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_credit_a_detail_method);
            listRow.mDetailTime		= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_credit_a_detail_time);
            listRow.mDetailCost		= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_credit_a_detail_cost);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }

        
        /* Data 적용 */

        BodyAssetCardDetail detail = mAlDataList.get(position);
        Level3ActivityAssetCreditDetail.SortType itemType = Level3ActivityAssetCreditDetail.SortType.ONCE;
        listRow.mDetailName.setText(detail.storeName);//mAlDataList.get(position).mAssetCardDetailCardName + " " + 카드 이름삭제 요청이 들어와서 삭제함
        if (AssetCardPaymentTypeEnum.SPLIT == detail.paymentType
                || AssetCardPaymentTypeEnum.SPLIT_ETC == detail.paymentType) {
            String value = String.valueOf(detail.installmentRound);
            String tag = value+"/"+String.valueOf(detail.installmentPeriod);
            listRow.mDetailMethod.setText(tag);
            listRow.mDetailMethod.setVisibility(View.VISIBLE);
            itemType = Level3ActivityAssetCreditDetail.SortType.SPLITTING;
        } else if (AssetCardPaymentTypeEnum.SHORT_LOAN == detail.paymentType
                || AssetCardPaymentTypeEnum.LONG_LOAN == detail.paymentType) {
            listRow.mDetailMethod.setText("대출");
            listRow.mDetailMethod.setVisibility(View.GONE);
            itemType = Level3ActivityAssetCreditDetail.SortType.LOAN;
        } else {
            listRow.mDetailMethod.setText("일시불");
            listRow.mDetailMethod.setVisibility(View.GONE);
            itemType = Level3ActivityAssetCreditDetail.SortType.ONCE;
        }

        switch (mSort) {
            case TOTAL:
                convertView.setVisibility(View.VISIBLE);
                break;
            case ONCE:
                if (itemType != Level3ActivityAssetCreditDetail.SortType.ONCE) {
                    convertView.setVisibility(View.GONE);
                } else {
                    convertView.setVisibility(View.VISIBLE);
                }
                break;
            case SPLITTING:
                if (itemType != Level3ActivityAssetCreditDetail.SortType.SPLITTING) {
                    convertView.setVisibility(View.GONE);
                } else {
                    convertView.setVisibility(View.VISIBLE);
                }
                break;
            case LOAN:
                if (itemType != Level3ActivityAssetCreditDetail.SortType.LOAN) {
                    convertView.setVisibility(View.GONE);
                } else {
                    convertView.setVisibility(View.VISIBLE);
                }
                break;
        }

        listRow.mDetailTime.setText(TransFormatUtils.transDateForm(detail.useDate));
        listRow.mDetailCost.setText(TransFormatUtils.getDecimalFormat(detail.realAmount));
        if (position == mAlDataList.size()-1) ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return convertView;
    }

    public void setSort(Level3ActivityAssetCreditDetail.SortType sort) {
        mSort = sort;
        if (mOriginalDataList != null && mOriginalDataList.size() > 0) {
            if (mAlDataList == null) {
                mAlDataList = new ArrayList<>();
            }
            mAlDataList.clear();
            for (BodyAssetCardDetail detail : mOriginalDataList) {
                Level3ActivityAssetCreditDetail.SortType itemType = Level3ActivityAssetCreditDetail.SortType.TOTAL;
                if (AssetCardPaymentTypeEnum.SPLIT == detail.paymentType
                    || AssetCardPaymentTypeEnum.SPLIT_ETC == detail.paymentType) {
                    itemType = Level3ActivityAssetCreditDetail.SortType.SPLITTING;
                } else if (AssetCardPaymentTypeEnum.SHORT_LOAN == detail.paymentType
                        || AssetCardPaymentTypeEnum.LONG_LOAN == detail.paymentType) {
                    itemType = Level3ActivityAssetCreditDetail.SortType.LOAN;
                } else {
                    itemType = Level3ActivityAssetCreditDetail.SortType.ONCE;
                }
                if (mSort != Level3ActivityAssetCreditDetail.SortType.TOTAL) {
                    if (mSort == itemType) {
                        mAlDataList.add(detail);
                    }
                } else {
                    mAlDataList.add(detail);
                }
            }
        }
        notifyDataSetChanged();
    }


    //====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private LinearLayout	mDetailLayout;
    	private TextView		mDetailName;
    	private TextView		mDetailMethod;
    	private TextView		mDetailTime;
    	private TextView		mDetailCost;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<BodyAssetCardDetail> array){
    	mOriginalDataList = array;
        setSort(mSort);
	}
    

}
