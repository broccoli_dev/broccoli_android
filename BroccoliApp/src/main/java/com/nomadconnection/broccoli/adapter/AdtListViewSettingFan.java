package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.InfoDataSettingNotice;
import com.nomadconnection.broccoli.data.Spend.Qna;

import java.util.ArrayList;
import java.util.List;


public class AdtListViewSettingFan extends BaseAdapter {

	private List<Qna> 	mAlDataList;
	private Activity			  		mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewSettingFan(Activity _activity, List<Qna> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        int count = 0;
        if (mAlDataList != null) {
            count = mAlDataList.size();
        }
        return count;
    }
 
    @Override
    public Qna getItem(int position) {
        Qna info = null;
        if (mAlDataList != null) {
            info = mAlDataList.get(position);
        }
        return info;
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_3_level_setting_notice, parent, false);
//            listRow.mBackGround		= (LinearLayout)convertView.findViewById(R.id.ll_row_main_list_item_background);
            listRow.mPhoto			= (ImageView) convertView.findViewById(R.id.iv_row_list_item_3_level_setting_notice_new_icon);
            listRow.mTitle			= (TextView) convertView.findViewById(R.id.tv_row_list_item_3_level_setting_notice_title);
            listRow.mTime			= (TextView) convertView.findViewById(R.id.tv_row_list_item_3_level_setting_notice_time);
            listRow.mContents			= (TextView) convertView.findViewById(R.id.tv_row_list_item_3_level_setting_notice_contents);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        /* Data 적용 */
//        listRow.mBackGround.setBackgroundColor(Color.parseColor(mAlDataList.get(position).mColor));
//        imageLoader.DisplayImage(mAlDataList.get(position).mPhoto, listRow.mPhoto);
        listRow.mPhoto.setVisibility(View.GONE);
        listRow.mTime.setVisibility(View.GONE);
        listRow.mTitle.setText(mAlDataList.get(position).getQuestion());
//        listRow.mTime.setText(mAlDataList.get(position).mTxt2);
        listRow.mContents.setVisibility(View.GONE);
        listRow.mContents.setText(mAlDataList.get(position).getAnswer());
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
//    	private LinearLayout 	mBackGround;		// Frame(색변경용도)
    	private ImageView       mPhoto;				// Image
    	private TextView		mTitle;
    	private TextView		mTime;
    	private TextView		mContents;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<Qna> array){
    	mAlDataList = array;
	}
    
     
}
