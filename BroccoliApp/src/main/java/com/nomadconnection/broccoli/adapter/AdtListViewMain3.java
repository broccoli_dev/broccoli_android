package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataMain3;
import com.nomadconnection.broccoli.utils.FontClass;

import java.util.ArrayList;

/*
 * Card Use : a, e
 */

public class AdtListViewMain3 extends BaseAdapter {
	
	private static int 			mTotalPageNum 	= 0;
	private static int 			mCurrentPageIdx	= 0;
	private ArrayList<InfoDataMain3> 	mAlDataList;
	private Activity			  		mActivity;

//==========================================================================//
// constructor	
//==========================================================================//
	public AdtListViewMain3(Activity _activity, ArrayList<InfoDataMain3> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public InfoDataMain3 getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_main_3, parent, false);
            init(listRow, convertView);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        dataSet(listRow, position);
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// Init
//====================================================================//
    private void init(ViewHolder _listRow, View _convertView){
    	/* Card A */
    	_listRow.mCardALayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_a);
    	_listRow.mCardATitle = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_title);
    	_listRow.mCardATitleCount = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_title_count);
    	_listRow.mCardAIcon = (ImageView)_listRow.mCardALayout.findViewById(R.id.iv_row_item_inc_home_card_a_icon);
    	_listRow.mCardACost = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_cost);
		_listRow.mCardACost.setTypeface(FontClass.setFont(mActivity));
    	_listRow.mCardASubInfo1 = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_sub_info_1);
    	_listRow.mCardASubInfo2 = (TextView)_listRow.mCardALayout.findViewById(R.id.tv_row_item_inc_home_card_a_sub_info_2);

    	/* Card E */
    	_listRow.mCardELayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_e);
    	_listRow.mCardETitle = (TextView)_listRow.mCardELayout.findViewById(R.id.tv_row_item_inc_home_card_e_title);
    	_listRow.mCardEIcon = (ImageView)_listRow.mCardELayout.findViewById(R.id.iv_row_item_inc_home_card_e_icon);

		/* Card H */
		_listRow.mCardHLayout = (RelativeLayout)_convertView.findViewById(R.id.inc_card_h);
		_listRow.mCardHPagerLayout = (ViewPager)_convertView.findViewById(R.id.row_viewpager_220);
		_listRow.mCardHPagerIndex = (LinearLayout)_convertView.findViewById(R.id.row_page_index_220);
    }
    
    
    
    
//====================================================================//
// Data Set
//====================================================================//
    private void dataSet(final ViewHolder _listRow, int _position){
    	/* Main 3 */
		if (mAlDataList.get(_position).mTxt1.equals("주식")){
			if (mAlDataList.get(_position).mTxt2.equals("y")) {
				_listRow.mCardALayout.setVisibility(View.VISIBLE);
				_listRow.mCardHLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardATitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardACost.setText(mAlDataList.get(_position).mTxt3);
//    			_listRow.mCardASubInfo1.setText(mAlDataList.get(_position).mTxt4);
    			_listRow.mCardASubInfo1.setText("전일대비 수익  (수익률)");
    			_listRow.mCardASubInfo2.setText(mAlDataList.get(_position).mTxt5);
				if(mAlDataList.get(_position).mTxt5.contains("-")){
					_listRow.mCardASubInfo2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stock_down, 0, 0, 0);
					_listRow.mCardASubInfo2.setCompoundDrawablePadding(10);
					_listRow.mCardASubInfo2.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_80_129_229));
				}
				else {
					_listRow.mCardASubInfo2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stock_up, 0, 0, 0);
					_listRow.mCardASubInfo2.setCompoundDrawablePadding(10);
					_listRow.mCardASubInfo2.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_230_77_00));
				}
				_listRow.mCardATitleCount.setText(mAlDataList.get(_position).mTxt8);
				_listRow.mCardAIcon.setBackgroundResource(R.drawable.home_icon_03);
			}
			else{
				_listRow.mCardALayout.setVisibility(View.GONE);
				_listRow.mCardHLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
			}
		}
		else if (mAlDataList.get(_position).mTxt1.equals("펀드")){
			if (mAlDataList.get(_position).mTxt2.equals("y")) {
				_listRow.mCardALayout.setVisibility(View.VISIBLE);
				_listRow.mCardHLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardATitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardACost.setText(mAlDataList.get(_position).mTxt3);
    			_listRow.mCardASubInfo1.setText(mAlDataList.get(_position).mTxt4);
    			_listRow.mCardASubInfo2.setText(mAlDataList.get(_position).mTxt5);
				_listRow.mCardAIcon.setBackgroundResource(R.drawable.home_icon_04);
			}
			else{
				_listRow.mCardALayout.setVisibility(View.GONE);
				_listRow.mCardHLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardETitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
			}
		}
		else if (mAlDataList.get(_position).mTxt1.equalsIgnoreCase("평균 주식 평가액")) {
			if (mAlDataList.get(_position).mTxt2.equals("y")) {
				_listRow.mCardALayout.setVisibility(View.VISIBLE);
				_listRow.mCardHLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardATitle.setText(mAlDataList.get(_position).mTxt1);
				_listRow.mCardACost.setText(mAlDataList.get(_position).mTxt3);
				_listRow.mCardASubInfo1.setText(mAlDataList.get(_position).mTxt4);
				_listRow.mCardASubInfo2.setVisibility(View.GONE);
				_listRow.mCardATitleCount.setVisibility(View.GONE);
				_listRow.mCardAIcon.setBackgroundResource(R.drawable.home_icon_03);
			}
		}
    }
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<InfoDataMain3> array){
    	mAlDataList = array;
	}
    
	/** Page Index **/
	private void initPageIndex(Context context, ViewHolder _listRow){
		try
		{

			_listRow.mCardHPagerIndex.removeAllViews();
			if(mTotalPageNum > 1){
				for(int i=0; i<mTotalPageNum; i++) {
					ImageView iv = new ImageView(context);	//페이지 표시 이미지 뷰 생성
					iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//				iv.setImageResource(getResources().obtainTypedArray(R.array.page_index_items).getResourceId(i, -1));

					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(18, 18);
					lp.setMargins( 0 , 0 , 15 , 0 ); // Left , Top, Right, Bottom

//				if(i==mCurrentPageIdx) iv.setStatus(true);
//				else iv.setStatus(false);
					if(i==mCurrentPageIdx) iv.setBackgroundResource(R.drawable.shape_row_indicator_on);
					else iv.setBackgroundResource(R.drawable.shape_row_indicator_off);

					_listRow.mCardHPagerIndex.addView(iv, lp);
				}
			}
		}
		catch(Exception e){
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initPageIndex() : " + e.toString());
		}
	}


//=======================================================================================//
// Page Index
//=======================================================================================//

//====================================================================//
// ViewHolder
//====================================================================//
    private class ViewHolder {

    	/* Card A */
    	private LinearLayout 	mCardALayout;
    	private TextView		mCardATitle;
    	private TextView		mCardATitleCount;
    	private ImageView 		mCardAIcon;
    	private TextView		mCardACost;
    	private TextView		mCardASubInfo1;
    	private TextView		mCardASubInfo2;

		/* Card H */
		private RelativeLayout mCardHLayout;
		private ViewPager mCardHPagerLayout;
		private LinearLayout mCardHPagerIndex;

    	/* Card B */
//    	private LinearLayout 	mCardBLayout;
//    	private TextView		mCardBTitle;
//    	private TextView		mCardBCost;
//    	private TextView		mCardBSubInfo1;
//    	private TextView		mCardBSubInfo2;

    	/* Card C */
//    	private LinearLayout 	mCardCLayout;
//    	private TextView		mCardCTitle;
//    	private ImageView 		mCardCIcon;
//    	private TextView		mCardCCost;

    	/* Card D */
//    	private LinearLayout 	mCardDLayout;
//    	private TextView		mCardDTitle;
//    	private ImageView 		mCardDIcon;
//    	private TextView		mCardDSubInfo1Cost;
//    	private TextView		mCardDSubInfo1Title;
//    	private TextView		mCardDSubInfo2Cost;
//    	private TextView		mCardDSubInfo2Title;

    	/* Card E */
    	private LinearLayout 	mCardELayout;
    	private TextView		mCardETitle;
    	private ImageView 		mCardEIcon;

    	/* Card F */
//    	private LinearLayout 	mCardFLayout;
//    	private TextView		mCardFTitle;
//    	private ImageView 		mCardFIcon;
//    	private TextView		mCardFSubInfo1Title;
//    	private TextView		mCardFSubInfo1Tag;
//    	private TextView		mCardFSubInfo1Txt;
//    	private TextView		mCardFSubInfo1Cost;
//    	private TextView		mCardFSubInfo2Title;
//    	private TextView		mCardFSubInfo2Tag;
//    	private TextView		mCardFSubInfo2Txt;
//    	private TextView		mCardFSubInfo2Cost;
//    	private TextView		mCardFSubInfo3Title;
//    	private TextView		mCardFSubInfo3Tag;
//    	private TextView		mCardFSubInfo3Txt;
//    	private TextView		mCardFSubInfo3Cost;

    	/* Card G */
//    	private LinearLayout 	mCardGLayout;
//    	private TextView		mCardGTitle;
//    	private ImageView 		mCardGIcon;
//    	private TextView		mCardGSubInfo1date;
//    	private TextView		mCardGSubInfo1Txt1;
//    	private TextView		mCardGSubInfo1Txt1Tag;
//    	private TextView		mCardGSubInfo1Txt2;
//    	private TextView		mCardGSubInfo1Txt3;
//    	private TextView		mCardGSubInfo1Cost;
//    	private TextView		mCardGSubInfo2date;
//    	private TextView		mCardGSubInfo2Txt1;
//    	private TextView		mCardGSubInfo2Txt1Tag;
//    	private TextView		mCardGSubInfo2Txt2;
//    	private TextView		mCardGSubInfo2Txt3;
//    	private TextView		mCardGSubInfo2Cost;
//    	private TextView		mCardGSubInfo3date;
//    	private TextView		mCardGSubInfo3Txt1;
//    	private TextView		mCardGSubInfo3Txt1Tag;
//    	private TextView		mCardGSubInfo3Txt2;
//    	private TextView		mCardGSubInfo3Txt3;
//    	private TextView		mCardGSubInfo3Cost;
//    	private TextView		mCardGSubInfo4date;
//    	private TextView		mCardGSubInfo4Txt1;
//    	private TextView		mCardGSubInfo4Txt1Tag;
//    	private TextView		mCardGSubInfo4Txt2;
//    	private TextView		mCardGSubInfo4Txt3;
//    	private TextView		mCardGSubInfo4Cost;

    	/* Card H */
//    	private LinearLayout 	mCardHLayout;
//    	private TextView		mCardHTitle;
//    	private TextView		mCardHTitleCount;
//    	private ImageView 		mCardHIcon;
//    	private TextView		mCardHSubInfoTitle;
//    	private TextView		mCardHSubInfoCost;
//    	private LinearLayout	mCardHPageIndex;

    	/* Card I */
//    	private LinearLayout 	mCardILayout;
//    	private TextView		mCardITitle;
//    	private TextView		mCardITitleCount;
//    	private ImageView 		mCardIIcon;
//    	private TextView		mCardISubInfo1Title;
//    	private TextView		mCardISubInfo1Cost;
//    	private TextView		mCardISubInfo2Title;
//    	private TextView		mCardISubInfo2Cost;

    	/* Card J */
//    	private LinearLayout 	mCardJLayout;
//    	private TextView		mCardJTitle;
//    	private TextView		mCardJTitleCount;
//    	private ImageView 		mCardJIcon;
//    	private TextView		mCardJSubInfo1Title;
//    	private TextView		mCardJSubInfo1Cost;
//    	private TextView		mCardJSubInfo2Title;
//    	private TextView		mCardJSubInfo2Cost;
//    	private TextView		mCardJSubInfo3Title;
//    	private TextView		mCardJSubInfo3Cost;

    	/* Card K */
//    	private LinearLayout 	mCardKLayout;
//    	private TextView		mCardKTitle;
//    	private ImageView 		mCardKIcon;
//    	private TextView		mCardKSubInfo1Title;
//    	private TextView		mCardKSubInfo1Cost;
//    	private TextView		mCardKSubInfo2Title;
//    	private TextView		mCardKSubInfo2Cost;
//    	private TextView		mCardKSubInfo3Title;
//    	private TextView		mCardKSubInfo3Cost;

    }
     
}
