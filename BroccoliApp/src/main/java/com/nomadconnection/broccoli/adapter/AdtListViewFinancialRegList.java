package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.common.dialog.CommonDialogListType;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class AdtListViewFinancialRegList extends BaseExpandableListAdapter {

    private Context mContext;
    private ArrayList<String> mGroupList = null;
    private HashMap<String, ArrayList<SelectBankData>> mChildList = null;

    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;
    private OnMoreItemClickListener mListener;

    public interface OnMoreItemClickListener {
        void refresh(SelectBankData data);
        void setting(SelectBankData data);
        void delete(SelectBankData data);
    }

    public AdtListViewFinancialRegList(Context context,ArrayList<String> group, HashMap<String, ArrayList<SelectBankData>> child, OnMoreItemClickListener listener) {
        mContext = context;
        inflater = LayoutInflater.from(context);
        mGroupList = group;
        mChildList = child;
        mListener = listener;
    }

    public void setData(ArrayList<String> group, HashMap<String, ArrayList<SelectBankData>> child) {
        mGroupList = group;
        mChildList = child;
    }

    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }

    @Override
    public int getGroupCount(){
        int count = 0;
        if (mGroupList != null) {
            count = mGroupList.size();
        }
        return count;
    }

    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;

        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_group_asset_stock, parent, false);
            mViewHolder.mGroupTitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_asset_stock_title);
            mViewHolder.mGroupErrorCount = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_group_error_count);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        String title = getGroup(groupPosition);
        if (title != null && title.contains("오류")) {
            int count = getChildrenCount(groupPosition);
            mViewHolder.mGroupErrorCount.setText(""+count);
            mViewHolder.mGroupErrorCount.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.mGroupErrorCount.setVisibility(View.GONE);
        }

        mViewHolder.mGroupTitle.setText(getGroup(groupPosition));

        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int count = 0;
        if (mChildList != null) {
            count = mChildList.get(mGroupList.get(groupPosition)).size();
        }
        return count;
    }

    @Override
    public SelectBankData getChild(int groupPosition, int childPosition) {
        return mChildList.get(mGroupList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;

        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_financial_reg, null);
            mViewHolder.mBankName = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_financial_bank);
            mViewHolder.mBankNameIv = (ImageView) v.findViewById(R.id.lv_row_expandable_listview_item_child_financial_bank);
            mViewHolder.mSetting = v.findViewById(R.id.ll_row_item_financial_setting);
            mViewHolder.mStatus = (TextView) v.findViewById(R.id.tv_row_listview_item_financial_status);
            mViewHolder.mStatusLayout = v.findViewById(R.id.fl_row_listview_item_financial_status);
            mViewHolder.mLoading = (ImageView)v.findViewById(R.id.iv_loading_small_img);
            v.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder)v.getTag();
        }

        SelectBankData data = getChild(groupPosition, childPosition);
        if(data.getType().equalsIgnoreCase(SelectBankData.TYPE_BANK)){
            List<String> mBankName = Arrays.asList(mContext.getResources().getStringArray(R.array.bank_name));
            for(int i = 0; i < mBankName.size(); i++){
                if(data.getName().equals(mBankName.get(i))){
                    mViewHolder.mBankNameIv.setVisibility(View.VISIBLE);
                    mViewHolder.mBankName.setVisibility(View.GONE);
                    TypedArray imgs = null;
                    if (data.getBankStatus() == null || data.getBankStatus().equals(BankData.NONE)) {
                        imgs = mContext.getResources().obtainTypedArray(R.array.bank_name_image);
                    } else {
                        imgs = mContext.getResources().obtainTypedArray(R.array.bank_name_image_dim_20);
                    }

                    // get resource ID by index
                    imgs.getResourceId(i, -1);

                    // or set you ImageView's resource to the id
                    mViewHolder.mBankNameIv.setImageResource(imgs.getResourceId(i, -1));

                    // recycle the array
                    imgs.recycle();
                }
            }
        } else if(data.getType().equalsIgnoreCase(SelectBankData.TYPE_CARD)){
            List<String> mBankName = Arrays.asList(mContext.getResources().getStringArray(R.array.card_name));
            for(int i = 0; i < mBankName.size(); i++){
                if(data.getName().equals(mBankName.get(i))){
                    mViewHolder.mBankNameIv.setVisibility(View.VISIBLE);
                    mViewHolder.mBankName.setVisibility(View.GONE);
                    TypedArray imgs = null;
                    if (data.getBankStatus() == null || data.getBankStatus().equals(BankData.NONE)) {
                        imgs = mContext.getResources().obtainTypedArray(R.array.card_name_image);
                    } else {
                        imgs = mContext.getResources().obtainTypedArray(R.array.card_name_image_dim_20);
                    }

                    // get resource ID by index
                    imgs.getResourceId(i, -1);

                    // or set you ImageView's resource to the id
                    mViewHolder.mBankNameIv.setImageResource(imgs.getResourceId(i, -1));

                    // recycle the array
                    imgs.recycle();
                }
            }
        } else {
            mViewHolder.mBankName.setText(data.getName());
            mViewHolder.mBankName.setVisibility(View.VISIBLE);
            mViewHolder.mBankNameIv.setVisibility(View.GONE);
            if (data.getBankStatus() == null || data.getBankStatus().equals(BankData.NONE)) {
                mViewHolder.mBankName.setTextColor(mViewHolder.mBankName.getResources().getColor(R.color.common_maintext_color));
            } else {
                mViewHolder.mBankName.setTextColor(mViewHolder.mBankName.getResources().getColor(R.color.subitem_left_txt_dim_color));
            }
        }

        mViewHolder.mSetting.setTag(data);
        mViewHolder.mSetting.setOnClickListener(mMoreItemClickListener);

        if (BankData.LOGIN_ID.equalsIgnoreCase(data.getLoginMethod())) {

        } else {

        }
        if (data.getBankStatus() == null || data.getBankStatus().equals(BankData.NONE)) {
            mViewHolder.mStatusLayout.setVisibility(View.GONE);
        } else if (data.getBankStatus().equals(BankData.FAIL_AUTH)) {
            mViewHolder.mStatusLayout.setVisibility(View.VISIBLE);
            if (data.getType().equals(SelectBankData.TYPE_CASH)) {
                mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_cash_auth_fail));
            }else if(data.getType().equals(SelectBankData.TYPE_CARD)){
                mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_card_auth_fail));
            } else {
                mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_auth_fail));
            }
        } else if (data.getBankStatus().equals(BankData.FAIL)) {
            mViewHolder.mStatusLayout.setVisibility(View.VISIBLE);
            mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_fail));
        } else if (data.getBankStatus().equals(BankData.FAIL_NETWORK)) {
            mViewHolder.mStatusLayout.setVisibility(View.VISIBLE);
            mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_network_fail));
        } else if (data.getBankStatus().equals(BankData.FAIL_SITERENEW)) {
            mViewHolder.mStatusLayout.setVisibility(View.VISIBLE);
            if (data.getType().equals(SelectBankData.TYPE_CARD)) {
                mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_card_siterenew_fail));
            } else {
                mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_auth_siterenew_fail));
            }
        } else if (data.getBankStatus().equalsIgnoreCase(BankData.FAIL_PASSWORD)) {
            mViewHolder.mStatusLayout.setVisibility(View.VISIBLE);
            mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_login_auth_fail));
        } else {
            mViewHolder.mStatusLayout.setVisibility(View.GONE);
        }

//        mViewHolder.mRetryBtn.setTag(data);
//        mViewHolder.mRetryBtn.setOnClickListener(mOnRetryListener);

        if (data.getBankStatus() == null) {
            mViewHolder.mLoading.setVisibility(View.GONE);
            AnimationDrawable frameAnimation = (AnimationDrawable) mViewHolder.mLoading.getDrawable();
            frameAnimation.setCallback(mViewHolder.mLoading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
        } else if (data.getBankStatus().equals(BankData.NONE)) {
            if(data.isLoading()){
                mViewHolder.mLoading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) mViewHolder.mLoading.getDrawable();
                frameAnimation.setCallback(mViewHolder.mLoading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
            } else {
                mViewHolder.mLoading.setVisibility(View.GONE);
            }
        } else {
            if(data.isLoading()){
                mViewHolder.mLoading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) mViewHolder.mLoading.getDrawable();
                frameAnimation.setCallback(mViewHolder.mLoading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
            }
            else {
                if (data.getBankStatus().equals(BankData.SCRAPING)) {
                    mViewHolder.mStatusLayout.setVisibility(View.VISIBLE);
                    mViewHolder.mStatus.setText(mContext.getString(R.string.setting_financial_status_init_fail));
                }
                mViewHolder.mLoading.setVisibility(View.GONE);
            }
        }
        return v;
    }

    class ViewHolder {
        /* group */
        TextView mGroupTitle;
        TextView mGroupErrorCount;

        TextView mBankName;
        ImageView mBankNameIv;
        View mSetting;
        TextView mStatus;
        View mStatusLayout;
        ImageView mLoading;
    }

    private View.OnClickListener mMoreItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SelectBankData data = (SelectBankData) view.getTag();
            showMenuDialog(data);
        }
    };

    private void showMenuDialog(final SelectBankData data) {
        final ArrayList<String> menu = new ArrayList<>();
        for (int i=0; i <= 2; i++) {
            if (i == 0) {
                if (!(BankData.LOGIN_ID.equalsIgnoreCase(data.getLoginMethod()) &&
                        (BankData.FAIL_AUTH.equalsIgnoreCase(data.getBankStatus()) || BankData.FAIL_PASSWORD.equalsIgnoreCase(data.getBankStatus())))
                        ) {
                    menu.add(mContext.getResources().getString(R.string.setting_financial_more_menu_refresh));
                }
            } else if (i == 1) {
                menu.add(mContext.getResources().getString(R.string.setting_financial_more_menu_setting));
            } else {
                menu.add(mContext.getResources().getString(R.string.setting_financial_more_menu_unregister));
            }
        }
        final CommonDialogListType menuDialog= new CommonDialogListType(mContext, null, new int[]{R.string.common_cancel},  menu);
        menuDialog.setDialogListener(new CommonDialogListType.DialogListTypeListener() {
            @Override
            public void onButtonClick(DialogInterface dialog, int stringResId) {
                menuDialog.dismiss();
            }

            @Override
            public void onItemClick(DialogInterface dialog, int itemPosition) {
                if (!(BankData.LOGIN_ID.equalsIgnoreCase(data.getLoginMethod()) &&
                        (BankData.FAIL_AUTH.equalsIgnoreCase(data.getBankStatus()) || BankData.FAIL_PASSWORD.equalsIgnoreCase(data.getBankStatus())))
                        ) {
                    switch (itemPosition) {
                        case 0:
                            mListener.refresh(data);
                            break;
                        case 1:
                            mListener.setting(data);
                            break;
                        case 2:
                            mListener.delete(data);
                            break;
                    }
                } else {
                    switch (itemPosition) {
                        case 0:
                            mListener.setting(data);
                            break;
                        case 1:
                            mListener.delete(data);
                            break;
                        case 2:
                            break;
                    }
                }
                dialog.dismiss();
            }
        });
        menuDialog.show();
    }
}
