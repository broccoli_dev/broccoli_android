package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.SpendHistoryHeader;
import com.nomadconnection.broccoli.data.Spend.SpendMonthlyInfo;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.HashMap;


public class AdtListViewSpendHistoryA extends BaseExpandableListAdapter {

	private HashMap<Integer, ArrayList<SpendMonthlyInfo>> mMap;
    private ArrayList<SpendHistoryHeader> mHeaderList;
	private Activity			  		mActivity;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewSpendHistoryA(Activity _activity) {
		mActivity 		= _activity;
	}
	
 	
//====================================================================//
// override method
//====================================================================//

    private int getHeader(int groupPosition) {
        int index = 0;
        if (mHeaderList != null) {
            SpendHistoryHeader header = mHeaderList.get(groupPosition);
            index = header.getDay();
        }
        return index;
    }

    @Override
    public int getGroupCount() {
        int count = 0;
        if (mHeaderList != null) {
            count = mHeaderList.size();
        }
        return count;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int count = 0;
        if (mMap != null) {
            ArrayList<SpendMonthlyInfo> list = mMap.get(getHeader(groupPosition));
            if (list != null)
                count = list.size();
        }
        return count;
    }

    @Override
    public Object getGroup(int groupPosition) {
        Object obj = null;
        if (mHeaderList != null) {
            obj = mHeaderList.get(groupPosition);
        }
        return obj;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Object obj = null;
        if (mMap != null) {
            ArrayList<SpendMonthlyInfo> list = mMap.get(getHeader(groupPosition));
            if (list != null)
                obj = list.get(childPosition);
        }
        return obj;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder listRow;
        LayoutInflater inflater = mActivity.getLayoutInflater();

        if(convertView==null) {
            listRow = new GroupViewHolder();
            convertView = inflater.inflate(R.layout.row_list_item_spend_history_a_header, parent, false);

            listRow.mDate		= (TextView) convertView.findViewById(R.id.tv_row_list_item_spend_history_header_date);
            listRow.mCost       = (TextView) convertView.findViewById(R.id.tv_row_list_item_spend_history_header_cost);
            convertView.setTag(listRow);
        }
        else {
            listRow = (GroupViewHolder) convertView.getTag();
        }

        SpendHistoryHeader header = mHeaderList.get(groupPosition);
        listRow.mDate.setText(header.getHeaderDate());
        ArrayList<SpendMonthlyInfo> list = mMap.get(getHeader(groupPosition));
        if (list == null || list.size() == 0) {
            listRow.mCost.setText("0");
        } else {
            long total = 0;
            for (SpendMonthlyInfo info : list) {
                total += Long.valueOf(info.getSum());
            }
            listRow.mCost.setText(ElseUtils.getDecimalFormat(total));
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder listRow;
        LayoutInflater inflater = mActivity.getLayoutInflater();

        if(convertView==null) {
            listRow = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_list_item_spend_history_a, parent, false);

            listRow.mDetailLayout		= (RelativeLayout) convertView.findViewById(R.id.tv_row_list_item_spend_history_layout);
            listRow.mDetailIcon       = (ImageView) convertView.findViewById(R.id.imv_row_item_spend_history_icon);
            listRow.mDetailName			= (TextView) convertView.findViewById(R.id.tv_row_list_item_sped_history_name);
            listRow.mDetailMethod	= (TextView) convertView.findViewById(R.id.tv_row_list_sped_history_method);
            listRow.mDetailTime		= (TextView) convertView.findViewById(R.id.tv_row_list_item_sped_history_time);
            listRow.mDetailCost		= (TextView) convertView.findViewById(R.id.tv_row_list_item_sped_history_cost);
            listRow.mDetailMemo     = (ImageView) convertView.findViewById(R.id.iv_row_list_item_sped_history_memo);
            convertView.setTag(listRow);
        }
        else {
            listRow = (ViewHolder)convertView.getTag();
        }


        /* Data 적용 */
        String[] payType = mActivity.getResources().getStringArray(R.array.spend_pay_type);
        int payTypeInt = 0;
        SpendMonthlyInfo info = mMap.get(getHeader(groupPosition)).get(childPosition);
        if(Integer.parseInt(info.getPayType()) > 5){
            payTypeInt = 5;
        }
        else {
            payTypeInt = Integer.parseInt(info.getPayType());
        }
        int CategoryTypeInt = Integer.parseInt(info.getCategoryId());
        TypedArray category = mActivity.getResources().obtainTypedArray(R.array.spend_category_list_img);
        category.getResourceId(CategoryTypeInt, -1);

        listRow.mDetailIcon.setImageResource(category.getResourceId(CategoryTypeInt, R.drawable.list_icon_25));
        category.recycle();

        listRow.mDetailName.setText(info.getOpponent());
        switch (payTypeInt){
            case 1: //신용카드
                listRow.mDetailMethod.setTextAppearance(mActivity, R.style.CS_TagA1);
                listRow.mDetailMethod.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
                break;
            case 2: //체크카드
                listRow.mDetailMethod.setTextAppearance(mActivity, R.style.CS_TagA2);
                listRow.mDetailMethod.setBackgroundResource(R.drawable.shape_tag_a_bg_2);
                break;
            case 3: //현금
                listRow.mDetailMethod.setTextAppearance(mActivity, R.style.CS_TagA4);
                listRow.mDetailMethod.setBackgroundResource(R.drawable.shape_tag_a_bg_4);
                break;
            case 4: //계좌이체
                listRow.mDetailMethod.setTextAppearance(mActivity, R.style.CS_TagA3);
                listRow.mDetailMethod.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
                break;
            case 5: //기타
                listRow.mDetailMethod.setTextAppearance(mActivity, R.style.CS_TagA3);
                listRow.mDetailMethod.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
                break;
        }
        listRow.mDetailMethod.setText(payType[payTypeInt]);
        listRow.mDetailTime.setText(ElseUtils.getSpendDate(info.getExpenseDate()));
        listRow.mDetailCost.setText(ElseUtils.getStringDecimalFormat(info.getSum()));
        if (info.getMemo() != null && !TextUtils.isEmpty(info.getMemo())) {
            listRow.mDetailMemo.setVisibility(View.VISIBLE);
        } else {
            listRow.mDetailMemo.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    //====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private RelativeLayout	mDetailLayout;
        private ImageView      mDetailIcon;
    	private TextView		mDetailName;
    	private TextView		mDetailMethod;
    	private TextView		mDetailTime;
    	private TextView		mDetailCost;
        private ImageView       mDetailMemo;
    }

    private class GroupViewHolder {
        private TextView mDate;
        private TextView mCost;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<SpendHistoryHeader> headerList, HashMap<Integer, ArrayList<SpendMonthlyInfo>> map){
        mHeaderList = headerList;
    	mMap = map;
	}
    

}
