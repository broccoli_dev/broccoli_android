package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoliraonsecure.KSW_CertItem;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 8. 4..
 */
public class AdtListViewAuthMgrList extends BaseAdapter {

    private Context mContext;
    private ArrayList<KSW_CertItem> mList;

    public AdtListViewAuthMgrList(Context context, ArrayList<KSW_CertItem> list) {
        mContext = context.getApplicationContext();
        setData(list);
    }

    public void setData(ArrayList<KSW_CertItem> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (mList != null) {
            count = mList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        if (mList != null) {
            object = mList.get(position);
        }
        return object;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.row_cert_auth_manager, null);
            holder = new ViewHolder();
            holder.mImageView = (ImageView) view.findViewById(R.id.iv_certStatus);
            holder.mErrorIcon = (ImageView) view.findViewById(R.id.iv_certStatusError);
            holder.mSubjectName = (TextView) view.findViewById(R.id.tv_certname);
            holder.mPolicy = (TextView) view.findViewById(R.id.tv_cert_policy);
            holder.mIssuerName = (TextView) view.findViewById(R.id.tv_cert_issuername);
            holder.mExpireTime = (TextView) view.findViewById(R.id.tv_cert_expire_time);
            holder.mDisable = (ImageView) view.findViewById(R.id.iv_disable_view);
            view.setTag(holder);
            convertView = view;
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String name = (String)mList.get(position).get(KSW_CertItem.SUBJECTNAME);
        String[] names = name.split("\\(");
        if (names != null && names.length > 0) {
            holder.mSubjectName.setText(names[0]);
        } else {
            holder.mSubjectName.setText(name);
        }

        holder.mPolicy.setText((String)mList.get(position).get(KSW_CertItem.POLICY));
        holder.mIssuerName.setText((String)mList.get(position).get(KSW_CertItem.ISSUERNAME));
        holder.mExpireTime.setText((String)mList.get(position).get(KSW_CertItem.EXPIREDTIME));

        if (mList.get(position).isAvailable()) {
            holder.mErrorIcon.setVisibility(View.INVISIBLE);
        } else {
            holder.mErrorIcon.setVisibility(View.VISIBLE);
        }

        if (mList.get(position).isContainRealName()) {
            convertView.setEnabled(true);
            holder.mDisable.setVisibility(View.GONE);
        } else {
            convertView.setEnabled(false);
            holder.mDisable.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    private class ViewHolder {
        private ImageView mImageView;
        private ImageView mErrorIcon;
        private TextView mSubjectName;
        private TextView mPolicy;
        private TextView mIssuerName;
        private TextView mExpireTime;
        private ImageView mDisable;
    }
}
