package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.InfoDataAssetOther;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

public class AdtExpListViewAssetOther extends BaseExpandableListAdapter {

    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<InfoDataAssetOther>> mChildList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;

    public AdtExpListViewAssetOther(Context c, ArrayList<String> groupList, ArrayList<ArrayList<InfoDataAssetOther>> childList){
        super();
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_group_asset_other, parent, false);
            mViewHolder.mGroupTitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_asset_other_title);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        mViewHolder.mGroupTitle.setText(getGroup(groupPosition));

        return v;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public InfoDataAssetOther getChild(int groupPosition, int childPosition){
//    	public String getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_asset_other, null);
            mViewHolder.mInfo1 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_info_1);
            mViewHolder.mInfo2 = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_info_2);
            mViewHolder.mInfoCost = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_cost);
            mViewHolder.mInfoQuantity = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_asset_other_quantity);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }
 
        mViewHolder.mInfo1.setText(getChild(groupPosition, childPosition).mTxt2);
        if (getChild(groupPosition, childPosition).mTxt1.equalsIgnoreCase("부동산")) {
            mViewHolder.mInfo2.setText(TransFormatUtils.transDealTypePosToName(getChild(groupPosition, childPosition).mTxt3));
            mViewHolder.mInfoCost.setText(TransFormatUtils.getDecimalFormatRecvString(getChild(groupPosition, childPosition).mTxt4));
            mViewHolder.mInfoQuantity.setVisibility(View.GONE);
        } else if (getChild(groupPosition, childPosition).mTxt1.equalsIgnoreCase("기타")) {
            mViewHolder.mInfoQuantity.setVisibility(View.VISIBLE);
            mViewHolder.mInfo2.setText(getChild(groupPosition, childPosition).mTxt3);
            mViewHolder.mInfoCost.setText(TransFormatUtils.getDecimalFormatRecvString(getChild(groupPosition, childPosition).mTxt5));
            mViewHolder.mInfoQuantity.setText(getChild(groupPosition, childPosition).mTxt4);
        } else {
            mViewHolder.mInfo2.setText(getChild(groupPosition, childPosition).mTxt3);
            mViewHolder.mInfoCost.setText(TransFormatUtils.getDecimalFormatRecvString(getChild(groupPosition, childPosition).mTxt4));
            mViewHolder.mInfoQuantity.setVisibility(View.GONE);
        }

        if (isLastChild) ((View)v.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)v.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return v;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
 
 
    class ViewHolder{
    	/* group */
        public TextView mGroupTitle;

        /* child */
        public TextView mInfo1;
        public TextView mInfo2;
        public TextView mInfoCost;
        public TextView mInfoQuantity;
    }

    public void setData(ArrayList<String> groupList, ArrayList<ArrayList<InfoDataAssetOther>> childList) {
        this.mGroupList = groupList;
        this.mChildList = childList;
        notifyDataSetChanged();
    }
}
