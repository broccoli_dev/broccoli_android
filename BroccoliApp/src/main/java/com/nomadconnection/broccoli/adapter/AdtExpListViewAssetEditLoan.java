package com.nomadconnection.broccoli.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kyleduo.switchbutton.SwitchButton;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.AssetDebtInfo;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

public class AdtExpListViewAssetEditLoan extends BaseExpandableListAdapter {

    private ArrayList<Integer> mGroupList = null;
    private ArrayList<ArrayList<AssetDebtInfo>> mChildList = null;
    private LayoutInflater inflater = null;
    private CompoundButton.OnCheckedChangeListener mListener;

    public AdtExpListViewAssetEditLoan(Context c, ArrayList<Integer> groupList, ArrayList<ArrayList<AssetDebtInfo>> childList, CompoundButton.OnCheckedChangeListener listener){
        super();
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
        this.mListener = listener;
    }

    @Override
    public Integer getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_expandable_listview_item_group_asset_das, parent, false);
            viewHolder.mGroupTitle = (TextView)convertView.findViewById(R.id.tv_row_expandable_listview_item_group_asset_das_title);
            viewHolder.mGroupCount = (TextView)convertView.findViewById(R.id.tv_row_expandable_listview_item_group_asset_das_title_count);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        if (getGroup(groupPosition) == AssetDebtInfo.LOAN) {
            viewHolder.mGroupTitle.setText(R.string.asset_loan_title_loan);
        } else {
            viewHolder.mGroupTitle.setText(R.string.asset_loan_title_card_loan);
        }
        viewHolder.mGroupCount.setText(String.valueOf(getChildrenCount(groupPosition)));

        return convertView;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public AssetDebtInfo getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_expandable_listview_item_child_asset_edit_loan, null);

            viewHolder.mTopLayout  = (LinearLayout) convertView.findViewById(R.id.ll_row_expandable_listview_item_child_asset_loan_top_layout);
            viewHolder.mBankName   = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_loan_name);
            viewHolder.mBankAccountNum = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_child_asset_loan_account_num);
            viewHolder.mAlertIcon  = (ImageView) convertView.findViewById(R.id.iv_row_expandable_listview_item_child_asset_loan_alert_icon);
            viewHolder.mSwitchBtn  = (SwitchButton) convertView.findViewById(R.id.switch_expandable_item_child_asset_edit_loan);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        AssetDebtInfo bankInfo = getChild(groupPosition, childPosition);
        if (bankInfo.mAdvertise == AdtExpListViewAssetLoan.ITEM_ADVERTISE) {
            convertView.setVisibility(View.GONE);
        } else {
            convertView.setVisibility(View.VISIBLE);
            viewHolder.mBankName.setText(TransFormatUtils.transBankCodeToName(getChild(groupPosition, childPosition).mAssetDebtCode));
            if (bankInfo.mAssetDebtAccountNumber == null || bankInfo.mAssetDebtAccountNumber.isEmpty()) {
                viewHolder.mBankAccountNum.setVisibility(View.GONE);
            } else {
                viewHolder.mBankAccountNum.setVisibility(View.VISIBLE);
                viewHolder.mBankAccountNum.setText(bankInfo.mAssetDebtAccountNumber);
            }
            viewHolder.mAlertIcon.setVisibility(View.GONE);
            viewHolder.mSwitchBtn.setOnCheckedChangeListener(null);
            viewHolder.mSwitchBtn.setAnimationDuration(0);
            viewHolder.mSwitchBtn.setChecked(bankInfo.isChecked());
            viewHolder.mSwitchBtn.setTag(bankInfo);
            viewHolder.mSwitchBtn.setOnCheckedChangeListener(mListener);

            if (bankInfo.mLoanType == AssetDebtInfo.LOAN_CARD) {
                viewHolder.mSwitchBtn.setVisibility(View.GONE);
                viewHolder.mBankName.setTextColor(Color.argb(128, 89, 89, 89));
            } else {
                viewHolder.mSwitchBtn.setVisibility(View.VISIBLE);
                viewHolder.mBankName.setTextColor(viewHolder.mBankName.getResources().getColorStateList(R.color.selector_button_type_b_text_color_change));
            }

            if (isLastChild) (convertView.findViewById(R.id.view_divider)).setVisibility(View.GONE);
            else (convertView.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);
        }
        return convertView;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
 
 
    class ViewHolder{
    	/* group */
        public TextView mGroupTitle;
        public TextView mGroupCount;

        /* child */
        private LinearLayout    mTopLayout;
        private TextView		mBankName;
        private TextView		mBankAccountNum;
        private ImageView       mAlertIcon;
        private SwitchButton    mSwitchBtn;
    }
}
