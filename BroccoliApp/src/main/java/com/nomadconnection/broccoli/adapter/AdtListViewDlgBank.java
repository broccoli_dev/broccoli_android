package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeBank;
import com.nomadconnection.broccoli.data.Spend.ResponseCardList;

import java.util.List;


public class AdtListViewDlgBank extends BaseAdapter {

	private Activity mActivity;
	private ViewHolder mViewHolder;

	private List<EtcChallengeBank> mEtcChallengeBankList;
	private int mSelectValue = 0;

	public AdtListViewDlgBank(Activity _Activity, List<EtcChallengeBank> _EtcChallengeBankList){
		mActivity = _Activity;
		mEtcChallengeBankList = _EtcChallengeBankList;
	}

	@Override
	public int getCount() {
		return mEtcChallengeBankList.size();
	}

	@Override
	public EtcChallengeBank getItem(int position) {
		return mEtcChallengeBankList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View mView = convertView;
		LayoutInflater inflator = mActivity.getLayoutInflater();
		
		if(mView == null){
			mViewHolder = new ViewHolder();
			mView = inflator.inflate(R.layout.row_grid_item_bank, null);
			
			mViewHolder.mLayout = (RelativeLayout)mView.findViewById(R.id.ll_row_grid_item_bank_layout);
			mViewHolder.mRadioButton = (RadioButton)mView.findViewById(R.id.rb_row_grid_item_bank_icon);
			mViewHolder.mRadioButton.setFocusable(false);
			mViewHolder.mRadioButton.setClickable(false);
			mViewHolder.mRadioButton.setChecked(((ListView) parent).isItemChecked(position));

			mViewHolder.mTitle = (TextView)mView.findViewById(R.id.tv_row_grid_item_bank_name);
			mViewHolder.mSubTitle = (TextView)mView.findViewById(R.id.tv_row_grid_item_bank_account);

			mView.setTag(mViewHolder);
		}
		else{
			mViewHolder = (ViewHolder)mView.getTag();
		}

		/* 값 입력 */
		String name = mEtcChallengeBankList.get(position).getCompanyName()
				+  " " + mEtcChallengeBankList.get(position).getAccountName();

		mViewHolder.mRadioButton.setChecked(mSelectValue == position);

		if(mEtcChallengeBankList.get(position).getAddedYn().equals("Y")){
			mViewHolder.mTitle.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_191_191_191));
			mViewHolder.mSubTitle.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_191_191_191));
		} else {
			mViewHolder.mTitle.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_89_89_89));
			mViewHolder.mSubTitle.setTextColor(mActivity.getResources().getColor(R.color.common_rgb_153_153_153));
		}

		mViewHolder.mTitle.setText(name);
		mViewHolder.mSubTitle.setText(mEtcChallengeBankList.get(position).getAccountNum());

		return mView;
	}

	public void setSelectValue(int _selectValue){
		mSelectValue = _selectValue;
	}


//====================================================================//
// ViewHolder
//====================================================================//   
	private class ViewHolder {
		private RelativeLayout mLayout;
		private RadioButton mRadioButton;
		private TextView  mTitle;
		private TextView  mSubTitle;
	}
}
