package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Asset.AssetBankInfo;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;


public class AdtListViewAssetDaS extends BaseAdapter {
	
	private ArrayList<AssetBankInfo> 	mAlDataList;
	private Activity			  		mActivity;
	
//==========================================================================//
// constructor	
//==========================================================================//
	public AdtListViewAssetDaS(Activity _activity, ArrayList<AssetBankInfo> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public AssetBankInfo getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_asset_das, parent, false);
            listRow.mBankName			= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_das_bank_name);
//            listRow.mBankAccountName	= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_das_bank_account_name);
            listRow.mBankAccountNum		= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_das_bank_account_num);
            listRow.mAlertIcon			= (ImageView) convertView.findViewById(R.id.iv_row_list_item_asset_das_alert_icon);
            listRow.mBankCost		= (TextView) convertView.findViewById(R.id.tv_row_list_item_asset_das_bank_cost);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        listRow.mBankName.setText(mAlDataList.get(position).mAssetBankAccountName);
//        listRow.mBankName.setText(mAlDataList.get(position).mAssetBankAccountName +" "+mAlDataList.get(position).mTxt2);
//        listRow.mBankAccountName.setText(mAlDataList.get(position).mTxt2);
        listRow.mBankAccountNum.setText(mAlDataList.get(position).mAssetBankAccountNumber);
        listRow.mAlertIcon.setVisibility(View.GONE);
        listRow.mBankCost.setText(TransFormatUtils.getDecimalFormatRecvString(mAlDataList.get(position).mAssetBankSum));
        if (position == mAlDataList.size()-1) ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)convertView.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private TextView		mBankName;
//    	private TextView		mBankAccountName;
    	private TextView		mBankAccountNum;
    	private ImageView 		mAlertIcon;
    	private TextView		mBankCost;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<AssetBankInfo> array){
    	mAlDataList = array;
	}
    
     
}
