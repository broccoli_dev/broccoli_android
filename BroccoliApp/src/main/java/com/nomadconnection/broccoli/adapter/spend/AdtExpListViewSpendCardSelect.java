package com.nomadconnection.broccoli.adapter.spend;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.common.CustomExpandableListView;
import com.nomadconnection.broccoli.data.Spend.ResponseCardList;
import com.nomadconnection.broccoli.utils.hangulutil.KoreanText;
import com.nomadconnection.broccoli.utils.hangulutil.KoreanTextMatch;
import com.nomadconnection.broccoli.utils.hangulutil.KoreanTextMatcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by YelloHyunminJang on 2017. 1. 19..
 */

public class AdtExpListViewSpendCardSelect extends BaseExpandableListAdapter implements SectionIndexer {

    private enum CARD_MODE {
        INPUT_MODE,
        SEARCH_MODE
    }

    private HashMap<String, ArrayList<ResponseCardList>> mMap;
    private ArrayList<String> mHeaderList;
    private HashMap<String, ArrayList<ResponseCardList>> mOriginalMap;
    private ArrayList<String> mOriginalList;
    private ArrayList<ResponseCardList> mDataList;
    private HashMap<String, ArrayList<ResponseCardList>> mSearchMap;
    private Context mContext;
    private String mOriginSelectedCardCode = "";
    private String mOriginSelectedCardHeader = "";
    private int mOriginSelectedCardIndex = 0;
    private Object[] sections;
    private CARD_MODE mode = CARD_MODE.INPUT_MODE;
    private String mFilter;

    public AdtExpListViewSpendCardSelect(Context context) {
        mContext = context;
    }


//====================================================================//
// override method
//====================================================================//

    private String getHeader(int groupPosition) {
        String key = null;
        if (mHeaderList != null) {
            key = mHeaderList.get(groupPosition);
        }
        return key;
    }

    @Override
    public int getGroupCount() {
        int count = 0;
        if (mHeaderList != null) {
            count = mHeaderList.size();
        }
        return count;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int count = 0;
        if (mMap != null) {
            ArrayList<ResponseCardList> list = mMap.get(getHeader(groupPosition));
            if (list != null)
                count = list.size();
        }
        return count;
    }

    @Override
    public Object getGroup(int groupPosition) {
        Object obj = null;
        if (mHeaderList != null) {
            obj = mHeaderList.get(groupPosition);
        }
        return obj;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Object obj = null;
        if (mMap != null) {
            ArrayList<ResponseCardList> list = mMap.get(getHeader(groupPosition));
            if (list != null)
                obj = list.get(childPosition);
        }
        return obj;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(mContext);

        if(convertView==null) {
            viewHolder = new GroupViewHolder();
            convertView = inflater.inflate(R.layout.row_expandable_listview_item_group_card, parent, false);

            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_expandable_listview_item_group_card_title);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (GroupViewHolder) convertView.getTag();
        }

        String key = String.valueOf(mHeaderList.get(groupPosition));
        viewHolder.mTitle.setText(key);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(mContext);

        if(convertView==null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_grid_item_card, parent, false);

            viewHolder.mDetailLayout = (RelativeLayout) convertView.findViewById(R.id.ll_row_grid_item_card_layout);
            viewHolder.mRadioBtn = (RadioButton) convertView.findViewById(R.id.rb_row_grid_item_card_icon);
            viewHolder.mRadioBtn.setFocusable(false);
            viewHolder.mRadioBtn.setClickable(false);
            viewHolder.mRadioBtn.setChecked(((ListView)parent).isItemChecked(childPosition));
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_grid_item_card_text);
            viewHolder.mTitle.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        ResponseCardList info = mMap.get(getHeader(groupPosition)).get(childPosition);
		/* 값 입력 */
        if (mOriginSelectedCardCode != null) {
            boolean selected = mOriginSelectedCardCode.equalsIgnoreCase(info.getCardCode());
            viewHolder.mRadioBtn.setChecked(selected);
        } else {
            viewHolder.mRadioBtn.setChecked(false);
        }
        viewHolder.mTitle.setText(info.getCardName(), TextView.BufferType.SPANNABLE);
        if (mode == CARD_MODE.SEARCH_MODE) {
            Spannable spannedText = (Spannable) viewHolder.mTitle.getText();

            KoreanTextMatcher matcher = new KoreanTextMatcher(mFilter);
            KoreanTextMatch match = matcher.match(info.getCardName());
            if (match.success()) {
                spannedText.setSpan(new ForegroundColorSpan(Color.argb(255, 80, 129, 229)),
                        match.index(), match.index() + match.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public Object[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        int index = 0;
        if (sections != null && sections.length > sectionIndex) {
            index = sectionIndex;
        }
        return index;
    }

    @Override
    public int getSectionForPosition(int position) {
        int index = 0;
        if (sections != null) {

        }
        return index;
    }


    //====================================================================//
// ViewHolder
//====================================================================//
    private class ViewHolder {
        private RelativeLayout	mDetailLayout;
        private RadioButton mRadioBtn;
        private TextView mTitle;
    }

    private class GroupViewHolder {
        private TextView mTitle;
    }


//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<ResponseCardList> list, String selectedCode){
        mDataList = list;
        setOriginCardCode(selectedCode);
        createHeaderAndList();
        notifyDataSetChanged();
    }

    public void setOriginCardCode(String code) {
        mOriginSelectedCardCode = code;
    }

    private void createHeaderAndList() {
        mOriginalMap = new HashMap<String, ArrayList<ResponseCardList>>();
        mOriginalList = new ArrayList<>();
        for (int i = 0; i < mDataList.size(); i++) {
            ResponseCardList data = mDataList.get(i);
            if (data != null) {
                String name = data.getCardName();
                String code = data.getCardCode();
                if (name.length() > 0) {
                    if (!name.equalsIgnoreCase("없음")) {
                        char firstLetter = name.charAt(0);
                        KoreanText.KoreanCategory category = KoreanText.getKoreanCategory(firstLetter);
                        if (category != KoreanText.KoreanCategory.HANGUL_NOT) {
                            char koreanLetter = '\0';
                            if (category == KoreanText.KoreanCategory.HANGUL_COMPAT) {
                                koreanLetter = KoreanText.getCompatChoseong(firstLetter);
                                if (koreanLetter == '\0') {
                                    koreanLetter = KoreanText.getChoseong(firstLetter);
                                }
                            } else if (category == KoreanText.KoreanCategory.HANGUL_CHO) {
                                koreanLetter = firstLetter;
                            }
                            String header = String.valueOf(koreanLetter);
                            if (header.equalsIgnoreCase("ㄲ")) {
                                header = "ㄱ";
                            } else if (header.equalsIgnoreCase("ㄸ")) {
                                header = "ㄷ";
                            } else if (header.equalsIgnoreCase("ㅃ")) {
                                header = "ㅂ";
                            } else if (header.equalsIgnoreCase("ㅆ")) {
                                header = "ㅅ";
                            } else if (header.equalsIgnoreCase("ㅉ")) {
                                header = "ㅈ";
                            }
                            if (!mOriginalList.contains(header)) {
                                mOriginalList.add(header);
                            }
                            ArrayList<ResponseCardList> list = mOriginalMap.get(header);
                            if (list == null) {
                                list = new ArrayList<>();
                            }
                            list.add(data);
                            mOriginalMap.put(header, list);
                            if (code.equalsIgnoreCase(mOriginSelectedCardCode)) {
                                mOriginSelectedCardHeader = header;
                                mOriginSelectedCardIndex = list.size()-1;
                            }
                        } else {
                            firstLetter = name.toUpperCase().charAt(0);
                            String header = String.valueOf(firstLetter);
                            if (!mOriginalList.contains(header)) {
                                mOriginalList.add(header);
                            }
                            ArrayList<ResponseCardList> list = mOriginalMap.get(header);
                            if (list == null) {
                                list = new ArrayList<>();
                            }
                            list.add(data);
                            mOriginalMap.put(header, list);
                            if (code.equalsIgnoreCase(mOriginSelectedCardCode)) {
                                mOriginSelectedCardHeader = header;
                                mOriginSelectedCardIndex = list.size()-1;
                            }
                        }
                    }
                }
            }
        }
        if (mOriginalList.size() > 1) {
            Collections.sort(mOriginalList, new Comparator<String>() {
                /*
                오름차순 (ASC)
                 */
                @Override
                public int compare(String left, String right) {
                    int com = 0;
                    Character lhs = left.charAt(0);
                    Character rhs = right.charAt(0);
                    boolean lKorean = KoreanText.isKoreanChar(lhs);
                    boolean rKorean = KoreanText.isKoreanChar(rhs);
                    if (lKorean && rKorean) {
                        com = lhs.compareTo(rhs);
                    } else if (lKorean && !rKorean) {
                        com = -1*lhs;
                    } else if (!lKorean && rKorean) {
                        com = 1*lhs;
                    } else {
                        boolean lEnglish = KoreanText.isEnglish(lhs);
                        boolean rEnglish = KoreanText.isEnglish(rhs);
                        if (lEnglish && rEnglish) {
                            com = lhs.compareTo(rhs);
                        } else if (lEnglish && !rEnglish) {
                            com = -1*lhs;
                        } else if (!lEnglish && rEnglish) {
                            com = 1*lhs;
                        } else {
                            com = lhs.compareTo(rhs);
                        }
                    }
                    return com;
                }
            });
        }
        if (mHeaderList == null) {
            mHeaderList = new ArrayList<>();
        } else {
            mHeaderList.clear();
        }
        mHeaderList.addAll(mOriginalList);
        sections = mHeaderList.toArray();
        if (mMap == null) {
            mMap = new HashMap<>();
        } else {
            mMap.clear();
        }
        mMap.putAll(mOriginalMap);
    }

    public void setFilter(String filter) {
        mFilter = filter;
        if (filter == null || filter.length() == 0) {
            mode = CARD_MODE.INPUT_MODE;
            mMap.clear();
            mHeaderList.clear();
            mMap.putAll(mOriginalMap);
            mHeaderList.addAll(mOriginalList);
            sections = mHeaderList.toArray();
        } else {
            mode = CARD_MODE.SEARCH_MODE;
            mMap.clear();
            mHeaderList.clear();
            if (mSearchMap == null) {
                mSearchMap = new HashMap<>();
            }
            mSearchMap.clear();
            for (ResponseCardList data : mDataList) {
                KoreanTextMatch match = KoreanTextMatcher.match(data.getCardName(), filter);
                if (match.success()) {
                    ArrayList<ResponseCardList> list = mSearchMap.get("신용카드");
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(data);
                    mSearchMap.put("신용카드", list);
                }
            }
            mHeaderList.add("신용카드");
            mMap.putAll(mSearchMap);
            sections = null;
        }
        notifyDataSetChanged();
    }

    public void showSelected(CustomExpandableListView listView) {
        int header = mHeaderList.indexOf(mOriginSelectedCardHeader);
        if (header >= 0) {
            listView.setSelectedChild(header, mOriginSelectedCardIndex, false);
            long packedPos = CustomExpandableListView.getPackedPositionForChild(header, mOriginSelectedCardIndex);
            int flatPos = listView.getFlatListPosition(packedPos);
            if (flatPos > 2) {
                listView.smoothScrollToPosition(flatPos-1);
            }
        }
    }

    public ArrayList<String> getHeaderList() {
        return mHeaderList;
    }

}
