package com.nomadconnection.broccoli.adapter.spend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.constant.BudgetGroupCategory;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetFavorite;

import java.util.ArrayList;

public class AdtExpListViewSpendBudgetFavoriteCategory extends BaseExpandableListAdapter {

    private ArrayList<BudgetGroupCategory> mGroupList = null;
    private ArrayList<ArrayList<SpendBudgetFavorite>> mChildList = null;
    private LayoutInflater inflater = null;
    private String[] mCategoryGroup;
    private String[] mDetailArray;

    public AdtExpListViewSpendBudgetFavoriteCategory(Context context, ArrayList<BudgetGroupCategory> groupList, ArrayList<ArrayList<SpendBudgetFavorite>> childList){
        super();
        this.inflater = LayoutInflater.from(context);
        this.mGroupList = groupList;
        this.mChildList = childList;
        mCategoryGroup = context.getResources().getStringArray(R.array.spend_budget_group_category);
        mDetailArray = context.getResources().getStringArray(R.array.spend_category);
    }


    public void setData(ArrayList<BudgetGroupCategory> groupList, ArrayList<ArrayList<SpendBudgetFavorite>> childList) {
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public BudgetGroupCategory getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_expandable_listview_item_group_common_title, parent, false);
            viewHolder.mGroupTitle = (TextView)convertView.findViewById(R.id.tv_row_expandable_listview_item_group_common_title);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        String title = "";
        try {
            title = mCategoryGroup[getGroup(groupPosition).ordinal()];
        } catch (Exception e) {
            e.printStackTrace();
        }
        viewHolder.mGroupTitle.setText(title);

        return convertView;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public SpendBudgetFavorite getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        ViewHolder viewHolder;

        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_expandable_listview_item_child_budget_favorite, null);
            viewHolder.mFavorite = (ImageView) convertView.findViewById(R.id.iv_row_list_item_spend_budget_favorite);
            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_list_item_spend_budget_favorite_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        SpendBudgetFavorite favoriteData = getChild(groupPosition, childPosition);
        viewHolder.mFavorite.setSelected(favoriteData.isFavorite);
        String title = "";
        try {
            if (favoriteData.id.ordinal() == 0) {
                title = viewHolder.mTitle.getResources().getString(R.string.spend_budget_list_unclassified_title);
            } else {
                title = mDetailArray[favoriteData.id.ordinal()];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewHolder.mTitle.setText(title);

        return convertView;
    }

    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
 
 
    class ViewHolder{
    	/* group */
        public TextView mGroupTitle;

        /* child */
        public ImageView mFavorite;
        public TextView mTitle;
    }
}
