package com.nomadconnection.broccoli.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.InfoDataAlarm;


public class AdtListViewAlarm extends BaseAdapter {
	
	private ArrayList<InfoDataAlarm> 		mAlDataList;
	private Activity			  			mActivity;
	
//==========================================================================//
// constructor	
//==========================================================================//
	public AdtListViewAlarm(Activity _activity, ArrayList<InfoDataAlarm> _AlDataList) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public InfoDataAlarm getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_alarm, parent, false);
            listRow.mAlarm1			= (TextView) convertView.findViewById(R.id.tv_row_list_item_alarm_1);
            listRow.mAlarm2			= (TextView) convertView.findViewById(R.id.tv_row_list_item_alarm_2);
            listRow.mAlarm3			= (TextView) convertView.findViewById(R.id.tv_row_list_item_alarm_3);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        listRow.mAlarm1.setText(mAlDataList.get(position).mTxt1);
        listRow.mAlarm2.setText(mAlDataList.get(position).mTxt2);
        listRow.mAlarm3.setText(mAlDataList.get(position).mTxt3);
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {
    	private TextView		mAlarm1;
    	private TextView		mAlarm2;
    	private TextView		mAlarm3;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<InfoDataAlarm> array){
    	mAlDataList = array;
	}
    
     
}
