package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendBudgetFavoriteCategory;
import com.nomadconnection.broccoli.data.Demo.TrialExpense;
import com.nomadconnection.broccoli.interf.OnRefreshListener;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2017. 4. 3..
 */

public class AdtListViewDemoSpend extends BaseAdapter {

    private Activity mActivity;

    private TrialExpense mData;

    private String[]					mDetailCategory;
    private OnRefreshListener mRefreshListener;

    //==========================================================================//
// constructor
//==========================================================================//
    public AdtListViewDemoSpend(Activity activity, TrialExpense data, OnRefreshListener listener) {
        mActivity 		= activity;
        mData	= data;
        mDetailCategory = mActivity.getResources().getStringArray(R.array.spend_category);
        mRefreshListener = listener;
    }


    //====================================================================//
// override method
//====================================================================//
    @Override
    public int getCount() {
        int count = 0;
        if (mData != null) {
            count = 4;
        }
        return count;
    }

    @Override
    public TrialExpense getItem(int position) {
        TrialExpense info = mData;
        return info;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();

        if(convertView==null) {
            listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_main_2, parent, false);
            init(listRow, convertView);
            convertView.setTag(listRow);

        }
        else {
            listRow = (ViewHolder)convertView.getTag();
        }

        /* Data 적용 */
        dataSet(listRow, position);

        return convertView;
    }





//====================================================================//
// Init
//====================================================================//
    private void init(ViewHolder _listRow, View _convertView){

        _convertView.findViewById(R.id.inc_card_f).setVisibility(View.GONE);

    	/* Card I */
        _listRow.mCardILayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_i);
        _listRow.mCardITitle = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_title);
        _listRow.mCardIIcon = (ImageView)_listRow.mCardILayout.findViewById(R.id.iv_row_item_inc_home_card_i_icon);
        _listRow.mCardISubInfo1Cost = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_sub_info_1_cost);
        _listRow.mCardISubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
        _listRow.mCardISubInfo2Cost = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_sub_info_2_cost);
        _listRow.mCardISubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
        _listRow.mCardIGraphCurrent = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_current_graph_amount);
        _listRow.mCardIGraphAmount = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_left_graph_amount);
        _listRow.mCardICurrentStatus = (TextView) _listRow.mCardILayout.findViewById(R.id.tv_spend_budget_current_status);
        _listRow.mCardIFavoriteEmpty = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_favorite_empty_layout);
        _listRow.mCardIFavoriteSetting = _listRow.mCardILayout.findViewById(R.id.fl_spend_budget_favorite_setting);
        _listRow.mCardIFavoriteListLayout = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_favorite_list_layout);
        _listRow.mCardIFavoriteList = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_favorite_list);
        _listRow.mCardIFavoriteEdit = _listRow.mCardILayout.findViewById(R.id.fl_spend_budget_favorite_edit);
        _listRow.mCardIMainLayout = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_row_item_budget_main_layout);
        _listRow.mCardIEmptyLayout = (LinearLayout)_listRow.mCardILayout.findViewById(R.id.ll_row_item_budget_empty_layout);

    	/* Card J */
        _listRow.mCardJLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_j);
        _listRow.mCardJTitle = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_title);
        _listRow.mCardJTitleCount = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_title_count);
        _listRow.mCardJChart = (PieChart)_listRow.mCardJLayout.findViewById(R.id.chart_bar_pie);
        _listRow.mCardJSubInfo1Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_1_title);
        _listRow.mCardJSubInfo1Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_1_cost);
        _listRow.mCardJSubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
        _listRow.mCardJLayoutSub_2 = (LinearLayout)_listRow.mCardJLayout.findViewById(R.id.ll_row_item_inc_home_card_j_sub_info_2);
        _listRow.mCardJSubInfo2Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_2_title);
        _listRow.mCardJSubInfo2Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_2_cost);
        _listRow.mCardJSubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
        _listRow.mCardJLayoutSub_3 = (LinearLayout)_listRow.mCardJLayout.findViewById(R.id.ll_row_item_inc_home_card_j_sub_info_3);
        _listRow.mCardJSubInfo3Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_3_title);
        _listRow.mCardJSubInfo3Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_3_cost);
        _listRow.mCardJSubInfo3Cost.setTypeface(FontClass.setFont(mActivity));

    	/* Card K */
        _listRow.mCardKLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_k);
        _listRow.mCardKTitle = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_title);
        _listRow.mCardKChart = (PieChart)_listRow.mCardKLayout.findViewById(R.id.chart_bar_pie);
        _listRow.mCardKSubInfo1Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_1_title);
        _listRow.mCardKSubInfo1Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_1_cost);
        _listRow.mCardKSubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
        _listRow.mCardKLayoutSub_2 = (LinearLayout)_listRow.mCardKLayout.findViewById(R.id.ll_row_item_inc_home_card_k_sub_info_2);
        _listRow.mCardKSubInfo2Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_2_title);
        _listRow.mCardKSubInfo2Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_2_cost);
        _listRow.mCardKSubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
        _listRow.mCardKLayoutSub_3 = (LinearLayout)_listRow.mCardKLayout.findViewById(R.id.ll_row_item_inc_home_card_k_sub_info_3);
        _listRow.mCardKSubInfo3Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_3_title);
        _listRow.mCardKSubInfo3Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_3_cost);
        _listRow.mCardKSubInfo3Cost.setTypeface(FontClass.setFont(mActivity));

		/* Card L */
        _listRow.mMainSpendCardLayout = (LinearLayout)_convertView.findViewById(R.id.inc_main_spend_card);
        _listRow.mMainSpendCardContentList = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_main_spend_card_list);
        _listRow.mMainSpendCardEmptyLayout = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_empty_layout);
        _listRow.mMainSpendCardContentLayout = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_main_spend_card_layout);
        _listRow.mMainSpendCardTitle = (TextView)_convertView.findViewById(R.id.tv_row_item_main_spend_card_title);

		/* Card E */
        _listRow.mCardELayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_e);
        _listRow.mCardETitle = (TextView)_listRow.mCardELayout.findViewById(R.id.tv_row_item_inc_home_card_e_title);
        _listRow.mCardEIcon = (ImageView)_listRow.mCardELayout.findViewById(R.id.iv_row_item_inc_home_card_e_icon);


		/* Card E */
        _listRow.mCardMLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_m);
        _listRow.mCardMTitle = (TextView)_listRow.mCardMLayout.findViewById(R.id.tv_row_item_inc_home_card_m_title);
        _listRow.mCardMSub = (TextView)_listRow.mCardMLayout.findViewById(R.id.tv_row_item_inc_home_card_m_text);
    }




    //====================================================================//
// Data Set
//====================================================================//
    private void dataSet(final ViewHolder listRow, int position){
        TrialExpense data = mData;

        if (data != null) {
            if (position == 0){		//소비예산
                long spend = Long.valueOf(data.expense);
                long budget = Long.valueOf(data.budget);
                long value;

                if(budget != 0) {
                    listRow.mCardIMainLayout.setVisibility(View.VISIBLE);
                    listRow.mCardIEmptyLayout.setVisibility(View.GONE);
                    if (budget != 0) {
                        value = (long)( (double)spend / (double)budget * 100.0 );
                    } else if (budget == 0 && spend == 0) {
                        value = 0;
                    } else {
                        value = 999;
                    }

                    long leftValue = budget - spend;
                    if (value > 999) {
                        value = 999;
                    }
                    listRow.mCardMLayout.setVisibility(View.GONE);
                    listRow.mCardELayout.setVisibility(View.GONE);
                    listRow.mCardILayout.setVisibility(View.VISIBLE);
                    listRow.mCardJLayout.setVisibility(View.GONE);
                    listRow.mCardKLayout.setVisibility(View.GONE);
                    listRow.mMainSpendCardLayout.setVisibility(View.GONE);
                    listRow.mCardITitle.setText(mActivity.getString(R.string.demo_monthly_budget));
                    listRow.mCardISubInfo1Cost.setText(ElseUtils.getDecimalFormat(budget));
                    listRow.mCardISubInfo2Cost.setText(ElseUtils.getDecimalFormat(spend));

                    Animation animation = new AlphaAnimation(0.2f, 1);
                    animation.setDuration(200);

                    if (leftValue >= 0) {
                        listRow.mCardICurrentStatus.setText(ElseUtils.getDecimalFormat(leftValue)+" 남음");
                    } else {
                        listRow.mCardICurrentStatus.setText(ElseUtils.getDecimalFormat(Math.abs(leftValue))+" 초과");
                    }

                    if (spend == 0) {
                        listRow.mCardIGraphCurrent.setVisibility(View.GONE);
                        listRow.mCardICurrentStatus.setTextColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage1_color));
                        LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) listRow.mCardIGraphAmount.getLayoutParams();
                        amountParams.weight = 1;
                        listRow.mCardIGraphAmount.setLayoutParams(amountParams);
                    } else {
                        listRow.mCardIGraphCurrent.setVisibility(View.VISIBLE);
                        listRow.mCardIGraphCurrent.setAnimation(animation);
                        float spendValue = 0;
                        if (budget != 0) {
                            spendValue = (float) ((double)spend/(double)budget);
                        } else {
                            spendValue = spend == 0 ? 0f:1f;
                        }

                        if (spendValue > 1f) {
                            spendValue = 1f;
                        }
                        float budgetValue = 1f - spendValue;

                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listRow.mCardIGraphCurrent.getLayoutParams();
                        params.weight = spendValue;
                        if (spendValue >= 1f) {
                            params.setMargins(0, 0, 0, 0);
                        } else {
                            params.setMargins(0, 0, ElseUtils.convertDp2Px(mActivity, 3), 0);
                        }
                        listRow.mCardIGraphCurrent.setLayoutParams(params);

                        LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) listRow.mCardIGraphAmount.getLayoutParams();
                        amountParams.weight = budgetValue;
                        listRow.mCardIGraphAmount.setLayoutParams(amountParams);

                        Drawable amountDrawable = listRow.mCardIGraphAmount.getBackground();
                        if (amountDrawable instanceof ShapeDrawable) {
                            ShapeDrawable shapeDrawable = (ShapeDrawable) amountDrawable;
                            shapeDrawable.getPaint().setColor(mActivity.getResources().getColor(R.color.common_white));
                        } else if (amountDrawable instanceof GradientDrawable) {
                            GradientDrawable gradientDrawable = (GradientDrawable) amountDrawable;
                            gradientDrawable.setColor(mActivity.getResources().getColor(R.color.common_white));
                        }

                        if(value <= 50){
                            listRow.mCardICurrentStatus.setTextColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage1_color));
                            Drawable drawable = listRow.mCardIGraphCurrent.getBackground();
                            if (drawable instanceof ShapeDrawable) {
                                ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                                shapeDrawable.getPaint().setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage1_color));
                            } else if (drawable instanceof GradientDrawable) {
                                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                                gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage1_color));
                            }
                        } else if(value <= 100){
                            listRow.mCardICurrentStatus.setTextColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage2_color));
                            Drawable drawable = listRow.mCardIGraphCurrent.getBackground();
                            if (drawable instanceof ShapeDrawable) {
                                ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                                shapeDrawable.getPaint().setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage2_color));
                            } else if (drawable instanceof GradientDrawable) {
                                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                                gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage2_color));
                            }
                        } else {
                            listRow.mCardICurrentStatus.setTextColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage3_color));
                            Drawable drawable = listRow.mCardIGraphCurrent.getBackground();
                            if (drawable instanceof ShapeDrawable) {
                                ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                                shapeDrawable.getPaint().setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage3_color));
                            } else if (drawable instanceof GradientDrawable) {
                                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                                gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage3_color));
                            }
                        }
                    }
                    listRow.mCardIFavoriteEmpty.setVisibility(View.GONE);
                    listRow.mCardIFavoriteListLayout.setVisibility(View.GONE);
                    listRow.mCardIFavoriteSetting.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mActivity, Level2ActivitySpendBudgetFavoriteCategory.class);
                            mActivity.startActivity(intent);
                        }
                    });
                } else {
                    listRow.mCardIMainLayout.setVisibility(View.GONE);
                    listRow.mCardIEmptyLayout.setVisibility(View.VISIBLE);
                    listRow.mCardMLayout.setVisibility(View.GONE);
                    listRow.mCardELayout.setVisibility(View.GONE);
                    listRow.mCardJLayout.setVisibility(View.GONE);
                    listRow.mCardKLayout.setVisibility(View.GONE);
                    listRow.mMainSpendCardLayout.setVisibility(View.GONE);
                }
            }
            else if (position == 1){		//소비분류
                int type1 = data.firstCategoryId;
                int type2 = data.secondCategoryId;
                int type3 = data.thirdCategoryId;
                int count = 3;
                String sum1 = ElseUtils.getDecimalFormat(data.firstCategoryAmount);
                String sum2 = ElseUtils.getDecimalFormat(data.secondCategoryAmount);
                String sum3 = ElseUtils.getDecimalFormat(data.thirdCategoryAmount);
                String[] categoryType = mActivity.getResources().getStringArray(R.array.spend_category);

                if(count != 0){
                    listRow.mCardJLayoutSub_2.setVisibility(View.VISIBLE);
                    listRow.mCardJLayoutSub_3.setVisibility(View.VISIBLE);
                    listRow.mCardMLayout.setVisibility(View.GONE);
                    listRow.mCardELayout.setVisibility(View.GONE);
                    listRow.mCardILayout.setVisibility(View.GONE);
                    listRow.mCardJLayout.setVisibility(View.VISIBLE);
                    listRow.mCardKLayout.setVisibility(View.GONE);
                    listRow.mMainSpendCardLayout.setVisibility(View.GONE);
                    listRow.mCardJTitle.setText(mActivity.getString(R.string.spend_category));
                    listRow.mCardJTitleCount.setVisibility(View.GONE);
                    TextView textView = (TextView) listRow.mCardJLayout.findViewById(R.id.tv_row_item_main_spend_category_subtitle);
                    textView.setText("분류별 평균 소비액");
                    listRow.mCardJSubInfo1Title.setText(categoryType[type1]);
                    listRow.mCardJSubInfo1Cost.setText(sum1);
                    listRow.mCardJSubInfo2Title.setText(categoryType[type2]);
                    listRow.mCardJSubInfo2Cost.setText(sum2);
                    listRow.mCardJSubInfo3Title.setText(categoryType[type3]);
                    listRow.mCardJSubInfo3Cost.setText(sum3);
                    PieChartInit(mActivity, listRow.mCardJChart, count, position);
                }
            }
            else if (position == 2){		//소비방법
                int type1 = 0, type2 = 0, type3 = 0;
                int count = 3;
                String sum1 = "0", sum2 = "0", sum3 = "0";

                listRow.mCardKLayoutSub_2.setVisibility(View.VISIBLE);
                listRow.mCardKLayoutSub_3.setVisibility(View.VISIBLE);

                for(int j = 0; j < count; j ++){
                    switch (j){
                        case 0:
                            type1 = 1;
                            sum1 = ElseUtils.getDecimalFormat(data.creditExpense);
                            break;
                        case 1:
                            type2 = 2;
                            sum2 = ElseUtils.getDecimalFormat(data.checkExpense);
                            break;
                        case 2:
                            type3 = 3;
                            sum3 = ElseUtils.getDecimalFormat(data.cashExpense);;
                            break;
                    }
                }

                String[] payType = mActivity.getResources().getStringArray(R.array.spend_pay_type);
                listRow.mCardMLayout.setVisibility(View.GONE);
                listRow.mCardELayout.setVisibility(View.GONE);
                listRow.mCardILayout.setVisibility(View.GONE);
                listRow.mCardJLayout.setVisibility(View.GONE);
                listRow.mCardKLayout.setVisibility(View.VISIBLE);
                listRow.mMainSpendCardLayout.setVisibility(View.GONE);
                listRow.mCardKTitle.setText(mActivity.getString(R.string.spend_way));
                listRow.mCardKSubInfo1Title.setText(payType[type1]);
                listRow.mCardKSubInfo1Cost.setText(sum1);
                listRow.mCardKSubInfo2Title.setText(payType[type2]);
                listRow.mCardKSubInfo2Cost.setText(sum2);
                listRow.mCardKSubInfo3Title.setText(payType[type3]);
                listRow.mCardKSubInfo3Cost.setText(sum3);
                PieChartInit(mActivity, listRow.mCardKChart, count, position);
            }
            else if (position == 3){ // 카드정도 조건 변경 해야함
                listRow.mCardMLayout.setVisibility(View.GONE);
                listRow.mCardELayout.setVisibility(View.GONE);
                listRow.mCardILayout.setVisibility(View.GONE);
                listRow.mCardJLayout.setVisibility(View.GONE);
                listRow.mCardKLayout.setVisibility(View.GONE);
                listRow.mMainSpendCardLayout.setVisibility(View.VISIBLE);
                listRow.mMainSpendCardEmptyLayout.setVisibility(View.GONE);
                listRow.mMainSpendCardContentLayout.setVisibility(View.VISIBLE);
                listRow.mMainSpendCardTitle.setText("가장 많이 쓰는 카드");

                listRow.mMainSpendCardContentList.removeAllViews();
                View view = mActivity.getLayoutInflater().inflate(R.layout.row_list_item_spend_card, null);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                view.setLayoutParams(params);
                ImageView logo = (ImageView) view.findViewById(R.id.iv_row_spend_card_logo);
                ImageView favorite = (ImageView) view.findViewById(R.id.iv_row_spend_card_favorite);
                View settingBtn = view.findViewById(R.id.fl_spend_card_setting);
                TextView titleView = (TextView) view.findViewById(R.id.tv_row_spend_card_title);
                TextView memoView = (TextView) view.findViewById(R.id.tv_row_spend_card_memo);
                TextView usedView = (TextView) view.findViewById(R.id.tv_row_spend_card_used);
                TextView requirementView = (TextView) view.findViewById(R.id.tv_row_spend_card_requirement);
                ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress_spend_card_graph);
                String title = null;
                long requirement = 0;
                if (data != null) {
                    title = data.cardName;
                    favorite.setVisibility(View.VISIBLE);
                    settingBtn.setVisibility(View.GONE);
                    favorite.setSelected(false);
                    favorite.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    requirement = data.cardRequirement;
                    requirementView.setText(usedView.getResources().getString(R.string.spend_card_need)+" "+ElseUtils.getDecimalFormat(requirement));
                } else {
                    favorite.setVisibility(View.GONE);
                    settingBtn.setVisibility(View.VISIBLE);
                    requirementView.setText(usedView.getResources().getString(R.string.spend_card_need)+" "+0);
                }
                logo.setImageResource(ElseUtils.createCardLogoItem(data.cardCompanyCode));
                titleView.setText(title);
                memoView.setText(null);
                usedView.setText(usedView.getResources().getString(R.string.spend_card_usage)+" "+ElseUtils.getDecimalFormat(data.cardExpense));
                float value = 0;
                if (requirement != 0) {
                    value = (long)( (double)data.cardExpense / (double)requirement * 100.0 );
                } else if (requirement == 0 && data.cardExpense == 0) {
                    value = 0;
                } else {
                    value = 999;
                }
                if (0 >= value) {
                    usedView.setTextColor(usedView.getResources().getColor(R.color.common_subtext_color));
                    progressBar.setProgress(0);
                } else if (value < 100) {
                    usedView.setTextColor(usedView.getResources().getColor(R.color.spend_card_progress_font_color));
                    progressBar.setProgress(((int) value));
                    Drawable drawable = progressBar.getProgressDrawable();
                    if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
                        shapeDrawable.setColorFilter(mActivity.getResources().getColor(R.color.spend_card_graph_progress_color), PorterDuff.Mode.SRC_IN);
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_card_graph_progress_color));
                    }
                } else {
                    usedView.setTextColor(usedView.getResources().getColor(R.color.spend_card_graph_over_color));
                    progressBar.setProgress(progressBar.getMax());
                    Drawable drawable = progressBar.getProgressDrawable();
                    if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
                        shapeDrawable.setColorFilter(mActivity.getResources().getColor(R.color.spend_card_graph_over_color), PorterDuff.Mode.SRC_IN);
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_card_graph_over_color));
                    }
                }
                listRow.mMainSpendCardContentList.addView(view);
            }
        }
    }


    //====================================================================//
// ViewHolder
//====================================================================//
    private class ViewHolder {

        /* Card M */
        private LinearLayout 	mCardMLayout;
        private TextView		mCardMTitle;
        private TextView 		mCardMSub;

        /* Card E */
        private LinearLayout 	mCardELayout;
        private TextView		mCardETitle;
        private ImageView 		mCardEIcon;

        /* Card I */
        private LinearLayout 	mCardILayout;
        private TextView		mCardITitle;

        private ImageView 		mCardIIcon;
        private TextView		mCardISubInfo1Cost;
        private TextView		mCardISubInfo2Cost;
        private LinearLayout    mCardIGraphCurrent;
        private LinearLayout    mCardIGraphAmount;
        private TextView    	mCardICurrentStatus;
        private LinearLayout	mCardIMainLayout;
        private LinearLayout	mCardIFavoriteListLayout;
        private LinearLayout	mCardIFavoriteList;
        private View			mCardIFavoriteEdit;
        private LinearLayout	mCardIFavoriteEmpty;
        private View			mCardIFavoriteSetting;
        private LinearLayout	mCardIEmptyLayout;

        /* Card J */
        private LinearLayout 	mCardJLayout;
        private LinearLayout 	mCardJLayoutSub_2;
        private LinearLayout 	mCardJLayoutSub_3;
        private TextView		mCardJTitle;
        private TextView		mCardJTitleCount;
        private PieChart		mCardJChart;
        private TextView		mCardJSubInfo1Title;
        private TextView		mCardJSubInfo1Cost;
        private TextView		mCardJSubInfo2Title;
        private TextView		mCardJSubInfo2Cost;
        private TextView		mCardJSubInfo3Title;
        private TextView		mCardJSubInfo3Cost;

        /* Card K */
        private LinearLayout 	mCardKLayout;
        private LinearLayout 	mCardKLayoutSub_2;
        private LinearLayout 	mCardKLayoutSub_3;
        private TextView		mCardKTitle;
        private PieChart 		mCardKChart;
        private TextView		mCardKSubInfo1Title;
        private TextView		mCardKSubInfo1Cost;
        private TextView		mCardKSubInfo2Title;
        private TextView		mCardKSubInfo2Cost;
        private TextView		mCardKSubInfo3Title;
        private TextView		mCardKSubInfo3Cost;

        /* Card L */
        private LinearLayout mMainSpendCardLayout;
        private LinearLayout mMainSpendCardEmptyLayout;
        private LinearLayout mMainSpendCardContentLayout;
        private LinearLayout mMainSpendCardContentList;
        private TextView     mMainSpendCardTitle;

    }


    //====================================================================//
// Data
//====================================================================//
    public void setData(TrialExpense data){
        mData = data;
    }


    //=======================================================================================//
// Pay Type Set
//=======================================================================================//
    private void pay_type_set(TextView textView, int payTypeInt){
        switch (payTypeInt){
            case 1: //신용카드
                textView.setTextAppearance(mActivity, R.style.CS_TagA1);
                textView.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
                break;
            case 2: //체크카드
                textView.setTextAppearance(mActivity, R.style.CS_TagA2);
                textView.setBackgroundResource(R.drawable.shape_tag_a_bg_2);
                break;
            case 3: //현금
                textView.setTextAppearance(mActivity, R.style.CS_TagA4);
                textView.setBackgroundResource(R.drawable.shape_tag_a_bg_4);
                break;
            case 4: //계좌이체
                textView.setTextAppearance(mActivity, R.style.CS_TagA3);
                textView.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
                break;
            case 5: //기타
                textView.setTextAppearance(mActivity, R.style.CS_TagA3);
                textView.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
                break;
        }
    }


    //=======================================================================================//
// Chart Init for pie
//=======================================================================================//
    private void PieChartInit(Context context, PieChart _pieChart, int value, int position){

        _pieChart.setUsePercentValues(true);
        _pieChart.setDescription("");
        //_listRow.mCardJChart.setExtraOffsets(5, 10, 5, 5);

        _pieChart.setDragDecelerationFrictionCoef(0.95f);

//		tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
//
//		mChart.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
//		mChart.setCenterText(generateCenterSpannableText());

        _pieChart.setDrawHoleEnabled(true);
        _pieChart.setHoleColorTransparent(false);
        _pieChart.setTouchEnabled(false);

//		_listRow.mCardJChart.setTransparentCircleColor(Color.WHITE);
//		_listRow.mCardJChart.setTransparentCircleAlpha(110);

        _pieChart.setHoleRadius(39.2f);
        _pieChart.setTransparentCircleRadius(39.2f);

        _pieChart.setDrawCenterText(false);
        _pieChart.setDrawSliceText(false);

        _pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        _pieChart.setRotationEnabled(false);
        _pieChart.setHighlightPerTapEnabled(false);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        //_listRow.mCardJChart.setOnChartValueSelectedListener(context);

        setData(_pieChart, 100, value, position);

        //_listRow.mCardJChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);

        Legend l = _pieChart.getLegend();
        l.setEnabled(false);
    }

    private void setData(PieChart _chart, float range, int count, int type) {

        float mult = range;

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        for (int i = 0; i < count; i++) {
            if(type == 1){
                long value = 0;
                switch (i) {
                    case 0:
                        value = mData.firstCategoryAmount;
                        break;
                    case 1:
                        value = mData.secondCategoryAmount;
                        break;
                    case 2:
                        value = mData.thirdCategoryAmount;
                        break;
                }
                float mCategoryValue = Float.valueOf(value);
                yVals1.add(new Entry (mCategoryValue, i));
            } else {
                long value = 0;
                switch (i) {
                    case 0:
                        value = mData.creditExpense;
                        break;
                    case 1:
                        value = mData.checkExpense;
                        break;
                    case 2:
                        value = mData.cashExpense;
                        break;
                }
                float mPayTypeValue = Float.valueOf(value);
                yVals1.add(new Entry (mPayTypeValue, i));
            }
        }

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < count; i++)
            xVals.add(mParties[i % mParties.length]);

        PieDataSet dataSet = new PieDataSet(yVals1, "Election Results");
        dataSet.setSliceSpace(2f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        if(count == 0){
            colors.add(Color.rgb(217, 217, 217));
        }
        else if (count != 0 && type == 1){
            for (int c : GROUP_COLORS)
                colors.add(c);
        }
        else {
            for (int c : WAY_COLORS)
                colors.add(c);
        }
        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(xVals, dataSet);
        data.setDrawValues(false);
        //data.setValueFormatter(new PercentFormatter());
        //data.setValueTextSize(11f);
        //data.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(tf);
        _chart.setData(data);

        // undo all highlights
        _chart.highlightValues(null);

        _chart.invalidate();
    }

    protected String[] mParties = new String[] {
            "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
            "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
            "Party Y", "Party Z"
    };

    public static final int[] GROUP_COLORS = {
            Color.rgb(54, 130, 245), Color.rgb(57, 158, 230), Color.rgb(32, 177, 195),
            Color.rgb(45, 162, 166), Color.rgb(60, 146, 169), Color.rgb(82, 125, 174),
            Color.rgb(105, 104, 178), Color.rgb(132, 81, 178), Color.rgb(167, 63, 178),
            Color.rgb(197, 79, 144), Color.rgb(354, 103, 113), Color.rgb(218, 129, 81),
            Color.rgb(230, 153, 46), Color.rgb(230, 137, 46), Color.rgb(230, 123, 51),
            Color.rgb(230, 107, 59), Color.rgb(217, 78, 83)
    };

    public static final int[] WAY_COLORS = {
            Color.rgb(32, 177, 195), Color.rgb(105, 104, 178), Color.rgb(230, 137, 46)
    };

}
