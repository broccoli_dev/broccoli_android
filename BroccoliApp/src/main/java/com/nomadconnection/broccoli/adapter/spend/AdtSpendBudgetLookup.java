package com.nomadconnection.broccoli.adapter.spend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetLookUpData;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by YelloHyunminJang on 2017. 3. 6..
 */

public class AdtSpendBudgetLookup extends BaseExpandableListAdapter {

    private LayoutInflater mInflater;
    private TreeMap<Integer, ArrayList<SpendBudgetLookUpData>> mMap = null;
    private Object[] mKeyArray = null;
    private String[] mCategoryGroup;

    public AdtSpendBudgetLookup(Context context) {
        mInflater = LayoutInflater.from(context);
        mCategoryGroup = context.getResources().getStringArray(R.array.spend_budget_group_category);
    }

    public AdtSpendBudgetLookup(Context context, TreeMap<Integer, ArrayList<SpendBudgetLookUpData>> map) {
        mInflater = LayoutInflater.from(context);
        mMap = map;
        mKeyArray = map.keySet().toArray();
        mCategoryGroup = context.getResources().getStringArray(R.array.spend_budget_group_category);
    }

    public void setData(TreeMap<Integer, ArrayList<SpendBudgetLookUpData>> map) {
        mMap = map;
        mKeyArray = map.keySet().toArray();
    }


    @Override
    public int getGroupCount() {
        int count = 0;
        if (mMap != null) {
            count = mMap.keySet().size();
        }
        return count;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int count = 0;
        if (mMap.get((int)mKeyArray[groupPosition]) != null) {
            count = mMap.get((int)mKeyArray[groupPosition]).size();
        }
        return count;
    }

    @Override
    public ArrayList<SpendBudgetLookUpData> getGroup(int groupPosition) {
        return mMap.get((int)mKeyArray[groupPosition]);
    }

    @Override
    public SpendBudgetLookUpData getChild(int groupPosition, int childPosition) {
        return mMap.get((int)mKeyArray[groupPosition]).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = convertView;
        GroupHolder holder = null;
        if(view == null){
            holder = new GroupHolder();
            view = mInflater.inflate(R.layout.row_expandable_listview_item_group_asset_das, parent, false);
            holder.mGroupTitle = (TextView)view.findViewById(R.id.tv_row_expandable_listview_item_group_asset_das_title);
            holder.mGroupCount = (TextView)view.findViewById(R.id.tv_row_expandable_listview_item_group_asset_das_title_count);
            view.setTag(holder);
        }else{
            holder = (GroupHolder)view.getTag();
        }

        int title = R.string.spend_budget_edit_classified;
        switch ((int)mKeyArray[groupPosition]) {
            case 0:
                title = R.string.spend_budget_edit_classified;
                break;
            case 1:
                title = R.string.spend_budget_edit_unclassified;
                break;
        }

        holder.mGroupTitle.setText(title);
        holder.mGroupCount.setVisibility(View.GONE);

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_list_item_spend_budget_lookup, parent, false);

            holder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_budget_title);
            holder.mExpense	= (TextView) convertView.findViewById(R.id.tv_row_spend_budget_last_expense);
            holder.mBudget	= (TextView) convertView.findViewById(R.id.tv_row_spend_budget_value);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        SpendBudgetLookUpData data = getChild(groupPosition, childPosition);
        String category = null;
        try {
            category = mCategoryGroup[data.categoryGroup];
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.mTitle.setText(category);
        if (!data.isUnClassifiedData) {
            holder.mExpense.setText(ElseUtils.getDecimalFormat(data.getExpense()));
            holder.mBudget.setText("예산 "+ElseUtils.getDecimalFormat(data.getBudget()));
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.common_white));
        } else {
            holder.mExpense.setText(ElseUtils.getDecimalFormat(data.getExpense()));
            holder.mBudget.setText("예산 "+ElseUtils.getDecimalFormat(data.getBudget()));
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.common_white));
        }
        if (data.getExpense() > data.getBudget()) {
            holder.mExpense.setTextColor(holder.mExpense.getResources().getColor(R.color.common_red_color));
        } else {
            holder.mExpense.setTextColor(holder.mExpense.getResources().getColor(R.color.common_maintext_color));
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class GroupHolder {
        public LinearLayout mGroup;
        public TextView mGroupTitle;
        public TextView mGroupCount;
    }

    private class ViewHolder {
        private TextView mTitle;
        private TextView mExpense;
        private TextView mBudget;
    }
}
