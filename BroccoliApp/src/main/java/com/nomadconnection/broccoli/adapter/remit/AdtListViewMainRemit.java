package com.nomadconnection.broccoli.adapter.remit;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.InfoDataMain4;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccount;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

/*
 * Card Use : g, h
 */

public class AdtListViewMainRemit extends BaseAdapter {

	private ArrayList<ResponseRemitAccount> 	mAlDataList;
	private Activity			  				mActivity;
	private View.OnClickListener				mOnClickListener;

	private static int 			mTotalPageNum 	= 0;
	private static int 			mCurrentPageIdx	= 0;

//==========================================================================//
// constructor
//==========================================================================//
	public AdtListViewMainRemit(Activity _activity, ArrayList<ResponseRemitAccount> _AlDataList, View.OnClickListener _OnClickListener) {
		mActivity 		= _activity;
		mAlDataList		= _AlDataList;
		mOnClickListener = _OnClickListener;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
        return mAlDataList.size();
    }
 
    @Override
    public ResponseRemitAccount getItem(int position) {
        return mAlDataList.get(position);        
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_main_remit, parent, false);
            init(listRow, convertView);
            convertView.setTag(listRow);
        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
//        dataSet(listRow, position);
		ResponseRemitAccount mRemitAccount = mAlDataList.get(position);
		if(mRemitAccount.getAccountInfo() != null && mRemitAccount.getAccountInfo().size() != 0 && position == 0){
			listRow.mCardRemitAccount.setVisibility(View.VISIBLE);
			listRow.mCardRemitMoneyLayout.setVisibility(View.GONE);
			listRow.mCardELayout.setVisibility(View.GONE);
			if(mRemitAccount.getAccountInfo() != null && mRemitAccount.getAccountInfo().size() != 0){
				RemitAccount regAccount =  mRemitAccount.getAccountInfo().get(0);
				listRow.mCardRemitLayountAccount.setVisibility(View.VISIBLE);
				listRow.mCardRemitAccountTitleCount.setVisibility(View.VISIBLE);
				listRow.mCardRemitLayoutAdd.setVisibility(View.GONE);
				listRow.mCardRemitAccountTitle.setText(mActivity.getResources().getString(R.string.remit_possible_account));
				listRow.mCardRemitAccountTitleCount.setText(mRemitAccount.getAccountCount());
				if(regAccount.getCompanyCode() != null){
					listRow.mCardRemitBank.setText(TransFormatUtils.transBankCodeToName(regAccount.getCompanyCode()));
				}
				listRow.mCardRemitBankRealName.setText(regAccount.getAccountName());
				listRow.mCardRemitBankNember.setText(regAccount.getAccountNumber());
				if(regAccount.getBalance() != null){
					listRow.mCardRemitSum.setText(ElseUtils.getStringDecimalFormat(regAccount.getBalance()));
				}
				if(regAccount.getStandardDate() != null){
					listRow.mCardRemitTime.setText(ElseUtils.getDate(regAccount.getStandardDate()));
				}
			}
			else {
				listRow.mCardRemitLayountAccount.setVisibility(View.GONE);
				listRow.mCardRemitLayoutAdd.setVisibility(View.VISIBLE);
				listRow.mCardRemitAccountTitleCount.setVisibility(View.GONE);
				listRow.mCardRemitAccountTitle.setText(mActivity.getResources().getString(R.string.remit_possible_account));

			}

		}
		else if(mRemitAccount.getWalletMoney() != null && position == 1){
			listRow.mCardRemitAccount.setVisibility(View.GONE);
			listRow.mCardRemitMoneyLayout.setVisibility(View.VISIBLE);
			listRow.mCardELayout.setVisibility(View.GONE);
			listRow.mCardRemitMoneySum.setText(ElseUtils.getStringDecimalFormat(mRemitAccount.getWalletMoney()));

		}
		else if(mRemitAccount.getRemit() != null && position == 2){
			listRow.mCardRemitAccount.setVisibility(View.GONE);
			listRow.mCardRemitMoneyLayout.setVisibility(View.GONE);
			listRow.mCardELayout.setVisibility(View.VISIBLE);
			listRow.mCardETitle.setText(mRemitAccount.getRemit());
			listRow.mCardEIcon.setBackgroundResource(R.drawable.home_icon_13);

		}
		else if(mRemitAccount.getRemitHistory() != null && position == 3){
			listRow.mCardRemitAccount.setVisibility(View.GONE);
			listRow.mCardRemitMoneyLayout.setVisibility(View.GONE);
			listRow.mCardELayout.setVisibility(View.VISIBLE);
			listRow.mCardETitle.setText(mRemitAccount.getRemitHistory());
			listRow.mCardEIcon.setBackgroundResource(R.drawable.home_icon_14);

		}
		else {
			listRow.mCardRemitAccount.setVisibility(View.GONE);
			listRow.mCardRemitMoneyLayout.setVisibility(View.GONE);
			listRow.mCardELayout.setVisibility(View.GONE);
		}

        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// Init
//====================================================================//
    private void init(ViewHolder _listRow, View _convertView){
		_listRow.mMain_remit_Margin = (FrameLayout)_convertView.findViewById(R.id.fl_row_item_main_4_margin);
    	
    	
    	/* Card E */
    	_listRow.mCardELayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_e);
    	_listRow.mCardETitle = (TextView)_listRow.mCardELayout.findViewById(R.id.tv_row_item_inc_home_card_e_title);
    	_listRow.mCardEIcon = (ImageView)_listRow.mCardELayout.findViewById(R.id.iv_row_item_inc_home_card_e_icon);

    	/* Card Remit Account */
    	_listRow.mCardRemitAccount = (LinearLayout)_convertView.findViewById(R.id.inc_card_remit_account);
    	_listRow.mCardRemitAccountTitle = (TextView)_listRow.mCardRemitAccount.findViewById(R.id.tv_row_item_inc_home_card_remit_account_title);
		_listRow.mCardRemitAccountTitleCount = (TextView)_listRow.mCardRemitAccount.findViewById(R.id.tv_row_item_inc_home_card_remit_account_title_count);
    	_listRow.mCardRemitAccountEdit = (TextView)_listRow.mCardRemitAccount.findViewById(R.id.tv_row_item_inc_home_card_remit_account_edit);

		_listRow.mCardRemitLayountAccount = (RelativeLayout)_convertView.findViewById(R.id.rl_row_item_inc_home_card_remit_account_bank);
    	_listRow.mCardRemitBank = (TextView)_listRow.mCardRemitLayountAccount.findViewById(R.id.tv_row_item_inc_home_card_remit_account_bank_name);
    	_listRow.mCardRemitBankRealName = (TextView)_listRow.mCardRemitLayountAccount.findViewById(R.id.tv_row_item_inc_home_card_remit_account_bank_sub_name);
    	_listRow.mCardRemitBankNember = (TextView)_listRow.mCardRemitLayountAccount.findViewById(R.id.tv_row_item_inc_home_card_remit_account_bank_num);
		_listRow.mCardRemitTime = (TextView)_listRow.mCardRemitLayountAccount.findViewById(R.id.tv_row_item_inc_home_card_remit_account_time);
    	_listRow.mCardRemitSum = (TextView)_listRow.mCardRemitLayountAccount.findViewById(R.id.tv_row_item_inc_home_card_remit_account_sum);

		_listRow.mCardRemitLayoutAdd = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_inc_home_card_remit_account_add);
		_listRow.mCardRemitAddButton = (Button)_listRow.mCardRemitLayoutAdd.findViewById(R.id.btn_row_item_inc_home_card_remit_account_add);
		_listRow.mCardRemitAddButton.setOnClickListener(mOnClickListener);

    	
    	
    	/* Card Remit Money */
		_listRow.mCardRemitMoneyLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_remit_money_charge);
		_listRow.mCardRemitMoneySum = (TextView)_listRow.mCardRemitMoneyLayout.findViewById(R.id.tv_row_item_inc_home_card_money_change_sum);
    }
    
    
    
    
//====================================================================//
// Data Set
//====================================================================//
    private void dataSet(final ViewHolder _listRow, int _position){

    }
    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {

		private FrameLayout mMain_remit_Margin;
    	
    	/* Card Remit Account */
    	private LinearLayout 	mCardRemitAccount;
    	private TextView		mCardRemitAccountTitle;
		private TextView		mCardRemitAccountTitleCount;
    	private TextView 		mCardRemitAccountEdit;
		private RelativeLayout 	mCardRemitLayountAccount;
    	private TextView		mCardRemitBank;
    	private TextView		mCardRemitBankRealName;
    	private TextView		mCardRemitBankNember;
    	private TextView		mCardRemitTime;
    	private TextView		mCardRemitSum;
		private LinearLayout 	mCardRemitLayoutAdd;
		private Button			mCardRemitAddButton;
    	
    	/* Card Remit Change */
		private LinearLayout mCardRemitMoneyLayout;
    	private TextView		mCardRemitMoneyTitle;
		private TextView		mCardRemitMoneyRealTime;
    	private TextView		mCardRemitMoneySum;

		/* Card E */
		private LinearLayout 	mCardELayout;
		private TextView		mCardETitle;
		private ImageView 		mCardEIcon;
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<ResponseRemitAccount> array){
    	mAlDataList = array;
	}
}
