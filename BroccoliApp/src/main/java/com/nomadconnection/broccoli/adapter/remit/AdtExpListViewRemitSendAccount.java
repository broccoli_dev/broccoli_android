package com.nomadconnection.broccoli.adapter.remit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Remit.RemitBankAccount;
import com.nomadconnection.broccoli.data.Remit.RemitBankDetail;
import com.nomadconnection.broccoli.data.Remit.RemitContent;

import java.util.ArrayList;

public class AdtExpListViewRemitSendAccount extends BaseExpandableListAdapter {

    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<RemitBankAccount>> mChildList = null;
    private LayoutInflater inflater = null;
    private ViewHolder mViewHolder = null;

    public AdtExpListViewRemitSendAccount(Context c, ArrayList<String> groupList, ArrayList<ArrayList<RemitBankAccount>> childList){
        super();
        this.inflater = LayoutInflater.from(c);
        this.mGroupList = groupList;
        this.mChildList = childList;
    }
 
    
//===============================================================================//
// Group
//===============================================================================//
    @Override
    public String getGroup(int groupPosition){
        return mGroupList.get(groupPosition);
    }
 
    @Override
    public int getGroupCount(){
        return mGroupList.size();
    }
 
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_group_remit_send, parent, false);
            mViewHolder.mGroupTitle = (TextView)v.findViewById(R.id.tv_row_expandable_listview_item_group_remit_send_title);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }
        mViewHolder.mGroupTitle.setText(getGroup(groupPosition));
        return v;
    }
    
    
//===============================================================================//
// Child
//===============================================================================//
 
    @Override
    public RemitBankAccount getChild(int groupPosition, int childPosition){
//    	public String getChild(int groupPosition, int childPosition){
        return mChildList.get(groupPosition).get(childPosition);
    }
 
    @Override
    public int getChildrenCount(int groupPosition){
        return mChildList.get(groupPosition).size();
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition){
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        View v = convertView;
 
        if(v == null){
            mViewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.row_expandable_listview_item_child_remit_send, null);
            mViewHolder.mName = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_remit_send_name);
            mViewHolder.mNumber = (TextView) v.findViewById(R.id.tv_row_expandable_listview_item_child_remit_send_number);
            v.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)v.getTag();
        }

        mViewHolder.mName.setText(getChild(groupPosition, childPosition).getHostName());
        mViewHolder.mNumber.setText(getChild(groupPosition, childPosition).getAccountNumber());

        if (isLastChild) ((View)v.findViewById(R.id.view_divider)).setVisibility(View.GONE);
        else ((View)v.findViewById(R.id.view_divider)).setVisibility(View.VISIBLE);

        return v;
    }
 
    @Override
    public boolean hasStableIds(){return true;}
 
    @Override
    public boolean isChildSelectable(int groupPostion, int childPosition){return true;}
 
 
 
    class ViewHolder{
    	/* group */
        public TextView mGroupTitle;

        /* child */
        public TextView mName;
        public TextView mNumber;
    }

    public void setData(ArrayList<String> groupList, ArrayList<ArrayList<RemitBankAccount>> childList) {
        this.mGroupList = groupList;
        this.mChildList = childList;
        notifyDataSetChanged();
    }

//====================================================================//
// Sort
//====================================================================//
    String mSort = "all";
    public void sortData(String _sort){
        mSort = _sort;
    }
}
