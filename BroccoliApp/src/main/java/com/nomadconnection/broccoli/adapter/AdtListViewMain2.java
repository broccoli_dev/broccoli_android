package com.nomadconnection.broccoli.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendBudgetFavoriteCategory;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.RequestCardList;
import com.nomadconnection.broccoli.data.Spend.ResponseCardList;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetDetail;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetInfo;
import com.nomadconnection.broccoli.data.Spend.SpendCardBanner;
import com.nomadconnection.broccoli.data.Spend.SpendCardUserListData;
import com.nomadconnection.broccoli.data.Spend.SpendCategoryInfo;
import com.nomadconnection.broccoli.data.Spend.SpendMainData;
import com.nomadconnection.broccoli.data.Spend.SpendMonthlyInfo;
import com.nomadconnection.broccoli.data.Spend.SpendMonthlyList;
import com.nomadconnection.broccoli.data.Spend.SpendPayType;
import com.nomadconnection.broccoli.interf.OnRefreshListener;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/*
 * Card Use : f, i, j, k
 */

public class AdtListViewMain2 extends BaseAdapter {
	
	private ArrayList<SpendMainData> 	mAlDataList;
	private Activity			  		mActivity;

	private List<SpendPayType>			mPayType;
	private List<SpendCategoryInfo> 	mCategory;

	private String[]					mDetailCategory;
	private OnRefreshListener			mRefreshListener;
	
//==========================================================================//
// constructor	
//==========================================================================//
	public AdtListViewMain2(Activity _activity, ArrayList<SpendMainData> _AlDataList, OnRefreshListener listener) {
		mActivity 		= _activity;
		mAlDataList	= _AlDataList;
		mDetailCategory = mActivity.getResources().getStringArray(R.array.spend_category);
		mRefreshListener = listener;
	}
	
 	
//====================================================================//
// override method
//====================================================================// 	 	
    @Override
    public int getCount() {
		int count = 0;
		if (mAlDataList != null) {
			count = mAlDataList.size();
		}
		return count;
    }
 
    @Override
    public SpendMainData getItem(int position) {
		SpendMainData info = null;
		if (mAlDataList != null) {
			info = mAlDataList.get(position);
		}
		return info;
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	ViewHolder listRow;
        LayoutInflater inflator = mActivity.getLayoutInflater();
 
        if(convertView==null) {
        	listRow = new ViewHolder();
            convertView = inflator.inflate(R.layout.row_list_item_main_2, parent, false);
            init(listRow, convertView);
            convertView.setTag(listRow);

        }
        else {
        	listRow = (ViewHolder)convertView.getTag();
        }
        
        
        /* Data 적용 */
        dataSet(listRow, position);
        
        return convertView;
    }    
    
    

    
    
//====================================================================//
// Init
//====================================================================//
    private void init(ViewHolder _listRow, View _convertView){
    	/* Card F */
    	_listRow.mCardFLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_f);
    	_listRow.mCardFTitle = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_title);
    	_listRow.mCardFCount = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_title_count);
    	_listRow.mCardFSubInfo1Title = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_1_txt_1);
    	_listRow.mCardFSubInfo1Tag = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_1_txt_1_tag);
    	_listRow.mCardFSubInfo1Txt = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_1_txt_2);
		_listRow.mCardFSubInfoMemo1 = _listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_memo_1);
    	_listRow.mCardFSubInfo1Cost = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_1_cost);
		_listRow.mCardFSubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardFLayoutSub_2 = (LinearLayout)_listRow.mCardFLayout.findViewById(R.id.ll_row_item_inc_home_card_f_sub_info_2);
    	_listRow.mCardFSubInfo2Title = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_2_txt_1);
    	_listRow.mCardFSubInfo2Tag = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_2_txt_1_tag);
    	_listRow.mCardFSubInfo2Txt = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_2_txt_2);
		_listRow.mCardFSubInfoMemo2 = _listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_memo_2);
    	_listRow.mCardFSubInfo2Cost = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_2_cost);
		_listRow.mCardFSubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardFLayoutSub_3 = (LinearLayout)_listRow.mCardFLayout.findViewById(R.id.ll_row_item_inc_home_card_f_sub_info_3);
    	_listRow.mCardFSubInfo3Title = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_3_txt_1);
    	_listRow.mCardFSubInfo3Tag = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_3_txt_1_tag);
    	_listRow.mCardFSubInfo3Txt = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_3_txt_2);
		_listRow.mCardFSubInfoMemo3 = _listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_memo_3);
    	_listRow.mCardFSubInfo3Cost = (TextView)_listRow.mCardFLayout.findViewById(R.id.tv_row_item_inc_home_card_f_sub_info_3_cost);
		_listRow.mCardFSubInfo3Cost.setTypeface(FontClass.setFont(mActivity));

		_listRow.mCardBannerLayout = (LinearLayout) _convertView.findViewById(R.id.card_banner);
		_listRow.mCardBannerBenefit = (TextView) _listRow.mCardBannerLayout.findViewById(R.id.tv_row_spend_card_additional_benefit);
		_listRow.mCardBannerBenefitUnit = (TextView) _listRow.mCardBannerLayout.findViewById(R.id.tv_row_spend_card_additional_benefit_unit);
    	
    	/* Card I */
    	_listRow.mCardILayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_i);
    	_listRow.mCardITitle = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_title);
    	_listRow.mCardIIcon = (ImageView)_listRow.mCardILayout.findViewById(R.id.iv_row_item_inc_home_card_i_icon);
    	_listRow.mCardISubInfo1Cost = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_sub_info_1_cost);
		_listRow.mCardISubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
    	_listRow.mCardISubInfo2Cost = (TextView)_listRow.mCardILayout.findViewById(R.id.tv_row_item_inc_home_card_i_sub_info_2_cost);
		_listRow.mCardISubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardIGraphCurrent = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_current_graph_amount);
		_listRow.mCardIGraphAmount = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_left_graph_amount);
		_listRow.mCardICurrentStatus = (TextView) _listRow.mCardILayout.findViewById(R.id.tv_spend_budget_current_status);
		_listRow.mCardIFavoriteEmpty = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_favorite_empty_layout);
		_listRow.mCardIFavoriteSetting = _listRow.mCardILayout.findViewById(R.id.fl_spend_budget_favorite_setting);
		_listRow.mCardIFavoriteListLayout = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_favorite_list_layout);
		_listRow.mCardIFavoriteList = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_spend_budget_favorite_list);
		_listRow.mCardIFavoriteEdit = _listRow.mCardILayout.findViewById(R.id.fl_spend_budget_favorite_edit);
		_listRow.mCardIMainLayout = (LinearLayout) _listRow.mCardILayout.findViewById(R.id.ll_row_item_budget_main_layout);
		_listRow.mCardIEmptyLayout = (LinearLayout)_listRow.mCardILayout.findViewById(R.id.ll_row_item_budget_empty_layout);
    	
    	/* Card J */
    	_listRow.mCardJLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_j);
    	_listRow.mCardJTitle = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_title);
    	_listRow.mCardJTitleCount = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_title_count);
    	_listRow.mCardJChart = (PieChart)_listRow.mCardJLayout.findViewById(R.id.chart_bar_pie);
    	_listRow.mCardJSubInfo1Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_1_title);
    	_listRow.mCardJSubInfo1Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_1_cost);
		_listRow.mCardJSubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardJLayoutSub_2 = (LinearLayout)_listRow.mCardJLayout.findViewById(R.id.ll_row_item_inc_home_card_j_sub_info_2);
    	_listRow.mCardJSubInfo2Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_2_title);
    	_listRow.mCardJSubInfo2Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_2_cost);
		_listRow.mCardJSubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardJLayoutSub_3 = (LinearLayout)_listRow.mCardJLayout.findViewById(R.id.ll_row_item_inc_home_card_j_sub_info_3);
    	_listRow.mCardJSubInfo3Title = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_3_title);
    	_listRow.mCardJSubInfo3Cost = (TextView)_listRow.mCardJLayout.findViewById(R.id.tv_row_item_inc_home_card_j_sub_info_3_cost);
		_listRow.mCardJSubInfo3Cost.setTypeface(FontClass.setFont(mActivity));

    	/* Card K */
    	_listRow.mCardKLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_k);
    	_listRow.mCardKTitle = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_title);
    	_listRow.mCardKChart = (PieChart)_listRow.mCardKLayout.findViewById(R.id.chart_bar_pie);
    	_listRow.mCardKSubInfo1Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_1_title);
    	_listRow.mCardKSubInfo1Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_1_cost);
		_listRow.mCardKSubInfo1Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardKLayoutSub_2 = (LinearLayout)_listRow.mCardKLayout.findViewById(R.id.ll_row_item_inc_home_card_k_sub_info_2);
    	_listRow.mCardKSubInfo2Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_2_title);
    	_listRow.mCardKSubInfo2Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_2_cost);
		_listRow.mCardKSubInfo2Cost.setTypeface(FontClass.setFont(mActivity));
		_listRow.mCardKLayoutSub_3 = (LinearLayout)_listRow.mCardKLayout.findViewById(R.id.ll_row_item_inc_home_card_k_sub_info_3);
    	_listRow.mCardKSubInfo3Title = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_3_title);
    	_listRow.mCardKSubInfo3Cost = (TextView)_listRow.mCardKLayout.findViewById(R.id.tv_row_item_inc_home_card_k_sub_info_3_cost);
		_listRow.mCardKSubInfo3Cost.setTypeface(FontClass.setFont(mActivity));

		/* Card L */
		_listRow.mMainSpendCardLayout = (LinearLayout)_convertView.findViewById(R.id.inc_main_spend_card);
		_listRow.mMainSpendCardContentList = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_main_spend_card_list);
		_listRow.mMainSpendCardEmptyLayout = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_empty_layout);
		_listRow.mMainSpendCardContentLayout = (LinearLayout)_convertView.findViewById(R.id.ll_row_item_main_spend_card_layout);

		/* Card E */
		_listRow.mCardELayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_e);
		_listRow.mCardETitle = (TextView)_listRow.mCardELayout.findViewById(R.id.tv_row_item_inc_home_card_e_title);
		_listRow.mCardEIcon = (ImageView)_listRow.mCardELayout.findViewById(R.id.iv_row_item_inc_home_card_e_icon);


		/* Card E */
		_listRow.mCardMLayout = (LinearLayout)_convertView.findViewById(R.id.inc_card_m);
		_listRow.mCardMTitle = (TextView)_listRow.mCardMLayout.findViewById(R.id.tv_row_item_inc_home_card_m_title);
		_listRow.mCardMSub = (TextView)_listRow.mCardMLayout.findViewById(R.id.tv_row_item_inc_home_card_m_text);
    }
    
    
    
    
//====================================================================//
// Data Set
//====================================================================//
    private void dataSet(final ViewHolder _listRow, int _position){
		SpendMainData mSpendMainData = mAlDataList.get(_position);

		if (mSpendMainData.getMonthlyInfo() != null && _position == 0 ){ 	//소비이력
			int type1 = 0, type2 = 0, type3 = 0;
			String title1 = "데이터 없음", title2 = "데이터 없음", title3 = "데이터 없음";
			String sum1 = "0", sum2 = "0", sum3 = "0";
			String date1 = "0", date2 = "0", date3 = "0";

			SpendMonthlyList mSpendHistory = mSpendMainData.getMonthlyInfo();
			int count = mSpendHistory.getList().size();

			if(mSpendHistory.getList().size() != 0){
				switch (count){
					case 1:
						_listRow.mCardFLayoutSub_2.setVisibility(View.GONE);
						_listRow.mCardFLayoutSub_3.setVisibility(View.GONE);
						break;
					case 2:
						_listRow.mCardFLayoutSub_2.setVisibility(View.VISIBLE);
						_listRow.mCardFLayoutSub_3.setVisibility(View.GONE);
						break;
					case 3:
						_listRow.mCardFLayoutSub_2.setVisibility(View.VISIBLE);
						_listRow.mCardFLayoutSub_3.setVisibility(View.VISIBLE);
						break;
				}
				List<SpendMonthlyInfo> list = mSpendHistory.getList();
				for (int i=0; i < count; i++) {
					SpendMonthlyInfo info = list.get(i);
					if (info.getMemo() != null && !info.getMemo().isEmpty()) {
						switch (i) {
							case 0:
								_listRow.mCardFSubInfoMemo1.setVisibility(View.VISIBLE);
								break;
							case 1:
								_listRow.mCardFSubInfoMemo2.setVisibility(View.VISIBLE);
								break;
							case 2:
								_listRow.mCardFSubInfoMemo3.setVisibility(View.VISIBLE);
								break;
						}
					} else {
						switch (i) {
							case 0:
								_listRow.mCardFSubInfoMemo1.setVisibility(View.INVISIBLE);
								break;
							case 1:
								_listRow.mCardFSubInfoMemo2.setVisibility(View.INVISIBLE);
								break;
							case 2:
								_listRow.mCardFSubInfoMemo3.setVisibility(View.INVISIBLE);
								break;
						}
					}
				}

				for(int f = 0; f < count; f++){
					switch (f){
						case 0:
							if(Integer.parseInt(mSpendHistory.getList().get(f).getPayType()) > 5){
								type1 = 5;
							}
							else {
								type1 = Integer.parseInt(mSpendHistory.getList().get(f).getPayType());
							}
							title1 = mSpendHistory.getList().get(f).getOpponent();
							sum1 = ElseUtils.getStringDecimalFormat(mSpendHistory.getList().get(f).getSum());
							date1 = ElseUtils.getSpendDate(mSpendHistory.getList().get(f).getExpenseDate());
							break;
						case 1:
							if(Integer.parseInt(mSpendHistory.getList().get(f).getPayType()) > 5){
								type2 = 5;
							}
							else {
								type2 = Integer.parseInt(mSpendHistory.getList().get(f).getPayType());
							}
							title2 = mSpendHistory.getList().get(f).getOpponent();
							sum2 = ElseUtils.getStringDecimalFormat(mSpendHistory.getList().get(f).getSum());
							date2 = ElseUtils.getSpendDate(mSpendHistory.getList().get(f).getExpenseDate());
							break;
						case 2:
							if(Integer.parseInt(mSpendHistory.getList().get(f).getPayType()) > 5){
								type3 = 5;
							}
							else {
								type3 = Integer.parseInt(mSpendHistory.getList().get(f).getPayType());
							}
							title3 = mSpendHistory.getList().get(f).getOpponent();
							sum3 = ElseUtils.getStringDecimalFormat(mSpendHistory.getList().get(f).getSum());
							date3 = ElseUtils.getSpendDate(mSpendHistory.getList().get(f).getExpenseDate());
							break;
					}
				}

				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardFLayout.setVisibility(View.VISIBLE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				String[] payType = mActivity.getResources().getStringArray(R.array.spend_pay_type);
				_listRow.mCardFTitle.setText(mActivity.getString(R.string.spend_history));
				_listRow.mCardFCount.setText(Integer.toString(mSpendHistory.getTotalCount()));
				_listRow.mCardFSubInfo1Tag.setText(payType[type1]);
				pay_type_set(_listRow.mCardFSubInfo1Tag, type1);
				_listRow.mCardFSubInfo1Title.setText(title1);
				_listRow.mCardFSubInfo1Cost.setText(sum1);
				_listRow.mCardFSubInfo1Txt.setText(date1);
				_listRow.mCardFSubInfo2Tag.setText(payType[type2]);
				pay_type_set(_listRow.mCardFSubInfo2Tag, type2);
				_listRow.mCardFSubInfo2Title.setText(title2);
				_listRow.mCardFSubInfo2Cost.setText(sum2);
				_listRow.mCardFSubInfo2Txt.setText(date2);
				_listRow.mCardFSubInfo3Tag.setText(payType[type3]);
				pay_type_set(_listRow.mCardFSubInfo3Tag, type3);
				_listRow.mCardFSubInfo3Title.setText(title3);
				_listRow.mCardFSubInfo3Cost.setText(sum3);
				_listRow.mCardFSubInfo3Txt.setText(date3);

			}
			else {
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
				_listRow.mCardETitle.setText(mActivity.getString(R.string.spend_history));
			}
		}
		else if (mSpendMainData.getBudgetInfo() != null && _position == 1) {        //카드추천 배너
			SpendCardBanner banner = mSpendMainData.getBanner();
			if (banner != null && banner.recommendYn > 0 && banner.diff > 0) {
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.VISIBLE);
				_listRow.mCardBannerBenefit.setText(ElseUtils.getDecimalFormat(banner.diff));
				if (banner.mileageYn > 0) {
					_listRow.mCardBannerBenefitUnit.setText(R.string.spend_card_recommend_unit_mileage);
				} else {
					_listRow.mCardBannerBenefitUnit.setText(R.string.spend_card_recommend_unit_won);
				}
			} else {
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
			}
		} else if (mSpendMainData.getBudgetInfo() != null && _position == 2){		//소비예산
			SpendBudgetInfo budgetInfo = mSpendMainData.getBudgetInfo();
			long spend = Long.valueOf(budgetInfo.getExpenseSum());
			long budget = Long.valueOf(budgetInfo.getBudgetSum());
			long value;

			if(budget != 0 || budgetInfo.getNextTotalSum() != 0) {
				_listRow.mCardIMainLayout.setVisibility(View.VISIBLE);
				_listRow.mCardIEmptyLayout.setVisibility(View.GONE);
				if (budget != 0) {
					value = (long)( (double)spend / (double)budget * 100.0 );
				} else if (budget == 0 && spend == 0) {
					value = 0;
				} else {
					value = 999;
				}

				long leftValue = budget - spend;
				if (value > 999) {
					value = 999;
				}
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.VISIBLE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
				_listRow.mCardITitle.setText(mActivity.getString(R.string.spend_budget));
				_listRow.mCardISubInfo1Cost.setText(ElseUtils.getDecimalFormat(budget));
				_listRow.mCardISubInfo2Cost.setText(ElseUtils.getDecimalFormat(spend));

				Animation animation = new AlphaAnimation(0.2f, 1);
				animation.setDuration(200);

				if (leftValue >= 0) {
					_listRow.mCardICurrentStatus.setText(ElseUtils.getDecimalFormat(leftValue)+" 남음");
				} else {
					_listRow.mCardICurrentStatus.setText(ElseUtils.getDecimalFormat(Math.abs(leftValue))+" 초과");
				}

				if (spend == 0) {
					_listRow.mCardIGraphCurrent.setVisibility(View.GONE);
					_listRow.mCardICurrentStatus.setTextColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage1_color));
					LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) _listRow.mCardIGraphAmount.getLayoutParams();
					amountParams.weight = 1;
					_listRow.mCardIGraphAmount.setLayoutParams(amountParams);
				} else {
					_listRow.mCardIGraphCurrent.setVisibility(View.VISIBLE);
					_listRow.mCardIGraphCurrent.setAnimation(animation);
					float spendValue = 0;
					if (budget != 0) {
						spendValue = (float) ((double)spend/(double)budget);
					} else {
						spendValue = spend == 0 ? 0f:1f;
					}

					if (spendValue > 1f) {
						spendValue = 1f;
					}
					float budgetValue = 1f - spendValue;

					LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) _listRow.mCardIGraphCurrent.getLayoutParams();
					params.weight = spendValue;
					if (spendValue >= 1f) {
						params.setMargins(0, 0, 0, 0);
					} else {
						params.setMargins(0, 0, ElseUtils.convertDp2Px(mActivity, 3), 0);
					}
					_listRow.mCardIGraphCurrent.setLayoutParams(params);

					LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) _listRow.mCardIGraphAmount.getLayoutParams();
					amountParams.weight = budgetValue;
					_listRow.mCardIGraphAmount.setLayoutParams(amountParams);

					Drawable amountDrawable = _listRow.mCardIGraphAmount.getBackground();
					if (amountDrawable instanceof ShapeDrawable) {
						ShapeDrawable shapeDrawable = (ShapeDrawable) amountDrawable;
						shapeDrawable.getPaint().setColor(mActivity.getResources().getColor(R.color.common_white));
					} else if (amountDrawable instanceof GradientDrawable) {
						GradientDrawable gradientDrawable = (GradientDrawable) amountDrawable;
						gradientDrawable.setColor(mActivity.getResources().getColor(R.color.common_white));
					}

					if(value <= 50){
						_listRow.mCardICurrentStatus.setTextColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage1_color));
						Drawable drawable = _listRow.mCardIGraphCurrent.getBackground();
						if (drawable instanceof ShapeDrawable) {
							ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
							shapeDrawable.getPaint().setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage1_color));
						} else if (drawable instanceof GradientDrawable) {
							GradientDrawable gradientDrawable = (GradientDrawable) drawable;
							gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage1_color));
						}
					} else if(value <= 100){
						_listRow.mCardICurrentStatus.setTextColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage2_color));
						Drawable drawable = _listRow.mCardIGraphCurrent.getBackground();
						if (drawable instanceof ShapeDrawable) {
							ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
							shapeDrawable.getPaint().setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage2_color));
						} else if (drawable instanceof GradientDrawable) {
							GradientDrawable gradientDrawable = (GradientDrawable) drawable;
							gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage2_color));
						}
					} else {
						_listRow.mCardICurrentStatus.setTextColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage3_color));
						Drawable drawable = _listRow.mCardIGraphCurrent.getBackground();
						if (drawable instanceof ShapeDrawable) {
							ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
							shapeDrawable.getPaint().setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage3_color));
						} else if (drawable instanceof GradientDrawable) {
							GradientDrawable gradientDrawable = (GradientDrawable) drawable;
							gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_budget_main_graph_bar_stage3_color));
						}
					}
				}
				if (budgetInfo.getDetailList() == null || budgetInfo.getDetailList().size() == 0) {
					_listRow.mCardIFavoriteEmpty.setVisibility(View.VISIBLE);
					_listRow.mCardIFavoriteListLayout.setVisibility(View.GONE);
					_listRow.mCardIFavoriteSetting.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(mActivity, Level2ActivitySpendBudgetFavoriteCategory.class);
							mActivity.startActivity(intent);
						}
					});
				} else {
					_listRow.mCardIFavoriteEmpty.setVisibility(View.GONE);
					_listRow.mCardIFavoriteListLayout.setVisibility(View.VISIBLE);
					_listRow.mCardIFavoriteEdit.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(mActivity, Level2ActivitySpendBudgetFavoriteCategory.class);
							mActivity.startActivity(intent);
						}
					});
					List<SpendBudgetDetail> list = budgetInfo.getDetailList();
					_listRow.mCardIFavoriteList.removeAllViews();
					for (SpendBudgetDetail detail : list) {
						View view = mActivity.getLayoutInflater().inflate(R.layout.row_item_child_main_budget_favorite, null);
						LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						view.setLayoutParams(params);
						String title = mDetailCategory[detail.largeCategoryId];
						TextView titleView = (TextView) view.findViewById(R.id.tv_row_list_item_main_budget_favorite_title);
						TextView expenseView = (TextView) view.findViewById(R.id.tv_row_list_item_main_budget_favorite_expense);
						TextView budgetView = (TextView) view.findViewById(R.id.tv_row_list_item_main_budget_favorite_budget);
						titleView.setText(title);
						long left = detail.sum - detail.expense;
						if (left >= 0) {
							expenseView.setText(ElseUtils.getDecimalFormat(left)+" "+mActivity.getResources().getString(R.string.common_left));
							expenseView.setTextColor(mActivity.getResources().getColor(R.color.common_maintext_color));
						} else {
							expenseView.setText(ElseUtils.getDecimalFormat(Math.abs(left))+" "+mActivity.getResources().getString(R.string.common_excess));
							expenseView.setTextColor(mActivity.getResources().getColor(R.color.common_red_color));
						}
						budgetView.setText(mActivity.getResources().getString(R.string.spend_budget)+" "+ElseUtils.getDecimalFormat(detail.sum));
						_listRow.mCardIFavoriteList.addView(view);
					}
				}
			} else {
				_listRow.mCardIMainLayout.setVisibility(View.GONE);
				_listRow.mCardIEmptyLayout.setVisibility(View.VISIBLE);
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
			}
		}
		else if (mSpendMainData.getCategory() != null && _position == 3){		//소비분류
			int type1 = 0, type2 = 0, type3 = 0;
			int count = 0;
			String sum1 = "0", sum2 ="0" , sum3 = "0";
			mCategory = mSpendMainData.getCategory();
			String[] categoryType = mActivity.getResources().getStringArray(R.array.spend_category);
			count = mCategory.size();

			if(count != 0){
				if(count == 1 && "25".equals(mCategory.get(0).getCategoryId())){
					_listRow.mCardMLayout.setVisibility(View.VISIBLE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardFLayout.setVisibility(View.GONE);
					_listRow.mCardILayout.setVisibility(View.GONE);
					_listRow.mCardJLayout.setVisibility(View.GONE);
					_listRow.mCardKLayout.setVisibility(View.GONE);
					_listRow.mCardBannerLayout.setVisibility(View.GONE);
					_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
					_listRow.mCardMSub.setText("분류이력 없음");
					_listRow.mCardMTitle.setText(mActivity.getString(R.string.spend_category));
				}
				else {
					int topCount = 0;

					for(int i = 0; i < count; i++){
						if(!("25".equals(mCategory.get(i).getCategoryId()))){
							switch (topCount){
								case 0:
									_listRow.mCardJLayoutSub_2.setVisibility(View.GONE);
									_listRow.mCardJLayoutSub_3.setVisibility(View.GONE);
									type1 = Integer.parseInt(mCategory.get(i).getCategoryId());
									sum1 = ElseUtils.getStringDecimalFormat(mCategory.get(i).getSum());
									topCount += 1;
									break;
								case 1:
									_listRow.mCardJLayoutSub_2.setVisibility(View.VISIBLE);
									_listRow.mCardJLayoutSub_3.setVisibility(View.GONE);
									type2 = Integer.parseInt(mCategory.get(i).getCategoryId());
									sum2 = ElseUtils.getStringDecimalFormat(mCategory.get(i).getSum());
									topCount += 1;
									break;
								case 2:
									_listRow.mCardJLayoutSub_2.setVisibility(View.VISIBLE);
									_listRow.mCardJLayoutSub_3.setVisibility(View.VISIBLE);
									type3 = Integer.parseInt(mCategory.get(i).getCategoryId());
									sum3 = ElseUtils.getStringDecimalFormat(mCategory.get(i).getSum());
									topCount += 1;
									break;
								default:
									break;
							}
						}
					}
					_listRow.mCardMLayout.setVisibility(View.GONE);
					_listRow.mCardELayout.setVisibility(View.GONE);
					_listRow.mCardFLayout.setVisibility(View.GONE);
					_listRow.mCardILayout.setVisibility(View.GONE);
					_listRow.mCardJLayout.setVisibility(View.VISIBLE);
					_listRow.mCardKLayout.setVisibility(View.GONE);
					_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
					_listRow.mCardJTitle.setText(mActivity.getString(R.string.spend_category));
					_listRow.mCardJTitleCount.setText(Integer.toString(count));
					_listRow.mCardJSubInfo1Title.setText(categoryType[type1]);
					_listRow.mCardJSubInfo1Cost.setText(sum1);
					_listRow.mCardJSubInfo2Title.setText(categoryType[type2]);
					_listRow.mCardJSubInfo2Cost.setText(sum2);
					_listRow.mCardJSubInfo3Title.setText(categoryType[type3]);
					_listRow.mCardJSubInfo3Cost.setText(sum3);
					PieChartInit(mActivity, _listRow.mCardJChart, count, _position);
				}
			}
			else {
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
				_listRow.mCardETitle.setText(mActivity.getString(R.string.spend_category));
			}


		}
		else if (mSpendMainData.getPayType() != null && _position == 4){		//소비방법
			int type1 = 0, type2 = 0, type3 = 0;
			int count = 0;
			String sum1 = "0", sum2 = "0", sum3 = "0";
			mPayType = mSpendMainData.getPayType();
			if(mPayType.size() != 0){
				count = mPayType.size();
				switch (count){
					case 1:
						_listRow.mCardKLayoutSub_2.setVisibility(View.GONE);
						_listRow.mCardKLayoutSub_3.setVisibility(View.GONE);
						break;
					case 2:
						_listRow.mCardKLayoutSub_2.setVisibility(View.VISIBLE);
						_listRow.mCardKLayoutSub_3.setVisibility(View.GONE);
						break;
					case 3:
						_listRow.mCardKLayoutSub_2.setVisibility(View.VISIBLE);
						_listRow.mCardKLayoutSub_3.setVisibility(View.VISIBLE);
						break;
				}

				for(int j = 0; j < count; j ++){
					switch (j){
						case 0:
							if(Integer.parseInt(mPayType.get(j).getPayType()) > 5){
								type1 = 5;
							}
							else {
								type1 = Integer.parseInt(mPayType.get(j).getPayType());
							}
							sum1 = ElseUtils.getStringDecimalFormat(mPayType.get(j).getSum());
							break;
						case 1:
							if(Integer.parseInt(mPayType.get(j).getPayType()) > 5)
							{
								type2 = 5;
							}
							else {
								type2 = Integer.parseInt(mPayType.get(j).getPayType());
							}
							sum2 = ElseUtils.getStringDecimalFormat(mPayType.get(j).getSum());
							break;
						case 2:
							if(Integer.parseInt(mPayType.get(j).getPayType()) > 5){
								type3 = 5;
							}
							else {
								type3 = Integer.parseInt(mPayType.get(j).getPayType());
							}
							sum3 = ElseUtils.getStringDecimalFormat(mPayType.get(j).getSum());
							break;
					}
				}

				String[] payType = mActivity.getResources().getStringArray(R.array.spend_pay_type);
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.VISIBLE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
				_listRow.mCardKTitle.setText(mActivity.getString(R.string.spend_way));
				_listRow.mCardKSubInfo1Title.setText(payType[type1]);
				_listRow.mCardKSubInfo1Cost.setText(sum1);
				_listRow.mCardKSubInfo2Title.setText(payType[type2]);
				_listRow.mCardKSubInfo2Cost.setText(sum2);
				_listRow.mCardKSubInfo3Title.setText(payType[type3]);
				_listRow.mCardKSubInfo3Cost.setText(sum3);
				PieChartInit(mActivity, _listRow.mCardKChart, count, _position);

			}
			else {
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.VISIBLE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.GONE);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
				_listRow.mCardETitle.setText(mActivity.getString(R.string.spend_way));
			}
		}
		else if (mSpendMainData != null && _position == 5){ // 카드정도 조건 변경 해야함
			if(mSpendMainData.getUserCardList() != null && mSpendMainData.getUserCardList().size() != 0){
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.VISIBLE);
				_listRow.mMainSpendCardEmptyLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardContentLayout.setVisibility(View.VISIBLE);

				_listRow.mMainSpendCardContentList.removeAllViews();
				List<SpendCardUserListData> list = mSpendMainData.getUserCardList();
				for (int i=0; i < list.size(); i++) {
					SpendCardUserListData userCard = list.get(i);
					View view = mActivity.getLayoutInflater().inflate(R.layout.row_list_item_spend_card, null);
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					view.setLayoutParams(params);
					ImageView logo = (ImageView) view.findViewById(R.id.iv_row_spend_card_logo);
					ImageView favorite = (ImageView) view.findViewById(R.id.iv_row_spend_card_favorite);
					View settingBtn = view.findViewById(R.id.fl_spend_card_setting);
					TextView titleView = (TextView) view.findViewById(R.id.tv_row_spend_card_title);
					TextView memoView = (TextView) view.findViewById(R.id.tv_row_spend_card_memo);
					TextView usedView = (TextView) view.findViewById(R.id.tv_row_spend_card_used);
					TextView requirementView = (TextView) view.findViewById(R.id.tv_row_spend_card_requirement);
					ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress_spend_card_graph);
					String title = null;
					settingBtn.setTag(userCard);
					long requirement = 0;
					if (userCard.card != null) {
						title = userCard.card.cardName;
						favorite.setVisibility(View.VISIBLE);
						settingBtn.setVisibility(View.GONE);
						if (userCard.bookmarkYn > 0) {
							favorite.setSelected(true);
						} else {
							favorite.setSelected(false);
						}
						favorite.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {

							}
						});
						requirement = userCard.card.requirement;
						requirementView.setText(usedView.getResources().getString(R.string.spend_card_need)+" "+ElseUtils.getDecimalFormat(requirement));
						logo.setAlpha(1f);
						titleView.setTextColor(mActivity.getResources().getColor(R.color.common_maintext_color));
						usedView.setTextColor(mActivity.getResources().getColor(R.color.common_maintext_color));
						requirementView.setTextColor(mActivity.getResources().getColor(R.color.common_maintext_color));
					} else {
						title = userCard.cardName;
						favorite.setVisibility(View.GONE);
						settingBtn.setVisibility(View.VISIBLE);
						settingBtn.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								SpendCardUserListData data = (SpendCardUserListData) v.getTag();
								if (data != null) {
									mSelectCardInfo = data;
								}
								String code = data.cmpnyCode;
								getCardList(code);
							}
						});
						requirementView.setText(usedView.getResources().getString(R.string.spend_card_need)+" "+0);
						logo.setAlpha(0.25f);
						titleView.setTextColor(mActivity.getResources().getColor(R.color.common_subtext_color));
						usedView.setTextColor(mActivity.getResources().getColor(R.color.common_subtext_color));
						requirementView.setTextColor(mActivity.getResources().getColor(R.color.common_subtext_color));
					}
					logo.setImageResource(ElseUtils.createCardLogoItem(userCard.cmpnyCode));
					titleView.setText(title);
					memoView.setText(userCard.memo);
					usedView.setText(usedView.getResources().getString(R.string.spend_card_usage)+" "+ElseUtils.getDecimalFormat(userCard.expenseAmt));
					float value = 0;
					if (requirement != 0) {
						value = (long)( (double)userCard.expenseAmt / (double)requirement * 100.0 );
					} else if (requirement == 0 && userCard.expenseAmt == 0) {
						value = 0;
					} else {
						value = 999;
					}

					if (userCard.card != null) {
						if (0 >= value) {
							usedView.setTextColor(usedView.getResources().getColor(R.color.common_subtext_color));
							progressBar.setProgress(0);
						} else if (value < 100) {
							usedView.setTextColor(usedView.getResources().getColor(R.color.spend_card_progress_font_color));
							progressBar.setProgress(((int) value));
							Drawable drawable = progressBar.getProgressDrawable();
							if (drawable instanceof LayerDrawable) {
								LayerDrawable layerDrawable = (LayerDrawable) drawable;
								ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
								shapeDrawable.setColorFilter(mActivity.getResources().getColor(R.color.spend_card_graph_progress_color), PorterDuff.Mode.SRC_IN);
							} else if (drawable instanceof GradientDrawable) {
								GradientDrawable gradientDrawable = (GradientDrawable) drawable;
								gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_card_graph_progress_color));
							}
						} else {
							usedView.setTextColor(usedView.getResources().getColor(R.color.spend_card_graph_over_color));
							progressBar.setProgress(progressBar.getMax());
							Drawable drawable = progressBar.getProgressDrawable();
							if (drawable instanceof LayerDrawable) {
								LayerDrawable layerDrawable = (LayerDrawable) drawable;
								ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
								shapeDrawable.setColorFilter(mActivity.getResources().getColor(R.color.spend_card_graph_over_color), PorterDuff.Mode.SRC_IN);
							} else if (drawable instanceof GradientDrawable) {
								GradientDrawable gradientDrawable = (GradientDrawable) drawable;
								gradientDrawable.setColor(mActivity.getResources().getColor(R.color.spend_card_graph_over_color));
							}
						}
					} else {
						usedView.setTextColor(usedView.getResources().getColor(R.color.common_subtext_color));
						progressBar.setProgress(0);
						Drawable drawable = progressBar.getProgressDrawable();
						if (drawable instanceof LayerDrawable) {
							LayerDrawable layerDrawable = (LayerDrawable) drawable;
							ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
							shapeDrawable.setColorFilter(mActivity.getResources().getColor(R.color.common_subtext_color), PorterDuff.Mode.SRC_IN);
						} else if (drawable instanceof GradientDrawable) {
							GradientDrawable gradientDrawable = (GradientDrawable) drawable;
							gradientDrawable.setColor(mActivity.getResources().getColor(R.color.common_subtext_color));
						}
					}

					_listRow.mMainSpendCardContentList.addView(view);
					if (i != list.size()-1) {
						LinearLayout line = new LinearLayout(mActivity);
						line.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
						line.setBackgroundColor(mActivity.getResources().getColor(R.color.divider_line_a_bg));
						_listRow.mMainSpendCardContentList.addView(line);
					}
				}
			}
			else {
				_listRow.mCardMLayout.setVisibility(View.GONE);
				_listRow.mCardELayout.setVisibility(View.GONE);
				_listRow.mCardFLayout.setVisibility(View.GONE);
				_listRow.mCardILayout.setVisibility(View.GONE);
				_listRow.mCardJLayout.setVisibility(View.GONE);
				_listRow.mCardKLayout.setVisibility(View.GONE);
				_listRow.mCardBannerLayout.setVisibility(View.GONE);
				_listRow.mMainSpendCardLayout.setVisibility(View.VISIBLE);
				_listRow.mMainSpendCardEmptyLayout.setVisibility(View.VISIBLE);
				_listRow.mMainSpendCardContentLayout.setVisibility(View.GONE);
				_listRow.mCardEIcon.setBackgroundResource(R.drawable.selector_action_add);
				_listRow.mCardETitle.setText(mActivity.getString(R.string.spend_card_info));
			}
		}
    }
    
    
//====================================================================//
// ViewHolder
//====================================================================//   
    private class ViewHolder {

		/* Card M */
		private LinearLayout 	mCardMLayout;
		private TextView		mCardMTitle;
		private TextView 		mCardMSub;

		/* Card E */
		private LinearLayout 	mCardELayout;
		private TextView		mCardETitle;
		private ImageView 		mCardEIcon;
    	
    	/* Card F */
    	private LinearLayout 	mCardFLayout;
		private LinearLayout 	mCardFLayoutSub_2;
		private LinearLayout 	mCardFLayoutSub_3;
    	private TextView		mCardFTitle;
    	private TextView 		mCardFCount;
    	private TextView		mCardFSubInfo1Title;
    	private TextView		mCardFSubInfo1Tag;
    	private TextView		mCardFSubInfo1Txt;
		private View			mCardFSubInfoMemo1;
    	private TextView		mCardFSubInfo1Cost;
    	private TextView		mCardFSubInfo2Title;
    	private TextView		mCardFSubInfo2Tag;
    	private TextView		mCardFSubInfo2Txt;
		private View			mCardFSubInfoMemo2;
    	private TextView		mCardFSubInfo2Cost;
    	private TextView		mCardFSubInfo3Title;
    	private TextView		mCardFSubInfo3Tag;
    	private TextView		mCardFSubInfo3Txt;
		private View			mCardFSubInfoMemo3;
    	private TextView		mCardFSubInfo3Cost;

		private LinearLayout		mCardBannerLayout;
		private TextView		mCardBannerBenefit;
		private TextView		mCardBannerBenefitUnit;
    	
    	/* Card I */
    	private LinearLayout 	mCardILayout;
    	private TextView		mCardITitle;

    	private ImageView 		mCardIIcon;
    	private TextView		mCardISubInfo1Cost;
    	private TextView		mCardISubInfo2Cost;
		private LinearLayout    mCardIGraphCurrent;
		private LinearLayout    mCardIGraphAmount;
		private TextView    	mCardICurrentStatus;
		private LinearLayout	mCardIMainLayout;
		private LinearLayout	mCardIFavoriteListLayout;
		private LinearLayout	mCardIFavoriteList;
		private View			mCardIFavoriteEdit;
		private LinearLayout	mCardIFavoriteEmpty;
		private View			mCardIFavoriteSetting;
		private LinearLayout	mCardIEmptyLayout;
    	
    	/* Card J */
    	private LinearLayout 	mCardJLayout;
		private LinearLayout 	mCardJLayoutSub_2;
		private LinearLayout 	mCardJLayoutSub_3;
    	private TextView		mCardJTitle;
    	private TextView		mCardJTitleCount;
		private PieChart		mCardJChart;
    	private TextView		mCardJSubInfo1Title;
    	private TextView		mCardJSubInfo1Cost;
    	private TextView		mCardJSubInfo2Title;
    	private TextView		mCardJSubInfo2Cost;
    	private TextView		mCardJSubInfo3Title;
    	private TextView		mCardJSubInfo3Cost;
    	
    	/* Card K */
    	private LinearLayout 	mCardKLayout;
		private LinearLayout 	mCardKLayoutSub_2;
		private LinearLayout 	mCardKLayoutSub_3;
    	private TextView		mCardKTitle;
    	private PieChart 		mCardKChart;
    	private TextView		mCardKSubInfo1Title;
    	private TextView		mCardKSubInfo1Cost;
    	private TextView		mCardKSubInfo2Title;
    	private TextView		mCardKSubInfo2Cost;
    	private TextView		mCardKSubInfo3Title;
    	private TextView		mCardKSubInfo3Cost;

		/* Card L */
		private LinearLayout mMainSpendCardLayout;
		private LinearLayout mMainSpendCardEmptyLayout;
		private LinearLayout mMainSpendCardContentLayout;
		private LinearLayout mMainSpendCardContentList;
    	
    }
    
    
//====================================================================//
// Data
//====================================================================//
    public void setData(ArrayList<SpendMainData> array){
    	mAlDataList = array;
	}


//=======================================================================================//
// Pay Type Set
//=======================================================================================//
	private void pay_type_set(TextView textView, int payTypeInt){
		switch (payTypeInt){
			case 1: //신용카드
				textView.setTextAppearance(mActivity, R.style.CS_TagA1);
				textView.setBackgroundResource(R.drawable.shape_tag_a_bg_1);
				break;
			case 2: //체크카드
				textView.setTextAppearance(mActivity, R.style.CS_TagA2);
				textView.setBackgroundResource(R.drawable.shape_tag_a_bg_2);
				break;
			case 3: //현금
				textView.setTextAppearance(mActivity, R.style.CS_TagA4);
				textView.setBackgroundResource(R.drawable.shape_tag_a_bg_4);
				break;
			case 4: //계좌이체
				textView.setTextAppearance(mActivity, R.style.CS_TagA3);
				textView.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
				break;
			case 5: //기타
				textView.setTextAppearance(mActivity, R.style.CS_TagA3);
				textView.setBackgroundResource(R.drawable.shape_tag_a_bg_3);
				break;
		}
	}

	private List<ResponseCardList> mCardInfoList;
	private SpendCardUserListData mSelectCardInfo;

	private void getCardList(String companyCode){
		DialogUtils.showLoading((Activity)mActivity);	//소비 로딩 추가
		RequestCardList mRequestCardList = new RequestCardList();
		mRequestCardList.setUserId(Session.getInstance(mActivity).getUserId());
		mRequestCardList.setCompanyCode(companyCode);

		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<List<ResponseCardList>> call = api.getCardList(mRequestCardList);
		call.enqueue(new Callback<List<ResponseCardList>>() {
			@Override
			public void onResponse(Call<List<ResponseCardList>> call, Response<List<ResponseCardList>> response) {
				DialogUtils.dismissLoading();	//소비 로딩 추가
				mCardInfoList = new ArrayList<ResponseCardList>();
				ResponseCardList mNoneData = new ResponseCardList();
				mNoneData.setCardCode("");
				mNoneData.setCardName(mActivity.getString(R.string.common_text_no));
				mCardInfoList.add(mNoneData);
				for (int r = 0; r < response.body().size(); r++) {
					mCardInfoList.add(response.body().get(r));
				}

				dialog_card_list();//리스트 팝업 부르기
			}

			@Override
			public void onFailure(Call<List<ResponseCardList>> call, Throwable t) {
				DialogUtils.dismissLoading();	//소비 로딩 추가
				ElseUtils.network_error(mActivity);
			}
		});
	}


	private void card_mapping(String cardId, String cardCode) {
		DialogUtils.dismissLoading();	//소비 로딩 추가
		HashMap<String, Object> map = new HashMap<>();
		map.put("cardId", Long.valueOf(cardId));
		map.put("cardCode", cardCode);

		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<Result> call = api.setMatchingCard(map);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();	//소비 로딩 추가
				if(response.body().isOk()){
					if (mRefreshListener != null) {
						mRefreshListener.refresh();
					}
				} else {
					Toast.makeText(mActivity, response.body().getDesc(), Toast.LENGTH_SHORT).show();
				}

			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();	//소비 로딩 추가
				ElseUtils.network_error(mActivity);
			}
		});

	}

	private void dialog_card_list(){
		DialogUtils.showDialogCardSelectList((mActivity), mCardInfoList, null, new ExpandableListView.OnChildClickListener(){

			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				ResponseCardList mCardList = (ResponseCardList) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);
				String cardId;
				if (id == -1) {
					cardId = "";
				} else {
					if (mSelectCardInfo != null) {
						cardId = String.valueOf(mSelectCardInfo.cardId);
						card_mapping(cardId, mCardList.getCardCode());
					}
				}
				return false;
			}

		}).show();
	}

//=======================================================================================//
// Chart Init for pie
//=======================================================================================//
	private void PieChartInit(Context context, PieChart _pieChart, int value, int position){

		_pieChart.setUsePercentValues(true);
		_pieChart.setDescription("");
		//_listRow.mCardJChart.setExtraOffsets(5, 10, 5, 5);

		_pieChart.setDragDecelerationFrictionCoef(0.95f);

//		tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
//
//		mChart.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
//		mChart.setCenterText(generateCenterSpannableText());

		_pieChart.setDrawHoleEnabled(true);
		_pieChart.setHoleColorTransparent(false);
		_pieChart.setTouchEnabled(false);

//		_listRow.mCardJChart.setTransparentCircleColor(Color.WHITE);
//		_listRow.mCardJChart.setTransparentCircleAlpha(110);

		_pieChart.setHoleRadius(39.2f);
		_pieChart.setTransparentCircleRadius(39.2f);

		_pieChart.setDrawCenterText(false);
		_pieChart.setDrawSliceText(false);

		_pieChart.setRotationAngle(0);
		// enable rotation of the chart by touch
		_pieChart.setRotationEnabled(false);
		_pieChart.setHighlightPerTapEnabled(false);

		// mChart.setUnit(" €");
		// mChart.setDrawUnitsInChart(true);

		// add a selection listener
		//_listRow.mCardJChart.setOnChartValueSelectedListener(context);

		setData(_pieChart, 100, value, position);

		//_listRow.mCardJChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
		// mChart.spin(2000, 0, 360);

		Legend l = _pieChart.getLegend();
		l.setEnabled(false);
	}

	private void setData(PieChart _chart, float range, int count, int type) {

		float mult = range;

		ArrayList<Entry> yVals1 = new ArrayList<Entry>();

		// IMPORTANT: In a PieChart, no values (Entry) should have the same
		// xIndex (even if from different DataSets), since no values can be
		// drawn above each other.
		for (int i = 0; i < count; i++) {
			if(type == 3){
				if(!"25".equals(mCategory.get(i).getCategoryId())){
					float mCategoryValue = Float.valueOf(mCategory.get(i).getSum());
					yVals1.add(new Entry (mCategoryValue, i));
				}
			}
			else{
				float mPayTypeValue = Float.valueOf(mPayType.get(i).getSum());
				yVals1.add(new Entry (mPayTypeValue, i));
			}
		}

		ArrayList<String> xVals = new ArrayList<String>();

		for (int i = 0; i < count; i++)
			xVals.add(mParties[i % mParties.length]);

		PieDataSet dataSet = new PieDataSet(yVals1, "Election Results");
		dataSet.setSliceSpace(2f);
		dataSet.setSelectionShift(5f);

		// add a lot of colors

		ArrayList<Integer> colors = new ArrayList<Integer>();

		if(count == 0){
			colors.add(Color.rgb(217, 217, 217));
		}
		else if (count != 0 && type == 2){
			for (int c : GROUP_COLORS)
				colors.add(c);
		}
		else {
			for (int c : WAY_COLORS)
				colors.add(c);
		}
		dataSet.setColors(colors);
		//dataSet.setSelectionShift(0f);

		PieData data = new PieData(xVals, dataSet);
		data.setDrawValues(false);
		//data.setValueFormatter(new PercentFormatter());
		//data.setValueTextSize(11f);
		//data.setValueTextColor(Color.WHITE);
		//data.setValueTypeface(tf);
		_chart.setData(data);

		// undo all highlights
		_chart.highlightValues(null);

		_chart.invalidate();
	}

	protected String[] mParties = new String[] {
			"Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
			"Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
			"Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
			"Party Y", "Party Z"
	};

	public static final int[] GROUP_COLORS = {
			Color.rgb(54, 130, 245), Color.rgb(57, 158, 230), Color.rgb(32, 177, 195),
			Color.rgb(45, 162, 166), Color.rgb(60, 146, 169), Color.rgb(82, 125, 174),
			Color.rgb(105, 104, 178), Color.rgb(132, 81, 178), Color.rgb(167, 63, 178),
			Color.rgb(197, 79, 144), Color.rgb(354, 103, 113), Color.rgb(218, 129, 81),
			Color.rgb(230, 153, 46), Color.rgb(230, 137, 46), Color.rgb(230, 123, 51),
			Color.rgb(230, 107, 59), Color.rgb(217, 78, 83)
	};

	public static final int[] WAY_COLORS = {
			Color.rgb(32, 177, 195), Color.rgb(105, 104, 178), Color.rgb(230, 137, 46)
	};
}
