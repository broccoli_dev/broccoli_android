package com.nomadconnection.broccoli;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nomadconnection.broccoli.activity.AlertDialogActivity;
import com.nomadconnection.broccoli.activity.CertListActivity;
import com.nomadconnection.broccoli.activity.CertPasswordActivity;
import com.nomadconnection.broccoli.activity.GuideAuthCopyActivity;
import com.nomadconnection.broccoli.activity.GuideAuthRegisterActivity;
import com.nomadconnection.broccoli.activity.GuideCertRegisterActivity;
import com.nomadconnection.broccoli.activity.Intro;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetCredit;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetDaS;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetLoan;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetOther;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetStock;
import com.nomadconnection.broccoli.activity.Level2ActivityNotice;
import com.nomadconnection.broccoli.activity.Level2ActivityOtherChall;
import com.nomadconnection.broccoli.activity.Level2ActivityOtherMini;
import com.nomadconnection.broccoli.activity.Level2ActivitySpendCategory;
import com.nomadconnection.broccoli.activity.Level2ActivitySpendHistory;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetCreditDetail;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetDasDetail;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetEditAccount;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetOtherDetail;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetStockDetail;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetStockSearch;
import com.nomadconnection.broccoli.activity.Level3ActivityOtherChallDetail;
import com.nomadconnection.broccoli.activity.Level3ActivityOtherMiniDetail;
import com.nomadconnection.broccoli.activity.Level3ActivitySpendCategoryDetail;
import com.nomadconnection.broccoli.activity.Level3ActivitySpendHistoryDetail;
import com.nomadconnection.broccoli.activity.Level4ActivityAssetStockHistoryDetail;
import com.nomadconnection.broccoli.activity.MainActivity;
import com.nomadconnection.broccoli.activity.Splash;
import com.nomadconnection.broccoli.activity.asset.Level3ActivityAssetCardLoanDetail;
import com.nomadconnection.broccoli.activity.setting.Level2ActivityBasicInfo;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFaq;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancial;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialReg;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingGuide;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingNotice;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingPassword;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingQuestion;
import com.nomadconnection.broccoli.activity.setting.Level4ActivitySettingInitialization;
import com.nomadconnection.broccoli.activity.setting.Level4ActivitySettingInitializationComplete;
import com.nomadconnection.broccoli.activity.setting.Level4ActivitySettingNoticeDetail;
import com.nomadconnection.broccoli.activity.setting.Level4ActivitySettingQnaDetail;
import com.nomadconnection.broccoli.activity.setting.Level4ActivitySettingUserInfoUpdate;
import com.nomadconnection.broccoli.data.InfoScreen;
import com.nomadconnection.broccoli.fragment.FragmentAssetCreditDetailA;
import com.nomadconnection.broccoli.fragment.FragmentAssetOther0A;
import com.nomadconnection.broccoli.fragment.FragmentAssetOther0B;
import com.nomadconnection.broccoli.fragment.FragmentAssetOtherCustom;
import com.nomadconnection.broccoli.fragment.FragmentAssetOtherListView;
import com.nomadconnection.broccoli.fragment.FragmentAssetStock0;
import com.nomadconnection.broccoli.fragment.FragmentAssetStock1;
import com.nomadconnection.broccoli.fragment.FragmentAssetStockDetail1;
import com.nomadconnection.broccoli.fragment.FragmentMain1;
import com.nomadconnection.broccoli.fragment.FragmentMain2;
import com.nomadconnection.broccoli.fragment.FragmentMain3;
import com.nomadconnection.broccoli.fragment.FragmentMain4;
import com.nomadconnection.broccoli.fragment.FragmentOtherMini0;
import com.nomadconnection.broccoli.fragment.FragmentOtherMini1;
import com.nomadconnection.broccoli.fragment.FragmentOtherMini1A;
import com.nomadconnection.broccoli.interf.LifecycleCallbacks;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


@ReportsCrashes(
		// Dialog 형태로 알림
		mode                    = ReportingInteractionMode.TOAST,
		resToastText = R.string.crash_notif_text,
//		resDialogIcon = android.R.drawable.ic_dialog_info,
//		resDialogTitle = R.string.crash_notif_title,
//		resDialogText = R.string.crash_notif_message,
		// Dialog OK 선택시 메일 발송
		mailTo       = "hmjang@yellofg.com"  // 마지막에 세미콜론이 붙지 않음.
)
public class MainApplication extends MultiDexApplication implements LifecycleCallbacks{

	public static final String TAG = MainApplication.class.getSimpleName();
	
	protected static MainApplication singletonApplication;
	public static Context mAPPContext;
	public static Resources mAPPResources;
	public static boolean sAPP_IsRunning = false;				// Simply Check - App running
	public static APPSharedPrefer mAPPShared;
	
	/* Device Info */
	public static DisplayMetrics sDisplayMetrics;

	private static final String DEFAULT_LANGUAGE = "kr";
	private static final String[] LANGUAGES_ARR;
	private static final Set<String> LANGUAGES_SET;
	static
	{
		LANGUAGES_ARR = new String[] { "kr", "en" };
		LANGUAGES_SET = new HashSet(Arrays.asList(LANGUAGES_ARR));
	}
	
	public static MainApplication get() {
		return singletonApplication;
	}
	
//=========================================================================================//
// @Override
//=========================================================================================//
	@Override
	public void onCreate() {
		//FontClass.setDefaultFont(this, "SANS_SERIF", "NotoSansCJKkr-Regular.otf");
		super.onCreate();
//		if (BuildConfig.DEBUG){
			ACRA.init(this);
//		}
		singletonApplication = this;
		mAPPContext = getApplicationContext();
		mAPPResources = getApplicationContext().getResources();
		mAPPShared   = new APPSharedPrefer(this.getApplicationContext());
		sAPP_IsRunning = true;

		Session.getInstance(this.getApplicationContext()).addIgnoredActivity(AlertDialogActivity.class);
		
		/* Device Info */
		sDisplayMetrics = getApplicationContext().getResources().getDisplayMetrics();
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		sAPP_IsRunning = false;
	}

	private Tracker mTracker;

	/**
	 * Gets the default {@link Tracker} for this {@link Application}.
	 * @return tracker
	 */
	synchronized public Tracker getDefaultTracker() {
		if (mTracker == null) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this.getApplicationContext());
			// To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
			mTracker = analytics.newTracker(R.xml.global_tracker);
		}
		return mTracker;
	}


//=========================================================================================//
// ApplicationVersion
//=========================================================================================//
	/** App 정보 읽기 - Version Code **/
	public static String getVersionCode() {
		String sVerCode = "";
		try
		{
			PackageManager pm = MainApplication.mAPPContext.getPackageManager();
			PackageInfo packageInfo = pm.getPackageInfo(MainApplication.mAPPContext.getPackageName(), 0);
			String version = packageInfo.versionName;
			sVerCode = Integer.toString(packageInfo.versionCode);
		}
		catch (Exception localException) {
		}

		return sVerCode;
	}

	/** App 정보 읽기 - Version Name **/
	public static String getVersionName() {
		String sVerName = "";
		try
		{
			PackageManager pm = MainApplication.mAPPContext.getPackageManager();
			PackageInfo packageInfo = pm.getPackageInfo(MainApplication.mAPPContext.getPackageName(), 0);
			sVerName = packageInfo.versionName;
		}
		catch (Exception localException) {
		}

		return sVerName;
	}


	
	
//=========================================================================================//
// Screen
//=========================================================================================//
	public static class Screen {
//		MainApplication.Screen.getInstance().getScreenInfo("FragmentMain1")
		private static Screen mInstance = null;
		static HashMap <String, InfoScreen> screensMap; 
		
		public Screen(){
			super();
			screensMap = new HashMap <String, InfoScreen>();
			setScreenInfo();
		}
		
		public static Screen getInstance(){
			if(mInstance == null || screensMap == null){
				mInstance = new Screen();
			}
			return mInstance;
		}
		
		public InfoScreen getScreenInfo(String _Tag){
			if (screensMap.get(_Tag) != null)
				return screensMap.get(_Tag);
			else
				return screensMap.get("default");
		}
		
		private void setScreenInfo(){
			// 메인
			screensMap.put("default", new InfoScreen("default", "Title", 0, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Splash.class.getSimpleName(), new InfoScreen(Splash.class.getSimpleName(), "", 0, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Intro.class.getSimpleName(), new InfoScreen(Intro.class.getSimpleName(), "", 0, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(MainActivity.class.getSimpleName(), new InfoScreen(MainActivity.class.getSimpleName(), "메인", 0, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentMain1.class.getSimpleName(), new InfoScreen(FragmentMain1.class.getSimpleName(), "자산", 0, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentMain2.class.getSimpleName(), new InfoScreen(FragmentMain2.class.getSimpleName(), "소비", 0, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentMain3.class.getSimpleName(), new InfoScreen(FragmentMain3.class.getSimpleName(), "주식", 0, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentMain4.class.getSimpleName(), new InfoScreen(FragmentMain4.class.getSimpleName(), "기타", 0, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			// 메인 : 자산
			screensMap.put(Level2ActivityAssetDaS.class.getSimpleName(), new InfoScreen(Level2ActivityAssetDaS.class.getSimpleName(), "예금/적금", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level2ActivityAssetCredit.class.getSimpleName(), new InfoScreen(Level2ActivityAssetCredit.class.getSimpleName(), "신용카드", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level2ActivityAssetStock.class.getSimpleName(), new InfoScreen(Level2ActivityAssetStock.class.getSimpleName(), "주식", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level2ActivityAssetLoan.class.getSimpleName(), new InfoScreen(Level2ActivityAssetLoan.class.getSimpleName(), "대출", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level2ActivityAssetOther.class.getSimpleName(), new InfoScreen(Level2ActivityAssetOther.class.getSimpleName(), "기타재산", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			screensMap.put(Level3ActivityAssetDasDetail.class.getSimpleName(), new InfoScreen(Level3ActivityAssetDasDetail.class.getSimpleName(), "예금/적금 상세", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivityAssetEditAccount.class.getSimpleName(), new InfoScreen(Level3ActivityAssetEditAccount.class.getSimpleName(), mAPPContext.getString(R.string.asset_edit_account_title), R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivityAssetCreditDetail.class.getSimpleName(), new InfoScreen(Level3ActivityAssetCreditDetail.class.getSimpleName(), "신용카드 상세", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivityAssetStockDetail.class.getSimpleName(), new InfoScreen(Level3ActivityAssetStockDetail.class.getSimpleName(), "매수 내역 상세", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivityAssetOtherDetail.class.getSimpleName(), new InfoScreen(Level3ActivityAssetOtherDetail.class.getSimpleName(), "기타재산 상세/편집", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivityAssetCardLoanDetail.class.getSimpleName(), new InfoScreen(Level3ActivityAssetCardLoanDetail.class.getSimpleName(), "장기카드대출", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivityAssetStockSearch.class.getSimpleName(), new InfoScreen(Level3ActivityAssetStockSearch.class.getSimpleName(), "종목 찾기", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level4ActivityAssetStockHistoryDetail.class.getSimpleName(), new InfoScreen(Level4ActivityAssetStockHistoryDetail.class.getSimpleName(), "매수 내역 상세/편집", R.color.main_1_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			screensMap.put(FragmentAssetCreditDetailA.class.getSimpleName(), new InfoScreen(FragmentAssetCreditDetailA.class.getSimpleName(), "신용카드 상세 Viewpager", R.color.main_1_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentAssetStock0.class.getSimpleName(), new InfoScreen(FragmentAssetStock0.class.getSimpleName(), "매수 내역 추가", R.color.main_1_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentAssetStock1.class.getSimpleName(), new InfoScreen(FragmentAssetStock1.class.getSimpleName(), "주식", R.color.main_1_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentAssetStockDetail1.class.getSimpleName(), new InfoScreen(FragmentAssetStockDetail1.class.getSimpleName(), "매수 내역 상세", R.color.main_1_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentAssetOther0A.class.getSimpleName(), new InfoScreen(FragmentAssetOther0A.class.getSimpleName(), "부동산 등록", R.color.main_1_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentAssetOther0B.class.getSimpleName(), new InfoScreen(FragmentAssetOther0B.class.getSimpleName(), "차량 등록", R.color.main_1_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentAssetOtherCustom.class.getSimpleName(), new InfoScreen(FragmentAssetOtherCustom.class.getSimpleName(), "기타 등록", R.color.main_1_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentAssetOtherListView.class.getSimpleName(), new InfoScreen(FragmentAssetOtherListView.class.getSimpleName(), "기타 재산", R.color.main_1_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			// 메인 : 소비
			screensMap.put(Level2ActivitySpendHistory.class.getSimpleName(), new InfoScreen(Level2ActivitySpendHistory.class.getSimpleName(), "소비이력", R.color.main_2_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level2ActivitySpendCategory.class.getSimpleName(), new InfoScreen(Level2ActivitySpendCategory.class.getSimpleName(), "소비분류", R.color.main_2_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			screensMap.put(Level3ActivitySpendHistoryDetail.class.getSimpleName(), new InfoScreen(Level3ActivitySpendHistoryDetail.class.getSimpleName(), "소비이력상세", R.color.main_2_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivitySpendCategoryDetail.class.getSimpleName(), new InfoScreen(Level3ActivitySpendCategoryDetail.class.getSimpleName(), "소비분류상세", R.color.main_2_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			// 메인 : 기타
			screensMap.put(Level2ActivityOtherChall.class.getSimpleName(), new InfoScreen(Level2ActivityOtherChall.class.getSimpleName(), "챌린지", R.color.main_4_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level2ActivityOtherMini.class.getSimpleName(), new InfoScreen(Level2ActivityOtherMini.class.getSimpleName(), "머니 캘린더", R.color.main_4_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			screensMap.put(Level3ActivityOtherMiniDetail.class.getSimpleName(), new InfoScreen(Level3ActivityOtherMiniDetail.class.getSimpleName(), "머니 캘린더 상세", R.color.main_4_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivityOtherChallDetail.class.getSimpleName(), new InfoScreen(Level3ActivityOtherChallDetail.class.getSimpleName(), "챌린지 상세", R.color.main_4_layout_bg, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			screensMap.put(FragmentOtherMini0.class.getSimpleName(), new InfoScreen(FragmentOtherMini0.class.getSimpleName(), "머니 캘린더 추가", R.color.main_4_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentOtherMini1.class.getSimpleName(), new InfoScreen(FragmentOtherMini1.class.getSimpleName(), "머니 캘린더", R.color.main_4_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(FragmentOtherMini1A.class.getSimpleName(), new InfoScreen(FragmentOtherMini1A.class.getSimpleName(), "머니 캘린더 상세 Viewpager", R.color.main_4_layout_bg, "off", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			screensMap.put(Level2ActivityNotice.class.getSimpleName(), new InfoScreen(Level2ActivityNotice.class.getSimpleName(), "알림", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level2ActivityBasicInfo.class.getSimpleName(), new InfoScreen(Level2ActivityBasicInfo.class.getSimpleName(), "기본 정보", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			screensMap.put(Level3ActivitySettingFinancial.class.getSimpleName(), new InfoScreen(Level3ActivitySettingFinancial.class.getSimpleName(), "금융 기관 등록", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivitySettingFinancialReg.class.getSimpleName(), new InfoScreen(Level3ActivitySettingFinancialReg.class.getSimpleName(), "금융 기관 등록", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivitySettingPassword.class.getSimpleName(), new InfoScreen(Level3ActivitySettingPassword.class.getSimpleName(), "비밀번호 설정", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivitySettingNotice.class.getSimpleName(), new InfoScreen(Level3ActivitySettingNotice.class.getSimpleName(), "공지사항", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivitySettingFaq.class.getSimpleName(), new InfoScreen(Level3ActivitySettingFaq.class.getSimpleName(), "자주 묻는 질문", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivitySettingQuestion.class.getSimpleName(), new InfoScreen(Level3ActivitySettingQuestion.class.getSimpleName(), "문의하기", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level3ActivitySettingGuide.class.getSimpleName(), new InfoScreen(Level3ActivitySettingGuide.class.getSimpleName(), "이용 안내", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));

			screensMap.put(Level4ActivitySettingUserInfoUpdate.class.getSimpleName(), new InfoScreen(Level4ActivitySettingUserInfoUpdate.class.getSimpleName(), "실명 인증", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level4ActivitySettingNoticeDetail.class.getSimpleName(), new InfoScreen(Level4ActivitySettingNoticeDetail.class.getSimpleName(), "공지사항 상세", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level4ActivitySettingQnaDetail.class.getSimpleName(), new InfoScreen(Level4ActivitySettingQnaDetail.class.getSimpleName(), "자주 묻는 질문 상세", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level4ActivitySettingInitialization.class.getSimpleName(), new InfoScreen(Level4ActivitySettingInitialization.class.getSimpleName(), "브로콜리 초기화", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(Level4ActivitySettingInitializationComplete.class.getSimpleName(), new InfoScreen(Level4ActivitySettingInitializationComplete.class.getSimpleName(), "회원탈퇴 완료", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(CertListActivity.class.getSimpleName(), new InfoScreen(CertListActivity.class.getSimpleName(), "인증서 로그인", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(CertPasswordActivity.class.getSimpleName(), new InfoScreen(CertPasswordActivity.class.getSimpleName(), "인증서 비밀번호", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(GuideCertRegisterActivity.class.getSimpleName(), new InfoScreen(GuideCertRegisterActivity.class.getSimpleName(), "공인인증서 등록하기", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(GuideAuthCopyActivity.class.getSimpleName(), new InfoScreen(GuideAuthCopyActivity.class.getSimpleName(), "인증서 복사 방법", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
			screensMap.put(GuideAuthRegisterActivity.class.getSimpleName(), new InfoScreen(GuideAuthRegisterActivity.class.getSimpleName(), "인증서 등록 방법", R.color.home_tag_b_bg_dark, "on", R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out));
		}
		
	}
  
	
//=========================================================================================//
// Interface - LifeCycle
//=========================================================================================//
	@Override
	public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

	}

	@Override
	public void onActivityStarted(Activity activity) {

	}

	@Override
	public void onActivityResumed(Activity activity) {

	}

	@Override
	public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

	}
	
	@Override
	public void onActivityPaused(Activity activity) {

	}

	@Override
	public void onActivityStopped(Activity activity) {

	}
	
	@Override
	public void onActivityDestroyed(Activity activity) {

	}



}
