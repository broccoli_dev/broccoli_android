package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 2017. 3. 30..
 */

public enum BenefitInternalType {
    NONE,
    RATE,
    UNIT,
    AMOUNT
}
