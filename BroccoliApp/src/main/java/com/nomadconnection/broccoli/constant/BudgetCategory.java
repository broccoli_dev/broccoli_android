package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 2017. 3. 13..
 */

public enum BudgetCategory {
    NONE,
    EAT_OUT,
    CAFE_BAKERY,
    THE_DRINK_PLEASURE,
    MART,
    LIVING,
    TRAFFIC,
    OILING_CAR,
    COMMUNICATION,
    SHOPPING,
    ONLINE_SHOPPING,
    BEAUTY,
    MOVIE_CULTURE,
    BOOKSTORE,
    LEISURE_SPORT,
    HEALTH,
    TRAVEL_STAY,
    STUDY,
    BABY,
    PET,
    FAMILY_EVENT_RELIGION,
    FINANCIAL,
    TAX,
    OVERSEAS_PAY,
    ETC,
    UNCLASSIFIED;
}
