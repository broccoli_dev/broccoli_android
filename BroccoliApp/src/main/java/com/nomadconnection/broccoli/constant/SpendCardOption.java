package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardOption {

    public enum CardType {
        ALL,
        CREDIT_CARD,
        CHECK_CARD
    }

    public enum BenefitType {
        DISCOUNT_POINT,
        DISCOUNT,
        POINT,
        MILEAGE
    }

    public enum AnnualFeeRange {
        FEE_NONE,
        FEE_BELOW_30000,
        FEE_BELOW_100000,
        FEE_MORE_100000,
        FEE_TOTAL
    }

}
