package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 2017. 2. 21..
 */

public class SharedPreferenceConstant {

    /**
     * CDN cache로 사용
     */
    public static final String CDN_INIT_KEY = "CDN_INIT_KEY";

}
