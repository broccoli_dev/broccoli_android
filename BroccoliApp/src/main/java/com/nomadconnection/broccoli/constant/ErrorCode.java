package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 16. 7. 18..
 */
public class ErrorCode {

    public static final String ERROR_CODE_502_LOGIN_DUPLICATE_ACCOUNT = "502";
    public static final String ERROR_CODE_503_LOGIN_INVALID_ACCOUNT = "503";
    public static final String ERROR_CODE_504_OVER_DAILY_REQUEST = "504";
    public static final String ERROR_CODE_601_REALNAME_REQUEST = "601";
    public static final String ERROR_CODE_605_REALNAME_REQUEST = "605";
    public static final String ERROR_CODE_605_STOCK_MARKET_CLOSED = "605";
    public static final String ERROR_CODE_701_LOGIN_INCORRECT_DEVICE = "701";
    public static final String ERROR_CODE_702_CHECK_ID_DUPLICATE = "702";

    public static final String ERROR_DESC_601_UNDER_14_AGE = "Under_14_Age";
    public static final String ERROR_DESC_504_OVER_DAILY_REQUEST = "Over Daily Request";

}
