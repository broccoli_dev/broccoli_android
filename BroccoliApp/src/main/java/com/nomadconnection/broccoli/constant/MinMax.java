package com.nomadconnection.broccoli.constant;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 27..
 */

public enum MinMax implements Serializable {
    @SerializedName("NONE")
    NONE,
    @SerializedName("LESS")
    LESS,
    @SerializedName("LESS_EQUAL")
    LESS_EQUAL,
    @SerializedName("EQUAL")
    EQUAL,
    @SerializedName("GREATER")
    GREATER,
    @SerializedName("GREATER_EQUAL")
    GREATER_EQUAL

}
