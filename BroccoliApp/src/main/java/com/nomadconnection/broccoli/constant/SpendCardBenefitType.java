package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public enum  SpendCardBenefitType {

    DISCOUNT,
    POINT,
    MILEAGE

}
