package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 16. 1. 22..
 */
public class Const {

    public static final String TOKEN = "accessToken";

    public static final String DATA = "DATA";
    public static final String CERT_PWD = "CERT_PASSWORD";
    public static final String CERT_NAME = "CERT_NAME";
    public static final String CERT_VALIDDATE = "CERT_VALIDDATE";
    public static final String USER_NAME = "NAME";
    public static final String FROM_MEMBERSHIP = "FROM_MEMBERSHIP";
    public static final String FROM_DP_LOGIN = "FROM_LOGIN";

    public static final String CERTIFY_KEY = "CERTIFY_KEY";

    public static final String TOTAL_SPEND_SUM = "TOTAL_SPEND_SUM";
    public static final String SPEND_HISTORY_DETAIL = "SPEND_HISTORY_DETAIL";
    public static final String SPEND_BUDGET = "SPEND_BUDGET";
    public static final String SPEND_CATEGORY_CODE = "SPEND_CATEGORY_CODE";
    public static final String SPEND_CATEGORY_DATE = "SPEND_CATEGORY_DATE";

    /**
     * 총 예산 설정값
     */
    public static final String SPEND_TOTAL_BUDGET = "SPEND_TOTAL_BUDGET";
    public static final String BUDGET_TARGET_MONTH = "TARGET_MONTH";

    /**
     * 예산 선택 데이터
     */
    public static final String SPEND_BUDGET_SELECT_DATA = "SPEND_BUDGET_SELECT_DATA";

    public static final String CHALLENGE_INFO = "CHALLENGE_INFO";
    public static final String MAIN_INVEST = "MAIN_INVEST";

    //preference
    public static final String PREFERENCE_SETTING_PUSH = "PREFERENCE_SETTING_PUSH";

    public static final String MEMBER_LEAVE = "MEMBER_LEAVE";
    public static final String TERMS_AGREE = "TERMS_AGREE";

    public static final String REMIT_REGISTER = "REMIT_REGISTER";
    public static final String REMIT_ACCOUNT_ADD = "REMIT_ACCOUNT_ADD";
    public static final String REMIT_SEND_SUM = "REMIT_SEND_SUM";
    public static final String REMIT_PASSWORD = "REMIT_PASSWORD";
    public static final String REMIT_MAIN_DATA = "REMIT_MAIN_DATA";
    public static final String REMIT_ACCOUNT_INFO = "REMIT_ACCOUNT_INFO";
    public static final String REMIT_CHARGE_SUM = "REMIT_CHARGE_SUM";
    public static final String REMIT_PAGE_STATUS = "REMIT_PAGE_STATUS";
    public static final String REMIT_MEMBER_EXIT = "REMIT_MEMBER_EXIT";
    public static final String REMIT_SELECT_ACCOUNT = "REMIT_SELECT_ACCOUNT";

    //----------------- common title, content
    public static final String TITLE = "TITLE";
    public static final String CONTENT = "CONTENT";


    public static final String OK = "ok";
    public static final String IS_UPDATE_MODE = "update_mode";

    public static final String RESET_PASSWORD = "RESET_PASSWORD";
    public static final String FIND_PIN = "FIND_PIN";

    /**
     * demo
     */
    public static final String DEMO_AGE = "DEMO_AGE";
    public static final String DEMO_GENDER = "DEMO_GENDER";

    /**
     * 스크래핑 못한 기간 최대 90일까지
     */
    public static final String SCRAPING_DAY = "SCRAPING_DAY";
}
