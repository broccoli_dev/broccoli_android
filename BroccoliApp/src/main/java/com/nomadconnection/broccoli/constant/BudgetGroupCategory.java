package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 2017. 3. 13..
 */

/**
 * 그룹 카테고리 서버에서 주는 CategoryId와 동일
 */
public enum BudgetGroupCategory {

    /**
     * 미설정 예상
     */
    UNCLASSIFIED(new BudgetCategory[]{}),

    /**
     * 식비 {외식, 카페/베이커리, 술/유흥, 마트}
     */
    FOOD(new BudgetCategory[] {
            BudgetCategory.EAT_OUT,
            BudgetCategory.CAFE_BAKERY,
            BudgetCategory.THE_DRINK_PLEASURE,
            BudgetCategory.MART}),

    /**
     * 뷰티/건강 {뷰티, 건강}
     */
    BEAUTY_HEALTH(new BudgetCategory[] {
            BudgetCategory.BEAUTY,
            BudgetCategory.HEALTH}),

    /**
     * 쇼핑 {쇼핑, 온라인쇼핑, 해외결제}
     */
    SHOPPING(new BudgetCategory[] {
            BudgetCategory.SHOPPING,
            BudgetCategory.ONLINE_SHOPPING,
            BudgetCategory.OVERSEAS_PAY}),

    /**
     * 문화/취미 {영화/문화, 레져/스포츠, 서점/문구}
     */
    CULTURE_HOBBY(new BudgetCategory[] {
            BudgetCategory.MOVIE_CULTURE,
            BudgetCategory.LEISURE_SPORT,
            BudgetCategory.BOOKSTORE}),

    /**
     * 교육/육아 {교육, 육아}
     */
    STUDY_BABY(new BudgetCategory[] {
            BudgetCategory.STUDY,
            BudgetCategory.BABY}),

    /**
     * 기타 {기타, 애완동물, 경조사/종교, 여행/숙박, 미분류}
     */
    ETC(new BudgetCategory[] {
            BudgetCategory.ETC,
            BudgetCategory.PET,
            BudgetCategory.FAMILY_EVENT_RELIGION,
            BudgetCategory.TRAVEL_STAY,
            BudgetCategory.UNCLASSIFIED}),

    /**
     * 고정비 {교통, 통신, 세금, 금융, 주유/자동차, 생활/주거}
     */
    FIXED(new BudgetCategory[] {
            BudgetCategory.TRAFFIC,
            BudgetCategory.COMMUNICATION,
            BudgetCategory.TAX,
            BudgetCategory.FINANCIAL,
            BudgetCategory.OILING_CAR,
            BudgetCategory.LIVING});

    private final BudgetCategory child[];

    BudgetGroupCategory(BudgetCategory[] code) {
        this.child = code;
    }

    public BudgetCategory[] child() {
        return this.child;
    }
}
