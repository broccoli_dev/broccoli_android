package com.nomadconnection.broccoli.constant;

/**
 * Created by YelloHyunminJang on 16. 6. 1..
 */
public class AnalyticsName {


    /**
     * Adbrix 신규유저
     */
    public static final String TermAgreeComplete = "TermAgreeComplete";
    public static final String NameCheckComplete = "NameCheckComplete";
    public static final String EmailRegComplete = "EmailRegComplete";
    public static final String BankRegComplete = "BankRegComplete";
    public static final String PinRegComplete = "PinRegComplete";
    public static final String TutorialComplete = "TutorialComplete";

    /*----- 초기진입 및 회원가입 -----*/
    public static final String SPLASH = "BroccoliSplash";
    public static final String LockScreenActivity = "LockScreen";
    public static final String IntroAsset = "IntroAsset";
    public static final String IntroSpend = "IntroSpend";
    public static final String IntroCard = "IntroCard";
    public static final String IntroEtc = "IntroEtc";
    public static final String IntroRefresh = "IntroRefresh";
    public static final String LoadingActivity = "BroccoliScraping";
    public static final String LoginScreen = "LoginScreen";
    public static final String FindEmail = "FindEmail";
    public static final String FindEmailResult = "FindEmailResult";
    public static final String FindPw = "FindPw";
    public static final String FindPwResult = "FindPwResult";
    public static final String FindPwResultConfirm = "FindPwResultConfirm";
    public static final String FindPin = "FindPin";
    public static final String FindPinResult = "FindPinResult";
    public static final String FindPinResultConfirm = "FindPinResultConfirm";
    public static final String UserTermAgree = "UserTermAgree";
    public static final String UserTermAgreeCancel = "UserTermAgreeCancel";
    public static final String UserNameCheck = "UserNameCheck";
    public static final String UserNameCheckCancel = "UserNameCheckCancel";
    public static final String UserEmailReg = "UserEmailReg";
    public static final String UserEmailRegCancel = "UserEmailRegCancel";
    public static final String UserPinReg = "UserPinReg";
    public static final String UserPinRegConfirm = "UserPinRegConfirm";
    public static final String UserPinRegCancel = "UserPinRegCancel";
    public static final String UserRegComplete = "UserRegComplete";
    public static final String UserBankSkip = "UserBankSkip";
    public static final String UserSelectBank = "UserSelectBank";
    public static final String UserSelectBankCancel = "UserSelectBankCancel";
    public static final String ExUserSplash = "ExUserSplash";
    public static final String ExUserInfo = "ExUserInfo";
    public static final String ExUserTermAgree = "ExUserTermAgree";
    public static final String ExUserEmailRegister = "ExUserEmailRegister";


    /**
     * 데모
     */
    public static final String DemoFilter = "DemoFilter";
    public static final String DemoSetting = "DemoSetting";
    public static final String DemoAsset = "DemoAsset";
    public static final String DemoSpend = "DemoFilter";
    public static final String DemoInvest = "DemoFilter";
    public static final String DemoEtc = "DemoFilter";

    /**
     * 안내 페이지
     */
    public static final String CertHowPwCopy = "CertHowPwCopy";

    /*----- 설정 - 금융기관 등록 ------*/
    public static final String SettingSelectBank = "SettingSelectBank";
    public static final String SettingRegBankList = "SettingRegBankList";
    public static final String CertLogin = "CertLogin";
    public static final String IdPwLogin = "IdPwLogin";
    public static final String CertManager = "CertManager";
    public static final String CertReplace = "CertReplace";
    public static final String BankReplace = "BankReplace";
    public static final String BankReplaceByCert = "BankReplaceByCert";
    public static final String BankReplaceById = "BankReplaceById";


    /*----- 설정 -----*/
    public static final String SettingList = "SettingList";
    public static final String SettingMyPic = "SettingMyPic";
    public static final String SettingMyPicDone = "SettingMyPicDone";
    public static final String SettingUserInfo = "SettingUserInfo";
    public static final String SettingUserinfoEdit = "SettingUserinfoEdit";
    public static final String SettingRegList = "SettingRegList";
    public static final String SettingRegEmail = "SettingRegEmail";
    public static final String SettingRegPwChange = "SettingRegPwChange";
    public static final String SettingRegPwChangeOn = "SettingRegPwChangeOn";
    public static final String SettingRegEmailSend = "SettingRegEmailSend";
    public static final String SettingRegPinChange = "SettingRegPinChange";
    public static final String SettingRegPinChangeConfirm = "SettingRegPinChangeConfirm";
    public static final String SettingRegCodeInput = "SettingRegCodeInput";
    public static final String SettingGuideDayPersonal = "SettingGuideDayPersonal";
    public static final String SettingGuideDayTerm = "SettingGuideDayTerm";
    public static final String SettingGuidePersonal = "SettingGuidePersonal";
    public static final String SettingGuideTerm = "SettingGuideTerm";
    public static final String SettingGuideDrop = "SettingGuideDrop";
    public static final String SettingGuideDropCheck = "SettingGuideDropCheck";
    public static final String SettingUserDataDel = "SettingUserDataDel";
    public static final String SettingVerClick = "SettingVerClick";
    public static final String ReceiptHowRegi = "ReceiptHowRegi";

    public static final String Level2ActivitySetting = "SettingList";
    public static final String Level2ActivitySettingInfo = "SettingInfoList";
    public static final String Level3ActivitySettingUserInfo = "SettingUserInfo";
    public static final String Level3ActivitySettingUserInfoEdit = "SettingUserInfoEdit";
    public static final String Level3ActivitySettingFinancial = "SettingBankRegister";
    public static final String Level3ActivitySettingNotice = "SettingNoti";
    public static final String Level4ActivitySettingNoticeDetail = "SettingNotiDetail";
    public static final String Level3ActivitySettingFaq = "SettingFAQList";
    public static final String SettingFAQDetail = "SettingFAQDetail";
    public static final String SettingQuestion = "SettingQuestion";
    public static final String Level3ActivitySettingGuide = "SettingGuideList";

    //가이드 화면
    public static final String GuideAuthRegister = "CertHowRegi";
    public static final String GuideAuthRegisterDetail = "CertBlogHowRegi";

    public static final String AlertDialogActivity = "AlertDialog";

    public static final String FragmentMain1 = "AssetMain";
    public static final String FragmentMain2 = "SpendMain";
    public static final String FragmentMain3 = "InvestMain";
    public static final String FragmentMain4 = "EtcMain";
    public static final String AssetGraphBtn = "AssetGraphBtn";
    public static final String AssetStockListNewsy = "AssetStockListNewsy";
    public static final String AssetStockDetailNewsy = "AssetStockDetailNewsy";
    public static final String AssetMainClickStock = "AssetMainClickStock";
    public static final String InvestMainClickStock = "InvestMainClickStock";
    public static final String Level2ActivityAssetDaS = "AssetSavingList";
    public static final String Level3ActivityAssetDasDetail = "AssetSavingDetail";
    public static final String AssetSavingEdit = "AssetSavingEdit";
    public static final String Level2ActivityAssetCredit = "AssetCreditList";
    public static final String Level3ActivityAssetCreditDetail = "AssetCreditDetail";
    public static final String Level2ActivityAssetStock = "AssetStockList";
    public static final String FragmentAssetStock0 = "AssetStockAdd";
    public static final String Level3ActivityAssetStockSearch = "AssetStockSearch";
    public static final String FragmentAssetStock1 = "AssetStockDailyInfo";
    public static final String Level3ActivityAssetStockDetail = "AssetStockDetail";
    public static final String Level3ActivityAssetStockDetailEdit = "AssetStockEdit";
    public static final String Level2ActivityAssetLoan = "AssetLoanList";
    public static final String Level3ActivityAssetLoanDetail = "AssetLoanDetail";
    public static final String AssetLoanEdit = "AssetLoanEdit";
    public static final String AssetCardLoanDetail = "AssetCardLoanDetail";
    public static final String Level2ActivityAssetOther = "AssetPropertyList";
    public static final String FragmentAssetOther0A = "AssetPropertyAdd(RE)";
    public static final String FragmentAssetOther0B = "AssetPropertyAdd(Car)";
    public static final String AssetPropertyAdd = "AssetPropertyAdd";
    public static final String Level3ActivityAssetOtherDetail = "AssetPropertyDetail";
    public static final String Level3ActivityAssetOtherDetailCAR = "AssetPropertyDetail(Car)";
    public static final String Level3ActivityAssetOtherDetailRE = "AssetPropertyDetail(RE)";
    public static final String Level3ActivityAssetOtherDetailEdit = "AssetPropertyEdit";
    public static final String Level3ActivityAssetOtherDetailEditCAR = "AssetPropertyEdit(Car)";
    public static final String Level3ActivityAssetOtherDetailEditRE = "AssetPropertyEdit(RE)";
    public static final String GuidePersonalAgree = "GuidePersonalAgree";
    public static final String GuideSendAgree = "GuideSendAgree";

    /**
     * 소비 - 메인
     */
    public static final String SpendGraphBtn = "SpendGraphBtn";
    public static final String SpendWayBtn = "SpendWayBtn";

    /**
     * 소비 - 이력
     */
    public static final String Level2ActivitySpendHistory = "SpendHistory";
    public static final String FragmentSpendHistoryAdd = "SpendHistoryAdd";
    public static final String Level3ActivitySpendHistoryDetail = "SpendHistoryDetail";
    public static final String Level3ActivitySpendHistoryDetailEdit = "SpendHistoryEdit";

    /**
     * 소비 - 예산
     */
    public static final String Level2ActivitySpendBudget = "SpendBudget";
    public static final String SpendBudgetBookmark = "SpendBudgetBookmark";
    public static final String SpendBudgetAddGoal = "SpendBudgetAddGoal";
    public static final String SpendBudgetAddCategory = "SpendBudgetAddCategory";
    public static final String SpendBudgetAddDetail = "SpendBudgetAddDetail";
    public static final String SpendBudgetEdit = "SpendBudgetEdit";
    public static final String SpendBudgetReset = "SpendBudgetReset";

    /**
     * 소비 - 카테고리
     */
    public static final String Level2ActivitySpendCategory = "SpendCategory";
    public static final String Level3ActivitySpendCategoryDetail = "SpendCategoryDetail";

    /**
     * 소비 - 카드
     */
    public static final String SpendCardInfoList = "SpendCardInfoList";
    public static final String SpendCardInfoDetail = "SpendCardInfoDetail";
    public static final String SpendCardRecmdList = "SpendCardRecmdList";
    public static final String SpendCardRecmdDetail = "SpendCardRecmdDetail";
    public static final String SpendCardInfoMemo = "SpendCardInfoMemo";
    public static final String SpendCardRecmdFilter = "SpendCardRecmdFilter";
    public static final String SpendCardRecmdSlide = "SpendCardRecmdSlide";

    /**
     * 투자 - 뉴지스탁
     */
    public static final String InvestNewsyMain = "InvestNewsyMain";
    public static final String InvestNewsyDetail = "InvestNewsyDetail";

    /**
     * 자산 - 카드론
     */
    public static final String AssetCreditListLoanBanner = "AssetCreditListLoanBanner";
    public static final String AssetLoanListLoanBanner = "AssetLoanListLoanBanner";

    /**
     * 카드추천 - 배너
     */
    public static final String AssetCreditListCardBanner = "AssetCreditListCardBanner";
    public static final String SpendCardBanner = "SpendCardBanner";
    public static final String SpendHistoryCardBanner = "SpendHistoryCardBanner";

    public static final String FragmentOtherChallAdd = "ChallengeAdd";
    public static final String FragmentOtherChallList = "ChallengeList";
    public static final String Level3ActivityOtherChallDetail = "ChallengeDetail";
    public static final String CalendarList = "CalendarList";
    public static final String CalendarAdd = "CalendarAdd";
    public static final String CalendarDetail = "CalendarDetail";
    public static final String CalendarEdit = "CalendarEdit";
}
