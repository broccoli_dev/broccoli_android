package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class Qna extends Result implements Serializable {
    private String qnaId;
    private String question;
    private String answer;

    public String getQnaId() {
        return qnaId;
    }

    public void setQnaId(String version) {
        this.qnaId = qnaId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String url) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String notice) {
        this.answer = answer;
    }
}
