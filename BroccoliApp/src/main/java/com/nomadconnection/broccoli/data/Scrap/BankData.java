package com.nomadconnection.broccoli.data.Scrap;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class BankData implements Serializable {

    private String id;
    private String bankName;
    private String bankCode;
    private String certSerial;
    private String passWord;
    private String type;
    private String validDate;
    private String uploaded;
    private String status;
    private String loginMethod;

    // ----- bank type ----- //
    public static final String TYPE_BANK = "bank";
    public static final String TYPE_CARD = "card";
    public static final String TYPE_CASH = "cash";

    // ----- login method ----- //
    public static final String LOGIN_CERT = "cert";
    public static final String LOGIN_ID   = "idpw";

    public static final String SCRAPING = "SCRAPING";
    public static final String FAIL = "fail";
    public static final String FAIL_AUTH = "authfail";
    public static final String FAIL_NETWORK = "networkfail";
    public static final String FAIL_SITERENEW = "siterenewalfail";
    public static final String FAIL_PASSWORD = "passwordfail";
    public static final String NONE = "none";

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * 인증서 접속 위한 로그인 정보
     * @return
     */
    public String getCertSerial() {
        return certSerial;
    }

    /**
     * 인증서 접속 위한 로그인 정보
     * @param certSerial
     */
    public void setCertSerial(String certSerial) {
        this.certSerial = certSerial;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getUploaded() {
        return uploaded;
    }

    public void setUploaded(String uploaded) {
        this.uploaded = uploaded;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLoginMethod() {
        return loginMethod;
    }

    public void setLoginMethod(String loginMethod) {
        this.loginMethod = loginMethod;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
