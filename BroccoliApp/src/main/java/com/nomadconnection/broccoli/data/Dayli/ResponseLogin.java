package com.nomadconnection.broccoli.data.Dayli;

/**
 * Created by YelloHyunminJang on 2016. 12. 27..
 */

public class ResponseLogin {

    private long userId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
