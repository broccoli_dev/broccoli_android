package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * 차량 세부차명 조회
 */
public class RequestAssetCarSubCode {

    @SerializedName("userId")
    public String mUserId;

    @SerializedName("makerCode")
    public String mMakerCode;

    @SerializedName("carCode")
    public String mCarCode;


    public RequestAssetCarSubCode(String _UserId, String _MakerCode, String _CarCode){
        this.mUserId = _UserId;
        this.mMakerCode = _MakerCode;
        this.mCarCode = _CarCode;
    }

}
