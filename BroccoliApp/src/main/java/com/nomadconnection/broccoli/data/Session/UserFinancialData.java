package com.nomadconnection.broccoli.data.Session;

/**
 * Created by YelloHyunminJang on 16. 3. 11..
 */
public class UserFinancialData {

    public static final String TYPE_BANK = "01";
    public static final String TYPE_CARD = "02";
    public static final String TYPE_TAX = "11";

    private String userId;
    private String companyType;
    private String companyCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyType() {
        return companyType;
    }

    /**
     * <p>01 - Bank</p>
     * <p>02 - Card</p>
     * 03 - Tax
     * @param companyType
     */
    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
}
