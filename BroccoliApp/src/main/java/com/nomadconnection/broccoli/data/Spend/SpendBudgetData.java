package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 3. 8..
 */

public class SpendBudgetData implements Serializable {

    public long budgetId;
    public String userId;
    public String baseMonth;
    public long totalSum;
    public long nextTotalSum;
    public long totalExpense;
    public long targetBudgetId;
    public List<SpendBudgetCategory> categoryList;
    public List<SpendBudgetDetail> detailList;

}
