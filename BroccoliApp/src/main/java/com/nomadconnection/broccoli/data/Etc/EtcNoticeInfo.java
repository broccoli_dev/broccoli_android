package com.nomadconnection.broccoli.data.Etc;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class EtcNoticeInfo extends Result implements Serializable{

    private String notificationId;
    private String categoryCode;
    private String message;
    private String date;
    private String readYn;



    /**
     * 알림 아이디
     * @return
     */
    public String getNotificationId() {
        return notificationId;
    }

    /**
     * 알림 아이디
     * @param notificationId
     */
    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    /**
     * 알림 분류 코드
     * @return
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * 알림 분류 코드
     * @param categoryCode
     */
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /**
     * 알림 내용
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     * 알림 내용
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 날짜
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     * 날짜
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 읽음 여부
     * @return
     */
    public String getReadYn() {
        return readYn;
    }

    /**
     * 읽음 여부
     * @param readYn
     */
    public void setReadYn(String readYn) {
        this.readYn = readYn;
    }
}
