package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 */
public class Result {

    public static final String CODE_OK = "100";
    public static final String CODE_OK2 = "200";
    public static final String DESC_OK = "ok";

    private String code;
    private String desc;
    private boolean ok;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isOk() {
        if (CODE_OK.equals(code) ||
                CODE_OK2.equalsIgnoreCase(code) ||
                DESC_OK.equalsIgnoreCase(desc)) {
            ok = true;
        } else {
            ok = false;
        }
        return ok;
    }

    public boolean checkUnable() {
        if (code == null) {
            return true;
        } else {
            return false;
        }
    }
}
