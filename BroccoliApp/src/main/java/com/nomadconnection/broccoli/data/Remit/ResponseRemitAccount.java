package com.nomadconnection.broccoli.data.Remit;

import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.BodySpendBudgetCategory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseRemitAccount extends RemitResult implements Serializable{
    private ArrayList<RemitAccount> accountInfo;
    private String walletMoney = "";
    private String accountCount = "";
    private String remit = "";
    private String remitHistory = "";

    /**
     * 송금 계좌 리스트
     * @return
     */
    public ArrayList<RemitAccount> getAccountInfo() {
        return accountInfo;
    }

    /**
     * 송금 계좌 리스트
     * @param accountInfo
     */
    public void setAccountInfo(ArrayList<RemitAccount> accountInfo) {
        this.accountInfo = accountInfo;
    }

    /**
     * 브로콜리 머니 잔액
     * @return
     */
    public String getWalletMoney() {
        return walletMoney;
    }

    /**
     * 브로콜리 머니 잔액
     * @param walletMoney
     */
    public void setWalletMoney(String walletMoney) {
        this.walletMoney = walletMoney;
    }

    /**
     * 총 계좌 갯수
     * @return
     */
    public String getAccountCount() {
        return accountCount;
    }

    /**
     * 총 계좌 갯수
     * @param accountCount
     */
    public void setAccountCount(String accountCount) {
        this.accountCount = accountCount;
    }


    /**
     * 송금 메뉴
     * @return
     */
    public String getRemit() {
        return remit;
    }

    /**
     * 송금 메뉴
     * @param remit
     */
    public void setRemit(String remit) {
        this.remit = remit;
    }

    /**
     * 내역 메뉴
     * @return
     */
    public String getRemitHistory() {
        return remitHistory;
    }

    /**
     * 내역 메뉴
     * @param remitHistory
     */
    public void setRemitHistory(String remitHistory) {
        this.remitHistory = remitHistory;
    }
}
