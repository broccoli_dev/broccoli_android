package com.nomadconnection.broccoli.data.Etc;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 챌린지 계좌 조회
 */
public class RequestEtcChallengeBank {

    private String userId;
    private Integer accountType;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }
}
