package com.nomadconnection.broccoli.data.Dayli;

/**
 * Created by YelloHyunminJang on 2016. 12. 28..
 */

public class DayliPwdReset {

    private String deviceId;
    private String newPassword;

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
