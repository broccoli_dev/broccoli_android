package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import static com.nomadconnection.broccoli.adapter.AdtExpListViewAssetLoan.ITEM_NORMAL;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class AssetDebtInfo implements Parcelable, Cloneable {

    public static final int LOAN = 1;
    public static final int LOAN_CARD = 2;

    @SerializedName("accountId")
    public String mAssetDebtId;                         // 대출 아이디

    @SerializedName("companyCode")
    public String mAssetDebtCode;                       // 대출사 코드

    @SerializedName("accountName")
    public String mAssetDebtName;                       // 대출 이름,종 거래일

    @SerializedName("sum")
    public String mAssetDebtSum;                        // 총액

    @SerializedName("accountNum")
    public String mAssetDebtAccountNumber;            // 계좌 번호

    @SerializedName("activeYn")
    public String mAssetDebtActiveYn;

    /**
     * 1 : 일반 대출
     * <P>2 : 카드론</P>
     */
    @SerializedName("loanType")
    public int mLoanType;

    public int mAdvertise = ITEM_NORMAL;

    public AssetDebtInfo() {

    }

    public boolean isChecked() {
        boolean checked = false;

        if (mAssetDebtActiveYn != null && mAssetDebtActiveYn.equalsIgnoreCase("Y")) {
            checked = true;
        }

        return checked;
    }

    public void toggleActive(boolean check) {
        if (mAssetDebtActiveYn != null) {
            if (check) {
                mAssetDebtActiveYn = "Y";
            } else {
                mAssetDebtActiveYn = "N";
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetDebtId);
        dest.writeString(this.mAssetDebtCode);
        dest.writeString(this.mAssetDebtName);
        dest.writeString(this.mAssetDebtSum);
        dest.writeString(this.mAssetDebtAccountNumber);
        dest.writeString(this.mAssetDebtActiveYn);
        dest.writeInt(this.mLoanType);
    }

    public AssetDebtInfo(Parcel in) {
        this.mAssetDebtId = in.readString();
        this.mAssetDebtCode = in.readString();
        this.mAssetDebtName = in.readString();
        this.mAssetDebtSum = in.readString();
        this.mAssetDebtAccountNumber = in.readString();
        this.mAssetDebtActiveYn = in.readString();
        this.mLoanType = in.readInt();
    }

    public static final Creator<AssetDebtInfo> CREATOR = new Creator<AssetDebtInfo>() {
        public AssetDebtInfo createFromParcel(Parcel in) {
            return new AssetDebtInfo(in);
        }
        public AssetDebtInfo[] newArray (int size) {
            return new AssetDebtInfo[size];
        }
    };

    @Override
    public AssetDebtInfo clone() throws CloneNotSupportedException {
        return (AssetDebtInfo) super.clone();
    }
}
