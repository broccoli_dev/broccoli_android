package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 2017. 2. 13..
 */

public class MainPopupData {

    public int popupId;
    public String title;
    public String contents;
    public boolean reOpenYn = false;
    public boolean neverShow = false;

}
