package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardDetailBenefitAffiliate implements Serializable {

    /**
     * 1: 할인, 2: 포인트, 3: 마일리지
     */
    public int benefitType;
    /**
     * 1 : 국내가맹점, 2 : 해외가맹점
     */
    public int countriesType;
    public String cardCode;
    /**
     * 전월 실적
     */
    public SpendCardMinMax requirement;
    /**
     * 이용 금액 조건
     */
    public SpendCardMinMax spend;
    public SpendCardDetailBenefitBody benefit;
    public Integer limitMonth;
    public Integer limitYear;

}
