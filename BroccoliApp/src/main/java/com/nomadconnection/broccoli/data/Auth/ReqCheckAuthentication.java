package com.nomadconnection.broccoli.data.Auth;

/**
 * Created by YelloHyunminJang on 16. 7. 1..
 */
public class ReqCheckAuthentication {

    private String instanceId;
    private String authenticationId;
    private String deviceId;

    public String getInstanceID() {
        return instanceId;
    }

    public void setInstanceID(String instanceID) {
        this.instanceId = instanceID;
    }

    public String getAuthenticationID() {
        return authenticationId;
    }

    public void setAuthenticationID(String authenticationID) {
        this.authenticationId = authenticationID;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
