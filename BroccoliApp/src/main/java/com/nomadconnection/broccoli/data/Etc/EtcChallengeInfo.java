package com.nomadconnection.broccoli.data.Etc;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class EtcChallengeInfo implements Serializable{
    private String challengeId;
    private String challengeType;
    private String title;
    private String startDate;
    private String endDate;
    private String amt;
    private String goalAmt;
    private String monthlyAmt;
    private String accountId;
    private String imagePath;



    /**
     * 챌린지 아이디
     * @return
     */
    public String getChallengeId() {
        return challengeId;
    }

    /**
     * 챌린지 아이디
     * @param challengeId
     */
    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    /**
     * 챌린지 종류
     * @return
     */
    public String getChallengeType() {
        return challengeType;
    }

    /**
     * 챌린지 종류
     * @param challengeType
     */
    public void setChallengeType(String challengeType) {
        this.challengeType = challengeType;
    }

    /**
     * 제목
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * 제목
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 시작일
     * @return
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * 시작일
     * @param startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * 종료일
     * @return
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * 종료일
     * @param endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * 현재 금액
     * @return
     */
    public String getAmt() {
        return amt;
    }

    /**
     * 현재 금액
     * @param amt
     */
    public void setAmt(String amt) {
        this.amt = amt;
    }

    /**
     * 목표액
     * @return
     */
    public String getGoalAmt() {
        return goalAmt;
    }

    /**
     * 목표액
     * @param goalAmt
     */
    public void setGoalAmt(String goalAmt) {
        this.goalAmt = goalAmt;
    }

    /**
     * 월 납입 금액
     * @return
     */
    public String getMonthlyAmt() {
        return monthlyAmt;
    }

    /**
     *  월 납입 금액
     * @param monthlyAmt
     */
    public void setMonthlyAmt(String monthlyAmt) {
        this.monthlyAmt = monthlyAmt;
    }

    /**
     * 계좌 아이디
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     *  계좌 아이디
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 이미지 경로
     * @return
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     *  이미지 경로
     * @param imagePath
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
