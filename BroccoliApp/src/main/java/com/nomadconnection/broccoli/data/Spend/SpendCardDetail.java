package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardDetail implements Serializable {
    public ArrayList<SpendCardDetailBenefit> benefitList;
    public ArrayList<SpendCardDetailAnnualFee> arr_annualFeeList;
    public ArrayList<SpendCardDetailBenefitAffiliate> benefitAffiliateList;
    public HashMap<String, Integer> benefit;
}
