package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataPW implements Serializable{
    private String userId = "";
    private String accountPW = "";

    /**
     * 유저아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 유저아이디
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 신규암호
     * @return
     */
    public String getAccountPW() {
        return accountPW;
    }

    /**
     * 신규암호
     * @param accountPW
     */
    public void setAccountPW(String accountPW) {
        this.accountPW = accountPW;
    }
}
