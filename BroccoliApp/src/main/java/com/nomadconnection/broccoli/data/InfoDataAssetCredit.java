package com.nomadconnection.broccoli.data;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoDataAssetCredit implements Parcelable {

	public String mTxt1 = null;
	public String mTxt2 = null;
	public String mTxt3 = null;
	public String mTxt4 = null;
	public String mTxt5 = null;
	public String mTxt6 = null;
	public String mTxt7 = null;
	public String mTxt8 = null;


	public InfoDataAssetCredit() {
	}

	public InfoDataAssetCredit(String _Txt1, String _Txt2, String _Txt3, String _Txt4, String _Txt5, String _Txt6, String _Txt7, String _Txt8) {
		this.mTxt1 = _Txt1;
		this.mTxt2 = _Txt2;
		this.mTxt3 = _Txt3;
		this.mTxt4 = _Txt4;
		this.mTxt5 = _Txt5;
		this.mTxt6 = _Txt6;
		this.mTxt7 = _Txt7;
		this.mTxt8 = _Txt8;
	}



	public InfoDataAssetCredit(Parcel in) {
		this.mTxt1 = in.readString();
		this.mTxt2 = in.readString();
		this.mTxt3 = in.readString();
		this.mTxt4 = in.readString();
		this.mTxt5 = in.readString();
		this.mTxt6 = in.readString();
		this.mTxt7 = in.readString();
		this.mTxt8 = in.readString();
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.mTxt1);
		dest.writeString(this.mTxt2);
		dest.writeString(this.mTxt3);
		dest.writeString(this.mTxt4);
		dest.writeString(this.mTxt5);
		dest.writeString(this.mTxt6);
		dest.writeString(this.mTxt7);
		dest.writeString(this.mTxt8);
	}
	
	public static final Creator<InfoDataAssetCredit> CREATOR = new Creator<InfoDataAssetCredit>() {
		public InfoDataAssetCredit createFromParcel(Parcel in) {
			return new InfoDataAssetCredit(in);
		}
		public InfoDataAssetCredit[] newArray (int size) {
			return new InfoDataAssetCredit[size];
		}
	};

	
}
