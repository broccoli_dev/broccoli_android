package com.nomadconnection.broccoli.data.Session;

/**
 * Created by YelloHyunminJang on 16. 7. 5..
 */
public class ResponseRealName {

    private String tempId;

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }
}
