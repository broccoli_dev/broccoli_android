package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.constant.MinMax;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardMinMax implements Serializable {

    /**
     * 최소 금액
     */
    public Integer min;
    /**
     * 최대 금액
     */
    public Integer max;
    /**
     * 최소 단위
     */
    public MinMax minSignType;
    /**
     * 최대 단위
     */
    public MinMax maxSignType;

}
