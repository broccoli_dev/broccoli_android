package com.nomadconnection.broccoli.data.Asset;

import java.util.List;

/**
 * Created by YelloHyunminJang on 2016. 10. 18..
 */

public class RequestAssetBankActive {

    private String userId;
    private List<BodyBankActive> active;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setActive(List<BodyBankActive> active) {
        this.active = active;
    }
}
