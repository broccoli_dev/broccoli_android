package com.nomadconnection.broccoli.data.Investment;

import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 2. 5..
 */
public class InventMainStock {

    private String date;
    private List<InventMainStockDetail> list;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<InventMainStockDetail> getList() {
        return list;
    }

    public void setList(List<InventMainStockDetail> list) {
        this.list = list;
    }
}
