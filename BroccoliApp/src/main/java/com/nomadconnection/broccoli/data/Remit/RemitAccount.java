package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RemitAccount implements Serializable {
    private String accountId;
    private String companyCode;
    private String accountName;
    private String accountNumber;
    private String balance;
    private String standardDate;

    /**
     * 계좌 아이디
     *
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌 아이디
     *
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 은행사 코드
     *
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 은행사 코드
     *
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 계좌번호
     *
     * @return
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * 계좌번호
     *
     * @param accountName
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * 계좌 번호
     *
     * @return
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * 계좌 번호
     *
     * @param accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * 잔액
     *
     * @return
     */
    public String getBalance() {
        return balance;
    }

    /**
     * 잔액
     *
     * @param balance
     */
    public void setBalance(String balance) {
        this.balance = balance;
    }

    /**
     * 날짜
     *
     * @return
     */
    public String getStandardDate() {
        return standardDate;
    }

    /**
     * 날짜
     *
     * @param standardDate
     */
    public void setStandardDate(String standardDate) {
        this.standardDate = standardDate;
    }
}
