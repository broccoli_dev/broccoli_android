package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseRemitPhoneCurrent extends RemitResult implements Serializable{
    private String userName;
    private ArrayList<RemitContent> sendTelList;

    /**
     * 본인이름
     * @return
     */
    public String getUserName () {
        return userName;
    }

    /**
     * 본인이름
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 솧금전화 리스트
     * @return
     */
    public ArrayList<RemitContent> getSendTelList () {
        return sendTelList;
    }

    /**
     * 송금전화 리스트
     * @param sendTelList
     */
    public void setSendTelList(ArrayList<RemitContent> sendTelList) {
        this.sendTelList = sendTelList;
    }
}
