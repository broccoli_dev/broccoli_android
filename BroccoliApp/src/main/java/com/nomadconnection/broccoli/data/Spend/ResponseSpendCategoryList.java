package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;

import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseSpendCategoryList extends Result {
    private List<BodySpendCategoryList> list;

    /**
     * 카테고리 리스트
     * @return
     */
    public List<BodySpendCategoryList> getList() {
        return list;
    }

    /**
     * 카테고리 리스트
     * @param list
     */
    public void setList(List<BodySpendCategoryList> list) {
        this.list = list;
    }
}
