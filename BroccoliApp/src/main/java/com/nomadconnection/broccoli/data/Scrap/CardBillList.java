package com.nomadconnection.broccoli.data.Scrap;

import com.google.gson.JsonObject;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 * 카드 청구 내역
 */
public class CardBillList {

    private String USERID;
    private String CMPNYCODE;
    private int TYPE;
    private String UPLOADID;
    private String PAYDATE;
    private String PAYAMT;
    private JsonObject result;

    /**
     * 데이터 종류
     * <p>데이터 종류 (1:은행계좌, 2:은행거래, 3:카드승인, 4:카드청구, 5:현금영수증)</p>
     * @return
     */
    public int getTYPE() {
        return TYPE;
    }

    /**
     * 데이터 종류
     * <p>데이터 종류 (1:은행계좌, 2:은행거래, 3:카드승인, 4:카드청구, 5:현금영수증)</p>
     * @param TYPE
     */
    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public String getCMPNYCODE() {
        return CMPNYCODE;
    }

    public void setCMPNYCODE(String CMPNYCODE) {
        this.CMPNYCODE = CMPNYCODE;
    }


    public String getPAYDATE() {
        return PAYDATE;
    }

    public void setPAYDATE(String PAYDATE) {
        this.PAYDATE = PAYDATE;
    }

    public String getPAYAMT() {
        return PAYAMT;
    }

    public void setPAYAMT(String PAYAMT) {
        this.PAYAMT = PAYAMT;
    }

    public String getUPLOADID() {
        return UPLOADID;
    }

    public void setUPLOADID(String UPLOADID) {
        this.UPLOADID = UPLOADID;
    }

    public JsonObject getResult() {
        return result;
    }

    public void setResult(JsonObject result) {
        this.result = result;
    }
}
