package com.nomadconnection.broccoli.data.Log;

/**
 * Created by YelloHyunminJang on 2016. 11. 2..
 */

public class LogMsgData {

    public static final int LOG_AGREE_TERM = 1000;
    public static final int LOG_REAL_NAME = 1001;
    public static final int LOG_FINANCIAL_REG = 1002;
    public static final int LOG_CERT_LOGIN = 1004;
    public static final int LOG_PWD_REG = 1005;
    public static final int LOG_TUTORIAL_SCRAPING = 1006;
    public static final int LOG_HOME_MAIN = 1006;

    public static final int LOG_UPGRADE_DETAIL = 1100;
    public static final int LOG_UPGRADE_AGREE = 1101;
    public static final int LOG_UPGRADE_EMAIL = 1102;

    private String deviceId;
    private int step;

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setStep(int step) {
        this.step = step;
    }
}
