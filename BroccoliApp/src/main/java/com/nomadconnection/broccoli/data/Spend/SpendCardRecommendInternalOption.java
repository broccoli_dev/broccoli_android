package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.constant.SpendCardOption;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 21..
 */

public class SpendCardRecommendInternalOption implements Serializable{

    public long cardId;
    public SpendCardOption.CardType cardType;
    public SpendCardOption.BenefitType optionDetailType;
    public SpendCardOption.BenefitType benefitType;
    public SpendCardOption.AnnualFeeRange annulFee;

}
