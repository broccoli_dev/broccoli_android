package com.nomadconnection.broccoli.data;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataTermsAgree {
    private String userId;
    private ArrayList<Integer> agrees;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<Integer> getAgrees() {
        return agrees;
    }

    public void setAgrees(ArrayList<Integer> agrees) {
        this.agrees = agrees;
    }
}
