package com.nomadconnection.broccoli.data;

import com.nomadconnection.broccoli.data.Etc.BodyOrgInfo;
import com.nomadconnection.broccoli.data.Spend.ResponseCardList;

import java.util.ArrayList;
import java.util.List;


public class InfoDataDialog {
//	public class InfoDataDialog implements Parcelable {
	
	public int mLayout = 0;
	public String mOneBtnTitle = "Title";
	public String mOneBtnButtonText = "버튼";
	public String mTwoBtnButtonTextL = "취소";
	public String mTwoBtnButtonTextR = "확인";
	
	
	public String mCase1Text = "";	/* Case 1 */
	public String mCase2Text1 = "";	/* Case 2 text - 1 */
	public String mCase2Text2 = "";	/* Case 2 text - 2 */
	public String mCase3Text = "";	/* Case 3 */
	public String mCase5Text = "";	/* Case 5 */
	public InfoDataAppVersion mCase5Info;
	public ArrayList<BodyOrgInfo> mArrBodyOrgInfo;
	public List<ResponseCardList> mArrCardInfo;
	public MainPopupData mNotice;
	
	
	
	public InfoDataDialog() {
	}
	
	/* Case 1 */
	public InfoDataDialog(int _layout, String _twoBtnButtonTxtL, String _twoBtnButtonTxtR, String _case1Text) {
		this.mLayout = _layout;
		this.mTwoBtnButtonTextL = _twoBtnButtonTxtL;
		this.mTwoBtnButtonTextR = _twoBtnButtonTxtR;
		this.mCase1Text = _case1Text;

		// Case 3 공통 사용
		this.mOneBtnTitle = _twoBtnButtonTxtL;
		this.mOneBtnButtonText = _twoBtnButtonTxtR;
		this.mCase3Text = _case1Text;
	}
	
	/* Case 2 */
	public InfoDataDialog(int _layout, String _twoBtnButtonTxtL, String _twoBtnButtonTxtR, String _case2Text1, String _case2Text2) {
		this.mLayout = _layout;
		this.mTwoBtnButtonTextL = _twoBtnButtonTxtL;
		this.mTwoBtnButtonTextR = _twoBtnButtonTxtR;
		this.mCase2Text1 = _case2Text1;
		this.mCase2Text2 = _case2Text2;
	}
	
	/* Case 3 */
	public InfoDataDialog(int _layout, String _oneBtnTitle, String _oneBtnButtonText) {
		this.mLayout = _layout;
		this.mOneBtnTitle = _oneBtnTitle;
		this.mOneBtnButtonText = _oneBtnButtonText;
	}

	/* Case DIALOG_BASE_GRID_OR_LIST_L */
	public InfoDataDialog(int _layout, String _oneBtnTitle, String _oneBtnButtonText, ArrayList<BodyOrgInfo> _Arr) {
		this.mLayout = _layout;
		this.mOneBtnTitle = _oneBtnTitle;
		this.mOneBtnButtonText = _oneBtnButtonText;
		this.mArrBodyOrgInfo = _Arr;
	}

	/* Case CARD_INFO_LIST */
	public InfoDataDialog(int _layout, String _oneBtnTitle, String _oneBtnButtonText, List<ResponseCardList> _ArrCard) {
		this.mLayout = _layout;
		this.mOneBtnTitle = _oneBtnTitle;
		this.mOneBtnButtonText = _oneBtnButtonText;
		this.mArrCardInfo = _ArrCard;
	}

	/* Case 5 */
	public InfoDataDialog(int _layout, String _twoBtnButtonTxtL, String _twoBtnButtonTxtR, String _case1Text, InfoDataAppVersion _TempAppVersion) {
		this.mLayout = _layout;
		this.mTwoBtnButtonTextL = _twoBtnButtonTxtL;
		this.mTwoBtnButtonTextR = _twoBtnButtonTxtR;
		this.mCase5Text = _case1Text;
		this.mCase5Info = _TempAppVersion;
	}


	/* Case EmergencyNotice */
	public InfoDataDialog(int _layout, String _twoBtnButtonTxtL, String _twoBtnButtonTxtR, MainPopupData _Notice, boolean isNotice) {
		this.mLayout = _layout;
		this.mTwoBtnButtonTextL = _twoBtnButtonTxtL;
		this.mTwoBtnButtonTextR = _twoBtnButtonTxtR;
		this.mNotice = _Notice;
	}
	
	
	
//	public InfoDataDialog(Parcel in) {
//		this.mLayout = in.readInt();
//		this.mOneBtnText = in.readString();
//		this.mTwoBtnTextL = in.readString();
//		this.mTwoBtnTextR = in.readString();
//		this.mCase1Text = in.readString();
//		this.mCase2Text1 = in.readString();
//		this.mCase2Text2 = in.readString();
//	}
//	
//	@Override
//	public int describeContents() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//
//	@Override
//	public void writeToParcel(Parcel dest, int flags) {
//		// TODO Auto-generated method stub
//		dest.writeInt(this.mLayout);
//		dest.writeString(this.mOneBtnText);
//		dest.writeString(this.mTwoBtnTextL);
//		dest.writeString(this.mTwoBtnTextR);
//		dest.writeString(this.mCase1Text);
//		dest.writeString(this.mCase2Text1);
//		dest.writeString(this.mCase2Text2);
//	}
//	
//	public static final Parcelable.Creator<InfoDataDialog> CREATOR = new Parcelable.Creator<InfoDataDialog>() {
//		public InfoDataDialog createFromParcel(Parcel in) {
//			return new InfoDataDialog(in);
//		}
//		public InfoDataDialog[] newArray (int size) {
//			return new InfoDataDialog[size];
//		}
//	};

	
}
