package com.nomadconnection.broccoli.data.Session;

import com.nomadconnection.broccoli.data.Scrap.BankData;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 7. 5..
 */
public class MembershipData implements Serializable {

    private String tempId;
    private String name;
    private String birth;
    private String gender;
    private String email;
    private String pwd;
    private String deviceKey;
    private String phoneNum;
    private ArrayList<BankData> list;

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public ArrayList<BankData> getBankList() {
        return this.list;
    }

    public void setBankList(ArrayList<BankData> list) {
        this.list = list;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getEmail() {
        return email;
    }

    public String getPwd() {
        return pwd;
    }
}
