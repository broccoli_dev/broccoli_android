package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 6..
 */

public class SpendBudgetSettingData implements Serializable {

    public int index = 0;
    public String title;
    public String subTitle;
    public String expense;
    public String budget;
    public boolean isChecked = false;

}
