package com.nomadconnection.broccoli.data.Etc;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class RequestEtcNoticeRead extends Result implements Serializable{

    private String userId;
    private List<String > list;



    /**
     * 사용자 아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 사용자 아이디
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 알림 리스트
     * @return
     */
    public List<String> getList() {
        return list;
    }

    /**
     * 알림 리스트
     * @param list
     */
    public void setList(List<String> list) {
        this.list = list;
    }
}
