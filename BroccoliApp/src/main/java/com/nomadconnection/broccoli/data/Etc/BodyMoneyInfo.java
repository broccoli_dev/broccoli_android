package com.nomadconnection.broccoli.data.Etc;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class BodyMoneyInfo implements Parcelable {

    @SerializedName("calendarId")
    public String mBodyMoneyCalendarId;

    @SerializedName("title")
    public String mBodyMoneyTitle;

//    @SerializedName("transactionId")
//    public String mBodyMoneyTransactionId;

//    @SerializedName("requestCompany")
//    public String mBodyMoneyRequestCompany;

    @SerializedName("opponent")
    public String mBodyMoneyOpponent;

    @SerializedName("amt")
    public String mBodyMoneyAmt;

    @SerializedName("payType")
    public String mBodyMoneyPayType;

//    @SerializedName("expectYn")
//    public String mBodyMoneyExpectYn;

    @SerializedName("expectOrPaid")
    public String mBodyMoneyExpectOrPaid;

    @SerializedName("payDate")
    public String mBodyMoneyPayDate;

    @SerializedName("repeatType")
    public String mBodyMoneyRepeatType;

    @SerializedName("matchedTag")
    public String mBodyMoneyMatchedTag;

//    @SerializedName("payYn")
//    public String mBodyMoneyPayYn;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mBodyMoneyCalendarId);
        dest.writeString(this.mBodyMoneyTitle);
//        dest.writeString(this.mBodyMoneyTransactionId);
//        dest.writeString(this.mBodyMoneyRequestCompany);
        dest.writeString(this.mBodyMoneyOpponent);
        dest.writeString(this.mBodyMoneyAmt);
        dest.writeString(this.mBodyMoneyPayType);
//        dest.writeString(this.mBodyMoneyExpectYn);
        dest.writeString(this.mBodyMoneyExpectOrPaid);
        dest.writeString(this.mBodyMoneyPayDate);
        dest.writeString(this.mBodyMoneyRepeatType);
        dest.writeString(this.mBodyMoneyMatchedTag);
//        dest.writeString(this.mBodyMoneyPayYn);
    }

    public BodyMoneyInfo(Parcel in) {
        this.mBodyMoneyCalendarId = in.readString();
        this.mBodyMoneyTitle = in.readString();
//        this.mBodyMoneyTransactionId = in.readString();
//        this.mBodyMoneyRequestCompany = in.readString();
        this.mBodyMoneyOpponent = in.readString();
        this.mBodyMoneyAmt = in.readString();
        this.mBodyMoneyPayType = in.readString();
//        this.mBodyMoneyExpectYn = in.readString();
        this.mBodyMoneyExpectOrPaid = in.readString();
        this.mBodyMoneyPayDate = in.readString();
        this.mBodyMoneyRepeatType = in.readString();
        this.mBodyMoneyMatchedTag = in.readString();
//        this.mBodyMoneyPayYn = in.readString();
    }

    public static final Creator<BodyMoneyInfo> CREATOR = new Creator<BodyMoneyInfo>() {
        public BodyMoneyInfo createFromParcel(Parcel in) {
            return new BodyMoneyInfo(in);
        }
        public BodyMoneyInfo[] newArray (int size) {
            return new BodyMoneyInfo[size];
        }
    };


}
