package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseSpendCategoryDetail extends Result implements Serializable{
    private String budget;
    private String expense;
    private List<SpendMonthlyInfo> list;

    /**
     * 예산 총액
     * @return
     */
    public String getBudget() {
        return budget;
    }

    /**
     * 예산 총액
     * @param budget
     */
    public void setBudget(String budget) {
        this.budget = budget;
    }

    /**
     * 카테고리 리스트
     * @return
     */
    public List<SpendMonthlyInfo> getList() {
        return list;
    }

    /**
     * 카테고리 리스트
     * @param list
     */
    public void setList(List<SpendMonthlyInfo> list) {
        this.list = list;
    }

    /**
     * 소비 총액
     * @return
     */
    public String getExpense() {
        return expense;
    }

    /**
     * 소비 총액
     * @param expense
     */
    public void setExpense(String expense) {
        this.expense = expense;
    }
}
