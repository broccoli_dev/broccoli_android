package com.nomadconnection.broccoli.data.Scrap;

import com.nomadconnection.broccoli.data.RequestDataByUserId;

/**
 * Created by YelloHyunminJang on 16. 4. 20..
 */
public class UploadLogMsg extends RequestDataByUserId {

    private String companyCode;
    private String errorCode;
    private String errorMsg;
    private String etc;

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getEtc() {
        return etc;
    }

    public void setEtc(String etc) {
        this.etc = etc;
    }
}
