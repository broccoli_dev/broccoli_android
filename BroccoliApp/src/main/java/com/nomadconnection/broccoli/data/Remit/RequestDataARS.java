package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataARS implements Serializable{
    private String userId = "";
    private String companyCode = "";
    private String accountNumber = "";
    private String hostName = "";
    private String telNo = "";
    private String instanceId = "";
    private String accountId = "";

    /**
     * 유저아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 유저아이디
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 은행번호
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 은행번호
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 계좌번호
     * @return
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * 계좌번호
     * @param accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * 이름
     * @return
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * 이름
     * @param hostName
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * 전화번호
     * @return
     */
    public String getTelNo() {
        return telNo;
    }

    /**
     * 전화번호
     * @param telNo
     */
    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    /**
     * 인스턴스 아이디
     * @return
     */
    public String getInstanceId() {
        return instanceId;
    }

    /**
     * 인스턴스 아이디
     * @param instanceId
     */
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    /**
     * 계좌아이디 (브로콜리계좌아이디)
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌아이디 (브로콜리계좌아이디)
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
