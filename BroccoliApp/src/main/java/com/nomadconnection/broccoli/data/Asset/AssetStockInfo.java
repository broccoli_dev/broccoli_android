package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;
import com.nomadconnection.broccoli.data.Result;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class AssetStockInfo extends Result {

    @SerializedName("date")
    public String                   mAssetStockDate;                    // 날자

    @SerializedName("list")
    public ArrayList<BodyAssetStockInfo> mAssetStockList;                    // 주식 목록
//    public List<BodyAssetStockInfo>    mAssetStockList;                    // 주식 목록

//    @SerializedName("date")
//    private String mDate;
//    @SerializedName("list")
//    private List<BodyAssetStockInfo> mList;
//
//    /**
//     * 기준일
//     * @return
//     */
//    public String getDate() {
//        return mDate;
//    }
//
//    /**
//     * 기준일
//     * @param mCode
//     */
//    public void setDate(String mCode) {
//        this.mDate = mCode;
//    }
//
//    /**
//     * 주식 내역 리스트
//     * @return
//     */
//    public List<BodyAssetStockInfo> getList() {
//        return mList;
//    }
//
//    /**
//     * 주식 내역 리스트
//     * @param list
//     */
//    public void setList(List<BodyAssetStockInfo> list) {
//        this.mList = list;
//    }
}
