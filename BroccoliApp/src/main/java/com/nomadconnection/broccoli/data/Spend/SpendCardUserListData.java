package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 21..
 */

public class SpendCardUserListData implements Serializable {

    public long cardId;
    public String userName;
    public String cmpnyCode;
    public String cardCode;
    public String cardNum;
    public String cardName;
    public String validPeriod;
    public String memo;
    public String useYn;
    public String trffYn;
    public String sleepYn;
    public Integer bookmarkYn;
    public long expenseAmt;
    public long maxLimitAmt;
    public SpendCardClassifiedData card;

}
