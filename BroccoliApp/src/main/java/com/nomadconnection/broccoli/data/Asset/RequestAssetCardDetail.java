package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 카드 상세 내역 조회 요청 바디
 */
public class RequestAssetCardDetail {

    @SerializedName("companyCode")
    public String mAssetCardDetailCompanyCode;

    @SerializedName("date")
    public String mAssetCardDetailDate;

    @SerializedName("billType")
    public String mAssetCardDetailBillType;

    public RequestAssetCardDetail(String _AssetCardDetailCompanyCode, String _AssetCardDetailDate, String _AssetCardDetailBillType){
        this.mAssetCardDetailCompanyCode = _AssetCardDetailCompanyCode;
        this.mAssetCardDetailDate = _AssetCardDetailDate;
        this.mAssetCardDetailBillType = _AssetCardDetailBillType;
    }
}
