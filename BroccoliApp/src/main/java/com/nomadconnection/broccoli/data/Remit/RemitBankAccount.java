package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RemitBankAccount implements Serializable{
    private String hostName = "";
    private String companyCode= "";
    private String accountNumber = "";

    /**
     * 이름
     * @return
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * 이름
     * @param hostName
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * 은행사 코드
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 은행사 코드
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 계좌번호
     * @return
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * 계좌번호
     * @param accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
