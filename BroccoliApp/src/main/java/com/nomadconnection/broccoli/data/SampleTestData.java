package com.nomadconnection.broccoli.data;

import com.nomadconnection.broccoli.data.Etc.EtcMainData;

import java.util.ArrayList;

public class SampleTestData {
	
	/* App Version */
	public static ArrayList<InfoDataAppVersion> GetSampleDataAppVersion() {
		ArrayList<InfoDataAppVersion> mSample = new ArrayList<InfoDataAppVersion>();
		mSample.add(new InfoDataAppVersion("1.0.2", "업데이트 내역/기타사용자 VOC 수정사항 반영/기능 업데이트1/기능 업데이트2/기능 업데이트3/기능 업데이트4", "http://www.naver.com"));
		return mSample;
	}
	
	/* Alarm */
	public static ArrayList<InfoDataAlarm> GetSampleDataAlarm() {
		ArrayList<InfoDataAlarm> mSample = new ArrayList<InfoDataAlarm>();
		mSample.add(new InfoDataAlarm("1", "메시지 1", "팝업 2버튼 - 1줄"));
		mSample.add(new InfoDataAlarm("2", "메시지 2", "팝업 2버튼 - 2줄"));
		mSample.add(new InfoDataAlarm("3", "메시지 3", "팝업 1버튼 - 알림선택"));
		mSample.add(new InfoDataAlarm("4", "메시지 4", "팝업 1버튼 - 카테고리"));
		mSample.add(new InfoDataAlarm("5", "메시지 5", "팝업 1버튼 - 리스트목록"));
		mSample.add(new InfoDataAlarm("6", "메시지 6", "기타"));
		mSample.add(new InfoDataAlarm("7", "메시지 7", "기타"));
		mSample.add(new InfoDataAlarm("8", "메시지 8", "기타"));
		mSample.add(new InfoDataAlarm("9", "메시지 9", "기타"));
		return mSample;
	}
	
	/* Main 2 */
	public static ArrayList<InfoDataMain2> GetSampleDataMain2() {
		ArrayList<InfoDataMain2> mSample = new ArrayList<InfoDataMain2>();
		mSample.add(new InfoDataMain2("소비이력", "y", 
										"신용카드", "FC몰 오리브영", "9,000", "어제 오후 10:30",
										"게좌이체", "김경미", "350,000", "어제 오후 06:00",
										"체크카드", "택시_KSCC_수도권", "25,100", "어제 오후 04:25",
										"", "", "", "", 
										"", "", "", "", "", "",
										"", "", "", "", "", ""));
		mSample.add(new InfoDataMain2("예산", "y",
										"", "", "", "", 
										"", "", "", "", 
										"", "", "", "",
										"10월 총 소비", "1,950,000", "10월 총 예산", "3,000,000",
										"", "", "", "", "", "",
										"", "", "", "", "", ""));
		mSample.add(new InfoDataMain2("소비분류", "y", 
										"", "", "", "", 
										"", "", "", "", 
										"", "", "", "",
										"", "", "", "",
										"식비", "1,530,000", "생활용품", "230,000", "쇼핑", "180,000",
										"", "", "", "", "", ""));
		mSample.add(new InfoDataMain2("소비방법", "y", 
										"", "", "", "", 
										"", "", "", "", 
										"", "", "", "",
										"", "", "", "",
										"", "", "", "", "", "",
										"신용카드", "2,650,000", "체크카드", "130,000", "현금지불", "50,000"));
		mSample.add(new InfoDataMain2("카드정보", "y",
				"", "", "", "",
				"", "", "", "",
				"", "", "", "",
				"", "", "", "",
				"", "", "", "", "", "",
				"신용카드", "2,650,000", "체크카드", "130,000", "현금지불", "50,000"));
		return mSample;
	}
	
	/* Main 3 */
	public static ArrayList<InfoDataMain3> GetSampleDataMain3() {
		ArrayList<InfoDataMain3> mSample = new ArrayList<InfoDataMain3>();
//		mSample.add(new InfoDataMain3("주식", "y", "20,152,725", "어제 오후 08:20  유정일", "+702,000 (2.41%)", "", "", "", ""));
//		mSample.add(new InfoDataMain3("펀드", "n", "28,654,214", "어제 오후 08:20  유정일", "+142,235 (0.34%", "", "", "", ""));
		return mSample;
	}
	
	/* Main 4 */
	public static ArrayList<InfoDataMain4> GetSampleDataMain4() {
		ArrayList<InfoDataMain4> mSample = new ArrayList<InfoDataMain4>();
		mSample.add(new InfoDataMain4("챌린지", "y",
										"떠나자~ 유럽으로!", "1,950,000", "떠나자~ 미국으로!", "2,000,000", "떠나자~ 한국으로!", "3,000,000",
										"", "", "", "", "", "",
										"", "", "", "", "", "",
										"", "", "", "", "", "",
										"", "", "", "", "", "", new EtcMainData()));
		mSample.add(new InfoDataMain4("머니 캘린더", "y",
										"", "", "", "", "", "",
										"오늘", "월세", "자동납부", "매월31일", "550,000", "확정",
										"내일", "G마켓", "게좌이체", "내일", "20,000", "확정",
										"08", "삼성카드 결제", "자동납부", "매월31일", "1,560,000", "예상",
										"11", "하나카드 결제", "자동납부", "매월31일", "760,000", "예상", new EtcMainData()));
		return mSample;
	}

	/* Asset DaS */
	public static ArrayList<InfoDataAssetLoan> GetSampleDataAssetLoan() {
		ArrayList<InfoDataAssetLoan> mSample = new ArrayList<InfoDataAssetLoan>();
		mSample.add(new InfoDataAssetLoan("국민은행", "KB Story 통장", "557-910149-*******", "32,560,000", "IFC 올리브영", "지로", "2015.10.29 오후 07:31", "-12,210"));
		mSample.add(new InfoDataAssetLoan("국민은행", "KB Story 통장", "557-910149-*******", "32,560,000", "메가박스", "지로", "어제 오후 01:31", "+57,210"));
		mSample.add(new InfoDataAssetLoan("국민은행", "KB Story 통장", "557-910149-*******", "32,560,000", "김경미", "인터넷", "2015.10.29 오후 11:31", "157,000"));
		mSample.add(new InfoDataAssetLoan("국민은행", "KB Story 통장", "557-910149-*******", "32,560,000", "GS25 IFC점", "인터넷", "2015.10.29 오후 07:31", "-2,210"));
		mSample.add(new InfoDataAssetLoan("국민은행", "KB Story 통장", "557-910149-*******", "32,560,000", "김경미", "인터넷", "2015.10.30 오전 08:31", "+70,000"));
		mSample.add(new InfoDataAssetLoan("우리은행", "우리 Story 통장", "999-910149-*******", "220,000", "코엑스", "지로", "2015.10.29 오후 07:31", "-12,210"));
		mSample.add(new InfoDataAssetLoan("우리은행", "우리 Story 통장", "999-910149-*******", "220,000", "강북밀리오레", "지로", "어제 오후 01:31", "+57,210"));
		mSample.add(new InfoDataAssetLoan("우리은행", "우리 Story 통장", "999-910149-*******", "220,000", "안철수", "인터넷", "2015.10.29 오후 11:31", "157,000"));
		mSample.add(new InfoDataAssetLoan("우리은행", "우리 Story 통장", "999-910149-*******", "220,000", "자라", "인터넷", "2015.10.29 오후 07:31", "-2,210"));
		mSample.add(new InfoDataAssetLoan("우리은행", "우리 Story 통장", "999-910149-*******", "220,000", "김기동", "인터넷", "2015.10.30 오전 08:31", "+70,000"));
		mSample.add(new InfoDataAssetLoan("신한은행", "Sin Story", "1117-2349-*******", "2,333,000", "IFC 올리브영", "지로", "2015.10.29 오후 07:31", "-12,210"));
		mSample.add(new InfoDataAssetLoan("신한은행", "Sin Story", "1117-2349-*******", "2,333,000", "강남 아울렛", "지로", "어제 오후 01:31", "+57,210"));
		mSample.add(new InfoDataAssetLoan("신한은행", "Sin Story", "1117-2349-*******", "2,333,000", "홍길동", "인터넷", "2015.10.29 오후 11:31", "157,000"));
		mSample.add(new InfoDataAssetLoan("신한은행", "Sin Story", "1117-2349-*******", "2,333,000", "E-mart", "인터넷", "2015.10.29 오후 07:31", "-2,210"));
		mSample.add(new InfoDataAssetLoan("신한은행", "Sin Story", "1117-2349-*******", "2,333,000", "또치", "인터넷", "2015.10.30 오전 08:31", "+70,000"));
		mSample.add(new InfoDataAssetLoan("외환은행", "W 통장", "907-910149-*******", "72,235,000", "현대홈쇼핑", "지로", "2015.10.29 오후 07:31", "-12,210"));
		mSample.add(new InfoDataAssetLoan("외환은행", "W 통장", "907-910149-*******", "72,235,000", "스타벅스 서교동점", "지로", "어제 오후 01:31", "+57,210"));
		mSample.add(new InfoDataAssetLoan("외환은행", "W 통장", "907-910149-*******", "72,235,000", "안드레아", "인터넷", "2015.10.29 오후 11:31", "157,000"));
		mSample.add(new InfoDataAssetLoan("외환은행", "W 통장", "907-910149-*******", "72,235,000", "롯데홈쇼핑 IFC점", "인터넷", "2015.10.29 오후 07:31", "-2,210"));
		mSample.add(new InfoDataAssetLoan("외환은행", "W 통장", "907-910149-*******", "72,235,000", "둘리", "인터넷", "2015.10.30 오전 08:31", "+70,000"));
		mSample.add(new InfoDataAssetLoan("농협은행", "NH 통장", "11-55549-*******", "102,599,000", "IFC 올리브영", "지로", "2015.10.29 오후 07:31", "-12,210"));
		mSample.add(new InfoDataAssetLoan("농협은행", "NH 통장", "11-55549-*******", "102,599,000", "커피빈 서교동점", "지로", "어제 오후 01:31", "+57,210"));
		mSample.add(new InfoDataAssetLoan("농협은행", "NH 통장", "11-55549-*******", "102,599,000", "통키", "인터넷", "2015.10.29 오후 11:31", "157,000"));
		mSample.add(new InfoDataAssetLoan("농협은행", "NH 통장", "11-55549-*******", "102,599,000", "세븐일레븐 IFC점", "인터넷", "2015.10.29 오후 07:31", "-2,210"));
		mSample.add(new InfoDataAssetLoan("농협은행", "NH 통장", "11-55549-*******", "102,599,000", "세일러문", "인터넷", "2015.10.30 오전 08:31", "+70,000"));
		return mSample;
	}
	

	/* Notice */
	public static ArrayList<InfoDataSettingNotice> GetSampleDataSettingNotice() {
		ArrayList<InfoDataSettingNotice> mSample = new ArrayList<InfoDataSettingNotice>();
		mSample.add(new InfoDataSettingNotice("브로콜리는 어떤 서비스인가요?", "2016.04.01", "브로콜리는 각 금융기관의 금융데이터를 한데 모아 자산, 소비, 투자 등 각 항목에 맞게 분류해주는 서비스를 제공합니다. 이를 통해 이용 고객들은 흩어져있던 개인의 금융정보를 편리하게 취합하여, 통합적인 자산관리를 할 수 있습니다."));
		mSample.add(new InfoDataSettingNotice("Ver 1.1.2 서비스 변경 업데이트 사항 공지", "2016.08.11", "안녕하세요. 브로콜리팀입니다.\n" +
				"브로콜리 서비스를 이용해주시는 회원 여러분께 감사드리며, 브로콜리 서비스 약관 변경 및 기존 서비스 지원 변경사항에 대한 안내 말씀 드립니다.\n" +
				" \n" +
				"1. 변경 내역 및 사유\n" +
				"  1) 브로콜리 서비스의 기능 고도화 및 추가에 따른 약관변경 반영\n" +
				"   · 회원가입 절차 변경: 실명인증을 통한 회원가입(기존 회원 포함)\n" +
				"   · 서비스 기능 추가: 송금 및 예약이체 서비스, 옐로원패스 멤버십 서비스 등\n" +
				"   · 서비스 개인정보 규정, 취급 및 처리에 따른 변경\n" +
				" \n" +
				"  2) 브로콜리 서버 개선에 따른 기존 데이터 초기화\n" +
				"   · 서버개선 및 변경 작업으로 인한 기존 데이터 삭제\n" +
				" \n" +
				"2. 적용 시기\n" +
				"   변경된 브로콜리 서비스 약관은 2016년 8월 24일자로 효력이 발생하며, 변경된 서버 운영은 Ver1.1.2부터 적용됩니다.\n" +
				" \n" +
				"3. 이이 및 문제제기\n" +
				"   변경약관 및 공지내용에 대한 문의사항이 있으신 경우 고객센터(broccoli@yellofg.com)로 접수해 주시면 신속하고 친절하게 안내해드리겠습니다.\n" +
				" \n" +
				"기존 브로콜리를 이용해주시는 회원분들께 서버 개선작업으로 인해 불편을 드린 점 사과드립니다.\n" +
				"더욱 안전하고 편리한 서비스를 제공하기 위해 노력하는 브로콜리가 되겠습니다.\n" +
				" \n" +
				"감사합니다." +
				" \n" + " \n" ));
//		mSample.add(new InfoDataSettingNotice("공지사항3", "2015.10.30 오후 10:20", "내용 1줄까지 표기, 한줄 이상의 내용은 표시못한, 내용 1줄까지 표기, 한줄 이상의 내용은 표시못한\n내용 1줄까지 표기, 한줄 이상의 내용은 표시못한, 내용 1줄까지 표기, 한줄 이상의 내용은 표시못한\n내용 1줄까지 표기, 한줄 이상의 내용은 표시못한, 내용 1줄까지 표기, 한줄 이상의 내용은 표시못한"));
		return mSample;
	}
	
	
	
	/*
	 * 
	String mStr = "기타 재산 등록 \n취소하시겠습니까?";
	DialogUtils.showDlgBaseTwoButton(this, null,
			new InfoDataDialog(R.layout.dialog_inc_case_1, "취소", "확인", mStr));
			
	String mStr1 = "브로콜리가 비활성 되었습니다.\n10분 후에 다시 시도";
	String mStr2 = "암호를 분실하셨을 경우 메일로 \n임시암호를 발급하였습니다.";
	DialogUtils.showDlgBaseTwoButton(this, null,
			new InfoDataDialog(R.layout.dialog_inc_case_2, "취소", "확인", mStr1, mStr2));
			
	String mStr3 = "청구기관";
	DialogUtils.showDlgBaseOneButton(this, null,
			new InfoDataDialog(R.layout.dialog_inc_case_3, "청구기관", "취소"));
			
	*/


}
