package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class BodySpendCategoryList {
    private String categoryId;
    private String sum;
    private String percent;

    /**
     * 소비분류코드
     * @return
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 소비분류코드
     * @param category_code
     */
    public void setCategoryId(String category_code) {
        this.categoryId = category_code;
    }

    /**
     * 금액
     * @return
     */
    public String getSum() {
        return sum;
    }

    /**
     * 금액
     * @param sum
     */
    public void setSum(String sum) {
        this.sum = sum;
    }

    /**
     * 비율
     * @return
     */
    public String getPercent() {
        return percent;
    }

    /**
     * 비율
     * @param percent
     */
    public void setPercent(String percent) {
        this.percent = percent;
    }
}
