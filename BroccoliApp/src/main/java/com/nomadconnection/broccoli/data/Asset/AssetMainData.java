package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BaseChartData;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 자산 메인 조회
 */
public class AssetMainData extends Result{

    @SerializedName("monthlyProperty")
    public List<BaseChartData>     mMainAssetMonthlyProperty;                    // 월별 재산 합계

    @SerializedName("monthlyDebt")
    public List<BaseChartData>     mMainAssetMonthlyDebt;                        // 월별 부채 합계 재산 합계

    @SerializedName("bank")
    public ArrayList<AssetBankInfo> mMainAssetBank;                                // 예금/적금 리스트

    @SerializedName("cardBill")
    public AssetCardInfo            mMainAssetCard;                                // 카드 내역

    @SerializedName("stock")
    public AssetStockInfo          mMainAssetStock;                               // 주식 내역

    @SerializedName("fund")
    public List<JSONObject>        mMainAssetFund;                                // 펀드

    @SerializedName("debt")
    public ArrayList<AssetDebtInfo>     mMainAssetDebt;                                // 대출 내역

    @SerializedName("realEstate")
    public ArrayList<AssetEstateInfo>   mMainAssetRealEstate;                         // 부동산 내역

    @SerializedName("car")
    public ArrayList<AssetCarInfo>      mMainAssetCar;                                 // 차량 내역

    @SerializedName("etc")
    public ArrayList<AssetEtcInfo>        mMainAssetEtc;                                 // 기타


    public boolean isComplete() {
        boolean ret = true;
        for (Field field : getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.get(this) == null) {
                    ret = false;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }
}
