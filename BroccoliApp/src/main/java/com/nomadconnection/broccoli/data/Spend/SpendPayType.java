package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 지출수단
 */
public class SpendPayType {
    private String payType;
    private String sum;

    /**
     * 지출수단
     * <p>(01:신용카드, 02:체크카드, 03:현금, 04:계좌이체)</p>
     * @return
     */
    public String getPayType() {
        return payType;
    }

    /**
     * 지출수단
     * <p>(01:신용카드, 02:체크카드, 03:현금, 04:계좌이체)</p>
     * @param pay_type
     */
    public void setPayType(String pay_type) {
        this.payType = pay_type;
    }

    /**
     * 잔액
     * @return
     */
    public String getSum() {
        return sum;
    }

    /**
     * 잔액
     * @param sum
     */
    public void setSum(String sum) {
        this.sum = sum;
    }
}
