package com.nomadconnection.broccoli.data.Session;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 16. 1. 27..
 */
public class ResponseMembership extends Result {
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }
}
