package com.nomadconnection.broccoli.data.Remit;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RemitResult extends Result implements Serializable{
    private String result;
    private String msg;

    /**
     * 사용여부
     * @return
     */
    public String getResult() {
        return result;
    }

    /**
     * 사용여부
     * @param result
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * Error 메세지
     * @return
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Error 메세지
     * @param msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }
}
