package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseRemitAccountEdit extends RemitResult implements Serializable{
    private List<RemitAccount> accountInfo;
    private String accountCount = "";

    /**
     * 송금 계좌 리스트
     * @return
     */
    public List<RemitAccount> getAccountInfo() {
        return accountInfo;
    }

    /**
     * 송금 계좌 리스트
     * @param accountInfo
     */
    public void setAccountInfo(List<RemitAccount> accountInfo) {
        this.accountInfo = accountInfo;
    }

    /**
     * 총 계좌 갯수
     * @return
     */
    public String getAccountCount() {
        return accountCount;
    }

    /**
     * 총 계좌 갯수
     * @param accountCount
     */
    public void setAccountCount(String accountCount) {
        this.accountCount = accountCount;
    }
}
