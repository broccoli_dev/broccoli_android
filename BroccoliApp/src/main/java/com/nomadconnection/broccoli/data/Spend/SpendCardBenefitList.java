package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 지출수단
 */
public class SpendCardBenefitList implements Serializable {
    private String benefitLargeCategoryCode;
    private String benefitLargeCategoryCodeName;
    private List<SpendCardBenefitDetail> benefitCategoryList;

    /**
     * 카드혜택 대분류 코드
     * @return
     */
    public String getBenefitLargeCategoryCode() {
        return benefitLargeCategoryCode;
    }

    /**
     * 카드혜택 대분류 코드
     * @param benefitLargeCategoryCode
     */
    public void setBenefitLargeCategoryCode(String benefitLargeCategoryCode) {
        this.benefitLargeCategoryCode = benefitLargeCategoryCode;
    }

    /**
     * 카드 혜택 대분류 코드
     * @return
     */
    public String getBenefitLargeCategoryCodeName() {
        return benefitLargeCategoryCodeName;
    }

    /**
     * 카드 혜택 대분류 코드
     * @param benefitLargeCategoryCodeName
     */
    public void setBenefitLargeCategoryCodeName(String benefitLargeCategoryCodeName) {
        this.benefitLargeCategoryCodeName = benefitLargeCategoryCodeName;
    }

    public List<SpendCardBenefitDetail> getBenefitCategoryList() {
        return benefitCategoryList;
    }

    public void setBenefitCategoryList(List<SpendCardBenefitDetail> benefitCategoryList) {
        this.benefitCategoryList = benefitCategoryList;
    }

}
