package com.nomadconnection.broccoli.data.Asset;

import com.nomadconnection.broccoli.data.Spend.AssetCardPaymentTypeEnum;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 카드 내역 상세 조회 결과
 */
public class BodyAssetCardDetail implements Serializable {

    public String cardName;
    public AssetCardPaymentTypeEnum paymentType;
    public Integer installmentPeriod;
    public Integer installmentRound;
    public String useDate;
    public String storeName;
    public long realAmount;

}
