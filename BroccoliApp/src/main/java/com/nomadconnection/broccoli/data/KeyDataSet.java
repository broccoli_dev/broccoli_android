package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 */
public class KeyDataSet extends Result {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
