package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseRemitARSCheck extends RemitResult implements Serializable{
    private String accountId;
    private String failCount = "0";

    /**
     * 계좌아이디
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌아이디
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 인증실패시
     * @return
     */
    public String getFailCount() {
        return failCount;
    }

    /**
     * 인증실패시
     * @param failCount
     */
    public void setFailCount(String failCount) {
        this.failCount = failCount;
    }
}
