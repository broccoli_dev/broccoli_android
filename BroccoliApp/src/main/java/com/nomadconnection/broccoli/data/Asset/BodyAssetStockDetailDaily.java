package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * 주식 내역 상세 Daily 조회 바디
 */
public class BodyAssetStockDetailDaily implements Parcelable, Serializable {

    @SerializedName("stockCode")
    public String mBodyAssetStockDetailDailyStockCode;

    @SerializedName("date")
    public String mBodyAssetStockDetailDailyDate;

    @SerializedName("price")
    public String mBodyAssetStockDetailDailyPrice;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mBodyAssetStockDetailDailyStockCode);
        dest.writeString(this.mBodyAssetStockDetailDailyDate);
        dest.writeString(this.mBodyAssetStockDetailDailyPrice);
    }

    public BodyAssetStockDetailDaily(Parcel in) {
        this.mBodyAssetStockDetailDailyStockCode = in.readString();
        this.mBodyAssetStockDetailDailyDate = in.readString();
        this.mBodyAssetStockDetailDailyPrice = in.readString();
    }

    public static final Creator<BodyAssetStockDetailDaily> CREATOR = new Creator<BodyAssetStockDetailDaily>() {
        public BodyAssetStockDetailDaily createFromParcel(Parcel in) {
            return new BodyAssetStockDetailDaily(in);
        }
        public BodyAssetStockDetailDaily[] newArray (int size) {
            return new BodyAssetStockDetailDaily[size];
        }
    };

}
