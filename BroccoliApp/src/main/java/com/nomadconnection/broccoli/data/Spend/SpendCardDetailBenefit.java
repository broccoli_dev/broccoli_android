package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardDetailBenefit implements Serializable {

    public int benefitId;
    /**
     * 혜택 종류
     */
    public int benefitType;
    public Integer firstCategoryId;
    public Integer secondCategoryId;
    public Integer thirdCategoryId;
    public String firstCategoryName;
    public String secondCategoryName;
    public String thirdCategoryName;
    public String cardCode;
    /**
     * 전월 실적
     */
    public SpendCardMinMax requirement;
    /**
     * 이용 금액 조건
     */
    public SpendCardMinMax spend;
    public SpendCardDetailBenefitBody benefit;
    public int affiliateYn;

}
