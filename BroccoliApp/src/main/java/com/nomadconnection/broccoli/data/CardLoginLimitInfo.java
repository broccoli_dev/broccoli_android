package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 2016. 11. 17..
 */

public class CardLoginLimitInfo {
    private String cardCode;
    private String message;
    private int limitCount;

    public String getCardCode() {
        return cardCode;
    }

    public String getMessage() {
        return message;
    }

    public int getLimitCount() {
        return limitCount;
    }
}
