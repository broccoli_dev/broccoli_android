package com.nomadconnection.broccoli.data.Scrap;

import org.json.JSONObject;

/**
 * Created by YelloHyunminJang on 16. 8. 10..
 */
public class DataResult {
    private JSONObject lists;

    public JSONObject getLists() {
        return lists;
    }

    public void setLists(JSONObject lists) {
        this.lists = lists;
    }
}
