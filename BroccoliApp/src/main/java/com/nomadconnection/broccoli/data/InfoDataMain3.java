package com.nomadconnection.broccoli.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.Investment.InventMainData;

import java.util.ArrayList;


public class InfoDataMain3 implements Parcelable {

	public String mTxt1 = null;			// title
	public String mTxt2 = null;			// 등록여부
	public String mTxt3 = null;			// 금액
	public String mTxt4 = null;			// 업데이트 날자
	public String mTxt5 = null;			// 손익?
	public String mTxt6 = null;			// 부동산
	public String mTxt7 = null;			// 차량
	public String mTxt8 = null;			// count1
	public String mTxt9 = null;			// count2
	public InventMainData mInventMainData = new InventMainData();	// 투자 데이터 주로 챌린지 구성 하는데 사용


	public InfoDataMain3() {
	}

	public InfoDataMain3(String _Txt1, String _Txt2, String _Txt3, String _Txt4, String _Txt5, String _Txt6, String _Txt7, String _Txt8, String _Txt9, InventMainData _mInventMainData) {
		this.mTxt1 = _Txt1;
		this.mTxt2 = _Txt2;
		this.mTxt3 = _Txt3;
		this.mTxt4 = _Txt4;
		this.mTxt5 = _Txt5;
		this.mTxt6 = _Txt6;
		this.mTxt7 = _Txt7;
		this.mTxt8 = _Txt8;
		this.mTxt9 = _Txt9;
		this.mInventMainData = _mInventMainData;
	}



	public InfoDataMain3(Parcel in) {
		this.mTxt1 = in.readString();
		this.mTxt2 = in.readString();
		this.mTxt3 = in.readString();
		this.mTxt4 = in.readString();
		this.mTxt5 = in.readString();
		this.mTxt6 = in.readString();
		this.mTxt7 = in.readString();
		this.mTxt8 = in.readString();
		this.mTxt9 = in.readString();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.mTxt1);
		dest.writeString(this.mTxt2);
		dest.writeString(this.mTxt3);
		dest.writeString(this.mTxt4);
		dest.writeString(this.mTxt5);
		dest.writeString(this.mTxt6);
		dest.writeString(this.mTxt7);
		dest.writeString(this.mTxt8);
		dest.writeString(this.mTxt9);
	}

	public static final Creator<InfoDataMain3> CREATOR = new Creator<InfoDataMain3>() {
		public InfoDataMain3 createFromParcel(Parcel in) {
			return new InfoDataMain3(in);
		}
		public InfoDataMain3[] newArray (int size) {
			return new InfoDataMain3[size];
		}
	};

}
