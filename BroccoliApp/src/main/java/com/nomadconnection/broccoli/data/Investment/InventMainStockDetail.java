package com.nomadconnection.broccoli.data.Investment;

/**
 * Created by YelloHyunminJang on 16. 2. 5..
 */
public class InventMainStockDetail {

    private String stockCode;
    private String stockName;
    private String sum;
    private String prevSum;
    private String nowPrice;
    private String prevPrice;
    private InvestNewsyStockData newsystock;

    public String getStockCode() {
        return stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public String getSum() {
        return sum;
    }

    public String getPrevSum() {
        return prevSum;
    }

    public String getNowPrice() {
        return nowPrice;
    }

    public String getPrevPrice() {
        return prevPrice;
    }

    public InvestNewsyStockData getNewsystock() {
        return newsystock;
    }
}
