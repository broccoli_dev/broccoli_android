package com.nomadconnection.broccoli.data.Spend;

import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class SpendMonthlyList {
    private int totalCount;
    private List<SpendMonthlyInfo> list;

    /**
     * 당월 전체 소비 개수
     * @return
     */
    public int getTotalCount() {
        return totalCount;
    }

    /**
     * 당월 전체 소비 개수
     * @param total_count
     */
    public void setTotalCount(int total_count) {
        this.totalCount = total_count;
    }

    /**
     * 소비이력
     * @return
     */
    public List<SpendMonthlyInfo> getList() {
        return list;
    }

    /**
     * 소비이력
     * @param list
     */
    public void setList(List<SpendMonthlyInfo> list) {
        this.list = list;
    }
}
