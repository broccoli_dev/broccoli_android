package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2016. 10. 7..
 */

public class SpendCardBenefitDetail implements Serializable {

    private String benefitCategoryCode;
    private String benefitCategoryCodeName;
    private String summary;
    private String detail;

    /**
     * 혜택 분류 코드
     * @return
     */
    public String getBenefitCategoryCode() {
        return benefitCategoryCode;
    }

    /**
     * 혜택 분류 코드
     * @param benefitCategoryCode
     */
    public void setBenefitCategoryCode(String benefitCategoryCode) {
        this.benefitCategoryCode = benefitCategoryCode;
    }

    /**
     * 혜택 이름
     * @return
     */
    public String getBenefitCategoryCodeName() {
        return benefitCategoryCodeName;
    }

    /**
     * 혜택 이름
     * @param benefitCategoryCodeName
     */
    public void setBenefitCategoryCodeName(String benefitCategoryCodeName) {
        this.benefitCategoryCodeName = benefitCategoryCodeName;
    }

    /**
     * 혜택 요약
     * @return
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 혜택 요약
     * @param summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * 혜택 상세 내역
     * @return
     */
    public String getDetail() {
        return detail;
    }

    /**
     * 혜택 상세 내역
     * @param detail
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }
}
