package com.nomadconnection.broccoli.data.Etc;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class EtcChallengeBank extends Result implements Serializable{
    private String accountId;
    private String companyName;
    private String accountName;
    private String accountNum;
    private String addedYn;


    /**
     * 계좌 아이디
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌 아이디
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 은행 코드
     * @return
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 은행 코드
     * @param companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * 계좌 이름
     * @return
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * 계좌 이름
     * @param accountName
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * 계좌 번호
     * @return
     */
    public String getAccountNum() {
        return accountNum;
    }

    /**
     * 계좌 번호
     * @param accountNum
     */
    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    /**
     * 다른 챌린지 등록된계좌 여부
     * @return
     */
    public String getAddedYn() {
        return addedYn;
    }

    /**
     * 다른 챌린지 등록된계좌 여부
     * @param addedYn
     */
    public void setAddedYn(String addedYn) {
        this.addedYn = addedYn;
    }
}
