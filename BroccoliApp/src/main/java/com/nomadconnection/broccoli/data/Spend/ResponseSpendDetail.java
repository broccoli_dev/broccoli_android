package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseSpendDetail extends Result implements Serializable{
    private String totalSum;
    private List<SpendMonthlyInfo> list;

    /**
     * 월간 소비액 합계
     * @return
     */
    public String getTotalSum() {
        return totalSum;
    }

    /**
     * 월간 소비액 합계
     * @param totalSum
     */
    public void setTotalSum(String totalSum) {
        this.totalSum = totalSum;
    }

    /**
     * 소비이력 리스트
     * @return
     */
    public List<SpendMonthlyInfo> getList() {
        return list;
    }

    /**
     * 소비이력 리스트
     * @param list
     */
    public void setList(List<SpendMonthlyInfo> list) {
        this.list = list;
    }
}
