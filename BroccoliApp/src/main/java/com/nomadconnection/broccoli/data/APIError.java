package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 2017. 1. 5..
 */

public class APIError {

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
