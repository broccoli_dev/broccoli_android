package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataSendPhone implements Serializable{
    private String userId;
    private String sendPW;
    private String sendKind;
    private String accountId;
    private String sendMoney;
    private String telNo;
    private String opponent;




    /**
     * 유저아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 유저아이디
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 송금암호
     * @return
     */
    public String getSendPW() {
        return sendPW;
    }

    /**
     * 송금암호
     * @param sendPW
     */
    public void setSendPW(String sendPW) {
        this.sendPW = sendPW;
    }

    /**
     * 전송구분 (W: 월렛에서 A: 계좌에서 )
     * @return
     */
    public String getSendKind() {
        return sendKind;
    }

    /**
     * 전송구분 (W: 월렛에서 A: 계좌에서 )
     * @param sendKind
     */
    public void setSendKind(String sendKind) {
        this.sendKind = sendKind;
    }

    /**
     * 계좌 아이디 (월렛일 경우 그냥 공백으로 전송)
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌 아이디 (월렛일 경우 그냥 공백으로 전송)
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 전송금액
     * @return
     */
    public String getSendMoney() {
        return sendMoney;
    }

    /**
     * 전송금액
     * @param sendMoney
     */
    public void setSendMoney(String sendMoney) {
        this.sendMoney = sendMoney;
    }

    /**
     * 전화번호
     * @return
     */
    public String getTelNo() {
        return telNo;
    }

    /**
     * 전화번호
     * @param telNo
     */
    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    /**
     * 받는사람이름
     * @return
     */
    public String getOpponent() {
        return opponent;
    }

    /**
     * 받는사람이름
     * @param opponent
     */
    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }
}
