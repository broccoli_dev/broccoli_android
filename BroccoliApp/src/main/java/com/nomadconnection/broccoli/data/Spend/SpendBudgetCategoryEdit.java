package com.nomadconnection.broccoli.data.Spend;

import android.view.View;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 8..
 */

public class SpendBudgetCategoryEdit extends SpendBudgetCategory implements Serializable {

    public enum EditType {
        TOTAL,
        GROUP
    }

    public EditType editType = EditType.GROUP;
    public long backupBudget = 0;
    public String mEditText = null;
    public long mSubTotalBudget = 0;
    public View mItemView;

}
