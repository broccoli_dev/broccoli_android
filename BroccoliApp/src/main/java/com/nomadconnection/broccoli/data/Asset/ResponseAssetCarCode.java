package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * 차량 대표차명 조회
 */
public class ResponseAssetCarCode implements Parcelable {

    @SerializedName("carCode")
    public String mAssetCarCarCode;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetCarCarCode);
    }

    public ResponseAssetCarCode(Parcel in) {
        this.mAssetCarCarCode = in.readString();
    }

    public static final Creator<ResponseAssetCarCode> CREATOR = new Creator<ResponseAssetCarCode>() {
        public ResponseAssetCarCode createFromParcel(Parcel in) {
            return new ResponseAssetCarCode(in);
        }
        public ResponseAssetCarCode[] newArray (int size) {
            return new ResponseAssetCarCode[size];
        }
    };

}
