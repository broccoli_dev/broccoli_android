package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardDetailAnnualFee implements Serializable {

    public Integer annualFee;
    public Integer familyAnnualFee;
    /**
     * 카드 연회비
     * <p>1:국내, 2:해외, 3:모바일 카드</p>
     */
    public Integer type;
    public String name;

}
