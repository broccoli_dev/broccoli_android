package com.nomadconnection.broccoli.data.Etc;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 챌린지 계좌 조회
 */
public class RequestEtcChallengeSave {

    private String userId;
    private String challengeId;
    private String challengeType;
    private String title;
    private String goalDate;
    private String accountId;
    private String goalAmt;
    private String monthlyAmt;
    private String imagePath;


    /**
     * 사용자아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 사용자아이디
     * @param userId
     */public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 챌린지 아이디(0:등록, 나머지:수정)
     * @return
     */
    public String getChallengeId() {
        return challengeId;
    }

    /**
     * 챌린지 아이디(0:등록, 나머지:수정)
     * @param challengeId
     */public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    /**
     * 챌린지 종류
     * @return
     */
    public String getChallengeType() {
        return challengeType;
    }

    /**
     * 챌린지 종류
     * @param challengeType
     */public void setChallengeType(String challengeType) {
        this.challengeType = challengeType;
    }

    /**
     * 제목
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * 제목
     * @param title
     */public void setTitle(String title) {
        this.title = title;
    }

       /**
     * 목표일
     * @return
     */
    public String getGoalDate() {
        return goalDate;
    }

    /**
     * 목표일
     * @param goalDate
     */public void setGoalDate(String goalDate) {
        this.goalDate = goalDate;
    }

    /**
     * 계좌 아이디
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌 아이디
     * @param accountId
     */public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 목표액
     * @return
     */
    public String getGoalAmt() {
        return goalAmt;
    }

    /**
     * 목표액
     * @param goalAmt
     */public void setGoalAmt(String goalAmt) {
        this.goalAmt = goalAmt;
    }

    /**
     * 월 납입액
     * @return
     */
    public String getMonthlyAmt() {
        return monthlyAmt;
    }

    /**
     * 월 납입액
     * @param monthlyAmt
     */public void setMonthlyAmt(String monthlyAmt) {
        this.monthlyAmt = monthlyAmt;
    }

    /**
     * 이미지 경로
     * @return
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     *  이미지 경로
     * @param imagePath
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
