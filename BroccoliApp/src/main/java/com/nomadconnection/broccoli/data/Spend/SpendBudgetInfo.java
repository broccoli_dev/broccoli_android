package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 예산
 */
public class SpendBudgetInfo implements Serializable{
    private long budgetId;
    private String baseMonth;
    private long totalSum;
    private long nextTotalSum;
    private long totalExpense;
    private List<SpendBudgetDetail> detailList;

    /**
     * 예산 합계
     * @return
     */
    public String getBudgetSum() {
        return String.valueOf(totalSum);
    }

    public long getBudgetTotalSum() {
        return totalSum;
    }

    /**
     * 소비합계
     * @return
     */
    public String getExpenseSum() {
        return String.valueOf(totalExpense);
    }

    public long getNextTotalSum() {
        return nextTotalSum;
    }

    public List<SpendBudgetDetail> getDetailList() {
        return detailList;
    }
}
