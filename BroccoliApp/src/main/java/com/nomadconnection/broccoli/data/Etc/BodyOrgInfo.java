package com.nomadconnection.broccoli.data.Etc;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class BodyOrgInfo implements Parcelable {

    public BodyOrgInfo(String _BodyMoneyOrgDate, String _BodyOrgMethod, String _BodyOrgOpponent, long _BodyOrgSum, String _BodyOrgId){
        this.mBodyOrgDate = _BodyMoneyOrgDate;
        this.mBodyOrgMethod = _BodyOrgMethod;
        this.mBodyOrgOpponent = _BodyOrgOpponent;
        this.mBodyOrgSum = _BodyOrgSum;
        this.mBodyOrgId = _BodyOrgId;
    }

    @SerializedName("transactionDate")
    public String mBodyOrgDate;

    @SerializedName("transactionMethod")
    public String mBodyOrgMethod;

    @SerializedName("opponent")
    public String mBodyOrgOpponent;

    @SerializedName("sum")
    public long mBodyOrgSum;

    @SerializedName("transactionId")
    public String mBodyOrgId;

    @SerializedName("addedYn")
    public String mBodyOrgAddedYn;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mBodyOrgDate);
        dest.writeString(this.mBodyOrgMethod);
        dest.writeString(this.mBodyOrgOpponent);
        dest.writeLong(this.mBodyOrgSum);
        dest.writeString(this.mBodyOrgId);
        dest.writeString(this.mBodyOrgAddedYn);
    }

    public BodyOrgInfo(Parcel in) {
        this.mBodyOrgDate = in.readString();
        this.mBodyOrgMethod = in.readString();
        this.mBodyOrgOpponent = in.readString();
        this.mBodyOrgSum = in.readLong();
        this.mBodyOrgId = in.readString();
        this.mBodyOrgAddedYn = in.readString();
    }

    public static final Creator<BodyOrgInfo> CREATOR = new Creator<BodyOrgInfo>() {
        public BodyOrgInfo createFromParcel(Parcel in) {
            return new BodyOrgInfo(in);
        }
        public BodyOrgInfo[] newArray (int size) {
            return new BodyOrgInfo[size];
        }
    };


}
