package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BaseChartData;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 소비 메인 조회
 */
public class SpendMainData extends Result implements Serializable{

    private List<BaseChartData>     dailyExpense; //single month
    private List<MonthlyChartData>  monthlyExpense; //six month
    private SpendMonthlyList        expenseList;
    private SpendBudgetInfo         budget;
    private List<SpendCategoryInfo> expenseCategoryList;
    private List<SpendPayType>      expensePaytypeList;
    private List<SpendCardUserListData> userCardList;
    private SpendCardBanner banner;

    /**
     * 일별 소비
     * @return
     */
    public List<BaseChartData> getChartMonthlyProperty() {
        return dailyExpense;
    }

    /**
     * 일별 소비
     * @param monthly_property
     */
    public void setChartMonthlyProperty(List<BaseChartData> monthly_property) {
        this.dailyExpense = monthly_property;
    }

    /**
     * 월별 부채 합계 재산 합계
     * @return
     */
    public SpendMonthlyList getMonthlyInfo() {
        return expenseList;
    }

    /**
     * 월별 부채 합계 재산 합계
     * @param info
     */
    public void setMonthlyInfo(SpendMonthlyList info) {
        expenseList = info;
    }

    /**
     * 예산
     * @return
     */
    public SpendBudgetInfo getBudgetInfo() {
        return budget;
    }

    /**
     * 예산
     * @param info
     */
    public void setBudgetInfo(SpendBudgetInfo info) {
        budget = info;
    }

    /**
     * 소비 카테고리별 분류
     * @return
     */
    public List<SpendCategoryInfo> getCategory() {
        return expenseCategoryList;
    }

    /**
     * 소비 카테고리별 분류
     * @param category
     */
    public void setCategory(List<SpendCategoryInfo> category) {
        this.expenseCategoryList = category;
    }

    /**
     * 소비 지출 수단별 분류
     * @return
     */
    public List<SpendPayType> getPayType() {
        return expensePaytypeList;
    }

    /**
     * 소비 지출 수단별 분류
     * @param expense_paytype_list
     */
    public void setPayType(List<SpendPayType> expense_paytype_list) {
        this.expensePaytypeList = expense_paytype_list;
    }

    public boolean isComplete() {
        boolean ret = true;
        for (Field field : getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.get(this) == null) {
                    ret = false;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    public List<MonthlyChartData> getMonthlyExpense() {
        return monthlyExpense;
    }

    public List<SpendCardUserListData> getUserCardList() {
        return userCardList;
    }

    public SpendCardBanner getBanner() {
        return banner;
    }
}
