package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 8..
 */

public class SpendBudgetDetail implements Serializable {

    public long budgetId;
    public long detailId;
    public int largeCategoryId;
    public long sum;
    public long expense;

}
