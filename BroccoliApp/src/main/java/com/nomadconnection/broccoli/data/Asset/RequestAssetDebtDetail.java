package com.nomadconnection.broccoli.data.Asset;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * <P>현재 데이터 미정</P>
 */
public class RequestAssetDebtDetail {
    private String userId;
    private int debtId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    public int getDebtId() {
        return debtId;
    }

    public void setDebtId(int debt_id) {
        this.debtId = debt_id;
    }
}
