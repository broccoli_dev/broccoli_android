package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 카드 내역 상세 조회 결과
 */
public class ResponseAssetCardDetail implements Serializable {

    public long sum;

    public long onceSum;

    public long installmentSum;

    public long longLoanSum;

    @SerializedName("list")
    public ArrayList<BodyAssetCardDetail> list;

}
