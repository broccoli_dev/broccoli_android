package com.nomadconnection.broccoli.data.Scrap;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 16. 1. 31..
 */
public class UploadStatus extends Result {

    private UploadStatusBank bank;
    private UploadStatusCard card;
    private UploadStatusCash cash;

}
