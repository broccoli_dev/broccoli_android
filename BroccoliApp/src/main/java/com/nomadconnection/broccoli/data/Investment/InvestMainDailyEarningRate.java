package com.nomadconnection.broccoli.data.Investment;

/**
 * Created by YelloHyunminJang on 16. 2. 5..
 */
public class InvestMainDailyEarningRate {
    private String date;
    private String earningRate;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEarningRate() {
        return earningRate;
    }

    public void setEarningRate(String estimate) {
        this.earningRate = estimate;
    }
}
