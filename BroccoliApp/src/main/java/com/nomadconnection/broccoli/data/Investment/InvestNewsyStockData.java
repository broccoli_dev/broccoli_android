package com.nomadconnection.broccoli.data.Investment;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2016. 11. 16..
 */

public class InvestNewsyStockData implements Serializable {

    private String ymd;
    private String env;
    private String fundamental;
    private String grade;
    private String longterm;
    private String momentum;
    private String total;
    private String totalChange;
    private String beta;
    private String stockcode;

    public String getYmd() {
        return ymd;
    }

    public String getEnv() {
        return env;
    }

    public String getFundamental() {
        return fundamental;
    }

    public String getGrade() {
        return grade;
    }

    public String getLongterm() {
        return longterm;
    }

    public String getMomentum() {
        return momentum;
    }

    public String getTotal() {
        return total;
    }

    public String getTotalChange() {
        return totalChange;
    }

    public String getBeta() {
        return beta;
    }

    public String getStockcode() {
        return stockcode;
    }
}
