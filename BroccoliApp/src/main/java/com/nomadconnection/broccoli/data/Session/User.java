package com.nomadconnection.broccoli.data.Session;

import java.io.Serializable;

/**
 * Created by whylee on 2015. 10. 26..
 */
public class User implements Serializable {

    private String userName;
    private String birthDate;
    private String gender;
    private String phoneNo;
    private String regNum;
    private String deviceKey;

    /**
     * 사용자 이름
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 사용자 이름
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 생년월일
     * <p>yyyyMMdd</p>
     * @return
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * 생년월일
     * <p>yyyyMMdd</p>
     * @param birth_date
     */
    public void setBirthDate(String birth_date) {
        this.birthDate = birth_date;
    }

    /**
     * 디바이스 키 Unique
     * @return
     */
    public String getDeviceKey() {
        return deviceKey;
    }

    /**
     * 디바이스 키 Unique
     * @param device_key
     */
    public void setDeviceKey(String device_key) {
        this.deviceKey = device_key;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * 성별(M:남성, F:여성)
     * @return
     */
    public String getGender() {
        return gender;
    }

    /**
     * 성별(M:남성, F:여성)
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }
}
