package com.nomadconnection.broccoli.data.Scrap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by YelloHyunminJang on 16. 4. 20..
 * 16.08.22 소스에서 사용안함
 */
public class UploadLogBody {
    private String RESULT;
    private String ERRMSG;
    private String ERRDOC;
    private String ACCTKIND;
    private String NUMBER;
    private String CURBAL;
    private String ENBBAL;
    private String ACCTNM;
    private String OPENDATE;
    private JSONArray LIST;

    public String getRESULT() {
        return RESULT;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public String getERRDOC() {
        return ERRDOC;
    }

    public void setERRDOC(String ERRDOC) {
        this.ERRDOC = ERRDOC;
    }

    public String getACCTKIND() {
        return ACCTKIND;
    }

    public void setACCTKIND(String ACCTKIND) {
        this.ACCTKIND = ACCTKIND;
    }

    public String getNUMBER() {
        return NUMBER;
    }

    public void setNUMBER(String NUMBER) {
        this.NUMBER = NUMBER;
    }

    public String getCURBAL() {
        return CURBAL;
    }

    public void setCURBAL(String CURBAL) {
        this.CURBAL = CURBAL;
    }

    public String getENBBAL() {
        return ENBBAL;
    }

    public void setENBBAL(String ENBBAL) {
        this.ENBBAL = ENBBAL;
    }

    public String getACCTNM() {
        return ACCTNM;
    }

    public void setACCTNM(String ACCTNM) {
        this.ACCTNM = ACCTNM;
    }

    public String getOPENDATE() {
        return OPENDATE;
    }

    public void setOPENDATE(String OPENDATE) {
        this.OPENDATE = OPENDATE;
    }

    public JSONArray getLIST() {
        return LIST;
    }

    public void setLIST(JSONArray LIST) {
        this.LIST = LIST;
    }

    public void setData(String jsonData) {
        if (jsonData != null) {
            try {
                JSONObject object = new JSONObject(jsonData);
                setRESULT(object.getString("RESULT"));
                setERRMSG(object.getString("ERRMSG"));
                setERRDOC(object.getString("ERRDOC"));
                setACCTKIND(object.getString("ACCTKIND"));
                setNUMBER(object.getString("NUMBER"));
                setCURBAL(object.getString("CURBAL"));
                setENBBAL(object.getString("ENBBAL"));
                setACCTNM(object.getString("ACCTNM"));
                setOPENDATE(object.getString("OPENDATE"));
                setLIST(object.getJSONArray("LIST"));
            } catch (JSONException e) {
                e.printStackTrace();
                setRESULT("fail");
                setERRMSG(jsonData);
            }
        }
    }
}
