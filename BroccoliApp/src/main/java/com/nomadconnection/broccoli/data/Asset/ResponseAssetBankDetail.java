package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 은행 거래 내역 상세 조회 결과
 */
public class ResponseAssetBankDetail implements Parcelable {

    @SerializedName("transDate")
    public String mAssetBankDetailTransDate;

    @SerializedName("transMethod")
    public String mAssetBankDetailTransMethod;

    @SerializedName("opponent")
    public String mAssetBankDetailOpponent;

    @SerializedName("sum")
    public String mAssetBankDetailSum;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetBankDetailTransDate);
        dest.writeString(this.mAssetBankDetailTransMethod);
        dest.writeString(this.mAssetBankDetailOpponent);
        dest.writeString(this.mAssetBankDetailSum);
    }

    public ResponseAssetBankDetail(Parcel in) {
        this.mAssetBankDetailTransDate = in.readString();
        this.mAssetBankDetailTransMethod = in.readString();
        this.mAssetBankDetailOpponent = in.readString();
        this.mAssetBankDetailSum = in.readString();
    }

    public ResponseAssetBankDetail(String value) {
        this.mAssetBankDetailTransDate = value;
        this.mAssetBankDetailTransMethod = value;
        this.mAssetBankDetailOpponent = value;
        this.mAssetBankDetailSum = value;
    }

    public static final Creator<ResponseAssetBankDetail> CREATOR = new Creator<ResponseAssetBankDetail>() {
        public ResponseAssetBankDetail createFromParcel(Parcel in) {
            return new ResponseAssetBankDetail(in);
        }
        public ResponseAssetBankDetail[] newArray (int size) {
            return new ResponseAssetBankDetail[size];
        }
    };

//    private String transDate;
//    private String transMethod;
//    private String opponent;
//    private String sum;
//
//    /**
//     * 거래일
//     * @return
//     */
//    public String getTransDate() {
//        return transDate;
//    }
//
//    /**
//     * 거래일
//     * @param date
//     */
//    public void setTransDate(String date) {
//        transDate = date;
//    }
//
//    /**
//     * 거래수단
//     * @return
//     */
//    public String getTransMethod() {
//        return transMethod;
//    }
//
//    /**
//     * 거래수단
//     * @param method
//     */
//    public void setTransMethod(String method) {
//        transMethod = method;
//    }
//
//    /**
//     * 거래대상
//     * @return
//     */
//    public String getOpponent() {
//        return opponent;
//    }
//
//    /**
//     * 거래대상
//     * @param opponent
//     */
//    public void setOpponent(String opponent) {
//        this.opponent = opponent;
//    }
//
//    /**
//     * 금액
//     * @return
//     */
//    public String getSum() {
//        return sum;
//    }
//
//    /**
//     * 금액
//     * @param sum
//     */
//    public void setSum(String sum) {
//        this.sum = sum;
//    }
}
