package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class AssetEstateInfo implements Parcelable {

    public AssetEstateInfo(){ }

    @SerializedName("estateId")
    public String mAssetEstateId;                      // 부동산 아이디

    @SerializedName("estateName")
    public String mAssetEstateName;                    // 부동산 이름

    @SerializedName("estateType")
    public String mAssetEstateType;                    // 부동산 종류

    @SerializedName("dealType")
    public String mAssetEstateDealType;                // 거래 유형

    @SerializedName("price")
    public String mAssetEstatePrice;                   // 시세 평가액



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetEstateId);
        dest.writeString(this.mAssetEstateName);
        dest.writeString(this.mAssetEstateType);
        dest.writeString(this.mAssetEstateDealType);
        dest.writeString(this.mAssetEstatePrice);
    }

    public AssetEstateInfo(Parcel in) {
        this.mAssetEstateId = in.readString();
        this.mAssetEstateName = in.readString();
        this.mAssetEstateType = in.readString();
        this.mAssetEstateDealType = in.readString();
        this.mAssetEstatePrice = in.readString();
    }

    public static final Creator<AssetEstateInfo> CREATOR = new Creator<AssetEstateInfo>() {
        public AssetEstateInfo createFromParcel(Parcel in) {
            return new AssetEstateInfo(in);
        }
        public AssetEstateInfo[] newArray (int size) {
            return new AssetEstateInfo[size];
        }
    };


//    @SerializedName("estateId")
//    private String mId;
//
//    @SerializedName("estateName")
//    private String mName;
//
//    @SerializedName("estateType")
//    private String mType;
//
//    @SerializedName("dealType")
//    private String mDealType;
//
//    @SerializedName("price")
//    private String mPrice;
//
//    /**
//     * 부동산 아이디
//     * @return
//     */
//    public String getId() {
//        return mId;
//    }
//
//    /**
//     * 부동산 아이디
//     * @param id
//     */
//    public void setId(String id) {
//        this.mId = id;
//    }
//
//    /**
//     * 부동산 이름
//     * @return
//     */
//    public String getName() {
//        return mName;
//    }
//
//    /**
//     * 부동산 이름
//     * @param mName
//     */
//    public void setUserName(String mName) {
//        this.mName = mName;
//    }
//
//    /**
//     * 부동산 종류
//     * @return
//     */
//    public String getType() {
//        return mType;
//    }
//
//    /**
//     * 부동산 종류
//     * @param mType
//     */
//    public void setType(String mType) {
//        this.mType = mType;
//    }
//
//    /**
//     * 거래 유형
//     * @return
//     */
//    public String getDealType() {
//        return mDealType;
//    }
//
//    /**
//     * 거래 유형
//     * @param mDealType
//     */
//    public void setDealType(String mDealType) {
//        this.mDealType = mDealType;
//    }
//
//    /**
//     * 시세 평가액
//     * @return
//     */
//    public String getPrice() {
//        return mPrice;
//    }
//
//    /**
//     * 시세 평가액
//     * @param mSum
//     */
//    public void setPrice(String mSum) {
//        this.mPrice = mSum;
//    }
}
