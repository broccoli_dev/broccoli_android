package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class Version {
    private String version;
    private String url;
    private String notice;
    private String termsYn;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getTermsYn() {
        return termsYn;
    }

    public void setTermsYn(String termsYn) {
        this.termsYn = termsYn;
    }
}
