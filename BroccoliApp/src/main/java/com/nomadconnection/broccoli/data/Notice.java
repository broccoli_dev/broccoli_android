package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class Notice {
    private String noticeId;
    private String subject;
    private String contents;
    private String emergencyYn;
    private String postDate;
    private String postFrom;
    private String postTo;

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getEmergencyYn() {
        return emergencyYn;
    }

    public void setEmergencyYn(String emergencyYn) {
        this.emergencyYn = emergencyYn;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostFrom() {
        return postFrom;
    }

    public void setPostFrom(String postFrom) {
        this.postFrom = postFrom;
    }

    public String getPostTo() {
        return postTo;
    }

    public void setPostTo(String postTo) {
        this.postTo = postTo;
    }
}
