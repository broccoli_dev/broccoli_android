package com.nomadconnection.broccoli.data.Investment;

import com.nomadconnection.broccoli.data.Result;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 2. 5..
 */
public class InventMainData extends Result {

    private String totalEstimate;
    private String difference;
    private String rate;
    private List<InvestMainDailyEstimate> dailyEstimate;
    private List<InvestMainDailyEarningRate> dailyEarningRate;
    private InventMainStock stock;

    public String getTotalEstimate() {
        return totalEstimate;
    }

    public void setTotalEstimate(String totalEstimate) {
        this.totalEstimate = totalEstimate;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public List<InvestMainDailyEstimate> getDailyEstimate() {
        return dailyEstimate;
    }

    public void setDailyEstimate(List<InvestMainDailyEstimate> dailyEstimate) {
        this.dailyEstimate = dailyEstimate;
    }

    public List<InvestMainDailyEarningRate> getDailyEarningRate() {
        return dailyEarningRate;
    }

    public void setDailyEarningRate(List<InvestMainDailyEarningRate> dailyEarningRate) {
        this.dailyEarningRate = dailyEarningRate;
    }

    public InventMainStock getStock() {
        return stock;
    }

    public void setStock(InventMainStock stock) {
        this.stock = stock;
    }

    public boolean isComplete() {
        boolean ret = true;
        for (Field field : getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.get(this) == null) {
                    ret = false;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }
}
