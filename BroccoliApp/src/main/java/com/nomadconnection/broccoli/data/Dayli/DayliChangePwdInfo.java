package com.nomadconnection.broccoli.data.Dayli;

/**
 * Created by YelloHyunminJang on 2016. 12. 28..
 */

public class DayliChangePwdInfo {

    private long userId;
    private String oldPassword;
    private String newPassword;

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
