package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class EditAssetCar {

    @SerializedName("userId")
    private String mEditAssetCarUserId;

    @SerializedName("carId")
    private String mEditAssetCarCarId;

    @SerializedName("makerCode")
    private String mEditAssetCarMakerCode;

    @SerializedName("carCode")
    private String mEditAssetCarCarCode;

    @SerializedName("madeYear")
    private String mEditAssetCarMadeYear;

    @SerializedName("subCode")
    private String mEditAssetCarSubCode;

    public EditAssetCar(String _EditAssetCarUserId, String _EditAssetCarCarId, String _EditAssetCarMakerCode, String _EditAssetCarCarCode, String _EditAssetCarMadeYear, String _EditAssetCarSubCode){
        this.mEditAssetCarUserId = _EditAssetCarUserId;
        this.mEditAssetCarCarId = _EditAssetCarCarId;
        this.mEditAssetCarMakerCode = _EditAssetCarMakerCode;
        this.mEditAssetCarCarCode = _EditAssetCarCarCode;
        this.mEditAssetCarMadeYear = _EditAssetCarMadeYear;
        this.mEditAssetCarSubCode = _EditAssetCarSubCode;
    }

//    private String userId;
//    private String carId;
//    private String makerCode;
//    private String carCode;
//    private String madeYear;
//    private String subCode;
//
//    /**
//     * 사용자 아이디
//     * @return
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * 사용자 아이디
//     * @param userId
//     */
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    /**
//     * 자동차 아이디
//     * <p>0:추가, 나머지: 수정</p>
//     * @return
//     */
//    public String getCarId() {
//        return carId;
//    }
//
//    /**
//     * 자동차 아이디
//     * <p>0:추가, 나머지: 수정</p>
//     * @param car_id
//     */
//    public void setCarId(String car_id) {
//        this.carId = car_id;
//    }
//
//    /**
//     * 제조사 코드
//     * @return
//     */
//    public String getMakerCode() {
//        return makerCode;
//    }
//
//    /**
//     * 제조사 코드
//     * @param maker_code
//     */
//    public void setMakerCode(String maker_code) {
//        this.makerCode = maker_code;
//    }
//
//    /**
//     * 차종 코드
//     * @return
//     */
//    public String getCarCode() {
//        return carCode;
//    }
//
//    /**
//     * 차종 코드
//     * @param car_code
//     */
//    public void setCarCode(String car_code) {
//        this.carCode = car_code;
//    }
//
//    /**
//     * 연식
//     * <p>yyyy</p>
//     * @return
//     */
//    public String getMadeYear() {
//        return madeYear;
//    }
//
//    /**
//     * 연식
//     * <p>yyyy</p>
//     * @param made_year
//     */
//    public void setMadeYear(String made_year) {
//        this.madeYear = made_year;
//    }
//
//    /**
//     * 세부차명
//     * @return
//     */
//    public String getSubCode() {
//        return subCode;
//    }
//
//    /**
//     * 세부차명
//     * @param subCode
//     */
//    public void setSubCode(String subCode) {
//        this.subCode = subCode;
//    }
}
