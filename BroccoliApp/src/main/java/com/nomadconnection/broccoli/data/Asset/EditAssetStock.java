package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 주식 내역 추가/수정
 */
public class EditAssetStock {

    @SerializedName("userId")
    private String mEditAssetStockUserId;

    @SerializedName("transId")
    private String mEditAssetStockTransId;

    @SerializedName("transType")
    private int mEditAssetStockTransType;

    @SerializedName("transDate")
    private String mEditAssetStockTransDate;

    @SerializedName("stockCode")
    private String mEditAssetStockStockCode;

    @SerializedName("price")
    private String mEditAssetStockPrice;

    @SerializedName("amount")
    private String mEditAssetStockAmount;

    @SerializedName("sum")
    private String mEditAssetStockSum;

    public EditAssetStock(String _EditAssetStockUserId, String _EditAssetStockTransId, int _EditAssetStockTransType, String _EditAssetStockTransDate,
                          String _EditAssetStockStockCode, String _EditAssetStockPrice, String _EditAssetStockAmount, String _EditAssetStockSum){
        this.mEditAssetStockUserId = _EditAssetStockUserId;
        this.mEditAssetStockTransId = _EditAssetStockTransId;
        this.mEditAssetStockTransType = _EditAssetStockTransType;
        this.mEditAssetStockTransDate = _EditAssetStockTransDate;
        this.mEditAssetStockStockCode = _EditAssetStockStockCode;
        this.mEditAssetStockPrice = _EditAssetStockPrice;
        this.mEditAssetStockAmount = _EditAssetStockAmount;
        this.mEditAssetStockSum = _EditAssetStockSum;
    }

//    private String userId;
//    private String stockCode;
//
//    /**
//     * 사용자아이디
//     * @return
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * 사용자아이디
//     * @param user_id
//     */
//    public void setUserId(String user_id) {
//        this.userId = user_id;
//    }
//
//    /**
//     * 거래 아이디
//     * <p>0:추가, 나머지:수정</p>
//     * @return
//     */
//    public String getTransId() {
//        return super.getTransId();
//    }
//
//    /**
//     * 거래 아이디
//     * <p>0:추가, 나머지:수정</p>
//     * @param transId
//     */
//    public void setTransId(String transId) {
//        super.setTransId(transId);
//    }
//
//    /**
//     * 주식코드
//     * @return
//     */
//    public String getStockCode() {
//        return stockCode;
//    }
//
//    /**
//     * 주식코드
//     * @param stockCode
//     */
//    public void setStockCode(String stockCode) {
//        this.stockCode = stockCode;
//    }
}
