package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.constant.BenefitInternalType;

import java.util.HashMap;

/**
 * Created by YelloHyunminJang on 2017. 3. 29..
 */

public class SpendCardBenefitDetailRowData {

    public boolean isSecondCategory = false;
    public String benefitId;
    public String benefitName;
    public HashMap<Integer, HashMap<BenefitInternalType, SpendCardBenefitInternalBody>> benefitTypeMap = new HashMap<>();
}
