package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * 차량 대표차명 조회
 */
public class RequestAssetCarCode {

    @SerializedName("userId")
    public String mUserId;

    @SerializedName("makerCode")
    public String mMakerCode;


    public RequestAssetCarCode(String _UserId, String _MakerCode){
        this.mUserId = _UserId;
        this.mMakerCode = _MakerCode;
    }

}
