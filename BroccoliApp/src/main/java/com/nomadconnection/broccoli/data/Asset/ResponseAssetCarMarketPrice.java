package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.nomadconnection.broccoli.data.Result;

/**
 * 차량 시세 조회
 */
public class ResponseAssetCarMarketPrice extends Result implements Parcelable {

    public ResponseAssetCarMarketPrice() {}

    @SerializedName("marketPrice")
    public String mAssetCarMarketPrice;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetCarMarketPrice);
        dest.writeString(this.getCode());
        dest.writeString(this.getDesc());
    }

    public ResponseAssetCarMarketPrice(Parcel in) {
        this.mAssetCarMarketPrice = in.readString();
    }

    public static final Creator<ResponseAssetCarMarketPrice> CREATOR = new Creator<ResponseAssetCarMarketPrice>() {
        public ResponseAssetCarMarketPrice createFromParcel(Parcel in) {
            return new ResponseAssetCarMarketPrice(in);
        }
        public ResponseAssetCarMarketPrice[] newArray (int size) {
            return new ResponseAssetCarMarketPrice[size];
        }
    };

}
