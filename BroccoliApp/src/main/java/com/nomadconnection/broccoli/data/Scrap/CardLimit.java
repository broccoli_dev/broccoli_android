package com.nomadconnection.broccoli.data.Scrap;

import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 16. 2. 5..
 */
public class CardLimit extends Result {

    private String USERID;
    private String CMPNYCODE;
    private int TYPE;
    private String UPLOADID;
    private JsonObject result;

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public String getCMPNYCODE() {
        return CMPNYCODE;
    }

    public void setCMPNYCODE(String CMPNYCODE) {
        this.CMPNYCODE = CMPNYCODE;
    }

    public int getTYPE() {
        return TYPE;
    }

    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getUPLOADID() {
        return UPLOADID;
    }

    public void setUPLOADID(String UPLOADID) {
        this.UPLOADID = UPLOADID;
    }

    public JsonObject getResult() {
        return result;
    }

    public void setResult(JsonObject result) {
        this.result = result;
    }
}
