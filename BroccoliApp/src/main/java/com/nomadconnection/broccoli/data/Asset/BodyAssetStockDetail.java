package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 주식 내역 상세 조회 결과
 *
 */
public class BodyAssetStockDetail extends Result implements Parcelable, Serializable {

    public BodyAssetStockDetail() {}

    @SerializedName("count")
    public String mAssetStockDetailCount;

    @SerializedName("totalSum")
    public String mAssetStockDetailTotalSum;

//    @SerializedName("prevTotalSum")
//    public String mAssetStockDetailPrevTotalSum;

    @SerializedName("nowPrice")
    public String mAssetStockDetailNowPrice;

    @SerializedName("prevPrice")
    public String mAssetStockDetailPrevPrice;

    @SerializedName("dailyPrice")
    public ArrayList<BodyAssetStockDetailDaily> mAssetStockDetailListDaily = new ArrayList<BodyAssetStockDetailDaily>();

    @SerializedName("list")
    public ArrayList<BodyAssetStockList> mAssetStockDetailList = new ArrayList<BodyAssetStockList>();

    @SerializedName("highPrice")
    public String mAssetStockDetailHighPrice;

    @SerializedName("lowPrice")
    public String mAssetStockDetailLowPrice;

    @SerializedName("totalPrice")
    public String mAssetStockDetailTotalPrice;

    @SerializedName("tradeAmount")
    public String mAssetStockDetailTradeAmount;

    @SerializedName("totalAmount")
    public String mAssetStockDetailTotalAmount;

    @SerializedName("foreignerPercent")
    public float mAssetStockDetailForeignerPercent;

    @SerializedName("per")
    public float mAssetStockDetailPer;

    @SerializedName("pbr")
    public float mAssetStockDetailPbr;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetStockDetailCount);
        dest.writeString(this.mAssetStockDetailTotalSum);
//        dest.writeString(this.mAssetStockDetailPrevTotalSum);
        dest.writeString(this.mAssetStockDetailNowPrice);
        dest.writeString(this.mAssetStockDetailPrevPrice);
        dest.writeList(this.mAssetStockDetailListDaily);
        dest.writeList(this.mAssetStockDetailList);
        dest.writeString(mAssetStockDetailHighPrice);
        dest.writeString(mAssetStockDetailLowPrice);
        dest.writeString(mAssetStockDetailTotalPrice);
        dest.writeString(mAssetStockDetailTradeAmount);
        dest.writeString(mAssetStockDetailTotalAmount);
        dest.writeFloat(mAssetStockDetailForeignerPercent);
        dest.writeFloat(mAssetStockDetailPer);
        dest.writeFloat(mAssetStockDetailPbr);
    }

    public BodyAssetStockDetail(Parcel in) {
        this.mAssetStockDetailCount = in.readString();
        this.mAssetStockDetailTotalSum = in.readString();
//        this.mAssetStockDetailPrevTotalSum = in.readString();
        this.mAssetStockDetailNowPrice = in.readString();
        this.mAssetStockDetailPrevPrice = in.readString();
        this.mAssetStockDetailListDaily = in.readArrayList(BodyAssetStockDetailDaily.class.getClassLoader());
        this.mAssetStockDetailList = in.readArrayList(BodyAssetStockList.class.getClassLoader());
        this.mAssetStockDetailHighPrice = in.readString();
        this.mAssetStockDetailLowPrice = in.readString();
        this.mAssetStockDetailTotalPrice = in.readString();
        this.mAssetStockDetailTradeAmount = in.readString();
        this.mAssetStockDetailTotalAmount = in.readString();
        this.mAssetStockDetailForeignerPercent = in.readFloat();
        this.mAssetStockDetailPer = in.readFloat();
        this.mAssetStockDetailPbr = in.readFloat();
    }

    public static final Creator<BodyAssetStockDetail> CREATOR = new Creator<BodyAssetStockDetail>() {
        public BodyAssetStockDetail createFromParcel(Parcel in) {
            return new BodyAssetStockDetail(in);
        }
        public BodyAssetStockDetail[] newArray (int size) {
            return new BodyAssetStockDetail[size];
        }
    };

//    private String count;
//    private String totalSum;
//    private String prevTotalSum;
//    private List<BodyAssetStockList> list;
//
//    /**
//     * 총 수량
//     * @return
//     */
//    public String getCount() {
//        return count;
//    }
//
//    /**
//     * 총 수량
//     * @param count
//     */
//    public void setCount(String count) {
//        this.count = count;
//    }
//
//    /**
//     * 총 평가액
//     * @return
//     */
//    public String getTotalSum() {
//        return totalSum;
//    }
//
//    /**
//     * 총 평가액
//     * @param sum
//     */
//    public void setTotalSum(String sum) {
//        this.totalSum = sum;
//    }
//
//    /**
//     * 전일 평가액
//     * @return
//     */
//    public String getPrevTotalSum() {
//        return prevTotalSum;
//    }
//
//    /**
//     * 전일 평가액
//     * @param sum
//     */
//    public void setPrevTotalSum(String sum) {
//        prevTotalSum = sum;
//    }
//
//    /**
//     * 거래 내역
//     * @return
//     */
//    public List<BodyAssetStockList> getList() {
//        return list;
//    }
//
//    /**
//     * 거래 내역
//     * @param list
//     */
//    public void setList(List<BodyAssetStockList> list) {
//        this.list = list;
//    }
}
