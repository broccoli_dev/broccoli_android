package com.nomadconnection.broccoli.data.Dayli;

/**
 * Created by YelloHyunminJang on 2016. 12. 27..
 */

public class DayliJoinData {



    private String marriedYn = "2";
    private String email;
    private String password;
    private String telNumber;
    private String nickName;
    private String postNumber;
    private String addressBase;
    private String addressDetail;
    private String deviceId;

    public void setMarriedYn(String marriedYn) {
        this.marriedYn = marriedYn;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
