package com.nomadconnection.broccoli.data.Spend;

import android.view.View;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 8..
 */

public class SpendBudgetDetailEdit extends SpendBudgetDetail implements Serializable {

    public long backupBudget = 0;
    public String mEditText = null;
    public View mItemView;

}
