package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseSpendBudget extends Result implements Serializable{
    private String budgetId;
    private String expenseSum;
    private String budgetSum;
    private List<BodySpendBudgetCategory> list;

    /**
     * 예산 아이디
     * @return
     */
    public String getBudgetId() {
        return budgetId;
    }

    /**
     * 예산 아이디
     * @param budgetId
     */
    public void setBudgetId(String budgetId) {
        this.budgetId = budgetId;
    }

    /**
     * 소비합계
     * @return
     */
    public String getExpenseSum() {
        return expenseSum;
    }

    /**
     * 소비합계
     * @param expense_sum
     */
    public void setExpenseSum(String expense_sum) {
        this.expenseSum = expense_sum;
    }

    /**
     * 예산합계
     * @return
     */
    public String getBudgetSum() {
        return budgetSum;
    }

    /**
     * 예산합계
     * @param budget_sum
     */
    public void setBudgetSum(String budget_sum) {
        this.budgetSum = budget_sum;
    }
    

    /**
     * 소비 분류 리스트
     * @return
     */
    public List<BodySpendBudgetCategory> getList() {
        return list;
    }

    /**
     * 소비 분류 리스트
     * @param list
     */
    public void setList(List<BodySpendBudgetCategory> list) {
        this.list = list;
    }
}
