package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class DeleteAssetEstate {

    @SerializedName("userId")
    private String mDeleteAssetEstateUserId;

    @SerializedName("estateId")
    private String mDeleteAssetEstateEstateId;

    public DeleteAssetEstate(String _DeleteAssetEstateUserId, String _DeleteAssetEstateEstateId){
        this.mDeleteAssetEstateUserId = _DeleteAssetEstateUserId;
        this.mDeleteAssetEstateEstateId = _DeleteAssetEstateEstateId;
    }


//    private String userId;
//    private String estateId;
//
//    /**
//     * 사용자 아이디
//     * @return
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * 사용자 아이디
//     * @param user_id
//     */
//    public void setUserId(String user_id) {
//        this.userId = user_id;
//    }
//
//    /**
//     * 부동산 아이디
//     * @return
//     */
//    public String getEstateId() {
//        return estateId;
//    }
//
//    /**
//     * 부동산 아이디
//     * @param estate_id
//     */
//    public void setEstateId(String estate_id) {
//        this.estateId = estate_id;
//    }
}
