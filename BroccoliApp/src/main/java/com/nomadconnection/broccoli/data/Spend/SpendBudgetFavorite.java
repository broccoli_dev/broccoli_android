package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.constant.BudgetCategory;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 8..
 */

public class SpendBudgetFavorite implements Serializable {

    public BudgetCategory id;
    public boolean isFavorite = false;

}
