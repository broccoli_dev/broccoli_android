package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 월별 부채 합계 재산 합계
 */
public class SpendMonthlyInfo  implements Serializable{

    public static final String ONETIME = "1";
    public static final String ALWAYS = "2";

    private String expenseId;
    private String expenseDate;
    private String opponent;
    private String categoryId;
    private String payType;
    private String editYn;
    private String sum;
    private String memo;
    private String periodType;

    /**
     * 소비 아이디
     * @return
     */
    public String getExpenseId() {
        return expenseId;
    }

    /**
     * 소비 아이디
     * @param expense_id
     */
    public void setExpenseId(String expense_id) {
        this.expenseId = expense_id;
    }

    /**
     * 소비일
     * <p>yyyyMMddHHmmss</p>
     * <p>ex)20160107125635</p>
     * @return
     */
    public String getExpenseDate() {
        return expenseDate;
    }

    /**
     * 소비일
     * <p>yyyyMMddHHmmss</p>
     * @param expense_date
     */
    public void setExpenseDate(String expense_date) {
        this.expenseDate = expense_date;
    }

    /**
     * 소비 대상
     * @return
     */
    public String getOpponent() {
        return opponent;
    }

    /**
     * 소비 대상
     * @param opponent
     */
    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    /**
     * 소비 분류 코드
     * @return
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 소비 분류 코드
     * @param categoryId
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 결제수단
     * <p>01:신용카드, 02:체크카드, 03:현금, 04:계좌이체</p>
     * @return
     */
    public String getPayType() {
        return payType;
    }

    /**
     * 결제수단
     * <p>01:신용카드, 02:체크카드, 03:현금, 04:계좌이체</p>
     * @param pay_type
     */
    public void setPayType(String pay_type) {
        this.payType = pay_type;
    }

    /**
     * 금액
     * @return
     */
    public String getSum() {
        return sum;
    }

    /**
     * 금액
     * @param sum
     */
    public void setSum(String sum) {
        this.sum = sum;
    }

    /**
     * 사용자 입력 여부
     * <p>Y:사용자 입력, N:스크래핑</p>
     * @return
     */
    public String getEditYn() {
        return editYn;
    }

    /**
     * 사용자 입력 여부
     * <p>Y:사용자 입력, N:스크래핑</p>
     * @param editYn
     */
    public void setEditYn(String editYn) {
        this.editYn = editYn;
    }

    /**
     * <p>사용자 메모</p>
     * "" empty string - 입력없음
     * @return
     */
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * 적용 방식
     * <p>1:이번만, 2:항상 적용<p/>
     * @param periodType
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public String getPeriod() {
        return periodType;
    }
}
