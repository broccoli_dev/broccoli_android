package com.nomadconnection.broccoli.data.Auth;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 16. 7. 1..
 */
public class ResAuthentication extends Result {

    private String instanceId;
    private String sendTime;
    private int dailyReqCount;

    public String getInstanceID() {
        return instanceId;
    }

    public void setInstanceID(String instanceID) {
        this.instanceId = instanceID;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public int getDailyReqCount() {
        return dailyReqCount;
    }

    public void setDailyReqCount(int dailyReqCount) {
        this.dailyReqCount = dailyReqCount;
    }
}
