package com.nomadconnection.broccoli.data.Asset;

/**
 * Created by YelloHyunminJang on 2016. 10. 18..
 */

public class BodyBankActive {

    private String accountId;
    private String activeYn;

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setActiveYn(String activeYn) {
        this.activeYn = activeYn;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getActiveYn() {
        return activeYn;
    }
}
