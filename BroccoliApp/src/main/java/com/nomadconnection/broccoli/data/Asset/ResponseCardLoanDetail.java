package com.nomadconnection.broccoli.data.Asset;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 2. 7..
 */

public class ResponseCardLoanDetail implements Serializable {

    public int loanId;
    public String companyCode;

    /**
     * 대출 번호
     */
    public String loanNum;

    /**
     * 대출 금액
     */
    public long loanAmount;

    /**
     * 이자율
     */
    public float interestRate;

    /**
     * 대출 일자
     * yyyyMMdd
     */
    public String loanDate;

    /**
     * 만기 일자
     * yyyyMMdd
     */
    public String endDate;

    /**
     * 대출 잔액
     */
    public long loanBalance;

}
