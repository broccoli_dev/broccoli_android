package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseCardList extends Result {
    private String cardCode;
    private String cardName;
    private String cardType;

    /**
     * 카드코드
     * @return
     */
    public String getCardCode() {
        return cardCode;
    }

    /**
     * 카드코드
     * @param cardCode
     */
    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    /**
     * 카드이름
     * @return
     */
    public String getCardName() {
        return cardName;
    }

    /**
     * 카드이름
     * @param cardName
     */
    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardType() {
        return cardType;
    }
}
