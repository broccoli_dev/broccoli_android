package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataByDate {
    private String userId;
    private String date;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    /**
     * 기준월
     * <p>yyyyMM</p>
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     * 기준월
     * <p>yyyyMM</p>
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }
}
