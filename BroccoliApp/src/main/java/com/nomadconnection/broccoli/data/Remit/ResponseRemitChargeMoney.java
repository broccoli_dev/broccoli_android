package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseRemitChargeMoney extends RemitResult implements Serializable{
    private String balance;
    private String date;
    private String companyCode;
    private String accountNumber;

    /**
     * 잔액
     * @return
     */
    public String getBalance() {
        return balance;
    }

    /**
     * 잔액
     * @param balance
     */
    public void setBalance(String balance) {
        this.balance = balance;
    }

    /**
     * 송금시간
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     * 송금시간
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 은행코드
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 은행코드
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 계좌번호
     * @return
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * 계좌번호
     * @param accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
