package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 지출수단
 */
public class SpendCardBenefit implements Serializable{
    private String realName;
    private String requireResult;
    private String requirement;
    private String annualFee;
    private List<SpendCardBenefitList> list;


    /**
     * 카드 혜택
     * @return
     */
    public String getRealName() {
        return realName;
    }

    /**
     * 카드 혜택
     * @param realName
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * 필요 실적
     * @return
     */
    public String getRequirement() {
        return requirement;
    }

    /**
     * 필요 실적
     * @param requirement
     */
    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    /**
     * 필요 실적
     * @return
     */
    public String getRequireResult() {
        return requireResult;
    }

    /**
     * 필요 실적
     * @param requireResult
     */
    public void setRequireResult(String requireResult) {
        this.requireResult = requireResult;
    }


    /**
     * 연회비
     * @return
     */
    public String getAnnualFee() {
        return annualFee;
    }

    /**
     * 연회비
     * @param annualFee
     */
    public void setAnnualFee(String annualFee) {
        this.annualFee = annualFee;
    }

    /**
     * 카드혜택 리스트
     * @return
     */
    public List<SpendCardBenefitList> getList() {
        return list;
    }

    /**
     * 카드혜택 리스트
     * @param list
     */
    public void setList(List<SpendCardBenefitList> list) {
        this.list = list;
    }
}
