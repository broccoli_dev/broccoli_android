package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 22..
 */

public class SpendCardCompanyDetail implements Serializable {

    public String companyCode;
    public String companyName;
    public String brCmpnyCode;

}
