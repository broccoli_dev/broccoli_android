package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 지출수단
 */
public class SpendCardInfo implements Serializable{
    private String cardId;
    private String companyCode;
    private String cardName;
    private String cardCode;
    private String isCredit;
    private String sum;
    private String totalLimit;
    private SpendCardBenefit benefit;


    /**
     * 카드 아이디
     * @return
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * 카드 아이디
     * @param cardId
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * 카드사 코드
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 카드사 코드
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 카드이름(스크래핑으로 수집된 이름)
     * @return
     */
    public String getCardName() {
        return cardName;
    }

    /**
     * 카드이름(스크래핑으로 수집된 이름)
     * @param cardName
     */
    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    /**
     * 카드코드
     * @return
     */
    public String getCardCode() {
        return cardCode;
    }

    /**
     * 카드코드
     * @param cardCode
     */
    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    /**
     * 신용카드 여부
     * <p>(Y:신용카드, N:체크카드)</p>
     * @return
     */
    public String getIsCredit() {
        return isCredit;
    }

    /**
     * 신용카드 여부
     * <p>(Y:신용카드, N:체크카드)</p>
     * @param isCredit
     */
    public void setIsCredit(String isCredit) {
        this.isCredit = isCredit;
    }

    /**
     * 사용 실적
     * @return
     */
    public String getSum() {
        return sum;
    }

    /**
     * 사용 실적
     * @param sum
     */
    public void setSum(String sum) {
        this.sum = sum;
    }

    /**
     * 총한도
     * @return
     */
    public String getTotalLimit() {
        return totalLimit;
    }

    /**
     * 총한도
     * @param totalLimit
     */
    public void setTotalLimit(String totalLimit) {
        this.totalLimit = totalLimit;
    }

    /**
     * 카드 혜택
     * @return
     */
    public SpendCardBenefit getBenefit() {
        return benefit;
    }

    /**
     * 카드 혜택
     * @param benefit
     */
    public void setBenefit(SpendCardBenefit benefit) {
        this.benefit = benefit;
    }
}
