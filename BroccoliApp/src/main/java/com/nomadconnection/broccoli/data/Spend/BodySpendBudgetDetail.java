package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class BodySpendBudgetDetail implements Serializable{
    private String categoryId;
    private String sum;

    /**
     * 소비 분류 코드
     * @return
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 소비 분류 코드
     * @param category_code
     */
    public void setCategoryId(String category_code) {
        this.categoryId = category_code;
    }

    /**
     * 예산액
     * @return
     */
    public String getSum() {
        return sum;
    }

    /**
     * 예산액
     * @param sum
     */
    public void setSum(String sum) {
        this.sum = sum;
    }
}
