package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class DeleteAssetCar {

    @SerializedName("userId")
    private String mDeleteAssetCarUserId;

    @SerializedName("carId")
    private String mDeleteAssetCarCarId;

    public DeleteAssetCar(String _DeleteAssetCarUserId, String _DeleteAssetCarCarId){
        this.mDeleteAssetCarUserId = _DeleteAssetCarUserId;
        this.mDeleteAssetCarCarId = _DeleteAssetCarCarId;
    }

//    private String userId;
//    private String carId;
//
//    /**
//     * 사용자아이디
//     * @return
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * 사용자아이디
//     * @param user_id
//     */
//    public void setUserId(String user_id) {
//        this.userId = user_id;
//    }
//
//    /**
//     * 자동차 아이디
//     * @return
//     */
//    public String getCarId() {
//        return carId;
//    }
//
//    /**
//     * 자동차 아이디
//     * @param car_id
//     */
//    public void setCarId(String car_id) {
//        this.carId = car_id;
//    }
}
