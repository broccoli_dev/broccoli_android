package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.nomadconnection.broccoli.data.Investment.InvestNewsyStockData;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class BodyAssetStockInfo implements Parcelable {

    @SerializedName("stockCode")
    public String mBodyAssetStockCode;                          // 주식코드

    @SerializedName("stockName")
    public String mBodyAssetStockName;                          // 주식이름

    @SerializedName("sum")
    public String mBodyAssetStockSum;                           // 총액

    @SerializedName("prevSum")
    public String mBodyAssetStockPreviousSum;                   // 이전 평가액

    @SerializedName("nowPrice")
    public String mBodyNowPrice;

    @SerializedName("prevPrice")
    public String mBodyPrevPrice;

    @SerializedName("newsystock")
    public InvestNewsyStockData mBodyNewsy;

    public BodyAssetStockInfo() {

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mBodyAssetStockCode);
        dest.writeString(this.mBodyAssetStockName);
        dest.writeString(this.mBodyAssetStockSum);
        dest.writeString(this.mBodyAssetStockPreviousSum);
        dest.writeString(this.mBodyNowPrice);
        dest.writeString(this.mBodyPrevPrice);
        dest.writeSerializable(this.mBodyNewsy);
    }

    public BodyAssetStockInfo(Parcel in) {
        this.mBodyAssetStockCode = in.readString();
        this.mBodyAssetStockName = in.readString();
        this.mBodyAssetStockSum = in.readString();
        this.mBodyAssetStockPreviousSum = in.readString();
        this.mBodyNowPrice = in.readString();
        this.mBodyPrevPrice = in.readString();
        this.mBodyNewsy = (InvestNewsyStockData) in.readSerializable();
    }

    public static final Creator<BodyAssetStockInfo> CREATOR = new Creator<BodyAssetStockInfo>() {
        public BodyAssetStockInfo createFromParcel(Parcel in) {
            return new BodyAssetStockInfo(in);
        }
        public BodyAssetStockInfo[] newArray (int size) {
            return new BodyAssetStockInfo[size];
        }
    };
}
