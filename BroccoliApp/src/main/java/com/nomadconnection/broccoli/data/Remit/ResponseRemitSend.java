package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseRemitSend extends RemitResult implements Serializable{
    private String sendDate;
    private String balance;

    /**
     * 전송시각
     * @return
     */
    public String getSendDate() {
        return sendDate;
    }

    /**
     * 전송시각
     * @param sendDate
     */
    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    /**
     * 웰렛잔액
     * @return
     */
    public String getBalance() {
        return balance;
    }

    /**
     * 웰렛잔액
     * @param balance
     */
    public void setBalance(String balance) {
        this.balance = balance;
    }
}
