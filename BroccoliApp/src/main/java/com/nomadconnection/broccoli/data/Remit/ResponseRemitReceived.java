package com.nomadconnection.broccoli.data.Remit;

/**
 * Created by YelloHyunminJang on 16. 7. 15..
 */
public class ResponseRemitReceived {

    private String result;
    private String senderName;
    private String successCount;
    private String successSum;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(String successCount) {
        this.successCount = successCount;
    }

    public String getSuccessSum() {
        return successSum;
    }

    public void setSuccessSum(String successSum) {
        this.successSum = successSum;
    }
}
