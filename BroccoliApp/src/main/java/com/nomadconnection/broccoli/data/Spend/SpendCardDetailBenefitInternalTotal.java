package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardDetailBenefitInternalTotal implements Serializable {
    public ArrayList<SpendCardDetailBenefit> cardBenefitList;
    public ArrayList<SpendCardDetailBenefitAffiliate> cardBenefitAffiliate;
}
