package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 은행거래 내역 상세 조회 요청용 바디
 */
public class RequestAssetBankDetail {

    @SerializedName("userId")
    public String mUserId;

    @SerializedName("accountId")
    public String mID;

    @SerializedName("startDate")
    public String mStartDate;

    @SerializedName("endDate")
    public String mEndDate;

    @SerializedName("mode")
    public int mRequestMode;

    public RequestAssetBankDetail(String _UserId, String _ID, String _StartDate, String _EndDate, int _RequestMode){
        this.mUserId = _UserId;
        this.mID = _ID;
        this.mStartDate = _StartDate;
        this.mEndDate = _EndDate;
        this.mRequestMode = _RequestMode;
    }

//    @SerializedName("userId")
//    private String userId;
//    @SerializedName("accountId")
//    private String mID;
//    @SerializedName("startDate")
//    private String mStartDate;
//    @SerializedName("endDate")
//    private String mEndDate;
//    @SerializedName("mode")
//    private int mRequestMode;
//
//    /**
//     * 사용자아이디
//     * @return
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * 사용자아이디
//     * @param userId
//     */
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    /**
//     * 계좌 아이디
//     * @return
//     */
//    public String getID() {
//        return mID;
//    }
//
//    /**
//     * 계좌 아이디
//     * @param id
//     */
//    public void setID(String id) {
//        this.mID = id;
//    }
//
//    /**
//     * 시작일
//     * @return
//     */
//    public String getStartDate() {
//        return mStartDate;
//    }
//
//    /**
//     * 시작일
//     * @param date
//     */
//    public void setStartDate(String date) {
//        this.mStartDate = date;
//    }
//
//    /**
//     * 종료일
//     * @return
//     */
//    public String getEndDate() {
//        return mEndDate;
//    }
//
//    /**
//     * 종료일
//     * @param date
//     */
//    public void setEndDate(String date) {
//        this.mEndDate = date;
//    }
//
//    /**
//     * 조회모드
//     * <p>0:전체, 1:입금, 2:출금</p>
//     * @return
//     */
//    public int getRequestMode() {
//        return mRequestMode;
//    }
//
//    /**
//     * 조회모드
//     * <p>0:전체, 1:입금, 2:출금</p>
//     * @param mode
//     */
//    public void setRequestMode(int mode) {
//        this.mRequestMode = mode;
//    }
}
