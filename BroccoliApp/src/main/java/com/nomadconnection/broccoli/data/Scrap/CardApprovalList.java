package com.nomadconnection.broccoli.data.Scrap;

import com.google.gson.JsonObject;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 * 카드 승인 내역
 */
public class CardApprovalList {

    private String USERID;
    private String CMPNYCODE;
    private int TYPE;
    private String UPLOADID;
    private JsonObject result;

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public String getCMPNYCODE() {
        return CMPNYCODE;
    }

    public void setCMPNYCODE(String CMPNYCODE) {
        this.CMPNYCODE = CMPNYCODE;
    }

    /**
     * 데이터 종류
     * <p>데이터 종류 (1:은행계좌, 2:은행거래, 3:카드승인, 4:카드청구, 5:현금영수증)</p>
     * @return
     */
    public int getTYPE() {
        return TYPE;
    }

    /**
     * 데이터 종류
     * <p>데이터 종류 (1:은행계좌, 2:은행거래, 3:카드승인, 4:카드청구, 5:현금영수증)</p>
     * @param TYPE
     */
    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getUPLOADID() {
        return UPLOADID;
    }

    public void setUPLOADID(String UPLOADID) {
        this.UPLOADID = UPLOADID;
    }

    public void setResult(JsonObject result) {
        this.result = result;
    }
}
