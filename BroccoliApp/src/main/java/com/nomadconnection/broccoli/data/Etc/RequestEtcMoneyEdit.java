package com.nomadconnection.broccoli.data.Etc;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 은행거래 내역 상세 조회 요청용 바디
 */
public class RequestEtcMoneyEdit {

    @SerializedName("userId")
    public String mUserId;

    @SerializedName("calendarId")
    public String mCalendarId;

    @SerializedName("title")
    public String mTitle;

//    @SerializedName("transactionId")
//    public String mTransactionId;
    @SerializedName("opponent")
    public String mOpponent;

    @SerializedName("amt")
    public String mAmt;

    @SerializedName("payType")
    public String mPayType;

    @SerializedName("payDate")
    public String mPayDate;

    @SerializedName("repeatType")
    public String mRepeatType;

//    public RequestEtcMoneyEdit(String _UserId, String _CalendarId, String _Title, String _TransactionId, String _Amt, String _PayType, String _PayDate, String _RepeatType){
    public RequestEtcMoneyEdit(String _UserId, String _CalendarId, String _Title, String _Opponent, String _Amt, String _PayType, String _PayDate, String _RepeatType){
        this.mUserId = _UserId;
        this.mCalendarId = _CalendarId;
        this.mTitle = _Title;
//        this.mTransactionId = _TransactionId;
        this.mOpponent = _Opponent;
        this.mAmt = _Amt;
        this.mPayType = _PayType;
        this.mPayDate = _PayDate;
        this.mRepeatType = _RepeatType;
    }
}
