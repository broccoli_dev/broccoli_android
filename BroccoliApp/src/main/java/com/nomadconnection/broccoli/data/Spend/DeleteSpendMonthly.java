package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class DeleteSpendMonthly {
    private String userId;
    private String expenseId;
    private String editYn;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    /**
     * 소비내역 아이디
     * @return
     */
    public String getExpenseId() {
        return expenseId;
    }

    /**
     * 소비내역 아이디
     * @param expense_id
     */
    public void setExpenseId(String expense_id) {
        this.expenseId = expense_id;
    }

    public void setEditYn(String editYn) {
        this.editYn = editYn;
    }
}
