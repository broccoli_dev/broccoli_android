package com.nomadconnection.broccoli.data.Etc;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 은행거래 내역 상세 조회 요청용 바디
 */
public class RequestEtcMoneyDelete {

    @SerializedName("userId")
    public String mUserId;

    @SerializedName("calendarId")
    public String mCalendarId;

    public RequestEtcMoneyDelete(String _UserId, String _CalendarId){
        this.mUserId = _UserId;
        this.mCalendarId = _CalendarId;
    }
}
