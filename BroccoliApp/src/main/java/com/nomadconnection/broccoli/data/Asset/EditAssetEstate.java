package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class EditAssetEstate {

    @SerializedName("userId")
    private String mEditAssetEstateUserId;

    @SerializedName("estateId")
    private String mEditAssetEstateEstateId;

    @SerializedName("estateName")
    private String mEditAssetEstateEstateName;

    @SerializedName("estateType")
    private String mEditAssetEstateEstateType;

    @SerializedName("dealType")
    private String mEditAssetEstateDealType;

    @SerializedName("price")
    private String mEditAssetEstatePrice;

    public EditAssetEstate(String _EditAssetEstateUserId, String _EditAssetEstateEstateId, String _EditAssetEstateEstateName, String _EditAssetEstateEstateType, String _EditAssetEstateDealType, String _EditAssetEstatePrice){
        this.mEditAssetEstateUserId = _EditAssetEstateUserId;
        this.mEditAssetEstateEstateId = _EditAssetEstateEstateId;
        this.mEditAssetEstateEstateName = _EditAssetEstateEstateName;
        this.mEditAssetEstateEstateType = _EditAssetEstateEstateType;
        this.mEditAssetEstateDealType = _EditAssetEstateDealType;
        this.mEditAssetEstatePrice = _EditAssetEstatePrice;
    }


//    private String userId;
//    private String estateId;
//    private String estateName;
//    private String estateType;
//    private String dealType;
//    private String price;
//
//    /**
//     * 사용자아이디
//     * @return
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * 사용자아이디
//     * @param user_id
//     */
//    public void setUserId(String user_id) {
//        this.userId = user_id;
//    }
//
//    /**
//     * 부동산 아이디
//     * <p>0: 추가, 나머지:수정</p>
//     * @return
//     */
//    public String getEstateId() {
//        return estateId;
//    }
//
//    /**
//     * 부동산 아이디
//     * <p>0: 추가, 나머지:수정</p>
//     * @param id
//     */
//    public void setEstateId(String id) {
//        this.estateId = id;
//    }
//
//    /**
//     * 부동산 이름
//     * @return
//     */
//    public String getEstateName() {
//        return estateName;
//    }
//
//    /**
//     * 부동산 이름
//     * @param estate_name
//     */
//    public void setEstateName(String estate_name) {
//        this.estateName = estate_name;
//    }
//
//    /**
//     * 부동산 종류
//     * <p>01:아파트, 02:빌라, 03:단독, 05:상가, 06:오피스텔, 07:대지</p>
//     * @return
//     */
//    public String getEstateType() {
//        return estateType;
//    }
//
//    /**
//     * 부동산 종류
//     * <p>01:아파트, 02:빌라, 03:단독, 05:상가, 06:오피스텔, 07:대지</p>
//     * @param estate_type
//     */
//    public void setEstateType(String estate_type) {
//        this.estateType = estate_type;
//    }
//
//    /**
//     * 거래 유형
//     * <p>01:매매, 02:전세, 03:월세</p>
//     * @return
//     */
//    public String getDealType() {
//        return dealType;
//    }
//
//    /**
//     * 거래 유형
//     * <p>01:매매, 02:전세, 03:월세</p>
//     * @param deal_type
//     */
//    public void setDealType(String deal_type) {
//        this.dealType = deal_type;
//    }
//
//    /**
//     * 금액
//     * @return
//     */
//    public String getPrice() {
//        return price;
//    }
//
//    /**
//     * 금액
//     * @param price
//     */
//    public void setPrice(String price) {
//        this.price = price;
//    }
}
