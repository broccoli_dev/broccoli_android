package com.nomadconnection.broccoli.data.Remit;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RemitBank extends Result implements Serializable{
    private String companyCode = "";
    private String companyName= "";
    private String haveAccount = "";
    private String possibleSend = "";

    /**
     * 은행사 코드
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 은행사 코드
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 은행사 이름
     * @return
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 은행사 이름
     * @param companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * 계좌존재여부
     * @return
     */
    public String getHaveAccount() {
        return haveAccount;
    }

    /**
     * 계좌존재여부
     * @param haveAccount
     */
    public void setHaveAccount(String haveAccount) {
        this.haveAccount = haveAccount;
    }

    /**
     * 전송가능은행여부
     * @return
     */
    public String getPossibleSend() {
        return possibleSend;
    }

    /**
     * 전송가능은행여부
     * @param possibleSend
     */
    public void setPossibleSend(String possibleSend) {
        this.possibleSend = possibleSend;
    }
}
