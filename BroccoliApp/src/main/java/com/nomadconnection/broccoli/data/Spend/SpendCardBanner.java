package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 4. 19..
 */

public class SpendCardBanner extends Result implements Serializable {

    public int diff;
    public int mileageYn;
    public int recommendYn;

}
