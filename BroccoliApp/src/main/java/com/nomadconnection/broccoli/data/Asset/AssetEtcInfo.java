package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by YelloHyunminJang on 2017. 2. 2..
 */

public class AssetEtcInfo implements Parcelable {

    public int etcId;
    public String type;
    public String description;
    public String quantity;
    public String value;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.etcId);
        dest.writeString(this.type);
        dest.writeString(this.description);
        dest.writeString(this.quantity);
        dest.writeString(this.value);
    }

    public AssetEtcInfo(Parcel in) {
        this.etcId = in.readInt();
        this.type = in.readString();
        this.description = in.readString();
        this.quantity = in.readString();
        this.value = in.readString();
    }

    public static final Creator<AssetEtcInfo> CREATOR = new Creator<AssetEtcInfo>() {
        public AssetEtcInfo createFromParcel(Parcel in) {
            return new AssetEtcInfo(in);
        }
        public AssetEtcInfo[] newArray (int size) {
            return new AssetEtcInfo[size];
        }
    };
}
