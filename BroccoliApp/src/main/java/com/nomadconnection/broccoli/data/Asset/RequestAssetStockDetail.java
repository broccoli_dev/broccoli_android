package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 주식 내역 상세 조회 요청 바디
 */
public class RequestAssetStockDetail {

    @SerializedName("userId")
    private String mAssetStockDetailUserId;

    @SerializedName("stockCode")
    private String mAssetStockDetailStockCode;

    public RequestAssetStockDetail(String _AssetStockDetailUserId, String _AssetStockDetailStockCode){
        this.mAssetStockDetailUserId = _AssetStockDetailUserId;
        this.mAssetStockDetailStockCode = _AssetStockDetailStockCode;
    }

//    private String userId;
//    private String stockCode;
//
//    /**
//     * 사용자아이디
//     * @return
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * 사용자아이디
//     * @param id
//     */
//    public void setUserId(String id) {
//        this.userId = id;
//    }
//
//    /**
//     * 주식코드
//     * @return
//     */
//    public String getStockCode() {
//        return stockCode;
//    }
//
//    /**
//     * 주식코드
//     * @param code
//     */
//    public void setStockCode(String code) {
//        this.stockCode = code;
//    }
}
