package com.nomadconnection.broccoli.data;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoDataOtherMini implements Parcelable {

	public String mTxt1 = null;					// group divider
	public String mTxt2 = null;					// title
	public String mTxt3 = null;					// paytype(01,02)
	public String mTxt4 = null;					// paydate
	public String mTxt5 = null;					// amount
	public String mTxt6 = null;					// expectyn(y:확정, n:예상)
	public String mTxt7 = null;					// calendarid
	public String mTxt8 = null;					// requestcompany=opponent
	public String mTxt9 = null;					// repeattype(반복종류(NO:없음, 1W:1주, 1M:1개월, 3M:3개월, 6M:6개월, 1Y:1년))
	public String mTxt10 = null;					// transcationId -> matchedTag



	public InfoDataOtherMini() {
	}

	public InfoDataOtherMini(String _Txt1, String _Txt2, String _Txt3, String _Txt4, String _Txt5, String _Txt6, String _Txt7, String _Txt8, String _Txt9, String _Txt10) {
		this.mTxt1 = _Txt1;
		this.mTxt2 = _Txt2;
		this.mTxt3 = _Txt3;
		this.mTxt4 = _Txt4;
		this.mTxt5 = _Txt5;
		this.mTxt6 = _Txt6;
		this.mTxt7 = _Txt7;
		this.mTxt8 = _Txt8;
		this.mTxt9 = _Txt9;
		this.mTxt10 = _Txt10;
	}



	public InfoDataOtherMini(Parcel in) {
		this.mTxt1 = in.readString();
		this.mTxt2 = in.readString();
		this.mTxt3 = in.readString();
		this.mTxt4 = in.readString();
		this.mTxt5 = in.readString();
		this.mTxt6 = in.readString();
		this.mTxt7 = in.readString();
		this.mTxt8 = in.readString();
		this.mTxt9 = in.readString();
		this.mTxt10 = in.readString();
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.mTxt1);
		dest.writeString(this.mTxt2);
		dest.writeString(this.mTxt3);
		dest.writeString(this.mTxt4);
		dest.writeString(this.mTxt5);
		dest.writeString(this.mTxt6);
		dest.writeString(this.mTxt7);
		dest.writeString(this.mTxt8);
		dest.writeString(this.mTxt9);
		dest.writeString(this.mTxt10);
	}
	
	public static final Creator<InfoDataOtherMini> CREATOR = new Creator<InfoDataOtherMini>() {
		public InfoDataOtherMini createFromParcel(Parcel in) {
			return new InfoDataOtherMini(in);
		}
		public InfoDataOtherMini[] newArray (int size) {
			return new InfoDataOtherMini[size];
		}
	};

	
}
