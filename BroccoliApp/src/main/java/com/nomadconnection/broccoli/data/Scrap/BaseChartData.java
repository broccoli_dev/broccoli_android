package com.nomadconnection.broccoli.data.Scrap;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class BaseChartData {
    /**
     * 일정, 기준날짜 - X축
     * <p>형태는 사용하는 자산은 yyyyMM, 소비는 yyyyMMdd</p>
     */
    public String date;

    public long dailySum;

    /**
     * 값, 금액 - Y축
     */
    public long sum;
}
