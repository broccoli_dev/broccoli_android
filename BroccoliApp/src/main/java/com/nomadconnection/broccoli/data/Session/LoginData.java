package com.nomadconnection.broccoli.data.Session;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 20..
 */
public class LoginData implements Serializable {
    private String userId;
    private String password;
    private String pinNumber;
    private long dayliId;
    private String dayliToken;
    private int type;
    private String isLogin;
    private String pushKey;
    private String deviceId;
    private String lastLogin;

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String PIN_ERROR = "pin_error";

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * os종류
     * <p>1:android, 2:ios</p>
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * os종류
     * <p>1:android, 2:ios</p>
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * 푸쉬용 키
     * @return
     */
    public String getPushKey() {
        return pushKey;
    }

    /**
     * 푸쉬용 키
     * @param pushKey
     */
    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    public long getDayliId() {
        return dayliId;
    }

    public void setDayliId(long dayliId) {
        this.dayliId = dayliId;
    }

    public String getDayliToken() {
        return dayliToken;
    }

    public void setDayliToken(String dayliToken) {
        this.dayliToken = dayliToken;
    }

    public String isLogin() {
        return isLogin;
    }

    public void setLogin(String loinged) {
        this.isLogin = loinged;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }
}
