package com.nomadconnection.broccoli.data.Spend;

import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 소비 내역 추가/수정
 */
public class EditSpendBudget {
    private String userId;
    private String budgetId;
    private String baseMonth;
    private String totalSum;
    private List<BodySpendBudgetDetail> list;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    /**
     * 예산 아이디
     * <p>0:추가, 1:수정</p>
     * @return
     */
    public String getBudgetId() {
        return budgetId;
    }

    /**
     * 예산 아이디
     * <p>0:추가, 1:수정</p>
     * @param id
     */
    public void setBudgetId(String id) {
        this.budgetId = id;
    }

    /**
     * 예산 총액
     * @return
     */
    public String getTotalSum() {
        return totalSum;
    }

    /**
     * 예산 총액
     * @param totalSum
     */
    public void setTotalSum(String totalSum) {
        this.totalSum = totalSum;
    }

    /**
     * 카테고리 리스트
     * @return
     */
    public List<BodySpendBudgetDetail> getList() {
        return list;
    }

    /**
     * 카테고리 리스트
     * @param list
     */
    public void setList(List<BodySpendBudgetDetail> list) {
        this.list = list;
    }

    /**
     * 기준월
     * @return
     */
    public String getBaseMonth() {
        return baseMonth;
    }

    /**
     * 기준월
     * @param baseMonth
     */
    public void setBaseMonth(String baseMonth) {
        this.baseMonth = baseMonth;
    }
}
