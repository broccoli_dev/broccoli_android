package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 2016. 10. 12..
 */

public class SpendHistoryHeader {

    private String headerDate;
    private String keyDate;
    private int day;

    public String getHeaderDate() {
        return headerDate;
    }

    public void setHeaderDate(String headerDate) {
        this.headerDate = headerDate;
    }

    public String getKeyDate() {
        return keyDate;
    }

    public void setKeyDate(String keyDate) {
        this.keyDate = keyDate;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
