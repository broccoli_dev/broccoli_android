package com.nomadconnection.broccoli.data.Etc;

import com.google.gson.annotations.SerializedName;
import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 기타 메인 조회
 */
public class EtcMainData extends Result implements Serializable{

    @SerializedName("challenge")
    public List<EtcChallengeInfo>       mMainEtcChallengeInfo;                        // 챌린지

    @SerializedName("moneyCalendar")
    public EtcMoneyInfo                 mMainEtcMoneyInfo;                             // 머니 캘린더

    public boolean isComplete() {
        boolean ret = true;
        for (Field field : getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.get(this) == null) {
                    ret = false;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }
}
