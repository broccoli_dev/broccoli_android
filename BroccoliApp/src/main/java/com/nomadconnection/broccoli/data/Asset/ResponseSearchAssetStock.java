package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseSearchAssetStock extends Result implements Parcelable {

    public ResponseSearchAssetStock(String _StockCode, String _StockName){
        mStockCode = _StockCode;
        mStockName = _StockName;
    }

    @SerializedName("stockCode")
    public String mStockCode;

    @SerializedName("stockName")
    public String mStockName;



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mStockCode);
        dest.writeString(this.mStockName);
    }

    public ResponseSearchAssetStock(Parcel in) {
        this.mStockCode = in.readString();
        this.mStockName = in.readString();
    }

    public static final Creator<ResponseSearchAssetStock> CREATOR = new Creator<ResponseSearchAssetStock>() {
        public ResponseSearchAssetStock createFromParcel(Parcel in) {
            return new ResponseSearchAssetStock(in);
        }
        public ResponseSearchAssetStock[] newArray (int size) {
            return new ResponseSearchAssetStock[size];
        }
    };

//    private String stockCode;
//    private String stockName;
//
//    /**
//     * 종목 코드
//     * @return
//     */
//    public String getStockCode() {
//        return stockCode;
//    }
//
//    /**
//     * 종목 코드
//     * @param stock_code
//     */
//    public void setStockCode(String stock_code) {
//        this.stockCode = stock_code;
//    }
//
//    /**
//     * 종목 이름
//     * @return
//     */
//    public String getStockName() {
//        return stockName;
//    }
//
//    /**
//     * 종목 이름
//     * @param stock_name
//     */
//    public void setStockName(String stock_name) {
//        this.stockName = stock_name;
//    }
}
