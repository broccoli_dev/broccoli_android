package com.nomadconnection.broccoli.data;

import com.google.gson.JsonObject;

/**
 * Created by YelloHyunminJang on 2017. 1. 25..
 */

public class CdnAppInfoData {

    public JsonObject host;
    public boolean notice_yn;
    public HonestFundInfo honest_fund;
    public AppVersionInfo version;
    public NoticeInfo notice;

    public class AppVersionInfo {
        public String min_version;
        public String current_version;
        public String server_version;
    }

    public class HonestFundInfo {
        public boolean display;
        public String url_to;
    }

    public class NoticeInfo {
        public String contents;
        public String title;
    }
}
