package com.nomadconnection.broccoli.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataByUserId {
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

//    @SerializedName("userId")
//    public String mUserId;
//
//    public RequestDataByUserId(String _UserId){
//        this.mUserId = _UserId;
//    }
}
