package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 2017. 1. 25..
 */

public class CdnServerAddress {

    /**
     * 브로콜리 메인 API Base Address
     */
    public String broccoli_col;

    /**
     * 브로콜리 Scraping Base Address
     */
    public String broccoli_inq;

    /**
     * 브로콜리 Daylipass Base Address
     */
    public String daylipass;

}
