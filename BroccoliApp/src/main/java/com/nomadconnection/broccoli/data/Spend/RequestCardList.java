package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestCardList {
    private String userId;
    private String companyCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    /**
     * 카드코드
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 카드코드
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
}
