package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataChangeMoney implements Serializable{
    private String userId;
    private String chargeMoney;
    private String sendPW;




    /**
     * 유저아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 유저아이디
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 충전금액
     * @return
     */
    public String getChargeMoney() {
        return chargeMoney;
    }

    /**
     * 충전금액
     * @param chargeMoney
     */
    public void setChargeMoney(String chargeMoney) {
        this.chargeMoney = chargeMoney;
    }

    /**
     * 송금암호
     * @return
     */
    public String getSendPW() {
        return sendPW;
    }

    /**
     * 송금암호
     * @param sendPW
     */
    public void setSendPW(String sendPW) {
        this.sendPW = sendPW;
    }
}
