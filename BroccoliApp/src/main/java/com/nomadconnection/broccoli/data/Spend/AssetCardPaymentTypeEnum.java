package com.nomadconnection.broccoli.data.Spend;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 5. 17..
 */

public enum  AssetCardPaymentTypeEnum implements Serializable {
    @SerializedName("1")
    ONCE,
    @SerializedName("2")
    SPLIT,
    @SerializedName("3")
    ETC,
    @SerializedName("4")
    SHORT_LOAN,
    @SerializedName("5")
    LONG_LOAN,
    @SerializedName("13")
    ONCE_ETC,
    @SerializedName("23")
    SPLIT_ETC

}
