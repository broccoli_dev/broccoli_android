package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.interf.InterfaceSpendBudgetList;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2017. 3. 8..
 */

public class SpendBudgetDetailLookupData extends SpendBudgetDetail implements Serializable, InterfaceSpendBudgetList {

    public long leftBudget;
    public long leftExpense;
    public boolean isUnClassifiedData = false;
    public ArrayList<SpendBudgetDetail> unClassifiedList;

    @Override
    public boolean isGroupCategory() {
        return false;
    }

    @Override
    public boolean isUnclassified() {
        return isUnClassifiedData;
    }

    @Override
    public int getCategoryId() {
        return largeCategoryId;
    }

    @Override
    public long getExpense() {
        return expense;
    }

    @Override
    public long getBudget() {
        return sum;
    }

    @Override
    public long getLeftExpense() {
        return leftExpense;
    }

    @Override
    public long getLeftBudget() {
        return leftBudget;
    }
}
