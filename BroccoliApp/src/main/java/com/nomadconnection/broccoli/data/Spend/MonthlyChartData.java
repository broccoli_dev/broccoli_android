package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 2017. 1. 3..
 */

public class MonthlyChartData extends Result {

    private String expenseMonth;
    private long monthlySum;

    public long getMonthlySum() {
        return monthlySum;
    }

    public String getMonthlyDate() {
        return expenseMonth;
    }
}
