package com.nomadconnection.broccoli.data.Scrap;

/**
 * Created by YelloHyunminJang on 16. 2. 14..
 */
public class Common {
    public static final int TYPE_BANK = 11;
    public static final int TYPE_BANK_DEAL = 12;
    public static final int TYPE_CARD_LIST = 10;
    public static final int TYPE_CARD_APPROVAL = 13;
    public static final int TYPE_CARD_BILL = 14;
    public static final int TYPE_CARD_LIMIT = 15;
    public static final int TYPE_CARD_DUE = 16;
    public static final int TYPE_CASH = 17;
    public static final int TYPE_CARD_LOAN = 18;
}
