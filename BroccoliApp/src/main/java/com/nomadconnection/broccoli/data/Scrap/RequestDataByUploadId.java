package com.nomadconnection.broccoli.data.Scrap;

import com.nomadconnection.broccoli.data.RequestDataByUserId;

/**
 * Created by YelloHyunminJang on 16. 1. 31..
 */
public class RequestDataByUploadId extends RequestDataByUserId {
    private String uploadId;

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }
}
