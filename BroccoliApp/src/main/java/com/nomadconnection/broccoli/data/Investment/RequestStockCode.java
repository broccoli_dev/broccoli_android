package com.nomadconnection.broccoli.data.Investment;

/**
 * Created by YelloHyunminJang on 2016. 11. 16..
 */

public class RequestStockCode {

    private String stockcode;

    public String getStockCode() {
        return stockcode;
    }

    public void setStockCode(String stockcode) {
        this.stockcode = stockcode;
    }
}
