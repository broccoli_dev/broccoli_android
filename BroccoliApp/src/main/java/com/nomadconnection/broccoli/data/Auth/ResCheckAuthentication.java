package com.nomadconnection.broccoli.data.Auth;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 16. 7. 4..
 */
public class ResCheckAuthentication extends Result {

    private String realId;

    public String getRealId() {
        return realId;
    }

    public void setRealId(String realId) {
        this.realId = realId;
    }
}
