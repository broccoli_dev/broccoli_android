package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * 차량 제조사 조회
 */
public class ResponseAssetCarMakerCode implements Parcelable {

    @SerializedName("makerCode")
    public String mAssetCarMakerCode;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetCarMakerCode);
    }

    public ResponseAssetCarMakerCode(Parcel in) {
        this.mAssetCarMakerCode = in.readString();
    }

    public static final Creator<ResponseAssetCarMakerCode> CREATOR = new Creator<ResponseAssetCarMakerCode>() {
        public ResponseAssetCarMakerCode createFromParcel(Parcel in) {
            return new ResponseAssetCarMakerCode(in);
        }
        public ResponseAssetCarMakerCode[] newArray (int size) {
            return new ResponseAssetCarMakerCode[size];
        }
    };

}
