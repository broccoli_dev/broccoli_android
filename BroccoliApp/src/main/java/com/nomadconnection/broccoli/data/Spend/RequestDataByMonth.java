package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataByMonth {
    private String userId;
    private String baseMonth;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    /**
     * 기준월
     * <p>yyyyMM</p>
     * @return
     */
    public String getMonth() {
        return baseMonth;
    }

    /**
     * 기준월
     * <p>yyyyMM</p>
     * @param month
     */
    public void setMonth(String month) {
        this.baseMonth = month;
    }
}
