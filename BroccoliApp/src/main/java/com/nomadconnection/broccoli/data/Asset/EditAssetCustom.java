package com.nomadconnection.broccoli.data.Asset;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 2017. 1. 24..
 */

public class EditAssetCustom extends Result {

    public String userId;
    public int etcId;
    public String type;
    public String description;
    public String quantity;
    public String value;

}
