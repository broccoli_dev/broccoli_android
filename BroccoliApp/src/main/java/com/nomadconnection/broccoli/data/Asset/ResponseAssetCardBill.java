package com.nomadconnection.broccoli.data.Asset;

import java.io.Serializable;

import static com.nomadconnection.broccoli.adapter.AdtExpListViewAssetCredit.CARD_NORMAL;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 카드 청구서 리스트 조회 결과
 */
public class ResponseAssetCardBill implements Serializable {

    public String companyCode;
    public String payDate;
    public long payAmount;
    public boolean estimateYn;
    public String billType;
    public String accountNum;

    public int CardType = CARD_NORMAL;
}
