package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestCardMap {
    private String userId;
    private String cardId;
    private String cardCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    /**
     * 카드 아이디
     * @return
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * 카드 아이디
     * @param cardId
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     * 카드코드
     * @return
     */
    public String getCardCode() {
        return cardCode;
    }

    /**
     * 카드코드
     * @param cardCode
     */
    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }
}
