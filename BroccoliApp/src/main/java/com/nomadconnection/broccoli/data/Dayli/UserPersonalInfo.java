package com.nomadconnection.broccoli.data.Dayli;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 1. 3..
 */

public class UserPersonalInfo extends Result implements Serializable {

    public static final String SINGLE = "0";
    public static final String MARRIED = "1";
    public static final String NOT_SELECT = "2";

    public PersonalInfo user;

    public static class PersonalInfo implements Serializable {
        public String marriedYn;
        public int children;
        public int monthlyIncome;
        public String foreignYn;
        public boolean realnameCertYn;
        public boolean emailCertYn;
        public long userId;
        public String realname;
        /**
         * 성별
         * <p>M:남, F:여</p>
         */
        public String gender;
        public String email;
        public String mobileNumber;
        public String telNumber;
        public String emailCertTo;
        public String emailCertKey;
        public String socialNumber;
        public String birthDate;
        public String profileImageUrl;
    }

}
