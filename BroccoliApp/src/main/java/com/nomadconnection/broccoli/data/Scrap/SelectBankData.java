package com.nomadconnection.broccoli.data.Scrap;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class SelectBankData implements Serializable {

    public static final int NONE = 0;
    public static final int SELECTED = 1;
    public static final int COMPLETE = 2;
    public static final int RENEWAL = 3;
    public static final int LOGIN_FINISH = 4;
    public static final int CERT_FINISH = 5;

    public static final String TYPE_BANK = "bank";
    public static final String TYPE_CARD = "card";
    public static final String TYPE_CASH = "cash";

    private int status = NONE;
    private String type = TYPE_BANK;
    private String name;
    private String code;
    private String typeName;
    private boolean isBeta = false;
    private String validDate;
    private String bankStatus;
    private boolean isLoading = false;
    private String categoryGroup;
    private int nameId;
    private String loginMethod;
    private String tempId;
    private String tempPwd;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public boolean isBeta() {
        return isBeta;
    }

    public void setBeta(boolean isBeta) {
        this.isBeta = isBeta;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getBankStatus() {
        return bankStatus;
    }

    public void setBankStatus(String bankStatus) {
        this.bankStatus = bankStatus;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    public void setCategoryGroup(String group) {
        categoryGroup = group;
    }

    public String getCategoryGroup() {
        return categoryGroup;
    }

    public void setNameId(int nameId) {
        this.nameId = nameId;
    }

    public int getNameId() {
        return nameId;
    }

    public String getLoginMethod() {
        return loginMethod;
    }

    public void setLoginMethod(String loginMethod) {
        this.loginMethod = loginMethod;
    }

    public void setId(String id) {
        this.tempId = id;
    }

    public String getId() {
        return tempId;
    }

    public void setPwd(String pwd) {
        this.tempPwd = pwd;
    }

    public String getPwd() {
        return tempPwd;
    }

    public void clear() {
        tempPwd = null;
        tempId = null;
        loginMethod = null;
        validDate = null;
    }
}
