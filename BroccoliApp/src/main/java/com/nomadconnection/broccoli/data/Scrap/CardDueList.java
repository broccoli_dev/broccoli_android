package com.nomadconnection.broccoli.data.Scrap;

import com.google.gson.JsonObject;

/**
 * Created by YelloHyunminJang on 16. 2. 17..
 * 카드 결제예정 내역
 */
public class CardDueList {

    private String USERID;
    private String CMPNYCODE;
    private int TYPE;
    private String UPLOADID;
    private String ESTDATE;
    private String ESTAMT;
    private JsonObject result;

    /**
     * 데이터 종류
     * <p>데이터 종류 (1:은행계좌, 2:은행거래, 3:카드승인, 4:카드청구, 5:카드한도, 6:카드결제예정 7:현금영수증)</p>
     * @return
     */
    public int getTYPE() {
        return TYPE;
    }

    /**
     * 데이터 종류
     * <p>데이터 종류 (1:은행계좌, 2:은행거래, 3:카드승인, 4:카드청구, 5:카드한도, 6:카드결제예정 7:현금영수증)</p>
     * @param TYPE
     */
    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public String getCMPNYCODE() {
        return CMPNYCODE;
    }

    public void setCMPNYCODE(String CMPNYCODE) {
        this.CMPNYCODE = CMPNYCODE;
    }

    public String getUPLOADID() {
        return UPLOADID;
    }

    public void setUPLOADID(String UPLOADID) {
        this.UPLOADID = UPLOADID;
    }

    public String getESTDATE() {
        return ESTDATE;
    }

    public void setESTDATE(String ESTDATE) {
        this.ESTDATE = ESTDATE;
    }

    public String getESTAMT() {
        return ESTAMT;
    }

    public void setESTAMT(String ESTAMT) {
        this.ESTAMT = ESTAMT;
    }

    public JsonObject getResult() {
        return result;
    }

    public void setResult(JsonObject result) {
        this.result = result;
    }
}
