package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class SpendCategoryInfo {
    private String categoryId;
    private String sum;

    /**
     * 카테고리 코드
     * @return
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 카테고리 코드
     * @param category_code
     */
    public void setCategoryId(String category_code) {
        this.categoryId = category_code;
    }

    /**
     * 잔액
     * @return
     */
    public String getSum() {
        return sum;
    }

    /**
     * 잔액
     * @param sum
     */
    public void setSum(String sum) {
        this.sum = sum;
    }
}
