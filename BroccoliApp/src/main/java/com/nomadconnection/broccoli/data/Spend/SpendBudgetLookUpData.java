package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.interf.InterfaceSpendBudgetList;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2017. 3. 8..
 */

public class SpendBudgetLookUpData extends SpendBudgetCategory implements Serializable, InterfaceSpendBudgetList {

    public long leftBudget;
    public long leftExpense;
    public boolean isUnClassifiedData = false;
    public ArrayList<SpendBudgetDetail> detailList;

    @Override
    public boolean isGroupCategory() {
        return true;
    }

    @Override
    public boolean isUnclassified() {
        return isUnClassifiedData;
    }

    @Override
    public int getCategoryId() {
        return categoryGroup;
    }

    @Override
    public long getExpense() {
        return expense;
    }

    @Override
    public long getBudget() {
        return sum;
    }

    @Override
    public long getLeftExpense() {
        return leftExpense;
    }

    @Override
    public long getLeftBudget() {
        return leftBudget;
    }
}
