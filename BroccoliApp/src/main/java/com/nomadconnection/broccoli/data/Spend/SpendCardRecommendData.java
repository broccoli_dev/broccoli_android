package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;
import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 3. 22..
 */

public class SpendCardRecommendData implements Serializable {

    public long cardId;
    public long averageTotalSum;
    public long averageTotalDiscount;
    public long averageTotalPoint;
    public long averageExpense;
    public long averageTotal;
    public long averageDiscount;
    public long averageMileage;
    public long averagePoint;
    public SpendCardClassifiedData card;
    public List<SpendCardDetailAnnualFee> cardDetailList;
    public SpendCardCompanyDetail cardCompany;

}
