package com.nomadconnection.broccoli.data.Remit;

import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 7. 14..
 */
public class ResponseRemitStandByReceivedMoney {

    private String result;
    private String msg;
    private List<ReceiveMoneyData> rcvMoneyWaitList;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ReceiveMoneyData> getRcvMoneyWaitList() {
        return rcvMoneyWaitList;
    }

    public void setRcvMoneyWaitList(List<ReceiveMoneyData> rcvMoneyWaitList) {
        this.rcvMoneyWaitList = rcvMoneyWaitList;
    }
}
