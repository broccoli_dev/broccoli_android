package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 16. 7. 18..
 */
public class RequestMainData extends RequestDataByUserId {

    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
