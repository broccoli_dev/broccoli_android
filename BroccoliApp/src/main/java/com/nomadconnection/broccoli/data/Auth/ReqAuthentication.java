package com.nomadconnection.broccoli.data.Auth;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class ReqAuthentication {

    public static final String PHONE_KIND_SKT = "01";
    public static final String PHONE_KIND_KT = "02";
    public static final String PHONE_KIND_LG = "03";
    public static final String PHONE_KIND_SKT_AL = "04";
    public static final String PHONE_KIND_KT_AL = "05";
    public static final String PHONE_KIND_LG_AL = "06";

    private String userName;
    private String birthDate;
    private String gender;
    private String phoneKind;
    private String phoneNo;
    private String deviceId;

    public String getName() {
        return userName;
    }

    public void setName(String name) {
        this.userName = name;
    }

    public String getBirth() {
        return birthDate;
    }

    public void setBirth(String birth) {
        this.birthDate = birth;
    }

    /**
     * 성별 - 주민등록 상의 숫자로 입력
     * @return
     */
    public String getGender() {
        return gender;
    }

    /**
     * 성별 - 주민등록 상의 숫자로 입력
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneKind() {
        return phoneKind;
    }

    /**
     * 01 : SKT
     * 02 : KT
     * 03 : LG
     * 04 : SKT알뜰폰
     * 05 : KT알뜰폰
     * 06 : LG알뜰폰
     *
     * @param phoneKind
     */
    public void setPhoneKind(String phoneKind) {
        this.phoneKind = phoneKind;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
