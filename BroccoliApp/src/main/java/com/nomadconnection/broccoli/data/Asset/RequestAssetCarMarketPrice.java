package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * 차량 연식 조회
 */
public class RequestAssetCarMarketPrice {

    @SerializedName("userId")
    public String mUserId;

    @SerializedName("makerCode")
    public String mMakerCode;

    @SerializedName("carCode")
    public String mCarCode;

    @SerializedName("subCode")
    public String mSubCode;

    @SerializedName("madeYear")
    public String mMadeYear;


    public RequestAssetCarMarketPrice(String _UserId, String _MakerCode, String _CarCode, String _SubCode, String _MadeYear){
        this.mUserId = _UserId;
        this.mMakerCode = _MakerCode;
        this.mCarCode = _CarCode;
        this.mSubCode = _SubCode;
        this.mMadeYear = _MadeYear;
    }

}
