package com.nomadconnection.broccoli.data;

import java.io.Serializable;


public class InfoDataMain1 implements Serializable {
	
	public String mTxt1 = null;			// title
	public String mTxt2 = null;			// 등록여부
	public String mTxt3 = null;			// 금액         
	public String mTxt4 = null;			// 업데이트 날자    
	public String mTxt5 = null;			// 손익?        
	public String mTxt6 = null;			// 부동산?
	public String mTxt7 = null;			// 차량?
	public String mTxt8 = null;			// count1
	public String mTxt9 = null;			// count2
	public String mSubInfo1 = null;		// sub info 1

	private Object mData;

	
	public InfoDataMain1() {
	}
	
	public InfoDataMain1(String _Txt1, String _Txt2, String _Txt3, String _Txt4, String _Txt5, String _Txt6, String _Txt7, String _Txt8, String _Txt9, String _SubInfo1) {
		this.mTxt1 = _Txt1;
		this.mTxt2 = _Txt2;
		this.mTxt3 = _Txt3;
		this.mTxt4 = _Txt4;
		this.mTxt5 = _Txt5;
		this.mTxt6 = _Txt6;
		this.mTxt7 = _Txt7;
		this.mTxt8 = _Txt8;
		this.mTxt9 = _Txt9;
		this.mSubInfo1 = _SubInfo1;
	}


	public Object getData() {
		return mData;
	}

	public void setData(Object data) {
		this.mData = data;
	}
}
