package com.nomadconnection.broccoli.data.Dayli;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 2016. 12. 27..
 */

public class ResponseJoin extends Result {

    private long userId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
