package com.nomadconnection.broccoli.data;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoDataSettingNotice implements Parcelable {
	
	public String mTxt1 = null;
	public String mTxt2 = null;
	public String mTxt3 = null;
	
	
	public InfoDataSettingNotice() {
	}
	
	public InfoDataSettingNotice(String _Txt1, String _Txt2, String _Txt3) {
		this.mTxt1 = _Txt1;
		this.mTxt2 = _Txt2;
		this.mTxt3 = _Txt3;
	}
	
	
	
	public InfoDataSettingNotice(Parcel in) {
		this.mTxt1 = in.readString();
		this.mTxt2 = in.readString();
		this.mTxt3 = in.readString();
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.mTxt1);
		dest.writeString(this.mTxt2);
		dest.writeString(this.mTxt3);
	}
	
	public static final Creator<InfoDataSettingNotice> CREATOR = new Creator<InfoDataSettingNotice>() {
		public InfoDataSettingNotice createFromParcel(Parcel in) {
			return new InfoDataSettingNotice(in);
		}
		public InfoDataSettingNotice[] newArray (int size) {
			return new InfoDataSettingNotice[size];
		}
	};

	
}
