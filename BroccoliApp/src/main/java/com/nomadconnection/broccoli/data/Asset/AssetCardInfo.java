package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class AssetCardInfo implements Serializable {

    @SerializedName("count")
    public int mAssetCardCount;                                // 카드 개수

    @SerializedName("sum")
    public long mAssetCardSum;                               // 금액

    @SerializedName("list")
    public List<HashMap<String, Long>> mAssetCardBillList;
}
