package com.nomadconnection.broccoli.data.Scrap;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 7. 6..
 */
public class UploadStatusCard implements Serializable {

    private String companyCode;
    /**
     * 승인 내역 최종 업로드 성공 날짜(yyyyMMdd)
     */
    private String approval;
    /**
     * 청구서 최종 월(yyyyMM)
     */
    private String bill;

}
