package com.nomadconnection.broccoli.data.Spend;

import com.nomadconnection.broccoli.constant.BenefitInternalType;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardBenefitInternalBody implements Serializable {

    public Float benefitRate = 0f;
    public Float benefitAmount = 0f;
    public Integer benefitAmountUnit = 0;
    public Integer benefitLimit = 0;
    public int affiliateYn;
    public boolean isDefaultBenefitExist = false;
    public boolean isMultipleData = false;
    public BenefitInternalType type = BenefitInternalType.NONE;
    public String defaultBenefitContent;
}
