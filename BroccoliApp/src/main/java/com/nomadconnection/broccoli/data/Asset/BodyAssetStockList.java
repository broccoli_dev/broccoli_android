package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 주식 내역 상세 조회 바디
 */
public class BodyAssetStockList implements Serializable {

    @SerializedName("transId")
    public String mBodyAssetStockDetailTransId;

    @SerializedName("transType")
    public int mBodyAssetStockDetailTransType;

    @SerializedName("transDate")
    public String mBodyAssetStockDetailTransDate;

    @SerializedName("price")
    public String mBodyAssetStockDetailPrice;

    @SerializedName("amount")
    public String mBodyAssetStockDetailAmount;

    @SerializedName("sum")
    public String mBodyAssetStockDetailSum;


    public BodyAssetStockList(Parcel in) {
        this.mBodyAssetStockDetailTransId = in.readString();
        this.mBodyAssetStockDetailTransType = in.readInt();
        this.mBodyAssetStockDetailTransDate = in.readString();
        this.mBodyAssetStockDetailPrice = in.readString();
        this.mBodyAssetStockDetailAmount = in.readString();
        this.mBodyAssetStockDetailSum = in.readString();
    }

//    private String transId;
//    private int transType;
//    private String transDate;
//    private String price;
//    private String amount;
//    private String sum;
//
//    /**
//     * 거래 아이디
//     * @return
//     */
//    public String getTransId() {
//        return transId;
//    }
//
//    /**
//     * 거래 아이디
//     * @param id
//     */
//    public void setTransId(String id) {
//        transId = id;
//    }
//
//    /**
//     * 거래 종류
//     * <p>1:매수, 2:매도</p>
//     * <p>0.51 문서에는 매수, 매도가 뒤바뀌어 들어가 있음</p>
//     * @return
//     */
//    public int getTransType() {
//        return transType;
//    }
//
//    /**
//     * 거래 종류
//     * <p>1:매수, 2:매도</p>
//     * <p>0.51 문서에는 매수, 매도가 뒤바뀌어 들어가 있음</p>
//     * @param trans_type
//     */
//    public void setTransType(int trans_type) {
//        this.transType = trans_type;
//    }
//
//    /**
//     * 거래일
//     * <P>yyyyMMdd</P>
//     * @return
//     */
//    public String getTransDate() {
//        return transDate;
//    }
//
//    /**
//     * 거래일
//     * <P>yyyyMMdd</P>
//     * @param trans_date
//     */
//    public void setTransDate(String trans_date) {
//        this.transDate = trans_date;
//    }
//
//    /**
//     * 가격 - 개당 가격
//     * @return
//     */
//    public String getPrice() {
//        return price;
//    }
//
//    /**
//     * 가격 - 개당 가격
//     * @param price
//     */
//    public void setPrice(String price) {
//        this.price = price;
//    }
//
//    /**
//     * 수량
//     * @return
//     */
//    public String getAmount() {
//        return amount;
//    }
//
//    /**
//     * 수량
//     * @param amount
//     */
//    public void setAmount(String amount) {
//        this.amount = amount;
//    }
//
//    /**
//     * 금액
//     * @return
//     */
//    public String getSum() {
//        return sum;
//    }
//
//    /**
//     * 금액
//     * @param sum
//     */
//    public void setSum(String sum) {
//        this.sum = sum;
//    }
}
