package com.nomadconnection.broccoli.data.Demo;

/**
 * Created by YelloHyunminJang on 2017. 4. 3..
 */

public class TrialEtc {

    public int etcId;
    public String month;
    public String challengeTitle;
    public long challengeBalance;
    public long challengeSum;
    public String firstCalendarName;
    public long firstCalendarSum;
    public String secondCalendarName;
    public long secondCalendarSum;
    public String thirdCalendarName;
    public long thirdCalendarSum;

}
