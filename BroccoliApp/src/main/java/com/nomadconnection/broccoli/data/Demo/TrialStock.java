package com.nomadconnection.broccoli.data.Demo;

import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 4. 3..
 */

public class TrialStock {

    public int stockId;
    public String month;
    public long value;
    public float stockCount;
    public List<TrialStockDaily> dailyList;

}
