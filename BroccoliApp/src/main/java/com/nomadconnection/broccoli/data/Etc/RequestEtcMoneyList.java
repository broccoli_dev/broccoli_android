package com.nomadconnection.broccoli.data.Etc;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 은행거래 내역 상세 조회 요청용 바디
 */
public class RequestEtcMoneyList {

    @SerializedName("userId")
    public String mUserId;

    @SerializedName("date")
    public String mDate;

    public RequestEtcMoneyList(String _UserId, String _Date){
        this.mUserId = _UserId;
        this.mDate = _Date;
    }
}
