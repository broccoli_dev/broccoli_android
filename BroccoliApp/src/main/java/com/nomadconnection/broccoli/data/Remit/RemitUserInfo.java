package com.nomadconnection.broccoli.data.Remit;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RemitUserInfo extends RemitResult implements Serializable{
    private String hostName;
    private String instanceId;
    private String sendTime;
    private String randValue;
    private String phoneNo;


    /**
     * 유저이름
     * @return
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * 유저이름
     * @param hostName
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * 임시아이디
     * @return
     */
    public String getInstanceId() {
        return instanceId;
    }

    /**
     * 임시아이디
     * @param instanceId
     */
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    /**
     * 전송시간
     * @return
     */
    public String getSendTime() {
        return sendTime;
    }

    /**
     * 전송시간
     * @param sendTime
     */
    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    /**
     * 인증번호
     * @return
     */
    public String getRandValue() {
        return randValue;
    }

    /**
     * 인증번호
     * @param randValue
     */
    public void setRandValue(String randValue) {
        this.randValue = randValue;
    }

    /**
     * 폰번호
     * @return
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * 폰번호
     * @param phoneNo
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
