package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class SpendCardDetailBenefitBody implements Serializable {

    public Float benefitRate;
    public Float benefitAmount;
    public Integer benefitAmountUnit;
    public Integer benefitLimit;

}
