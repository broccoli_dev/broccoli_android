package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 2016. 11. 2..
 */

public class MemoData {

    private String expenseId;
    private String userId;
    private String memo;

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
