package com.nomadconnection.broccoli.data.Session;

/**
 * Created by YelloHyunminJang on 2016. 10. 17..
 */

public class UserDeviceInfo {

    private String deviceId;
    private int type;
    private String pushKey;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPushKey() {
        return pushKey;
    }

    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }
}
