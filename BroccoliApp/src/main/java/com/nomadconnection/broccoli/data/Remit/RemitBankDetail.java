package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RemitBankDetail extends RemitResult implements Serializable{
    private String companyCode = "";
    private String companyName= "";
    private String accountNo = "";
    private String possibleSend = "";
    private String accountID = "";

    /**
     * 은행사 코드
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 은행사 코드
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 은행사 이름
     * @return
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * 은행사 이름
     * @param companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * 계좌번호
     * @return
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * 계좌번호
     * @param accountNo
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * 전송가능은행여부
     * @return
     */
    public String getPossibleSend() {
        return possibleSend;
    }

    /**
     * 전송가능은행여부
     * @param possibleSend
     */
    public void setPossibleSend(String possibleSend) {
        this.possibleSend = possibleSend;
    }

    /**
     * 계좌아이디 (브로콜리계좌아이디)
     * @return
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * 계좌아이디 (브로콜리계좌아이디)
     * @param accountID
     */
    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }
}
