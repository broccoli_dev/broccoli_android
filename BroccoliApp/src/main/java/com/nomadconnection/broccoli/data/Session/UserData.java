package com.nomadconnection.broccoli.data.Session;

/**
 * Created by YelloHyunminJang on 2016. 12. 21..
 */

public class UserData {

    public static final int ANDROID_OS = 1;

    private long dpId;
    private String dpAccessToken;
    private String deviceId;
    private int osType;
    private String pushKey;
    private String userId;
    private String signedRequest;
    private String userYn;
    private String ci;
    private String useYn;
    private String adId;

    public long getDpId() {
        return dpId;
    }

    public void setDpId(long dpId) {
        this.dpId = dpId;
    }

    public String getDpAccessToken() {
        return dpAccessToken;
    }

    public void setDpAccessToken(String dpAccessToken) {
        this.dpAccessToken = dpAccessToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * os종류
     * <p>1:android, 2:ios</p>
     * @return
     */
    public int getOsType() {
        return osType;
    }

    /**
     * os종류
     * <p>1:android, 2:ios</p>
     * @return
     */
    public void setOsType(int osType) {
        this.osType = osType;
    }

    public String getPushKey() {
        return pushKey;
    }

    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return signedRequest;
    }

    public void setPassword(String password) {
        this.signedRequest = password;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getAdId() {
        return adId;
    }
}
