package com.nomadconnection.broccoli.data.Session;

/**
 * Created by YelloHyunminJang on 16. 7. 7..
 */
public class JoinData {

    public static final String SUCCESS_EXIST_USER = "502";

    private String realId;
    private String password;
    private String deviceId;
    private String pushYn;
    private String pushKey;
    private String yellopassId;

    public String getRealId() {
        return realId;
    }

    public void setRealId(String realId) {
        this.realId = realId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPushYn() {
        return pushYn;
    }

    public void setPushYn(String pushYn) {
        this.pushYn = pushYn;
    }

    public String getPushKey() {
        return pushKey;
    }

    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }

    public String getYellopassId() {
        return yellopassId;
    }

    public void setYellopassId(String yellopassId) {
        this.yellopassId = yellopassId;
    }
}
