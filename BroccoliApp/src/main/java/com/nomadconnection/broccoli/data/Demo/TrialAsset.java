package com.nomadconnection.broccoli.data.Demo;

/**
 * Created by YelloHyunminJang on 2017. 4. 3..
 */

public class TrialAsset {

    public int assetId;
    public String month;
    public long deposit;
    public float depositCount;
    public long card;
    public float cardCount;
    public long loan;
    public long realEstate;
    public long car;

}
