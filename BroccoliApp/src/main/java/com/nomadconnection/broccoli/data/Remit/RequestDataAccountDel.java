package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataAccountDel implements Serializable{
    private String userId = "";
    private String accountId = "";

    /**
     * 유저아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 유저아이디
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 계좌아이디
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌아이디
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
