package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * 차량 세부차명 조회
 */
public class ResponseAssetCarSubCode implements Parcelable {

    @SerializedName("subCode")
    public String mAssetCarSubCode;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetCarSubCode);
    }

    public ResponseAssetCarSubCode(Parcel in) {
        this.mAssetCarSubCode = in.readString();
    }

    public static final Creator<ResponseAssetCarSubCode> CREATOR = new Creator<ResponseAssetCarSubCode>() {
        public ResponseAssetCarSubCode createFromParcel(Parcel in) {
            return new ResponseAssetCarSubCode(in);
        }
        public ResponseAssetCarSubCode[] newArray (int size) {
            return new ResponseAssetCarSubCode[size];
        }
    };

}
