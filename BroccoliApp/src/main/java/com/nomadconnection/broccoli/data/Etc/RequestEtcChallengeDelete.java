package com.nomadconnection.broccoli.data.Etc;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 챌린지 계좌 조회
 */
public class RequestEtcChallengeDelete {

    private String userId;
    private String challengeId;


    /**
     * 사용자아이디
     *
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 사용자아이디
     *
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 챌린지 아이디
     *
     * @return
     */
    public String getChallengeId() {
        return challengeId;
    }

    /**
     * 챌린지 아이디
     *
     * @param challengeId
     */
    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

}
