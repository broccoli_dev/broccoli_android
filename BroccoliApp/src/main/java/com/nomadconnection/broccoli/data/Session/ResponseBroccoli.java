package com.nomadconnection.broccoli.data.Session;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 2016. 12. 21..
 */

public class ResponseBroccoli extends Result {

    public String userId;
    public String signedRequest;
    public long dpId;
    public String dpAccessToken;
    public int scrapingDays;

}
