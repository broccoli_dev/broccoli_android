package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class AssetBankInfo implements Parcelable, Cloneable {

    @SerializedName("accountId")
    public int mAssetBankID;                                 // 계좌 아이디

    @SerializedName("companyCode")
    public String mAssetBankBankCode;                        // 은행 코드

    @SerializedName("accountNum")
    public String mAssetBankAccountNumber;                   // 계좌 번호

    @SerializedName("accountName")
    public String mAssetBankAccountName;                     // 계좌 이름

    @SerializedName("afterValue")
    public String mAssetBankSum;                             // 잔액

    @SerializedName("lastTransDate")
    public String mAssetBankLastTransDate;                   // 최종거래일

    @SerializedName("lastTransOpponent")
    public String mAssetBankLastTransOpponent;               // 거래처

    @SerializedName("lastTransSum")
    public String mAssetBankAfterValue;                       // 증가, 감소

    @SerializedName("activeYn")
    public String mAssetBankActiveYn;                          // 계좌 활성 여부



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mAssetBankID);
        dest.writeString(this.mAssetBankBankCode);
        dest.writeString(this.mAssetBankAccountNumber);
        dest.writeString(this.mAssetBankAccountName);
        dest.writeString(this.mAssetBankSum);
        dest.writeString(this.mAssetBankLastTransDate);
        dest.writeString(this.mAssetBankLastTransOpponent);
        dest.writeString(this.mAssetBankAfterValue);
        dest.writeString(this.mAssetBankActiveYn);
    }

    public AssetBankInfo(Parcel in) {
        this.mAssetBankID = in.readInt();
        this.mAssetBankBankCode = in.readString();
        this.mAssetBankAccountNumber = in.readString();
        this.mAssetBankAccountName = in.readString();
        this.mAssetBankSum = in.readString();
        this.mAssetBankLastTransDate = in.readString();
        this.mAssetBankLastTransOpponent = in.readString();
        this.mAssetBankAfterValue = in.readString();
        this.mAssetBankActiveYn = in.readString();
    }

    public static final Creator<AssetBankInfo> CREATOR = new Creator<AssetBankInfo>() {
        public AssetBankInfo createFromParcel(Parcel in) {
            return new AssetBankInfo(in);
        }
        public AssetBankInfo[] newArray (int size) {
            return new AssetBankInfo[size];
        }
    };

    public boolean isChecked() {
        boolean checked = false;

        if (mAssetBankActiveYn != null && mAssetBankActiveYn.equalsIgnoreCase("Y")) {
            checked = true;
        }

        return checked;
    }

    public void toggleActive(boolean check) {
        if (mAssetBankActiveYn != null) {
            if (check) {
                mAssetBankActiveYn = "Y";
            } else {
                mAssetBankActiveYn = "N";
            }
        }
    }

    @Override
    public AssetBankInfo clone() throws CloneNotSupportedException {
        return (AssetBankInfo) super.clone();
    }

    //    @SerializedName("accountId")
//    private String mID;
//    @SerializedName("companyCode")
//    private String mBankCode;
//    @SerializedName("accountNum")
//    private String mAccountNumber;
//    @SerializedName("accountName")
//    private String mAccountName;
//    @SerializedName("afterValue")
//    private String mSum;
//    @SerializedName("lastTransDate")
//    private String mLastTransDate;
//    @SerializedName("lastTransOpponent")
//    private String mLastTransOpponent;
//    @SerializedName("lastTransSum")
//    private String mAfterValue;
//
//    /**
//     * 계좌 아이디
//     * @return
//     */
//    public String getID() {
//        return mID;
//    }
//
//    /**
//     * 계좌 아이디
//     * @param mID
//     */
//    public void setID(String mID) {
//        this.mID = mID;
//    }
//
//    /**
//     * 은행 코드
//     * <p>참조 {@link com.nomad.broccolismartaib.data.BankCode}</p>
//     * @return
//     */
//    public String getBankCode() {
//        return mBankCode;
//    }
//
//    /**
//     * 은행 코드
//     * <p>참조 {@link com.nomad.broccolismartaib.data.BankCode}</p>
//     * @param mBankCode
//     */
//    public void setBankCode(String mBankCode) {
//        this.mBankCode = mBankCode;
//    }
//
//    /**
//     * 계좌 번호
//     * @return
//     */
//    public String getAccountNumber() {
//        return mAccountNumber;
//    }
//
//    /**
//     * 계좌 번호
//     * @param mAccountNumber
//     */
//    public void setAccountNumber(String mAccountNumber) {
//        this.mAccountNumber = mAccountNumber;
//    }
//
//    /**
//     * 계좌 이름
//     * @return
//     */
//    public String getAccountName() {
//        return mAccountName;
//    }
//
//    /**
//     * 계좌 이름
//     * @param mAccountName
//     */
//    public void setAccountName(String mAccountName) {
//        this.mAccountName = mAccountName;
//    }
//
//    /**
//     * 잔액
//     * @return
//     */
//    public String getSum() {
//        return mSum;
//    }
//
//    /**
//     * 잔액
//     * @param mSum
//     */
//    public void setSum(String mSum) {
//        this.mSum = mSum;
//    }
//
//    /**
//     * 최종거래일
//     * <p>ex) yyyyMMddHHmmss</p>
//     * @return
//     */
//    public String getLastTransDate() {
//        return mLastTransDate;
//    }
//
//    /**
//     * 최종거래일
//     * <p>ex) yyyyMMddHHmmss</p>
//     * @param mLastTransDate
//     */
//    public void setLastTransDate(String mLastTransDate) {
//        this.mLastTransDate = mLastTransDate;
//    }
//
//    /**
//     * 거래처
//     * @return
//     */
//    public String getLastTransOpponent() {
//        return mLastTransOpponent;
//    }
//
//    /**
//     * 거래처
//     * @param mLastTransOpponent
//     */
//    public void setLastTransOpponent(String mLastTransOpponent) {
//        this.mLastTransOpponent = mLastTransOpponent;
//    }
//
//    /**
//     * 최종 거래액
//     * @return
//     */
//    public String getLastTransSum() {
//        return mAfterValue;
//    }
//
//    /**
//     * 최종 거래액
//     * @param mVariation
//     */
//    public void setLastTransSum(String mVariation) {
//        this.mAfterValue = mVariation;
//    }
}
