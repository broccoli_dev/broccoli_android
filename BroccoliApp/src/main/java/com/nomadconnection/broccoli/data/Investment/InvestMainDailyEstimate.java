package com.nomadconnection.broccoli.data.Investment;

/**
 * Created by YelloHyunminJang on 16. 2. 5..
 */
public class InvestMainDailyEstimate {
    private String date;
    private String estimate;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }
}
