package com.nomadconnection.broccoli.data.Session;

/**
 * Created by YelloHyunminJang on 16. 7. 7..
 */
public class ModifyData extends JoinData {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
