package com.nomadconnection.broccoli.data.Dayli;

import com.nomadconnection.broccoli.data.Result;

/**
 * Created by YelloHyunminJang on 2016. 12. 28..
 */

public class ResponseAuthRealName extends Result {

    private int reqCount;
    private String sendTime;

    public int getReqCount() {
        return reqCount;
    }

    public String getSendTime() {
        return sendTime;
    }
}
