package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RemitContent implements Serializable {
    private String hostName;
    private String telNo;

    /**
     * 전화번호부 이름
     *
     * @return
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * 전화번호부 이름
     *
     * @param hostName
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * 전화번호부 번호
     *
     * @return
     */
    public String getTelNo() {
        return telNo;
    }

    /**
     * 전화번호부 번호
     *
     * @param telNo
     */
    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }
}
