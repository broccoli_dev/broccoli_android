package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestSpendCategoryDetail extends RequestDataByDate {
    private String categoryId;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String category_code) {
        this.categoryId = category_code;
    }
}
