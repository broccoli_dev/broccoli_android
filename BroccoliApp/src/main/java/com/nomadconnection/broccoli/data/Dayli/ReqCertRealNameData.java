package com.nomadconnection.broccoli.data.Dayli;

/**
 * Created by YelloHyunminJang on 2016. 12. 27..
 */

public class ReqCertRealNameData {

    private String smsCertNo;
    private String deviceId;

    public void setSmsCertNo(String smsCertNo) {
        this.smsCertNo = smsCertNo;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
