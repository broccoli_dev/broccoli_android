package com.nomadconnection.broccoli.data.Scrap;

import com.google.gson.JsonObject;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 * 은행 계좌 리스트
 */
public class BankAccountList {
    private String USERID;
    private String CMPNYCODE;
    private int TYPE;
    private String UPLOADID;
    private String ACCTNM;
    private JsonObject result;

    /**
     * 사용자 아이디
     * @return
     */
    public String getUSERID() {
        return USERID;
    }

    /**
     * 사용자 아이디
     * @param USERID
     */
    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    /**
     * 업체코드
     * @return
     */
    public String getCMPNYCODE() {
        return CMPNYCODE;
    }

    /**
     * 업체코드
     * @param CMPNYCODE
     */
    public void setCMPNYCODE(String CMPNYCODE) {
        this.CMPNYCODE = CMPNYCODE;
    }



    /**
     * 데이터 종류
     * <p>데이터 종류 (11:은행계좌, 12:은행거래, 3:카드승인, 4:카드청구, 5:현금영수증)</p>
     * @return
     */
    public int getTYPE() {
        return TYPE;
    }

    /**
     * 데이터 종류
     * <p>데이터 종류 (11:은행계좌, 12:은행거래, 3:카드승인, 4:카드청구, 5:현금영수증)</p>
     * @param TYPE
     */
    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getUPLOADID() {
        return UPLOADID;
    }

    public void setUPLOADID(String UPLOADID) {
        this.UPLOADID = UPLOADID;
    }

    public JsonObject getResult() {
        return result;
    }

    public void setResult(JsonObject result) {
        this.result = result;
    }
}
