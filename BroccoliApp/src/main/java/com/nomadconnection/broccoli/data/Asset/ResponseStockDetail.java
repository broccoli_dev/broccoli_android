package com.nomadconnection.broccoli.data.Asset;

import com.nomadconnection.broccoli.data.Investment.InvestNewsyStockData;
import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2016. 11. 18..
 */

public class ResponseStockDetail extends Result implements Serializable{

    private BodyAssetStockDetail stock;
    private InvestNewsyStockData newsystock;

    public BodyAssetStockDetail getStock() {
        return stock;
    }

    public InvestNewsyStockData getNewsystock() {
        return newsystock;
    }
}
