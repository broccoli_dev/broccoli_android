package com.nomadconnection.broccoli.data.Dayli;

/**
 * Created by YelloHyunminJang on 2016. 12. 27..
 */

public class ReqAuthRealNameData {

    private String userName;
    private String birthDate;
    private String genderCode;
    private String phoneKind;
    private String phoneNo;
    private String rqstCausCd;
    private String deviceId;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    public void setRqstCausCd(String rqstCausCd) {
        this.rqstCausCd = rqstCausCd;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setPhoneKind(String phoneKind) {
        this.phoneKind = phoneKind;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getUserName() {
        return userName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }
}
