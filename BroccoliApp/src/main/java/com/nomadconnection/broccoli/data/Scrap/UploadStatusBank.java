package com.nomadconnection.broccoli.data.Scrap;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 7. 6..
 */
public class UploadStatusBank implements Serializable {

    private String companyCode;

    /**
     * 거래 내역 최종 업로드 성공 날짜(yyyyMMdd)
     */
    private String transaction;

}
