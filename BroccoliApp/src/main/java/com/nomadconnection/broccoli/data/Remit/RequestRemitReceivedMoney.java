package com.nomadconnection.broccoli.data.Remit;

import com.nomadconnection.broccoli.data.RequestDataByUserId;

/**
 * Created by YelloHyunminJang on 16. 7. 15..
 */
public class RequestRemitReceivedMoney extends RequestDataByUserId {
    private String sendId;

    public String getSendId() {
        return sendId;
    }

    public void setSendId(String sendId) {
        this.sendId = sendId;
    }
}
