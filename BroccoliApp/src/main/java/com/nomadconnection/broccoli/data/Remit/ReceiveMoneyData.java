package com.nomadconnection.broccoli.data.Remit;

/**
 * Created by YelloHyunminJang on 16. 7. 14..
 */
public class ReceiveMoneyData {

    private String sendId;
    private String senderName;
    private String senderWalletId;
    private String sendMoney;

    public String getSendId() {
        return sendId;
    }

    public void setSendId(String sendId) {
        this.sendId = sendId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderWalletId() {
        return senderWalletId;
    }

    public void setSenderWalletId(String senderWalletId) {
        this.senderWalletId = senderWalletId;
    }

    public String getSendMoney() {
        return sendMoney;
    }

    public void setSendMoney(String sendMoney) {
        this.sendMoney = sendMoney;
    }
}
