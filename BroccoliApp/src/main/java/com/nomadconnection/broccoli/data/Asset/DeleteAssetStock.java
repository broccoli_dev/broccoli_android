package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class DeleteAssetStock {

    @SerializedName("userId")
    private String mDeleteAssetStockUserId;

    @SerializedName("transId")
    private String mDeleteAssetStockTransId;

    public DeleteAssetStock(String _DeleteAssetStockUserId, String _DeleteAssetStockTransId){
        this.mDeleteAssetStockUserId = _DeleteAssetStockUserId;
        this.mDeleteAssetStockTransId = _DeleteAssetStockTransId;
    }

//    private String userId;
//    private String transId;
//
//    /**
//     * 사용자아이디
//     * @return
//     */
//    public String getUserId() {
//        return userId;
//    }
//
//    /**
//     * 사용자아이디
//     * @param user_id
//     */
//    public void setUserId(String user_id) {
//        this.userId = user_id;
//    }
//
//    /**
//     * 거래아이디
//     * <p>0.51문서에는 int타입으로 표시되어 있지만 조회나 수정 api에서는 모두 String형태이므로 String으로 변경</p>
//     * @return
//     */
//    public String getTransId() {
//        return transId;
//    }
//
//    /**
//     * 거래아이디
//     * <p>0.51문서에는 int타입으로 표시되어 있지만 조회나 수정 api에서는 모두 String형태이므로 String으로 변경</p>
//     * @param id
//     */
//    public void setTransId(String id) {
//        this.transId = id;
//    }
}
