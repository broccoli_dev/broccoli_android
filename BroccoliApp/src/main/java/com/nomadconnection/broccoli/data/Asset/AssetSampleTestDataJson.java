package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.data.Etc.BodyMoneyInfo;
import com.nomadconnection.broccoli.data.Etc.BodyOrgInfo;
import com.nomadconnection.broccoli.data.Etc.EtcMainData;
import com.nomadconnection.broccoli.data.Result;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class AssetSampleTestDataJson {

	public static final String	SAMPLE_USER	= "386ca2791a7dc5a40b49de15995ec10216aa267610336fc4b149501956af1075";
	

	/* ###**자산 메인 조회** */
	public static AssetMainData GetSampleData1() {
		String mTestJson =
				"{\n" +
						"  \"monthlyProperty\": [\n" +
						"    {\n" +
						"      \"date\": \"201508\",\n" +
						"      \"sum\": \"0\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201509\",\n" +
						"      \"sum\": \"0\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201510\",\n" +
						"      \"sum\": \"0\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201511\",\n" +
						"      \"sum\": \"1130037\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201512\",\n" +
						"      \"sum\": \"898927\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201601\",\n" +
						"      \"sum\": \"898927\"\n" +
						"    }\n" +
						"  ],\n" +
						"  \"monthlyDebt\": [\n" +
						"    {\n" +
						"      \"date\": \"201508\",\n" +
						"      \"sum\": \"0\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201509\",\n" +
						"      \"sum\": \"100000\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201510\",\n" +
						"      \"sum\": \"200000\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201511\",\n" +
						"      \"sum\": \"300000\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201512\",\n" +
						"      \"sum\": \"400000\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"date\": \"201601\",\n" +
						"      \"sum\": \"500000\"\n" +
						"    }\n" +
						"  ],\n" +
						"  \"bank\": [\n" +
						"    {\n" +
						"      \"accountId\": \"2\",\n" +
						"      \"companyCode\": \"081\",\n" +
						"      \"accountNum\": \"45591015121307\",\n" +
						"      \"accountName\": \" 저축예금 \",\n" +
						"      \"afterValue\": \"316032\",\n" +
						"      \"lastTransDate\": \"20151223134042\",\n" +
						"      \"lastTransOpponent\": \"김태호 \",\n" +
						"      \"lastTransSum\": \"-30000\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"accountId\": \"1\",\n" +
						"      \"companyCode\": \"081\",\n" +
						"      \"accountNum\": \"36291022279707\",\n" +
						"      \"accountName\": \" 저축예금 \",\n" +
						"      \"afterValue\": \"0\",\n" +
						"      \"lastTransDate\": \"\",\n" +
						"      \"lastTransOpponent\": \"\",\n" +
						"      \"lastTransSum\": \"\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"accountId\": \"3\",\n" +
						"      \"companyCode\": \"081\",\n" +
						"      \"accountNum\": \"45591015172607\",\n" +
						"      \"accountName\": \" 저축예금 \",\n" +
						"      \"afterValue\": \"2801416\",\n" +
						"      \"lastTransDate\": \"\",\n" +
						"      \"lastTransOpponent\": \"\",\n" +
						"      \"lastTransSum\": \"\"\n" +
						"    }\n" +
						"  ],\n" +
						"  \"card\": {\n" +
						"    \"cardCount\": 1,\n" +
						"    \"sum\": \"756000\",\n" +
						"    \"lastTransDate\": \"20151223120700\",\n" +
						"    \"lastTransOpponent\": \"후루룩\",\n" +
						"    \"lastSum\": \"9500\"\n" +
						"  },\n" +
						"  \"stock\": {\n" +
						"    \"date\": \"20160120030000\",\n" +
						"    \"list\": [\n" +
						"      {\n" +
						"        \"stockCode\": \"111111\",\n" +
						"        \"stockName\": \"YFG\",\n" +
						"        \"sum\": \"1000000\",\n" +
						"        \"prevSum\": \"1050000\"\n" +
						"      },\n" +
						"      {\n" +
						"        \"stockCode\": \"333333\",\n" +
						"        \"stockName\": \"YFG123\",\n" +
						"        \"sum\": \"0\",\n" +
						"        \"prevSum\": \"1050000\"\n" +
						"      },\n" +
						"      {\n" +
						"        \"stockCode\": \"444444\",\n" +
						"        \"stockName\": \"YFG456\",\n" +
						"        \"sum\": \"0\",\n" +
						"        \"prevSum\": \"1050000\"\n" +
						"      },\n" +
						"      {\n" +
						"        \"stockCode\": \"222222\",\n" +
						"        \"stockName\": \"YMP\",\n" +
						"        \"sum\": \"2000000\",\n" +
						"        \"prevSum\": \"1900000\"\n" +
						"      }\n" +
						"    ]\n" +
						"  },\n" +
						"  \"debt\": [\n" +
						"    {\n" +
						"      \"debtId\": \"1\",\n" +
						"      \"companyCode\": \"004\",\n" +
						"      \"debtName\": \"이자 싼 대출\",\n" +
						"      \"sum\": \"40000000\",\n" +
						"      \"repayPercent\": 30\n" +
						"    },\n" +
						"    {\n" +
						"      \"debtId\": \"1\",\n" +
						"      \"companyCode\": \"004\",\n" +
						"      \"debtName\": \"더 싼 대출\",\n" +
						"      \"sum\": \"20000000\",\n" +
						"      \"repayPercent\": 10\n" +
						"    }\n" +
						"  ],\n" +
						"  \"realEstate\": [\n" +
						"    {\n" +
						"      \"estateId\": \"1\",\n" +
						"      \"estateName\": \"우리집\",\n" +
						"      \"estateType\": \"01\",\n" +
						"      \"dealType\": \"01\",\n" +
						"      \"price\": \"100000000\"\n" +
						"    }\n" +
						"  ],\n" +
						"  \"car\": [\n" +
						"    {\n" +
						"      \"carId\": \"1\",\n" +
						"      \"makerCode\": \"아우디\",\n" +
						"      \"carCode\": \"Audi A7\",\n" +
						"      \"madeYear\": \"2015\",\n" +
						"      \"subCode\": \"3.0\",\n" +
						"      \"marketPrice\": \"28000000\"\n" +
						"    }\n" +
						"  ]\n" +
						"}";


		AssetMainData mSample = new AssetMainData();
		mSample = new Gson().fromJson(mTestJson, AssetMainData.class);
		return mSample;
	}


	/* ###**은행거래 내역 상세 조회** */
	public static ArrayList<ResponseAssetBankDetail> GetSampleData2() {
		String mTestJson =
				"[\n" +
						"  {\n" +
						"    \"transDate\": \"20151214175937\",\n" +
						"    \"transMethod\": \"카드입금\",\n" +
						"    \"opponent\": \"체크할인\",\n" +
						"    \"sum\": \"2052\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151214094850\",\n" +
						"    \"transMethod\": \"전자금융\",\n" +
						"    \"opponent\": \"고용부실업급여\",\n" +
						"    \"sum\": \"1059780\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151212162404\",\n" +
						"    \"transMethod\": \"스마트입금\",\n" +
						"    \"opponent\": \"이은ㅁ\",\n" +
						"    \"sum\": \"70000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151212021207\",\n" +
						"    \"transMethod\": \"결산이자\",\n" +
						"    \"opponent\": \"이자세금：４０원\",\n" +
						"    \"sum\": \"268\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151209213210\",\n" +
						"    \"transMethod\": \"전자금융\",\n" +
						"    \"opponent\": \"이기영\",\n" +
						"    \"sum\": \"14000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151209213035\",\n" +
						"    \"transMethod\": \"전자금융\",\n" +
						"    \"opponent\": \"박영재\",\n" +
						"    \"sum\": \"35000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151209174124\",\n" +
						"    \"transMethod\": \"체크카드\",\n" +
						"    \"opponent\": \"수노래연습장럭셔리\",\n" +
						"    \"sum\": \"19000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151130181644\",\n" +
						"    \"transMethod\": \"전자금융\",\n" +
						"    \"opponent\": \"최은호\",\n" +
						"    \"sum\": \"110000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151128192543\",\n" +
						"    \"transMethod\": \"체크카드\",\n" +
						"    \"opponent\": \"한화호텔앤드리조트（\",\n" +
						"    \"sum\": \"20000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151125162140\",\n" +
						"    \"transMethod\": \"체크카드\",\n" +
						"    \"opponent\": \"이니시스（빌링）\",\n" +
						"    \"sum\": \"1000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151123180703\",\n" +
						"    \"transMethod\": \"카드입금\",\n" +
						"    \"opponent\": \"체크할인\",\n" +
						"    \"sum\": \"23000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151123180703\",\n" +
						"    \"transMethod\": \"카드입금\",\n" +
						"    \"opponent\": \"체크할인\",\n" +
						"    \"sum\": \"25000\"\n" +
						"  }\n" +
						"]";


		ArrayList<ResponseAssetBankDetail> mSample = new ArrayList<ResponseAssetBankDetail>();
		Type listType = new TypeToken<ArrayList<ResponseAssetBankDetail>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}


	/* ###**주식 내역 상세 조회**** */
	public static BodyAssetStockDetail GetSampleData5() {
		String mTestJson =
				"{\n" +
						"  \"count\": \"80\",\n" +
						"  \"totalSum\": \"688000\",\n" +
						"  \"prevTotalSum\": \"640000\",\n" +
						"  \"list\": [\n" +
						"    {\n" +
						"      \"transType\": 2,\n" +
						"      \"transDate\": \"20160111\",\n" +
						"      \"price\": \"950\",\n" +
						"      \"amount\": \"20\",\n" +
						"      \"sum\": \"19000\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"transType\": 1,\n" +
						"      \"transDate\": \"20160107\",\n" +
						"      \"price\": \"1000\",\n" +
						"      \"amount\": \"100\",\n" +
						"      \"sum\": \"100000\"\n" +
						"    }\n" +
						"  ]\n" +
						"}";


		BodyAssetStockDetail mSample = new BodyAssetStockDetail();
		mSample = new Gson().fromJson(mTestJson, BodyAssetStockDetail.class);
		return mSample;
	}

	/* ###**주식 내역 추가/수정** */
	public static Result GetSampleData6() {
		String mTestJson =
				"{\n" +
						"\t\"code\" : \"100\",\n" +
						"\t\"desc\" : \"ok\"\n" +
						"}";


		Result mSample = new Result();
		mSample = new Gson().fromJson(mTestJson, Result.class);
		return mSample;
	}

	/* ###**주식 내역 삭제** */
	public static Result GetSampleData7() {
		String mTestJson =
				"{\n" +
						"\t\"code\" : \"100\",\n" +
						"\t\"desc\" : \"ok\"\n" +
						"}";


		Result mSample = new Result();
		mSample = new Gson().fromJson(mTestJson, Result.class);
		return mSample;
	}

	/* ###**주식 종목 검색** */
	public static ArrayList<ResponseSearchAssetStock> GetSampleData8() {
		String mTestJson =
				"[\n" +
						"\t{\n" +
						"\t\t\"stockCode\" : \"111111\",\n" +
						"\t\t\"stockName\" : \"(주)센토리\"\n" +
						"\t}, {\n" +
						"\t\t\"stockCode\" : \"123456\",\n" +
						"\t\t\"stockName\" : \"센토하우스\"\n" +
						"\t}\n" +
						"]";


		ArrayList<ResponseSearchAssetStock> mSample = new ArrayList<ResponseSearchAssetStock>();
		Type listType = new TypeToken<ArrayList<ResponseSearchAssetStock>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}

	/* ###**부동산 추가/수정** */
	public static Result GetSampleData9() {
		String mTestJson =
				"{\n" +
						"\t\"code\" : \"100\",\n" +
						"\t\"desc\" : \"ok\"\n" +
						"}";


		Result mSample = new Result();
		mSample = new Gson().fromJson(mTestJson, Result.class);
		return mSample;
	}

	/* ###**부동산 내역 삭제** */
	public static Result GetSampleData10() {
		String mTestJson =
				"{\n" +
						"\t\"code\" : \"100\",\n" +
						"\t\"desc\" : \"ok\"\n" +
						"}";


		Result mSample = new Result();
		mSample = new Gson().fromJson(mTestJson, Result.class);
		return mSample;
	}

	/* ###**자동차 추가/수정** */
	public static Result GetSampleData11() {
		String mTestJson =
				"{\n" +
						"\t\"code\" : \"100\",\n" +
						"\t\"desc\" : \"ok\"\n" +
						"}";


		Result mSample = new Result();
		mSample = new Gson().fromJson(mTestJson, Result.class);
		return mSample;
	}

	/* ###**자동차 삭제** */
	public static Result GetSampleData12() {
		String mTestJson =
				"{\n" +
						"\t\"code\" : \"100\",\n" +
						"\t\"desc\" : \"ok\"\n" +
						"}";


		Result mSample = new Result();
		mSample = new Gson().fromJson(mTestJson, Result.class);
		return mSample;
	}

	/* ###**대출 상세 조회** */
	public static ArrayList<ResponseAssetBankDetail> GetSampleData13() {
		String mTestJson =
				"[\n" +
						"  {\n" +
						"    \"transDate\": \"20151214175937\",\n" +
						"    \"transMethod\": \"카드입금\",\n" +
						"    \"opponent\": \"체크할인\",\n" +
						"    \"sum\": \"2052\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151214094850\",\n" +
						"    \"transMethod\": \"전자금융\",\n" +
						"    \"opponent\": \"고용부실업급여\",\n" +
						"    \"sum\": \"1059780\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151212162404\",\n" +
						"    \"transMethod\": \"스마트입금\",\n" +
						"    \"opponent\": \"이은ㅁ\",\n" +
						"    \"sum\": \"70000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151212021207\",\n" +
						"    \"transMethod\": \"결산이자\",\n" +
						"    \"opponent\": \"이자세금：４０원\",\n" +
						"    \"sum\": \"268\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151209213210\",\n" +
						"    \"transMethod\": \"전자금융\",\n" +
						"    \"opponent\": \"이기영\",\n" +
						"    \"sum\": \"14000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151209213035\",\n" +
						"    \"transMethod\": \"전자금융\",\n" +
						"    \"opponent\": \"박영재\",\n" +
						"    \"sum\": \"35000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151209174124\",\n" +
						"    \"transMethod\": \"체크카드\",\n" +
						"    \"opponent\": \"수노래연습장럭셔리\",\n" +
						"    \"sum\": \"19000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151130181644\",\n" +
						"    \"transMethod\": \"전자금융\",\n" +
						"    \"opponent\": \"최은호\",\n" +
						"    \"sum\": \"110000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151128192543\",\n" +
						"    \"transMethod\": \"체크카드\",\n" +
						"    \"opponent\": \"한화호텔앤드리조트（\",\n" +
						"    \"sum\": \"20000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151125162140\",\n" +
						"    \"transMethod\": \"체크카드\",\n" +
						"    \"opponent\": \"이니시스（빌링）\",\n" +
						"    \"sum\": \"1000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151123180703\",\n" +
						"    \"transMethod\": \"카드입금\",\n" +
						"    \"opponent\": \"체크할인\",\n" +
						"    \"sum\": \"23000\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transDate\": \"20151123180703\",\n" +
						"    \"transMethod\": \"카드입금\",\n" +
						"    \"opponent\": \"체크할인\",\n" +
						"    \"sum\": \"25000\"\n" +
						"  }\n" +
						"]";


		ArrayList<ResponseAssetBankDetail> mSample = new ArrayList<ResponseAssetBankDetail>();
		Type listType = new TypeToken<ArrayList<ResponseAssetBankDetail>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}

	/* ###**주식 리스트 조회** */
	public static AssetStockInfo GetSampleData14() {
		String mTestJson =
				"{\n" +
						"  \"date\": \"201601310300\",\n" +
						"  \"list\": [\n" +
						"    {\n" +
						"      \"stockCode\": \"111111\",\n" +
						"      \"stockName\": \"(주)센토리\",\n" +
						"      \"sum\": \"300000\",\n" +
						"      \"prevSum\": \"330000\"\n" +
						"    },\n" +
						"    {\n" +
						"      \"stockCode\": \"123456\",\n" +
						"      \"stockName\": \"(주)센토리\",\n" +
						"      \"sum\": \"400000\",\n" +
						"      \"prevSum\": \"440000\"\n" +
						"    }\n" +
						"  ]\n" +
						"}";


		AssetStockInfo mSample = new AssetStockInfo();
		mSample = new Gson().fromJson(mTestJson, AssetStockInfo.class);
		return mSample;
	}

	/* ###**기타 재산 조회** */
	public static AssetOtherInfo GetSampleData15() {
		String mTestJson =
				"{\n" +
						"  \"realEstate\": [\n" +
						"    {\n" +
						"      \"estateId\": \"1\",\n" +
						"      \"estateName\": \"우리집\",\n" +
						"      \"estateType\": \"01\",\n" +
						"      \"dealType\": \"01\",\n" +
						"      \"price\": \"100000000\"\n" +
						"    }\n" +
						"  ],\n" +
						"  \"car\": [\n" +
						"    {\n" +
						"      \"carId\": \"1\",\n" +
						"      \"makerCode\": \"아우디\",\n" +
						"      \"carCode\": \"Audi A7\",\n" +
						"      \"madeYear\": \"2015\",\n" +
						"      \"subCode\": \"3.0\",\n" +
						"      \"marketPrice\": \"28000000\"\n" +
						"    }\n" +
						"  ]\n" +
						"}";


		AssetOtherInfo mSample = new AssetOtherInfo();
		mSample = new Gson().fromJson(mTestJson, AssetOtherInfo.class);
		return mSample;
	}

	/* ###**기타 메인 조회(첼린지, 머니캘린더)** */
	public static EtcMainData GetSampleData16() {
		String mTestJson =
				"{\n" +
						"  \"challenge\": [\n" +
						"    {\n" +
						"      \"challengeId\": \"4\",\n" +
						"      \"challengeType\": \"02\",\n" +
						"      \"title\": \"그냥 그랬어\",\n" +
						"      \"startDate\": \"20160202\",\n" +
						"      \"endDate\": \"20161131\",\n" +
						"      \"amt\": \"\",\n" +
						"      \"goalAmt\": \"500000\"\n" +
						"    }\n" +
						"  ],\n" +
						"  \"moneyCalendar\": {\n" +
						"    \"totalCount\": 5,\n" +
						"    \"list\": [\n" +
						"      {\n" +
						"        \"calendarId\": \"3\",\n" +
						"        \"title\": \"삼성카드\",\n" +
						"        \"transactionId\": \"1\",\n" +
						"        \"requestCompany\": \"펀드예탁금이자\",\n" +
						"        \"amt\": \"1560000\",\n" +
						"        \"payType\": \"01\",\n" +
						"        \"expectYn\": \"Y\",\n" +
						"        \"payDate\": \"20160205\",\n" +
						"        \"payYn\": \"Y\",\n" +
						"        \"repeatType\": \"1M\"\n" +
						"      },\n" +
						"      {\n" +
						"        \"calendarId\": \"5\",\n" +
						"        \"title\": \"몰라\",\n" +
						"        \"transactionId\": \"1\",\n" +
						"        \"requestCompany\": \"나영문\",\n" +
						"        \"amt\": \"50000\",\n" +
						"        \"payType\": \"02\",\n" +
						"        \"expectYn\": \"Y\",\n" +
						"        \"payDate\": \"20160207\",\n" +
						"        \"payYn\": \"Y\",\n" +
						"        \"repeatType\": \"1M\"\n" +
						"      },\n" +
						"      {\n" +
						"        \"calendarId\": \"2\",\n" +
						"        \"title\": \"신한카드\",\n" +
						"        \"transactionId\": \"1\",\n" +
						"        \"requestCompany\": \"신한카드\",\n" +
						"        \"amt\": \"10000\",\n" +
						"        \"payType\": \"02\",\n" +
						"        \"expectYn\": \"Y\",\n" +
						"        \"payDate\": \"20160210\",\n" +
						"        \"payYn\": \"Y\",\n" +
						"        \"repeatType\": \"1M\"\n" +
						"      },\n" +
						"      {\n" +
						"        \"calendarId\": \"4\",\n" +
						"        \"title\": \"전기세\",\n" +
						"        \"transactionId\": \"1\",\n" +
						"        \"requestCompany\": \"나영문\",\n" +
						"        \"amt\": \"32680\",\n" +
						"        \"payType\": \"01\",\n" +
						"        \"expectYn\": \"Y\",\n" +
						"        \"payDate\": \"20160215\",\n" +
						"        \"payYn\": \"Y\",\n" +
						"        \"repeatType\": \"1M\"\n" +
						"      }\n" +
						"    ]\n" +
						"  }\n" +
						"}";


		EtcMainData mSample = new EtcMainData();
		mSample = new Gson().fromJson(mTestJson, EtcMainData.class);
		return mSample;
	}


	/* ###**머니 캘린더 리스트 조회** */
	public static ArrayList<BodyMoneyInfo> GetSampleData17() {
		String mTestJson =
				"[\n" +
						"  {\n" +
						"    \"calendarId\": \"1\",\n" +
						"    \"title\": \"월세\",\n" +
						"    \"transactionId\": \"1\",\n" +
						"    \"requestCompany\": \"250099898010\",\n" +
						"    \"amt\": \"400000\",\n" +
						"    \"payType\": \"02\",\n" +
						"    \"expectYn\": \"Y\",\n" +
						"    \"payDate\": \"20160229\",\n" +
						"    \"payYn\": \"Y\",\n" +
						"    \"repeatType\": \"1M\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"calendarId\": \"2\",\n" +
						"    \"title\": \"신한카드\",\n" +
						"    \"transactionId\": \"1\",\n" +
						"    \"requestCompany\": \"신한카드\",\n" +
						"    \"amt\": \"10000\",\n" +
						"    \"payType\": \"02\",\n" +
						"    \"expectYn\": \"Y\",\n" +
						"    \"payDate\": \"20160110\",\n" +
						"    \"payYn\": \"Y\",\n" +
						"    \"repeatType\": \"1M\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"calendarId\": \"3\",\n" +
						"    \"title\": \"삼성카드\",\n" +
						"    \"transactionId\": \"1\",\n" +
						"    \"requestCompany\": \"펀드예탁금이자\",\n" +
						"    \"amt\": \"1560000\",\n" +
						"    \"payType\": \"01\",\n" +
						"    \"expectYn\": \"Y\",\n" +
						"    \"payDate\": \"20160105\",\n" +
						"    \"payYn\": \"N\",\n" +
						"    \"repeatType\": \"1M\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"calendarId\": \"4\",\n" +
						"    \"title\": \"전기세\",\n" +
						"    \"transactionId\": \"1\",\n" +
						"    \"requestCompany\": \"나영문\",\n" +
						"    \"amt\": \"32680\",\n" +
						"    \"payType\": \"01\",\n" +
						"    \"expectYn\": \"Y\",\n" +
						"    \"payDate\": \"20160204\",\n" +
						"    \"payYn\": \"Y\",\n" +
						"    \"repeatType\": \"1M\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"calendarId\": \"4\",\n" +
						"    \"title\": \"전기세\",\n" +
						"    \"transactionId\": \"1\",\n" +
						"    \"requestCompany\": \"나영문\",\n" +
						"    \"amt\": \"32680\",\n" +
						"    \"payType\": \"01\",\n" +
						"    \"expectYn\": \"Y\",\n" +
						"    \"payDate\": \"20160205\",\n" +
						"    \"payYn\": \"Y\",\n" +
						"    \"repeatType\": \"1M\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"calendarId\": \"4\",\n" +
						"    \"title\": \"전기세\",\n" +
						"    \"transactionId\": \"1\",\n" +
						"    \"requestCompany\": \"나영문\",\n" +
						"    \"amt\": \"32680\",\n" +
						"    \"payType\": \"01\",\n" +
						"    \"expectYn\": \"Y\",\n" +
						"    \"payDate\": \"20160206\",\n" +
						"    \"payYn\": \"Y\",\n" +
						"    \"repeatType\": \"1M\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"calendarId\": \"5\",\n" +
						"    \"title\": \"몰라\",\n" +
						"    \"transactionId\": \"1\",\n" +
						"    \"requestCompany\": \"나영문\",\n" +
						"    \"amt\": \"50000\",\n" +
						"    \"payType\": \"02\",\n" +
						"    \"expectYn\": \"Y\",\n" +
						"    \"payDate\": \"20160207\",\n" +
						"    \"payYn\": \"N\",\n" +
						"    \"repeatType\": \"1M\"\n" +
						"  }\n" +
						"]";


		ArrayList<BodyMoneyInfo> mSample = new ArrayList<BodyMoneyInfo>();
		Type listType = new TypeToken<ArrayList<BodyMoneyInfo>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}

	/* ###**머니캘린더 등록 수정** */
	public static Result GetSampleData18() {
		String mTestJson =
				"{\n" +
						"\t\"code\" : \"100\",\n" +
						"\t\"desc\" : \"ok\"\n" +
						"}";


		Result mSample = new Result();
		mSample = new Gson().fromJson(mTestJson, Result.class);
		return mSample;
	}

	/* ###**머니캘린더 삭제** */
	public static Result GetSampleData19() {
		String mTestJson =
				"{\n" +
						"\t\"code\" : \"100\",\n" +
						"\t\"desc\" : \"ok\"\n" +
						"}";


		Result mSample = new Result();
		mSample = new Gson().fromJson(mTestJson, Result.class);
		return mSample;
	}

	/* ###**머니 캘린더 청구기관 리스트 조회** */
	public static ArrayList<BodyOrgInfo> GetSampleData20() {
		String mTestJson =
				"[\n" +
						"  {\n" +
						"    \"transactionId\": \"1\",\n" +
						"    \"transactionDate\": \"20160202180146\",\n" +
						"    \"transactionMethod\": \"카드결\",\n" +
						"    \"opponent\": \"신한카드\",\n" +
						"    \"sum\": \"-1104606\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transactionId\": \"2\",\n" +
						"    \"transactionDate\": \"20160202180146\",\n" +
						"    \"transactionMethod\": \"카드결\",\n" +
						"    \"opponent\": \"우리카드\",\n" +
						"    \"sum\": \"-1104606\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transactionId\": \"3\",\n" +
						"    \"transactionDate\": \"20160107180142\",\n" +
						"    \"transactionMethod\": \"카드결\",\n" +
						"    \"opponent\": \"신한카드\",\n" +
						"    \"sum\": \"-395394\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transactionId\": \"4\",\n" +
						"    \"transactionDate\": \"20160107180142\",\n" +
						"    \"transactionMethod\": \"카드결\",\n" +
						"    \"opponent\": \"외환카드\",\n" +
						"    \"sum\": \"-395394\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transactionId\": \"5\",\n" +
						"    \"transactionDate\": \"20160104210451\",\n" +
						"    \"transactionMethod\": \"카드결\",\n" +
						"    \"opponent\": \"신한카드\",\n" +
						"    \"sum\": \"-774785\"\n" +
						"  },\n" +
						"  {\n" +
						"    \"transactionId\": \"6\",\n" +
						"    \"transactionDate\": \"20160104210451\",\n" +
						"    \"transactionMethod\": \"카드결\",\n" +
						"    \"opponent\": \"신한카드\",\n" +
						"    \"sum\": \"-774785\"\n" +
						"  }\n" +
						"]";


		ArrayList<BodyOrgInfo> mSample = new ArrayList<BodyOrgInfo>();
		Type listType = new TypeToken<ArrayList<BodyOrgInfo>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}


	/* ###**차량 제조사 조회** */
	public static ArrayList<ResponseAssetCarMakerCode> GetSampleData21() {
		String mTestJson =
				"[\n" +
						"\t{\n" +
						"\t\t\"makerCode\": \"아우디\"\n" +
						"\t}\n" +
						"\t{\n" +
						"\t\t\"makerCode\": \"현대\"\n" +
						"\t}\n" +
						"\t{\n" +
						"\t\t\"makerCode\": \"기아\"\n" +
						"\t}\n" +
						"]";


		ArrayList<ResponseAssetCarMakerCode> mSample = new ArrayList<ResponseAssetCarMakerCode>();
		Type listType = new TypeToken<ArrayList<ResponseAssetCarMakerCode>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}

	/* ###**차량 대표차명 조회** */
	public static ArrayList<ResponseAssetCarCode> GetSampleData22() {
		String mTestJson =
				"[\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"A3(3세대)\"\n" +
						"\t}\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"A5(2013~)\"\n" +
						"\t}\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"Q7(2009~)\"\n" +
						"\t}\n" +
						"]";


		ArrayList<ResponseAssetCarCode> mSample = new ArrayList<ResponseAssetCarCode>();
		Type listType = new TypeToken<ArrayList<ResponseAssetCarCode>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}

	/* ###**차량 세부차명 조회** */
	public static ArrayList<ResponseAssetCarSubCode> GetSampleData23() {
		String mTestJson =
				"[\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"A3(3세대)\"\n" +
						"\t}\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"A5(2013~)\"\n" +
						"\t}\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"Q7(2009~)\"\n" +
						"\t}\n" +
						"]";


		ArrayList<ResponseAssetCarSubCode> mSample = new ArrayList<ResponseAssetCarSubCode>();
		Type listType = new TypeToken<ArrayList<ResponseAssetCarSubCode>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}

	/* ###**차량 연식 조회** */
	public static ArrayList<ResponseAssetCarYear> GetSampleData24() {
		String mTestJson =
				"[\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"A3(3세대)\"\n" +
						"\t}\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"A5(2013~)\"\n" +
						"\t}\n" +
						"\t{\n" +
						"\t\t\"carCode\": \"Q7(2009~)\"\n" +
						"\t}\n" +
						"]";


		ArrayList<ResponseAssetCarYear> mSample = new ArrayList<ResponseAssetCarYear>();
		Type listType = new TypeToken<ArrayList<ResponseAssetCarYear>>() {}.getType();
		mSample = new Gson().fromJson(mTestJson, listType);
		return mSample;
	}


	/* ###**차량 시세 조회** */
	public static ResponseAssetCarMarketPrice GetSampleData25() {
		String mTestJson =
				"{\n" +
						"\t`marketPrice` : \"70000000\"\n" +
						"}";


		ResponseAssetCarMarketPrice mSample = new ResponseAssetCarMarketPrice();
		mSample = new Gson().fromJson(mTestJson, ResponseAssetCarMarketPrice.class);
		return mSample;
	}


}
