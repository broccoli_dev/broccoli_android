package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 21..
 */

public class SpendCardClassifiedData implements Serializable {

    public String cardCode;
    public String companyCode;
    public String companyName;
    public String cardName;
    public int cardType;
    public int mileageYn;
    public int internalAnnualFee;
    public int foreignAnnualFee;
    public int requirement;
    public String approvedNo;
    public String approvedDate;
    public String applyUrl;
    public float overdueMinRate;
    public float overdueMaxRate;

}
