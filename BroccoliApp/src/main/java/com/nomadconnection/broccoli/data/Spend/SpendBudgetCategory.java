package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 2017. 3. 8..
 */

public class SpendBudgetCategory implements Serializable {

    public long budgetId;
    public int categoryGroup;
    public long sum;
    public long expense;

}
