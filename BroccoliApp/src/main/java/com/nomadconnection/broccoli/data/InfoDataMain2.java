package com.nomadconnection.broccoli.data;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoDataMain2 implements Parcelable {
	
	public String mTxt1 = null;			// title
	public String mTxt2 = null;			// 등록여부
	
	public String mTxt3 = null;			// 소비이력 - 태그
	public String mTxt4 = null;			// 소비이력 - 제목    
	public String mTxt5 = null;			// 소비이력 - 금액
	public String mTxt6 = null;			// 소비이력 - 날자
	public String mTxt7 = null;			// 소비이력 - 태그
	public String mTxt8 = null;			// 소비이력 - 제목    
	public String mTxt9 = null;			// 소비이력 - 금액
	public String mTxt10 = null;		// 소비이력 - 날자
	public String mTxt11 = null;		// 소비이력 - 태그
	public String mTxt12 = null;		// 소비이력 - 제목    
	public String mTxt13 = null;		// 소비이력 - 금액
	public String mTxt14 = null;		// 소비이력 - 날자
	
	public String mTxt15 = null;		// 예산 - 서브 1 제목
	public String mTxt16 = null;		// 예산 - 서브 1 금액
	public String mTxt17 = null;		// 예산 - 서브 2 제목
	public String mTxt18 = null;		// 예산 - 서브 2 금액
	
	public String mTxt19 = null;		// 소비분류 - 서브 1 제목
	public String mTxt20 = null;		// 소비분류 - 서브 1 금액
	public String mTxt21 = null;		// 소비분류 - 서브 2 제목
	public String mTxt22 = null;		// 소비분류 - 서브 2 금액
	public String mTxt23 = null;		// 소비분류 - 서브 3 제목
	public String mTxt24 = null;		// 소비분류 - 서브 3 금액
	
	public String mTxt25 = null;		// 소비방법 - 서브 1 제목
	public String mTxt26 = null;		// 소비방법 - 서브 1 금액
	public String mTxt27 = null;		// 소비방법 - 서브 2 제목
	public String mTxt28 = null;		// 소비방법 - 서브 2 금액
	public String mTxt29 = null;		// 소비방법 - 서브 3 제목
	public String mTxt30 = null;		// 소비방법 - 서브 3 금액
	
	
	public InfoDataMain2() {
	}
	
	public InfoDataMain2(String _Txt1, String _Txt2, 
			String _Txt3, String _Txt4, String _Txt5, String _Txt6, 
			String _Txt7, String _Txt8, String _Txt9, String _Txt10, 
			String _Txt11, String _Txt12, String _Txt13, String _Txt14, 
			String _Txt15, String _Txt16, String _Txt17, String _Txt18, 
			String _Txt19, String _Txt20, String _Txt21, String _Txt22, String _Txt23, String _Txt24,
			String _Txt25, String _Txt26, String _Txt27, String _Txt28, String _Txt29, String _Txt30
			) {
		this.mTxt1 = _Txt1;
		this.mTxt2 = _Txt2;
		this.mTxt3 = _Txt3;
		this.mTxt4 = _Txt4;
		this.mTxt5 = _Txt5;
		this.mTxt6 = _Txt6;
		this.mTxt7 = _Txt7;
		this.mTxt8 = _Txt8;
		this.mTxt9 = _Txt9;
		this.mTxt10 = _Txt10;
		this.mTxt11 = _Txt11;
		this.mTxt12 = _Txt12;
		this.mTxt13 = _Txt13;
		this.mTxt14 = _Txt14;
		this.mTxt15 = _Txt15;
		this.mTxt16 = _Txt16;
		this.mTxt17 = _Txt17;
		this.mTxt18 = _Txt18;
		this.mTxt19 = _Txt19;
		this.mTxt20 = _Txt20;
		this.mTxt21 = _Txt21;
		this.mTxt22 = _Txt22;
		this.mTxt23 = _Txt23;
		this.mTxt24 = _Txt24;
		this.mTxt25 = _Txt25;
		this.mTxt26 = _Txt26;
		this.mTxt27 = _Txt27;
		this.mTxt28 = _Txt28;
		this.mTxt29 = _Txt29;
		this.mTxt30 = _Txt30;
	}
	
	
	
	public InfoDataMain2(Parcel in) {
		this.mTxt1 = in.readString();
		this.mTxt2 = in.readString();
		this.mTxt3 = in.readString();
		this.mTxt4 = in.readString();
		this.mTxt5 = in.readString();
		this.mTxt6 = in.readString();
		this.mTxt7 = in.readString();
		this.mTxt8 = in.readString();
		this.mTxt9 = in.readString();
		this.mTxt10 = in.readString();
		this.mTxt11 = in.readString();
		this.mTxt12 = in.readString();
		this.mTxt13 = in.readString();
		this.mTxt14 = in.readString();
		this.mTxt15 = in.readString();
		this.mTxt16 = in.readString();
		this.mTxt17 = in.readString();
		this.mTxt18 = in.readString();
		this.mTxt19 = in.readString();
		this.mTxt20 = in.readString();
		this.mTxt21 = in.readString();
		this.mTxt22 = in.readString();
		this.mTxt23 = in.readString();
		this.mTxt24 = in.readString();
		this.mTxt25 = in.readString();
		this.mTxt26 = in.readString();
		this.mTxt27 = in.readString();
		this.mTxt28 = in.readString();
		this.mTxt29 = in.readString();
		this.mTxt30 = in.readString();
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.mTxt1);
		dest.writeString(this.mTxt2);
		dest.writeString(this.mTxt3);
		dest.writeString(this.mTxt4);
		dest.writeString(this.mTxt5);
		dest.writeString(this.mTxt6);
		dest.writeString(this.mTxt7);
		dest.writeString(this.mTxt8);
		dest.writeString(this.mTxt9);
		dest.writeString(this.mTxt10);
		dest.writeString(this.mTxt11);
		dest.writeString(this.mTxt12);
		dest.writeString(this.mTxt13);
		dest.writeString(this.mTxt14);
		dest.writeString(this.mTxt15);
		dest.writeString(this.mTxt16);
		dest.writeString(this.mTxt17);
		dest.writeString(this.mTxt18);
		dest.writeString(this.mTxt19);
		dest.writeString(this.mTxt20);
		dest.writeString(this.mTxt21);
		dest.writeString(this.mTxt22);
		dest.writeString(this.mTxt23);
		dest.writeString(this.mTxt24);
		dest.writeString(this.mTxt25);
		dest.writeString(this.mTxt26);
		dest.writeString(this.mTxt27);
		dest.writeString(this.mTxt28);
		dest.writeString(this.mTxt29);
		dest.writeString(this.mTxt30);
	}
	
	public static final Creator<InfoDataMain2> CREATOR = new Creator<InfoDataMain2>() {
		public InfoDataMain2 createFromParcel(Parcel in) {
			return new InfoDataMain2(in);
		}
		public InfoDataMain2[] newArray (int size) {
			return new InfoDataMain2[size];
		}
	};

	
}
