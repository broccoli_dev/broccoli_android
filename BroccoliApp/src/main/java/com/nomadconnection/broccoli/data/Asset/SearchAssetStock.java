package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 18..
 * 주식 종목 검색
 */
public class SearchAssetStock {

    @SerializedName("userId")
    private String mSearchAssetStockUserId;

    @SerializedName("keyWord")
    private String mKeyWord;

    public SearchAssetStock(String _SearchAssetStockUserId, String _KeyWord){
        this.mSearchAssetStockUserId = _SearchAssetStockUserId;
        this.mKeyWord = _KeyWord;
    }

//    private String keyWord;
//
//    /**
//     * 키워드 검색
//     * @return
//     */
//    public String getKeyWord() {
//        return keyWord;
//    }
//
//    /**
//     * 키워드 검색
//     * @param keyWord
//     */
//    public void setKeyWord(String keyWord) {
//        this.keyWord = keyWord;
//    }
}
