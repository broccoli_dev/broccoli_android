package com.nomadconnection.broccoli.data.Demo;

/**
 * Created by YelloHyunminJang on 2017. 4. 3..
 */

public class TrialExpense {

    public int expenseId;
    public String month;
    public long expense;
    public long budget;
    public int firstCategoryId;
    public long firstCategoryAmount;
    public int secondCategoryId;
    public long secondCategoryAmount;
    public int thirdCategoryId;
    public long thirdCategoryAmount;
    public long creditExpense;
    public long checkExpense;
    public long cashExpense;
    public String cardCompanyCode;
    public String cardName;
    public long cardExpense;
    public long cardRequirement;

}
