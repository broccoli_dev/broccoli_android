package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class AssetCarInfo implements Parcelable {

    public AssetCarInfo(){ }

    @SerializedName("carId")
    public String mAssetCarId;                          // 차량 아이디

    @SerializedName("makerCode")
    public String mAssetCarMakerCode;                   // 제조사

    @SerializedName("carCode")
    public String mAssetCarCode;                        // 차량 분류

    @SerializedName("madeYear")
    public String mAssetCarYear;                        // 제조연도

    @SerializedName("subCode")
    public String mAssetCarSubCode;                     // 차량상세

    @SerializedName("marketPrice")
    public String mAssetCarMarketPrice;                 // 시세 평가액



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetCarId);
        dest.writeString(this.mAssetCarMakerCode);
        dest.writeString(this.mAssetCarCode);
        dest.writeString(this.mAssetCarYear);
        dest.writeString(this.mAssetCarSubCode);
        dest.writeString(this.mAssetCarMarketPrice);
    }

    public AssetCarInfo(Parcel in) {
        this.mAssetCarId = in.readString();
        this.mAssetCarMakerCode = in.readString();
        this.mAssetCarCode = in.readString();
        this.mAssetCarYear = in.readString();
        this.mAssetCarSubCode = in.readString();
        this.mAssetCarMarketPrice = in.readString();
    }

    public static final Creator<AssetCarInfo> CREATOR = new Creator<AssetCarInfo>() {
        public AssetCarInfo createFromParcel(Parcel in) {
            return new AssetCarInfo(in);
        }
        public AssetCarInfo[] newArray (int size) {
            return new AssetCarInfo[size];
        }
    };

//    @SerializedName("carId")
//    private String mId;
//    @SerializedName("makerCode")
//    private String mMakerCode;
//    @SerializedName("carCode")
//    private String mCode;
//    @SerializedName("madeYear")
//    private String mYear;
//    @SerializedName("subCode")
//    private String mSubCode;
//    @SerializedName("marketPrice")
//    private String mMarketPrice;
//
//    /**
//     * 차량 아이디
//     * @return
//     */
//    public String getId() {
//        return mId;
//    }
//
//    /**
//     * 차량 아이디
//     * @param id
//     */
//    public void setId(String id) {
//        this.mId = id;
//    }
//
//    /**
//     * 제조사
//     * @return
//     */
//    public String getMakerCode() {
//        return mMakerCode;
//    }
//
//    /**
//     * 제조사
//     * @param maker
//     */
//    public void setMakerCode(String maker) {
//        this.mMakerCode = maker;
//    }
//
//    /**
//     * 차량 분류
//     * @return
//     */
//    public String getCode() {
//        return mCode;
//    }
//
//    /**
//     * 차량 분류
//     * @param mCode
//     */
//    public void setCode(String mCode) {
//        this.mCode = mCode;
//    }
//
//    /**
//     * 제조연도
//     * @return
//     */
//    public String getYear() {
//        return mYear;
//    }
//
//    /**
//     * 제조연도
//     * @param mYear
//     */
//    public void setYear(String mYear) {
//        this.mYear = mYear;
//    }
//
//    /**
//     * 차량상세
//     * @return
//     */
//    public String getSubCode() {
//        return mSubCode;
//    }
//
//    /**
//     * 차량상세
//     * @param subCode
//     */
//    public void setSubCode(String subCode) {
//        this.mSubCode = subCode;
//    }
//
//    /**
//     * 시세 평가액
//     * @return
//     */
//    public String getMarketPrice() {
//        return mMarketPrice;
//    }
//
//    /**
//     * 시세 평가액
//     * @param mSum
//     */
//    public void setMarketPrice(String mSum) {
//        this.mMarketPrice = mSum;
//    }
}
