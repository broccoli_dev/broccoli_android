package com.nomadconnection.broccoli.data.Asset;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * 차량 연식 조회
 */
public class ResponseAssetCarYear implements Parcelable {

    @SerializedName("madeYear")
    public String mAssetCarMadeYear;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAssetCarMadeYear);
    }

    public ResponseAssetCarYear(Parcel in) {
        this.mAssetCarMadeYear = in.readString();
    }

    public static final Creator<ResponseAssetCarYear> CREATOR = new Creator<ResponseAssetCarYear>() {
        public ResponseAssetCarYear createFromParcel(Parcel in) {
            return new ResponseAssetCarYear(in);
        }
        public ResponseAssetCarYear[] newArray (int size) {
            return new ResponseAssetCarYear[size];
        }
    };

}
