package com.nomadconnection.broccoli.data.Asset;

import com.google.gson.annotations.SerializedName;
import com.nomadconnection.broccoli.data.Result;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 * 자산 메인 조회
 */
public class AssetOtherInfo extends Result{


    @SerializedName("realEstate")
    public ArrayList<AssetEstateInfo>   mMainAssetRealEstate;                         // 부동산 내역

    @SerializedName("car")
    public ArrayList<AssetCarInfo>      mMainAssetCar;                                 // 차량 내역

    @SerializedName("etc")
    public ArrayList<AssetEtcInfo>      mMainAssetEtc;                                 // 기타 내역

}
