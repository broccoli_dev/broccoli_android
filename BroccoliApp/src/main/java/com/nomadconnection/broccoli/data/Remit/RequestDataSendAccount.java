package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataSendAccount implements Serializable{
    private String userId;
    private String sendPW;
    private String sendKind;
    private String accountId;
    private String sendMoney;
    private String companyCode;
    private String accountNumber;
    private String opponent;




    /**
     * 유저아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 유저아이디
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 송금암호
     * @return
     */
    public String getSendPW() {
        return sendPW;
    }

    /**
     * 송금암호
     * @param sendPW
     */
    public void setSendPW(String sendPW) {
        this.sendPW = sendPW;
    }

    /**
     * 전송구분 (W: 월렛에서 A: 계좌에서 )
     * @return
     */
    public String getSendKind() {
        return sendKind;
    }

    /**
     * 전송구분 (W: 월렛에서 A: 계좌에서 )
     * @param sendKind
     */
    public void setSendKind(String sendKind) {
        this.sendKind = sendKind;
    }

    /**
     * 계좌 아이디 (월렛일 경우 그냥 공백으로 전송)
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌 아이디 (월렛일 경우 그냥 공백으로 전송)
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 전송금액
     * @return
     */
    public String getSendMoney() {
        return sendMoney;
    }

    /**
     * 전송금액
     * @param sendMoney
     */
    public void setSendMoney(String sendMoney) {
        this.sendMoney = sendMoney;
    }

    /**
     * 수신 은행코드
     * @return
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * 수신 은행코드
     * @param companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * 수신 계좌번호
     * @return
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * 수신 계좌번호
     * @param accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * 수신자
     * @return
     */
    public String getOpponent() {
        return opponent;
    }

    /**
     * 수신자
     * @param opponent
     */
    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }
}
