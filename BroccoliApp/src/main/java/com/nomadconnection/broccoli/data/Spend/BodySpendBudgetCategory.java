package com.nomadconnection.broccoli.data.Spend;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class BodySpendBudgetCategory extends BodySpendBudgetDetail implements Serializable{
    private String budget;
    private String expense;
    private String expensePrev;

    /**
     * 예산액
     * @return
     */
    public String getBudget() {
        return budget;
    }

    /**
     * 예산액
     * @param budget
     */
    public void setBudget(String budget) {
        this.budget = budget;
    }

    /**
     * 소비액
     * @return
     */
    public String getExpense() {
        return expense;
    }

    /**
     * 소비액
     * @param expense
     */
    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getExpensePrev() {
        return expensePrev;
    }
}
