package com.nomadconnection.broccoli.data;

import android.os.Parcel;
import android.os.Parcelable;


public class InfoScreen implements Parcelable {
	
	public String mTag;				// Tag
	public String mTitle;			// Title
	public int mTitleBg;			// Title Bg
	public String mAnimSet;			// Animation 적용 여뷰 (페이지 전환)
	public int mStartEnterAnim;		// Start Enter Anim         
	public int mStartExitAnim;		// Start Exit Anim    
	public int mFinishEnterAnim;	// Finish Enter Anim         
	public int mFinishExitAnim;		// Finish Exit Anim    
	
	
	public InfoScreen() {
	}
	
	public InfoScreen(String _Tag, String _Title, int _TitleBg, String _AnimSet, int _StartEnterAnim, int _StartExitAnim, int _FinishEnterAnim, int _FinishExitAnim) {
		this.mTag = _Tag;
		this.mTitle = _Title;
		this.mTitleBg = _TitleBg;
		this.mAnimSet = _AnimSet;
		this.mStartEnterAnim = _StartEnterAnim;
		this.mStartExitAnim = _StartExitAnim;
		this.mFinishEnterAnim = _FinishEnterAnim;
		this.mFinishExitAnim = _FinishExitAnim;
	}
	
	
	
	public InfoScreen(Parcel in) {
		this.mTag = in.readString();
		this.mTitle = in.readString();
		this.mTitleBg = in.readInt();
		this.mAnimSet = in.readString();
		this.mStartEnterAnim = in.readInt();
		this.mStartExitAnim = in.readInt();
		this.mFinishEnterAnim = in.readInt();
		this.mFinishExitAnim = in.readInt();
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.mTag);
		dest.writeString(this.mTitle);
		dest.writeInt(this.mTitleBg);
		dest.writeString(this.mAnimSet);
		dest.writeInt(this.mStartEnterAnim);
		dest.writeInt(this.mStartExitAnim);
		dest.writeInt(this.mFinishEnterAnim);
		dest.writeInt(this.mFinishExitAnim);
	}
	
	public static final Creator<InfoScreen> CREATOR = new Creator<InfoScreen>() {
		public InfoScreen createFromParcel(Parcel in) {
			return new InfoScreen(in);
		}
		public InfoScreen[] newArray (int size) {
			return new InfoScreen[size];
		}
	};

	
}
