package com.nomadconnection.broccoli.data.Scrap;

import com.google.gson.JsonObject;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 * 현금 영수증 내역
 */
public class CashReceiptList {

    private String USERID;
    private int TYPE;
    private String UPLOADID;
    private JsonObject result;

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public int getTYPE() {
        return TYPE;
    }

    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getUPLOADID() {
        return UPLOADID;
    }

    public void setUPLOADID(String UPLOADID) {
        this.UPLOADID = UPLOADID;
    }

    public JsonObject getResult() {
        return result;
    }

    public void setResult(JsonObject result) {
        this.result = result;
    }
}
