package com.nomadconnection.broccoli.data.Remit;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseRemitAccountCurrent extends RemitResult implements Serializable{
    private ArrayList<RemitBankAccount> myAccountList;
    private ArrayList<RemitBankAccount> sendAccountList;

    /**
     * 자신의 송금 계좌번호 정보
     * @return
     */
    public ArrayList<RemitBankAccount> getMyAccountList () {
        return myAccountList;
    }

    /**
     * 자신의 송금 계좌번호 정보
     * @param myAccountList
     */
    public void setMyAccountList(ArrayList<RemitBankAccount> myAccountList) {
        this.myAccountList = myAccountList;
    }

    /**
     * 송금계좌 리스트
     * @return
     */
    public ArrayList<RemitBankAccount> getSendAccountList () {
        return sendAccountList;
    }

    /**
     * 송금계좌 리스트
     * @param sendAccountList
     */
    public void setSendAccountList(ArrayList<RemitBankAccount> sendAccountList) {
        this.sendAccountList = sendAccountList;
    }
}
