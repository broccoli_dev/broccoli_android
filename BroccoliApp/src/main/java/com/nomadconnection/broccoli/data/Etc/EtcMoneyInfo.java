package com.nomadconnection.broccoli.data.Etc;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public class EtcMoneyInfo {

    @SerializedName("totalCount")
    public String                   mEtcMoneyTotalCount;

    @SerializedName("list")
    public ArrayList<BodyMoneyInfo> mEtcMoneyList;

}
