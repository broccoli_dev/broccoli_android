package com.nomadconnection.broccoli.data;

import com.nomadconnection.broccoliespider.data.BankRequestData;

/**
 * Created by YelloHyunminJang on 2016. 11. 24..
 */

public class FinancialLogin {
    private BankRequestData requestData;
    private boolean success;
    private int errorCode;
    private String userError;
    private String errorMsg;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getUserError() {
        return userError;
    }

    public void setUserError(String userError) {
        this.userError = userError;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public BankRequestData getRequestData() {
        return requestData;
    }

    public void setRequestData(BankRequestData data) {
        this.requestData = data;
    }
}
