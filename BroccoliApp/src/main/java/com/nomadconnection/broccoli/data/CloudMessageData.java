package com.nomadconnection.broccoli.data;

/**
 * Created by YelloHyunminJang on 2016. 10. 11..
 */

public class CloudMessageData {

    public static final String NORMAL = "normal";
    public static final String NOTICE = "notice";
    public static final String TRANSFER = "transfer";

    private int badge;
    private String type;
    private String content;

    public int getBadge() {
        return badge;
    }

    public void setBadge(int badge) {
        this.badge = badge;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
