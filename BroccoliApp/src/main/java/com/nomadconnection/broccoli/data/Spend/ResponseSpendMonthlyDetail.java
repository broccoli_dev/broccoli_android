package com.nomadconnection.broccoli.data.Spend;

import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseSpendMonthlyDetail {
    private String code;
    private String desc;
    private List<SpendMonthlyInfo> list;

    /**
     * response code
     * @return
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * response desc
     * @return
     */
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<SpendMonthlyInfo> getList() {
        return list;
    }

    public void setList(List<SpendMonthlyInfo> list) {
        this.list = list;
    }
}
