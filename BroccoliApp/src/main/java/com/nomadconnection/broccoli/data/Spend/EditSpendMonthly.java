package com.nomadconnection.broccoli.data.Spend;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 * 소비 내역 추가/수정
 */
public class EditSpendMonthly extends SpendMonthlyInfo {
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }
}
