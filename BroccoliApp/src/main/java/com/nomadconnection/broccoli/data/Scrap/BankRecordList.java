package com.nomadconnection.broccoli.data.Scrap;

import com.google.gson.JsonObject;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 * 은행 입출력 기록들
 */
public class BankRecordList {
    private String USERID;
    private String CMPNYCODE;
    private String UPLOADID;
    private int TYPE;
    private String reqAccount;
    private JsonObject result;
    private String CURRCD;

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public String getCMPNYCODE() {
        return CMPNYCODE;
    }

    public void setCMPNYCODE(String CMPNYCODE) {
        this.CMPNYCODE = CMPNYCODE;
    }

    /**
     * 계좌번호
     * @return
     */
    public String getReqAccount() {
        return reqAccount;
    }

    /**
     * 계좌번호
     * @param reqAccount
     */
    public void setReqAccount(String reqAccount) {
        this.reqAccount = reqAccount;
    }

    public JsonObject getResult() {
        return result;
    }

    public void setResult(JsonObject result) {
        this.result = result;
    }

    public String getCURRCD() {
        return CURRCD;
    }

    public void setCURRCD(String CURRCD) {
        this.CURRCD = CURRCD;
    }

    /**
     * 데이터 종류
     * <p>데이터 종류 (1:은행계좌, 2:은행거래, 3:카드승인, 4:카드청구, 5:현금영수증)</p>
     * @return
     */
    public int getTYPE() {
        return TYPE;
    }

    /**
     * 데이터 종류
     * <p>데이터 종류 (1:은행계좌, 2:은행거래, 3:카드승인, 4:카드청구, 5:현금영수증)</p>
     * @param TYPE
     */
    public void setTYPE(int TYPE) {
        this.TYPE = TYPE;
    }

    public String getUPLOADID() {
        return UPLOADID;
    }

    public void setUPLOADID(String UPLOADID) {
        this.UPLOADID = UPLOADID;
    }
}
