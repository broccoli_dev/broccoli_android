package com.nomadconnection.broccoli.data.Remit;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class ResponseRemitARS extends RemitResult implements Serializable{
    private String accountId;

    /**
     * 계좌아이디
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌아이디
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
