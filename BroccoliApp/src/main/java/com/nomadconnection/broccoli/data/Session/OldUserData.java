package com.nomadconnection.broccoli.data.Session;

/**
 * Created by YelloHyunminJang on 2016. 12. 27..
 */

public class OldUserData {

    private String userId;
    private String password;
    private String newPassword;
    private String email;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }
}
