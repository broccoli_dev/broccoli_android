package com.nomadconnection.broccoli.data.Dayli;

/**
 * Created by YelloHyunminJang on 2016. 12. 27..
 */

public class DayliLoginData {

    private String email;
    private String password;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
