package com.nomadconnection.broccoli.data.Remit;

import com.nomadconnection.broccoli.data.Result;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 19..
 */
public class RequestDataARSCheck extends Result implements Serializable{
    private String userId = "";
    private String accountId = "";
    private String failCount = "";

    /**
     * 유저아이디
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 유저아이디
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 계좌아이디(코인원계좌)
     * @return
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * 계좌아이디(코인원계좌)
     * @param accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * 인증실패시
     * @return
     */
    public String getFailCount() {
        return failCount;
    }

    /**
     * 인증실패시
     * @param failCount
     */
    public void setFailCount(String failCount) {
        this.failCount = failCount;
    }
}
