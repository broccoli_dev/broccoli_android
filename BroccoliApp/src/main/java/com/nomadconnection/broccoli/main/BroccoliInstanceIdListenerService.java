package com.nomadconnection.broccoli.main;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by YelloHyunminJang on 16. 3. 14..
 */
public class BroccoliInstanceIdListenerService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Intent intent = new Intent(this, BroccoliRegistrationIntentService.class);
        startService(intent);
    }
}
