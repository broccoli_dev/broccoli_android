package com.nomadconnection.broccoli;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.gson.Gson;
import com.nomadconnection.broccoli.activity.Splash;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.CloudMessageData;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

public class GCMIntentService extends GCMBaseIntentService {
	/* Google API consol project number */
	public static final String SEND_ID = "664545738078";

	private static final String GCM = "gcm";
	private static final String MESSAGE = "message";

	public static Activity CALLER;

	Gson gson = new Gson();

	//===================================================================//
// constructor/interface 
//===================================================================//
	public interface OnGcmIdResponseReceiver {
		public void onGcmIdResponse(String _sGcmId);
	}

	public GCMIntentService() {
		this(SEND_ID);
	}

	public GCMIntentService(String senderId) {
		super(senderId);
	}

//===================================================================//
// override method
//===================================================================//

	/**
	 * GCM 메시지 받으면  - 1
	 **/
	@Override
	protected void onMessage(Context context, Intent intent) {

		String type = "";
		String title = "";
		String content = "message empty";
		Bundle bundle = intent.getExtras();
		String gcm = bundle.getString(GCM);
		if (gcm == null || gcm.isEmpty()) {
			gcm = bundle.getString(MESSAGE);
		}

		CloudMessageData data = gson.fromJson(gcm, CloudMessageData.class);

		/* GCM 메시지 처리 함수 */
		showMessage(context, data);

	}


	/**
	 * GCM 오류
	 **/
	@Override
	protected void onError(Context _context, String _sError) {
		sendGcmIdSend(null);
	}

	/**
	 * GCM 등록
	 **/
	@Override
	protected void onRegistered(Context _context, String _sGcmId) {
		sendGcmIdSend(_sGcmId);
	}

	/**
	 * GCM 해지
	 **/
	@Override
	protected void onUnregistered(Context _context, String _sGcmId) {
	}

	@Override
	protected boolean onRecoverableError(Context _context, String _sError) {
		return super.onRecoverableError(_context, _sError);
	}

//===================================================================//
// private method
//===================================================================//

	/**
	 * GCM 등록 결과를 핸들러로 전송
	 */
	private void sendGcmIdSend(String _sGcmId) {
		if (CALLER != null) {
			((OnGcmIdResponseReceiver) CALLER).onGcmIdResponse(_sGcmId);
		}
	}


	/**
	 * GCM 메시지 받으면
	 **/
	private void showMessage(Context context, CloudMessageData data) {

		if (!APPSharedPrefer.getInstance(context).getPrefBool(Const.PREFERENCE_SETTING_PUSH, true)) {
			return;
		}

		Intent notificationIntent;
		String content = null;
		String type = null;
		if (data != null) {
			content = data.getContent();
			type = data.getType();
		}

		// message type check
		if (type != null) {
			if (CloudMessageData.TRANSFER.equalsIgnoreCase(type)) {
				if (Session.getInstance(context).isForeground()) {
					DialogUtils.showReceivedMoneyDialog(context, 200, content);
				} else {
					notificationIntent = new Intent(context, Splash.class);
					makeNotification(notificationIntent, content);
				}
			} else if (CloudMessageData.NOTICE.equalsIgnoreCase(type)) {
				if (Session.getInstance(context).isForeground()) {
					DialogUtils.showGcmDialog(context, 300, content);
				} else {
					notificationIntent = new Intent(context, Splash.class);
					makeNotification(notificationIntent, content);
				}
			} else {
				if (Session.getInstance(context).isForeground()) {
					// nothing
				} else {
					notificationIntent = new Intent(context, Splash.class);
					makeNotification(notificationIntent, content);
				}
			}
		} else {
			// if type is null or empty show normal noti
			if (Session.getInstance(context).isForeground()) {
				// nothing
			} else {
				notificationIntent = new Intent(context, Splash.class);
				makeNotification(notificationIntent, content);
			}
		}

	}


	/**
	 * Notification Setting
	 **/
	private void makeNotification(Intent notificationIntent, String msg) {
		int mCode = 0;
//		int mCode = (int)System.currentTimeMillis();

		/* Noti 클릭시 이벤트 (Frag_XXX 상태유지용)*/
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), mCode, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		/* Noti Message */
		Notification notification = new NotificationCompat.Builder(getBaseContext())
				.setTicker("Broccoli")
				.setContentTitle("Broccoli")
				.setContentText(msg)
				.setSmallIcon(ElseUtils.getNotificationIcon())
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notification_large))
				.setAutoCancel(true)
				.setContentIntent(pendingIntent)
				.setWhen(System.currentTimeMillis())
				.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
				.setPriority(NotificationCompat.PRIORITY_MAX)
				.build();

		// GCM 받았을때 진동/벨 선택 (Shared Preference 기준)
		String mAlarmType = MainApplication.mAPPShared.getPrefString(APP.SP_SETTING_2);
		if (mAlarmType.equals("1")) {
			notification.defaults |= Notification.DEFAULT_SOUND;
		} else if (mAlarmType.equals("2")) {
			notification.defaults |= Notification.DEFAULT_VIBRATE;
		} else {
			notification.defaults |= Notification.DEFAULT_ALL;
		}

		NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(mCode, notification);
	}

}