package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Spend.BodySpendCategoryList;
import com.nomadconnection.broccoli.data.Spend.RequestDataByDate;
import com.nomadconnection.broccoli.fragment.FragmentSpendCategory0;
import com.nomadconnection.broccoli.fragment.FragmentSpendEmpty;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level2ActivitySpendCategory extends BaseActivity implements InterfaceEditOrDetail {

	private static final String TAG = Level2ActivitySpendCategory.class.getSimpleName();

	public static int mCurrentFragmentIndex = 0;	// default
	//	private final int FRAGMENT_ONE 		= 0;		// 추가, 편집
	private final int FRAGMENT_TWO 			= 1;		// 상세
	private final int FRAGMENT_THREE 		= 2;		// 빈페이지

	/* Get Parcel Data */
	//private ResponseSpendCategoryList mSpendCategoryList;
	private List<BodySpendCategoryList> mAlInfoDataList;
	private String mSelectDate = null;

	/* Layout */

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_spend_category);
		sendTracker(AnalyticsName.Level2ActivitySpendCategory);
		/* Title */
		((RelativeLayout)findViewById(R.id.rl_navi_img_bg)).setBackgroundResource(R.color.main_2_layout_bg);
		((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.spend_category));

			/* Layout 선언 */
		((LinearLayout)findViewById(R.id.ll_navi_img_txt_back)).setOnClickListener(BtnClickListener);

		spend_category_check();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}



	
	
	

//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {    	
		try
		{
			switch (_view.getId()) {

			case R.id.ll_navi_img_txt_back:
//				finish();
				startActivity(new Intent(this, Level3ActivitySpendCategoryDetail.class));
				break;

			default:
				break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());			
		}
	}
	
	
	
	
	
	
	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
//			if(mAlInfoDataList.size() != 0){
				mCurrentFragmentIndex = 1;
//			}
//			else{
//				mCurrentFragmentIndex = 2;
//			}
			fragmentReplace(mCurrentFragmentIndex);
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Button
//===============================================================================//
	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

				case R.id.ll_navi_img_txt_back:
					finish();
					break;

				default:
					break;
			}

		}
	};


//=========================================================================================//
// Fragment Navi
//=========================================================================================//
	/** Fragment 변경 **/
	private void fragmentReplace(int reqNewFragmentIndex)
	{
		try
		{
			Fragment newFragment = null;
			newFragment = getFragment(reqNewFragmentIndex);

			// replace fragment
			final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fl_level_2_activity_spend_category_container, newFragment);

			// Commit the transaction
			transaction.commitAllowingStateLoss();

		}
		catch (Exception e)
		{}
	}

	/** Fragment Get **/
	private Fragment getFragment(int idx)
	{
		Fragment newFragment = null;

		switch (idx) {
//		case FRAGMENT_ONE:	// 추가/편집
//			newFragment = new FragmentAssetCreditDetail0();
//			break;

			case FRAGMENT_TWO:	// 상세
				newFragment = new FragmentSpendCategory0();
				break;

			case FRAGMENT_THREE:
				newFragment = new FragmentSpendEmpty();
				break;

			default:
				break;
		}

		return newFragment;
	}



//=========================================================================================//
// Interface
//=========================================================================================//
	@Override
	public void onEdit() {
		// TODO Auto-generated method stub
		mCurrentFragmentIndex = 1;
		fragmentReplace(mCurrentFragmentIndex);
	}

	@Override
	public void onDetail() {
		// TODO Auto-generated method stub
		mCurrentFragmentIndex = 1;
		fragmentReplace(mCurrentFragmentIndex);
	}

	@Override
	public void onError() {
		// TODO Auto-generated method stub
//		mCurrentFragmentIndex = 0;
//		fragmentReplace(mCurrentFragmentIndex);
	}

	@Override
	public void onRefresh() {

	}


	//=========================================================================================//
// Init
//=========================================================================================//
	private  void  init(){
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}




//=========================================================================================//
// Category get API
//=========================================================================================//
	private void spend_category_check(){
		long now = System.currentTimeMillis();
		Date date = new Date(now);
		SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyyMM");
		mSelectDate = CurDateFormat.format(date);

		RequestDataByDate userDate = new RequestDataByDate();
		userDate.setUserId(Session.getInstance(this).getUserId());
		userDate.setDate(mSelectDate);


		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<List<BodySpendCategoryList>> call = api.getCategory(userDate);
		call.enqueue(new Callback<List<BodySpendCategoryList>>() {
			@Override
			public void onResponse(Call<List<BodySpendCategoryList>> call, Response<List<BodySpendCategoryList>> response) {
				mAlInfoDataList = new ArrayList<BodySpendCategoryList>();
				mAlInfoDataList = response.body();
				init();
			}

			@Override
			public void onFailure(Call<List<BodySpendCategoryList>> call, Throwable t) {
				ElseUtils.network_error(Level2ActivitySpendCategory.this);

			}
		});

	}
}
