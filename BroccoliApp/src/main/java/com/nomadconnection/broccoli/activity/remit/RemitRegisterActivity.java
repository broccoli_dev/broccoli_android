package com.nomadconnection.broccoli.activity.remit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Intro;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.fragment.remit.FragmentRemitRegisterAccount;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;

public class RemitRegisterActivity extends BaseActivity implements View.OnClickListener {

	private TextView guide;
	private LinearLayout mStepFirst;
	private LinearLayout mStepSecond;
	private LinearLayout mStepThird;
	private LinearLayout mStepFourth;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remit_register);

		init();
	}

	void init() {
		guide = (TextView) findViewById(R.id.tv_activity_remit_register_guide);
		mStepFirst = (LinearLayout) findViewById(R.id.ll_activity_remit_register_step_first);
		mStepSecond = (LinearLayout) findViewById(R.id.ll_activity_remit_register_step_second);
		mStepThird = (LinearLayout) findViewById(R.id.ll_activity_remit_register_step_third);
		mStepFourth = (LinearLayout) findViewById(R.id.ll_activity_remit_register_step_fourth);

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction ft = fragmentManager.beginTransaction();
		Fragment newFragment = null;
		newFragment = new FragmentRemitRegisterAccount();
		Bundle mBundle = new Bundle();
		mBundle.putBoolean(Const.REMIT_REGISTER, true);
		newFragment.setArguments(mBundle);
		ft.add(R.id.fragment_remit_area, newFragment);
		ft.commit();
	}

	public void setStepGuide(int step) {
		mStepFirst.setPressed(false);
		mStepSecond.setPressed(false);
		mStepThird.setPressed(false);
		mStepFourth.setPressed(false);
		switch (step) {
			case 0:
				mStepFirst.setSelected(false);
				mStepFirst.setPressed(true);
				mStepSecond.setSelected(false);
				mStepThird.setSelected(false);
				mStepFourth.setSelected(false);
				guide.setText(R.string.remit_register_text_step_guide_first);
				break;
			case 1:
				mStepFirst.setSelected(true);
				mStepSecond.setSelected(false);
				mStepSecond.setPressed(true);
				mStepThird.setSelected(false);
				mStepFourth.setSelected(false);
				guide.setText(R.string.remit_register_text_step_guide_fourth);
				break;
			case 2:
				mStepFirst.setSelected(true);
				mStepSecond.setSelected(true);
				mStepThird.setSelected(false);
				mStepThird.setPressed(true);
				mStepFourth.setPressed(false);
				guide.setText(R.string.remit_register_text_step_guide_third);
				break;
			case 3:
				mStepFirst.setSelected(true);
				mStepSecond.setSelected(true);
				mStepThird.setSelected(true);
				mStepFourth.setPressed(true);
				guide.setText(R.string.remit_register_text_step_guide_fourth);
				break;
		}
	}

	public void setGuideText(String text) {
		guide.setText(text);
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			//cancel_popup();
			onBackPressed();
		}
		return super.onKeyDown(keyCode, event);
	}

	public void onBtnClickNaviImgTxtBar(View view) {
		super.onBackPressed();
//		cancel_popup();
	}
	
	public int getFragmentArea() {
		return R.id.fragment_remit_area;
	}

	private void cancel_popup(){
		final String mStr = getString(R.string.membership_popup_cancel);
		DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
							Intent intro = new Intent(RemitRegisterActivity.this, Intro.class);
							intro.putExtra(Const.DATA, "intro");
							startActivity(intro);
							finish();
						}
					}
				},
				new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
	}
}
