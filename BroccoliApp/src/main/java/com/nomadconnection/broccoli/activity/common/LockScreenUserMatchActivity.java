package com.nomadconnection.broccoli.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.membership.MembershipFindPwdActivity;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingPassword;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 1. 4..
 */

public class LockScreenUserMatchActivity extends BaseActivity implements View.OnClickListener {

    public static final String PIN_ERROR_OVERTIME = "PIN_ERROR_OVERTIME";

    private EditText mPwd;
    private View mBtn;
    private boolean isPINErrorOver = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lockscreen_usermatch);
        init();
    }

    private void init() {
        Intent intent = getIntent();
        if (intent.hasExtra(PIN_ERROR_OVERTIME)) {
            isPINErrorOver = true;
            findViewById(R.id.ll_navi_img_txt_back).setVisibility(View.GONE);
            try {
                DbManager.getInstance().updateLoginStatus(this, Session.getInstance(this).getUserId(), LoginData.PIN_ERROR);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView) findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_user_info_pin_password_reset);
        final TextView email = (TextView) findViewById(R.id.tv_activity_dayli_pwd_change_email);
        UserPersonalInfo info = Session.getInstance(this).getUserPersonalInfo();
        if (info != null && info.user != null) {
            email.setText(Session.getInstance(this).getUserPersonalInfo().user.email);
        } else {
            DialogUtils.showLoading(this);
            Session.getInstance(this).refreshUserPersonalInfo(new Session.OnUserInfoUpdateCompleteInterface() {
                @Override
                public void complete(UserPersonalInfo info) {
                    DialogUtils.dismissLoading();
                    if (info != null) {
                        email.setText(info.user.email);
                    }
                }

                @Override
                public void error(ErrorMessage message) {
                    switch (message) {
                        case ERROR_713_NO_SUCH_USER:
                        case ERROR_10007_SESSION_NOT_FOUND:
                        case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                        case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                        case ERROR_20004_NO_SUCH_USER:
                            showTokenExpire();
                            break;
                    }
                }
            });
        }

        findViewById(R.id.rl_activity_dayli_find_pwd_btn).setOnClickListener(this);
        mBtn = findViewById(R.id.btn_confirm);
        mBtn.setOnClickListener(this);
        mBtn.setEnabled(false);

        mPwd = (EditText) findViewById(R.id.et_activity_dayli_pwd_change_cert);
        mPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 2) {
                    mBtn.setEnabled(true);
                } else {
                    mBtn.setEnabled(false);
                }
            }
        });

    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.rl_activity_dayli_find_pwd_btn:
                showFindPwd();
                break;
            case R.id.btn_confirm:
                matchPwd();
                break;
        }
    }

    private void showFindPwd() {
        Intent intent = new Intent(this, MembershipFindPwdActivity.class);
        startActivity(intent);
    }

    private int mPwdErrorCount = 0;

    private void matchPwd() {
        DialogUtils.showLoading(this);
        final String pwd = mPwd.getText().toString();
        DayliApi api = ServiceGenerator.createServiceDayli(DayliApi.class);
        long id = DbManager.getInstance().getDayliId(this, Session.getInstance(this).getUserId());
        String token = DbManager.getInstance().getUserDayliToken(this, Session.getInstance(this).getUserId());
        Call<Result> call = api.dayliPasswordCheck(token, id, pwd);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    changePin();
                } else {
                    ErrorMessage message = ErrorUtils.parseError(response);
                    switch (message) {
                        case ERROR_701_INCORRECT_ACCOUNT:
                            Toast.makeText(getBase(), "비밀번호가 일치하지 않습니다. 다시 입력해 주세요.", Toast.LENGTH_LONG).show();
                            mPwdErrorCount++;
                            mPwd.setText(null);
                            mPwd.requestFocus();
                            break;
                        case ERROR_719_QUITE_USER:
                            Toast.makeText(getBase(), "탈퇴 회원은 탈퇴일로부터 5일 이후 재가입 가능합니다.", Toast.LENGTH_LONG).show();
                            break;
                        case ERROR_713_NO_SUCH_USER:
                            showLoginActivity();
                            break;
                        case ERROR_716_TOKEN_EXPIRE:
                            showTokenExpire();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getBase());
            }
        });
    }

    private void changePin() {
        if (isPINErrorOver) {
            try {
                DbManager.getInstance().updateLoginStatus(this, Session.getInstance(this).getUserId(), LoginData.TRUE);
                ElseUtils.setLockDownTime(getApplicationContext(), null);
                ElseUtils.setLockOverCount(getApplicationContext(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Intent intent = new Intent(this, Level3ActivitySettingPassword.class);
        intent.putExtra(Const.FIND_PIN, true);
        startActivity(intent);
        finish();
    }
}
