package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lumensoft.ks.KSCertificate;
import com.lumensoft.ks.KSCertificateLoader;
import com.lumensoft.ks.KSException;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.AdtListViewCertList;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoliraonsecure.KSW_CertItem;
import com.nomadconnection.broccoliraonsecure.KSW_CertListManager;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Vector;

public class CertListActivity extends BaseActivity implements OnClickListener {

    public static final int CERT_PASSWORD = 1;
    public static final int ID_PASSWORD = 2;
    public static final int RESULT_ID_OK = 100;

    private TextView mHeaderTitle;
	private ListView mListView;
	private View mIdLogin;
	private View mEmptyLayout;
	private AdtListViewCertList mAdapter;
	//인증서정보 리스트
	private ArrayList<KSW_CertItem> mCertList = null;
	private ArrayList<SelectBankData> mSelectedBankData;
	private Vector<?> userCerts = null;
	private boolean fromMembership = false;
	private boolean isReplace = false;
	private String mExpire;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cert);
		init();
	}

	private void init() {
		initWidget();
		initData();
	}

	private void initWidget() {
		mHeaderTitle = (TextView) findViewById(R.id.tv_cert_header_title);
		mListView = (ListView) findViewById(R.id.lv_CertList);
		mIdLogin = findViewById(R.id.tv_cert_id_login);
		mEmptyLayout = findViewById(R.id.fl_empty_layout);
		mEmptyLayout.setVisibility(View.GONE);
		mIdLogin.setOnClickListener(this);
		findViewById(R.id.fl_cert_copy_guide_btn).setOnClickListener(this);

		findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
		TextView title = (TextView) findViewById(R.id.tv_navi_img_txt_title);
		title.setText(R.string.membership_text_cert_title);
	}

	private void initData() {
		Intent intent = getIntent();
		if (intent != null) {
			if(intent.getExtras() != null) {
				mSelectedBankData = (ArrayList<SelectBankData>) intent.getSerializableExtra(Const.DATA);
				fromMembership = intent.getBooleanExtra(Const.FROM_MEMBERSHIP, false);
				String title = intent.getStringExtra(Const.TITLE);
				if (title != null) {
					isReplace = true;
					TextView titleView = (TextView) findViewById(R.id.tv_navi_img_txt_title);
					titleView.setText(title);
				}
			}
		}

		if (fromMembership) {
			sendTracker(AnalyticsName.UserSelectBank);
		} else if (isReplace) {
			sendTracker(AnalyticsName.BankReplace);
		} else {
			sendTracker(AnalyticsName.SettingSelectBank);
		}

		String title = new String();
		boolean isContainBank = false;
		if (mSelectedBankData != null) {
			for (int i=0; i < mSelectedBankData.size(); i++) {
				SelectBankData data = mSelectedBankData.get(i);
				if (BankData.TYPE_BANK.equalsIgnoreCase(data.getType())) {
					isContainBank = true;
				}
				if (i == 0) {
					title += data.getName();
				} else if (i <= 2) {
					title += " / "+data.getName();
				}
				if ((i == mSelectedBankData.size()-1)) {
					if (i >= 2 && mSelectedBankData.size() >= 4) {
						title += " "+String.format(getString(R.string.membership_text_cert_list_title), String.valueOf(i-2));
					} else if (i < 3) {
						title += " 등록";
					}
				}
			}
		}
		if (!isContainBank) {
			mIdLogin.setVisibility(View.VISIBLE);
		} else {
			mIdLogin.setVisibility(View.GONE);
		}
		mHeaderTitle.setText(title);
		loadCertListInApp(false);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == CERT_PASSWORD && resultCode == RESULT_OK) {
			if (data != null) {
				Intent intent = new Intent();
				intent.putExtra(Const.CERT_NAME, data.getStringExtra(Const.CERT_NAME));
				intent.putExtra(Const.CERT_PWD, data.getStringExtra(Const.CERT_PWD));
				intent.putExtra(Const.CERT_VALIDDATE, mExpire);
				setResult(RESULT_OK, intent);
				finish();
			}
		} else if (requestCode == ID_PASSWORD) {
			if (resultCode == RESULT_OK ) {
				setResult(RESULT_ID_OK);
				finish();
			}
        }
	}

	public void onBtnClickNaviImgTxtBar(View view) {
		super.onBackPressed();
	}

	private void loadCertListInApp(boolean loadApp) {
		try {
			// 앱내 인증서 보기 체크 선택/해제 시 리스트 초기
			if (userCerts != null) {
				userCerts.clear();
				if (mCertList != null)
					mCertList.clear();
			}

			// 인증서 읽기
			if (loadApp)
				// 앱내 저장된 NPKI 인증서 로드
//				userCerts = KSCertificateLoader.getCertificateListfromAppAsVector(KSW_Activity_CertList.this);
				// 앱내 저장된 NPKI / GPKI 인증서 로드
				userCerts = KSCertificateLoader.getCertificateListfromAppWithGpkiAsVector(CertListActivity.this);
			else
				// SD카드 내 저장된 NPKI 인증서 로드
//				userCerts = KSCertificateLoader.getCertificateListfromSDCardAsVector(KSW_Activity_CertList.this);
				// SD카드 내 저장된 NPKI / GPKI 인증서 로드
				userCerts = KSCertificateLoader.getCertificateListfromSDCardWithGpkiAsVector(CertListActivity.this);


//			KSW_Filter.setIssuerCertFilter(issuerCertFilter);
//			KSW_Filter.setPolicyOidCertFilter(policyOidCertFilter);
//			userCerts = KSCertificateLoader.issuerCertFilter(userCerts, KSW_Filter.getIssuerCertFilter());
//			userCerts = KSCertificateLoader.policyOidCertFilter(userCerts, KSW_Filter.getPolicyOidCertFilter());
			// 발급기관과 정책에 따른 인증서 필터링 끝
		} catch (KSException e) {
			Toast.makeText(getApplicationContext(), "인증서 목록 초기화 실패 인증서 목록 생성에 실패하였습니다.", Toast.LENGTH_LONG).show();
		}

		if (userCerts == null || userCerts.size() == 0) {
			mEmptyLayout.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.INVISIBLE);
		} else {
			mEmptyLayout.setVisibility(View.GONE);
			mListView.setVisibility(View.VISIBLE);
			mCertList = new ArrayList<KSW_CertItem>();
			for (int i = 0; i < userCerts.size(); i++) {
				try {
					mCertList.add(new KSW_CertItem((KSCertificate) userCerts.elementAt(i)));
				} catch (UnsupportedEncodingException e) {
					Toast.makeText(this, "인증서 목록 초기화 실패 인증서 목록 생성에 실패하였습니다.", Toast.LENGTH_LONG).show();
				}
			}
			mAdapter = new AdtListViewCertList(this,
					mCertList);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

					KSCertificate userCert = (KSCertificate) userCerts.elementAt(position);
					// 선택한 인증서는 KSW_CertListManager.setSelectedCert에 setting
					KSW_CertListManager.setSelectedCert(userCert);
					String certNM = userCert.getSubjectName();

					if (userCert.isExpired()) {
						Toast.makeText(CertListActivity.this, "기간이 만료된 인증서입니다.", Toast.LENGTH_SHORT).show();
						return;
					} else {
						mExpire = userCert.getExpiredTime();
					}

					if (certNM != null) {
						if (isReplace) {
							sendTracker(AnalyticsName.BankReplaceByCert);
						}
						Intent intent = new Intent(CertListActivity.this, CertPasswordActivity.class);
						intent.putExtra(Const.CERT_NAME, certNM);
						intent.putExtra(Const.FROM_MEMBERSHIP, fromMembership);
						startActivityForResult(intent, CERT_PASSWORD);
					}
				}
			});
		}
	}

	@Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
			case R.id.fl_cert_copy_guide_btn:
				Intent guideAuthIntent = new Intent(this, GuideAuthCopyActivity.class);
				guideAuthIntent.putExtra(Const.FROM_MEMBERSHIP, fromMembership);
				startActivity(guideAuthIntent);
				break;
			case R.id.tv_cert_id_login:
				if (isReplace) {
					sendTracker(AnalyticsName.BankReplaceById);
				}
				Intent intent = new Intent(this, IdPasswordLoginActivity.class);
				intent.putExtra(Const.FROM_MEMBERSHIP, fromMembership);
				intent.putExtra(Const.DATA, mSelectedBankData);
				startActivityForResult(intent, ID_PASSWORD);
				break;
        }
    }
}
