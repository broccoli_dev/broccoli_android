package com.nomadconnection.broccoli.activity.setting;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lumensoft.ks.KSCertificateLoader;
import com.lumensoft.ks.KSException;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.CertListActivity;
import com.nomadconnection.broccoli.activity.IdPasswordLoginActivity;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.financial.LinkageFinancialSettingActivity;
import com.nomadconnection.broccoli.adapter.AdtListViewFinancialRegList;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoli.data.Session.UserFinancialData;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.service.BatchService;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.service.ScrapingModule;
import com.nomadconnection.broccoli.service.data.ScrapRequester;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoliespider.data.BankRequestData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 16. 3. 10..
 * 사용자 금융 기관 연동
 */
public class Level3ActivitySettingFinancialReg extends BaseActivity implements View.OnClickListener {

    public static final int RESULT_FROM_SETTING = 1;
    public static final int RESULT_FROM_IDPW = 2;
    public static final int REUSLT_FROM_FINANCIAL = 3;

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private ArrayList<String> mGroupList = null;
    private HashMap<String, ArrayList<SelectBankData>> mChildList = null;
    private ArrayList<SelectBankData> mChildListContent1 = null;
    private ArrayList<SelectBankData> mChildListContent2 = null;
    private ArrayList<SelectBankData> mChildListContent3 = null;
    private View mEmptyView;
    private ExpandableListView mExpListView;
    private ArrayList<SelectBankData> mAlInfoDataList;
    private AdtListViewFinancialRegList mAdapter;
    private TextView mSynchronize;
    private TextView mHeaderCount;
    private TextView mTitle;
    private TextView mSettingBtn;
    private int mCertCount;
    private ProcessMode mMode = ProcessMode.COLLECT_IDLE;
    private boolean isConnection = false;
    private Messenger			mService;
    private ServiceConnection 	mServiceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_setting_financial_reg);
        sendTracker(AnalyticsName.SettingRegBankList);
        initWidget();
        initBinderService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestQueueList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_fragment_membership_cert:
                startAddFinancial();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        finishBinderService();
        super.onDestroy();
    }

    private void initWidget() {
        mGroupList = new ArrayList<String>();
        mChildList = new HashMap<String, ArrayList<SelectBankData>>();
        mChildListContent1 = new ArrayList<SelectBankData>();
        mChildListContent2 = new ArrayList<SelectBankData>();
        mChildListContent3 = new ArrayList<SelectBankData>();
        findViewById(R.id.rl_navi_img_txt_txt_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        mTitle = (TextView) findViewById(R.id.tv_navi_img_txt_txt_title);
        mTitle.setText(R.string.setting_financial_reg_title);
        mSettingBtn = (TextView) findViewById(R.id.tv_navi_img_txt_txt_else);
        mSettingBtn.setText(R.string.setting_financial_reg_link_setting);
        View view = findViewById(R.id.btn_fragment_membership_cert);
        view.setOnClickListener(this);
        mSynchronize =(TextView) findViewById(R.id.tv_activity_setting_financial_reg_synchronize);
        mHeaderCount = (TextView) findViewById(R.id.tv_fragment_financial_header_count);
        mExpListView = (ExpandableListView) findViewById(R.id.lv_fragment_financial_explistview);
        mEmptyView = findViewById(R.id.fl_financial_reg_empty);
        ((TextView)findViewById(R.id.tv_empty_layout_txt)).setText(R.string.setting_financial_empty_content);
        mAdapter = new AdtListViewFinancialRegList(this, mGroupList, mChildList, listener);
        mExpListView.setAdapter(mAdapter);

         /* group list click 시 */
        mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });

        mExpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectBankData mTemp = null;
                mTemp = mAlInfoDataList.get(position);
                deleteDialog(mTemp);
            }
        });
        mAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                int count = 0;
                if (mAlInfoDataList != null) {
                    count = mAlInfoDataList.size();
                }
                mHeaderCount.setText(Integer.toString(count) + getString(R.string.common_count));
            }
        });

        String time = MainApplication.mAPPShared.getPrefString(APP.SP_CHECK_SCRAPING_DATA_TIME);
        String synchronize_time = getString(R.string.common_synchronize) + " " + time;
        if("".equals(time)){
            mSynchronize.setVisibility(View.GONE);
        }
        else {
            mSynchronize.setVisibility(View.VISIBLE);
            mSynchronize.setText(synchronize_time);
        }
    }

    private int getCertCount() {
        int count = 0;
        try {
            Vector<?> userCerts = KSCertificateLoader.getCertificateListfromSDCardWithGpkiAsVector(this);
            if (userCerts != null && userCerts.size() > 0) {
                count = userCerts.size();
            }
        } catch (KSException e) {
            e.printStackTrace();
        }

        return count;
    }

    AdtListViewFinancialRegList.OnMoreItemClickListener listener = new AdtListViewFinancialRegList.OnMoreItemClickListener() {

        @Override
        public void refresh(SelectBankData data) {
            if (data != null) {
                if (!data.isLoading()) {
                    String status = data.getBankStatus();
                    if (BankData.FAIL_PASSWORD.equalsIgnoreCase(status)) {
                        ArrayList<SelectBankData> selectedList = new ArrayList<SelectBankData>();
                        selectedList.add(data);
                        Intent intent = new Intent(Level3ActivitySettingFinancialReg.this, IdPasswordLoginActivity.class);
                        intent.putExtra(Const.DATA, selectedList);
                        startActivityForResult(intent, RESULT_FROM_IDPW);
                    } else {
                        data.setLoading(true);
                        mAdapter.notifyDataSetChanged();
                        retryBackgroundService(data);
                        Toast.makeText(Level3ActivitySettingFinancialReg.this, getString(R.string.setting_financial_retry_start), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        @Override
        public void setting(SelectBankData data) {
            if (data != null) {
                if (data.isLoading()) {
                    Toast.makeText(getActivity(), R.string.setting_financial_status_loading, Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<SelectBankData> selectedList = new ArrayList<SelectBankData>();
                    selectedList.add(data);
                    Intent intent = new Intent(Level3ActivitySettingFinancialReg.this, CertListActivity.class);
                    intent.putExtra(Const.TITLE, getString(R.string.setting_financial_more_menu_setting));
                    intent.putExtra(Const.DATA, selectedList);
                    startActivityForResult(intent, RESULT_FROM_SETTING);
                }
            }
        }

        @Override
        public void delete(SelectBankData data) {
            if (data != null) {
                if (data.isLoading()) {
                    Toast.makeText(getActivity(), R.string.setting_financial_status_loading, Toast.LENGTH_SHORT).show();
                } else {
                    deleteDialog(data);
                }
            }
        }
    };

    private void postInit(final ArrayList<ScrapRequester> list) {
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                init(list, str);
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {

            }
        });
    }

    private void init(ArrayList<ScrapRequester> list, String key) {

        ArrayList<BankRequestData> bankCodeList = new ArrayList<>();
        for (ScrapRequester requester : list) {
            List<BankRequestData> bankRequestDatas = requester.getData();
            for (BankRequestData bank : bankRequestDatas) {
                bankCodeList.add(bank);
            }
        }

        mGroupList.clear();
        mChildList.clear();
        mChildListContent1.clear();
        mChildListContent2.clear();
        mChildListContent3.clear();
        mAlInfoDataList = new ArrayList<SelectBankData>();

        List<BankData> bankDataList = ScrapDao.getInstance().getBankData(this, key, Session.getInstance(getApplicationContext()).getUserId());
        final String bankStatus = getString(R.string.setting_financial_cert_error);
        for (BankData data : bankDataList) {
            if (BankData.TYPE_BANK.equals(data.getType())) {
                SelectBankData selectBankData = new SelectBankData();
                selectBankData.setType(SelectBankData.TYPE_BANK);
                selectBankData.setCategoryGroup(getString(R.string.membership_text_second_cert_bank));
                selectBankData.setTypeName(getString(R.string.membership_text_second_cert_bank));
                selectBankData.setBankStatus(data.getStatus());
                String bankName = data.getBankName();
                selectBankData.setName(bankName);
                selectBankData.setCode(data.getBankCode());
                selectBankData.setValidDate(data.getValidDate());
                selectBankData.setLoginMethod(data.getLoginMethod());
                for (int i=0; i < bankCodeList.size(); i++) {
                    BankRequestData bankRequestData = bankCodeList.get(i);
                    if (BankData.TYPE_BANK.equalsIgnoreCase(bankRequestData.getOriginalBankType())
                            && bankRequestData.getOriginalBankCode().equalsIgnoreCase(selectBankData.getCode())) {
                        selectBankData.setLoading(true);
                    }
                }
//                if (!BankData.NONE.equals(selectBankData.getBankStatus()) && !BankData.SCRAPING.equals(selectBankData.getBankStatus())) {
//                    selectBankData.setCategoryGroup(bankStatus);
//                } else if (BankData.SCRAPING.equals(selectBankData.getBankStatus()) && !selectBankData.isLoading()) {
//                    selectBankData.setCategoryGroup(bankStatus);
//                }
                mAlInfoDataList.add(selectBankData);
            } else if (BankData.TYPE_CARD.equals(data.getType())) {
                SelectBankData selectBankData = new SelectBankData();
                selectBankData.setType(SelectBankData.TYPE_CARD);
                selectBankData.setCategoryGroup(getString(R.string.membership_text_second_cert_card));
                selectBankData.setTypeName(getString(R.string.membership_text_second_cert_card));
                selectBankData.setBankStatus(data.getStatus());
                String cardName = data.getBankName();
                selectBankData.setName(cardName);
                selectBankData.setCode(data.getBankCode());
                selectBankData.setValidDate(data.getValidDate());
                selectBankData.setLoginMethod(data.getLoginMethod());
                for (int i=0; i < bankCodeList.size(); i++) {
                    BankRequestData bankRequestData = bankCodeList.get(i);
                    if (BankData.TYPE_CARD.equalsIgnoreCase(bankRequestData.getOriginalBankType())
                            && bankRequestData.getOriginalBankCode().equalsIgnoreCase(selectBankData.getCode())) {
                        selectBankData.setLoading(true);
                    }
                }
//                if (!BankData.NONE.equals(selectBankData.getBankStatus()) && !BankData.SCRAPING.equals(selectBankData.getBankStatus())) {
//                    selectBankData.setCategoryGroup(bankStatus);
//                } else if (BankData.SCRAPING.equals(selectBankData.getBankStatus()) && !selectBankData.isLoading()) {
//                    selectBankData.setCategoryGroup(bankStatus);
//                }
                mAlInfoDataList.add(selectBankData);
            } else {
                SelectBankData selectBankData = new SelectBankData();
                selectBankData.setType(SelectBankData.TYPE_CASH);
                selectBankData.setCategoryGroup(getString(R.string.membership_text_second_cert_cash));
                selectBankData.setTypeName(getString(R.string.membership_text_second_cert_cash));
                selectBankData.setBankStatus(data.getStatus());
                selectBankData.setName(data.getBankName());
                selectBankData.setCode(data.getBankCode());
                selectBankData.setValidDate(data.getValidDate());
                selectBankData.setLoginMethod(data.getLoginMethod());
                for (int i=0; i < bankCodeList.size(); i++) {
                    BankRequestData bankRequestData = bankCodeList.get(i);
                    if (BankData.TYPE_CASH.equalsIgnoreCase(bankRequestData.getOriginalBankType())
                            && bankRequestData.getOriginalBankCode().equalsIgnoreCase(selectBankData.getCode())) {
                        selectBankData.setLoading(true);
                    }
                }
//                if (!BankData.NONE.equals(selectBankData.getBankStatus()) && !BankData.SCRAPING.equals(selectBankData.getBankStatus())) {
//                    selectBankData.setCategoryGroup(bankStatus);
//                } else if (BankData.SCRAPING.equals(selectBankData.getBankStatus()) && !selectBankData.isLoading()) {
//                    selectBankData.setCategoryGroup(bankStatus);
//                }
                mAlInfoDataList.add(selectBankData);
            }
        }

        /* gourp list */
//        for (int i = 0; i < mAlInfoDataList.size(); i++) {
//            if (mAlInfoDataList.get(i).getCategoryGroup().equals(bankStatus)) {
//                mGroupList.add(bankStatus);
//                break;
//            }
//        }
        for (int i = 0; i < mAlInfoDataList.size(); i++) {
            if (!mGroupList.contains(mAlInfoDataList.get(i).getCategoryGroup())) {
                mGroupList.add(mAlInfoDataList.get(i).getCategoryGroup());
            }
        }

        Collections.sort(mGroupList, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                int ret = 0;
                if ("은행".equalsIgnoreCase(s)) {
                    ret = -1;
                } else if ("은행".equalsIgnoreCase(t1)) {
                    ret = 1;
                } else if ("현금영수증".equalsIgnoreCase(s)) {
                    ret = 1;
                } else if ("현금영수증".equalsIgnoreCase(t1)) {
                    ret = -1;
                }
                return ret;
            }
        });

        for (int i = 0; i < mAlInfoDataList.size(); i++) {
            if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_BANK)) {
					/* gourp 1의 child */
                mChildListContent1.add(mAlInfoDataList.get(i));
            }
            else if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_CARD)) {
					/* gourp 2의 child */
                mChildListContent2.add(mAlInfoDataList.get(i));
            }
            else if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_CASH)) {
                mChildListContent3.add(mAlInfoDataList.get(i));
            }
        }
        Collections.sort(mChildListContent1, new Comparator<SelectBankData>() {
            @Override
            public int compare(SelectBankData bankData, SelectBankData bankData2) {
                int ret = 0;
                if (bankData.getCategoryGroup().equals(bankStatus)) {
                    ret = 1;
                } else if (bankData2.getCategoryGroup().equals(bankStatus)) {
                    ret = -1;
                }
                return ret;
            }
        });
        Collections.sort(mChildListContent2, new Comparator<SelectBankData>() {
            @Override
            public int compare(SelectBankData bankData, SelectBankData bankData2) {
                int ret = 0;
                if (bankData.getCategoryGroup().equals(bankStatus)) {
                    ret = 1;
                } else if (bankData2.getCategoryGroup().equals(bankStatus)) {
                    ret = -1;
                }
                return ret;
            }
        });


			/* child list에 각각의 content 넣기 */
//        mChildList.put(getString(R.string.membership_text_second_cert_error), mChildListContent1);
        mChildList.put(getString(R.string.membership_text_second_cert_bank), mChildListContent1);
        mChildList.put(getString(R.string.membership_text_second_cert_card), mChildListContent2);
        mChildList.put(getString(R.string.membership_text_second_cert_cash), mChildListContent3);
        mAdapter.setData(mGroupList, mChildList);
        mAdapter.notifyDataSetChanged();
        initData();
    }

    private void deleteDialog(final SelectBankData data) {
        CommonDialogTextType dialogTextType = new CommonDialogTextType(this, new String[] {"취소", "해제하기"});
        dialogTextType.addTextView(R.string.setting_financial_reg_disable, R.color.common_rgb_102_102_102, 15);
        final CheckBox checkBox = new CheckBox(this);
        checkBox.setButtonDrawable(R.drawable.selector_checkbox_four_status);
        checkBox.setText(R.string.setting_financial_reg_checkbox_content);
        checkBox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
        checkBox.setTextColor(getResources().getColor(R.color.common_subtext_color));
        int padding = ElseUtils.convertDp2Px(this, 15);
        checkBox.setPadding(0, padding, 0, padding);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        checkBox.setLayoutParams(params);
        checkBox.setGravity(Gravity.CENTER);
        FrameLayout frameLayout = new FrameLayout(this);
        LinearLayout.LayoutParams bodyLayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        bodyLayout.gravity = Gravity.CENTER;
        frameLayout.setLayoutParams(bodyLayout);
        frameLayout.addView(checkBox);
        dialogTextType.addCustomBody(frameLayout);
        dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                switch (id) {
                    case CommonDialogTextType.BUTTON_LEFT:
                        dialog.dismiss();
                        break;
                    case CommonDialogTextType.BUTTON_RIGHT:
                        if (checkBox.isChecked()) {
                            removedFinancialData(data);
                        } else {
                            Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
                                @Override
                                public void acquired(String str) {
                                    CustomToast.makeText(getApplicationContext(), data.getName() + " 기관등록이 해제되었습니다.", Toast.LENGTH_SHORT).show();
                                    ScrapDao.getInstance().deleteBankData(Level3ActivitySettingFinancialReg.this, str, Session.getInstance(getApplicationContext()).getUserId(), data.getName());
                                    requestQueueList();
                                }

                                @Override
                                public void error(ErrorMessage errorMessage) {
                                    switch (errorMessage) {
                                        case ERROR_713_NO_SUCH_USER:
                                        case ERROR_10007_SESSION_NOT_FOUND:
                                        case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                                        case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                                        case ERROR_20004_NO_SUCH_USER:
                                            showLoginActivity();
                                            break;
                                    }
                                }

                                @Override
                                public void fail() {

                                }
                            });
                        }
                        break;
                }
            }
        });
        dialogTextType.show();
    }

    private void removedFinancialData(final SelectBankData data) {
        if (data == null) return;
        DialogUtils.showLoading(Level3ActivitySettingFinancialReg.this);
        UserFinancialData financialData = new UserFinancialData();
        financialData.setUserId(Session.getInstance(Level3ActivitySettingFinancialReg.this).getUserId());
        if (SelectBankData.TYPE_BANK.equals(data.getType())) {
            financialData.setCompanyType(UserFinancialData.TYPE_BANK);
            financialData.setCompanyCode(data.getCode());
        } else if (SelectBankData.TYPE_CARD.equals(data.getType())) {
            financialData.setCompanyType(UserFinancialData.TYPE_CARD);
            financialData.setCompanyCode(data.getCode());
        } else {
            financialData.setCompanyType(UserFinancialData.TYPE_TAX);
            financialData.setCompanyCode("1003");
        }
        Call<Result> call = ServiceGenerator.createService(UserApi.class).userFinancialDelete(financialData);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        if (!Level3ActivitySettingFinancialReg.this.isFinishing()) {
                            Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
                                @Override
                                public void acquired(String str) {
                                    CustomToast.makeText(getApplicationContext(), data.getName() + " 기관등록이 해제되었습니다.", Toast.LENGTH_SHORT).show();
                                    ScrapDao.getInstance().deleteBankData(Level3ActivitySettingFinancialReg.this, str, Session.getInstance(getApplicationContext()).getUserId(), data.getName());
                                    requestQueueList();
                                }

                                @Override
                                public void error(ErrorMessage errorMessage) {
                                    switch (errorMessage) {
                                        case ERROR_713_NO_SUCH_USER:
                                        case ERROR_10007_SESSION_NOT_FOUND:
                                        case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                                        case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                                        case ERROR_20004_NO_SUCH_USER:
                                            showLoginActivity();
                                            break;
                                    }
                                }

                                @Override
                                public void fail() {

                                }
                            });
                        }
                    } else {
                        Toast.makeText(Level3ActivitySettingFinancialReg.this, "등록해제 중에 문제가 발생하였습니다. 잠시 후 재시작해주세요.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    ElseUtils.network_error(getBase());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private boolean initData() {
        boolean bResult = true;
        mCertCount = getCertCount();
        try {
            if (mAlInfoDataList == null || mAlInfoDataList.size() == 0) {
                mEmptyView.setVisibility(View.VISIBLE);
                mExpListView.setVisibility(View.GONE);
            } else {
                mEmptyView.setVisibility(View.GONE);
                mExpListView.setVisibility(View.VISIBLE);
            }
            if(mGroupList != null){
                for (int i = 0; i <mGroupList.size() ; i++) {
                    mExpListView.expandGroup(i);
                }
            }
        }
        catch(Exception e) {
            bResult = false;
            if(APP._DEBUG_MODE_)
                Log.e("broccoli", "initData() : " + e.toString());
            return bResult;
        }

        return bResult;
    }

    public void onBtnClickNaviImgTxtTxtBar(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_img_txt_txt_else:
                Intent intent = new Intent(this, LinkageFinancialSettingActivity.class);
                startActivityForResult(intent, RESULT_FROM_SETTING);
                break;
            default:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_FROM_IDPW) {
            if (resultCode == RESULT_OK) {
                requestQueueList();
                mAdapter.notifyDataSetChanged();
            }
        } else if (requestCode == RESULT_FROM_SETTING) {
            if (resultCode == RESULT_OK) {
                requestQueueList();
                mAdapter.notifyDataSetChanged();
            }
        } else if (requestCode == REUSLT_FROM_FINANCIAL) {
            if (resultCode == RESULT_OK) {
                requestQueueList();
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void insertBankData(String key, ArrayList<BankData> list) {
        if (list != null) {
            ScrapDao scrapDao = ScrapDao.getInstance();
            for (BankData data : list) {
                BankData bankData = scrapDao.getBankDataByName(this, key, data.getBankName(), Session.getInstance(getApplicationContext()).getUserId());
                if (bankData == null || bankData.getBankName() == null || TextUtils.isEmpty(bankData.getBankName())) {
                    scrapDao.insertBankData(this, key, Session.getInstance(getApplicationContext()).getUserId(), data);
                } else {
                    bankData.setCertSerial(data.getCertSerial());
                    bankData.setPassWord(data.getPassWord());
                    bankData.setValidDate(data.getValidDate());
                    scrapDao.updateBankData(this, key, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                }
            }
        }
    }

    private void startAddFinancial() {
        Intent intent = new Intent(this, Level3ActivitySettingFinancial.class);
        startActivityForResult(intent, REUSLT_FROM_FINANCIAL);
    }

    private void startBackgroundService(ArrayList<BankData> list) {
        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_INDIVIDUAL);
        intent.putExtra(Const.DATA, list);
        sendBroadcast(intent);
    }

    private void retryBackgroundService(final SelectBankData data) {
        DialogUtils.showLoading(this);
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                DialogUtils.dismissLoading();
                sendRetryMessage(data, str);
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void sendRetryMessage(SelectBankData data, String str) {
        ArrayList<BankData> list = new ArrayList<>();
        List<BankData> bankDataList = ScrapDao.getInstance().getBankData(this, str, Session.getInstance(getApplicationContext()).getUserId());
        for (BankData bank : bankDataList) {
            if (bank.getBankCode().equals(data.getCode())) {
                list.add(bank);
            }
        }
        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_INDIVIDUAL);
        intent.putExtra(Const.DATA, list);
        sendBroadcast(intent);
    }

    private void initBinderService() {
        mServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
                isConnection = false;
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                isConnection = true;
                mService = new Messenger(service);
                registerMessenger();
                requestQueueList();
            }
        };

        if(ElseUtils.isServiceRunning(this, BatchService.class.getName()) == false) {
            ComponentName compName = new ComponentName(getPackageName(), BatchService.class.getName());
            startService(new Intent().setComponent(compName));
        }
        bindService(new Intent(this, BatchService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void registerMessenger() {
        if (isConnection) {
            try {
                // Give it some value as an example.
                Message msg = Message.obtain(null,
                        BatchService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private void requestQueueList() {
        if (isConnection) {
            try {
                // Give it some value as an example.
                Message msg = Message.obtain(null,
                        BatchService.MSG_REQUEST_QUEUE, this.hashCode(), 0);
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /** Binder Service End **/
    private void finishBinderService() {
        if (isConnection) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            BatchService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            unbindService(mServiceConnection);
            isConnection = false;
        }
    }

    class IncomingHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case BatchService.MSG_UPLOAD_STATUS:
//                    Log.d("test", "MSG_UPLOAD_STATUS : "+msg.arg1);
                    switch (msg.arg1) {
                        case ScrapingModule.STATUS_COMPLETE:
                            if (mMode == ProcessMode.COLLECT_FIRST_THIS_MONTH) {

                            } else {

                            }
                            break;
                        case ScrapingModule.STATUS_PREPARE:
                            break;
                        case ScrapingModule.STATUS_REQUEST:
                            break;
                        case ScrapingModule.STATUS_SCRAPING:
                            //String message = (String) msg.obj;
                            break;
                        case ScrapingModule.STATUS_UPLOADING:
                            break;
                    }
                    break;
                case BatchService.MSG_UPLOAD_COMPLETE:
                    break;
                case BatchService.MSG_UPLOAD_MODE:
                    int mode = msg.arg1;
                    ProcessMode[] modes = ProcessMode.values();
                    mMode = modes[mode];
//                    Log.d("test", "MSG_UPLOAD_MODE : "+mMode);
                    switch (mMode) {
                        case COLLECT_FIRST_THIS_MONTH:
                            break;
                        case COLLECT_FIRST_PAST_MONTH:
                        case COLLECT_DAILY:
                        case COLLECT_INDIVIDUAL:
                        case COLLECT_RETRY:
                            break;
                        case COLLECT_IDLE:
                            break;
                    }
                    break;
                case BatchService.MSG_REQUEST_QUEUE:
                case BatchService.MSG_BANK_COMPLETE:
                    ArrayList<ScrapRequester> list = (ArrayList<ScrapRequester>) msg.obj;
                    postInit(list);
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }
}
