package com.nomadconnection.broccoli.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewAssetDaSA;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.common.FloatingActionButton;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetBankInfo;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Asset.RequestAssetBankDetail;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetBankDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level3ActivityAssetDasDetail extends BaseActivity implements View.OnClickListener{

	private static final String TAG = Level3ActivityAssetDasDetail.class.getSimpleName();


	/* Layout */
	private RadioGroup mRadioGroup;
	private TextView mBankName;
	private TextView mBankAccountNum;
	private TextView mBankCost;
	private TextView mInquiryPeriod;
	private FloatingActionButton mFloatingButton;
	private ListView mListView;
	private AdtListViewAssetDaSA mAdapter;

	/* getParcelableExtra */
	private ArrayList<ResponseAssetBankDetail> mAlInfoDataList;
	private AssetBankInfo						mInfoData;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_asset_das_detail);
		sendTracker(AnalyticsName.Level3ActivityAssetDasDetail);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	 public void onClick(View v) {
		switch (v.getId()){
			case R.id.fab_asset_das_button:
				mListView.smoothScrollToPosition(0);
				break;
		}
	}






//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {

				case R.id.ll_navi_img_txt_back:
					finish();
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}







//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mInfoData = getIntent().getParcelableExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_2_HEADER);
			mAlInfoDataList = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_2_BODY);
			if(mInfoData == null || mAlInfoDataList == null)
				bResult = false;
			else{
				/*group 2*/
				refreshData();
			}
			
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitle);
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) ((RelativeLayout)findViewById(R.id.rl_navi_img_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			
			/* Layout */
			((LinearLayout)findViewById(R.id.ll_contents_area_inc_type_2_layout)).setBackgroundResource(R.color.main_1_layout_bg);
			mBankName= (TextView)findViewById(R.id.tv_contents_area_inc_type_2_txt_1);
			mBankAccountNum = (TextView)findViewById(R.id.tv_contents_area_inc_type_2_txt_2);
			mBankCost = (TextView)findViewById(R.id.tv_contents_area_inc_type_2_txt_3);
			mBankCost.setTypeface(FontClass.setFont(this));
			mInquiryPeriod = (TextView)findViewById(R.id.level_3_activity_asset_das_detail_inquiry_period);
			mRadioGroup = (RadioGroup)findViewById(R.id.rg_level_3_activity_asset_das_detail_group);
			mRadioGroup.setOnCheckedChangeListener(RadioBtnCheckedChangeListener);

			mListView = (ListView) findViewById(R.id.lv_fragment_asset_das_detail_a_listview);
			mAdapter = new AdtListViewAssetDaSA(this, mAlInfoDataList);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					if(mAdapter.getCount() -1 == position){
						mListView.smoothScrollToPosition(0);
					}
				}
			});

			mFloatingButton = (FloatingActionButton)findViewById(R.id.fab_asset_das_button);
			mFloatingButton.attachToListView(mListView);
			mFloatingButton.setShadow(false);
			mFloatingButton.setOnClickListener(this);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			String mStartDate = ElseUtils.getBirthDay(TransFormatUtils.getRangeMonth(TransFormatUtils.getDataFormatWhich(0), 3)[0].toString());
//			String mEndDate = ElseUtils.getDate(TransFormatUtils.getDataFormatWhich(2));
			String mEndDate = MainApplication.mAPPShared.getPrefString(APP.SP_CHECK_SCRAPING_DATA_TIME);

			mBankName.setText(mInfoData.mAssetBankAccountName);
			mBankAccountNum.setText(mInfoData.mAssetBankAccountNumber);
			mBankCost.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mAssetBankSum));
			mInquiryPeriod.setText(mEndDate);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}





//===============================================================================//
// Refresh
//===============================================================================//

	/*group 2*/
	private void refreshData(){
		DialogUtils.showLoading(Level3ActivityAssetDasDetail.this);											// 자산 로딩
		Call<ArrayList<ResponseAssetBankDetail>> mCall = ServiceGenerator.createService(AssetApi.class).getBankDetail(
				new RequestAssetBankDetail(Session.getInstance(Level3ActivityAssetDasDetail.this).getUserId(), String.valueOf(mInfoData.mAssetBankID),
						TransFormatUtils.getRangeMonth(TransFormatUtils.getDataFormatWhich(0), 4)[0].toString(), TransFormatUtils.getDataFormatWhich(0), 0)
		);

		mCall.enqueue(new Callback<ArrayList<ResponseAssetBankDetail>>(){

			@Override
			public void onResponse(Call<ArrayList<ResponseAssetBankDetail>> call, Response<ArrayList<ResponseAssetBankDetail>> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (APP._SAMPLE_MODE_) mAlInfoDataList = AssetSampleTestDataJson.GetSampleData2();
				else
				{
					mAlInfoDataList = response.body();
					if(mAlInfoDataList.size() != 0){
						ResponseAssetBankDetail lastItem = new ResponseAssetBankDetail("");
						mAlInfoDataList.add(lastItem);
					}
				}
				resetData();

			}

			@Override
			public void onFailure(Call<ArrayList<ResponseAssetBankDetail>> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (APP._SAMPLE_MODE_) {
					mAlInfoDataList = AssetSampleTestDataJson.GetSampleData2();
					resetData();
				}
				else
					ElseUtils.network_error(Level3ActivityAssetDasDetail.this);
					//CustomToast.getInstance(Level3ActivityAssetDasDetail.this).showShort("Level3ActivityAssetDasDetail 서버 통신 실패");
			}
		});

	}

	/*group 2*/
	private void resetData(){
		if (mAlInfoDataList.size() == 0) {
			((FrameLayout) findViewById(R.id.inc_fragment_asset_das_detail_a_empty)).setVisibility(View.VISIBLE);
			((ImageView) findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
			((TextView) findViewById(R.id.tv_empty_layout_txt)).setText(getResources().getString(R.string.asset_empty));
		}
		else {
			((FrameLayout) findViewById(R.id.inc_fragment_asset_das_detail_a_empty)).setVisibility(View.GONE);
			mAdapter.setData(mAlInfoDataList);
			mAdapter.notifyDataSetChanged();
		}
	}


//===============================================================================//
// Listener
//===============================================================================//
	/** Radio Button Click Listener **/
	private RadioGroup.OnCheckedChangeListener RadioBtnCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			switch (checkedId) {
				case R.id.rb_level_3_activity_asset_das_detail_1:
					refreshAdt("all");
					break;

				case R.id.rb_level_3_activity_asset_das_detail_2:
					refreshAdt("in");
					break;

				case R.id.rb_level_3_activity_asset_das_detail_3:
					refreshAdt("out");
					break;
			}
		}
	};


//=========================================================================================//
// Refresh Adapter
//=========================================================================================//
	public void refreshAdt(String _sort){
		mAdapter.sortData(_sort);
		mAdapter.notifyDataSetChanged();
	}
}
