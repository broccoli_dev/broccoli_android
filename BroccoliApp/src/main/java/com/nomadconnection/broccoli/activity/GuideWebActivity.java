package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;

/**
 * Created by YelloHyunminJang on 16. 2. 4..
 */
public class GuideWebActivity extends BaseActivity {

    private WebView mWebView;
    private ProgressBar mProgress;
    private String mUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_web);
        sendTracker(AnalyticsName.GuideAuthRegisterDetail);

        mWebView = (WebView) findViewById(R.id.wb_guide_view);
        ((TextView)findViewById(R.id.tv_navi_img_txt_subtitle)).setText("blog.naver.com");
        mProgress = (ProgressBar) findViewById(R.id.pb_guide_view);

        Intent intent = getIntent();
        String url = intent.getStringExtra(Const.DATA);

        // 웹뷰에서 자바스크립트실행가능
        mWebView.getSettings().setJavaScriptEnabled(true);
        // get url
        mUrl = url;
        mWebView.loadUrl(mUrl);
        // WebViewClient 지정
        mWebView.setWebViewClient(new WebViewClientClass());
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(title);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                mProgress.setProgress(newProgress);
            }
        });
    }

    public void onBtnClickNaviImgTxtBar(View _view) {
        try {
            switch (_view.getId()) {

                case R.id.ll_navi_img_txt_back:
                    finish();
                    break;

                default:
                    break;
            }
        } catch (Exception e) {
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
        }
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mProgress.setVisibility(View.INVISIBLE);
        }
    }
}
