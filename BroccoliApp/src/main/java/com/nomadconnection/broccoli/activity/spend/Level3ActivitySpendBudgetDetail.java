package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.spend.AdtSpendBudgetLookupDetail;
import com.nomadconnection.broccoli.common.CustomExpandableListView;
import com.nomadconnection.broccoli.constant.BudgetCategory;
import com.nomadconnection.broccoli.constant.BudgetGroupCategory;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetDetail;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetDetailLookupData;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetLookUpData;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeMap;

import static com.igaworks.impl.CommonFrameworkImpl.getContext;

/**
 * Created by YelloHyunminJang on 2017. 2. 28..
 */

public class Level3ActivitySpendBudgetDetail extends BaseActivity {


    private TextView mMonthlyBudgetTitle;
    private TextView mMonthlySpendTitle;
    private TextView mMonthlyBudget;
    private TextView mMonthlySpend;
    private LinearLayout mGraphCurrent;
    private LinearLayout mGraphLeft;
    private TextView mCurrentStatus;
    private CustomExpandableListView mExpListView;
    private WrapperExpandableListAdapter mWrapAdapter;
    private AdtSpendBudgetLookupDetail mAdapter;

    private SpendBudgetLookUpData mData;
    private String mSelectYearMonth;
    private TreeMap<Integer, ArrayList<SpendBudgetDetailLookupData>> mListData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_spend_budget_detail);
        init();
    }

    private void init() {
        Intent intent = getIntent();
        mData = (SpendBudgetLookUpData) intent.getSerializableExtra(Const.DATA);
        mSelectYearMonth = intent.getStringExtra(Const.BUDGET_TARGET_MONTH);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_budget_detail_title);
        findViewById(R.id.ll_navi_motion_else).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
        mMonthlyBudgetTitle = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_budget_title);
        mMonthlySpendTitle = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_spend_title);
        mMonthlyBudget = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_budget);
        mMonthlySpend = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_spend);
        mGraphCurrent = (LinearLayout) findViewById(R.id.ll_spend_budget_current_graph_amount);
        mGraphLeft = (LinearLayout) findViewById(R.id.ll_spend_budget_left_graph_amount);
        mCurrentStatus = (TextView) findViewById(R.id.tv_spend_budget_current_status);
        mExpListView = (CustomExpandableListView) findViewById(R.id.lv_spend_budget_monthly);
        mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        mAdapter = new AdtSpendBudgetLookupDetail(getContext());
        mWrapAdapter = new WrapperExpandableListAdapter(mAdapter);
    }

    private void initData() {
        mExpListView.setAdapter(mWrapAdapter);
        SpendBudgetLookUpData data = mData;
        String month = mSelectYearMonth.substring(4);

        if (data.isUnClassifiedData) {
            String budget = String.format(getResources().getString(R.string.spend_budget_unclassified_budget_month), month);
            String spend = String.format(getResources().getString(R.string.spend_budget_unclassified_expense_month), month);
            mMonthlyBudgetTitle.setText(budget);
            mMonthlySpendTitle.setText(spend);

            mMonthlyBudget.setText(ElseUtils.getDecimalFormat(data.leftBudget));
            mMonthlySpend.setText(ElseUtils.getDecimalFormat(data.leftExpense));

            long currentStatus = data.leftBudget - data.leftExpense;
            String string = "남음";
            if (currentStatus < 0) {
                string = "초과";
            }
            mCurrentStatus.setText(ElseUtils.getDecimalFormat(Math.abs(currentStatus))+" "+string);
        } else {
            int id = mData.getCategoryId();
            String[] categoryGroup = getResources().getStringArray(R.array.spend_budget_group_category);
            BudgetGroupCategory groupCategory = BudgetGroupCategory.values()[id];
            String budget = String.format(getResources().getString(R.string.spend_budget_detail_budget_month), month)+" "+categoryGroup[groupCategory.ordinal()]+ " 예산";
            String spend = String.format(getResources().getString(R.string.spend_budget_detail_expense_month), month)+" "+categoryGroup[groupCategory.ordinal()]+ " 소비";
            mMonthlyBudgetTitle.setText(budget);
            mMonthlySpendTitle.setText(spend);

            mMonthlyBudget.setText(ElseUtils.getDecimalFormat(data.sum));
            mMonthlySpend.setText(ElseUtils.getDecimalFormat(data.expense));

            long currentStatus = data.sum - data.expense;
            String string = "남음";
            if (currentStatus < 0) {
                string = "초과";
            }
            mCurrentStatus.setText(ElseUtils.getDecimalFormat(Math.abs(currentStatus))+" "+string);
        }

        Animation animation = new AlphaAnimation(0.2f, 1);
        animation.setDuration(200);

        long expense = 0;
        long budget = 0;
        if (data.isUnClassifiedData) {
            expense = data.leftExpense;
            budget = data.leftBudget;
        } else {
            expense = data.expense;
            budget = data.sum;
        }

        long ratio = 0;
        if (budget != 0) {
            ratio = (long) ((double)expense / (double)budget * 100.0 );
        } else {
            ratio = 999;
        }
        if (ratio > 999) {
            ratio = 999;
        }

        if (expense == 0) {
            mGraphCurrent.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) mGraphLeft.getLayoutParams();
            amountParams.weight = 1;
            mGraphLeft.setLayoutParams(amountParams);

            Drawable graphLeftBackground = mGraphLeft.getBackground();
            if (graphLeftBackground instanceof ShapeDrawable) {
                ShapeDrawable shapeDrawable = (ShapeDrawable) graphLeftBackground;
                shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
            } else if (graphLeftBackground instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) graphLeftBackground;
                gradientDrawable.setColor(getResources().getColor(R.color.common_white));
            }
        } else {
            mGraphCurrent.setVisibility(View.VISIBLE);
            mGraphCurrent.setAnimation(animation);
            float spendValue = 0f;
            if (budget != 0) {
                spendValue = (float) ((double)expense/(double)budget);
            } else {
                spendValue = 1f;
            }
            if (spendValue > 1f) {
                spendValue = 1f;
            }
            float budgetValue = 1f - spendValue;

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraphCurrent.getLayoutParams();
            params.weight = spendValue;
            if (spendValue >= 1f) {
                params.setMargins(0, 0, 0, 0);
            } else {
                params.setMargins(0, 0, ElseUtils.convertDp2Px(this, 3), 0);
            }
            mGraphCurrent.setLayoutParams(params);

            LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) mGraphLeft.getLayoutParams();
            amountParams.weight = budgetValue;
            mGraphLeft.setLayoutParams(amountParams);

            Drawable graphLeftBackground = mGraphLeft.getBackground();
            if (graphLeftBackground instanceof ShapeDrawable) {
                ShapeDrawable shapeDrawable = (ShapeDrawable) graphLeftBackground;
                shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
            } else if (graphLeftBackground instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) graphLeftBackground;
                gradientDrawable.setColor(getResources().getColor(R.color.common_white));
            }

            if(ratio <= 50){
                Drawable drawable = mGraphCurrent.getBackground();
                if (drawable instanceof ShapeDrawable) {
                    ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage1_color));
                } else if (drawable instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                    gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage1_color));
                }
            } else if(ratio <= 100){
                Drawable drawable = mGraphCurrent.getBackground();
                if (drawable instanceof ShapeDrawable) {
                    ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage2_color));
                } else if (drawable instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                    gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage2_color));
                }
            } else {
                Drawable drawable = mGraphCurrent.getBackground();
                if (drawable instanceof ShapeDrawable) {
                    ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage3_color));
                } else if (drawable instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                    gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage3_color));
                }
            }
        }

        mListData = createList(data);

        mAdapter.setData(mListData);
        mAdapter.notifyDataSetChanged();
        for (int i=0; i < mListData.size(); i++) {
            mExpListView.expandGroup(i);
        }
    }

    private TreeMap<Integer, ArrayList<SpendBudgetDetailLookupData>> createList(SpendBudgetLookUpData lookUpData) {
        TreeMap<Integer, ArrayList<SpendBudgetDetailLookupData>> childList = new TreeMap<>();
        ArrayList<SpendBudgetDetailLookupData> classifiedList = new ArrayList<>();
        ArrayList<SpendBudgetDetailLookupData> unClassifiedList = new ArrayList<>();
        ArrayList<BudgetCategory> existList = new ArrayList<>();
        if (lookUpData.detailList != null) {
            for (SpendBudgetDetail targetCategory : lookUpData.detailList) {
                SpendBudgetDetailLookupData data = new SpendBudgetDetailLookupData();
                data.budgetId = targetCategory.budgetId;
                data.largeCategoryId = targetCategory.largeCategoryId;
                data.detailId = targetCategory.detailId;
                data.expense = targetCategory.expense;
                data.sum = targetCategory.sum;
                BudgetCategory group = BudgetCategory.values()[targetCategory.largeCategoryId];
                existList.add(group);
                data.leftBudget = 0;
                data.leftExpense = 0;
                if (data.sum > 0) {
                    data.isUnClassifiedData = false;
                    classifiedList.add(data);
                } else if (data.sum == 0) {
                    data.isUnClassifiedData = true;
                    unClassifiedList.add(data);
                }
            }
        }
        if (classifiedList.size() > 0) {
            Collections.sort(classifiedList, new Comparator<SpendBudgetDetailLookupData>() {
                @Override
                public int compare(SpendBudgetDetailLookupData lhs, SpendBudgetDetailLookupData rhs) {
                    return lhs.getExpense() > rhs.getExpense() ? -1 : lhs.getExpense() < rhs.getExpense() ? 1:0;
                }
            });
            childList.put(0, classifiedList);
        }

        BudgetGroupCategory categories = BudgetGroupCategory.values()[lookUpData.getCategoryId()];
        BudgetCategory[] group = categories.child();
        for (int index=0; index < group.length; index++) {
            BudgetCategory detailCategory = group[index];
            if (!existList.contains(detailCategory)) {
                SpendBudgetDetailLookupData data = new SpendBudgetDetailLookupData();
                data.budgetId = 0;
                data.largeCategoryId = detailCategory.ordinal();
                data.detailId = 0;
                data.expense = 0;
                data.sum = 0;
                data.isUnClassifiedData = true;
                unClassifiedList.add(data);
            }
        }

        if (unClassifiedList.size() > 0) {
            Collections.sort(unClassifiedList, new Comparator<SpendBudgetDetailLookupData>() {
                @Override
                public int compare(SpendBudgetDetailLookupData lhs, SpendBudgetDetailLookupData rhs) {
                    return lhs.getExpense() > rhs.getExpense() ? -1 : lhs.getExpense() < rhs.getExpense() ? 1:0;
                }
            });
            childList.put(1, unClassifiedList);
        }

        return childList;
    }


}
