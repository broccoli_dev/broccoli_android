package com.nomadconnection.broccoli.activity.setting;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.lumensoft.ks.KSCertificateLoader;
import com.lumensoft.ks.KSException;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.CertListActivity;
import com.nomadconnection.broccoli.activity.GuideAuthCopyActivity;
import com.nomadconnection.broccoli.activity.LoadingActivity;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.AdtListViewFinancialList;
import com.nomadconnection.broccoli.common.CustomExpandableListView;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static com.nomadconnection.broccoli.activity.CertListActivity.RESULT_ID_OK;
import static com.nomadconnection.broccoli.data.Scrap.SelectBankData.CERT_FINISH;
import static com.nomadconnection.broccoli.data.Scrap.SelectBankData.LOGIN_FINISH;
import static com.nomadconnection.broccoli.data.Scrap.SelectBankData.NONE;

/**
 * Created by YelloHyunminJang on 16. 2. 2..
 * 사용자 금융 기관 연동
 */
public class Level3ActivitySettingFinancial extends BaseActivity implements View.OnClickListener {

    public static final int CERT_PASSWORD = 1;
    public static final int ID_PASSWORD = 2;

    private ArrayList<String> mGroupList = new ArrayList<>();
    private ArrayList<ArrayList<SelectBankData>> mChildList = new ArrayList<>();
    private ArrayList<SelectBankData> mChildListContent1 = new ArrayList<>();
    private ArrayList<SelectBankData> mChildListContent2 = new ArrayList<>();
    private ArrayList<SelectBankData> mChildListContent3 = new ArrayList<>();
    private CustomExpandableListView mExpListView;
    private ArrayList<SelectBankData> mAlInfoDataList = new ArrayList<>();
    private AdtListViewFinancialList mAdapter;
    private WrapperExpandableListAdapter mWarpAdapter;
    private TextView mTitle;
    private Button mNextBtn;
    private String mPwd;
    private String mCert;
    private String mValidDate;
    private String mId;
    private int mCertCount = 0;
    private boolean fromMembership = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_setting_financial);
        DialogUtils.showLoading(this);
        Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                init(str);
                initComponent();
                initData();
                DialogUtils.dismissLoading();
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
            }
        });
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        showWaringExitDialog();
    }

    private void onRealExit() {
        if (fromMembership) {
            sendTracker(AnalyticsName.UserSelectBankCancel);
        }
        super.onBackPressed();
    }

    private void showWaringExitDialog() {
        DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
                    onRealExit();
                }
            }
        }, new InfoDataDialog(R.layout.dialog_inc_case_1, "아니요", "예", "금융기관 등록을 취소하시겠어요?"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_fragment_membership_next:
                startCertification();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initComponent() {
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        mTitle = (TextView) findViewById(R.id.tv_navi_img_txt_title);
        mTitle.setText(R.string.setting_financial_add_title);
        mNextBtn = (Button) findViewById(R.id.btn_fragment_membership_next);
        mNextBtn.setOnClickListener(this);
        mNextBtn.setEnabled(false);

        mExpListView = (CustomExpandableListView) findViewById(R.id.elv_fragment_financial_explistview);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mExpListView.setNestedScrollingEnabled(true);
        }
        mAdapter = new AdtListViewFinancialList(this, mGroupList, mChildList);
        mWarpAdapter = new WrapperExpandableListAdapter(mAdapter);
        mExpListView.setAdapter(mWarpAdapter);

	        /* group list click 시 */
        mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });

			/* Child list click 시 */
        mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                SelectBankData mTemp = null;
                mTemp = mChildList.get(groupPosition).get(childPosition);

                if (mTemp == null) return true;
                int status = mTemp.getStatus();
                switch (status) {
                    case SelectBankData.SELECTED:
                        mTemp.setStatus(SelectBankData.NONE);
                        break;
                    case SelectBankData.NONE:
                        if (mTemp.getName().contains("신한은행")) {
                            showShinhanWarning();
                        }
                        mTemp.setStatus(SelectBankData.SELECTED);
                        break;
                    case CERT_FINISH:
                        mTemp.setStatus(SelectBankData.NONE);
                        mTemp.clear();
                        break;
                    case SelectBankData.LOGIN_FINISH:
                        mTemp.setStatus(SelectBankData.NONE);
                        mTemp.clear();
                        break;
                }
                mAdapter.notifyDataSetChanged();
                mWarpAdapter.notifyDataSetChanged();
                return false;
            }
        });
        mAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                int count = 0;
                for (SelectBankData data : mAlInfoDataList) {
                    if (data.getStatus() == SelectBankData.SELECTED) {
                        count++;
                    }
                }

                if (count > 0) {
                    mNextBtn.setEnabled(true);
                    mNextBtn.setText(String.valueOf(count) + getString(R.string.setting_financial_reg_counting_btn));
                } else {
                    mNextBtn.setEnabled(false);
                    mNextBtn.setText(R.string.setting_financial_reg_btn);
                }
            }
        });
    }

    private void init(String key) {
        Intent intent = getIntent();
        if (intent != null) {
            fromMembership = intent.getBooleanExtra(Const.FROM_MEMBERSHIP, false);
        }
        if (fromMembership) {
            sendTracker(AnalyticsName.UserSelectBank);
        } else {
            sendTracker(AnalyticsName.SettingSelectBank);
        }
        mGroupList.clear();
        mChildList.clear();
        mChildListContent1.clear();
        mChildListContent2.clear();
        mChildListContent3.clear();
        mAlInfoDataList.clear();
        String[] bankName = getResources().getStringArray(R.array.bank_name);
        String[] bankCode = getResources().getStringArray(R.array.bank_code);
        List<BankData> bankDataList = ScrapDao.getInstance().getBankData(this, key, Session.getInstance(getApplicationContext()).getUserId());
        List<String> selectedCodeList = new ArrayList<String>();
        for (BankData data : bankDataList) {
            selectedCodeList.add(data.getBankCode());
        }
        if (bankName != null) {
            for (int i=0; i < bankName.length; i++) {
                SelectBankData data = new SelectBankData();
                data.setType(SelectBankData.TYPE_BANK);
                data.setTypeName(getString(R.string.membership_text_second_cert_bank));
                data.setName(bankName[i]);
                data.setCode(bankCode[i]);
                data.setNameId(i);
                if (selectedCodeList.contains(bankCode[i])) {
                    int index = selectedCodeList.indexOf(bankCode[i]);
                    int lastIndex = selectedCodeList.lastIndexOf(bankCode[i]);
                    if (SelectBankData.TYPE_BANK.equalsIgnoreCase(bankDataList.get(index).getType())) {
                        data.setStatus(SelectBankData.COMPLETE);
                    } else if (SelectBankData.TYPE_BANK.equalsIgnoreCase(bankDataList.get(index).getType())) {
                        data.setStatus(SelectBankData.COMPLETE);
                    }
                    data.setLoginMethod(bankDataList.get(index).getLoginMethod());
                }
                mAlInfoDataList.add(data);
            }
        }

        String[] cardName = getResources().getStringArray(R.array.card_name);
        String[] cardCode = getResources().getStringArray(R.array.card_code);
        if (cardName != null) {
            for (int i=0; i < cardName.length; i++) {
                SelectBankData data = new SelectBankData();
                data.setType(SelectBankData.TYPE_CARD);
                data.setTypeName(getString(R.string.membership_text_second_cert_card));
                data.setName(cardName[i]);
                data.setCode(cardCode[i]);
                data.setNameId(i);
                if (selectedCodeList.contains(cardCode[i])) {
                    data.setStatus(SelectBankData.COMPLETE);
                    int index = selectedCodeList.indexOf(cardCode[i]);
                    data.setLoginMethod(bankDataList.get(index).getLoginMethod());
                }
                mAlInfoDataList.add(data);
            }
        }
        String[] cashName = getResources().getStringArray(R.array.cash_name);
        String[] cashCode = getResources().getStringArray(R.array.cash_code);
        if (cashName != null) {
            for (int i=0; i < cashName.length; i++) {
                SelectBankData data = new SelectBankData();
                data.setType(SelectBankData.TYPE_CASH);
                data.setTypeName(getString(R.string.membership_text_second_cert_cash));
                data.setName(cashName[i]);
                data.setCode(cashCode[i]);
                data.setNameId(i);
                if (selectedCodeList.contains(cashCode[i])) {
                    int index = selectedCodeList.indexOf(cashCode[i]);
                    int lastIndex = selectedCodeList.lastIndexOf(cashCode[i]);
                    if (SelectBankData.TYPE_CASH.equalsIgnoreCase(bankDataList.get(index).getType())) {
                        data.setStatus(SelectBankData.COMPLETE);
                    } else if (SelectBankData.TYPE_CASH.equalsIgnoreCase(bankDataList.get(lastIndex).getType())) {
                        data.setStatus(SelectBankData.COMPLETE);
                    }
                    data.setLoginMethod(bankDataList.get(index).getLoginMethod());
                }
                mAlInfoDataList.add(data);
            }
        }

			/* gourp list */
        for (int i = 0; i < mAlInfoDataList.size(); i++) {
            if (!mGroupList.contains(mAlInfoDataList.get(i).getTypeName())) {
                mGroupList.add(mAlInfoDataList.get(i).getTypeName());
            }
        }

        for (int i = 0; i < mAlInfoDataList.size(); i++) {
            if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_BANK)) {
					/* gourp 1의 child */
                mChildListContent1.add(mAlInfoDataList.get(i));
            }
            else if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_CARD)) {
					/* gourp 2의 child */
                mChildListContent2.add(mAlInfoDataList.get(i));;
            }
            else if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_CASH)) {
                mChildListContent3.add(mAlInfoDataList.get(i));
            }
        }

			/* child list에 각각의 content 넣기 */
        mChildList.add(mChildListContent1);
        mChildList.add(mChildListContent2);
        mChildList.add(mChildListContent3);
    }

    private void setData() {
        mGroupList.clear();
        mChildList.clear();
        mChildListContent1.clear();
        mChildListContent2.clear();
        mChildListContent3.clear();

			/* gourp list */
        for (int i = 0; i < mAlInfoDataList.size(); i++) {
            if (!mGroupList.contains(mAlInfoDataList.get(i).getTypeName())) {
                mGroupList.add(mAlInfoDataList.get(i).getTypeName());
            }
        }

        for (int i = 0; i < mAlInfoDataList.size(); i++) {

            if (mAlInfoDataList.get(i).getStatus() == SelectBankData.SELECTED) {
                mAlInfoDataList.get(i).setStatus(SelectBankData.NONE);
            }

            if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_BANK)) {
					/* gourp 1의 child */
                mChildListContent1.add(mAlInfoDataList.get(i));
            }
            else if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_CARD)) {
					/* gourp 2의 child */
                mChildListContent2.add(mAlInfoDataList.get(i));;
            }
            else if (mAlInfoDataList.get(i).getType().equals(SelectBankData.TYPE_CASH)) {
                mChildListContent3.add(mAlInfoDataList.get(i));
            }
        }

			/* child list에 각각의 content 넣기 */
        mChildList.add(mChildListContent1);
        mChildList.add(mChildListContent2);
        mChildList.add(mChildListContent3);

        mAdapter.setVisibleHeaderGuide(true);
        mAdapter.notifyDataSetChanged();
        for (int i = 0; i <mGroupList.size() ; i++) {
            mExpListView.expandGroup(i);
        }
        mExpListView.setSelectionAfterHeaderView();
    }

    private void initData() {
        mCertCount = getCertCount();
        setData();

        for (int i = 0; i <mGroupList.size() ; i++) {
            mExpListView.expandGroup(i);
        }
    }

    private int getCertCount() {
        int count = 0;
        try {
            Vector<?> userCerts = KSCertificateLoader.getCertificateListfromSDCardWithGpkiAsVector(this);
            if (userCerts != null && userCerts.size() > 0) {
                count = userCerts.size();
            }
        } catch (KSException e) {
            e.printStackTrace();
        }

        return count;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CERT_PASSWORD) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    mPwd = data.getStringExtra(Const.CERT_PWD);
                    mCert = data.getStringExtra(Const.CERT_NAME);
                    mValidDate = data.getStringExtra(Const.CERT_VALIDDATE);
                    setCertComplete(mCert, mPwd, mValidDate);
                    refreshFinancialData();
                }
            } else if (resultCode == RESULT_ID_OK) {
                setResult(RESULT_OK);
                finish();
            }
        } else if (requestCode == ID_PASSWORD) {
            if (resultCode == RESULT_OK ||
                    resultCode == RESULT_ID_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    private void setCertComplete(String cert, String pwd, String vaild) {
        if (mAlInfoDataList != null) {
            for (int i=0; i < mAlInfoDataList.size(); i++) {
                SelectBankData selectBankData = mAlInfoDataList.get(i);
                if (selectBankData.getStatus() == SelectBankData.SELECTED) {
                    selectBankData.setStatus(CERT_FINISH);
                    selectBankData.setId(cert);
                    selectBankData.setPwd(pwd);
                    selectBankData.setValidDate(vaild);
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void setLoginComplete(ArrayList<SelectBankData> selectBankList, String id, String pwd) {
        if (selectBankList != null) {
            if (mAlInfoDataList != null) {
                for (int i=0; i < mAlInfoDataList.size(); i++) {
                    for (int j=0; j < selectBankList.size(); j++) {
                        SelectBankData selectBankData = mAlInfoDataList.get(i);
                        if (selectBankData.getType().equalsIgnoreCase(selectBankList.get(j).getType())
                                && selectBankData.getCode().equalsIgnoreCase(selectBankList.get(j).getCode())) {
                            selectBankData.setStatus(LOGIN_FINISH);
                            selectBankData.setId(id);
                            selectBankData.setPwd(pwd);
                        } else if (selectBankData.getStatus() == SelectBankData.SELECTED){
                            selectBankData.setStatus(NONE);
                        }
                    }
                }
            }
        }
    }

    private void startCertification() {
        final ArrayList<SelectBankData> selectedList = new ArrayList<SelectBankData>();
        for (ArrayList<SelectBankData> data : mChildList) {
            for (SelectBankData bank  : data) {
                if (bank.getStatus() == SelectBankData.SELECTED) {
                    selectedList.add(bank);
                }
            }
        }
        if (mCertCount == 0) {
            Intent intent = new Intent(Level3ActivitySettingFinancial.this, GuideAuthCopyActivity.class);
            intent.putExtra(Const.FROM_MEMBERSHIP, fromMembership);
            intent.putExtra(Const.DATA, selectedList);
            startActivityForResult(intent, ID_PASSWORD);
        } else {
            if (selectedList.size() > 0) {
                Intent intent = new Intent(Level3ActivitySettingFinancial.this, CertListActivity.class);
                intent.putExtra(Const.FROM_MEMBERSHIP, fromMembership);
                intent.putExtra(Const.DATA, selectedList);
                startActivityForResult(intent, CERT_PASSWORD);
            } else {
                Toast.makeText(Level3ActivitySettingFinancial.this, R.string.setting_text_warning_not_selected, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void refreshFinancialData() {
        DialogUtils.showLoading(this);
        final ArrayList<BankData> selectList = getBankData();
        Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                DialogUtils.dismissLoading();
                insertBankData(str, selectList);
                if (fromMembership) {
                    startBackgroundLoading();
                } else {
                    startBackgroundService(selectList);
                }
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
            }
        });
    }

    private ArrayList<BankData> getBankData() {
        ArrayList<BankData> bankList = new ArrayList<BankData>();
        for (SelectBankData bank : mAlInfoDataList) {
            if (bank.getStatus() == SelectBankData.CERT_FINISH) {
                BankData dataSet = new BankData();
                dataSet.setPassWord(bank.getPwd());
                dataSet.setBankName(bank.getName());
                dataSet.setBankCode(bank.getCode());
                dataSet.setCertSerial(bank.getId());
                dataSet.setValidDate(bank.getValidDate());
                dataSet.setType(bank.getType());
                dataSet.setStatus(BankData.SCRAPING);
                dataSet.setLoginMethod(BankData.LOGIN_CERT);
                bankList.add(dataSet);
            } else if (bank.getStatus() == SelectBankData.LOGIN_FINISH) {
                BankData dataSet = new BankData();
                dataSet.setPassWord(bank.getPwd());
                dataSet.setBankName(bank.getName());
                dataSet.setBankCode(bank.getCode());
                dataSet.setCertSerial(bank.getId());
                dataSet.setValidDate(bank.getValidDate());
                dataSet.setType(bank.getType());
                dataSet.setStatus(BankData.SCRAPING);
                dataSet.setLoginMethod(BankData.LOGIN_ID);
                bankList.add(dataSet);
            }
        }
        return bankList;
    }

    private void insertBankData(String key, ArrayList<BankData> list) {
        if (list != null) {
            ScrapDao scrapDao = ScrapDao.getInstance();
            for (BankData data : list) {
                BankData bankData = scrapDao.getBankDataByName(this, key, data.getBankName(), Session.getInstance(getApplicationContext()).getUserId());
                if (bankData == null || bankData.getBankName() == null || TextUtils.isEmpty(bankData.getBankName())) {
                    scrapDao.insertBankData(this, key, Session.getInstance(getApplicationContext()).getUserId(), data);
                } else {
                    bankData.setCertSerial(data.getCertSerial());
                    bankData.setPassWord(data.getPassWord());
                    bankData.setValidDate(data.getValidDate());
                    scrapDao.updateBankData(this, key, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                }
            }
        }
    }

    private void startBackgroundService(ArrayList<BankData> list) {
        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_INDIVIDUAL);
        intent.putExtra(Const.DATA, list);
        sendBroadcast(intent);
        finish();
    }

    private void startBackgroundLoading() {
        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_FIRST);
        sendBroadcast(intent);
        ActivityCompat.finishAffinity(this);
        Intent loadingIntent = new Intent(this, LoadingActivity.class);
        loadingIntent.putExtra(Const.FROM_MEMBERSHIP, true);
        startActivity(loadingIntent);
        finish();
    }

    private void showShinhanWarning() {

    }

    private void showCardWarning(DialogInterface.OnClickListener onClickListener) {
        DialogUtils.showDlgBaseTwoButton(this, onClickListener, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.guide_auth_register), getString(R.string.common_yes), getString(R.string.setting_financial_warning_auth_registered)));
    }
}
