package com.nomadconnection.broccoli.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialReg;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Scrap.ServiceRecord;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoliespider.data.BankRequestData;

import java.util.ArrayList;

public class AlertDialogActivity extends BaseActivity implements OnClickListener{

	public static final String PARAMETER_TITLE = "title";
	public static final String PARAMETER_MESSAGE = "message";
	public static final String PARAMETER_RETRY_MODE = "mode";
	public static final String PARAMETER_TYPE = "type";
	public static final String PARAMETER_RETRY_LIST = "retryList";
	public static final String PARAMETER_ERROR_LIST = "errorList";

	public static final String TYPE_AUTH_FAIL = "TYPE_AUTH_FAIL";
	public static final String TYPE_SITE_RENEW = "TYPE_SITE_RENEW";
	public static final String TYPE_FAIL_RETRY = "TYPE_FAIL_RETRY";
	public static final String TYPE_FAIL_MULTI = "TYPE_FAIL_MULTI";

	private String title;
	private String notiMessage;
	private TextView mTilte;
	private TextView mContent;
	private Button mButton;
	private Button mCancel;
	private ListView mListView;
	private View mListParent;
	private ImageView mWarning;
	private ProcessMode mMode = ProcessMode.COLLECT_IDLE;
	private String mType = null;
	private ArrayList<BankRequestData> mList;
	private ArrayList<ServiceRecord> mErrorList;
    
//=======================================================================================//
// override method	
//=======================================================================================//	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		sendTracker(AnalyticsName.AlertDialogActivity);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				// 키잠금 해제하기
				| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
				// 화면 켜기
				| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_FULLSCREEN);
		WindowManager.LayoutParams winLayoutParam = new WindowManager.LayoutParams();
		winLayoutParam.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		winLayoutParam.dimAmount = 0.5f;
		getWindow().setAttributes(winLayoutParam);
		setContentView(R.layout.activity_alert_dialog);
		
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed(){
		super.onBackPressed();
	}	
	
	@Override
	protected void onDestroy() {		
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		if(v == mButton) {
//			if (TYPE_FAIL_RETRY.equals(mType)) {
//				switch (mMode) {
//					case COLLECT_FIRST_THIS_MONTH:
//						break;
//					case COLLECT_FIRST_PAST_MONTH:
//						break;
//					case COLLECT_DAILY:
//						break;
//					case COLLECT_INDIVIDUAL:
//						break;
//					case COLLECT_RETRY:
//						break;
//					case COLLECT_IDLE:
//						break;
//				}
//				sendRetryMessage();
//			} else {
//
//			}
			Intent intent = new Intent(this, Level3ActivitySettingFinancialReg.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS|Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(intent);
			finish();
		} else if (v == mCancel) {
			finish();
		}
	}
	
	private void sendRetryMessage() {
		Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_RETRY);
		intent.putExtra(Const.DATA, mList);
		sendBroadcast(intent);
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			Bundle bun = getIntent().getExtras();
			title = bun.getString(PARAMETER_TITLE);
			notiMessage = bun.getString(PARAMETER_MESSAGE);
			int mode = bun.getInt(PARAMETER_RETRY_MODE);
			mType = bun.getString(PARAMETER_TYPE);
			mList = (ArrayList<BankRequestData>) bun.getSerializable(PARAMETER_RETRY_LIST);
			mErrorList = (ArrayList<ServiceRecord>) bun.getSerializable(PARAMETER_ERROR_LIST);
			ProcessMode[] modes = ProcessMode.values();
			mMode = modes[mode];
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			mTilte = (TextView)findViewById(R.id.tv_activity_alert_dialog_title);
			mContent =(TextView)findViewById(R.id.tv_activity_alert_dialog_content);
			mButton = (Button)findViewById(R.id.btn_activity_alert_dialog_click);
			mCancel = (Button)findViewById(R.id.btn_activity_alert_dialog_click_cancel);
			mListView = (ListView)findViewById(R.id.lv_activity_alert_dialog_list);
			mListParent = (View)findViewById(R.id.rl_activity_alert_dialog_list);
			mWarning = (ImageView) findViewById(R.id.iv_activity_alert_dialog_warning);
			mButton.setOnClickListener(this);
			mCancel.setOnClickListener(this);
			mCancel.setVisibility(View.GONE);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mTilte.setText(title);
			mTilte.setTypeface(Typeface.SANS_SERIF);
			mContent.setText(notiMessage);
			mContent.setTypeface(Typeface.SANS_SERIF);
			mContent.setTextColor(getResources().getColor(R.color.common_maintext_color));
			mButton.setText(getResources().getString(R.string.common_confirm));
			mButton.setTypeface(Typeface.SANS_SERIF);
			if (mType.equals(TYPE_FAIL_MULTI)) {
				mWarning.setVisibility(View.VISIBLE);
				mListParent.setVisibility(View.GONE);
			} else {
				if (mErrorList != null && mErrorList.size() > 0) {
					mListParent.setVisibility(View.VISIBLE);
					mListView.setAdapter(new AlertListAdapter(getBaseContext(), mErrorList));
				} else {
					mListParent.setVisibility(View.GONE);
				}
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private class AlertListAdapter extends BaseAdapter {

		private ArrayList<ServiceRecord> mDataSet;
		private Context mContext;

		private AlertListAdapter(Context context, ArrayList<ServiceRecord> data) {
			mDataSet = data;
			mContext = context;
		}

		@Override
		public int getCount() {
			int count = 0;
			if (mDataSet != null) {
				count = mDataSet.size();
			}
			return count;
		}

		@Override
		public Object getItem(int position) {
			Object obj = null;
			if (mDataSet != null) {
				obj = mDataSet.get(position);
			}
			return obj;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView view = (TextView) convertView;
			if (view == null) {
				view = new TextView(mContext);
				AbsListView.LayoutParams params = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				view.setLayoutParams(params);
				view.setPadding(0, 5, 0, 5);
				view.setGravity(Gravity.CENTER);
				view.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				view.setTypeface(Typeface.SANS_SERIF);
			}
			view.setText(mDataSet.get(position).getName());
			return view;
		}
	}
}
