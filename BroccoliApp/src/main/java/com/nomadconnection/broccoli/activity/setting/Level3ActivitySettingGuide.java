package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;


public class Level3ActivitySettingGuide extends BaseActivity implements View.OnClickListener{

	private static final String TAG = Level3ActivitySettingGuide.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_setting_guide);
		sendTracker(AnalyticsName.Level3ActivitySettingGuide);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}




//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {    	
		try
		{
			switch (_view.getId()) {
				
			case R.id.ll_navi_img_txt_back:
				finish();
				break;
				
			default:
				break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());			
		}
	}


	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {

		} catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try {
			/* Title */
			((RelativeLayout) findViewById(R.id.rl_navi_img_bg)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_guide_title);
			findViewById(R.id.ll_activity_setting_rule_dayli_term).setOnClickListener(this);
			findViewById(R.id.ll_activity_setting_rule_dayli_personal).setOnClickListener(this);
			findViewById(R.id.ll_activity_setting_rule_broccoli_term).setOnClickListener(this);
			findViewById(R.id.ll_activity_setting_rule_broccoli_personal).setOnClickListener(this);
			findViewById(R.id.ll_activity_setting_broccoli_init_del).setOnClickListener(this);
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			/* Default Value */
			
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	
	
	


//===============================================================================//
// Item Click Listener
//===============================================================================//

	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId()) {
			case R.id.ll_activity_setting_rule_dayli_term:
				sendTracker(AnalyticsName.SettingGuideDayTerm);
				intent = new Intent(this, Level4ActivitySettingCommonTerm.class);
				intent.putExtra(Const.TITLE, "DAYLI Pass 이용약관");
				intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/DAYLI%20PASS%20%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80_full.html");
				startActivity(intent);
				break;
			case R.id.ll_activity_setting_rule_dayli_personal:
				sendTracker(AnalyticsName.SettingGuideDayPersonal);
				intent = new Intent(this, Level4ActivitySettingCommonTerm.class);
				intent.putExtra(Const.TITLE, "DAYLI Pass 개인정보 처리방침");
				intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/DAYLI%20PASS%20%EA%B0%9C%EC%9D%B8%EC%A0%95%EB%B3%B4%EC%B2%98%EB%A6%AC%EB%B0%A9%EC%B9%A8.html");
				startActivity(intent);
				break;
			case R.id.ll_activity_setting_rule_broccoli_term:
				sendTracker(AnalyticsName.SettingGuideTerm);
				intent = new Intent(this, Level4ActivitySettingCommonTerm.class);
				intent.putExtra(Const.TITLE, "브로콜리 이용 약관");
				intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/%EB%B8%8C%EB%A1%9C%EC%BD%9C%EB%A6%AC%20%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80_full.html");
				startActivity(intent);
				break;
			case R.id.ll_activity_setting_rule_broccoli_personal:
				sendTracker(AnalyticsName.SettingGuidePersonal);
				intent = new Intent(this, Level4ActivitySettingCommonTerm.class);
				intent.putExtra(Const.TITLE, "브로콜리 개인정보 처리방침");
				intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/%EB%B8%8C%EB%A1%9C%EC%BD%9C%EB%A6%AC%20%EA%B0%9C%EC%9D%B8%EC%A0%95%EB%B3%B4%EC%B2%98%EB%A6%AC%EB%B0%A9%EC%B9%A8_full.html");
				startActivity(intent);
				break;
			case R.id.ll_activity_setting_broccoli_init_del:
				startActivity(new Intent(this, Level4ActivitySettingInitialization.class));
				break;
		}
	}
}
