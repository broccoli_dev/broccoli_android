package com.nomadconnection.broccoli.activity.remit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RequestDataChangeMoney;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccount;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.ElseUtils;


public class RemitChargeActivity extends BaseActivity {

	private static final String TAG = RemitChargeActivity.class.getSimpleName();

	public static int mCurrentFragmentIndex = 0;	// default

	/* Layout */
	private Button mRemitMoneyOne;
	private Button mRemitMoneyFive;
	private Button mRemitMoneyTen;
	private EditText mRemitChargeCost;
	private TextView mRemitChargeCostText;
	private TextView mForwardAccount;
	private Button mRemitCharge;
	private String mTextWatcherResult = "";
	private ResponseRemitAccount mRemitMainData;

	/* Get Parcel Data */
//	private SpendMonthlyInfo mSpendMonthlyInfo;
//	private List<SpendMonthlyInfo> mAlInfoDataList;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remit_charge);
//		sendTracker(AnalyticsName.Level2ActivitySpendHistory);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			onBackPressed();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			finish();
		}
	}

	//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {
				case R.id.ll_navi_img_txt_back:
					onBackPressed();
					break;
				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}

	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			if (getIntent().getExtras() != null) {
				mRemitMainData = (ResponseRemitAccount)getIntent().getExtras().getSerializable(Const.REMIT_MAIN_DATA);
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout)findViewById(R.id.rl_navi_img_bg)).setBackgroundResource(R.color.main_4_layout_bg);
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.remit_charge));

			/* Layout */
			mRemitMoneyOne = (Button)findViewById(R.id.btn_activity_remit_charge_one);
			mRemitMoneyOne.setOnClickListener(BtnClickListener);
			mRemitMoneyFive = (Button)findViewById(R.id.btn_activity_remit_charge_five);
			mRemitMoneyFive.setOnClickListener(BtnClickListener);
			mRemitMoneyTen = (Button)findViewById(R.id.btn_activity_remit_charge_ten);
			mRemitMoneyTen.setOnClickListener(BtnClickListener);

			mRemitChargeCost = (EditText)findViewById(R.id.et_activity_remit_charge_input_cost);
			mRemitChargeCostText = (TextView)findViewById(R.id.tv_activity_remit_change_text_cost);
			mRemitChargeCostText.setVisibility(View.INVISIBLE);

			mForwardAccount = (TextView)findViewById(R.id.tv_activity_remit_charge_account);

			mRemitCharge = (Button)findViewById(R.id.btn_activity_remit_charge_confirm);
			mRemitCharge.setOnClickListener(BtnClickListener);
			mRemitCharge.setEnabled(false);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mRemitChargeCost.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					if (!s.toString().equals(mTextWatcherResult)) {
						if ("".equals(s.toString())) {
							mRemitChargeCost.setHint("0");
							mRemitChargeCostText.setVisibility(View.INVISIBLE);
							mRemitCharge.setEnabled(false);
						} else {
							try {
								// The comma in the format specifier does the trick
								mTextWatcherResult = String.format("%,d", Long.parseLong(s.toString().replaceAll(",", "")));
//								Log.d("broccoli", "afterTextChanged() : " + mTextWatcherResult);
							} catch (NumberFormatException e) {
								Log.e("broccoli", "afterTextChanged() : " + e.toString());
							}
							mRemitChargeCostText.setVisibility(View.VISIBLE);
							mRemitChargeCost.setText(mTextWatcherResult);
							mRemitChargeCost.setSelection(mTextWatcherResult.length());
							mRemitChargeCostText.setText(ElseUtils.convertHangul(s.toString().replaceAll(",", "")) + getString(R.string.common_won));
							mRemitCharge.setEnabled(true);
						}
					}
				}
			});

			String accountInfo = mRemitMainData.getAccountInfo().get(0).getAccountName()
					+ mRemitMainData.getAccountInfo().get(0).getAccountNumber();
			mForwardAccount.setText(accountInfo);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//	
	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.btn_activity_remit_charge_one:
				long one = 10000;
				long temp = 0;
				if(!mRemitChargeCost.getText().toString().equals("")){
					temp = Long.parseLong(mRemitChargeCost.getText().toString().replaceAll(",", ""));
				}
				mRemitChargeCost.setText(Long.toString(temp + one));
				break;
				
			case R.id.btn_activity_remit_charge_five:
				long five = 50000;
				long temp2 = 0;
				if(!mRemitChargeCost.getText().toString().equals("")){
					temp2 = Long.parseLong(mRemitChargeCost.getText().toString().replaceAll(",", ""));
				}
				mRemitChargeCost.setText(Long.toString(temp2 + five));
				break;

			case R.id.btn_activity_remit_charge_ten:
				long ten = 100000;
				long temp3 = 0;
				if(!mRemitChargeCost.getText().toString().equals("")){
					temp3 = Long.parseLong(mRemitChargeCost.getText().toString().replaceAll(",", ""));
				}
				mRemitChargeCost.setText(Long.toString(temp3 + ten));
				break;

			case R.id.btn_activity_remit_charge_confirm:
				Intent remit_charge = new Intent(RemitChargeActivity.this, RemitSendPasswordActivity.class);
				remit_charge.putExtra(Const.REMIT_PAGE_STATUS, APP.REMIT_CHANGE_MONEY);
				remit_charge.putExtra(Const.DATA, chargeMoney());
				startActivityForResult(remit_charge, 0);
				break;

			default:
				break;
			}

		}
	};

	private RequestDataChangeMoney chargeMoney(){
		RequestDataChangeMoney mRequestDataChangeMoney = new RequestDataChangeMoney();
		mRequestDataChangeMoney.setUserId(Session.getInstance(this).getUserId());
		mRequestDataChangeMoney.setChargeMoney(mRemitChargeCost.getText().toString().replaceAll(",", ""));
		return mRequestDataChangeMoney;
	}
}
