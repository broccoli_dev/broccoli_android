package com.nomadconnection.broccoli.activity.spend;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.BudgetCategory;
import com.nomadconnection.broccoli.constant.BudgetGroupCategory;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetCategory;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetCategoryEdit;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetData;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetDetail;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetDetailEdit;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 2. 28..
 */

public class Level3ActivitySpendBudgetEdit extends BaseActivity {

    private TextView mTotalBudget;
    private LinearLayout mContentView;
    private View mGraph;
    private TextView mCurrentBudget;
    private View mDeleteBtn;

    private String mTargetYearMonth;
    private long mTotalBudgetValue = 0;
    private SpendBudgetData mTargetData;
    private TreeMap<Integer, SpendBudgetCategoryEdit> mGroupData = new TreeMap<>();
    private HashMap<Integer, ArrayList<SpendBudgetDetailEdit>> mChildData = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_spend_budget_edit);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SpendBudgetEdit);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_budget_setting_title);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setText(R.string.common_complete);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundResource(R.color.main_2_layout_bg);
        mTotalBudget = (TextView) findViewById(R.id.tv_category_total_budget);
        mContentView = (LinearLayout) findViewById(R.id.ll_budget_edit);
        mDeleteBtn = findViewById(R.id.btn_confirm);
        mDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionDeleteBudget();
            }
        });
        final View activityRootView = findViewById(R.id.root_layout);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > ElseUtils.convertDp2Px(getActivity(), 200)) {
                    mDeleteBtn.setVisibility(View.GONE);
                } else {
                    mDeleteBtn.setVisibility(View.VISIBLE);
                }
            }
        });
        mGraph = findViewById(R.id.ll_spend_budget_current_graph_amount);
        mCurrentBudget = (TextView) findViewById(R.id.tv_spend_budget_current_value);
    }

    private void initData() {
        Intent intent = getIntent();
        mTargetYearMonth = intent.getStringExtra(Const.BUDGET_TARGET_MONTH);
        mTargetData = (SpendBudgetData) intent.getSerializableExtra(Const.DATA);
        mTotalBudget.setText(ElseUtils.getDecimalFormat(mTargetData.totalSum));
        mTotalBudgetValue = mTargetData.totalSum;

        createData(mTargetData);
    }

    private void createData(SpendBudgetData spendBudgetData) {
        mGroupData.clear();
        mChildData.clear();
        BudgetGroupCategory[] group = BudgetGroupCategory.values();
        for (int i=0; i < group.length; i++) {
            BudgetGroupCategory groupCategory = group[i];
            SpendBudgetCategoryEdit data = new SpendBudgetCategoryEdit();
            data.budgetId = 0;
            data.categoryGroup = groupCategory.ordinal();
            data.expense = 0;
            data.sum = 0;
            data.editType = SpendBudgetCategoryEdit.EditType.GROUP;
            if (i == 0) {
                data.editType = SpendBudgetCategoryEdit.EditType.TOTAL;
                data.sum = spendBudgetData.totalSum/10000;
                data.expense = spendBudgetData.totalExpense;
                data.mEditText = String.valueOf((spendBudgetData.totalSum/10000));
            } else {
                for (SpendBudgetCategory targetCategory : spendBudgetData.categoryList) {
                    if (targetCategory.categoryGroup == groupCategory.ordinal()) {
                        data.budgetId = targetCategory.budgetId;
                        data.categoryGroup = targetCategory.categoryGroup;
                        data.expense = targetCategory.expense;
                        data.sum = targetCategory.sum/10000;
                        data.mEditText = (targetCategory.sum != 0 ? String.valueOf((targetCategory.sum/10000)):null);
                        break;
                    }
                }
            }

            mGroupData.put(groupCategory.ordinal(), data);
            ArrayList<SpendBudgetDetailEdit> detailList = new ArrayList<>();
            BudgetCategory[] detailCategory = groupCategory.child();
            for (BudgetCategory detail : detailCategory) {
                if (detail.ordinal() != 0) {
                    SpendBudgetDetailEdit budgetDetail = new SpendBudgetDetailEdit();
                    budgetDetail.budgetId = 0;
                    budgetDetail.largeCategoryId = detail.ordinal();
                    budgetDetail.sum = 0;
                    for (SpendBudgetDetail targetDetail : spendBudgetData.detailList) {
                        if (targetDetail.largeCategoryId == detail.ordinal()) {
                            budgetDetail.budgetId = targetDetail.budgetId;
                            budgetDetail.detailId = targetDetail.detailId;
                            budgetDetail.largeCategoryId = targetDetail.largeCategoryId;
                            budgetDetail.expense = targetDetail.expense;
                            budgetDetail.sum = targetDetail.sum/10000;
                            budgetDetail.mEditText = (targetDetail.sum != 0 ? String.valueOf((targetDetail.sum/10000)):null);
                            break;
                        }
                    }
                    detailList.add(budgetDetail);
                }
            }
            mChildData.put(groupCategory.ordinal(), detailList);
        }

        showContent();
    }

    private void showContent() {
        mContentView.removeAllViews();
        Object[] keyArray = mGroupData.keySet().toArray();
        String[] categoryGroup = getResources().getStringArray(R.array.spend_budget_group_category);
        for (int i = 0; i < mGroupData.size(); i++) {
            SpendBudgetCategoryEdit categoryEdit = mGroupData.get(keyArray[i]);
            View headerView = this.getLayoutInflater().inflate(R.layout.row_item_group_budget_edit, mContentView, false);
            categoryEdit.mItemView = headerView;
            if (categoryEdit.editType.ordinal() == SpendBudgetCategoryEdit.EditType.GROUP.ordinal()) {
                GroupHolder groupHolder = new GroupHolder();
                groupHolder.mGroupTitle = (TextView)headerView.findViewById(R.id.tv_row_spend_budget_title);
                groupHolder.mGroupTitle.setTypeface(Typeface.DEFAULT_BOLD);
                groupHolder.mGroupBudget = (EditText)headerView.findViewById(R.id.et_row_spend_budget_category);
                groupHolder.mGroupUnit = (TextView) headerView.findViewById(R.id.tv_row_spend_budget_unit);
                groupHolder.mWatcher = new IndexTextWatcher(categoryEdit);
                groupHolder.mGroupBudget.addTextChangedListener(groupHolder.mWatcher);
                groupHolder.mId = categoryEdit.editType.ordinal();
                headerView.setTag(groupHolder);

                String title = null;
                try {
                    title = categoryGroup[categoryEdit.categoryGroup];
                } catch (Exception e) {
                    e.printStackTrace();
                }
                groupHolder.mGroupTitle.setText(title);
                String budget = null;
                if (categoryEdit.mEditText != null && !TextUtils.isEmpty(categoryEdit.mEditText)) {
                    budget = categoryEdit.mEditText;
                } else {
                    if (categoryEdit.sum > 0) {
                        budget = String.valueOf(categoryEdit.sum);
                    }
                }
                if (budget != null) {
                    groupHolder.mGroupBudget.setText(ElseUtils.getStringDecimalFormat(budget));
                }
            } else {
                GroupHolder groupHolder = new GroupHolder();
                groupHolder.mGroupTitle = (TextView)headerView.findViewById(R.id.tv_row_spend_budget_title);
                groupHolder.mGroupBudget = (EditText)headerView.findViewById(R.id.et_row_spend_budget_category);
                groupHolder.mGroupTitle.setTypeface(Typeface.DEFAULT_BOLD);
                groupHolder.mGroupUnit = (TextView) headerView.findViewById(R.id.tv_row_spend_budget_unit);
                groupHolder.mWatcher = new IndexTextWatcher(categoryEdit);
                groupHolder.mGroupBudget.addTextChangedListener(groupHolder.mWatcher);
                groupHolder.mId = categoryEdit.editType.ordinal();
                headerView.setTag(groupHolder);

                String title = "총 예산";
                groupHolder.mGroupTitle.setText(title);
                String budget = null;
                if (categoryEdit.mEditText != null && !TextUtils.isEmpty(categoryEdit.mEditText)) {
                    budget = categoryEdit.mEditText;
                } else {
                    if (categoryEdit.sum > 0) {
                        budget = String.valueOf(categoryEdit.sum);
                    }
                }
                if (budget != null) {
                    groupHolder.mGroupBudget.setText(ElseUtils.getStringDecimalFormat(budget));
                }
            }
            mContentView.addView(headerView);

            if (categoryEdit.editType.ordinal() == SpendBudgetCategoryEdit.EditType.GROUP.ordinal()) {
                ArrayList<SpendBudgetDetailEdit> list = mChildData.get(keyArray[i]);
                if (list != null) {
                    String[] detailArray = getResources().getStringArray(R.array.spend_category);
                    for (SpendBudgetDetailEdit edit : list) {
                        ChildHolder childHolder = new ChildHolder();
                        View childView = this.getLayoutInflater().inflate(R.layout.row_item_child_budget_edit, mContentView, false);
                        edit.mItemView = childView;
                        childHolder.mTitle = (TextView) childView.findViewById(R.id.tv_row_spend_budget_title);
                        childHolder.mEditText = (EditText) childView.findViewById(R.id.et_row_spend_budget_sub_category);
                        childHolder.mWatcher = new IndexTextWatcher(edit);
                        childHolder.mEditText.addTextChangedListener(childHolder.mWatcher);
                        childView.setTag(childHolder);

                        String title = detailArray[edit.largeCategoryId];
                        childHolder.mTitle.setText(title);


                        String budget = null;
                        if (edit.mEditText != null && !TextUtils.isEmpty(edit.mEditText)) {
                            budget = edit.mEditText.replaceAll(",", "");
                        } else {
                            if (edit.sum > 0) {
                                budget = String.valueOf(edit.sum);
                            }
                        }
                        if (budget != null) {
                            childHolder.mEditText.setText(ElseUtils.getStringDecimalFormat(budget));
                        }
                        mContentView.addView(childView);
                    }
                }
            }
        }
    }

    public void onBtnClickNaviMotion(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_motion_else:
                editBudget();
                break;
            case R.id.ll_navi_motion_back:
                onBackPressed();
                break;
        }
    }

    class GroupHolder {
        public int mId;
        public TextView mGroupTitle;
        public EditText mGroupBudget;
        public TextView mGroupUnit;
        public IndexTextWatcher mWatcher;
    }

    class ChildHolder{
        /* child */
        public int mId;
        public TextView mTitle;
        public EditText mEditText;
        public TextView mUnit;
        public IndexTextWatcher mWatcher;
    }

    private class IndexTextWatcher implements TextWatcher {
        private int index = 0;
        String strAmount = "";
        Object mObj;

        public IndexTextWatcher(Object object) {
            mObj = object;
        }

        public void setDataSet(Object object) {
            mObj = object;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mObj instanceof SpendBudgetCategoryEdit) {
                SpendBudgetCategoryEdit edit = ((SpendBudgetCategoryEdit) mObj);
                String text = s.toString();
                if (!strAmount.equalsIgnoreCase(text)) {
                    GroupHolder groupHolder = (GroupHolder) edit.mItemView.getTag();
                    strAmount = makeStringComma(text.replaceAll(",", ""));
                    groupHolder.mGroupBudget.setText(strAmount);
                }
                if (text != null && !TextUtils.isEmpty(text)) {
                    text = text.replaceAll(",", "");
                }
                if (edit.mEditText != null){
                    if (!edit.mEditText.equalsIgnoreCase(text)) {
                        edit.mEditText = text;
                        mObserver.onChanged();
                    }
                } else {
                    edit.mEditText = text;
                    mObserver.onChanged();
                }
            } else if (mObj instanceof SpendBudgetDetailEdit) {
                SpendBudgetDetailEdit edit = ((SpendBudgetDetailEdit) mObj);
                String text = s.toString();
                if (!strAmount.equalsIgnoreCase(text)) {
                    ChildHolder childHolder = (ChildHolder) edit.mItemView.getTag();
                    strAmount = makeStringComma(text.replaceAll(",", ""));
                    childHolder.mEditText.setText(strAmount);
                }
                if (text != null && !TextUtils.isEmpty(text)) {
                    text = text.replaceAll(",", "");
                }
                if (edit.mEditText != null) {
                    if (!edit.mEditText.equalsIgnoreCase(text)) {
                        String str = text;
                        if (TextUtils.isEmpty(str)) {
                            str = "";
                        }
                        edit.mEditText = str;
                        mObserver.onChanged();
                    }
                } else {
                    edit.mEditText = text;
                    mObserver.onChanged();
                }
            }
            if (s.length() > 0) {
                Selection.setSelection(s, s.length());
            }
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        protected String makeStringComma(String str) {
            if (str.length() == 0)
                return "";
            long value = Long.parseLong(str);
            DecimalFormat format = new DecimalFormat("###,###");
            return format.format(value);
        }
    }

    private DataSetObserver mObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();

            TreeMap<Integer, SpendBudgetCategoryEdit> map = mGroupData;
            HashMap<Integer, ArrayList<SpendBudgetDetailEdit>> detailMap = mChildData;

            Object[] detailKeys = detailMap.keySet().toArray();
            for (int i=0; i < detailMap.size(); i++) {
                int groupCategory = (int)detailKeys[i];
                ArrayList<SpendBudgetDetailEdit> detailEdits = detailMap.get(groupCategory);
                long totalBudget = 0;
                if (detailEdits != null) {
                    for (SpendBudgetDetailEdit detailEdit : detailEdits) {
                        long budget = 0;
                        if (detailEdit.mEditText != null && !TextUtils.isEmpty(detailEdit.mEditText)) {
                            String value = detailEdit.mEditText.replaceAll(",", "");
                            budget = Long.valueOf(value);
                        }
                        totalBudget += budget;
                    }
                }
                SpendBudgetCategoryEdit categoryEdit = map.get(groupCategory);
                long groupBudget = 0;
                if (categoryEdit.mEditText != null && !TextUtils.isEmpty(categoryEdit.mEditText)) {
                    String value = categoryEdit.mEditText;
                    groupBudget = Long.valueOf(value);
                } else {
                    categoryEdit.mEditText = String.valueOf(totalBudget);
                    if (categoryEdit.mItemView != null) {
                        GroupHolder holder = (GroupHolder) categoryEdit.mItemView.getTag();
                        holder.mGroupBudget.setText(ElseUtils.getStringDecimalFormat(categoryEdit.mEditText));
                    }
                }
                if (groupBudget < totalBudget) {
                    categoryEdit.mEditText = String.valueOf(totalBudget);
                    if (categoryEdit.mItemView != null) {
                        GroupHolder holder = (GroupHolder) categoryEdit.mItemView.getTag();
                        holder.mGroupBudget.setText(ElseUtils.getStringDecimalFormat(categoryEdit.mEditText));
                    }
                }
            }

            long totalGroupBudget = 0;
            long groupBudget = 0;
            if (map != null) {
                Object[] keys = map.keySet().toArray();
                for (int i=0; i < map.size(); i++) {
                    SpendBudgetCategoryEdit categoryEdit = map.get((int)keys[i]);
                    if (categoryEdit != null) {
                        if (categoryEdit.editType == SpendBudgetCategoryEdit.EditType.GROUP) {
                            long budget = categoryEdit.sum;
                            if (categoryEdit.mEditText != null && !TextUtils.isEmpty(categoryEdit.mEditText)) {
                                String value = categoryEdit.mEditText;
                                if (!TextUtils.isEmpty(value)) {
                                    budget = Long.valueOf(value);
                                }
                            }
                            totalGroupBudget += budget;
                        }
                    }
                }
                SpendBudgetCategoryEdit totalGroup = map.get(0);
                groupBudget = 0;
                if (totalGroup.mEditText != null && !TextUtils.isEmpty(totalGroup.mEditText)) {
                    String value = totalGroup.mEditText;
                    groupBudget = Long.valueOf(value);
                } else {
                    GroupHolder holder = (GroupHolder) totalGroup.mItemView.getTag();
                    holder.mGroupBudget.setText(ElseUtils.getStringDecimalFormat(totalGroup.mEditText));
                }
                if (groupBudget <= totalGroupBudget) {
                    totalGroup.mEditText = String.valueOf(totalGroupBudget);
                    mTotalBudgetValue = totalGroupBudget * 10000;
                    GroupHolder holder = (GroupHolder) totalGroup.mItemView.getTag();
                    holder.mGroupBudget.setText(ElseUtils.getStringDecimalFormat(totalGroup.mEditText));
                }
            }

            long leftValue = mTotalBudgetValue - (totalGroupBudget * 10000);
            if (leftValue >= 0) {
                mCurrentBudget.setText(ElseUtils.getDecimalFormat(leftValue)+" 남음");
            } else {
                mCurrentBudget.setText(ElseUtils.getDecimalFormat(Math.abs(leftValue))+" 남음");
            }


            setGraph(totalGroupBudget * 10000, leftValue);
        }
    };

    private void setGraph(long current, long left) {
        mTotalBudget.setText(ElseUtils.getDecimalFormat(mTotalBudgetValue));

        if (current == 0) {
            mGraph.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraph.getLayoutParams();
            params.weight = 1;
            mGraph.setLayoutParams(params);
            Drawable drawable = mGraph.getBackground();
            if (left >= 0) {
                if (drawable instanceof ShapeDrawable) {
                    ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                } else if (drawable instanceof LayerDrawable) {
                    LayerDrawable layerDrawable = (LayerDrawable) drawable;
                    ShapeDrawable shapeDrawable = (ShapeDrawable) layerDrawable.getDrawable(0);
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                } else if (drawable instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                    gradientDrawable.setColor(getResources().getColor(R.color.common_white));
                }
            } else {
                if (drawable instanceof ShapeDrawable) {
                    ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_graph_over));
                } else if (drawable instanceof LayerDrawable) {
                    LayerDrawable layerDrawable = (LayerDrawable) drawable;
                    ShapeDrawable shapeDrawable = (ShapeDrawable) layerDrawable.getDrawable(0);
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_graph_over));
                } else if (drawable instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                    gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_graph_over));
                }
            }
        } else {
            mGraph.setVisibility(View.VISIBLE);
            //mGraph.setAnimation(animation);
            if (mTotalBudgetValue != 0) {
                float value = (float) ((double)current/(double)mTotalBudgetValue);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraph.getLayoutParams();
                params.weight = value;
                mGraph.setLayoutParams(params);
                Drawable drawable = mGraph.getBackground();
                if (left >= 0) {
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                    } else if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ShapeDrawable shapeDrawable = (ShapeDrawable) layerDrawable.getDrawable(0);
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.common_white));
                    }
                } else {
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    } else if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ShapeDrawable shapeDrawable = (ShapeDrawable) layerDrawable.getDrawable(0);
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    }
                }
            }
        }
    }

    private void questionDeleteBudget() {
        DialogUtils.showDialogBaseTwoButton(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case R.string.common_confirm:
                        deleteBudget();
                        dialog.dismiss();
                        break;
                    case R.string.common_cancel:
                        dialog.dismiss();
                        break;
                }
            }
        }, getResources().getString(R.string.spend_budget_edit_delete_content));
    }

    private void deleteBudget() {
        sendTracker(AnalyticsName.SpendBudgetReset);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("budgetId", mTargetData.budgetId);
        Call<Result> call = ServiceGenerator.createService(SpendApi.class).deleteBudget(hashMap);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response != null && response.message().equalsIgnoreCase(Const.OK)) {
                    if(response.body().isOk()){
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void editBudget() {
        long highestSum = mTotalBudgetValue;
        TreeMap<Integer, SpendBudgetCategoryEdit> map = mGroupData;
        HashMap<Integer, ArrayList<SpendBudgetDetailEdit>> detailMap = mChildData;
        List<SpendBudgetCategory> categoryList = new ArrayList<>();
        List<SpendBudgetDetail> detailList = new ArrayList<>();
        int count = 0;
        long currentValue = 0;
        Object[] keys = map.keySet().toArray();
        for (int i=0; i < map.size(); i++) {
            SpendBudgetCategoryEdit categoryEdit = map.get((int)keys[i]);
            if (categoryEdit != null) {
                long budget = categoryEdit.sum;
                if (categoryEdit.mEditText != null && !TextUtils.isEmpty(categoryEdit.mEditText)) {
                    String value = categoryEdit.mEditText;
                    budget = Long.valueOf(value);
                }
                if (categoryEdit.editType == SpendBudgetCategoryEdit.EditType.GROUP) {
                    if (budget > 0) {
                        count++;
                        SpendBudgetCategory category = new SpendBudgetCategory();
                        category.budgetId = categoryEdit.budgetId;
                        category.categoryGroup = categoryEdit.categoryGroup;
                        category.sum = budget*10000;
                        categoryList.add(category);
                    } else if (budget == 0 && categoryEdit.sum > 0) {
                        SpendBudgetCategory category = new SpendBudgetCategory();
                        category.budgetId = categoryEdit.budgetId;
                        category.categoryGroup = categoryEdit.categoryGroup;
                        category.sum = 0;
                        categoryList.add(category);
                    }
                    currentValue += budget;
                } else if (categoryEdit.editType == SpendBudgetCategoryEdit.EditType.TOTAL) {
                    highestSum = budget * 10000;
                }
            }
        }
        if (count == 0) {
            CommonDialogTextType dialogTextType = new CommonDialogTextType(this, new int[] {R.string.common_confirm});
            dialogTextType.addTextView(R.string.spend_budget_edit_warning, R.color.common_maintext_color, 15);
            dialogTextType.show();
            return;
        }
        Object[] detailKeys = detailMap.keySet().toArray();
        for (int i=0; i < detailMap.size(); i++) {
            ArrayList<SpendBudgetDetailEdit> detailEdits = detailMap.get((int)detailKeys[i]);
            if (detailEdits != null) {
                for (SpendBudgetDetailEdit detailEdit : detailEdits) {
                    long budget = detailEdit.sum;
                    if (detailEdit.mEditText != null && !TextUtils.isEmpty(detailEdit.mEditText)) {
                        String value = detailEdit.mEditText.replaceAll(",", "");
                        budget = Long.valueOf(value);
                    }
                    if (budget > 0) {
                        SpendBudgetDetail detail = new SpendBudgetDetail();
                        detail.detailId = detailEdit.detailId;
                        detail.largeCategoryId = detailEdit.largeCategoryId;
                        detail.sum = budget*10000;
                        detailList.add(detail);
                    } else if (budget == 0 && detailEdit.sum > 0) {
                        SpendBudgetDetail detail = new SpendBudgetDetail();
                        detail.detailId = detailEdit.detailId;
                        detail.largeCategoryId = detailEdit.largeCategoryId;
                        detail.sum = 0;
                        detailList.add(detail);
                    }
                }
            }
        }
        SpendBudgetData data = new SpendBudgetData();
        data.budgetId = mTargetData.budgetId;
        data.totalSum = highestSum;
        data.categoryList = categoryList;
        data.detailList = detailList;
        Call<Result> call = ServiceGenerator.createService(SpendApi.class).editBudget(data);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if(getActivity() == null || getActivity().isFinishing()){
                    return;
                }

                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        Result result = response.body();
                        if (result != null && result.isOk()) {
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            Toast.makeText(getBase(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 다시 등록해 주세요.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        ErrorMessage message = ErrorUtils.parseError(response);
                        switch (message) {
                            case ERROR_713_NO_SUCH_USER:
                                Toast.makeText(getBase(), "가입되지 않은 사용자입니다. 새로 회원가입해주세요.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_719_QUITE_USER:
                                Toast.makeText(getBase(), "탈퇴 회원은 탈퇴일로부터 5일 이후 재가입 가능합니다.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_1011_CERT_SESSION_EXPIRE:
                                Toast.makeText(getBase(), "인증시간이 만료되었습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_1012_CERT_NOT_FINISH:
                                Toast.makeText(getBase(), "인증에 실패하였습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                ElseUtils.network_error(getBase());
            }
        });
    }

}
