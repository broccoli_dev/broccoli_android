package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialReg;
import com.nomadconnection.broccoli.adapter.AdtExpListViewAssetDaS;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetBankInfo;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetBankDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;

import java.util.ArrayList;


public class Level2ActivityAssetDaS extends BaseActivity {

	private static final String TAG = Level2ActivityAssetDaS.class.getSimpleName();

	private static final int RESULT_EDIT_ACCOUNT = 1;

	/* Layout */
	private ExpandableListView mExpListView;
	private AdtExpListViewAssetDaS mAdapter;
	private ArrayList<String> mGroupList = null;
	private ArrayList<ArrayList<AssetBankInfo>> mChildList = null;
	private ArrayList<AssetBankInfo> mAlInfoDataList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_asset_das);
		sendTracker(AnalyticsName.Level2ActivityAssetDaS);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}



	@Override
	public void onResume() {
		super.onResume();
	}
	
	

//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {

			case R.id.ll_navi_img_txt_txt_back:
				finish();
				break;
			case R.id.ll_navi_img_txt_txt_else:
				if (mAlInfoDataList == null || mAlInfoDataList.size() == 0) {
					Intent setting_financial = new Intent(this, Level3ActivitySettingFinancialReg.class);
					startActivity(setting_financial);
				} else {
					Intent editAccount = new Intent(this, Level3ActivityAssetEditAccount.class);
					editAccount.putExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_1, mAlInfoDataList);
					startActivityForResult(editAccount, RESULT_EDIT_ACCOUNT);
				}
				break;

			default:
				break;
			}
		}
		catch(Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());			
		}
	}
	
	
	
	
	
	
	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mAlInfoDataList = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_1);
			mGroupList = new ArrayList<String>();
			mChildList = new ArrayList<ArrayList<AssetBankInfo>>();

			if(mAlInfoDataList == null){
				bResult = false;
			}

		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_img_txt_txt_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitle);
			if (mAlInfoDataList != null && mAlInfoDataList.size() > 0) {
				((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_edit));
			} else {
				((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_add));
			}
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) {
				((RelativeLayout)findViewById(R.id.rl_navi_img_txt_txt_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			}

			/* Layout */
			mExpListView = (ExpandableListView)findViewById(R.id.elv_level_2_activity_asset_das_listview);
			mAdapter = new AdtExpListViewAssetDaS(this, mGroupList, mChildList);
			mExpListView.setAdapter(mAdapter);

			/* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return true;
				}
			});

			/* Child list click 시 */
			mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
											int groupPosition, int childPosition, long id) {
					AssetBankInfo mTemp = null;
					mTemp = mChildList.get(groupPosition).get(childPosition);
					if (mTemp == null) return true;

					NextStep(mTemp, new ArrayList<ResponseAssetBankDetail>());
					return false;
				}
			});
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	/**
	 * initial method
	 * <p> show child list view</p>
	 * @return
     */
	private boolean initData() {
		boolean bResult = true;
		try {
			resetData();
			if (mAlInfoDataList != null){
				if (mAlInfoDataList.size() == 0) {
					((FrameLayout)findViewById(R.id.inc_level_2_activity_asset_das_empty)).setVisibility(View.VISIBLE);
					((ImageView)findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
					((TextView)findViewById(R.id.tv_empty_layout_txt)).setText(getString(R.string.common_empty_das));
				} else {
					if ((mAlInfoDataList != null && mAlInfoDataList.size() > 0) && mGroupList != null && mGroupList.size() == 0) {
						((FrameLayout)findViewById(R.id.inc_level_2_activity_asset_das_empty)).setVisibility(View.VISIBLE);
						((ImageView)findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.bank_account_icon);
						((TextView)findViewById(R.id.tv_empty_layout_txt)).setText(getString(R.string.common_empty_edit_account));
						TextView textView = (TextView)findViewById(R.id.tv_empty_layout_txt_second);
						textView.setVisibility(View.VISIBLE);
						textView.setText(R.string.common_empty_edit_account_second);
					} else {
						((FrameLayout)findViewById(R.id.inc_level_2_activity_asset_das_empty)).setVisibility(View.GONE);
					}
				}
			}
			for (int i = 0; i <mGroupList.size() ; i++) {
				mExpListView.expandGroup(i);
			}
		}
		catch(Exception e) {
			bResult = false;
			e.printStackTrace();
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//	
	/**  Item Click Listener **/
	private OnItemClickListener ItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, final int position,
				long id) {
			if (mAlInfoDataList.get(position) == null) {	// test 임시
				return;
			}

			/*group 2*/
			NextStep(mAlInfoDataList.get(position), new ArrayList<ResponseAssetBankDetail>());
		}
	};

//=========================================================================================//
// Refresh
//=========================================================================================//
	private void resetData(){
		mGroupList.clear();
		mChildList.clear();

		for (int i = 0; i < mAlInfoDataList.size(); i++) {
			if (i == 0) {
				if (mAlInfoDataList.get(i).isChecked()) {
					mGroupList.add(mAlInfoDataList.get(i).mAssetBankBankCode);
				}
			} else {
				if (!mGroupList.contains(mAlInfoDataList.get(i).mAssetBankBankCode)){
					if (mAlInfoDataList.get(i).isChecked()) {
						mGroupList.add(mAlInfoDataList.get(i).mAssetBankBankCode);
					}
				}
			}
		}

		ArrayList<AssetBankInfo> childList;
		for (int i = 0; i < mGroupList.size() ; i++) {
			childList = new ArrayList<AssetBankInfo>();
			for (int j = 0; j < mAlInfoDataList.size(); j++) {
				if (mGroupList.get(i).equalsIgnoreCase(mAlInfoDataList.get(j).mAssetBankBankCode)){
					if (mAlInfoDataList.get(j).isChecked()) {
						childList.add(mAlInfoDataList.get(j));
					}
				}
			}
			mChildList.add(childList);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RESULT_EDIT_ACCOUNT) {
			if (resultCode == RESULT_OK) {
				mAlInfoDataList = data.getParcelableArrayListExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_1);
				initData();
				mAdapter.notifyDataSetChanged();
			}
		}
	}

	//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep **/
	private void NextStep(AssetBankInfo _AssetBankInfo, ArrayList<ResponseAssetBankDetail> _Respose){
		startActivity(new Intent(Level2ActivityAssetDaS.this, Level3ActivityAssetDasDetail.class)
						.putExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_2_HEADER, _AssetBankInfo)
						.putExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_2_BODY, _Respose)
		);
	}
	




}
