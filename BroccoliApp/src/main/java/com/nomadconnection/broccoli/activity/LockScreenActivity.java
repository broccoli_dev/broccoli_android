package com.nomadconnection.broccoli.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.common.LockScreenUserMatchActivity;
import com.nomadconnection.broccoli.api.FileDownloadService;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.ServiceUrl;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.CdnAppInfoData;
import com.nomadconnection.broccoli.data.InfoDataAppVersion;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.dialog.DialogBaseTwoButton;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.Crypto;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.utils.NetStatusChecker;
import com.nomadconnection.broccoli.utils.PermissionUtils;
import com.nomadconnection.broccoli.constant.SharedPreferenceConstant;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.internal.FacebookRequestErrorClassification.KEY_NAME;

/**
 * Created by YelloHyunminJang on 16. 2. 1..
 */
public class LockScreenActivity extends BaseActivity implements View.OnClickListener {

    public static final int RESULT_PERMISSION_SETTING = 2;

    private static final int RESULT_WIFI_SETTING = 1;
    private static final int RESULT_CERT_LOGIN = 2;
    private static final int MAX_FAIL_COUNT = 5;
    private static final long FAIL_TIME_OUT = 1000*60;

    private EditText mSecretEditText;
    private ImageView mIndicator1, mIndicator2, mIndicator3, mIndicator4, mIndicator5, mIndicator6;
    private boolean mCheckNotice = false;
    private boolean mCheckVersion = false;
    private DialogBaseTwoButton mPopup;
    private FingerprintManager mFingerprintManager;
    private KeyStore mKeyStore;
    private KeyGenerator mKeyGenerator;
    private Cipher mCipher;
    private FingerprintManager.CryptoObject mCryptoObject;
    private FingerprintHandler mFingerPrintHandler;
    private CommonDialogTextType mFingerPrintDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lock_screen);
        sendTracker(AnalyticsName.LockScreenActivity);
        init();
        initFingerPrint();
        permissionCheck();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCheck();
        startFingerPrint();
    }

    private void init() {
        Session.getInstance(this);
        mIndicator1 = (ImageView) findViewById(R.id.screen_lock_num1_imgview);
        mIndicator2 = (ImageView) findViewById(R.id.screen_lock_num2_imgview);
        mIndicator3 = (ImageView) findViewById(R.id.screen_lock_num3_imgview);
        mIndicator4 = (ImageView) findViewById(R.id.screen_lock_num4_imgview);
        mIndicator5 = (ImageView) findViewById(R.id.screen_lock_num5_imgview);
        mIndicator6 = (ImageView) findViewById(R.id.screen_lock_num6_imgview);

        findViewById(R.id.screen_lock_key_num_0).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_1).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_2).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_3).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_4).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_5).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_6).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_7).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_8).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_9).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_back).setOnClickListener(this);
        findViewById(R.id.screen_lock_pin_reset_btn).setOnClickListener(this);
        mSecretEditText = (EditText) findViewById(R.id.secret_edittext);
        mSecretEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showLengthIndicator(s.length());
                if (s.length() == 6) {
                    String confirm = s.toString();
                    checkPwd(confirm);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.screen_lock_key_num_0:
                mSecretEditText.setText(mSecretEditText.getText() + "0");
                break;
            case R.id.screen_lock_key_num_1:
                mSecretEditText.setText(mSecretEditText.getText()+"1");
                break;
            case R.id.screen_lock_key_num_2:
                mSecretEditText.setText(mSecretEditText.getText() + "2");
                break;
            case R.id.screen_lock_key_num_3:
                mSecretEditText.setText(mSecretEditText.getText() + "3");
                break;
            case R.id.screen_lock_key_num_4:
                mSecretEditText.setText(mSecretEditText.getText() + "4");
                break;
            case R.id.screen_lock_key_num_5:
                mSecretEditText.setText(mSecretEditText.getText() + "5");
                break;
            case R.id.screen_lock_key_num_6:
                mSecretEditText.setText(mSecretEditText.getText() + "6");
                break;
            case R.id.screen_lock_key_num_7:
                mSecretEditText.setText(mSecretEditText.getText() + "7");
                break;
            case R.id.screen_lock_key_num_8:
                mSecretEditText.setText(mSecretEditText.getText() + "8");
                break;
            case R.id.screen_lock_key_num_9:
                mSecretEditText.setText(mSecretEditText.getText() + "9");
                break;
            case R.id.screen_lock_key_num_back:
                Editable text = mSecretEditText.getText();
                if (text.length()-1 > 0) {
                    mSecretEditText.setText(text.delete(text.length()-2,text.length()-1));
                } else {
                    mSecretEditText.setText("");
                }
                break;
            case R.id.screen_lock_pin_reset_btn:
                screenLockReset();
                break;
        }
    }

    private void showLengthIndicator(int count) {
        switch (count) {
            case 0:
                mIndicator1.setImageResource(R.drawable.shape_lock_indicator_off);
                mIndicator2.setImageResource(R.drawable.shape_lock_indicator_off);
                mIndicator3.setImageResource(R.drawable.shape_lock_indicator_off);
                mIndicator4.setImageResource(R.drawable.shape_lock_indicator_off);
                mIndicator5.setImageResource(R.drawable.shape_lock_indicator_off);
                mIndicator6.setImageResource(R.drawable.shape_lock_indicator_off);
                break;
            case 1:
                mIndicator1.setImageResource(R.drawable.shape_lock_indicator_on);
                mIndicator2.setImageResource(R.drawable.shape_lock_indicator_off);
                break;
            case 2:
                mIndicator2.setImageResource(R.drawable.shape_lock_indicator_on);
                mIndicator3.setImageResource(R.drawable.shape_lock_indicator_off);
                break;
            case 3:
                mIndicator3.setImageResource(R.drawable.shape_lock_indicator_on);
                mIndicator4.setImageResource(R.drawable.shape_lock_indicator_off);
                break;
            case 4:
                mIndicator4.setImageResource(R.drawable.shape_lock_indicator_on);
                mIndicator5.setImageResource(R.drawable.shape_lock_indicator_off);
                break;
            case 5:
                mIndicator5.setImageResource(R.drawable.shape_lock_indicator_on);
                mIndicator6.setImageResource(R.drawable.shape_lock_indicator_off);
                break;
            case 6:
                mIndicator6.setImageResource(R.drawable.shape_lock_indicator_on);
                break;
        }
    }

    private int mFailCount = 0;
    private int mOverCount = 0;
    private void checkPwd(final String pwd) {
        mOverCount = ElseUtils.getLockOverCount(getApplicationContext());
        DialogUtils.showLoading(this);
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                DialogUtils.dismissLoading();
                if (comparePwd(str, pwd)) {
                    grantAccess();
                } else {
                    mSecretEditText.setText(null);
                    Toast.makeText(LockScreenActivity.this, "비밀번호가 일치하지 않습니다.", Toast.LENGTH_LONG).show();
                }
                if (mFailCount >= MAX_FAIL_COUNT) {
                    mOverCount++;
                    mFailCount = 0;
                    Calendar calendar = Calendar.getInstance();
                    long time = calendar.getTimeInMillis();
                    time += FAIL_TIME_OUT;
                    ElseUtils.setLockDownTime(getApplicationContext(), String.valueOf(time));
                    ElseUtils.setLockOverCount(getApplicationContext(), mOverCount);
                    if (mOverCount >= 2) {
                        showCertLogin();
                    } else {
                        showLockDialog();
                    }
                }
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
                Toast.makeText(LockScreenActivity.this, "서버와의 통신에 실패하였습니다. 브로콜리 앱을 재시작 해주세요.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean comparePwd(String key, String str) {
        boolean bool = false;
        LoginData data = DbManager.getInstance().getUserInfoDataSet(this, key, Session.getInstance(this).getUserId());
        String pwd = data.getPinNumber();
        if (pwd != null) {
            if (pwd.equals(str)) {
                bool = true;
            }
        }
        if (bool) {
            mFailCount = 0;
        } else {
            mFailCount++;
        }
        return bool;
    }


    private void grantAccess() {
        mFailCount = 0;
        ElseUtils.setLockDownTime(getApplicationContext(), null);
        ElseUtils.setLockOverCount(getApplicationContext(), mOverCount=0);
        Session.getInstance(this).setLock(false);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    private void showSetting() {
        Session.getInstance(this).setLockTemporaryDisable(true);
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        startActivityForResult(intent, RESULT_WIFI_SETTING);
    }

    private void showLockDialog() {
        DialogUtils.showDlgBaseOneButton(LockScreenActivity.this, null, new InfoDataDialog(R.layout.dialog_inc_case_10,
                getString(R.string.common_warning_pwd_mismatch_overtime_title), getString(R.string.common_confirm))
                , new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        ActivityCompat.finishAffinity(LockScreenActivity.this);
                    }
                });
    }

    private void showCertLogin() {
        DialogUtils.showDlgBaseOneButton(LockScreenActivity.this, null, new InfoDataDialog(R.layout.dialog_inc_case_10,
                        getString(R.string.common_warning_pin_number_overtime), getString(R.string.common_confirm))
                , new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        Intent intent = new Intent(getBase(), LockScreenUserMatchActivity.class);
                        intent.putExtra(LockScreenUserMatchActivity.PIN_ERROR_OVERTIME, true);
                        startActivityForResult(intent, RESULT_CERT_LOGIN);
                    }
                });
    }

    private void updateCheck() {

        DialogUtils.showLoading(this);

        if (!NetStatusChecker.getInstance().getNetworkConnectStatus(this)) {
            DialogUtils.dismissLoading();
            DialogUtils.showDlgBaseOneButtonNoTitle(LockScreenActivity.this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
                        showSetting();
                    }
                }
            }, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_network_error_guide), "확인"));
            return;
        }

        DialogUtils.dismissLoading();

        int certOver = ElseUtils.getCertOverCount(this);
        if (certOver >= 2) {
            showPopup(getString(R.string.common_warning_cert_pwd_mismatch_overtime_title), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    ActivityCompat.finishAffinity(LockScreenActivity.this);
                }
            });
            return;
        } else if (certOver == 1) {
            String certOverTime = ElseUtils.getCertOverTime(this);
            if (certOverTime != null && !TextUtils.isEmpty(certOverTime)) {
                long overtime = Long.valueOf(certOverTime);
                long current = Calendar.getInstance().getTimeInMillis();
                if (current < overtime) {
                    showPopup(getString(R.string.common_warning_cert_pwd_mismatch_title) + " 당일 사용이 제한됩니다.", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.finishAffinity(LockScreenActivity.this);
                        }
                    });
                } else {
                    ElseUtils.setCertOverTime(this, null);
                    showCertLogin();
                }
            } else {
                showCertLogin();
            }
            return;
        }

        mOverCount = ElseUtils.getLockOverCount(this);
        if (mOverCount >= 2) {
            showCertLogin();
            return;
        }

        String time = ElseUtils.getLockDownTime(this);
        if (time != null && !TextUtils.isEmpty(time)) {
            long lockTime = Long.valueOf(time);
            long current = Calendar.getInstance().getTimeInMillis();
            if (lockTime > current) {
                showLockDialog();
                return;
            } else {
                ElseUtils.setLockDownTime(this, null);
            }
        }

        DialogUtils.showLoading(this);

        FileDownloadService downloadService = FileDownloadService.retrofit.create(FileDownloadService.class);
        Call<ResponseBody> call = downloadService.downloadFileFromCdn(ServiceUrl.getCdnVersion());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    CdnAppInfoData data = null;
                    try {
                        String body = response.body().string();
                        String decBody = Crypto.decCDNAES(body);
                        JsonObject jsonObject = new Gson().fromJson(decBody, JsonObject.class);
                        data = new Gson().fromJson(jsonObject.get("aos"), CdnAppInfoData.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (data != null) {
                        APPSharedPrefer.getInstance(getBase()).setPrefString(SharedPreferenceConstant.CDN_INIT_KEY, new Gson().toJson(data));
                        String ver = data.version.min_version;
                        CdnAppInfoData.NoticeInfo notice = data.notice;
                        if (data == null || !data.notice_yn) {
                            mCheckNotice = true;
                        } else {
                            DialogUtils.showDialogBaseOneButton(LockScreenActivity.this, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onBackPressed();
                                }
                            }, notice.title, notice.contents);
                            DialogUtils.dismissLoading();
                            return;
                        }
                        if (ver != null) {
                            String[] server_ver = ver.split("\\.");
                            String[] local_ver = MainApplication.getVersionName().split("\\.");
                            int flag = ElseUtils.isHigherVersion(local_ver, server_ver);
                            if (flag < 0) {
                                InfoDataAppVersion version1 = new InfoDataAppVersion();
                                version1.mTxt1 = ver;
                                version1.mTxt2 = "알림/Broccoli 신버전이 배포되었습니다./확인을 눌러 신규버전을 받아주세요.";
                                version1.mTxt3 = "";
                                mPopup = DialogUtils.showDlgBaseTwoButton(LockScreenActivity.this, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
                                                    ElseUtils.showPlayStore(LockScreenActivity.this);
                                                } else {
                                                    onBackPressed();
                                                }
                                            }
                                        },
                                        new InfoDataDialog(R.layout.dialog_inc_case_5, "취소", "확인", getString(R.string.str_init_version_mismatch), version1));
                                DialogUtils.dismissLoading();
                                return;
                            } else {
                                mCheckVersion = true;
                                setServerAddress(data);
                            }
                        }
                    }
                } else {
                    DialogUtils.showDlgBaseOneButtonNoTitle(LockScreenActivity.this, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
                                onBackPressed();
                            }
                        }
                    }, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_warning_version_fail), "확인"));
                }
                DialogUtils.dismissLoading();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void setServerAddress(CdnAppInfoData data) {
        ServiceGenerator.setAddress(data);
    }

    private void permissionCheck() {
        HashMap<String, PermissionUtils.PermissionState> permissionState = new HashMap<>();
        permissionState.put(Manifest.permission.RECEIVE_SMS, null);
        permissionState.put(Manifest.permission.READ_EXTERNAL_STORAGE, null);
        permissionState.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, null);
        permissionState.put(Manifest.permission.READ_PHONE_STATE, null);
        if (PermissionUtils.checkPermission(this, permissionState)) {
            PermissionUtils.showPermissionDialog(this, permissionState, PermissionUtils.MY_PERMISSIONS_REQUEST_BROCCOLI);
            DialogUtils.dismissLoading();
            return;
        }
    }

    private void showPopup(String str, DialogInterface.OnClickListener dialog) {
        DialogUtils.showDlgBaseOneButtonNoTitle(this, dialog, new InfoDataDialog(R.layout.dialog_inc_case_1, str, "확인"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_WIFI_SETTING) {
            updateCheck();
        } else if (requestCode == RESULT_PERMISSION_SETTING) {
            permissionCheck();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionUtils.MY_PERMISSIONS_REQUEST_BROCCOLI: {
                HashMap<String, PermissionUtils.PermissionState> map = new HashMap<>();
                for (int i=0; i < permissions.length; i++) {
                    map.put(permissions[i], null);
                }
                if (PermissionUtils.checkPermission(this, map)) {
                    PermissionUtils.determineWhichPermissionDialog(map, mListener);
                } else {
                    updateCheck();
                }
                return;
            }
        }
    }

    private PermissionUtils.OnDeterminePermissionListener mListener = new PermissionUtils.OnDeterminePermissionListener() {

        @Override
        public void onEnabled() {
            updateCheck();
        }

        @Override
        public void onShowRequestDialog(HashMap<String, PermissionUtils.PermissionState> map) {
            CommonDialogTextType dialogTextType = new CommonDialogTextType(LockScreenActivity.this, new String[] {"설정", "다시 보기"});
            CommonDialogTextType.TextData main = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_permission_main);
            main.mTextStyle = Typeface.BOLD;
            main.mTextSize = 18;
            main.mLineSpace = getResources().getDimension(R.dimen.common_dimens_6);
            dialogTextType.addTextView(main);
            CommonDialogTextType.TextData text1 = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_permission_sms);
            text1.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
            text1.mLeftIcon = R.drawable.notify_list_icon;
            text1.mMargin = (int) LockScreenActivity.this.getResources().getDimension(R.dimen.common_dimens_20);
            CommonDialogTextType.TextData text2 = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_permission_storage);
            text2.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
            text2.mLeftIcon = R.drawable.notify_list_icon;
            CommonDialogTextType.TextData text3 = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_permission_phone);
            text3.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
            text3.mLeftIcon = R.drawable.notify_list_icon;
            dialogTextType.addTextView(text1);
            dialogTextType.addTextView(text2);
            dialogTextType.addTextView(text3);
            ImageView imageView = new ImageView(dialogTextType.getContext());
            imageView.setImageResource(R.drawable.permission_setting1);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            Button[] buttons = dialogTextType.getButtons();
            buttons[0].setBackground(getResources().getDrawable(R.drawable.selector_permission_button_popup));
            buttons[0].setTextColor(getResources().getColor(R.color.common_argb_60p_255_255_255));
            buttons[1].setBackground(getResources().getDrawable(R.drawable.selector_permission_button_popup));
            buttons[1].setTextColor(getResources().getColor(R.color.common_white));
            dialogTextType.addCustomBody(imageView);
            dialogTextType.setCancelable(false);
            dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int stringresid) {
                    if (stringresid == 0) {
                        PermissionUtils.showSettingPermission(LockScreenActivity.this, RESULT_PERMISSION_SETTING);
                    } else {
                        permissionCheck();
                    }
                }
            });
            dialogTextType.show();
        }

        @Override
        public void onShowPermissionSettingDialog(HashMap<String, PermissionUtils.PermissionState> map) {
            CommonDialogTextType dialogTextType = new CommonDialogTextType(LockScreenActivity.this, new String[] {"권한설정 하러가기"});
            CommonDialogTextType.TextData main = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_permission_main);
            main.mTextStyle = Typeface.BOLD;
            main.mTextSize = 18;
            main.mLineSpace = getResources().getDimension(R.dimen.common_dimens_6);
            dialogTextType.addTextView(main);
            CommonDialogTextType.TextData text1 = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_permission_sms);
            text1.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
            text1.mLeftIcon = R.drawable.notify_list_icon;
            text1.mMargin = (int) LockScreenActivity.this.getResources().getDimension(R.dimen.common_dimens_20);
            CommonDialogTextType.TextData text2 = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_permission_storage);
            text2.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
            text2.mLeftIcon = R.drawable.notify_list_icon;
            CommonDialogTextType.TextData text3 = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_permission_phone);
            text3.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
            text3.mLeftIcon = R.drawable.notify_list_icon;
            dialogTextType.addTextView(text1);
            dialogTextType.addTextView(text2);
            dialogTextType.addTextView(text3);
            ImageView imageView = new ImageView(dialogTextType.getContext());
            imageView.setImageResource(R.drawable.permission_setting1);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            dialogTextType.addCustomBody(imageView);
            Button[] buttons = dialogTextType.getButtons();
            buttons[0].setBackground(getResources().getDrawable(R.drawable.selector_permission_button_popup));
            buttons[0].setTextColor(getResources().getColor(R.color.common_white));
            dialogTextType.setCancelable(false);
            dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int stringresid) {
                    if (stringresid == 0) {
                        PermissionUtils.showSettingPermission(LockScreenActivity.this, RESULT_PERMISSION_SETTING);
                    }
                }
            });
            dialogTextType.show();
        }
    };

    /**
     * 지문 인식 api 체크 및 생성
     */
    private void initFingerPrint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mFingerprintManager = getSystemService(FingerprintManager.class);
        }
        if (isFingerprintAuthAvailable()) {
            generateKey();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (cipherInit()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mCryptoObject = new FingerprintManager.CryptoObject(mCipher);
                        mFingerPrintHandler = new FingerprintHandler(this);
                    }
                }
            }
        }
    }

    /**
     * 지문 인식이 지원된다면 현재 화면에서 지문인식 기능 활성화 및 지문입력 StandBy
     */
    private void startFingerPrint() {
        if (isFingerprintAuthAvailable()) {
            if (mFingerPrintHandler != null) {
                cancleFingerPrint();
                mFingerPrintHandler.startAuth(mFingerprintManager, mCryptoObject);
            }
        }
    }

    /**
     * 지문인식 시에 단말에 저장된 지문을 읽기 위한 KeyStore생성
     */
    protected void generateKey() {
        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mKeyGenerator = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES,
                    "AndroidKeyStore");
        } catch (NoSuchAlgorithmException |
                NoSuchProviderException e) {
            throw new RuntimeException(
                    "Failed to get KeyGenerator instance", e);
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mKeyStore.load(null);
                mKeyGenerator.init(new
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT |
                                KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(
                                KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
                mKeyGenerator.generateKey();
            }
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean cipherInit() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                mCipher = Cipher.getInstance(
                        KeyProperties.KEY_ALGORITHM_AES + "/"
                                + KeyProperties.BLOCK_MODE_CBC + "/"
                                + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            } catch (NoSuchAlgorithmException |
                    NoSuchPaddingException e) {
                throw new RuntimeException("Failed to get Cipher", e);
            }

            try {
                mKeyStore.load(null);
                SecretKey key = (SecretKey) mKeyStore.getKey(KEY_NAME,
                        null);
                mCipher.init(Cipher.ENCRYPT_MODE, key);
                return true;
            } catch (KeyStoreException | CertificateException
                    | UnrecoverableKeyException | IOException
                    | NoSuchAlgorithmException | InvalidKeyException e) {
                throw new RuntimeException("Failed to init Cipher", e);
            }
        } else {
            return false;
        }
    }

    /**
     * 단말에서 지문인식을 지원하는지 체크
     * @return boolean
     */
    public boolean isFingerprintAuthAvailable() {
        boolean ret = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // The line below prevents the false positive inspection from Android Studio
            // noinspection ResourceType
            ret = mFingerprintManager.isHardwareDetected()
                    && mFingerprintManager.hasEnrolledFingerprints();
        } else {
            ret = false;
        }

        if (!APPSharedPrefer.getInstance(this).getPrefBool(APP.SETTING_BROCCOLI_FINGERPRINT, false)) {
            ret = false;
        }

        return ret;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
        private CancellationSignal cancellationSignal;
        private Context context;
        private int mErrorCount = 0;

        public FingerprintHandler(Context context) {
            this.context = context;
        }

        public void startAuth(FingerprintManager manager,
                              FingerprintManager.CryptoObject cryptoObject) {

            if(cancellationSignal == null || cancellationSignal.isCanceled()){
                cancellationSignal = new CancellationSignal();
            }

            if (ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
                showFingerPrintDialog();
            }
        }

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            if (cancellationSignal != null) {
                if(FingerprintManager.FINGERPRINT_ERROR_CANCELED != errMsgId) {
                    mErrorCount++;
                    if (mErrorCount > 3) {
                        cancleFingerPrint();
                    } else {
                        Toast.makeText(context,
                                errString,
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            Toast.makeText(context,
                    helpString,
                    Toast.LENGTH_LONG).show();
        }

        @Override
        public void onAuthenticationFailed() {
            mErrorCount++;
            if (mErrorCount > 3) {
                cancleFingerPrint();
            } else {
                Toast.makeText(context,
                        context.getResources().getString(R.string.guide_fingerprint_fail),
                        Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            grantAccess();
        }
    }

    private void showFingerPrintDialog() {
        mFingerPrintDialog = new CommonDialogTextType(LockScreenActivity.this, new String[] {"취소"});
        CommonDialogTextType.TextData main = new CommonDialogTextType.TextData(LockScreenActivity.this, R.string.guide_fingerprint_guide);
        main.mTextStyle = Typeface.BOLD;
        main.mLineSpace = getResources().getDimension(R.dimen.common_dimens_4);
        mFingerPrintDialog.addTextView(main);
        ImageView imageView = new ImageView(mFingerPrintDialog.getContext());
        imageView.setImageResource(R.drawable.android_studio_2_fingerprint_icon);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setPadding(0, 0, 0, (int) getResources().getDimension(R.dimen.common_dimens_20));
        mFingerPrintDialog.addCustomBody(imageView);
        Button[] buttons = mFingerPrintDialog.getButtons();
        buttons[0].setBackground(getResources().getDrawable(R.drawable.selector_permission_button_popup));
        buttons[0].setTextColor(getResources().getColor(R.color.common_white));
        mFingerPrintDialog.setCancelable(false);
        mFingerPrintDialog.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int stringresid) {
                if (stringresid == 0) {
                    cancleFingerPrint();
                }
            }
        });
        mFingerPrintDialog.show();
    }

    private void cancleFingerPrint() {
        if (mFingerPrintDialog != null) {
            mFingerPrintDialog.dismiss();
        }
        if (mFingerprintManager != null) {
            if (mFingerPrintHandler.cancellationSignal != null && !mFingerPrintHandler.cancellationSignal.isCanceled()) {
                mFingerPrintHandler.cancellationSignal.cancel();
            }
        }
    }

    private void screenLockReset() {
        Intent intent = new Intent(this, LockScreenUserMatchActivity.class);
        startActivity(intent);
    }
}
