package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.remit.RemitSendActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccount;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 16. 2. 4..
 */
public class Level4ActivitySettingInitialization extends BaseActivity implements View.OnClickListener {

    private Button mMemberLeave;
    private LinearLayout mMemberDelCheck;
    private ImageView mCheckBox;
    private boolean mDelCheckBox = false;

    private ResponseRemitAccount mAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_setting_broccoli_init);
        sendTracker(AnalyticsName.SettingGuideDrop);
        ((RelativeLayout) findViewById(R.id.rl_navi_img_bg)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_member_leave);
        /* Layout */
        mMemberDelCheck = (LinearLayout)findViewById(R.id.ll_level_4_activity_setting_broccoli_init_del);
        mMemberDelCheck.setOnClickListener(this);
        mCheckBox = (ImageView)findViewById(R.id.btn_level_4_activity_setting_broccoli_init_check_box);
        mMemberLeave = (Button)findViewById(R.id.btn_level_4_activity_setting_broccoli_init_ok);
        mMemberLeave.setOnClickListener(this);
        mMemberLeave.setEnabled(false);
        //checkWalletEmpty();
    }

    public void onBtnClickNaviImgTxtBar(View _view) {
        try {
            switch (_view.getId()) {

                case R.id.ll_navi_img_txt_back:
                    finish();
                    break;

                default:
                    break;
            }
        } catch (Exception e) {
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_level_4_activity_setting_broccoli_init_ok:
                member_leave();
                break;
            case R.id.ll_level_4_activity_setting_broccoli_init_del:
                if(mDelCheckBox){
                    mDelCheckBox = false;
                    mMemberLeave.setEnabled(false);
                    mCheckBox.setEnabled(true);
                    mCheckBox.setSelected(false);
                }
                else {
                    mDelCheckBox = true;
                    mMemberLeave.setEnabled(true);
                    mCheckBox.setEnabled(true);
                    mCheckBox.setSelected(true);
                }
                break;
        }
    }

    private void member_leave() {
        sendTracker(AnalyticsName.SettingUserDataDel);
        DialogUtils.showLoading(this);	//로딩 추가
        UserApi api = ServiceGenerator.createService(UserApi.class);
        String token = DbManager.getInstance().getUserDayliToken(this, Session.getInstance(this).getUserId());
        HashMap<String, Object> body = new HashMap<>();
        body.put("dpAccessToken", token);
        Call<Result> deleteUser = api.reqQuit(body);
        deleteUser.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        startActivity(new Intent(Level4ActivitySettingInitialization.this, Level4ActivitySettingInitializationComplete.class));
                        finish();
                    } else {
                        ElseUtils.network_error(getBase());
                    }
                } else {
                    ElseUtils.network_error(getBase());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getBase());
            }
        });
    }

    private void checkWalletData() {
        String temp = mAccount.getWalletMoney();
        BigDecimal wallet = null;
        BigDecimal zero = new BigDecimal(0);
        if (temp != null || TextUtils.isEmpty(temp)) {
            wallet = new BigDecimal(temp);
        }
        if (wallet != null && wallet.compareTo(zero) == 1) {
            RemitAccount account = mAccount.getAccountInfo().get(0);
            if (account == null || account.getAccountNumber() == null || TextUtils.isEmpty(account.getAccountNumber())) {
                linkToCustomerService();
            } else {
                transferWalletMoney();
            }
        } else {
            //membershipExit();
        }
    }

    private void dialog_output_bank_list() {
        final ArrayList<String> outputBankList = new ArrayList<String>();
        if(mAccount != null && mAccount.getAccountInfo() != null){
            for(int i = 0; i < mAccount.getAccountInfo().size(); i++){
                String tempAccountInfo =  TransFormatUtils.transBankCodeToName(mAccount.getAccountInfo().get(i).getCompanyCode())
                        + " " + mAccount.getAccountInfo().get(i).getAccountNumber();
                outputBankList.add(tempAccountInfo);
            }
        }
        String mStr4 = getString(R.string.remit_account_withdraw_select);
        DialogUtils.showDlgBaseRemitList(this, new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        outputBankList.get(position);
                        mAccount.getAccountInfo().get(position);
                        sendBroccoliMoney(mAccount.getAccountInfo().get(position));
                    }
                },
                new InfoDataDialog(APP.DIALOG_BASE_LIST_LARGE, mStr4, getString(R.string.common_cancel)), 0, outputBankList);
    }

    private void transferWalletMoney() {
        dialog_output_bank_list();
    }

    private void linkToCustomerService() {

    }

    private void sendBroccoliMoney(RemitAccount account) {
        Intent intent = new Intent(this, RemitSendActivity.class);
        intent.putExtra(Const.REMIT_MEMBER_EXIT, true);
        intent.putExtra(Const.REMIT_SELECT_ACCOUNT, account);
        intent.putExtra(Const.REMIT_MAIN_DATA, mAccount);
        startActivityForResult(intent, 0);
    }
}
