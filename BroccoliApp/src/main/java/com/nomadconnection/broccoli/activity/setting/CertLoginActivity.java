package com.nomadconnection.broccoli.activity.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lumensoft.ks.KSCertificate;
import com.lumensoft.ks.KSCertificateLoader;
import com.lumensoft.ks.KSException;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.CertPasswordActivity;
import com.nomadconnection.broccoli.adapter.AdtListViewAuthMgrList;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoli.data.Session.User;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoliraonsecure.KSW_CertItem;
import com.nomadconnection.broccoliraonsecure.KSW_CertListManager;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Vector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 16. 9. 20..
 */
public class CertLoginActivity extends BaseActivity implements View.OnClickListener {

    private String mExpire;
    //인증정보 리스트
    private ArrayList<KSW_CertItem> mCertList = null;
    private ArrayList<SelectBankData> mSelectedBankData;
    private Vector<?> userCerts = null;
    private ListView mListView;
    private AdtListViewAuthMgrList mAdapter;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_setting_authmanager);

        mListView = (ListView) findViewById(R.id.lv_CertList);
        TextView title = (TextView) findViewById(R.id.tv_navi_txt_txt_txt_title);
        title.setText(R.string.membership_text_cert_title);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            if (data != null) {
                ElseUtils.setCertCount(this, 0);
                ElseUtils.setCertOverCount(this, 0);
                ElseUtils.setCertOverTime(this, null);
                ElseUtils.setLockDownTime(this, null);
                ElseUtils.setLockOverCount(this, 0);
                Intent intent = new Intent(this, Level3ActivitySettingPassword.class);
                intent.putExtra(Const.RESET_PASSWORD, true);
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestUserRealName();
    }

    private void requestUserRealName() {
        DialogUtils.showLoading(this);
        RequestDataByUserId id = new RequestDataByUserId();
        id.setUserId(Session.getInstance(this).getUserId());
        Call<User> call = ServiceGenerator.createService(UserApi.class).requestRealNameInfo(id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    mUser = response.body();
                    loadCertListInApp(false);
                } else {
                    Toast.makeText(CertLoginActivity.this, getString(R.string.common_warning_server_link_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(CertLoginActivity.this, getString(R.string.common_warning_server_link_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadCertListInApp(boolean loadApp) {
        try {
            // 앱내 인증서 보기 체크 선택/해제 시 리스트 초기
            if (userCerts != null) {
                userCerts.clear();
                if (mCertList != null)
                    mCertList.clear();
            }

            // 인증서 읽기
            if (loadApp)
                // 앱내 저장된 NPKI 인증서 로드
//				userCerts = KSCertificateLoader.getCertificateListfromAppAsVector(KSW_Activity_CertList.this);
                // 앱내 저장된 NPKI / GPKI 인증서 로드
                userCerts = KSCertificateLoader.getCertificateListfromAppWithGpkiAsVector(this);
            else
                // SD카드 내 저장된 NPKI 인증서 로드
//				userCerts = KSCertificateLoader.getCertificateListfromSDCardAsVector(KSW_Activity_CertList.this);
                // SD카드 내 저장된 NPKI / GPKI 인증서 로드
                userCerts = KSCertificateLoader.getCertificateListfromSDCardWithGpkiAsVector(this);


//			KSW_Filter.setIssuerCertFilter(issuerCertFilter);
//			KSW_Filter.setPolicyOidCertFilter(policyOidCertFilter);
//			userCerts = KSCertificateLoader.issuerCertFilter(userCerts, KSW_Filter.getIssuerCertFilter());
//			userCerts = KSCertificateLoader.policyOidCertFilter(userCerts, KSW_Filter.getPolicyOidCertFilter());
            // 발급기관과 정책에 따른 인증서 필터링 끝
        } catch (KSException e) {
            Toast.makeText(getApplicationContext(), "인증서 목록 초기화 실패 인증서 목록 생성에 실패하였습니다.", Toast.LENGTH_LONG).show();
        }

        if (userCerts == null || userCerts.size() == 0) {
            DialogUtils.showDlgBaseOneButtonNoTitle(this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.setting_financial_warning_auth), "확인"));
        } else {
            mCertList = new ArrayList<KSW_CertItem>();
            for (int i = 0; i < userCerts.size(); i++) {
                try {
                    KSW_CertItem item = new KSW_CertItem((KSCertificate) userCerts.elementAt(i));
                    String name = (String) item.get(KSW_CertItem.SUBJECTNAME);
                    if (name.contains(mUser.getUserName())) {
                        item.setContainRealName(true);
                    }
                    mCertList.add(item);
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(this, "인증서 목록 초기화 실패 인증서 목록 생성에 실패하였습니다.", Toast.LENGTH_LONG).show();
                }
            }
            mAdapter = new AdtListViewAuthMgrList(this,
                    mCertList);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    KSCertificate userCert = (KSCertificate) userCerts.elementAt(position);
                    // 선택한 인증서는 KSW_CertListManager.setSelectedCert에 setting
                    KSW_CertListManager.setSelectedCert(userCert);
                    String certNM = userCert.getSubjectName();

                    if (userCert.isExpired()) {
                        Toast.makeText(CertLoginActivity.this, "기간이 만료된 인증서입니다.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (certNM != null) {
                        Intent intent = new Intent(CertLoginActivity.this, CertPasswordActivity.class);
                        intent.putExtra(Const.CERT_NAME, certNM);
                        startActivityForResult(intent, 0);
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        return;
    }

    @Override
    public void onClick(View v) {

    }

}
