package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.asset.HonestFundWebActivity;
import com.nomadconnection.broccoli.activity.asset.Level3ActivityAssetCardLoanDetail;
import com.nomadconnection.broccoli.activity.asset.Level3ActivityAssetEditLoan;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialReg;
import com.nomadconnection.broccoli.adapter.AdtExpListViewAssetLoan;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.LogApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetDebtInfo;
import com.nomadconnection.broccoli.data.Asset.ResponseCardLoanDetail;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.adapter.AdtExpListViewAssetLoan.ITEM_ADVERTISE;


public class Level2ActivityAssetLoan extends BaseActivity {

	private static final String TAG = Level2ActivityAssetLoan.class.getSimpleName();

	private static final int RESULT_EDIT_LOAN = 1;

	/* Layout */
	private ExpandableListView mExpListView;
	private AdtExpListViewAssetLoan mAdapter;
	private ArrayList<String> mGroupList = null;
	private ArrayList<ArrayList<AssetDebtInfo>> mChildList = null;
	private ArrayList<AssetDebtInfo> mAlInfoDataList;
	private ArrayList<AssetDebtInfo> mChildListContent1 = null;
	private ArrayList<AssetDebtInfo> mChildListContent2 = null;
	private boolean isShowHonest = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_asset_loan);
		sendTracker(AnalyticsName.Level2ActivityAssetLoan);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}



	@Override
	public void onResume() {
		super.onResume();
	}
	
	

//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {

			case R.id.ll_navi_img_txt_txt_back:
				finish();
				break;
			case R.id.ll_navi_img_txt_txt_else:
				if (mAlInfoDataList == null || mAlInfoDataList.size() == 0) {
					Intent setting_financial = new Intent(this, Level3ActivitySettingFinancialReg.class);
					startActivity(setting_financial);
				} else {
					Intent editAccount = new Intent(this, Level3ActivityAssetEditLoan.class);
					editAccount.putExtra(Const.DATA, mAlInfoDataList);
					startActivityForResult(editAccount, RESULT_EDIT_LOAN);
				}
				break;

			default:
				break;
			}
		}
		catch(Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());			
		}
	}
	
	
	
	
	
	
	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			mAlInfoDataList = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_ASSET_LOAN_DEPTH_1);
			mGroupList = new ArrayList<String>();
			mChildList = new ArrayList<ArrayList<AssetDebtInfo>>();
			mChildListContent1 = new ArrayList<>();
			mChildListContent2 = new ArrayList<>();

			isShowHonest = Session.getInstance(this).getAppInfo().honest_fund.display;

			if(mAlInfoDataList == null){
				bResult = false;
			}

		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_img_txt_txt_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitle);
			if (mGroupList != null && mGroupList.size() > 0) {
				((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_edit));
			} else {
				((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_add));
			}
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) {
				((RelativeLayout)findViewById(R.id.rl_navi_img_txt_txt_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			}

			/* Layout */
			mExpListView = (ExpandableListView) findViewById(R.id.elv_fragment_asset_loan_1_explistview);
			mAdapter = new AdtExpListViewAssetLoan(this, mGroupList, mChildList);
			mExpListView.setAdapter(mAdapter);

	        /* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return true;
				}
			});

			/* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return true;
				}
			});

			/* Child list click 시 */
			mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
											int groupPosition, int childPosition, long id) {
					AssetDebtInfo debtInfo = null;
					debtInfo = mChildList.get(groupPosition).get(childPosition);
					if (debtInfo == null) return true;

					if (debtInfo.mAdvertise == ITEM_ADVERTISE) {
						showHonestFund();
					} else {
						if (debtInfo.mLoanType == AssetDebtInfo.LOAN_CARD) {
							NextStep(debtInfo);
						}
					}
					return false;
				}
			});
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	/**
	 * initial method
	 * <p> show child list view</p>
	 * @return
     */
	private boolean initData() {
		boolean bResult = true;
		try {
			resetData();
			if (mAlInfoDataList != null){
				if (mAlInfoDataList.size() == 0) {
					((FrameLayout)findViewById(R.id.inc_fragment_asset_loan_1_empty)).setVisibility(View.VISIBLE);
					((ImageView)findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
					((TextView)findViewById(R.id.tv_empty_layout_txt)).setText(getString(R.string.common_empty_loan));
				} else {
					if ((mAlInfoDataList != null && mAlInfoDataList.size() > 0) && mGroupList != null && mGroupList.size() == 0) {
						((FrameLayout)findViewById(R.id.inc_fragment_asset_loan_1_empty)).setVisibility(View.VISIBLE);
						((ImageView)findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.bank_account_icon);
						((TextView)findViewById(R.id.tv_empty_layout_txt)).setText(getString(R.string.common_empty_edit_account));
						((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_edit));
						TextView textView = (TextView)findViewById(R.id.tv_empty_layout_txt_second);
						textView.setVisibility(View.VISIBLE);
						textView.setText(R.string.common_empty_edit_account_second);
					} else {
						((FrameLayout)findViewById(R.id.inc_fragment_asset_loan_1_empty)).setVisibility(View.GONE);
					}
				}
			}
			for (int i = 0; i <mGroupList.size() ; i++) {
				mExpListView.expandGroup(i);
			}
		}
		catch(Exception e) {
			bResult = false;
			e.printStackTrace();
			return bResult;
		}

		return bResult;
	}

//=========================================================================================//
// Refresh
//=========================================================================================//
	private void resetData(){
		mGroupList.clear();
		mChildList.clear();
		mChildListContent1.clear();
		mChildListContent2.clear();

		for (int i = 0; i < mAlInfoDataList.size(); i++) {
			if (i == 0) {
				AssetDebtInfo info = mAlInfoDataList.get(i);
				if (info.mLoanType == AssetDebtInfo.LOAN_CARD || info.isChecked()) {
					if (info.mLoanType == AssetDebtInfo.LOAN) {
						if (!mGroupList.contains("일반대출"))
							mGroupList.add("일반대출");
					} else {
						if (!mGroupList.contains("장기카드대출"))
							mGroupList.add("장기카드대출");
					}
				}
			} else {
				AssetDebtInfo info = mAlInfoDataList.get(i);
				if (info.mLoanType == AssetDebtInfo.LOAN) {
					if (!mGroupList.contains("일반대출"))
						mGroupList.add("일반대출");
				} else {
					if (!mGroupList.contains("장기카드대출"))
						mGroupList.add("장기카드대출");
				}
			}
		}

		for (int j = 0; j < mAlInfoDataList.size(); j++) {
			if (mAlInfoDataList.get(j).mLoanType == AssetDebtInfo.LOAN){
				if (mAlInfoDataList.get(j).isChecked()) {
					mChildListContent1.add(mAlInfoDataList.get(j));
				}
			} else {
				mChildListContent2.add(mAlInfoDataList.get(j));
			}
		}
		if (mChildListContent1.size() > 0) {
			mChildList.add(mChildListContent1);
		}
		if (mChildListContent2.size() > 0) {
			// 장기카드대출 있으면 honestFund 광고
			if (isShowHonest) {
				AssetDebtInfo info = new AssetDebtInfo();
				info.mAdvertise = ITEM_ADVERTISE;
				mChildListContent2.add(info);
			}
			mChildList.add(mChildListContent2);
		}
		mAdapter.notifyDataSetChanged();

		if (mGroupList != null && mGroupList.size() > 0) {
			((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_edit));
		} else {
			((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_add));
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RESULT_EDIT_LOAN) {
			if (resultCode == RESULT_OK) {
				mAlInfoDataList = data.getParcelableArrayListExtra(Const.DATA);
				initData();
				mAdapter.notifyDataSetChanged();
			}
		}
	}

	//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep **/
	private void NextStep(AssetDebtInfo assetBankInfo){
		DialogUtils.showLoading(this);
		AssetApi api = ServiceGenerator.createService(AssetApi.class);
		HashMap<String, Object> hashMap = new HashMap<>();
		hashMap.put("userId", Session.getInstance(this).getUserId());
		hashMap.put("companyCode", assetBankInfo.mAssetDebtCode);
		Call<ArrayList<ResponseCardLoanDetail>> call = api.getCardLoanDetail(hashMap);
		call.enqueue(new Callback<ArrayList<ResponseCardLoanDetail>>() {
			@Override
			public void onResponse(Call<ArrayList<ResponseCardLoanDetail>> call, Response<ArrayList<ResponseCardLoanDetail>> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					ArrayList<ResponseCardLoanDetail> cardLoanDetails = response.body();
					if (cardLoanDetails.size() > 0) {
						startActivity(new Intent(Level2ActivityAssetLoan.this, Level3ActivityAssetCardLoanDetail.class)
								.putExtra(Const.DATA, cardLoanDetails));
					}
				} else {
					ElseUtils.network_error(getBase());
				}
			}

			@Override
			public void onFailure(Call<ArrayList<ResponseCardLoanDetail>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getBase());
			}
		});
	}

	private void showHonestFund() {
		sendTracker(AnalyticsName.AssetLoanListLoanBanner);
		HashMap<String, Object> hashMap = new HashMap<>();
		hashMap.put("userId", Session.getInstance(this).getUserId());
		ServiceGenerator.createService(LogApi.class).sendLogHonestFund(hashMap).enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {

			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {

			}
		});
		Intent intent = new Intent(this, HonestFundWebActivity.class);
		intent.putExtra(Const.DATA, Session.getInstance(this).getAppInfo().honest_fund.url_to);
		startActivity(intent);
	}
}
