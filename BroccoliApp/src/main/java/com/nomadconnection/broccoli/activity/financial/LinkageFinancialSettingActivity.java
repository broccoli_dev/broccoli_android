package com.nomadconnection.broccoli.activity.financial;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lumensoft.ks.KSCertificate;
import com.lumensoft.ks.KSCertificateLoader;
import com.lumensoft.ks.KSException;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.AdtExpListViewFinancialSetting;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoliraonsecure.KSW_CertItem;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import static com.nomadconnection.broccoli.activity.financial.LinkageFinancialSettingActivity.LinkageData.KEY_ID_PWD;
import static com.nomadconnection.broccoliraonsecure.KSW_CertItem.PATH;

/**
 * Created by YelloHyunminJang on 2017. 5. 24..
 */

public class LinkageFinancialSettingActivity extends BaseActivity {

    private static final int REPLACE_CERTIFICATE = 1;

    private ExpandableListView mExpListView;
    private AdtExpListViewFinancialSetting mAdapter;
    private FrameLayout mEmptyLayout;

    private Vector<?> userCerts = null;
    private ArrayList<LinkageData> mGroupList = new ArrayList<>();
    private HashMap<String, ArrayList<BankData>> mChildMap = new HashMap();

    public static class LinkageData {

        public static final String KEY_ID_PWD = "KEY_ID_PWD";

        public enum LinkType {
            CERTIFICATE,
            ID_PASS,
            CERTIFICATE_LOSS
        }

        public LinkType type;
        public String key;
        public KSW_CertItem item;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_financial_linkage_setting);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.CertManager);
        initWidget();
        initData();
    }

    private void initWidget() {
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        TextView title = (TextView) findViewById(R.id.tv_navi_img_txt_title);
        title.setText(R.string.setting_financial_reg_link_setting_title);
        mExpListView = (ExpandableListView) findViewById(R.id.elv_financial_linkage_explistview);
        mEmptyLayout = (FrameLayout) findViewById(R.id.fl_empty_layout);
        mAdapter = new AdtExpListViewFinancialSetting(this, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KSW_CertItem item = (KSW_CertItem) view.getTag();
                Intent intent = new Intent(getActivity(), LinkageFinancialSettingDetailActivity.class);
                intent.putExtra(Const.DATA, item);
                startActivityForResult(intent, REPLACE_CERTIFICATE);
            }
        });
        mExpListView.setAdapter(mAdapter);
        mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return true;
            }
        });
			/* Child list click 시 */
        mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                return false;
            }
        });
    }

    private void initData() {
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                checkData(str);
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REPLACE_CERTIFICATE) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    private void checkData(String key) {
        mGroupList.clear();
        mChildMap.clear();
        try {
            userCerts = KSCertificateLoader.getCertificateListfromSDCardWithGpkiAsVector(this);
        } catch (KSException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < userCerts.size(); i++) {
            try {
                LinkageData data = new LinkageData();
                data.type = LinkageData.LinkType.CERTIFICATE;
                data.item = new KSW_CertItem((KSCertificate) userCerts.elementAt(i));
                data.key = (String) data.item.get(PATH);
                mGroupList.add(data);
                mChildMap.put(data.key, new ArrayList<BankData>());
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(this, "인증서 목록 초기화 실패 인증서 목록 생성에 실패하였습니다.", Toast.LENGTH_LONG).show();
            }
        }
        List<BankData> bankDataList = ScrapDao.getInstance().getBankData(this, key, Session.getInstance(getApplicationContext()).getUserId());
        ArrayList<BankData> idPassWordList = new ArrayList<>();
        for (BankData bankData : bankDataList) {
            boolean isInclude = false;
            if (BankData.LOGIN_CERT.equalsIgnoreCase(bankData.getLoginMethod())) {
                for (int i=0; i < mGroupList.size(); i++) {
                    LinkageData data = mGroupList.get(i);
                    String path = data.key;
                    if (bankData != null) {
                        String serial = bankData.getCertSerial();
                        if (path.equals(serial)) {
                            if (data.type == LinkageData.LinkType.CERTIFICATE) {
                                isInclude = true;
                                ArrayList<BankData> childList = mChildMap.get(path);
                                if (childList == null) {
                                    childList = new ArrayList<>();
                                    mChildMap.put(path, childList);
                                }
                                childList.add(bankData);
                            } else if (data.type == LinkageData.LinkType.CERTIFICATE_LOSS) {
                                isInclude = true;
                                ArrayList<BankData> childList = mChildMap.get(path);
                                if (childList == null) {
                                    childList = new ArrayList<>();
                                    mChildMap.put(path, childList);
                                }
                                childList.add(bankData);
                            }
                        }
                    }
                }
            } else {
                isInclude = true;
                idPassWordList.add(bankData);
            }
            if (!isInclude) {
                String path = bankData.getCertSerial();
                KSW_CertItem item = new KSW_CertItem();
                item.put(PATH, path);
                try {
                    KSCertificate certificate = new KSCertificate(path.getBytes(), path);
                    item.setCertificate(certificate);
                } catch (KSException e) {
                    e.printStackTrace();
                }
                ArrayList<BankData> excludeBankList = mChildMap.get(path);
                if (excludeBankList == null) {
                    LinkageData data = new LinkageData();
                    data.type = LinkageData.LinkType.CERTIFICATE_LOSS;
                    data.item = item;
                    data.key = path;
                    mGroupList.add(data);
                    excludeBankList = new ArrayList<>();
                    mChildMap.put(path, excludeBankList);
                }
                excludeBankList.add(bankData);
            }
        }
        if (idPassWordList.size() > 0) {
            LinkageData data = new LinkageData();
            data.type = LinkageData.LinkType.ID_PASS;
            data.item = null;
            data.key = KEY_ID_PWD;
            mGroupList.add(data);
            mChildMap.put(KEY_ID_PWD, idPassWordList);
        }
        setData();
    }

    private void setData() {
        mAdapter.setData(mGroupList, mChildMap);
        mAdapter.notifyDataSetChanged();

        if (mGroupList.size() == 0) {
            mEmptyLayout.setVisibility(View.VISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.GONE);
        }

        for (int i = 0; i < mGroupList.size() ; i++) {
            mExpListView.expandGroup(i);
        }
    }
}
