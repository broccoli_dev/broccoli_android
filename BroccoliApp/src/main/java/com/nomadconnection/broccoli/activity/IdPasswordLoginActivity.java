package com.nomadconnection.broccoli.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.igaworks.adbrix.IgawAdbrix;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogCustomType;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.CardLoginLimitInfo;
import com.nomadconnection.broccoli.data.FinancialLogin;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.service.BatchService;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.service.ScrapingModule;
import com.nomadconnection.broccoli.service.data.ScrapRequester;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoliespider.data.BankRequestData;
import com.nomadconnection.broccoliraonsecure.TransKeyManager;
import com.softsecurity.transkey.Global;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.service.mode.ProcessMode.COLLECT_FIRST_THIS_MONTH;

/**
 * Created by YelloHyunminJang on 16. 1. 25..
 */
public class IdPasswordLoginActivity extends BaseActivity implements View.OnClickListener, ITransKeyActionListenerEx {

    private static final int PASSWORD_MINIMUM_LENGTH = 6;
    private static final int PASSWORD_ERROR_MAX_COUNT = 5;

    private static final int RESULT_PASSWORD_ERROR = 0;
    private static final int RESULT_PASSWORD_CORRECT = 1;
    private static final int RESULT_PASSWORD_MULTIPLE = 2;

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private ProcessMode mMode = ProcessMode.COLLECT_IDLE;
    private boolean isConnection = false;
    private Messenger			mService;
    private ServiceConnection 	mServiceConnection;
    private boolean isScraping = false;
    private TransKeyCtrl mTransKey;
    private boolean isTransKeyShowing = false;

    private View mSuccessLayout;
    private TextView mSuccessTextView;
    private LinearLayout mInputLayout;
    private View mBtn;
    private View mSkipBtn;
    private ArrayList<SelectBankData> mSelectedBankData = new ArrayList<>();
    private ArrayList<SelectBankData> mRequestLoginData = new ArrayList<>();
    private ArrayList<SelectBankData> mSuccessBankData = new ArrayList<>();

    private List<CardLoginLimitInfo> mLimitList;
    private int mDefaultCount = 5;
    private String mCertNm = null;
    private byte[] mSecureKey;
    private byte[] mCipher;
    private boolean fromMembership;
    private ArrayList<FinancialLogin> mResultList = new ArrayList<>();
    private Handler mTimeOutHandler = new Handler();
    private Handler mUiHandler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg != null) {
                DialogUtils.dismissLoading();
                int what = msg.what;
                if (what == RESULT_PASSWORD_CORRECT) {
                    final FinancialLogin login = (FinancialLogin) msg.obj;
                    mUiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (!isFinishing()) {
                                showPopup(login.getRequestData().getOriginalBankName()+" 등록에 성공하였습니다.", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mUiHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                setLoginComplete(mRequestLoginData);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                } else if (what == RESULT_PASSWORD_ERROR) {
                    final String str = (String) msg.obj;
                    mUiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            showPopup(str, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    setResult(RESULT_CANCELED);
                                }
                            });
                        }
                    });
                } else if (what == RESULT_PASSWORD_MULTIPLE) {
                    showMultiPopup(mResultList);
                }
            }
        }

    };

    private Runnable mTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            DialogUtils.dismissLoading();
            Toast.makeText(getBase(), "현재 로그인 시간이 길어지고 있습니다. 잠시 후 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_id_password_login);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.IdPwLogin);
        initBinderService();
        initWidget();
        initData();
    }

    private void initWidget() {
        findViewById(R.id.rl_navi_top_layout).setBackgroundColor(Color.BLACK);
        TextView title = (TextView) findViewById(R.id.tv_navi_txt_txt_txt_title);
        title.setTextColor(Color.WHITE);
        title.setText(R.string.membership_text_login_title);

        findViewById(R.id.ll_navi_txt_txt_txt_back).setOnClickListener(this);
        findViewById(R.id.ll_navi_txt_txt_txt_else).setVisibility(View.GONE);
        mSuccessLayout = findViewById(R.id.ll_id_pwd_success_layout);
        mSuccessTextView = (TextView) findViewById(R.id.tv_id_pwd_success_layout);
        mSuccessLayout.setVisibility(View.GONE);
        mInputLayout = (LinearLayout) findViewById(R.id.ll_id_pwd_input_layout);

        mBtn = findViewById(R.id.btn_next);
        mBtn.setEnabled(false);
        mBtn.setOnClickListener(this);
        mSkipBtn = findViewById(R.id.btn_skip);
        mSkipBtn.setEnabled(true);
        mSkipBtn.setOnClickListener(this);
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            if(intent.getExtras() != null) {
                mSelectedBankData = (ArrayList<SelectBankData>) intent.getSerializableExtra(Const.DATA);
                fromMembership = intent.getBooleanExtra(Const.FROM_MEMBERSHIP, false);
            }
        }

        if (fromMembership) {
            mSkipBtn.setVisibility(View.VISIBLE);
        } else {
            mSkipBtn.setVisibility(View.GONE);
        }

        mRequestLoginData.addAll(mSelectedBankData);

        UserApi api = ServiceGenerator.createService(UserApi.class);
        Call<JsonElement> call = api.getCardLoginLimit();
        DialogUtils.showLoading(this);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        JsonElement element = response.body();
                        JsonObject object = element.getAsJsonObject();
                        JsonArray array = object.getAsJsonArray("info");
                        Type listType = new TypeToken<List<CardLoginLimitInfo>>() {}.getType();
                        mLimitList = new Gson().fromJson(array, listType);
                        showContent();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (mTransKey != null && isTransKeyShowing){
            DialogUtils.showLoading(this);
            mTransKey.finishTransKey(true);
            DialogUtils.dismissLoading();
            isTransKeyShowing = false;
            return;
        }
        if (mSuccessBankData.size() > 0) {
            CommonDialogTextType textType = new CommonDialogTextType(this, new int[] {R.string.common_cancel, R.string.common_confirm});
            textType.addTextView(R.string.membership_text_login_cancel);
            textType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    switch (id) {
                        case R.string.common_confirm:
                            setResult(RESULT_CANCELED);
                            finish();
                            break;
                    }
                }
            });
            textType.show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finishBinderService();
    }

    private void showContent() {
        if (mSuccessBankData.size() > 0) {
            String text = new String();
            for (int i=0; i < mSuccessBankData.size(); i++) {
                SelectBankData data = mSuccessBankData.get(i);
                if (i < 3) {
                    text += (i == 0 ? "":" / ")+data.getName();
                }
                if (i == mSuccessBankData.size()-1) {
                    if (mSuccessBankData.size() <= 3) {
                        text += " 등록 완료";
                    } else {
                        text += "외 "+String.valueOf(i-2)+"개";
                    }
                }
            }
            mSuccessLayout.setVisibility(View.VISIBLE);
            mSuccessTextView.setText(text);
        } else {
            mSuccessLayout.setVisibility(View.GONE);
        }
        LayoutInflater inflater = getLayoutInflater();
        mInputLayout.removeAllViews();
        if (mRequestLoginData != null) {
            for (int i=0; i < mRequestLoginData.size(); i++) {
                final ViewHolder holder = new ViewHolder();
                holder.data = mRequestLoginData.get(i);
                View view = inflater.inflate(R.layout.custom_content_financial_id_login, null);
                holder.mRaonView = view.findViewById(R.id.et_login_password);
                holder.mTitle = (TextView) view.findViewById(R.id.tv_cert_bank_name);
                holder.mTitle.setText(holder.data.getName());
                holder.mIdEditText = (EditText) view.findViewById(R.id.et_login_id);
                holder.mIdEditText.setPrivateImeOptions("defaultInputmode=english;");
                holder.mIdEditText.setOnClickListener(this);
                if (i == 0) {
                    holder.mIdEditText.requestFocus();
                }
                holder.mIdEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (hasFocus) {
                            onHideKeyboard();
                            onShowSystemKeyboard((EditText) view);
                        }
                    }
                });
                holder.mResultListener = new ITransKeyActionListener() {

                    @Override
                    public void done(Intent data) {
                        isTransKeyShowing = false;
                        String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
                        byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
                        int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

                        if (iRealDataLength == 0)
                            return;

                        String cipherStr = null;
                        try {
                            mTkc.setSecureKey(secureKey);
                            mSecureKey = secureKey;
                            APPSharedPrefer.getInstance(getActivity()).setPrefString(APP.PUBLIC_KEY, toHexString(secureKey));
                            cipherStr = mTkc.getPBKDF2DataEncryptCipherData(cipherData);
                        } catch (Exception e) {
                            if(Global.debug) Log.d("STACKTRACE", e.getStackTrace().toString());
                        }

                        holder.data.setId(holder.mIdEditText.getText().toString());
                        holder.data.setPwd(cipherStr);
                    }

                    @Override
                    public void cancel(Intent intent) {
                        isTransKeyShowing = false;
                        if (mTransKey != null)
                            mTransKey.ClearAllData();
                    }
                };
                String hint = getString(R.string.membership_text_login_pwd_hint);
                for (CardLoginLimitInfo info : mLimitList) {
                    if (holder.data.getCode().equalsIgnoreCase(info.getCardCode())) {
                        hint = info.getMessage();
                    }
                }
                holder.mKeyCtrl = TransKeyManager.getInstance(this).initTransKeyPad(
                        this,
                        0,
                        TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER,
                        TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_LAST_IMAGE,
                        "텍스트입력",
                        hint,
                        100,
                        "최대 글자 100자를 입력하셨습니다.",
                        5,
                        true,
                        (FrameLayout)findViewById(R.id.keypadContainer),
                        (EditText) view.findViewById(R.id.editText),
                        (HorizontalScrollView) view.findViewById(R.id.keyscroll),
                        (LinearLayout) view.findViewById(R.id.keylayout),
                        (ImageButton) view.findViewById(R.id.clearall),
                        (RelativeLayout)findViewById(R.id.keypadBallon),
                        null, false, holder.mResultListener, this);
                holder.mIdEditText.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        int action = event.getAction();
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                            case KeyEvent.KEYCODE_NAVIGATE_NEXT:
                                if (action == KeyEvent.ACTION_UP) {
                                    onHideSystemKeyboard(holder.mIdEditText);
                                    if (!isTransKeyShowing) {
                                        onShowKeyboard(holder.mKeyCtrl, holder.mIdEditText, holder.mEditText, TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                                    } else {
                                        onHideKeyboard();
                                        onShowKeyboard(holder.mKeyCtrl, holder.mIdEditText, holder.mEditText, TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                                    }
                                }
                                break;
                            case KeyEvent.KEYCODE_DEL:
                            case KeyEvent.KEYCODE_FORWARD_DEL:
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });

                holder.mClickLayout = view.findViewById(R.id.ll_edittext_click_layout);
                holder.mEditText = (EditText) holder.mRaonView.findViewById(R.id.editText);
                holder.mEditText.setInputType(0);
                holder.mEditText.setHint(hint);
                holder.mEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean focused) {
                        if (focused) {
                            if (!isTransKeyShowing) {
                                onShowKeyboard(holder.mKeyCtrl, holder.mIdEditText, holder.mEditText, TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                            } else {
                                onHideKeyboard();
                                onShowKeyboard(holder.mKeyCtrl, holder.mIdEditText, holder.mEditText, TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                            }
                        }
                    }
                });
                holder.mClickLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.mEditText.requestFocus();
                    }
                });
                holder.mCopyBtn = view.findViewById(R.id.tv_row_item_financial_copy_id);
                holder.mErrorMessage = (TextView) view.findViewById(R.id.tv_login_error_message);
                if (holder.data.getBankStatus() != null && !TextUtils.isEmpty(holder.data.getBankStatus())) {
                    holder.mErrorMessage.setText(holder.data.getBankStatus());
                }
                if (holder.data.getId() != null) {
                    holder.mIdEditText.setText(holder.data.getId());
                }
                if (i == 0 && mRequestLoginData.size() > 1) {
                    holder.mCopyBtn.setVisibility(View.VISIBLE);
                    holder.mCopyBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String id = holder.mIdEditText.getText().toString();
                            replaceId(id);
                        }
                    });
                    holder.mIdEditText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (editable.length() > 1) {
                                holder.mCopyBtn.setEnabled(true);
                            } else {
                                holder.mCopyBtn.setEnabled(false);
                            }
                        }
                    });
                }
                view.setTag(holder);
                mInputLayout.addView(view);
            }
        }
    }

    private class ViewHolder {
        public SelectBankData data;
        public View mCopyBtn;
        public View mRaonView;
        public TextView mTitle;
        public TextView mErrorMessage;
        public EditText mIdEditText;
        public EditText mEditText;
        public View mClickLayout;
        public TransKeyCtrl mKeyCtrl;
        public ITransKeyActionListener mResultListener;
    }

    private void replaceId(String id) {
        if (mInputLayout != null) {
            for (int i = 0; i < mInputLayout.getChildCount(); i++) {
                View view = mInputLayout.getChildAt(i);
                final ViewHolder holder = (ViewHolder) view.getTag();
                holder.mIdEditText.setText(id);
            }
        }
    }

    private void onShowKeyboard(final TransKeyCtrl keyCtrl, EditText view, EditText pwdEditText, int keyPadType) {
        onHideSystemKeyboard(view);
        isTransKeyShowing = true;
        for (CardLoginLimitInfo info : mLimitList) {
            info.getMessage();
        }
        mTransKey = keyCtrl;
        mTransKey.showKeypad(keyPadType);
    }

    private void onHideKeyboard() {
        if (mTransKey != null) {
            isTransKeyShowing = false;
            mTransKey.finishTransKey(true);
        }
    }

    private void onShowSystemKeyboard(EditText text) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInputFromInputMethod(text.getWindowToken(), 0);
    }

    private void onHideSystemKeyboard(EditText text) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(text.getWindowToken(), 0);
    }

    private void enterMain() {
        if (mSuccessBankData != null && mSuccessBankData.size() > 0) {
            setLoginComplete(mSuccessBankData);
        } else {
            ActivityCompat.finishAffinity(IdPasswordLoginActivity.this);
            startLoading();
        }
    }

    private void processCert(List<SelectBankData> selectBankDatas) {
        if (isScraping) {
            Toast.makeText(this, "연동하신 금융기관 정보를 조회중입니다.\n완료 후 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();
        } else {
            isTransKeyShowing = false;
            for (int i=0; i < mInputLayout.getChildCount(); i++) {
                View view = mInputLayout.getChildAt(i);
                ViewHolder holder = (ViewHolder) view.getTag();
                holder.mKeyCtrl.done();
                holder.mKeyCtrl.finishTransKey(true);
            }
            mTimeOutHandler.postDelayed(mTimeoutRunnable, 60*1000);
            DialogUtils.showLoading(this);
            final ArrayList<BankData> bankList = new ArrayList<>();
            for (SelectBankData bankData : selectBankDatas) {
                if (bankData != null) {
                    BankData dataSet = new BankData();
                    dataSet.setPassWord(bankData.getPwd());
                    dataSet.setBankName(bankData.getName());
                    dataSet.setBankCode(bankData.getCode());
                    dataSet.setCertSerial(bankData.getId());
                    dataSet.setType(bankData.getType());
                    dataSet.setStatus(BankData.SCRAPING);
                    dataSet.setLoginMethod(BankData.LOGIN_ID);
                    bankList.add(dataSet);
                }
            }
            sendUploadMessage(bankList);
        }
    }

    private void sendUploadMessage(ArrayList<BankData> list) {
        Intent intent = new Intent(IntentDef.INTENT_ACTION_LOGIN);
        intent.putExtra(Const.DATA, list);
        sendBroadcast(intent);
    }

    private void showPopup(final String str, final DialogInterface.OnClickListener dialog) {
        DialogUtils.showDlgBaseOneButtonNoTitle(this, dialog, new InfoDataDialog(R.layout.dialog_inc_case_1, str, "확인"));
    }

    private void showMultiPopup(List<FinancialLogin> list) {
        ArrayList<FinancialLogin> failList = new ArrayList<>();
        boolean result = true;
        for (FinancialLogin login : list) {
            if (login.isSuccess()) {
                for (SelectBankData data : mRequestLoginData) {
                    BankRequestData bankRequestData = login.getRequestData();
                    if (bankRequestData.getOriginalBankCode().equalsIgnoreCase(data.getCode())
                            && bankRequestData.getOriginalBankType().equalsIgnoreCase(data.getType())) {
                        mSuccessBankData.add(data);
                    }
                }
            } else {
                result = false;
                for (SelectBankData data : mRequestLoginData) {
                    BankRequestData bankRequestData = login.getRequestData();
                    if (bankRequestData.getOriginalBankCode().equalsIgnoreCase(data.getCode())
                            && bankRequestData.getOriginalBankType().equalsIgnoreCase(data.getType())) {
                        String subText = null;
                        int error = getErrorMsg(login.getErrorCode(), login.getUserError(), login.getErrorMsg(), false);
                        switch (error) {
                            case 2300:
                            case 2301:
                            case 2302:
                                subText = "비밀번호가 일치하지 않습니다.";
                                // cert p/w error
                                break;
                            case 2001:
                            case 2200:
                                // network error
                                subText = "네트워크 오류가 발생하였습니다. 잠시 후 재시도해주세요.";
                                break;
                            case 2003:
                            case 2701:
                            case 2702:
                            case 2710:
                                // site renew
                                subText = "금융기관 사이트가 점검 또는 개편 중입니다.";
                                break;
                            case 2801: // password error
                                subText = "비밀번호가 일치하지 않습니다.";
//                                if (login.getUserError() != null && !TextUtils.isEmpty(login.getUserError())) {
//                                    int errorCount = Integer.valueOf(login.getUserError());
//                                }
                                break;
                            case 2802: // password error count over
                                subText = "로그인 입력횟수를 초과하였습니다.";
                                break;
                            case 2803: // id or password error
                                subText = "아이디 또는 비밀번호가 일치하지 않습니다.";
                                break;
                            case 2800:
                            case 20000:
                                subText = "등록되지 않은 아이디입니다.";
                                break;
                            case 3002:
                                if ("PASS_DIGITS".equalsIgnoreCase(login.getUserError())) {
                                    subText = "로그인에 실패하였습니다 해당 금융기관 서비스에서 비밀번호를 확인해주세요";
                                }
                                break;
                            default:
                                subText = "에러가 발생하였습니다.";
                                break;
                        }
                        data.setBankStatus(subText);
                    }
                }
                failList.add(login);
            }
        }
        mResultList.clear();
        if (mSuccessBankData.size() > 0) {
            for (SelectBankData data : mSuccessBankData) {
                mRequestLoginData.remove(data);
            }
        }
        String title = null;
        String content = null;
        if (result) {
            setLoginComplete(mSuccessBankData);
        } else {
            for (int i=0; i < mInputLayout.getChildCount(); i++) {
                View view = mInputLayout.getChildAt(i);
                ViewHolder holder = (ViewHolder) view.getTag();
                if (holder != null) {
                    holder.mKeyCtrl.ClearAllData();
                    holder.mKeyCtrl.clearKeypad();
                    holder.mEditText.setText(null);
                }
            }
            checkBtnStatus();
            showContent();
            if (fromMembership) {
                title = getResources().getString(R.string.membership_text_login_fail);
                content = getResources().getString(R.string.membership_text_login_fail_content);
                CommonDialogCustomType type = new CommonDialogCustomType(this, title, new int[] {R.string.common_skip, R.string.common_retry});
                TextView titleView = new TextView(this);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;
                titleView.setLayoutParams(params);
                titleView.setText(title);
                titleView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                titleView.setCompoundDrawablePadding((int) ElseUtils.getDiptoPx(5));
                titleView.setGravity(Gravity.CENTER);
                type.addCustomTitle(titleView);
                View body = this.getLayoutInflater().inflate(R.layout.custom_dialog_financial, null);
                TextView bodyContent = (TextView) body.findViewById(R.id.tv_custom_dialog_content);
                bodyContent.setText(content);
                ListView listView = (ListView) body.findViewById(R.id.lv_custom_dialog_list_result);
                listView.setAdapter(new ResultListAdapter(this, failList));
                type.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int idOrWhich) {
                        switch (idOrWhich) {
                            case R.string.common_skip:
                                setLoginComplete(mSuccessBankData);
                                break;
                            case R.string.common_yes:
                            case R.string.common_retry:
                                break;
                            case R.string.common_no:
                                cancelIdPassword();
                                break;
                        }
                        dialog.dismiss();
                    }
                });
                type.addCustomBody(body);
                type.show();
            }
        }
    }

    private class ResultListAdapter extends BaseAdapter {

        private List<FinancialLogin> mDataSet;
        private Context mContext;

        private ResultListAdapter(Context context, List<FinancialLogin> data) {
            mDataSet = data;
            mContext = context;
        }

        @Override
        public int getCount() {
            int count = 0;
            if (mDataSet != null) {
                count = mDataSet.size();
            }
            return count;
        }

        @Override
        public Object getItem(int position) {
            Object obj = null;
            if (mDataSet != null) {
                obj = mDataSet.get(position);
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout view = (LinearLayout) convertView;
            ViewHolder viewHolder = null;
            if (view == null) {
                view = new LinearLayout(mContext);
                view.setOrientation(LinearLayout.HORIZONTAL);
                AbsListView.LayoutParams params = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                view.setLayoutParams(params);
                viewHolder = new ViewHolder();
                viewHolder.mName  = new TextView(mContext);
                LinearLayout.LayoutParams nameParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                viewHolder.mName.setLayoutParams(nameParams);
                viewHolder.mName.setPadding(0, 5, 10, 5);
                viewHolder.mName.setGravity(Gravity.CENTER);
                viewHolder.mName.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
                viewHolder.mName.setTypeface(Typeface.SANS_SERIF);
                view.addView(viewHolder.mName);

                viewHolder.mSub  = new TextView(mContext);
                LinearLayout.LayoutParams subParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                subParams.leftMargin = 15;
                viewHolder.mSub.setLayoutParams(subParams);
                viewHolder.mSub.setPadding(0, 5, 0, 5);
                viewHolder.mSub.setGravity(Gravity.CENTER);
                viewHolder.mSub.setTextColor(getResources().getColor(R.color.red));
                viewHolder.mSub.setTypeface(Typeface.SANS_SERIF);
                view.addView(viewHolder.mSub);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            FinancialLogin login = mDataSet.get(position);
            viewHolder.mName.setText(" • "+login.getRequestData().getOriginalBankName());
            if (!login.isSuccess()) {
                String subText = null;
                int error = getErrorMsg(login.getErrorCode(), login.getUserError(), login.getErrorMsg(), false);
                switch (error) {
                    case 2300:
                    case 2301:
                    case 2302:
                        subText = "비밀번호가 일치하지 않습니다.";
                        // cert p/w error
                        break;
                    case 2001:
                    case 2200:
                        // network error
                        subText = "네트워크 오류가 발생하였습니다. 잠시 후 재시도해주세요.";
                        break;
                    case 2003:
                    case 2701:
                    case 2702:
                    case 2710:
                        // site renew
                        subText = "금융기관 사이트가 점검 또는 개편 중입니다.";
                        break;
                    case 2801: // password error
                        subText = "비밀번호가 일치하지 않습니다.";
                        break;
                    case 2802: // password error count over
                        subText = "로그인 입력횟수를 초과하였습니다.";
                        break;
                    case 2803: // id or password error
                        subText = "아이디 또는 비밀번호가 일치하지 않습니다.";
                        break;
                    case 2800:
                    case 20000:
                        subText = "등록되지 않은 아이디입니다.";
                        break;
                    case 3002:
                        if ("PASS_DIGITS".equalsIgnoreCase(login.getUserError())) {
                            subText = "로그인에 실패하였습니다 해당 금융기관 서비스에서 비밀번호를 확인해주세요";
                        }
                        break;
                    default:
                        subText = "에러가 발생하였습니다. 잠시 후 재시도해주세요.";
                        break;
                }
                viewHolder.mSub.setText(subText);
                viewHolder.mSub.setVisibility(View.GONE);
            } else {
                viewHolder.mSub.setVisibility(View.GONE);
            }
            return view;
        }

        private class ViewHolder {
            private TextView mName;
            private TextView mSub;
        }
    }

    public void onBtnClickNaviTxtTxtTxtBar(View view) {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_navi_txt_txt_txt_back:
                onBackPressed();
                break;
            case R.id.btn_next:
                processCert(mRequestLoginData);
                break;
            case R.id.btn_skip:
                enterMain();
                break;
            case R.id.et_login_id:
                if (isTransKeyShowing) {
                    onHideKeyboard();
                }
                break;
        }
    }

    TransKeyCipher mTkc = new TransKeyCipher("SEED");

    public void done(Intent data) {
        isTransKeyShowing = false;
        String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
//        String cipherDataex = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA_EX);
//        String cipherDataexp = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA_EX_PADDING);
//        String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);

        byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
        int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

        if (iRealDataLength == 0)
            return;

        StringBuffer plainData = null;
        String cipherStr = null;
        try {
            mTkc.setSecureKey(secureKey);
            mSecureKey = secureKey;
            APPSharedPrefer.getInstance(this).setPrefString(APP.PUBLIC_KEY, toHexString(secureKey));
            cipherStr = mTkc.getPBKDF2DataEncryptCipherData(cipherData);

            byte pbPlainData[] = new byte[iRealDataLength];
            if (mTkc.getDecryptCipherData(cipherData, pbPlainData)) {
//                plainData = new StringBuffer(new String(pbPlainData));
                mCipher = pbPlainData.clone();
                for(int i=0;i<pbPlainData.length;i++)
                    pbPlainData[i]=0x01;
            } else {
                // 복호화 실패
                //plainData = new StringBuffer("plainData decode fail...");
            }

        } catch (Exception e) {
            if(Global.debug) Log.d("STACKTRACE", e.getStackTrace().toString());
        }
//        mPassWord = cipherStr;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public String toHexString(byte[] bytes) {
        char[] hexChar = new char[bytes.length *2];
        for (int i=0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            hexChar[i * 2] = hexArray[v >>> 4];
            hexChar[i * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChar);
    }

    @Override
    public void input(int type) {
        checkBtnStatus();
    }

    private void checkBtnStatus() {
        int count = 0;
        for (int i=0; i < mInputLayout.getChildCount(); i++) {
            View view = mInputLayout.getChildAt(i);
            ViewHolder holder = (ViewHolder) view.getTag();
            if (holder.mKeyCtrl.getInputLength() >= PASSWORD_MINIMUM_LENGTH) {
                count++;
            }
        }
        if (count >= mInputLayout.getChildCount()) {
            mBtn.setEnabled(true);
        } else {
            mBtn.setEnabled(false);
        }
    }

    private void sendMultipleLoginMsg() {
        if (mResultList.size() == mRequestLoginData.size()) {
            DialogUtils.dismissLoading();
            Message message = new Message();
            message.what = RESULT_PASSWORD_MULTIPLE;
            message.obj = mResultList;
            mUiHandler.dispatchMessage(message);
        }
    }

    private int getErrorMsg(int errorCode, String userError, String errorMsg, boolean showSingleDialog) {
        Message message = new Message();
        int convertError = (errorCode & 0xFFFF);
        switch (convertError) {
            case 2300:
            case 2301:
            case 2302: // cert p/w error
                // cert error
                break;
            case 2001:
            case 2200:
                // network error
                break;
            case 2003:
            case 2701:
            case 2702:
            case 2710:
                // site renew
                break;
            case 2800:
                break;
            case 2801: // password error
//                int errorCount = 0;
//                if (showSingleDialog) {
//                    if (userError != null && !TextUtils.isEmpty(userError)) {
//                        errorCount = Integer.valueOf(userError);
//                        String warningMsg = "";
//                        if (errorCount == 99) {
//                            warningMsg = "("+1+"회 이상 연속 실패 시 사용자 중지)";
//                        } else if (errorCount > 0) {
//                            warningMsg = "("+(mDefaultCount-errorCount)+"회 이상 연속 실패 시 사용자 중지)";
//                        }
//                        errMsg = "로그인에 실패하였습니다.\n해당 금융기관 서비스에서\n비밀번호를 확인해주세요. "+ warningMsg;
//                        message = new Message();
//                        message.what = RESULT_PASSWORD_ERROR;
//                        message.obj = errMsg;
//                        mUiHandler.dispatchMessage(message);
//                    }
//                }
                break;
            case 2802: // password error count over
                break;
            case 2803: // id or password error
                break;
            case 2040:
                // do nothing
                break;
            case 3002:
                break;
            default:
                if (convertError == 2100) {
                    if (errorMsg.equalsIgnoreCase("정확하지 않은 아이디입니다. 아이디를 확인하시고 다시 로그인해 주십시오.")
                            || errorMsg.equalsIgnoreCase("죄송합니다.인터넷 회원으로 등록되어 있지 않습니다 회원가입 후 이용하여 주시기 바랍니다")) {
                        convertError = 20000;
                    }
                    if (userError.equalsIgnoreCase("UCXO5230")
                            || userError.equalsIgnoreCase("WCHPP1119")
                            || userError.equalsIgnoreCase("EACO03002")) {
                        convertError = 20000;
                    }
                }
                break;
        }
        return convertError;
    }

    private void initBinderService() {
        mServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
                isConnection = false;
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                isConnection = true;
                mService = new Messenger(service);
                registerMessenger();
                requestQueueList();
            }
        };

        if(ElseUtils.isServiceRunning(this, BatchService.class.getName()) == false) {
            ComponentName compName = new ComponentName(getPackageName(), BatchService.class.getName());
            startService(new Intent().setComponent(compName));
        }
        bindService(new Intent(this, BatchService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void registerMessenger() {
        if (isConnection) {
            try {
                // Give it some value as an example.
                Message msg = Message.obtain(null,
                        BatchService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /** Binder Service End **/
    private void finishBinderService() {
        if (isConnection) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            BatchService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            unbindService(mServiceConnection);
            isConnection = false;
        }
    }

    private void cancelIdPassword() {
        finish();
    }

    private void setLoginComplete(final ArrayList<SelectBankData> data) {
        DialogUtils.showLoading(this);
        final ArrayList<BankData> bankList = new ArrayList<>();
        if (data != null) {
            for (SelectBankData selectBankData : data) {
                BankData dataSet = new BankData();
                dataSet.setPassWord(selectBankData.getPwd());
                dataSet.setBankName(selectBankData.getName());
                dataSet.setBankCode(selectBankData.getCode());
                dataSet.setCertSerial(selectBankData.getId());
                dataSet.setType(selectBankData.getType());
                dataSet.setStatus(BankData.SCRAPING);
                dataSet.setLoginMethod(BankData.LOGIN_ID);
                bankList.add(dataSet);
            }
        }
        Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                DialogUtils.dismissLoading();
                insertBankData(str, bankList);
                if (fromMembership) {
                    IgawAdbrix.firstTimeExperience(AnalyticsName.BankRegComplete);
                    startBackgroundLoading();
                    ActivityCompat.finishAffinity(IdPasswordLoginActivity.this);
                    startLoading();
                } else {
                    startBackgroundService(bankList);
                    setResult(RESULT_OK);
                    finish();
                }
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void startLoading() {
        Intent intent = new Intent(IdPasswordLoginActivity.this, LoadingActivity.class);
        intent.putExtra(Const.FROM_MEMBERSHIP, true);
        startActivity(intent);
    }

    private void insertBankData(String key, ArrayList<BankData> list) {
        if (list != null) {
            ScrapDao scrapDao = ScrapDao.getInstance();
            for (BankData data : list) {
                BankData bankData = scrapDao.getBankDataByName(this, key, data.getBankName(), Session.getInstance(getApplicationContext()).getUserId());
                if (bankData == null || bankData.getBankName() == null || TextUtils.isEmpty(bankData.getBankName())) {
                    scrapDao.insertBankData(this, key, Session.getInstance(getApplicationContext()).getUserId(), data);
                } else {
                    bankData.setCertSerial(data.getCertSerial());
                    bankData.setPassWord(data.getPassWord());
                    bankData.setValidDate(data.getValidDate());
                    scrapDao.updateBankData(this, key, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                }
            }
        }
    }

    private void startBackgroundLoading() {
        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_FIRST);
        sendBroadcast(intent);
    }

    private void startBackgroundService(ArrayList<BankData> list) {
        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_INDIVIDUAL);
        intent.putExtra(Const.DATA, list);
        sendBroadcast(intent);
    }

    private void checkScrapState(ArrayList<ScrapRequester> list) {
        if (list.size() > 0) {
            isScraping = true;
        } else {
            isScraping = false;
        }
    }

    private void requestQueueList() {
        if (isConnection) {
            try {
                // Give it some value as an example.
                Message msg = Message.obtain(null,
                        BatchService.MSG_REQUEST_QUEUE, this.hashCode(), 0);
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    class IncomingHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case BatchService.MSG_UPLOAD_STATUS:
                    switch (msg.arg1) {
                        case ScrapingModule.STATUS_COMPLETE:
                            if (mMode == COLLECT_FIRST_THIS_MONTH) {

                            } else {

                            }
                            DialogUtils.dismissLoading();
                            break;
                        case ScrapingModule.STATUS_PREPARE:
                            break;
                        case ScrapingModule.STATUS_REQUEST:
                            break;
                        case ScrapingModule.STATUS_SCRAPING:
                            //String message = (String) msg.obj;
                            break;
                        case ScrapingModule.STATUS_UPLOADING:
                            break;
                    }
                    break;
                case BatchService.MSG_UPLOAD_COMPLETE:
                    break;
                case BatchService.MSG_UPLOAD_MODE:
                    int mode = msg.arg1;
                    ProcessMode[] modes = ProcessMode.values();
                    mMode = modes[mode];
                    switch (mMode) {
                        case COLLECT_FIRST_THIS_MONTH:
                            isScraping = true;
                            break;
                        case COLLECT_FIRST_PAST_MONTH:
                        case COLLECT_DAILY:
                        case COLLECT_INDIVIDUAL:
                        case COLLECT_RETRY:
                            isScraping = true;
                            break;
                        case COLLECT_IDLE:
                            break;
                        case COLLECT_LOGIN:
                            isScraping = true;
                            break;
                    }
                    requestQueueList();
                    break;
                case BatchService.MSG_REQUEST_QUEUE:
                    ArrayList<ScrapRequester> scrapRequesters = (ArrayList<ScrapRequester>) msg.obj;
                    checkScrapState(scrapRequesters);
                    break;
                case BatchService.MSG_BANK_COMPLETE:
                    DialogUtils.dismissLoading();
                    ArrayList<ScrapRequester> list = (ArrayList<ScrapRequester>) msg.obj;
                    checkScrapState(list);
                    sendMultipleLoginMsg();
                    break;
                case BatchService.MSG_LOGIN_COMPLETE:
                    mTimeOutHandler.removeCallbacks(mTimeoutRunnable);
                    FinancialLogin login = (FinancialLogin) msg.obj;
                    mResultList.add(login);
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }

}
