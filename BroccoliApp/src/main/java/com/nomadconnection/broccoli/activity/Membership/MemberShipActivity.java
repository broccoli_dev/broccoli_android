package com.nomadconnection.broccoli.activity.membership;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.fragment.FragmentMembershipTerm;
import com.nomadconnection.broccoli.utils.DialogUtils;

public class MembershipActivity extends BaseActivity implements
		View.OnClickListener {

	private int mGetStep = 0;
	private TextView guide;
	private LinearLayout mStepFirst;
	private LinearLayout mStepSecond;
	private LinearLayout mStepThird;
	private LinearLayout mStepFourth;
	private LinearLayout mCancel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_membership);

		init();
	}

	void init() {
		mCancel = (LinearLayout) findViewById(R.id.ll_navi_img_txt_back);
		mCancel.setVisibility(View.GONE);
		guide = (TextView) findViewById(R.id.tv_activity_membership_guide);
		mStepFirst = (LinearLayout) findViewById(R.id.ll_activity_membership_step_first);
		mStepSecond = (LinearLayout) findViewById(R.id.ll_activity_membership_step_second);
		mStepThird = (LinearLayout) findViewById(R.id.ll_activity_membership_step_third);
		mStepFourth = (LinearLayout) findViewById(R.id.ll_activity_membership_step_fourth);

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction ft = fragmentManager.beginTransaction();
		ft.add(R.id.fragment_area, new FragmentMembershipTerm());
		ft.commit();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setStepGuide(mGetStep);
	}

	public void setStepGuide(int step) {
		mGetStep = step;
		mStepFirst.setEnabled(true);
		mStepSecond.setEnabled(true);
		mStepThird.setEnabled(true);
		mStepFourth.setEnabled(true);
		mCancel.setVisibility(View.VISIBLE);
		switch (step) {
			case 0:
				mStepFirst.setSelected(false);
				mStepFirst.setEnabled(false);
				mStepSecond.setSelected(false);
				mStepThird.setSelected(false);
				mStepFourth.setSelected(false);
				guide.setText(R.string.membership_text_step_guide_first);
				break;
			case 1:
				mStepFirst.setSelected(true);
				mStepSecond.setSelected(false);
				mStepSecond.setEnabled(false);
				mStepThird.setSelected(false);
				mStepFourth.setSelected(false);
				guide.setText(R.string.membership_text_step_guide_second);
				break;
			case 2:
				mStepFirst.setSelected(true);
				mStepSecond.setSelected(true);
				mStepThird.setSelected(false);
				mStepThird.setEnabled(false);
				mStepFourth.setSelected(false);
				guide.setText(R.string.membership_text_step_guide_third);
				break;
			case 3:
				mStepFirst.setSelected(true);
				mStepSecond.setSelected(true);
				mStepThird.setSelected(true);
				mStepFourth.setEnabled(false);
				guide.setText(R.string.membership_text_step_guide_fourth);
				break;
		}
	}

	public void setGuideText(String text) {
		guide.setText(text);
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		cancel_popup();
	}

	public void onBtnClickNaviImgTxtBar(View view) {
		onBackPressed();
	}
	
	public int getFragmentArea() {
		return R.id.fragment_area;
	}

	private void cancel_popup(){
		final String mStr;
		if(mGetStep != 0){
			mStr = getString(R.string.membership_popup_cancel);
		} else {
			mStr = getString(R.string.membership_popup_exit);
		}
		DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
							switch (mGetStep) {
								case 0:
									sendTracker(AnalyticsName.UserTermAgreeCancel);
									break;
								case 1:
									sendTracker(AnalyticsName.UserNameCheckCancel);
									break;
								case 2:
									sendTracker(AnalyticsName.UserEmailRegCancel);
									break;
								case 3:
									sendTracker(AnalyticsName.UserPinRegCancel);
									break;
							}
							if(mGetStep != 0){
								FragmentManager fragmentManager = getSupportFragmentManager();
								FragmentTransaction ft = fragmentManager.beginTransaction();
								ft.replace(R.id.fragment_area, new FragmentMembershipTerm());
								ft.commit();
								setStepGuide(0);
								mCancel.setVisibility(View.GONE);
							}
							else {
								startActivity(new Intent(MembershipActivity.this, LoginActivity.class));
								finish();
							}
						}
					}
				},
				new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
	}
}
