package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewSettingNotice;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.SettingNotice;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level3ActivitySettingNotice extends BaseActivity implements OnItemClickListener{

	private static final String TAG = Level3ActivitySettingNotice.class.getSimpleName();

	
	/* Layout */
	private ListView mListView;
	private AdtListViewSettingNotice mAdapter;
	private List<SettingNotice> mAlInfoDataList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_setting_notice);
		sendTracker(AnalyticsName.Level3ActivitySettingNotice);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}



	
	
	

//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {    	
		try
		{
			switch (_view.getId()) {
				
			case R.id.ll_navi_img_txt_back:
				finish();
				break;
				
			default:
				break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());			
		}
	}


	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//			}
//			mAlInfoDataList = new ArrayList<SettingNotice>();
//			mAlInfoDataList.addAll(SampleTestData.GetSampleDataSettingNotice());
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout) findViewById(R.id.rl_navi_img_bg)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText("공지사항");
			
			/* Layout */
    		mListView = (ListView)findViewById(R.id.lv_level_3_activity_setting_notice_listview);
    		mAdapter = new AdtListViewSettingNotice(this, mAlInfoDataList);
    		mListView.setAdapter(mAdapter);
    		mListView.setOnItemClickListener(this);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			requestDataSet();
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	
	
	


//===============================================================================//
// Item Click Listener
//===============================================================================//
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (mAlInfoDataList.get(position) == null) {
			return;
		}
		
		Intent intent = new Intent(this, Level4ActivitySettingNoticeDetail.class);
		intent.putExtra(APP.INFO_DATA_SETTING_NOTICE_PARCEL, mAlInfoDataList.get(position));
		startActivity(intent);

	}


//=========================================================================================//
// Refresh Adapter
//=========================================================================================//
	public void refreshAdt(String _sort){
		mAdapter.notifyDataSetChanged();
	}


	public void requestDataSet() {
		DialogUtils.showLoading(this);
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(this).getUserId());

		UserApi api = ServiceGenerator.createService(UserApi.class);
		Call<List<SettingNotice>> call = api.SettingNotice(mRequestDataByUserId);
		call.enqueue(new Callback<List<SettingNotice>>() {
			@Override
			public void onResponse(Call<List<SettingNotice>> call, Response<List<SettingNotice>> response) {
				DialogUtils.dismissLoading();
				mAlInfoDataList = new ArrayList<SettingNotice>();
				mAlInfoDataList = response.body();
				mAdapter.setData((ArrayList<SettingNotice>) mAlInfoDataList);
				refreshAdt(null);
			}

			@Override
			public void onFailure(Call<List<SettingNotice>> call, Throwable t) {
				DialogUtils.dismissLoading();
			}
		});
	}
}
