package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtExpListViewAssetEditAccount;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Asset.AssetBankInfo;
import com.nomadconnection.broccoli.data.Asset.BodyBankActive;
import com.nomadconnection.broccoli.data.Asset.RequestAssetBankActive;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2016. 11. 14..
 */

public class Level3ActivityAssetEditAccount extends BaseActivity {

    private static final String TAG = Level3ActivityAssetEditAccount.class.getSimpleName();

    private ExpandableListView mExpListView;
    private ArrayList<String> mGroupList = null;
    private ArrayList<ArrayList<AssetBankInfo>> mChildList = null;
    private ArrayList<AssetBankInfo> mAlInfoDataList;
    private HashMap<String, Boolean> mChangeList = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_asset_das);
        sendTracker(AnalyticsName.AssetSavingEdit);
        if (parseParam()){
            if (initWidget()) {
                if (!initData()){
                    finish();
                }
            }
        }
    }

    private boolean parseParam() {
        boolean ret = true;
        if (getIntent() != null) {
            mAlInfoDataList = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_1);
            mGroupList = new ArrayList<String>();
            mChildList = new ArrayList<ArrayList<AssetBankInfo>>();
        }

        if(mAlInfoDataList == null){
            ret = false;
        }

        setData();
        return ret;
    }

    private boolean initWidget() {
        boolean ret = true;
        try {
			/* Title */
            ((TextView)findViewById(R.id.tv_navi_img_txt_txt_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitle);
            ((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_complete));
            if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) {
                ((RelativeLayout)findViewById(R.id.rl_navi_img_txt_txt_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
            }

			/* Layout */
            mExpListView = (ExpandableListView)findViewById(R.id.elv_level_2_activity_asset_das_listview);
            mExpListView.setAdapter(new AdtExpListViewAssetEditAccount(this, mGroupList, mChildList, new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    AssetBankInfo info = (AssetBankInfo) buttonView.getTag();
                    info.toggleActive(isChecked);
                    mChangeList.put(String.valueOf(info.mAssetBankID), isChecked);
                }
            }));

			/* group list click 시 */
            mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });

			/* Child list click 시 */
            mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                                            int groupPosition, int childPosition, long id) {
                    AssetBankInfo mTemp = null;
                    mTemp = mChildList.get(groupPosition).get(childPosition);
                    if (mTemp == null) return true;
                    return false;
                }
            });

            if (mAlInfoDataList != null){
                if (mAlInfoDataList.size() == 0) {
                    mExpListView.setVisibility(View.GONE);
                    ((FrameLayout)findViewById(R.id.inc_level_2_activity_asset_das_empty)).setVisibility(View.VISIBLE);
                    ((ImageView)findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
                    ((TextView)findViewById(R.id.tv_empty_layout_txt)).setText(getString(R.string.common_empty_das));
                } else {
                    mExpListView.setVisibility(View.VISIBLE);
                    ((FrameLayout)findViewById(R.id.inc_level_2_activity_asset_das_empty)).setVisibility(View.GONE);
                }
            }
        }
        catch(Exception e) {
            ret = false;
        }
        return ret;
    }

    /**
     * initial method
     * <p> open child view </p>
     * @return
     */
    private boolean initData() {
        boolean ret = true;
        try {
            for (int i = 0; i <mGroupList.size() ; i++) {
                mExpListView.expandGroup(i);
            }

        } catch(Exception e) {
            ret = false;
            e.printStackTrace();
        }
        return ret;
    }

    private void setData() {

        mChangeList.clear();

        if (mAlInfoDataList != null) {
            for (int i = 0; i < mAlInfoDataList.size(); i++) {
                if (i == 0) {
                    mGroupList.add(mAlInfoDataList.get(i).mAssetBankBankCode);
                } else {
                    if (!mGroupList.contains(mAlInfoDataList.get(i).mAssetBankBankCode)){
                        mGroupList.add(mAlInfoDataList.get(i).mAssetBankBankCode);
                    }
                }
            }

            ArrayList<AssetBankInfo> bankAccount;
            for (int i = 0; i < mGroupList.size() ; i++) {
                bankAccount = new ArrayList<AssetBankInfo>();
                for (int j = 0; j < mAlInfoDataList.size(); j++) {
                    if (mGroupList.get(i).equalsIgnoreCase(mAlInfoDataList.get(j).mAssetBankBankCode)){
                        AssetBankInfo bankInfo = mAlInfoDataList.get(j);
                        try {
                            bankAccount.add(bankInfo.clone());
                        } catch (CloneNotSupportedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mChildList.add(bankAccount);
            }
        }
    }

    public void onBtnClickNaviImgTxtTxtBar(View _view) {
        try {
            switch (_view.getId()) {
                case R.id.ll_navi_img_txt_txt_back:
                    finish();
                    break;
                case R.id.ll_navi_img_txt_txt_else:
                    sendEditList();
                    break;
                default:
                    break;
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void sendEditList() {
        if (mChangeList != null && mChangeList.size() > 0) {
            Set<String> set = mChangeList.keySet();
            Iterator<String> infoIterator = set.iterator();
            final List<BodyBankActive> list = new ArrayList<>();
            while (infoIterator.hasNext()) {
                BodyBankActive bankActive = new BodyBankActive();
                String id = infoIterator.next();
                bankActive.setAccountId(id);
                bankActive.setActiveYn(mChangeList.get(id) ? "Y":"N");
                list.add(bankActive);
            }
            RequestAssetBankActive assetBankActive = new RequestAssetBankActive();
            assetBankActive.setUserId(Session.getInstance(getApplicationContext()).getUserId());
            assetBankActive.setActive(list);
            Call call = ServiceGenerator.createScrapingService(AssetApi.class).setBankActiveRequest(assetBankActive);
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if (response.isSuccessful()) {
                        Result result = response.body();
                        if (result != null) {
                            if (!result.isOk()) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.common_warning_server_link_error), Toast.LENGTH_LONG).show();
                            } else {
                                for (int i=0; i < mAlInfoDataList.size(); i++) {
                                    AssetBankInfo info = mAlInfoDataList.get(i);
                                    int id = info.mAssetBankID;
                                    for (int j=0; j < list.size(); j++) {
                                        BodyBankActive bankActive = list.get(j);
                                        int targetId = Integer.valueOf(bankActive.getAccountId());
                                        if (id == targetId) {
                                            info.mAssetBankActiveYn = bankActive.getActiveYn();
                                        }
                                    }
                                }
                                Intent intent = new Intent();
                                intent.putExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_1, mAlInfoDataList);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.common_warning_server_link_error), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

}
