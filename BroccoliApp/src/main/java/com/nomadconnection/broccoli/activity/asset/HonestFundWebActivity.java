package com.nomadconnection.broccoli.activity.asset;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.Const;

/**
 * Created by YelloHyunminJang on 2017. 1. 17..
 */

public class HonestFundWebActivity extends BaseActivity {

    private static final String HONEST_FUND = "https://www.honest-fund.com/apply/1";

    private WebView mWebView;
    private ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_honest_webview);
        init();
    }

    private void init() {
        String url = null;

        Intent intent = getIntent();
        if (intent.hasExtra(Const.DATA)) {
            url = intent.getStringExtra(Const.DATA);
        } else {
            url = HONEST_FUND;
        }

        mWebView = (WebView) findViewById(R.id.wb_guide_view);
        mProgress = (ProgressBar) findViewById(R.id.pb_guide_view);

        // 웹뷰에서 자바스크립트실행가능
        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.loadUrl(url);
        // WebViewClient 지정
        mWebView.setWebViewClient(new HonestFundWebActivity.WebViewClientClass());
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                mProgress.setProgress(newProgress);
            }
        });

        View linkBtn = findViewById(R.id.link_layout);
        linkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyClipboardHonestFund();
            }
        });
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains("goBroccoli")) {
                view.stopLoading();
                finish();
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mProgress.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private void copyClipboardHonestFund() {
        Toast.makeText(this, "어니스트 펀드 주소를 복사하였습니다.", Toast.LENGTH_SHORT).show();
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("HONEST_FUND", HONEST_FUND);
        clipboard.setPrimaryClip(clip);
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        finish();
    }
}
