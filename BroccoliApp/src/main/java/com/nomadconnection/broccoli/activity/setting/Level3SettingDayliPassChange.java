package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.membership.MembershipFindPwdActivity;
import com.nomadconnection.broccoli.activity.membership.MembershipResetPwdActivity;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 1. 4..
 */

public class Level3SettingDayliPassChange extends BaseActivity implements View.OnClickListener {

    private EditText mPwd;
    private View mBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_setting_dayli_pass_change);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SettingRegPwChange);
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView) findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_user_account);
        TextView email = (TextView) findViewById(R.id.tv_activity_dayli_pwd_change_email);
        email.setText(Session.getInstance(this).getUserPersonalInfo().user.email);
        findViewById(R.id.rl_activity_dayli_find_pwd_btn).setOnClickListener(this);
        mBtn = findViewById(R.id.btn_confirm);
        mBtn.setOnClickListener(this);
        mBtn.setEnabled(false);

        mPwd = (EditText) findViewById(R.id.et_activity_dayli_pwd_change_cert);
        mPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 2) {
                    mBtn.setEnabled(true);
                } else {
                    mBtn.setEnabled(false);
                }
            }
        });

    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.rl_activity_dayli_find_pwd_btn:
                showFindPwd();
                break;
            case R.id.btn_confirm:
                matchPwd();
                break;
        }
    }

    private void showFindPwd() {
        Intent intent = new Intent(this, MembershipFindPwdActivity.class);
        startActivity(intent);
    }

    private void matchPwd() {
        DialogUtils.showLoading(this);
        final String pwd = mPwd.getText().toString();
        DayliApi api = ServiceGenerator.createServiceDayli(DayliApi.class);
        long id = DbManager.getInstance().getDayliId(this, Session.getInstance(this).getUserId());
        String token = DbManager.getInstance().getUserDayliToken(this, Session.getInstance(this).getUserId());
        Call<Result> call = api.dayliPasswordCheck(token, id, pwd);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    changePwd(pwd);
                } else {
                    ErrorMessage message = ErrorUtils.parseError(response);
                    switch (message) {
                        case ERROR_701_INCORRECT_ACCOUNT:
                            Toast.makeText(getBase(), "비밀번호가 일치하지 않습니다. 다시 입력해 주세요.", Toast.LENGTH_LONG).show();
                            mPwd.setText(null);
                            mPwd.requestFocus();
                            break;
                        case ERROR_719_QUITE_USER:
                            Toast.makeText(getBase(), "탈퇴 회원은 탈퇴일로부터 5일 이후 재가입 가능합니다.", Toast.LENGTH_LONG).show();
                            break;
                        case ERROR_713_NO_SUCH_USER:
                            Toast.makeText(getBase(), "가입되지 않은 사용자입니다. 새로 회원가입해주세요.", Toast.LENGTH_LONG).show();
                            break;
                        case ERROR_716_TOKEN_EXPIRE:
                            Toast.makeText(getBase(), "다른 단말에서 로그인하셨습니다.", Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getBase());
            }
        });
    }

    private void changePwd(String oldPwd) {
        Intent intent = new Intent(this, MembershipResetPwdActivity.class);
        intent.putExtra(MembershipResetPwdActivity.MODE_CHANGE, true);
        intent.putExtra(Const.DATA, oldPwd);
        startActivity(intent);
        finish();
    }
}
