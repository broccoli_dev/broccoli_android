package com.nomadconnection.broccoli.activity.spend;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.CustomTabView;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogCustomType;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Spend.SpendCardBanner;
import com.nomadconnection.broccoli.data.Spend.SpendCardRecommendData;
import com.nomadconnection.broccoli.data.Spend.SpendCardUserListData;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.fragment.spend.FragmentSpendCardList;
import com.nomadconnection.broccoli.fragment.spend.FragmentSpendCardRecommend;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 3. 17..
 */

public class Level2ActivitySpendCardTabList extends BaseActivity implements InterfaceEditOrDetail {

    public static final String REQUEST_SHOW_RECOMMEND_CARD = "REQUEST_SHOW_RECOMMEND_CARD";

    private static final int FRAGMENT_CARD_LIST = 0;
    private static final int FRAGMENT_CARD_RECOMMEND = 1;

    private View mTabParentLayout;
    private TabLayout mTabLayout;
    private ArrayList<SpendCardUserListData> mMyCardList;
    private ArrayList<SpendCardRecommendData> mClassifiedCardList;
    private ArrayList<String> enableCompanyList;
    private int mRecommendYn = 0;
    private int mClassifiedCardCount = 0;
    private int mCurrentIndex = FRAGMENT_CARD_LIST;
    private long mCardId = 0;
    private boolean isFromLink = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_spend_card_list);
        init();
    }

    private void init() {
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_card_info);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
        mTabParentLayout = findViewById(R.id.rl_spend_card_tab);
        mTabLayout = (TabLayout) findViewById(R.id.tl_spend_card_tab);
        mTabLayout.setOnTabSelectedListener(onTabSelectedListener);
        CustomTabView tabView = new CustomTabView(this);
        tabView.createNormalTabView(getString(R.string.spend_card_list_mycard_title));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(tabView));
        tabView = new CustomTabView(this);
        tabView.createNormalTabView(getString(R.string.spend_card_list_recommend_title));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(tabView));
    }

    private BaseFragment getFragment(int idx) {
        BaseFragment fragment = null;
        Bundle bundle = new Bundle();
        mCurrentIndex = idx;
        switch (idx) {
            case FRAGMENT_CARD_LIST:
                fragment = new FragmentSpendCardList();
                bundle.putSerializable(Const.DATA, mMyCardList);
                bundle.putSerializable(FragmentSpendCardList.ENABLE_COMPANY, enableCompanyList);
                bundle.putInt(FragmentSpendCardList.RECOMMEND_YN , mRecommendYn);
                fragment.setArguments(bundle);
                break;

            case FRAGMENT_CARD_RECOMMEND:
                fragment = new FragmentSpendCardRecommend();
                if (mCardId > 0) {
                    bundle.putLong(FragmentSpendCardRecommend.SHOW_SPECIFIC_CARD, mCardId);
                }
                bundle.putSerializable(Const.DATA, mClassifiedCardList);
                fragment.setArguments(bundle);
                break;
            default:
                break;
        }

        return fragment;
    }

    private void setFragment(BaseFragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_area, fragment, fragment.getTagName());
        ft.commit();
    }

    private void initData() {
        Intent intent = getIntent();
        SpendCardBanner banner = (SpendCardBanner) intent.getSerializableExtra(Const.DATA);
        if (banner != null) {
            mRecommendYn = banner.recommendYn;
            if (mRecommendYn > 0) {
                mTabParentLayout.setVisibility(View.VISIBLE);
            } else {
                mTabParentLayout.setVisibility(View.GONE);
            }
        }
        if (intent.hasExtra(REQUEST_SHOW_RECOMMEND_CARD)) {
            mCardId = intent.getLongExtra(REQUEST_SHOW_RECOMMEND_CARD, 0);
            mCurrentIndex = FRAGMENT_CARD_RECOMMEND;
            isFromLink = true;
        }
        getRecommendData();
    }

    private void getRecommendData() {

        DialogUtils.showLoading(getActivity());
        Call<JsonElement> ownCardList = ServiceGenerator.createService(SpendApi.class).getOwnCardList();
        ownCardList.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                JsonObject object = element.getAsJsonObject();
                mClassifiedCardCount = object.get("cardCount").getAsInt();
                JsonArray array = object.getAsJsonArray("targetList");
                if (array != null) {
                    Type listType = new TypeToken<ArrayList<SpendCardRecommendData>>() {}.getType();
                    mClassifiedCardList = new Gson().fromJson(array, listType);
                    getMyCardList();
                } else {
                    ErrorUtils.parseError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void getMyCardList() {
        DialogUtils.showLoading(getActivity());
        Call<JsonElement> call = ServiceGenerator.createService(SpendApi.class).getMyCardList();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                JsonObject object = element.getAsJsonObject();
                JsonArray array = object.getAsJsonArray("cardList");
                if (array != null) {
                    JsonArray companyList = object.getAsJsonArray("cardCompanyList");
                    Type stringType = new TypeToken<ArrayList<String>>() {}.getType();
                    enableCompanyList = new Gson().fromJson(companyList, stringType);
                    Type listType = new TypeToken<ArrayList<SpendCardUserListData>>() {}.getType();
                    mMyCardList = new Gson().fromJson(array, listType);
                    setData(mRecommendYn, mMyCardList);
                } else {
                    ErrorUtils.parseError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void setData(int recommendYn, ArrayList<SpendCardUserListData> list) {
        if (recommendYn == 0 && mTabLayout.getTabCount() > 1) {
            mTabLayout.removeTabAt(1);
        }
        setFragment(getFragment(mCurrentIndex));
        if (isFromLink) {
            mTabLayout.getTabAt(1).select();
        }
    }

    @Override
    public void onEdit() {

    }

    @Override
    public void onDetail() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onRefresh() {
        mCardId = 0;
        getRecommendData();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(REQUEST_SHOW_RECOMMEND_CARD)) {
            mCardId = intent.getLongExtra(REQUEST_SHOW_RECOMMEND_CARD, 0);
            boolean isExist = false;
            for (SpendCardRecommendData data : mClassifiedCardList) {
                if (mCardId == data.cardId) {
                    isExist = true;
                    break;
                }
            }
            if (isExist) {
                mCurrentIndex = FRAGMENT_CARD_RECOMMEND;
                getRecommendData();
                mTabLayout.getTabAt(1).select();
            } else {
                if (mClassifiedCardList.size() == 0) {
                    showQna();
                } else {
                    showRecommendUnablePopup();
                }
            }
        }
    }

    TabLayout.OnTabSelectedListener onTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            CustomTabView customView = (CustomTabView) tab.getCustomView();
            if (customView != null) {
                isFromLink = false;
                int position = tab.getPosition();
                if (position == FRAGMENT_CARD_RECOMMEND) {
                    if (mClassifiedCardList != null && mClassifiedCardList.size() > 0) {
                        setFragment(getFragment(position));
                        customView.getTabView().setEnabled(true);
                    } else {
                        showQna();
                    }
                } else {
                    mCardId = 0;
                    setFragment(getFragment(position));
                    customView.getTabView().setEnabled(true);
                }
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            CustomTabView customView = (CustomTabView) tab.getCustomView();
            if (customView != null) {
                customView.getTabView().setEnabled(false);
            }
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private void showQna() {
        mTabLayout.getTabAt(0).select();
        Intent intent = new Intent(getBase(), Level3ActivitySpendCardRecommendQna.class);
        startActivity(intent);
    }

    private void showRecommendUnablePopup() {
        mTabLayout.getTabAt(0).select();
        CommonDialogCustomType customType = new CommonDialogCustomType(this, R.string.spend_card_recommend_unable, new int[] {R.string.common_confirm});
        View view = getLayoutInflater().inflate(R.layout.custom_dialog_spend_card_recommend_fail, null);
        customType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        customType.addCustomBody(view);
        customType.show();
    }
}
