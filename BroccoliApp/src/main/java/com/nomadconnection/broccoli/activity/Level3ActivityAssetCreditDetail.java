package com.nomadconnection.broccoli.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.PagerRefreshAdapter;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.common.CustomPagerSlidingTabStripSide;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Asset.BodyAssetCardDetail;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCardDetail;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCardBill;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCardDetail;
import com.nomadconnection.broccoli.data.Spend.AssetCardPaymentTypeEnum;
import com.nomadconnection.broccoli.fragment.FragmentAssetCreditDetailA;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level3ActivityAssetCreditDetail extends BaseActivity {

	private static final String TAG = Level3ActivityAssetCreditDetail.class.getSimpleName();


	/* Layout */
	private TextView mCardName;
	private TextView mCardInfo1Title;
	private TextView mCardInfo1Cost;
	private RadioGroup mSortGroup;
	private LinearLayout mGraphOnce;
	private LinearLayout mGraphSplit;
	private LinearLayout mGraphLoan;
	private TextView mLegendOnce;
	private TextView mLegendSplit;
	private TextView mLegendLoan;

	/* Pager */
	private PagerRefreshAdapter mPagerRefreshAdapter;
	private ViewPager mViewPager;
	private int mCurrentPageInx;

	/* Pager Title */
	private String[] mPagerTitle;

	/* Child Fragment */
	private ArrayList<Fragment> alFragment;

	/* getParcelableExtra */
	private ArrayList<BodyAssetCardDetail> mAlInfoDataList1;
	private ArrayList<BodyAssetCardDetail> mAlInfoDataList2;
	private ArrayList<BodyAssetCardDetail> mAlInfoDataList3;
	private ArrayList<BodyAssetCardDetail> mAlInfoDataList4;
	private ArrayList<BodyAssetCardDetail> mAlInfoDataList5;
	private ArrayList<BodyAssetCardDetail> mAlInfoDataList6;
	private ResponseAssetCardBill				mInfoData;
	private int mSideMonth = 1;
	private String[] mTempBillDate;
	private String[] mTempMonthSpace;
	private String[] mTempMonthSpaceBefore;
	private String[] mTempMonthSpaceAfter;
	private HashMap<String, Boolean> mHmFlag;
	private long mAllCardSum1 = 0;
	private long mAllCardSum2 = 0;
	private long mAllCardSum3 = 0;
	private long mAllCardSum4 = 0;
	private long mAllCardSum5 = 0;
	private long mAllCardSum6 = 0;
	private SortType mSort = SortType.TOTAL;

	public enum SortType {
		TOTAL,
		ONCE,
		SPLITTING,
		LOAN
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_asset_credit_detail);
		sendTracker(AnalyticsName.Level3ActivityAssetCreditDetail);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}










//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {

				case R.id.ll_navi_img_txt_back:
					finish();
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}





//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mInfoData = (ResponseAssetCardBill) getIntent().getSerializableExtra(APP.INFO_DATA_ASSET_CREDIT_DEPTH_2_HEADER);
			mAlInfoDataList6 = new ArrayList<>();
			if(mInfoData == null || mAlInfoDataList6 == null)
				bResult = false;

			mTempMonthSpace = TransFormatUtils.getRangeMonth(TransFormatUtils.getAfterDate(mInfoData.payDate, "m", mSideMonth), 6 + mSideMonth * 2);
			mTempMonthSpaceBefore = new String[mSideMonth];
			mTempMonthSpaceAfter = new String[mSideMonth];
			for (int i = 0; i <mTempMonthSpace.length ; i++) {
				if (i == 0) mTempMonthSpaceBefore[0] = TransFormatUtils.transAddDot(mTempMonthSpace[i]);
				else if (i == mTempMonthSpace.length-1) mTempMonthSpaceAfter[0] = TransFormatUtils.transAddDot(mTempMonthSpace[i]);
			}

			mTempBillDate = TransFormatUtils.getRangeMonth(mInfoData.payDate, 6);
			mCurrentPageInx = mTempBillDate.length-1;
			// 여기
			mPagerTitle = new String[mTempBillDate.length];
			mAlInfoDataList1 = new ArrayList<BodyAssetCardDetail>();
			mAlInfoDataList2 = new ArrayList<BodyAssetCardDetail>();
			mAlInfoDataList3 = new ArrayList<BodyAssetCardDetail>();
			mAlInfoDataList4 = new ArrayList<BodyAssetCardDetail>();
			mAlInfoDataList5 = new ArrayList<BodyAssetCardDetail>();
			mHmFlag = new HashMap<String, Boolean>();
			for (int i = 0; i <mTempBillDate.length ; i++) {
				mHmFlag.put(mTempBillDate[i].toString(), false);
				mPagerTitle[i] = TransFormatUtils.transAddDot(mTempBillDate[i]);
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitle);
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) ((RelativeLayout)findViewById(R.id.rl_navi_img_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);

			/* Layout */
			((LinearLayout)findViewById(R.id.ll_contents_area_inc_type_5_layout)).setBackgroundResource(R.color.main_1_layout_bg);

			mCardName = (TextView)findViewById(R.id.tv_contents_area_inc_type_5_txt_card_1);
			mCardInfo1Title = (TextView)findViewById(R.id.tv_contents_area_inc_type_5_txt_1);
			mCardInfo1Cost = (TextView)findViewById(R.id.tv_contents_area_inc_type_5_txt_2);
			mCardInfo1Cost.setTypeface(FontClass.setFont(this));

			mGraphOnce = (LinearLayout) findViewById(R.id.ll_asset_card_detail_once);
			mGraphSplit = (LinearLayout) findViewById(R.id.ll_asset_card_detail_splitting);
			mGraphLoan = (LinearLayout) findViewById(R.id.ll_asset_card_detail_loan);
			mLegendOnce = (TextView) findViewById(R.id.tv_asset_card_detail_legend_once);
			mLegendSplit = (TextView) findViewById(R.id.tv_asset_card_detail_legend_splitting);
			mLegendLoan = (TextView) findViewById(R.id.tv_asset_card_detail_legend_loan);

			alFragment = new ArrayList<Fragment>();
			alFragment.add(new FragmentAssetCreditDetailA());
			alFragment.add(new FragmentAssetCreditDetailA());
			alFragment.add(new FragmentAssetCreditDetailA());
			alFragment.add(new FragmentAssetCreditDetailA());
			alFragment.add(new FragmentAssetCreditDetailA());
			alFragment.add(new FragmentAssetCreditDetailA());

			mSortGroup = (RadioGroup) findViewById(R.id.rg_level_3_activity_asset_credit_detail_group);
			mSortGroup.setOnCheckedChangeListener(mRadioBtnCheckedChangeListener);

			/* Pager */
			mPagerRefreshAdapter = new PagerRefreshAdapter(getSupportFragmentManager(), alFragment, mPagerTitle);
			mViewPager = (ViewPager) findViewById(R.id.pager_level_3_activity_asset_credit_detail_body);
			mViewPager.setAdapter(mPagerRefreshAdapter);
			mViewPager.setOffscreenPageLimit(alFragment.size());

			CustomPagerSlidingTabStripSide tabs = (CustomPagerSlidingTabStripSide) findViewById(R.id.pager_level_3_activity_asset_credit_detail_tabs);
			tabs.setSpace(APP.CUSTOM_PAGE_SLIDING_TAB_STRIP_SIDE_TYPE_CARD, mSideMonth, mTempMonthSpaceBefore, mTempMonthSpaceAfter);
			tabs.setShouldExpand(true);
			tabs.setViewPager(mViewPager);
			tabs.setOnPageChangeListener(mPageChangeListener);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCardName.setText(TransFormatUtils.transCardCodeToName(mInfoData.companyCode));
			mViewPager.setCurrentItem(mCurrentPageInx);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//===============================================================================//
// Refresh
//===============================================================================//
	private void refreshHeader(){
		String mTempDate ="";
		if (TransFormatUtils.getCompareDate(TransFormatUtils.getDataFormatWhich(1),TransFormatUtils.transAddDot(mTempBillDate[mCurrentPageInx]))){
			mTempDate = mTempBillDate[mCurrentPageInx].toString().substring(4,6) + "월 결제 예정 금액";
		}
		else{
			mTempDate = mTempBillDate[mCurrentPageInx].toString().substring(4,6) + "월 결제 금액";
		}

		ArrayList<BodyAssetCardDetail> currentDataList = null;

		int onceCount = 0;
		int splitCount = 0;
		int loanCount = 0;
		long onceValue = 0;
		long splitValue = 0;
		long loanValue = 0;
		long tempTotal = 0;
		switch (mCurrentPageInx) {
			case 0:
				currentDataList = mAlInfoDataList1;
				tempTotal = mAllCardSum1;
				break;
			case 1:
				currentDataList = mAlInfoDataList2;
				tempTotal = mAllCardSum2;
				break;

			case 2:
				currentDataList = mAlInfoDataList3;
				tempTotal = mAllCardSum3;
				break;

			case 3:
				currentDataList = mAlInfoDataList4;
				tempTotal = mAllCardSum4;
				break;

			case 4:
				currentDataList = mAlInfoDataList5;
				tempTotal = mAllCardSum5;
				break;

			case 5:
				currentDataList = mAlInfoDataList6;
				tempTotal = mAllCardSum6;
				break;

			default:
				break;
		}
		long totalValue = 0;

		if (currentDataList != null) {
			for (int i=0; i < currentDataList.size(); i++) {
				BodyAssetCardDetail detail = currentDataList.get(i);
				totalValue += detail.realAmount;
				if (AssetCardPaymentTypeEnum.SPLIT == detail.paymentType
					|| AssetCardPaymentTypeEnum.SPLIT_ETC == detail.paymentType) {
					splitCount++;
					splitValue += detail.realAmount;
				} else if (AssetCardPaymentTypeEnum.SHORT_LOAN == detail.paymentType
						|| AssetCardPaymentTypeEnum.LONG_LOAN == detail.paymentType) {
					loanCount++;
					loanValue += detail.realAmount;
				} else {
					onceCount++;
					onceValue += detail.realAmount;
				}
			}
		}

		totalValue = tempTotal;

		Animation animation = new AlphaAnimation(0, 1);
		animation.setDuration(500);

		if (onceCount == 0) {
			mGraphOnce.setVisibility(View.GONE);
			mLegendOnce.setVisibility(View.GONE);
		} else {
			mGraphOnce.setVisibility(View.VISIBLE);
			mGraphOnce.setAnimation(animation);
			mLegendOnce.setVisibility(View.VISIBLE);
			if (totalValue != 0) {
				float value = (float) ((double)onceValue/(double)totalValue * 10f);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraphOnce.getLayoutParams();
				params.weight = value;
				mGraphOnce.setLayoutParams(params);
			}
		}
		if (loanCount == 0) {
			mGraphLoan.setVisibility(View.GONE);
			mLegendLoan.setVisibility(View.GONE);
		} else {
			mGraphLoan.setVisibility(View.VISIBLE);
			mGraphLoan.setAnimation(animation);
			mLegendLoan.setVisibility(View.VISIBLE);
			if (totalValue != 0) {
				float value = (float) ((double)loanValue/(double)totalValue * 10f);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraphLoan.getLayoutParams();
				params.weight = value;
				mGraphLoan.setLayoutParams(params);
			}
		}
		if (splitCount == 0) {
			mGraphSplit.setVisibility(View.GONE);
			mLegendSplit.setVisibility(View.GONE);
		} else {
			mGraphSplit.setVisibility(View.VISIBLE);
			mGraphSplit.setAnimation(animation);
			mLegendSplit.setVisibility(View.VISIBLE);
			if (totalValue != 0) {
				float value = (float) ((double)splitValue/(double)totalValue * 10f);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraphSplit.getLayoutParams();
				params.weight = value;
				mGraphSplit.setLayoutParams(params);
			}
		}

		mCardInfo1Title.setText(mTempDate);
		mCardInfo1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(String.valueOf(totalValue)));
	}

//===============================================================================//
// View Pager
//===============================================================================//

	/**  View Page Change Listener **/
	private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {

		@Override
		public void onPageSelected(int _position) {
			mCurrentPageInx = _position;
			if (mHmFlag.get(mTempBillDate[mCurrentPageInx])) {
				mSortGroup.check(R.id.rb_level_3_activity_asset_credit_detail_total);
				refreshHeader();
				refreshAdapter();
				return;
			} else {
				mHmFlag.put(mTempBillDate[mCurrentPageInx].toString(), true);
			}
			switch (mCurrentPageInx) {
				default:
					DialogUtils.showLoading(Level3ActivityAssetCreditDetail.this);											// 자산 로딩
					ServiceGenerator.createService(AssetApi.class).getCardBillDetail(
							new RequestAssetCardDetail(
									mInfoData.companyCode, mTempBillDate[mCurrentPageInx], mInfoData.billType)
					).enqueue(new Callback<ResponseAssetCardDetail>() {
						@Override
						public void onResponse(Call<ResponseAssetCardDetail> call, Response<ResponseAssetCardDetail> response) {
							DialogUtils.dismissLoading();
							if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
								processBillData(response.body());
							} else {
								ElseUtils.network_error(getBase());
							}
						}

						@Override
						public void onFailure(Call<ResponseAssetCardDetail> call, Throwable t) {
							DialogUtils.dismissLoading();											// 자산 로딩
							ElseUtils.network_error(Level3ActivityAssetCreditDetail.this);
						}
					});
					break;
			}

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}

		private void processBillData(ResponseAssetCardDetail body) {
			switch (mCurrentPageInx) {
				case 0:
					mAllCardSum1 = body.sum;
					mAlInfoDataList1 = body.list;
					break;
				case 1:
					mAllCardSum2 = body.sum;
					mAlInfoDataList2 = body.list;
					break;
				case 2:
					mAllCardSum3 = body.sum;
					mAlInfoDataList3 = body.list;
					break;
				case 3:
					mAllCardSum4 = body.sum;
					mAlInfoDataList4 = body.list;
					break;
				case 4:
					mAllCardSum5 = body.sum;
					mAlInfoDataList5 = body.list;
					break;
				case 5:
					mAllCardSum6 = body.sum;
					mAlInfoDataList6 = body.list;
					break;
			}

			mSortGroup.check(R.id.rb_level_3_activity_asset_credit_detail_total);

			ArrayList<BodyAssetCardDetail> detailList = body.list;
			FragmentAssetCreditDetailA fragmentAssetCreditDetailA = (FragmentAssetCreditDetailA) alFragment.get(mCurrentPageInx);

			if (fragmentAssetCreditDetailA != null) {
				fragmentAssetCreditDetailA.setData(detailList);
				fragmentAssetCreditDetailA.refreshData();
			}
			refreshHeader();
		}
	};

	private void refreshAdapter() {
		FragmentAssetCreditDetailA fragmentAssetCreditDetailA = (FragmentAssetCreditDetailA) alFragment.get(mCurrentPageInx);
		if (fragmentAssetCreditDetailA != null) {
			fragmentAssetCreditDetailA.setSort(mSort);
			fragmentAssetCreditDetailA.refreshData();
			if (mPagerRefreshAdapter != null) mPagerRefreshAdapter.notifyDataSetChanged();
		}
	}

	private RadioGroup.OnCheckedChangeListener mRadioBtnCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			switch (checkedId) {
				case R.id.rb_level_3_activity_asset_credit_detail_total:
					mSort = SortType.TOTAL;
					break;
				case R.id.rb_level_3_activity_asset_credit_detail_once:
					mSort = SortType.ONCE;
					break;
				case R.id.rb_level_3_activity_asset_credit_detail_splitting:
					mSort = SortType.SPLITTING;
					break;
				case R.id.rb_level_3_activity_asset_credit_detail_loan:
					mSort = SortType.LOAN;
					break;
			}
			refreshAdapter();
		}
	};

}
