package com.nomadconnection.broccoli.activity.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.igaworks.IgawCommon;
import com.igaworks.adbrix.IgawAdbrix;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Splash;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.interf.LifecycleCallbacks;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;

import java.util.List;

public class BaseActivity extends AppCompatActivity {

    private static LifecycleCallbacks mLifeCycleListener;
    Runnable lockRunnable = new Runnable() {
        @Override
        public void run() {
            Session.getInstance(getApplicationContext()).setLock(true);
        }
    };

//========================================================================//
// @Override
//========================================================================//

    public static void setListener(LifecycleCallbacks callbacks) {
        mLifeCycleListener = callbacks;
    }

    public static boolean isForeGroundProcess(Context context, String packageName) {

        boolean isRunning = false;

        ActivityManager actMng = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> list = actMng.getRunningTasks(1);

        for(ActivityManager.RunningTaskInfo rap : list) {
            if(rap.baseActivity.getPackageName().equals(packageName) && !rap.baseActivity.getClassName().equals(BaseActivity.class.getName())) {
                isRunning = true;
                break;
            }
            rap.topActivity.getClass();
        }

        return isRunning;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        MainApplication.get().onActivitySaveInstanceState(this, outState);
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onActivitySaveInstanceState(this, outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        MainApplication.get().onActivityStarted(this);
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onActivityStarted(this);
        }
        super.onStart();
    }

    @Override
    protected void onResume() {
        AppEventsLogger.activateApp(this);
        MainApplication.get().onActivityResumed(this);
        IgawCommon.startSession(this); //애드브릭스 추가
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onActivityResumed(this);
        }
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onActivityCreated(this, savedInstanceState);
        }
        super.onCreate(savedInstanceState);
        Session.getInstance(this);
        MainApplication.get().onActivityCreated(this, savedInstanceState);
        if (MainApplication.Screen.getInstance().getScreenInfo(getClass().getSimpleName()) != null
                && MainApplication.Screen.getInstance().getScreenInfo(getClass().getSimpleName()).mAnimSet.equals("on")) {
            overridePendingTransition(MainApplication.Screen.getInstance().getScreenInfo(getClass().getSimpleName()).mStartEnterAnim,
                    MainApplication.Screen.getInstance().getScreenInfo(getClass().getSimpleName()).mStartExitAnim);
        }
    }

    public void sendTracker(String name) {
        Tracker tracker = MainApplication.get().getDefaultTracker();
        tracker.setScreenName(name);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
        IgawAdbrix.retention(name);
    }

    @Override
    protected void onPause() {
        AppEventsLogger.deactivateApp(this);
        MainApplication.get().onActivityPaused(this);
        IgawCommon.endSession();    //애드브릭스 추가
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onActivityPaused(this);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        MainApplication.get().onActivityStopped(this);
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onActivityStopped(this);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        MainApplication.get().onActivityDestroyed(this);
        if (mLifeCycleListener != null) {
            mLifeCycleListener.onActivityDestroyed(this);
        }
        super.onDestroy();
    }

    public Activity getBase() {
        return this;
    }

    /**
     * 광고 라이브러리에서 getActivity()함수가 항상 null을 return해서 호출하지 못하도록 base 예외처리
     * @return
     */
    public Activity getActivity() {
        return this;
    }

    //========================================================================//
// public method    
//========================================================================//

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void finish() {
        // TODO Auto-generated method stub
        super.finish();
        if (MainApplication.Screen.getInstance().getScreenInfo(getClass().getSimpleName()) != null
                && MainApplication.Screen.getInstance().getScreenInfo(getClass().getSimpleName()).mAnimSet.equals("on")) {
            overridePendingTransition(MainApplication.Screen.getInstance().getScreenInfo(getClass().getSimpleName()).mFinishEnterAnim,
                    MainApplication.Screen.getInstance().getScreenInfo(getClass().getSimpleName()).mFinishExitAnim);
        }
    }

    public void showTokenExpire() {
        DialogUtils.showDlgBaseOneButtonNoTitle(getBase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
                    Session.getInstance(getBase()).logout();
                    Intent intent = new Intent(getBase(), Splash.class);
                    getBase().startActivity(intent);
                    ActivityCompat.finishAffinity(getBase());
                }
            }
        }, new InfoDataDialog(R.layout.dialog_inc_case_1, getBase().getString(R.string.membership_text_login_fail_disable_device), "확인"));
    }

    public void showLoginActivity() {
        DialogUtils.showDlgBaseOneButtonNoTitle(getBase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
                    Session.getInstance(getBase()).logout();
                    Intent intent = new Intent(getBase(), Splash.class);
                    getBase().startActivity(intent);
                    ActivityCompat.finishAffinity(getBase());
                }
            }
        }, new InfoDataDialog(R.layout.dialog_inc_case_1, "가입되지 않은 사용자입니다. 새로 회원가입해주세요.", "확인"));
    }

    public void sessionLoseLoginAgain() {
        Intent intent = new Intent(this, Splash.class);
        startActivity(intent);
    }

    public void onBtnClickNaviMotion(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_motion_else:
                break;
            case R.id.ll_navi_motion_back:
                onBackPressed();
                break;
        }
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_motion_else:
                break;
            case R.id.ll_navi_img_txt_back:
                onBackPressed();
                break;
        }
    }
}