package com.nomadconnection.broccoli.activity.financial;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialSelectNewCert;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoliraonsecure.KSW_CertItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 5. 24..
 */

public class LinkageFinancialSettingDetailActivity extends BaseActivity {

    private static final int SELECT_NEW_CERT = 1;
    private HashMap<String, Object> mSelectItem;
    private ImageView mStatus;
    private TextView mSubjectName;
    private TextView mMessage;
    private TextView mPolicy;
    private TextView mIssuerName;
    private TextView mExpireTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_financial_linkage_setting_detail);
        init();
    }

    private void init() {
        initWidget();
        initData();
    }

    private void initWidget() {
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        TextView title = (TextView) findViewById(R.id.tv_navi_img_txt_title);
        title.setText(R.string.setting_financial_reg_link_setting);
        findViewById(R.id.ll_cert_renewal_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNewCert();
            }
        });

        mStatus = (ImageView)findViewById(R.id.iv_cert_status);
        mSubjectName = (TextView)findViewById(R.id.tv_certname);
        mMessage = (TextView)findViewById(R.id.tv_message);
        mIssuerName = (TextView)findViewById(R.id.tv_cert_issuername);
        mPolicy = (TextView)findViewById(R.id.tv_cert_policy);
        mExpireTime = (TextView)findViewById(R.id.tv_cert_expire_time);
    }

    private void initData() {
        Intent intent = getIntent();
        mSelectItem = (HashMap<String, Object>) intent.getSerializableExtra(Const.DATA);

        String name = (String) mSelectItem.get(KSW_CertItem.SUBJECTNAME);
        if (name != null && !TextUtils.isEmpty(name)) {
            String[] names = name.split("\\(");
            if (names != null && names.length > 0) {
                mSubjectName.setText(names[0]);
            } else {
                mSubjectName.setText(name);
            }
            mPolicy.setText((String)mSelectItem.get(KSW_CertItem.POLICY));
            mIssuerName.setText((String)mSelectItem.get(KSW_CertItem.ISSUERNAME));
            mExpireTime.setText((String)mSelectItem.get(KSW_CertItem.EXPIREDTIME));
        } else {
            // delete certification
            name = (String) mSelectItem.get(KSW_CertItem.PATH);
            boolean isLowerCase = false;
            if (name != null) {
                isLowerCase = name.contains("cn=");
            }
            String[] splitTemp = isLowerCase ? name.split("cn=") : name.split("CN=");
            if (splitTemp.length > 0) {
                String[] names = splitTemp[1].split("\\(");
                if (names != null && names.length > 0) {
                    mSubjectName.setText(names[0]);
                } else {
                    mSubjectName.setText(name);
                }
            }
            mExpireTime.setText(R.string.setting_financial_reg_link_setting_cert_missing);
            mExpireTime.setTextColor(getResources().getColor(R.color.common_warning_color));
            findViewById(R.id.middle_layout).setVisibility(View.GONE);
        }
    }

    private void startNewCert() {
        Intent intent = new Intent(this, Level3ActivitySettingFinancialSelectNewCert.class);
        startActivityForResult(intent, SELECT_NEW_CERT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_NEW_CERT && resultCode == RESULT_OK) {
            if (data != null) {
                String id = data.getStringExtra(Const.CERT_NAME);
                String pwd = data.getStringExtra(Const.CERT_PWD);
                String expire = data.getStringExtra(Const.CERT_VALIDDATE);
                setCertComplete(id, pwd, expire);
            }
        }
    }

    private void setCertComplete(final String cert, final String pwd, final String expire) {
        DialogUtils.showLoading(this);
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                replaceCertificate(str, cert, pwd, expire);
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
                finish();
            }
        });

    }

    private void replaceCertificate(String key, final String cert, final String pwd, final String expire) {
        if (mSelectItem != null) {
            ArrayList<BankData> bankList = new ArrayList<BankData>();
            List<BankData> list = ScrapDao.getInstance().getBankData(getBase(), key, Session.getInstance(getBase()).getUserId());
            for (BankData data : list) {
                if (BankData.LOGIN_CERT.equalsIgnoreCase(data.getLoginMethod()) && data.getCertSerial().equalsIgnoreCase((String) mSelectItem.get(KSW_CertItem.PATH))) {
                    bankList.add(data);
                }
            }
            for (int i=0; i < bankList.size(); i++) {
                BankData target = bankList.get(i);
                target.setCertSerial(cert);
                target.setPassWord(pwd);
                target.setValidDate(expire);
            }
            if (list.size() > 0) {
                String id = Session.getInstance(getBase()).getUserId();
                for (BankData data : bankList) {
                    ScrapDao.getInstance().updateBankData(getBase(), key, id, data);
                }
            }
            startBackgroundService(bankList);
            DialogUtils.dismissLoading();
            setResult(RESULT_OK);
            finish();
        }
    }

    private void startBackgroundService(ArrayList<BankData> list) {
        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_INDIVIDUAL);
        intent.putExtra(Const.DATA, list);
        sendBroadcast(intent);
    }

}
