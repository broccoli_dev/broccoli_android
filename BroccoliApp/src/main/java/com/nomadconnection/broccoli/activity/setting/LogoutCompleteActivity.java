package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.MainActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 2. 1..
 */

public class LogoutCompleteActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout_complete);
        init();
    }

    private void init() {
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLogout();
            }
        });
    }

    @Override
    public void onBackPressed() {
        requestLogout();
    }

    private void requestLogout() {
        UserApi api = ServiceGenerator.createService(UserApi.class);
        Call<Result> call = api.logout();
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response != null && (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK))) {
                    clearSession();
                } else {

                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    private void clearSession() {
        Session.getInstance(this).logout();

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(Const.MEMBER_LEAVE, "member_leave");
        startActivity(intent);
        finish();
    }
}
