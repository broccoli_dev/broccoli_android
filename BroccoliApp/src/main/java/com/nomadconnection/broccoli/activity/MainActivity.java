package com.nomadconnection.broccoli.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.hyunmin.materialrefresh.MaterialRefreshLayout;
import com.hyunmin.materialrefresh.MaterialRefreshListener;
import com.kyleduo.switchbutton.SwitchButton;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.setting.Level2ActivityBasicInfo;
import com.nomadconnection.broccoli.activity.setting.Level2ActivitySettingUserInfoEdit;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFaq;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialReg;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingGuide;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingNotice;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingPassword;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingQuestion;
import com.nomadconnection.broccoli.adapter.PagerAdapter;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.common.CustomSwipeRefresh;
import com.nomadconnection.broccoli.common.CustomTabView;
import com.nomadconnection.broccoli.common.MainRefreshView;
import com.nomadconnection.broccoli.common.SmartTabLayout;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.CdnAppInfoData;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.Dayli.UserProfileUrl;
import com.nomadconnection.broccoli.data.Etc.EtcNoticeInfo;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.MainPopupData;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Scrap.ServiceRecord;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.database.ServiceRecordDao;
import com.nomadconnection.broccoli.fragment.FragmentMain1;
import com.nomadconnection.broccoli.fragment.FragmentMain2;
import com.nomadconnection.broccoli.fragment.FragmentMain3;
import com.nomadconnection.broccoli.fragment.FragmentMain4;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.interf.InterfaceScrollable;
import com.nomadconnection.broccoli.service.BatchService;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.service.ScrapingModule;
import com.nomadconnection.broccoli.service.data.ScrapRequester;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.service.mode.ScrapResult;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FileUtils;
import com.nomadconnection.broccoli.view.RoundedAvatarDrawable;
import com.nomadconnection.broccoliespider.data.BankRequestData;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.MainApplication.mAPPShared;
import static com.nomadconnection.broccoli.api.ServiceGenerator.createService;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

	private static final String TAG = MainActivity.class.getSimpleName();
	private final int ACTIVITY_REQUEST_ALBUM_IMAGE = 2;

	private final long	FINSH_INTERVAL_TIME    = 2000;
	/* Refresh */
	private long		backPressedTime        = 0;

	/* Pager */
	private DrawerLayout mDrawerLayout;
	private FrameLayout mTabParent;
	private SmartTabLayout tabs;
	private PagerAdapter mMainPagerAdapter;
	private ViewPager mViewPager;
	private View Header;
	private int mCurrentPageInx;
	private CustomSwipeRefresh mCustomRefresh;
	private CustomSwipeRefresh mSwipeRefreshLayout;
	private LinearLayout mLoadingLayout;
	private ImageView mLoading;
	private TextView mScrapingTime;
	private NavigationView mNavigationView;
	private boolean isNoticeCheck = false;

	/* Pager Title */
	private String[] mPagerTitle;
	private int mNoticeCount = 0;
	private String mValue;
	private boolean mFromDPLogin = false;
    private boolean fromMembership = false;

	/* Child Fragment */
	private ArrayList<Fragment> alFragment;
	private List<EtcNoticeInfo> mAlInfoDataList;
	private Handler mColorChangeHandler = new Handler(){

		@Override
		public void dispatchMessage(Message msg) {
			pageScrolling(msg.what, msg.arg1);
		}
	};
	private Handler mHandler = new Handler();
	final Messenger mMessenger = new Messenger(new IncomingHandler());
	private ProcessMode mMode = ProcessMode.COLLECT_IDLE;
	private boolean isConnection = false;
	private Messenger			mService;
	private ServiceConnection 	mServiceConnection;
	/**  View Page Change Listener **/
	private OnPageChangeListener mPageChangeListener = new OnPageChangeListener() {

	    @Override
	    public void onPageSelected(int _position) {
	    	mCurrentPageInx = _position;
	    }

	    @Override
	    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
	    	final float[] from	= new float[3];
	    	final float[] to 	= new float[3];
	    	final float[] hsv  	= new float[3];                  // transition color
	    	if (position == 0 ) {
	    		Color.colorToHSV((getResources().getColor(R.color.main_1_layout_bg)), from);   // from
	    		Color.colorToHSV((getResources().getColor(R.color.main_2_layout_bg)), to);     // to
			}
	    	else if (position == 1 ) {
	    		Color.colorToHSV((getResources().getColor(R.color.main_2_layout_bg)), from);   // from
	    		Color.colorToHSV((getResources().getColor(R.color.main_3_layout_bg)), to);     // to
	    	}
	    	else if (position == 2 ) {
	    		Color.colorToHSV((getResources().getColor(R.color.main_3_layout_bg)), from);   // from
	    		Color.colorToHSV((getResources().getColor(R.color.main_4_layout_bg)), to);     // to
	    	}
	    	else if (position == 3 ) {
				Color.colorToHSV((getResources().getColor(R.color.main_4_layout_bg)), from);   // from
				Color.colorToHSV((getResources().getColor(R.color.main_4_layout_bg)), to);     // to
			}

	    	hsv[0] = from[0] + (to[0] - from[0])*positionOffset;
	    	hsv[1] = from[1] + (to[1] - from[1])*positionOffset;
	    	hsv[2] = from[2] + (to[2] - from[2])*positionOffset;

	    	Message mMsg = new Message();
	    	mMsg.what = position;
	    	mMsg.arg1 = Color.HSVToColor(hsv);
	    	mColorChangeHandler.sendMessage(mMsg);
	    }

	    @Override
	    public void onPageScrollStateChanged(int state) {
			int position = tabs.getSelectedTabPosition();
			CustomTabView tab = (CustomTabView) tabs.getTabAt(position);
			switch (state) {
				case ViewPager.SCROLL_STATE_IDLE:
					tab.getTabView().setEnabled(true);
					break;
				default:
					tab.getTabView().setEnabled(false);
					break;
			}
			toggleScrollStateChanged(state == ViewPager.SCROLL_STATE_IDLE);
	    }
	};

	private void toggleScrollStateChanged(boolean toggle) {
		//mSwipeRefreshLayout.setEnabled(toggle);
	}

//===============================================================================//
// Refresh Handler
//===============================================================================//
	private Handler mGetDataTaskHandler = new Handler(){

		@Override
		public void dispatchMessage(Message msg) {
			switch (msg.what) {
			case APP.PTR_GETDATATASK_HANDLER_SUCCESS:
				break;

			case APP.PTR_GETDATATASK_HANDLER_FAIL:
				break;
			}

		}
	};

	private boolean isScraping = false;
	private boolean isLoadingShowing = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		sendTracker(AnalyticsName.FragmentMain1);
		initBinderService();
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		super.onStart();
		if(!isNoticeCheck){
			emergency_notice(); //메인 공지사항 추가
		}
	}

	@Override
	protected void onDestroy(){
		super.onDestroy();
		finishBinderService();
	}

	@Override
	protected void onPause() {
		super.onPause();
//		mSwipeRefreshLayout.setRefreshing(false);
//		mSwipeRefreshLayout.computeScroll();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Session.getInstance(this).setLockTemporaryDisable(false);
		requestQueueList();
		notice_get_list();
		checkScarpResult();
		if (mNavigationView != null) {
			mNavigationView.post(new Runnable() {
				@Override
				public void run() {
					showLatestScrapingTime();
					setNavigationView(mNavigationView);
				}
			});
		}
	}

	//=========================================================================================//
// ToolBar - Main Bar Button Action
//=========================================================================================//

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (intent.hasExtra(Const.MAIN_INVEST)){
			mValue = intent.getStringExtra(Const.MAIN_INVEST);
			mCurrentPageInx = 2;
			mViewPager.setCurrentItem(mCurrentPageInx);
		}
		else if(intent.hasExtra(Const.MEMBER_LEAVE)){
			Intent member_leave = new Intent(this, Splash.class);
			startActivity(member_leave);
			finish();
		}
	}

	@Override
	public void onBackPressed() {

		if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
			mDrawerLayout.closeDrawer(GravityCompat.END);
			return;
		}

		long tempTime        = System.currentTimeMillis();
		long intervalTime    = tempTime - backPressedTime;

		if ( 0 <= intervalTime && FINSH_INTERVAL_TIME >= intervalTime ) {
			super.onBackPressed();
		}
		else {
			backPressedTime = tempTime;
			Toast.makeText(getApplicationContext(),getResources().getString(R.string.common_app_kill), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){

		}
		return super.onKeyDown(keyCode, event);
	}

	/**  Main Bar button click event handler **/
	View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.btn_pop_menu:
					startActivity(new Intent(MainActivity.this, Level2ActivityNotice.class));
					//MainActivity.this.sendBroadcast(new Intent(IntentDef.INTENT_ACTION_UPLOAD_FIRST));
					break;

				case R.id.btn_close:
					mDrawerLayout.openDrawer(GravityCompat.END);
					sendTracker(AnalyticsName.SettingList);
					break;

				default:
					break;
			}
		}
	};

//===============================================================================//
// Page Change After setting
//===============================================================================//

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			notice_get_list();
			/* getExtra */
			Intent intent = getIntent(); // 값을 받아온다.
			if(intent.hasExtra(Const.MAIN_INVEST)){
				mValue = intent.getStringExtra(Const.MAIN_INVEST);
			}
			if (intent.hasExtra(Const.FROM_DP_LOGIN)) {
				mFromDPLogin = true;
			}
			if (intent.hasExtra(Const.FROM_MEMBERSHIP)) {
                fromMembership = true;
            }
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			Header = findViewById(R.id.navi_title);
			mLoadingLayout = (LinearLayout) Header.findViewById(R.id.main_loading_layout);
			mLoading = (ImageView) Header.findViewById(R.id.main_loading_small);
			mScrapingTime = (TextView) Header.findViewById(R.id.main_loading_scraping_time);

			/* PagerFragment Tab Items */
			mPagerTitle = getResources().getStringArray(R.array.pager_fragment_tab_items);

			alFragment = new ArrayList<Fragment>();
			alFragment.add(new FragmentMain1());
			alFragment.add(new FragmentMain2());
			alFragment.add(new FragmentMain3());
			alFragment.add(new FragmentMain4());
//			alFragment.add(new FragmentMainRemit());


			/* Pager */
			mMainPagerAdapter = new PagerAdapter(getSupportFragmentManager(), alFragment, mPagerTitle);
			mViewPager = (ViewPager) findViewById(R.id.pager_body);
			mViewPager.setAdapter(mMainPagerAdapter);
			mViewPager.setOffscreenPageLimit(alFragment.size());
			mViewPager.addOnPageChangeListener(mPageChangeListener);
			//mViewPager.setScrollDurationFactor(5);

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
				ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) mViewPager.getLayoutParams();
				mViewPager.setLayoutParams(params);
			}

			mTabParent = (FrameLayout) findViewById(R.id.tabs_parent);
			tabs = (SmartTabLayout) findViewById(R.id.tabs);
			tabs.setCustomTabView(new SmartTabLayout.TabProvider() {
				@Override
				public View createTabView(ViewGroup container, int position, android.support.v4.view.PagerAdapter adapter) {
					CustomTabView customTabView = new CustomTabView(MainActivity.this);
					customTabView.createTabView(container, position, adapter);
					return customTabView;
				}
			});
			tabs.setOnPageChangeListener(mPageChangeListener);
			tabs.setViewPager(mViewPager);
			for (int i = 0; i < tabs.getTabCount(); i++) {
				CustomTabView tab = (CustomTabView) tabs.getTabAt(i);
				if (i == 0) {
					tab.getTabView().setEnabled(true);
				}
				ViewGroup layout = (ViewGroup) tab.getParent();
				layout.setBackground(null);
			}
			tabs.setOnTabClickListener(new SmartTabLayout.OnTabClickListener() {
				@Override
				public void onTabClicked(int position) {
					switch (position) {
						case 0:
							sendTracker(AnalyticsName.FragmentMain1);
							break;
						case 1:
							sendTracker(AnalyticsName.FragmentMain2);
							break;
						case 2:
							sendTracker(AnalyticsName.FragmentMain3);
							break;
						case 3:
							sendTracker(AnalyticsName.FragmentMain4);
							break;
						default:
							break;
					}
					mViewPager.setCurrentItem(position, false);
					CustomTabView tab = (CustomTabView) tabs.getTabAt(position);
					tab.getTabView().setEnabled(true);
					Fragment frg = mMainPagerAdapter.getItem(position);
					frg.onResume();

					if (frg != null)
						((InterfaceFragmentInteraction) frg).reset(true);
				}

				@Override
				public void onTabReSelected(int position) {

				}

				@Override
				public void onTabUnSelected(int position) {
					CustomTabView tab = (CustomTabView) tabs.getTabAt(position);
					tab.getTabView().setEnabled(false);
					Fragment frg = mMainPagerAdapter.getItem(position);

					if (frg != null)
						((InterfaceFragmentInteraction) frg).reset(true);
				}
			});
			Header.setAlpha(0.9f);
			mTabParent.setAlpha(0.9f);

			mCustomRefresh = (CustomSwipeRefresh) findViewById(R.id.swipe_layout);
			mCustomRefresh.addCustomHeader(new MainRefreshView(this));
			mCustomRefresh.setMaterialRefreshListener(new MaterialRefreshListener() {

				@Override
				public void onfinish() {
					super.onfinish();
					if (!isScraping) {
						Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_REFRESH);
						MainActivity.this.sendBroadcast(intent);
					}
				}

				@Override
				public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
					onProcessRefresh();
				}
			});
			mCustomRefresh.setInterfaceFragmentReset(new InterfaceScrollable() {
				@Override
				public boolean canScroll() {
					boolean ret = false;
					int index = mViewPager.getCurrentItem();
					Fragment frg = mMainPagerAdapter.getItem(index);
					if (frg != null)
						ret = ((InterfaceFragmentInteraction) frg).canScrollUp();
					return ret;
				}
			});
			mCustomRefresh.setIsOverLay(false);
			mCustomRefresh.setWaveShow(false);

//			mSwipeRefreshLayout = (CustomSwipeRefresh) findViewById(R.id.swipe_layout);
//			mSwipeRefreshLayout.setProgressViewOffset(true, ElseUtils.convertDp2Px(this, 80f), ElseUtils.convertDp2Px(this, 160));
//			mSwipeRefreshLayout.setOnRefreshListener(new CustomSwipeRefresh.OnRefreshListener() {
//				@Override
//				public void onRefresh() {
//					if (isScraping) {
//						Handler handler = new Handler();
//						handler.postDelayed(new Runnable() {
//							@Override
//							public void run() {
//								mSwipeRefreshLayout.setRefreshing(false);
//								mSwipeRefreshLayout.computeScroll();
//								requestQueueList();
//							}
//						}, 1500);
//					} else {
//						Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_REFRESH);
//						MainActivity.this.sendBroadcast(intent);
//						Handler handler = new Handler();
//						handler.postDelayed(new Runnable() {
//							@Override
//							public void run() {
//								mSwipeRefreshLayout.setRefreshing(false);
//								mSwipeRefreshLayout.computeScroll();
//								requestQueueList();
//							}
//						}, 1500);
//					}
//				}
//			});
//			mSwipeRefreshLayout.setInterfaceFragmentReset(new InterfaceScrollable() {
//				@Override
//				public boolean canScroll() {
//					boolean ret = false;
//					int index = mViewPager.getCurrentItem();
//					Fragment frg = mMainPagerAdapter.getItem(index);
//					if (frg != null)
//						ret = ((InterfaceFragmentReset) frg).canScrollUp();
//					return ret;
//				}
//			});
//			mSwipeRefreshLayout.setColorSchemeColors(Color.GRAY);


			findViewById(R.id.btn_pop_menu).setOnClickListener(onClickListener);
			findViewById(R.id.btn_close).setOnClickListener(onClickListener);

//			tabs.setShouldExpand(true);
//			tabs.setViewPager(mViewPager);
//			tabs.setOnPageChangeListener(mPageChangeListener);
//			mScrollLayout.setDraggableView(tabs);
//			mScrollLayout.setOnScrollChangedListener(new OnScrollChangedListener() {
//				@Override
//				public void onScrollChanged(int y, int oldY, int maxY) {
//
//					final float tabsTranslationY;
//					if (y < maxY) {
//						tabsTranslationY = .0F;
//					} else {
//						tabsTranslationY = y - maxY;
//					}
//
//					tabs.setTranslationY(tabsTranslationY);
//
//					Header.setTranslationY(y / 2);
//				}
//			});
//			mScrollLayout.setCanScrollVerticallyDelegate(new CanScrollVerticallyDelegate() {
//				@Override
//				public boolean canScrollVertically(int direction) {
//					BaseFragment fragment = (BaseFragment) alFragment.get(mCurrentPageInx);
//					return fragment.canScrollVertically(direction);
//				}
//			});
			mNavigationView = (NavigationView) findViewById(R.id.nav_view);
			mNavigationView.setNavigationItemSelectedListener(this);
			setNavigationView(mNavigationView);
			mNavigationView.post(new Runnable() {
				@Override
				public void run() {
					if (mFromDPLogin) {
						Intent intent = new Intent(MainActivity.this, Level3ActivitySettingFinancialReg.class);
						startActivity(intent);
					}
				}
			});
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private SwitchCompat mTouchIdSwitch;
	private SwitchButton mPushMsgSwitch;
	private TextView mFinancialStatus;
	private TextView mVersionCheck;
	private TextView mServerVersion;
	private TextView mBtn;
	private ImageView mProfileView;

	private void setNavigationView(NavigationView view) {
		View header = view.getHeaderView(0);

		UserPersonalInfo info = Session.getInstance(this).getUserPersonalInfo();

		mProfileView = (ImageView) findViewById(R.id.iv_main_round_avatar);
		mProfileView.setOnClickListener(mBtnClickListener);
		final TextView name = (TextView) findViewById(R.id.tv_main_name);
		final TextView email = (TextView) findViewById(R.id.tv_main_email);

		if (info != null) {
			showProfileImage(info);
			name.setText(info.user.realname);
			email.setText(info.user.email);
		} else {
			Session.getInstance(this).refreshUserPersonalInfo(new Session.OnUserInfoUpdateCompleteInterface() {
				@Override
				public void complete(UserPersonalInfo info) {
					if (info == null) {
						showProfileImage(info);
						name.setText(info.user.realname);
						email.setText(info.user.email);
					}
				}

				@Override
				public void error(ErrorMessage message) {
					switch (message) {
						case ERROR_713_NO_SUCH_USER:
						case ERROR_10007_SESSION_NOT_FOUND:
						case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
						case ERROR_20003_MISMATCH_ACCESS_TOKEN:
						case ERROR_20004_NO_SUCH_USER:
							showTokenExpire();
							break;
					}
				}
			});
		}

		View infoEdit = findViewById(R.id.ll_main_info_edit);
		infoEdit.setOnClickListener(mBtnClickListener);

		/* Layout */
		mFinancialStatus = (TextView) header.findViewById(R.id.tv_activity_setting_financial_status);
		mVersionCheck = (TextView) header.findViewById(R.id.tv_activity_setting_version_check);
		mServerVersion = (TextView) header.findViewById(R.id.tv_activity_setting_server_version);
		mBtn = (TextView) header.findViewById(R.id.tv_activity_setting_version_btn);
		View userInfo =  header.findViewById(R.id.ll_activity_setting_user_info);
		View connectFI =  header.findViewById(R.id.ll_activity_setting_connect_fi);
		View userPw =  header.findViewById(R.id.ll_activity_setting_user_pw);
		View version =  header.findViewById(R.id.ll_activity_setting_version);
		View notice =  header.findViewById(R.id.ll_activity_setting_notice);
		View faq =  header.findViewById(R.id.ll_activity_setting_faq);
		View question =  header.findViewById(R.id.ll_activity_setting_question);
		View guide =  header.findViewById(R.id.ll_activity_setting_guide);

		userInfo.setOnClickListener(mBtnClickListener);
		connectFI.setOnClickListener(mBtnClickListener);
		userPw.setOnClickListener(mBtnClickListener);

		version.setOnClickListener(mBtnClickListener);
		notice.setOnClickListener(mBtnClickListener);
		guide.setOnClickListener(mBtnClickListener);
		faq.setOnClickListener(mBtnClickListener);
		question.setOnClickListener(mBtnClickListener);
		mBtn.setOnClickListener(mBtnClickListener);

		mTouchIdSwitch = (SwitchCompat) header.findViewById(R.id.switch_activity_setting_touch_id);
		mPushMsgSwitch = (SwitchButton) header.findViewById(R.id.switch_activity_setting_push_msg);

		mTouchIdSwitch.setOnCheckedChangeListener(CheckChangeListener);
		mPushMsgSwitch.setOnCheckedChangeListener(CheckChangeListener);

		DialogUtils.showLoading(this);
		Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
			@Override
			public void acquired(String str) {
				List<BankData> dataList = ScrapDao.getInstance().getBankData(getApplicationContext(), str, Session.getInstance(getApplicationContext()).getUserId());
				if (dataList != null && dataList.size() > 0) {
					mFinancialStatus.setText(Integer.toString(dataList.size())+getString(R.string.common_count));
				} else {
					mFinancialStatus.setText("미연결");
				}
				DialogUtils.dismissLoading();
			}

			@Override
			public void error(ErrorMessage errorMessage) {
				switch (errorMessage) {
					case ERROR_713_NO_SUCH_USER:
					case ERROR_10007_SESSION_NOT_FOUND:
					case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
					case ERROR_20003_MISMATCH_ACCESS_TOKEN:
					case ERROR_20004_NO_SUCH_USER:
						showLoginActivity();
						break;
				}
			}

			@Override
			public void fail() {
				DialogUtils.dismissLoading();
			}
		});

		mVersionCheck.setText("V" + MainApplication.getVersionName());
		boolean push = APPSharedPrefer.getInstance(this).getPrefBool(Const.PREFERENCE_SETTING_PUSH, true);
		mPushMsgSwitch.setChecked(push);
		showLatestVersion();
	}

	private void showLatestVersion() {
		CdnAppInfoData appInfoData = Session.getInstance(this).getAppInfo();
		if (appInfoData != null && appInfoData.version != null) {
			String ver = appInfoData.version.server_version;
			String[] server_ver = ver.split("\\.");
			String[] local_ver = MainApplication.getVersionName().split("\\.");
			int flag = ElseUtils.isHigherVersion(local_ver, server_ver);
			if (flag < 0) {
				mBtn.setEnabled(true);
				mBtn.setText("최신 버전 업데이트하기");
			} else {
				mBtn.setEnabled(false);
				mBtn.setText("최신 버전");
			}
			mServerVersion.setText(ver);
		}

	}

	private void showProfileImage(UserPersonalInfo info) {
		Glide.with(this).load(info.user.profileImageUrl).asBitmap().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(new BitmapImageViewTarget(mProfileView) {

			@Override
			protected void setResource(Bitmap resource) {
				RoundedAvatarDrawable avatarDrawable = new RoundedAvatarDrawable(resource);
//				RoundedBitmapDrawable circularBitmapDrawable =
//						RoundedBitmapDrawableFactory.create(MainActivity.this.getResources(), resource);
//				circularBitmapDrawable.setCircular(true);
				mProfileView.setImageDrawable(avatarDrawable);
			}
		});
	}

	/**  check change Listener **/
	private CompoundButton.OnCheckedChangeListener CheckChangeListener = new CompoundButton.OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			switch (buttonView.getId()) {
				case R.id.switch_activity_setting_touch_id:
					break;
				case R.id.switch_activity_setting_push_msg:
					APPSharedPrefer.getInstance(MainActivity.this).setPrefBool(Const.PREFERENCE_SETTING_PUSH, isChecked);
					updatePushYn(isChecked);
					break;
			}
		}
	};

	private void updatePushYn(boolean bool) {
		DialogUtils.showLoading(this);
		HashMap<String, Object> hashMap = new HashMap<>();
		hashMap.put("userId", Session.getInstance(this).getUserId());
		hashMap.put("pushYn", bool ? "Y" : "N");
		Call<Result> call = ServiceGenerator.createService(UserApi.class).setUserPushEnable(hashMap);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();
			}
		});
	}

	private View.OnClickListener mBtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.iv_main_round_avatar:
					pickPhoto();
					break;

				case R.id.ll_main_info_edit:
					startActivity(new Intent(MainActivity.this, Level2ActivitySettingUserInfoEdit.class));
					break;

				case R.id.ll_activity_setting_user_info:
					startActivity(new Intent(MainActivity.this, Level2ActivityBasicInfo.class));
					break;

				case R.id.ll_activity_setting_connect_fi:
					startActivity(new Intent(MainActivity.this, Level3ActivitySettingFinancialReg.class));
					break;

				case R.id.ll_activity_setting_user_pw:
					startActivity(new Intent(MainActivity.this, Level3ActivitySettingPassword.class));
					break;
				case R.id.ll_activity_setting_version:
					break;

				case R.id.ll_activity_setting_notice:
					startActivity(new Intent(MainActivity.this, Level3ActivitySettingNotice.class));
					break;

				case R.id.ll_activity_setting_guide:
					startActivity(new Intent(MainActivity.this, Level3ActivitySettingGuide.class));
					break;

				case R.id.ll_activity_setting_faq:
					startActivity(new Intent(MainActivity.this, Level3ActivitySettingFaq.class));
					break;

				case R.id.ll_activity_setting_question:
					startActivity(new Intent(MainActivity.this, Level3ActivitySettingQuestion.class));
					break;
				case R.id.tv_activity_setting_version_btn:
					sendTracker(AnalyticsName.SettingVerClick);
					ElseUtils.showPlayStore(getBase());
					break;
				default:
					break;
			}

		}
	};

	private void pickPhoto() {
		sendTracker(AnalyticsName.SettingMyPic);
		Session.getInstance(this).setLockTemporaryDisable(true);
		Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        intent.setType("image/*");
		startActivityForResult(intent, ACTIVITY_REQUEST_ALBUM_IMAGE);
	}

	private Uri convertImage(String path) {
		int degrees = ElseUtils.getExifOrientation(path);

		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, opt);
		opt.inSampleSize = ElseUtils.calculateInSampleSize(opt, 100, 100);
		opt.inJustDecodeBounds = false;

		Bitmap orgImage = BitmapFactory.decodeFile(path, opt);

		orgImage = ElseUtils.GetRotatedBitmap(orgImage, degrees);
		String filename = "Profile_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
		return ElseUtils.saveImage(this, orgImage, filename);
	}

	private void uploadFile(Uri uri) {

		File file = FileUtils.getFile(this, uri);

		// create RequestBody instance from file
		RequestBody requestFile =
				RequestBody.create(MediaType.parse("image/jpeg"), file);

		// MultipartBody.Part is used to send also the actual file name
		MultipartBody.Part body =
				MultipartBody.Part.createFormData("file", file.getName(), requestFile);

		DayliApi api = ServiceGenerator.createServiceDayliUpload(DayliApi.class);
		String token = DbManager.getInstance().getUserDayliToken(this, Session.getInstance(this).getUserId());
		Call<UserProfileUrl> call = api.uploadProfileImage(token, body);
		call.enqueue(new Callback<UserProfileUrl>() {
			@Override
			public void onResponse(Call<UserProfileUrl> call, Response<UserProfileUrl> response) {
				if (response != null && response.isSuccessful()) {
					sendTracker(AnalyticsName.SettingMyPicDone);
					UserProfileUrl url = response.body();
					UserPersonalInfo info = Session.getInstance(MainActivity.this).getUserPersonalInfo();
					info.user.profileImageUrl = url.profileImageUrl;
					showProfileImage(info);
				} else {
					ElseUtils.network_error(MainActivity.this);
				}
			}

			@Override
			public void onFailure(Call<UserProfileUrl> call, Throwable t) {
				ElseUtils.network_error(MainActivity.this);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ACTIVITY_REQUEST_ALBUM_IMAGE) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null) {
					Uri uri = data.getData();
					Uri convertUri = convertImage(ElseUtils.getPath(this, uri));
					uploadFile(convertUri);
				}
			}
		}
	}

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			/* Default Value */
			if("Invest".equals(mValue)){
				mCurrentPageInx = 2;
			}
			else {
				mCurrentPageInx = 0;
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void pageScrolling(int _Page, int _Color){
		if (_Color != 0) {
			mDrawerLayout.setBackgroundColor(_Color);
			Header.setBackgroundColor(_Color);
			mTabParent.setBackgroundColor(_Color);
			//mSwipeRefreshLayout.setBackgroundColor(_Color);
			for (int i=0; i < mViewPager.getChildCount(); i++) {
				mViewPager.getChildAt(i).setBackgroundColor(_Color);
			}
		}
	}

	private void onProcessRefresh() {
		if (isScraping) {
			mHandler.postDelayed(mFinishRefresh, 3050);
		} else {
			mHandler.postDelayed(mFinishRefresh, 3050);
		}
	}

	private Runnable mFinishRefresh = new Runnable() {
		@Override
		public void run() {
			mCustomRefresh.finishRefresh();
			showLoadingProgress();
		}
	};

	private void notice_get_list() {
		RequestDataByUserId requestDataByUserId = new RequestDataByUserId();
		requestDataByUserId.setUserId(Session.getInstance(this).getUserId());

		EtcApi api = createService(EtcApi.class);
		Call<List<EtcNoticeInfo>> call = api.listNotice(requestDataByUserId);
		call.enqueue(new Callback<List<EtcNoticeInfo>>() {
			@Override
			public void onResponse(Call<List<EtcNoticeInfo>> call, Response<List<EtcNoticeInfo>> response) {
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						mAlInfoDataList = new ArrayList<EtcNoticeInfo>();
						mAlInfoDataList = response.body();
						mNoticeCount = 0;

						if (mAlInfoDataList != null) {
							for (int i = 0; i < mAlInfoDataList.size(); i++) {
								if ("N".equals(mAlInfoDataList.get(i).getReadYn())) {
									mNoticeCount += 1;
								}
							}
						}

						if (mNoticeCount != 0) {
							((ImageView) findViewById(R.id.btn_pop_menu)).setSelected(true);
							((TextView) findViewById(R.id.tv_navi_main_notice_count)).setVisibility(View.VISIBLE);
							((TextView) findViewById(R.id.tv_navi_main_notice_count)).setText(String.valueOf(mNoticeCount));
						} else {
							((ImageView) findViewById(R.id.btn_pop_menu)).setSelected(false);
							((TextView) findViewById(R.id.tv_navi_main_notice_count)).setVisibility(View.GONE);
						}
					}
				}
			}

			@Override
			public void onFailure(Call<List<EtcNoticeInfo>> call, Throwable t) {

			}
		});
	}

	private void showLoadingProgress() {
		if (isScraping) {
			isLoadingShowing = true;
			hideScrapingTime();
			mLoadingLayout.setVisibility(View.VISIBLE);
			mLoadingLayout.invalidate();
			AnimationDrawable frameAnimation = (AnimationDrawable) mLoading.getDrawable();
			frameAnimation.setCallback(mLoading);
			frameAnimation.setVisible(true, true);
			frameAnimation.start();
		}
	}

	private void hideLoadingProgress() {
		AnimationDrawable frameAnimation = (AnimationDrawable) mLoading.getDrawable();
		frameAnimation.setCallback(mLoading);
		frameAnimation.setVisible(false, false);
		frameAnimation.stop();
		mLoadingLayout.setVisibility(View.GONE);
		if (isLoadingShowing) {
			isLoadingShowing = false;
			showLatestScrapingTime();
		}
	}

	private void showLatestScrapingTime() {
		String time = mAPPShared.getPrefString(APP.SP_CHECK_SCRAPING_DATA_TIME);
		String syncTime = "최근 업데이트" + " " + time;
		if (time != null && !TextUtils.isEmpty(time)) {
			mScrapingTime.setVisibility(View.VISIBLE);
			mScrapingTime.setText(syncTime);
		} else {
			mScrapingTime.setVisibility(View.GONE);
			mScrapingTime.setText(syncTime);
		}
//		Handler handler = new Handler();
//		handler.postDelayed(new Runnable() {
//			@Override
//			public void run() {
//				mScrapingTime.setVisibility(View.GONE);
//			}
//		}, 5000);
	}

	private void hideScrapingTime() {
		mScrapingTime.setVisibility(View.GONE);
	}

	private void checkScrapState(ArrayList<ScrapRequester> list) {
		if (list.size() > 0) {
			isScraping = true;
			showLoadingProgress();
		} else {
			isScraping = false;
			hideLoadingProgress();
		}
	}

	private void checkScarpResult() {
		if (!isScraping && !Session.getInstance(this).isLock()) {
			final ProcessMode mode = ProcessMode.COLLECT_DAILY;
			List<ServiceRecord> list = ServiceRecordDao.getInstance().getSelectedModeData(this, mode.name());
			List<ServiceRecord> list_individual = ServiceRecordDao.getInstance().getSelectedModeData(this, ProcessMode.COLLECT_INDIVIDUAL.name());

			List<ServiceRecord> mergeList = new ArrayList<>();
			mergeList.addAll(list);
			for (int i = 0; i < list_individual.size(); i++) {
				String name = list_individual.get(i).getName();
				boolean included = false;
				for (ServiceRecord target : list) {
					if (target.getName().equalsIgnoreCase(name)) {
						included = true;
						String recordTime = list_individual.get(i).getTime();
						String targetTime = target.getTime();
						if (Long.valueOf(recordTime) > Long.valueOf(targetTime)) {
							mergeList.set(i, list_individual.get(i));
						}
					}
				}
				if (!included) {
					mergeList.add(list_individual.get(i));
				}
			}

			HashMap<Integer, Boolean> checkMap = new HashMap<>();
			int errorCount = 0;

			List<ServiceRecord> successList = new ArrayList<>();
			final List<ServiceRecord> tryAgainList = new ArrayList<>();
			final List<ServiceRecord> networkFailList = new ArrayList<>();
			List<ServiceRecord> authFailList = new ArrayList<>();
			List<ServiceRecord> siteRenewList = new ArrayList<>();
			if (mergeList != null) {
				for (ServiceRecord record : mergeList) {
					String recordCount = record.getCount();
					if (recordCount != null && !recordCount.isEmpty()) {
						int displayTime = Integer.parseInt(recordCount);
						if (displayTime == 0) {
							displayTime++;
							record.setCount(String.valueOf(displayTime));
							ServiceRecordDao.getInstance().updateData(this, record);
							switch (record.getResult()) {
								case ScrapResult.COMPLETE:
									successList.add(record);
									break;
								case ScrapResult.FAIL:
								case ScrapResult.FAIL_TIMEOUT:
								case ScrapResult.FAIL_MISSING:
								case ScrapResult.FAIL_UNKNOWN:
									tryAgainList.add(record);
									checkMap.put(ScrapResult.FAIL, true);
									errorCount++;
									break;
								case ScrapResult.FAIL_AUTH:
									authFailList.add(record);
									checkMap.put(ScrapResult.FAIL_AUTH, true);
									errorCount++;
									break;
								case ScrapResult.FAIL_RENEWAL:
									siteRenewList.add(record);
									checkMap.put(ScrapResult.FAIL_RENEWAL, true);
									errorCount++;
									break;
								case ScrapResult.FAIL_NETWORK:
									networkFailList.add(record);
									checkMap.put(ScrapResult.FAIL_NETWORK, true);
									errorCount++;
									break;
								case ScrapResult.FAIL_IDPWD_ERROR:
									break;
							}
						}
					}
				}
			}



			int requestCode = 100;
			if (checkMap.size() > 1) {
				String title = getString(R.string.dialog_scrap_error_title);
				String str = "총 "+errorCount+"개의 오류가 발견되었습니다.";
				DialogUtils.showScrapingDialog(this, requestCode++, title, str, AlertDialogActivity.TYPE_FAIL_MULTI, mode.ordinal(), null);
			} else {
				if (authFailList.size() > 0) {
					String title = getString(R.string.dialog_scrap_error_auth_fail_title);
					String str = getString(R.string.common_warning_auth_not_reg);
					DialogUtils.showScrapingDialog(this, requestCode++, title, str, AlertDialogActivity.TYPE_AUTH_FAIL, mode.ordinal(), (ArrayList<ServiceRecord>) authFailList);
				}
				if (siteRenewList.size() > 0) {
					String title = getString(R.string.dialog_scrap_error_site_renew_title);
					String str = getString(R.string.common_warning_site_renew);
					DialogUtils.showScrapingDialog(this, requestCode++, title, str, AlertDialogActivity.TYPE_FAIL_RETRY, mode.ordinal(), (ArrayList<ServiceRecord>) siteRenewList);
				}
				Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
					@Override
					public void acquired(String key) {
						int requestCode = 200;
						ArrayList<BankRequestData> retryList = null;
						if (tryAgainList.size() > 0) {
							String str = "브로콜리에서 은행 데이터를 조회 중 "+tryAgainList.size()+"개의 조회가 실패하였습니다.\n" +
									"연동 금융 기관 페이지에서 다시 한 번 시도하여 주세요.";
							DialogUtils.showScrapingDialog(getApplicationContext(), requestCode++, null, str, AlertDialogActivity.TYPE_FAIL_RETRY, mode.ordinal(), (ArrayList<ServiceRecord>) tryAgainList);
						}
						if (networkFailList.size() > 0) {
							String str = "네트워크 연결이 되지 않아 금융정보조회에 실패 하였습니다. 연동 금융 기관 페이지에서 다시 한 번 시도하여 주세요.";
							DialogUtils.showScrapingDialog(getApplicationContext(), requestCode++, null, str, AlertDialogActivity.TYPE_FAIL_RETRY, mode.ordinal(), (ArrayList<ServiceRecord>) networkFailList);
						}
					}

					@Override
					public void error(ErrorMessage errorMessage) {
						switch (errorMessage) {
							case ERROR_713_NO_SUCH_USER:
							case ERROR_10007_SESSION_NOT_FOUND:
							case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
							case ERROR_20003_MISMATCH_ACCESS_TOKEN:
							case ERROR_20004_NO_SUCH_USER:
								showLoginActivity();
								break;
						}
					}

					@Override
					public void fail() {

					}
				});
			}
		}
	}


	private void initCheckScarpResult() {
		if (!Session.getInstance(this).isLock()) {
			final ProcessMode mode = ProcessMode.COLLECT_FIRST_THIS_MONTH;
			List<ServiceRecord> list = ServiceRecordDao.getInstance().getSelectedModeData(this, mode.name());
			HashMap<Integer, Boolean> checkMap = new HashMap<>();
			int errorCount = 0;

			List<ServiceRecord> successList = new ArrayList<>();
			final List<ServiceRecord> tryAgainList = new ArrayList<>();
			final List<ServiceRecord> networkFailList = new ArrayList<>();
			List<ServiceRecord> authFailList = new ArrayList<>();
			List<ServiceRecord> siteRenewList = new ArrayList<>();
			if (list != null) {
				for (ServiceRecord record : list) {
					String recordCount = record.getCount();
					if (recordCount != null && !recordCount.isEmpty()) {
						int displayTime = Integer.parseInt(recordCount);
						if (displayTime == 0) {
							displayTime++;
							record.setCount(String.valueOf(displayTime));
							ServiceRecordDao.getInstance().updateData(this, record);
							switch (record.getResult()) {
								case ScrapResult.COMPLETE:
									successList.add(record);
									break;
								case ScrapResult.FAIL:
								case ScrapResult.FAIL_TIMEOUT:
								case ScrapResult.FAIL_MISSING:
								case ScrapResult.FAIL_UNKNOWN:
									tryAgainList.add(record);
									checkMap.put(ScrapResult.FAIL, true);
									errorCount++;
									break;
								case ScrapResult.FAIL_AUTH:
									authFailList.add(record);
									checkMap.put(ScrapResult.FAIL_AUTH, true);
									errorCount++;
									break;
								case ScrapResult.FAIL_RENEWAL:
									siteRenewList.add(record);
									checkMap.put(ScrapResult.FAIL_RENEWAL, true);
									errorCount++;
									break;
								case ScrapResult.FAIL_NETWORK:
									networkFailList.add(record);
									checkMap.put(ScrapResult.FAIL_NETWORK, true);
									errorCount++;
									break;
							}
						}
					}
				}
			}

			if (mode == ProcessMode.COLLECT_RETRY) {
				return;
			}

			int requestCode = 100;

			if (checkMap.size() > 1) {
				String title = getString(R.string.dialog_scrap_error_title);
				String str = "총 "+errorCount+"개의 오류가 발견되었습니다.\n연동 금융 기관 페이지에서 다시 한 번 시도하여 주세요.";
				DialogUtils.ServiceDialogShow(this, requestCode++, title, str, AlertDialogActivity.TYPE_FAIL_MULTI, mode.ordinal(), null);
			} else {
				if (authFailList.size() > 0) {
					String title = getString(R.string.dialog_scrap_error_auth_fail_title);
					String str = getString(R.string.common_warning_auth_not_reg);
					DialogUtils.ServiceDialogShow(this, requestCode++, title, str, AlertDialogActivity.TYPE_AUTH_FAIL, mode.ordinal(), (ArrayList<ServiceRecord>) authFailList);
				}
				if (siteRenewList.size() > 0) {
					String title = getString(R.string.dialog_scrap_error_site_renew_title);
					String str = getString(R.string.common_warning_site_renew);
					DialogUtils.ServiceDialogShow(this, requestCode++, title, str, AlertDialogActivity.TYPE_FAIL_RETRY, mode.ordinal(), (ArrayList<ServiceRecord>) siteRenewList);
				}
				Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
					@Override
					public void acquired(String key) {
						int requestCode = 200;
						ArrayList<BankRequestData> retryList = null;
						if (tryAgainList.size() > 0) {
							String str = "브로콜리에서 은행 데이터를 조회 중 "+tryAgainList.size()+"개의 조회가 실패하였습니다.\n" +
									"연동 금융 기관 페이지에서 다시 한 번 시도하여 주세요.";
							DialogUtils.ServiceDialogShow(getApplicationContext(), requestCode++, null, str, AlertDialogActivity.TYPE_FAIL_RETRY, mode.ordinal(), (ArrayList<ServiceRecord>) tryAgainList);
						}
						if (networkFailList.size() > 0) {
							String str = "네트워크 연결이 되지 않아 금융정보조회에 실패 하였습니다. 연동 금융 기관 페이지에서 다시 한 번 시도하여 주세요.";
							DialogUtils.ServiceDialogShow(getApplicationContext(), requestCode++, null, str, AlertDialogActivity.TYPE_FAIL_RETRY, mode.ordinal(), (ArrayList<ServiceRecord>) networkFailList);
						}
					}

					@Override
					public void error(ErrorMessage errorMessage) {
						switch (errorMessage) {
							case ERROR_713_NO_SUCH_USER:
							case ERROR_10007_SESSION_NOT_FOUND:
							case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
							case ERROR_20003_MISMATCH_ACCESS_TOKEN:
							case ERROR_20004_NO_SUCH_USER:
								showLoginActivity();
								break;
						}
					}

					@Override
					public void fail() {

					}
				});
			}
		}
	}

	private void checkCertValidDate() {
		Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
			@Override
			public void acquired(String str) {
				ArrayList<BankData> expireCertList = new ArrayList<BankData>();
				List<BankData> list = ScrapDao.getInstance().getBankData(getApplicationContext(), str, Session.getInstance(getApplicationContext()).getUserId());
				for (BankData data : list) {
					String valid = data.getValidDate();
					if (ElseUtils.isExpireDate(valid)) {
						expireCertList.add(data);
					}
				}
			}

			@Override
			public void error(ErrorMessage errorMessage) {
				switch (errorMessage) {
					case ERROR_713_NO_SUCH_USER:
					case ERROR_10007_SESSION_NOT_FOUND:
					case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
					case ERROR_20003_MISMATCH_ACCESS_TOKEN:
					case ERROR_20004_NO_SUCH_USER:
						showLoginActivity();
						break;
				}
			}

			@Override
			public void fail() {

			}
		});
	}

	private void initBinderService() {
		mServiceConnection = new ServiceConnection() {

			@Override
			public void onServiceDisconnected(ComponentName name) {
				mService = null;
				isConnection = false;
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				isConnection = true;
				mService = new Messenger(service);
				registerMessenger();
				requestQueueList();
			}
		};

		if(ElseUtils.isServiceRunning(this, BatchService.class.getName()) == false) {
			ComponentName compName = new ComponentName(getPackageName(), BatchService.class.getName());
			startService(new Intent().setComponent(compName));
		}
		bindService(new Intent(this, BatchService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	private void registerMessenger() {
		if (isConnection) {
			try {
				// Give it some value as an example.
				Message msg = Message.obtain(null,
						BatchService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	private void requestQueueList() {
		if (isConnection) {
			try {
				// Give it some value as an example.
				Message msg = Message.obtain(null,
						BatchService.MSG_REQUEST_QUEUE, this.hashCode(), 0);
				mService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	/** Binder Service End **/
	private void finishBinderService() {
		if (isConnection) {
			if (mService != null) {
				try {
					Message msg = Message.obtain(null,
							BatchService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

			unbindService(mServiceConnection);
			isConnection = false;
		}
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		mDrawerLayout.closeDrawer(GravityCompat.END);
		return false;
	}

	class IncomingHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case BatchService.MSG_UPLOAD_STATUS:
//                    Log.d("test", "MSG_UPLOAD_STATUS : "+msg.arg1);
					switch (msg.arg1) {
						case ScrapingModule.STATUS_COMPLETE:
							if (mMode == ProcessMode.COLLECT_FIRST_THIS_MONTH) {

							} else {

							}
							break;
						case ScrapingModule.STATUS_PREPARE:
							break;
						case ScrapingModule.STATUS_REQUEST:
							break;
						case ScrapingModule.STATUS_SCRAPING:
							//String message = (String) msg.obj;
							break;
						case ScrapingModule.STATUS_UPLOADING:
							break;
					}
					break;
				case BatchService.MSG_UPLOAD_COMPLETE:
					break;
				case BatchService.MSG_UPLOAD_MODE:
					int mode = msg.arg1;
					ProcessMode[] modes = ProcessMode.values();
					mMode = modes[mode];
//                    Log.d("test", "MSG_UPLOAD_MODE : "+mMode);
					switch (mMode) {
						case COLLECT_FIRST_THIS_MONTH:
							break;
						case COLLECT_FIRST_PAST_MONTH:
						case COLLECT_DAILY:
						case COLLECT_INDIVIDUAL:
						case COLLECT_RETRY:
							break;
						case COLLECT_IDLE:
							break;
					}
					break;
				case BatchService.MSG_REQUEST_QUEUE:
				case BatchService.MSG_BANK_COMPLETE:
					ArrayList<ScrapRequester> list = (ArrayList<ScrapRequester>) msg.obj;
					checkScrapState(list);
					break;
				default:
					super.handleMessage(msg);
					break;
			}
		}
	}


	private void emergency_notice(){
		isNoticeCheck = true;
		DialogUtils.showLoading(this);
		RequestDataByUserId userID = new RequestDataByUserId();
		userID.setUserId(Session.getInstance(this).getUserId());

		UserApi api = createService(UserApi.class);
		Call<List<MainPopupData>> requestMainNotice = api.requestMainNotice(userID);
		requestMainNotice.enqueue(new Callback<List<MainPopupData>>() {
			@Override
			public void onResponse(Call<List<MainPopupData>> call, Response<List<MainPopupData>> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					List<MainPopupData> data = response.body();
					ArrayList<MainPopupData> showList = new ArrayList<MainPopupData>();
					for (int i=0; i < data.size(); i++) {
						MainPopupData popupData = data.get(i);
						if (popupData.reOpenYn) {
							boolean isNeverShowAgain = mAPPShared.getPrefBool(APP.SP_CHECK_MAIN_NOTICE_DATA+popupData.popupId, false);
							if (!isNeverShowAgain) {
								showList.add(popupData);
							}
						} else {
							showList.add(popupData);
						}
					}
					if (showList.size() > 0) {
						for (int i=0; i < showList.size(); i++) {
							MainPopupData mainPopupData = showList.get(i);
							final int id = mainPopupData.popupId;
							DialogUtils.showDlgMainNotieceTwoButton(MainActivity.this, new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {

											} else if (which == APP.DIALOG_BASE_TWO_BTN_CANCEL) {
												mAPPShared.setPrefBool(APP.SP_CHECK_MAIN_NOTICE_DATA+id, true);
											}
											if (!mAPPShared.getPrefBool(APP.SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER, false)){
												showTutorialData();
											} else {
												checkFinancialData();
											}
										}
									},
									new InfoDataDialog(R.layout.dialog_inc_case_em_notice, getResources().getString(R.string.common_not_look_back), getResources().getString(R.string.common_confirm), mainPopupData, true));
						}
					} else {
						if (!mAPPShared.getPrefBool(APP.SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER, false)){
							showTutorialData();
						} else {
							checkFinancialData();
						}
					}
				}
			}

			@Override
			public void onFailure(Call call, Throwable t) {
				DialogUtils.dismissLoading();
			}
		});

	}

	private void showTutorialData() {
		sendTracker(AnalyticsName.IntroRefresh);
		DialogUtils.showDlgTutorialImage(this, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
					mAPPShared.setPrefBool(APP.SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER, true);
					initCheckScarpResult();
                    checkFinancialData();
				}
			}
		});
	}

	private void checkFinancialData() {
        int count = ScrapDao.getInstance().getBankCount(this);
        if (count == 0) {
            if (!fromMembership) {
                CommonDialogTextType dialogTextType = new CommonDialogTextType(this, R.string.dialog_main_financial_data_empty_title,
                        new int[]{R.string.dialog_main_financial_data_empty_cancel, R.string.dialog_main_financial_data_empty_confirm});
                dialogTextType.addTextView(R.string.dialog_main_financial_data_empty_content);
                dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        switch (id) {
                            case R.string.dialog_main_financial_data_empty_confirm:
                                Intent intent = new Intent(MainActivity.this, Level3ActivitySettingFinancialReg.class);
                                startActivity(intent);
                                break;
                            case R.string.dialog_main_financial_data_empty_cancel:
                                break;
                        }
                        dialog.dismiss();
                    }
                });
                dialogTextType.show();
            }
        }
    }

}
