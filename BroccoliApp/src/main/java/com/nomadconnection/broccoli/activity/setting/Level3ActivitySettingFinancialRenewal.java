package com.nomadconnection.broccoli.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.constant.ErrorMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by YelloHyunminJang on 2017. 2. 21..
 */

public class Level3ActivitySettingFinancialRenewal extends BaseActivity {

    private static final int SELECT_NEW_CERT = 1;

    private ListView mList;
    private View mEmptyLayout;
    private AdapterBankDataCertList mAdapterCert;
    private ArrayList<BankData> mCertList = null;
    private BankData mSelectData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_setting_financial_renewal);
        init();
    }

    private void init() {
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView) findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_financial_renewal_title);
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        mEmptyLayout = findViewById(R.id.fl_empty_layout);
        ((TextView) findViewById(R.id.tv_empty_layout_txt)).setText("연동된 공인인증서가 없습니다.");
        mList = (ListView) findViewById(R.id.setting_financial_renewal_list);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSelectData = (BankData) parent.getAdapter().getItem(position);
                startNewCert();
            }
        });
        mAdapterCert = new AdapterBankDataCertList(this, mCertList);
        mList.setAdapter(mAdapterCert);
    }

    private void initData() {
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                getStoreCertList(str);
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {

            }
        });
    }

    private void getStoreCertList(String key) {
        if (mCertList == null)
            mCertList = new ArrayList<>();
        else {
            mCertList.clear();
        }
        ArrayList<String> containerList = new ArrayList<>();
        List<BankData> list = ScrapDao.getInstance().getBankData(getBase(), key, Session.getInstance(getBase()).getUserId());
        for (BankData data : list) {
            if (BankData.LOGIN_CERT.equalsIgnoreCase(data.getLoginMethod()) && !containerList.contains(data.getCertSerial())) {
                containerList.add(data.getCertSerial());
                mCertList.add(data);
            }
        }
        mAdapterCert.setData(mCertList);
        mAdapterCert.notifyDataSetChanged();
        if (mCertList == null || mCertList.size() == 0) {
            mEmptyLayout.setVisibility(View.VISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.GONE);
        }
    }

    private void startNewCert() {
        Intent intent = new Intent(this, Level3ActivitySettingFinancialSelectNewCert.class);
        startActivityForResult(intent, SELECT_NEW_CERT);
    }

    public class AdapterBankDataCertList extends BaseAdapter {

        private Context mContext;
        private ArrayList<BankData> mList;

        public AdapterBankDataCertList(Context context, ArrayList<BankData> list) {
            mContext = context.getApplicationContext();
            setData(list);
        }

        public void setData(ArrayList<BankData> list) {
            mList = list;
        }

        @Override
        public int getCount() {
            int count = 0;
            if (mList != null) {
                count = mList.size();
            }
            return count;
        }

        @Override
        public Object getItem(int position) {
            Object object = null;
            if (mList != null) {
                object = mList.get(position);
            }
            return object;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                View view = LayoutInflater.from(mContext).inflate(R.layout.row_cert_auth, null);
                holder = new ViewHolder();
                holder.mImageView = (ImageView) view.findViewById(R.id.iv_certStatus);
                holder.mErrorIcon = (ImageView) view.findViewById(R.id.iv_certStatusError);
                holder.mSubjectName = (TextView) view.findViewById(R.id.tv_certname);
                holder.mPolicy = (TextView) view.findViewById(R.id.tv_cert_policy);
                holder.mIssuerName = (TextView) view.findViewById(R.id.tv_cert_issuername);
                holder.mExpireTime = (TextView) view.findViewById(R.id.tv_cert_expire_time);
                view.findViewById(R.id.middle_layout).setVisibility(View.GONE);
                view.setTag(holder);
                convertView = view;
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String name = mList.get(position).getCertSerial();
            boolean isLowerCase = false;
            if (name != null) {
                isLowerCase = name.contains("cn=");
            }
            String[] splitName = isLowerCase ? name.split("cn=") : name.split("CN=");

            if (splitName.length > 1) {
                holder.mSubjectName.setText(splitName[1]);
            }
            String expire = mList.get(position).getValidDate();
            holder.mExpireTime.setText("만료일 : " + (expire == null ? "":expire));
            holder.mErrorIcon.setVisibility(View.INVISIBLE);

            return convertView;
        }

        private class ViewHolder {
            private ImageView mImageView;
            private ImageView mErrorIcon;
            private TextView mSubjectName;
            private TextView mPolicy;
            private TextView mIssuerName;
            private TextView mExpireTime;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_NEW_CERT && resultCode == RESULT_OK) {
            if (data != null) {
                String id = data.getStringExtra(Const.CERT_NAME);
                String pwd = data.getStringExtra(Const.CERT_PWD);
                String expire = data.getStringExtra(Const.CERT_VALIDDATE);
                setCertComplete(id, pwd, expire);
            }
        }
    }

    private void setCertComplete(final String cert, final String pwd, final String expire) {
        DialogUtils.showLoading(this);
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                if (mSelectData != null) {
                    ArrayList<BankData> bankList = new ArrayList<BankData>();
                    List<BankData> list = ScrapDao.getInstance().getBankData(getBase(), str, Session.getInstance(getBase()).getUserId());
                    for (BankData data : list) {
                        if (BankData.LOGIN_CERT.equalsIgnoreCase(data.getLoginMethod()) && data.getCertSerial().equalsIgnoreCase(mSelectData.getCertSerial())) {
                            bankList.add(data);
                        }
                    }
                    for (int i=0; i < bankList.size(); i++) {
                        BankData target = bankList.get(i);
                        target.setCertSerial(cert);
                        target.setPassWord(pwd);
                        target.setValidDate(expire);
                    }
                    if (list.size() > 0) {
                        String id = Session.getInstance(getBase()).getUserId();
                        for (BankData data : bankList) {
                            ScrapDao.getInstance().updateBankData(getBase(), str, id, data);
                        }
                    }
                    startBackgroundService(bankList);
                    DialogUtils.dismissLoading();
                    finish();
                }
                mSelectData = null;
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
                finish();
            }
        });

    }

    private void startBackgroundService(ArrayList<BankData> list) {
        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_INDIVIDUAL);
        intent.putExtra(Const.DATA, list);
        sendBroadcast(intent);
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

}
