package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetStockInfo;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockInfo;
import com.nomadconnection.broccoli.data.Asset.EditAssetStock;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.fragment.FragmentAssetStock0;
import com.nomadconnection.broccoli.fragment.FragmentAssetStock1;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorCode;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;


public class Level2ActivityAssetStock extends BaseActivity implements InterfaceEditOrDetail {

	private static final String TAG = Level2ActivityAssetStock.class.getSimpleName();
	private static final String TAG0 = FragmentAssetStock0.class.getSimpleName();
	private static final String TAG1 = FragmentAssetStock1.class.getSimpleName();

	public static int mCurrentFragmentIndex 	= 0;	// default
	private final int FRAGMENT_ONE 			= 0;		// 추가
	private final int FRAGMENT_TWO 			= 1;		// 상세

	/* Layout */
	private Animation mAnimTitle;

	/* getParcelableExtra */
	private ArrayList<BodyAssetStockInfo> mAlInfoDataList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_asset_stock);
		sendTracker(AnalyticsName.Level2ActivityAssetStock);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		onBtnClickNaviMotion(findViewById(R.id.ll_navi_motion_back));
//		super.onBackPressed();
	}


	//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try
		{
			switch (_view.getId()) {

				case R.id.tv_navi_motion_title:
					if (mCurrentFragmentIndex == 0)
						break;
					
				case R.id.ll_navi_motion_back:
					if (mCurrentFragmentIndex == 1) goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_MAIN_1_RESULT_CODE_SUCCESS);
					else if (mCurrentFragmentIndex == 0) {
						String mStr = "매수 내역 추가를\n취소하시겠습니까?";
						DialogUtils.showDlgBaseTwoButton(Level2ActivityAssetStock.this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
											if (mAlInfoDataList.size() == 0) goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_MAIN_1_RESULT_CODE_SUCCESS);
											else {mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");}
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
//						mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentFragmentIndex == 1) { mCurrentFragmentIndex = 0; fragmentReplace(mCurrentFragmentIndex, "in_right"); afterMotion("else"); }
					else if (mCurrentFragmentIndex == 0) {
						if (checkValueIsFail()) return;
						DialogUtils.showLoading(Level2ActivityAssetStock.this);											// 자산 로딩
						final Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).editStock(
								new EditAssetStock(Session.getInstance(this).getUserId(),
										"0",	// 추가
										Integer.valueOf(TransFormatUtils.transStockNameToCode(FragmentAssetStock0.mInfoType.getText().toString())),
										TransFormatUtils.transRemoveDot(FragmentAssetStock0.mInfoDate.getText().toString()),
										FragmentAssetStock0.mStockSearchReturnData.mStockCode,
										TransFormatUtils.transRemoveComma(FragmentAssetStock0.mInfoValue.getText().toString()),
										TransFormatUtils.transRemoveComma(FragmentAssetStock0.mInfoAmount.getText().toString()),
										TransFormatUtils.transRemoveComma(FragmentAssetStock0.mInfoCost.getText().toString()))
						);
						mCall.enqueue(new Callback<Result>() {
							@Override
							public void onResponse(Call<Result> call, Response<Result> response) {
								DialogUtils.dismissLoading();											// 자산 로딩
								if (response.isSuccessful()) {
									if (response.body() != null)
										NextStepAdd(response.body());
								} else {
									Retrofit retrofit = ServiceGenerator.getRetrofit();
									Converter<ResponseBody, Result> converter = retrofit.responseBodyConverter(Result.class, new Annotation[0]);
									try {
										Result result = converter.convert(response.errorBody());
										if (result != null) {
											NextStepAdd(result);
										} else {
											ElseUtils.network_error(Level2ActivityAssetStock.this);
										}
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							}

							@Override
							public void onFailure(Call<Result> call, Throwable t) {
								DialogUtils.dismissLoading();											// 자산 로딩
								ElseUtils.network_error(Level2ActivityAssetStock.this);
								//CustomToast.getInstance(Level2ActivityAssetStock.this).showShort("Level2ActivityAssetStock 서버 통신 실패 - 주식내역 추가");
							}
						});
					}
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}


	private void afterMotion(String _Divider){
		if(_Divider.equals("else")){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG0).mTitle);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("완료");
			complete_button_enable_disable(false);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_04_0_0_400_fill);
		}
		else{	// default
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG1).mTitle);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("추가");
			complete_button_enable_disable(true);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_04_0_0_0_400_fill);
		}
		((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);
	}




//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mAlInfoDataList = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_1);

			if(mAlInfoDataList == null){
				bResult = false;
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG1).mTitle);
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) {
				((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			}
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("추가");
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			if (getIntent().hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
				findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_3_layout_bg));
			}
			/* Layout */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCurrentFragmentIndex = 1;
			if (mAlInfoDataList.size() == 0) ((LinearLayout)findViewById(R.id.ll_navi_motion_else)).callOnClick();
			else fragmentReplace(mCurrentFragmentIndex, "");
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//=========================================================================================//
// Fragment Navi
//=========================================================================================//
	/** Fragment 변경 **/
	private void fragmentReplace(int reqNewFragmentIndex, String _Anim)
	{
		try
		{
			Fragment newFragment = null;
			newFragment = getFragment(reqNewFragmentIndex);

			// replace fragment
			final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			if (_Anim.length() != 0 && _Anim.equals("in_right")) transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out);
			if (_Anim.length() != 0 && _Anim.equals("out_right")) transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
			transaction.replace(R.id.fl_level_2_activity_asset_stock_container, newFragment);

			// Commit the transaction
			transaction.commitAllowingStateLoss();

		}
		catch (Exception e)
		{}
	}

	/** Fragment Get **/
	private Fragment getFragment(int idx)
	{
		Fragment newFragment = null;

		switch (idx) {
			case FRAGMENT_ONE:	// 추가/편집
				newFragment = new FragmentAssetStock0();
				break;

			case FRAGMENT_TWO:	// 상세
				newFragment = new FragmentAssetStock1();
				Bundle bundle = newFragment.getArguments();
				if (bundle == null) {
					bundle = new Bundle();
				}
				bundle.putParcelableArrayList(Const.DATA, mAlInfoDataList);
				newFragment.setArguments(bundle);
				break;

			default:
				break;
		}

		return newFragment;
	}





//===============================================================================//
// Value Check
//===============================================================================//
	/**  Value check **/
	private Boolean checkValueIsFail(){
		boolean mResult = false;

		if (FragmentAssetStock0.mInfoName.getText().toString().equals("선택")){
			Toast.makeText(Level2ActivityAssetStock.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetStock0.mInfoType.getText().toString().equals("선택")){
			Toast.makeText(Level2ActivityAssetStock.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetStock0.mInfoDate.getText().toString().equals("선택")){
			Toast.makeText(Level2ActivityAssetStock.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetStock0.mInfoValue.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetStock.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			CustomToast.getInstance(Level2ActivityAssetStock.this).showShort("금액을 입력하세요");
			mResult = true;
		}
		else if (FragmentAssetStock0.mInfoAmount.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetStock.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}

		return  mResult;
	}

//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep Add **/
	private void NextStepAdd(Result result){
		if (result.getCode().equals("100")){
			//CustomToast.getInstance(Level2ActivityAssetStock.this).showShort("주식내역 추가 완료");
//			mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
			refreshData();
		} else if (ErrorCode.ERROR_CODE_605_STOCK_MARKET_CLOSED.equalsIgnoreCase(result.getCode())){
			Toast.makeText(Level2ActivityAssetStock.this, getString(R.string.common_stock_add_error), Toast.LENGTH_SHORT).show();
		}
	}


//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int resultCode){
		Intent intent = getIntent();
		setResult(resultCode, intent);
		finish();
	}




//===============================================================================//
// Refresh
//===============================================================================//
	private void refreshData(){
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(this).getUserId());

		DialogUtils.showLoading(Level2ActivityAssetStock.this);											// 자산 로딩
		Call<AssetStockInfo> mCall = ServiceGenerator.createService(AssetApi.class).getStockList(mRequestDataByUserId);
		mCall.enqueue(new Callback<AssetStockInfo>() {
			@Override
			public void onResponse(Call<AssetStockInfo> call, Response<AssetStockInfo> response) {
				DialogUtils.dismissLoading();                                            // 자산 로딩
				AssetStockInfo mTemp = response.body();
				mAlInfoDataList.removeAll(mAlInfoDataList);
				mAlInfoDataList.addAll(mTemp.mAssetStockList);
				mCurrentFragmentIndex = 1;
				fragmentReplace(mCurrentFragmentIndex, "out_right");
				afterMotion("back");
			}

			@Override
			public void onFailure(Call<AssetStockInfo> call, Throwable t) {
				DialogUtils.dismissLoading();                                            // 자산 로딩
				ElseUtils.network_error(Level2ActivityAssetStock.this);
			}
		});
	}




//=========================================================================================//
// Interface
//=========================================================================================//
	@Override
	public void onEdit() {
//		mCurrentFragmentIndex = 0;
//		fragmentReplace(mCurrentFragmentIndex, "");
		complete_button_enable_disable(true);
	}

	@Override
	public void onDetail() {
		mCurrentFragmentIndex = 1;
		fragmentReplace(mCurrentFragmentIndex, "");
	}

	@Override
	public void onError() {
		complete_button_enable_disable(false);
	}

	@Override
	public void onRefresh() {

	}


	private void complete_button_enable_disable(boolean value){
		if(value){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}
}
