package com.nomadconnection.broccoli.activity.remit;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RemitBankAccount;
import com.nomadconnection.broccoli.data.Remit.RemitContent;
import com.nomadconnection.broccoli.fragment.remit.FragmentRemitSend;
import com.nomadconnection.broccoli.fragment.remit.FragmentRemitSendAccount;
import com.nomadconnection.broccoli.fragment.remit.FragmentRemitSendPhone;
import com.nomadconnection.broccoli.interf.InterfaceRemitSend;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.Const;


public class RemitSendActivity extends BaseActivity implements InterfaceRemitSend {
	private static final String TAG = RemitSendActivity.class.getSimpleName();

	public static int mCurrentFragmentIndex = 0;	// default
	private final int FRAGMENT_ONE 		= 0;		// 전화번호
	private final int FRAGMENT_TWO 		= 1;		// 송금하기
	private final int FRAGMENT_THREE	= 2;		// 계좌번호

	private enum Page { REMIT_SEND_PHONE, REMIT_SEND_ACCOUNT, CHARGE_MONEY};

	/* Layout */
	private Animation mAnimTitle;
	private int mPageStatus;
	private RemitBankAccount mRemitBankAccount;
	private RemitContent mRemitContent;
	private String mRemitSendSum = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remit_send);
//		sendTracker(AnalyticsName.Level2ActivitySpendHistory);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if (mCurrentFragmentIndex == 1) {
				finish();
			}
			else if (mCurrentFragmentIndex ==2) {
				keyboard_hide();
				mCurrentFragmentIndex = 1;
				fragmentReplace(mCurrentFragmentIndex, "out_right");
				afterMotion("back");
			}
			else if (mCurrentFragmentIndex == 0) {
				keyboard_hide();
				mCurrentFragmentIndex = 1;
				fragmentReplace(mCurrentFragmentIndex, "out_right");
				afterMotion("back");
			}
		}
		return super.onKeyDown(keyCode, event);
	}

//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try
		{
			switch (_view.getId()) {

				case R.id.tv_navi_motion_title:
				case R.id.ll_navi_motion_back:
					if (mCurrentFragmentIndex == 1) {
						finish();
					}
					else if (mCurrentFragmentIndex ==2) {
						keyboard_hide();
						mCurrentFragmentIndex = 1;
						fragmentReplace(mCurrentFragmentIndex, "out_right");
						afterMotion("back");
					}
					else if (mCurrentFragmentIndex == 0) {
						keyboard_hide();
						mCurrentFragmentIndex = 1;
						fragmentReplace(mCurrentFragmentIndex, "out_right");
						afterMotion("back");
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentFragmentIndex == 1) { mCurrentFragmentIndex = 0; fragmentReplace(mCurrentFragmentIndex, "in_right"); afterMotion("else"); }
					else if (mCurrentFragmentIndex == 2) { mCurrentFragmentIndex = 0; fragmentReplace(mCurrentFragmentIndex, "in_right"); afterMotion("else");}
					else if (mCurrentFragmentIndex == 0) {
//						((FragmentSpendHistoryAdd) getSupportFragmentManager().findFragmentByTag(String.valueOf(mCurrentFragmentIndex))).onComplete();
					}
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}


	private void afterMotion(String _Divider){
		if(_Divider.equals("else_1")){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.remit_phone_number));
			((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_03_0_0_400_fill);
		}
		else if(_Divider.equals("else_2")){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.remit_account_number));
			((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_03_0_0_400_fill);
		}
		else{	// default
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.remit_service));
			((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_03_0_0_0_400_fill);
		}
		((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);
	}



	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



	//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(R.color.main_4_layout_bg);
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.remit_service));
			((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);

			/* Layout */

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




	//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCurrentFragmentIndex = 1;
			fragmentReplace(mCurrentFragmentIndex, "");
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// keyboard hide
//===============================================================================//
	private void keyboard_hide() {
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}


//=========================================================================================//
// Fragment Navi
//=========================================================================================//
	/** Fragment 변경 **/
	private void fragmentReplace(int reqNewFragmentIndex, String _Anim)
	{
		try
		{
			Fragment newFragment = null;
			newFragment = getFragment(reqNewFragmentIndex);

			// replace fragment
			final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			if (_Anim.length() != 0 && _Anim.equals("in_right")) transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out);
			if (_Anim.length() != 0 && _Anim.equals("out_right")) transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
			transaction.replace(R.id.fl_activity_remit_send_container, newFragment, String.valueOf(reqNewFragmentIndex));

			// Commit the transaction
			transaction.commitAllowingStateLoss();

		}
		catch (Exception e)
		{}
	}

	/** Fragment Get **/
	private Fragment getFragment(int idx)
	{
		Fragment newFragment = null;

		switch (idx) {
			case FRAGMENT_ONE:	// 전화번호
				newFragment = new FragmentRemitSendPhone();
				break;

			case FRAGMENT_TWO:	// 송금하기
				Bundle bundle = new Bundle();
				newFragment = new FragmentRemitSend();
				if(mPageStatus == APP.REMIT_SEND_PHONE){
					bundle.putSerializable(Const.DATA, mRemitContent);
					bundle.putInt(Const.REMIT_PAGE_STATUS, APP.REMIT_SEND_PHONE);
					bundle.putString(Const.REMIT_SEND_SUM, mRemitSendSum);
					newFragment.setArguments(bundle);
				}
				else if(mPageStatus == APP.REMIT_SEND_ACCOUNT) {
					bundle.putSerializable(Const.DATA, mRemitBankAccount);
					bundle.putInt(Const.REMIT_PAGE_STATUS, APP.REMIT_SEND_ACCOUNT);
					bundle.putString(Const.REMIT_SEND_SUM, mRemitSendSum);
					newFragment.setArguments(bundle);
				}
				else {
					newFragment.setArguments(bundle);
				}
				break;

			case FRAGMENT_THREE: // 계좌번호
				newFragment = new FragmentRemitSendAccount();
				break;

			default:
				break;
		}

		return newFragment;
	}



//=========================================================================================//
// Interface
//=========================================================================================//
	@Override
	public void onRemitSendPhone(RemitContent remitContent) {
		mRemitContent = remitContent;
		mCurrentFragmentIndex = 1;
		fragmentReplace(mCurrentFragmentIndex, "out_right");
		afterMotion("back");
	}

	@Override
	public void onRemitSendAccount(RemitBankAccount remitBankAccount) {
		mRemitBankAccount = remitBankAccount;
		mCurrentFragmentIndex = 1;
		fragmentReplace(mCurrentFragmentIndex, "out_right");
		afterMotion("back");
	}

	@Override
	public void onRemitSendSet(int value , String sum) {
		mPageStatus = value;
		mRemitSendSum = sum;
		if(mPageStatus == APP.REMIT_SEND_PHONE){ //전화번호 송금
			mCurrentFragmentIndex = 0;
			fragmentReplace(mCurrentFragmentIndex, "in_right");
			afterMotion("else_1");
		}
		else if(mPageStatus == APP.REMIT_SEND_ACCOUNT) { //계좌번호 송금
			mCurrentFragmentIndex = 2;
			fragmentReplace(mCurrentFragmentIndex, "in_right");
			afterMotion("else_2");
		}
		else {

		}
	}
}
