package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.Const;

/**
 * Created by YelloHyunminJang on 16. 2. 4..
 */
public class Level4ActivitySettingCommonTerm extends BaseActivity {

    private WebView mWebView;
    private String mUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_setting_term);
        ((RelativeLayout) findViewById(R.id.rl_navi_img_bg)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        Intent intent = getIntent();
        String title = intent.getStringExtra(Const.TITLE);
        String url = intent.getStringExtra(Const.CONTENT);
        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(title);

        mWebView = (WebView) findViewById(R.id.wb_level4_activity_setting_term_view);

        // 웹뷰에서 자바스크립트실행가능
        mWebView.getSettings().setJavaScriptEnabled(true);
        // get url
        mUrl = url;
        mWebView.loadUrl(mUrl);
        // WebViewClient 지정
        mWebView.setWebViewClient(new WebViewClientClass());
    }

    public void onBtnClickNaviImgTxtBar(View _view) {
        try {
            switch (_view.getId()) {

                case R.id.ll_navi_img_txt_back:
                    finish();
                    break;

                default:
                    break;
            }
        } catch (Exception e) {
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
        }
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
