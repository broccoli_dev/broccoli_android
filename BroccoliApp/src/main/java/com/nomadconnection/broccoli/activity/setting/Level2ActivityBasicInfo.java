package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.DialogUtils;


public class Level2ActivityBasicInfo extends BaseActivity {

	private static final String TAG = Level2ActivityBasicInfo.class.getSimpleName();

	/* Layout */
	private LinearLayout mUserInfo;
	private TextView mUserEmail;
	private View mUserNotCertWarning;
	private View mUserReqCert;
	private LinearLayout mUserPw;
	private LinearLayout mCode;
	private LinearLayout mPinPw;

	/**  Button Click Listener **/
	private OnClickListener mBtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.ll_activity_setting_dayli_user_info:
				startActivity(new Intent(Level2ActivityBasicInfo.this, Level3ActivitySettingAccount.class));
				break;

			case R.id.ll_activity_setting_dayli_password:
				startActivity(new Intent(Level2ActivityBasicInfo.this, Level3SettingDayliPassChange.class));
				break;

			case R.id.ll_activity_setting_pin_password:
				startActivity(new Intent(Level2ActivityBasicInfo.this, Level3ActivitySettingPassword.class));
				break;

			case R.id.ll_activity_setting_input_code:
				startActivity(new Intent(Level2ActivityBasicInfo.this, Level3SettingInputCode.class));
				break;
			case R.id.tv_activity_setting_dayli_user_cert_btn:
				startActivity(new Intent(Level2ActivityBasicInfo.this, Level3SettingEmailCert.class));
				break;

			default:
				break;
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_basic_info);
		sendTracker(AnalyticsName.SettingRegList);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	//=========================================================================================//
// ToolBar - Image/Text/Text
//=========================================================================================//

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		initData();
	}

	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {

			case R.id.ll_navi_img_txt_back:
				finish();
				break;

			default:
				break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//	

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.setting_user_info));
			findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));

			/* Layout */
			mUserInfo = (LinearLayout)findViewById(R.id.ll_activity_setting_dayli_user_info);
			mUserEmail = (TextView) findViewById(R.id.tv_activity_setting_dayli_cert);
			mUserNotCertWarning = findViewById(R.id.ll_activity_setting_dayli_warnig);
			mUserReqCert = findViewById(R.id.rl_activity_setting_dayli_user_cert);
			mUserPw = (LinearLayout)findViewById(R.id.ll_activity_setting_dayli_password);
			mPinPw = (LinearLayout)findViewById(R.id.ll_activity_setting_pin_password);
			mCode = (LinearLayout)findViewById(R.id.ll_activity_setting_input_code);

			DialogUtils.showLoading(this);
			Session.getInstance(this).refreshUserPersonalInfo(new Session.OnUserInfoUpdateCompleteInterface() {
				@Override
				public void complete(UserPersonalInfo info) {
					DialogUtils.dismissLoading();
					showUserEmail(info);
				}

				@Override
				public void error(ErrorMessage message) {
					switch (message) {
						case ERROR_713_NO_SUCH_USER:
						case ERROR_10007_SESSION_NOT_FOUND:
						case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
						case ERROR_20003_MISMATCH_ACCESS_TOKEN:
						case ERROR_20004_NO_SUCH_USER:
							showTokenExpire();
							break;
					}
				}
			});

			mUserInfo.setOnClickListener(mBtnClickListener);
			mUserPw.setOnClickListener(mBtnClickListener);
			mPinPw.setOnClickListener(mBtnClickListener);
			mCode.setOnClickListener(mBtnClickListener);
			findViewById(R.id.tv_activity_setting_dayli_user_cert_btn).setOnClickListener(mBtnClickListener);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void showUserEmail(UserPersonalInfo info) {
		if (info != null) {
			mUserEmail.setText(info.user.email);
			if (info.user.emailCertYn) {
				mUserEmail.setTextColor(getResources().getColor(R.color.common_subtext_color));
				mUserNotCertWarning.setVisibility(View.GONE);
				mUserReqCert.setVisibility(View.GONE);
			} else {
				mUserEmail.setTextColor(getResources().getColor(R.color.common_warning_color));
				mUserNotCertWarning.setVisibility(View.VISIBLE);
				mUserReqCert.setVisibility(View.VISIBLE);
			}
		}
	}

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}
}
