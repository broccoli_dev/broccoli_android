package com.nomadconnection.broccoli.activity.spend;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetData;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 2. 28..
 */

public class Level3ActivitySpendBudgetDetailCategorySetting extends BaseActivity {


    private TextView mMonthlyBudgetTitle;
    private TextView mMonthlySpendTitle;
    private TextView mMonthlyBudget;
    private TextView mMonthlySpend;
    private LinearLayout mGraphCurrent;
    private LinearLayout mGraphLeft;
    private TextView mCurrentStatus;

    private String mSelectYearMonth;
    private String mSelectMonth;
    private List<SpendBudgetData> mDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_spend_budget);
        init();
    }

    private void init() {
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_budget);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
        mMonthlyBudgetTitle = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_budget_title);
        mMonthlySpendTitle = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_spend_title);
        mMonthlyBudget = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_budget);
        mMonthlySpend = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_spend);
        mGraphCurrent = (LinearLayout) findViewById(R.id.ll_spend_budget_current_graph_amount);
        mGraphLeft = (LinearLayout) findViewById(R.id.ll_spend_budget_left_graph_amount);
        mCurrentStatus = (TextView) findViewById(R.id.tv_spend_budget_current_status);
//        mListView = (ListView) findViewById(R.id.lv_fragment_spend_budget_monthly);
//        mAdapter = new AdtSpendBudgetLookup(getContext());
    }

    private void initData() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
        Calendar calendar = Calendar.getInstance();
        mSelectYearMonth = dateFormat.format(calendar.getTime());
        mSelectMonth = mSelectYearMonth.substring(4);

        String budget = String.format(getResources().getString(R.string.spend_budget_total_budget_month), mSelectMonth);
        String spend = String.format(getResources().getString(R.string.spend_budget_total_expense_month), mSelectMonth);
        mMonthlyBudgetTitle.setText(budget);
        mMonthlySpendTitle.setText(spend);
        getData();
    }

    private void getData() {
        DialogUtils.showLoading(getActivity()); 	//소비 로딩 추가

        Call<JsonElement> call = ServiceGenerator.createService(SpendApi.class).getBudgetList();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                JsonObject object = element.getAsJsonObject();
                JsonArray array = object.getAsJsonArray("budgetList");
                if (array != null) {
                    Type listType = new TypeToken<List<SpendBudgetData>>() {}.getType();
                    mDataList = new Gson().fromJson(array, listType);
                    Collections.reverse(mDataList);
                    setData(mDataList);
                } else {
                    ErrorUtils.parseError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading(); 	//소비 로딩 추가
                ElseUtils.network_error(getActivity());
            }
        });
    }

    private void setData(List<SpendBudgetData> data) {

    }

    private void showTargetMonth() {
        SpendBudgetData targetData = null;
        if (mDataList != null) {
            for (int i=0; i < mDataList.size(); i++) {
                SpendBudgetData budgetData = mDataList.get(i);
                if (budgetData != null) {
                    if (budgetData.baseMonth.equalsIgnoreCase(mSelectYearMonth)) {
                        targetData = budgetData;
                        break;
                    }
                }
            }

            String budget = String.format(getResources().getString(R.string.spend_budget_total_budget_month), mSelectMonth);
            String spend = String.format(getResources().getString(R.string.spend_budget_total_expense_month), mSelectMonth);
            mMonthlyBudgetTitle.setText(budget);
            mMonthlySpendTitle.setText(spend);
            mMonthlyBudget.setText(ElseUtils.getDecimalFormat(targetData.totalSum));
            mMonthlySpend.setText(ElseUtils.getDecimalFormat(targetData.totalExpense));
            long currentStatus = targetData.totalSum - targetData.totalExpense;
            String string = "남음";
            if (currentStatus < 0) {
                string = "초과";
            }
            mCurrentStatus.setText(ElseUtils.getDecimalFormat(Math.abs(currentStatus))+" "+string);

            Animation animation = new AlphaAnimation(0.2f, 1);
            animation.setDuration(200);

            long ratio = (long)( (double)targetData.totalExpense / (double)targetData.totalSum * 100.0 );
            if (ratio > 999) {
                ratio = 999;
            }

            if (targetData.totalExpense == 0) {
                mGraphCurrent.setVisibility(View.GONE);
                LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) mGraphLeft.getLayoutParams();
                amountParams.weight = 1;
                mGraphLeft.setLayoutParams(amountParams);
            } else {
                mGraphCurrent.setVisibility(View.VISIBLE);
                mGraphCurrent.setAnimation(animation);
                float spendValue = 0f;
                if (targetData.totalSum != 0) {
                    spendValue = (float) ((double)targetData.totalExpense/(double)targetData.totalSum);
                } else {
                    spendValue = 1f;
                }
                if (spendValue > 1f) {
                    spendValue = 1f;
                }
                float budgetValue = 1f - spendValue;

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraphCurrent.getLayoutParams();
                params.weight = spendValue;
                if (spendValue >= 1f) {
                    params.setMargins(0, 0, 0, 0);
                } else {
                    params.setMargins(0, 0, ElseUtils.convertDp2Px(this, 3), 0);
                }
                mGraphCurrent.setLayoutParams(params);

                LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) mGraphLeft.getLayoutParams();
                amountParams.weight = budgetValue;
                mGraphLeft.setLayoutParams(amountParams);

                Drawable graphLeftBackground = mGraphLeft.getBackground();
                if (graphLeftBackground instanceof ShapeDrawable) {
                    ShapeDrawable shapeDrawable = (ShapeDrawable) graphLeftBackground;
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                } else if (graphLeftBackground instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) graphLeftBackground;
                    gradientDrawable.setColor(getResources().getColor(R.color.common_white));
                }

                if(ratio <= 50){
                    Drawable drawable = mGraphCurrent.getBackground();
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage1_color));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage1_color));
                    }
                } else if(ratio <= 100){
                    Drawable drawable = mGraphCurrent.getBackground();
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage2_color));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage2_color));
                    }
                } else {
                    Drawable drawable = mGraphCurrent.getBackground();
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage3_color));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage3_color));
                    }
                }
            }
        }
    }

}
