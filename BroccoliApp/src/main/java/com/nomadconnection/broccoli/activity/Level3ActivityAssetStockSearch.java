package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewAssetStockSearch;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Asset.ResponseSearchAssetStock;
import com.nomadconnection.broccoli.data.Asset.SearchAssetStock;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level3ActivityAssetStockSearch extends BaseActivity {

	private static final String TAG = Level3ActivityAssetStockSearch.class.getSimpleName();


	/* Layout */
	private FrameLayout mSearchResultLayout;
	private EditText mSearchWord;
	//	private Button mSearchBtn;
	private RelativeLayout mSearchBtn;

	private ListView mListView;
	private AdtListViewAssetStockSearch mAdapter;


	/* DataClass */
	private ArrayList<ResponseSearchAssetStock> mAlInfoDataList;
	/**  Item Click Listener **/
	private AdapterView.OnItemClickListener ItemClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_SUCCESS, mAlInfoDataList.get(position));
		}
	};
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
			switch (v.getId()) {

				case R.id.rl_level_3_activity_asset_stock_search_button:
					doSearch();
					break;

				default:
					break;
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_asset_stock_search);
		sendTracker(AnalyticsName.Level3ActivityAssetStockSearch);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}







//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {

				case R.id.ll_navi_img_txt_back:
					goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_CANCLE, null);
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}

	@Override
	public void onBackPressed() {
		goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_CANCLE, null);
		super.onBackPressed();
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			/* getExtra */
			/* getParcelableExtra */
			mAlInfoDataList = new ArrayList<ResponseSearchAssetStock>();
		} catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//===============================================================================//
// Listener
//===============================================================================//

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitle);
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0)
				((RelativeLayout)findViewById(R.id.rl_navi_img_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);

			/* Layout */
			mSearchResultLayout = (FrameLayout)findViewById(R.id.fl_level_3_activity_asset_stock_search_result_layout);
			mSearchWord = (EditText)findViewById(R.id.et_level_3_activity_asset_stock_search_word);
			mSearchWord.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					if(s.length() == 0){
						mSearchWord.setHint(getString(R.string.common_stock_search_hint));
					}
				}
			});
			mSearchWord.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if (event != null && (event.getAction() == KeyEvent.ACTION_DOWN)) {
						if ((keyCode == KeyEvent.KEYCODE_ENTER) || (keyCode == KeyEvent.KEYCODE_SEARCH)) {
							doSearch();
						}
					}
					return false;
				}
			});
			mSearchBtn = (RelativeLayout)findViewById(R.id.rl_level_3_activity_asset_stock_search_button);

			mListView = (ListView)findViewById(R.id.lv_level_3_activity_asset_stock_search_listview);
			mAdapter = new AdtListViewAssetStockSearch(this, mAlInfoDataList);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(ItemClickListener);

			mSearchBtn.setOnClickListener(BtnClickListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mSearchResultLayout.setVisibility(View.GONE);
			mSearchWord.setHint(getString(R.string.common_stock_search_hint));
			Intent intent = getIntent();
			if (intent != null) {
				if (intent.hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
					findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.main_3_layout_bg));
				}
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void doSearch() {
		if (mSearchWord.getText().toString().length() == 0) {
			Toast.makeText(Level3ActivityAssetStockSearch.this, "검색어를 입력하세요", Toast.LENGTH_SHORT).show();
			return;
		}
		ElseUtils.hideSoftKeyboard(mSearchWord);

		DialogUtils.showLoading(Level3ActivityAssetStockSearch.this);											// 자산 로딩
		Call<ArrayList<ResponseSearchAssetStock>> mCall = ServiceGenerator.createService(AssetApi.class).searchStock(new SearchAssetStock(Session.getInstance(Level3ActivityAssetStockSearch.this).getUserId(), mSearchWord.getText().toString()));

		mCall.enqueue(new Callback<ArrayList<ResponseSearchAssetStock>>() {
			@Override
			public void onResponse(Call<ArrayList<ResponseSearchAssetStock>> call, Response<ArrayList<ResponseSearchAssetStock>> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						if (!response.body().isEmpty()) {
							NextStepSearchWord(response.body());
						}
					} else {
						ElseUtils.network_error(getBase());
					}
				} else {
					ElseUtils.network_error(getBase());
				}
			}

			@Override
			public void onFailure(Call<ArrayList<ResponseSearchAssetStock>> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (APP._SAMPLE_MODE_) {
					NextStepSearchWord(AssetSampleTestDataJson.GetSampleData8());
				}
				else ElseUtils.network_error(Level3ActivityAssetStockSearch.this);  //CustomToast.getInstance(Level3ActivityAssetStockSearch.this).showShort("Level3ActivityAssetStockSearch 서버 통신 실패");
			}
		});
	}

//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int _resultCod, ResponseSearchAssetStock _Data){
		Intent intent = getIntent();
		if (_Data != null) intent.putExtra(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_DATA,_Data);
		setResult(_resultCod, intent);
		finish();
	}




//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep **/
	private void NextStepSearchWord(ArrayList<ResponseSearchAssetStock> _Respose){
		if (_Respose.size() == 0) {
			((FrameLayout)findViewById(R.id.inc_level_3_activity_asset_stock_search_empty)).setVisibility(View.VISIBLE);
			((ImageView)findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_search);
			((TextView)findViewById(R.id.tv_empty_layout_txt)).setText("검색 결과가 없습니다.");
		}
		else ((FrameLayout)findViewById(R.id.inc_level_3_activity_asset_stock_search_empty)).setVisibility(View.GONE);
		mSearchResultLayout.setVisibility(View.VISIBLE);
		mAlInfoDataList.removeAll(mAlInfoDataList);
		mAlInfoDataList.addAll(_Respose);
		mAdapter.notifyDataSetChanged();
	}


}
