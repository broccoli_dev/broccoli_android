package com.nomadconnection.broccoli.activity.remit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.remit.AdtListViewRemitPossibleBankList;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RemitBank;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemitPossibleBankActivity extends BaseActivity implements View.OnClickListener {

	private static final String TAG = RemitPossibleBankActivity.class.getSimpleName();

	/* Layout */
	private Button mCancle;
	private ListView mListView;
	private AdtListViewRemitPossibleBankList mAdapter;
	private Button mConfirm;
	private ArrayList<RemitBank> mAlInfoDataList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remit_possible_bank);

		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.btn_activity_remit_possible_bank_cancel:
				onBackPressed();
				break;

			case R.id.btn_activity_remit_possible_bank_confirm:
				Intent intent = new Intent(this, RemitRegisterActivity.class);
				startActivityForResult(intent, 0);
				break;
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			Intent intent = new Intent();
			setResult(RESULT_OK, intent);
			finish();
		}
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//				mAlInfoDataList = (ArrayList<EtcChallengeBank>) getIntent().getSerializableExtra(Const.DATA);
//			}
			get_possible_bank_list();
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



	//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			mCancle = (Button)findViewById(R.id.btn_activity_remit_possible_bank_cancel);
			mCancle.setOnClickListener(this);
			mConfirm = (Button)findViewById(R.id.btn_activity_remit_possible_bank_confirm);
			mConfirm.setOnClickListener(this);

			/* Layout */
			mListView = (ListView)findViewById(R.id.lv_activity_remit_possible_bank_list);
			mAdapter = new AdtListViewRemitPossibleBankList(this, mAlInfoDataList);	// test  임시
			mListView.setAdapter(mAdapter);

		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


//=========================================================================================//
// get bank list API
//=========================================================================================//
	private void get_possible_bank_list(){
		DialogUtils.showLoading(this);
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(this).getUserId());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ArrayList<RemitBank>> call = api.getRemitBank(mRequestDataByUserId);
		call.enqueue(new Callback<ArrayList<RemitBank>>() {
			@Override
			public void onResponse(Call<ArrayList<RemitBank>> call, Response<ArrayList<RemitBank>> response) {
				DialogUtils.dismissLoading();
				mAlInfoDataList = new ArrayList<RemitBank>();
				mAlInfoDataList = response.body();
				if(mAlInfoDataList.size() != 0){
					mAdapter.setData(mAlInfoDataList);
					mAdapter.notifyDataSetChanged();
				}
				else {

				}
			}

			@Override
			public void onFailure(Call<ArrayList<RemitBank>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(RemitPossibleBankActivity.this);

			}
		});
	}
}
