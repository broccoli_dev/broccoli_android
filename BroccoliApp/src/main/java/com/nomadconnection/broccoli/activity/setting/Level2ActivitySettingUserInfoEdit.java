package com.nomadconnection.broccoli.activity.setting;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfoEdit;
import com.nomadconnection.broccoli.data.Dayli.UserProfileUrl;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FileUtils;
import com.nomadconnection.broccoli.view.RoundedAvatarDrawable;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 1. 3..
 */

public class Level2ActivitySettingUserInfoEdit extends BaseActivity implements View.OnClickListener {

    private final static int REQUEST_CERT_REALNAME = 1;
    private final static int ACTIVITY_REQUEST_ALBUM_IMAGE = 2;

    private EditText mIncomeEdit;
    private ImageView mAvatarImage;
    private TextView mPhone;
    private RadioGroup mMarriedRadio;
    private View mChildLayout;
    private View mChildDivider;
    private View mChild1, mChild2, mChild3, mChild4, mChild5, mChild6;
    private View mBtn;
    private TextView mTitle;

    private boolean isAvatarProfileUpdate = false;
    private boolean isPhoneNumberUpdate = false;
    private String backup_married = null;
    private int backup_child = 0;
    private int backup_income = 0;
    private String married = null;
    private int child = 0;
    private int income = 0;

    private ToggleButton mNotMarriedBtn;
    private ToggleButton mMarriedBtn;

    private int mRetryCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_setting_userinfo_edit);
        sendTracker(AnalyticsName.SettingUserInfo);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Session.getInstance(this).setLockTemporaryDisable(false);
    }

    private void init() {

        (findViewById(R.id.rl_navi_motion_bg)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        mTitle = (TextView) findViewById(R.id.tv_navi_motion_title);
        mTitle.setText(R.string.setting_user_realname_title);
        findViewById(R.id.tv_navi_motion_else).setVisibility(View.GONE);


        UserPersonalInfo info = Session.getInstance(this).getUserPersonalInfo();
        if (info == null && mRetryCount < 2) {
            Session.getInstance(this).refreshUserPersonalInfo(new Session.OnUserInfoUpdateCompleteInterface() {
                @Override
                public void complete(UserPersonalInfo info) {
                    init();
                    mRetryCount++;
                }

                @Override
                public void error(ErrorMessage message) {
                    switch (message) {
                        case ERROR_713_NO_SUCH_USER:
                        case ERROR_10007_SESSION_NOT_FOUND:
                        case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                        case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                        case ERROR_20004_NO_SUCH_USER:
                            showTokenExpire();
                            break;
                    }
                }
            });
            return;
        }
        if (info != null && info.user != null) {
            mAvatarImage = (ImageView) findViewById(R.id.iv_activity_setting_userinfo_edit_round_avatar);
            mAvatarImage.setOnClickListener(this);
            TextView name = (TextView) findViewById(R.id.tv_activity_setting_userinfo_edit_name);
            TextView email = (TextView) findViewById(R.id.tv_activity_setting_userinfo_edit_email);
            TextView birthDate = (TextView) findViewById(R.id.tv_activity_setting_userinfo_edit_birth_day);
            mPhone = (TextView) findViewById(R.id.tv_activity_setting_userinfo_edit_phone_num);
            mPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
                @Override
                public synchronized void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);
                }
            });
            mPhone.setOnClickListener(this);

            name.setText(info.user.realname);
            email.setText(info.user.email);
            birthDate.setText(info.user.birthDate);

            mMarriedRadio = (RadioGroup) findViewById(R.id.rg_activity_setting_userinfo_married);
            mMarriedRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                }
            });

            mNotMarriedBtn = (ToggleButton) findViewById(R.id.rb_setting_userinfo_not_married);
            mMarriedBtn = (ToggleButton) findViewById(R.id.rb_setting_userinfo_married);
            mNotMarriedBtn.setOnCheckedChangeListener(mOnCheckListener);
            mMarriedBtn.setOnCheckedChangeListener(mOnCheckListener);
//            findViewById(R.id.rb_setting_userinfo_married).setOnClickListener(this);
//            findViewById(R.id.rb_setting_userinfo_not_married).setOnClickListener(this);

            mIncomeEdit = (EditText) findViewById(R.id.et_activity_setting_income);
            mIncomeEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String value = s.toString();
                    if (s.length() > 0) {
                        income = Integer.valueOf(value);
                        mBtn.setEnabled(true);
                    } else if (s.length() == 0) {
                        mIncomeEdit.setText("0");
                    }
                    checkButtonState();
                }
            });

            mChildLayout = findViewById(R.id.ll_activity_setting_userinfo_edit_child_group);
            mChildDivider = findViewById(R.id.ll_activity_setting_userinfo_edit_child_group_divider);

            mChild1 = findViewById(R.id.iv_setting_userinfo_child1);
            mChild2 = findViewById(R.id.iv_setting_userinfo_child2);
            mChild3 = findViewById(R.id.iv_setting_userinfo_child3);
            mChild4 = findViewById(R.id.iv_setting_userinfo_child4);
            mChild5 = findViewById(R.id.iv_setting_userinfo_child5);
            mChild6 = findViewById(R.id.iv_setting_userinfo_child6);
            mChild1.setOnClickListener(this);
            mChild2.setOnClickListener(this);
            mChild3.setOnClickListener(this);
            mChild4.setOnClickListener(this);
            mChild5.setOnClickListener(this);
            mChild6.setOnClickListener(this);

            mBtn = findViewById(R.id.btn_confirm);
            mBtn.setEnabled(false);
            mBtn.setOnClickListener(this);

            setData(info);
        }

    }

    public void onToggle(View view) {
        ToggleButton button = ((ToggleButton) view);
        if (button.isChecked()) {

        }
        ((RadioGroup)view.getParent()).check(view.getId());
    }

    private void setData(UserPersonalInfo info) {
        DialogUtils.dismissLoading();
        backup_married = info.user.marriedYn;
        backup_child = info.user.children;
        backup_income = info.user.monthlyIncome;
        married = info.user.marriedYn;
        child = info.user.children;
        income = info.user.monthlyIncome;

        mPhone.setText(info.user.mobileNumber);
        if (UserPersonalInfo.MARRIED.equalsIgnoreCase(married)) {
            showChildLayout(true);
            mMarriedBtn.setChecked(true);
            mNotMarriedBtn.setChecked(false);
        } else if (UserPersonalInfo.NOT_SELECT.equalsIgnoreCase(married)) {
            showChildLayout(false);
            mNotMarriedBtn.setChecked(false);
            mMarriedBtn.setChecked(false);
        } else {
            showChildLayout(false);
            mNotMarriedBtn.setChecked(true);
            mMarriedBtn.setChecked(false);
        }
        setChildCount(child);
        mIncomeEdit.setText(String.valueOf(income));

        showProfileImage(info);
    }

    private void showProfileImage(UserPersonalInfo info) {
        if (!this.isFinishing()) {
            Glide.with(this).load(info.user.profileImageUrl).asBitmap().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(new BitmapImageViewTarget(mAvatarImage) {

                @Override
                protected void setResource(Bitmap resource) {
                    RoundedAvatarDrawable avatarDrawable = new RoundedAvatarDrawable(resource);
                    mAvatarImage.setImageDrawable(avatarDrawable);
                }
            });
        }
    }


    private void pickPhoto() {
        Session.getInstance(this).setLockTemporaryDisable(true);
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        intent.setType("image/*");
        startActivityForResult(intent, ACTIVITY_REQUEST_ALBUM_IMAGE);
    }

    private void uploadFile(Uri uri) {

        File file = FileUtils.getFile(this, uri);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        DayliApi api = ServiceGenerator.createServiceDayliUpload(DayliApi.class);
        String token = DbManager.getInstance().getUserDayliToken(this, Session.getInstance(this).getUserId());
        Call<UserProfileUrl> call = api.uploadProfileImage(token, body);
        call.enqueue(new Callback<UserProfileUrl>() {
            @Override
            public void onResponse(Call<UserProfileUrl> call, Response<UserProfileUrl> response) {
                if (response != null && response.isSuccessful()) {
                    UserProfileUrl url = response.body();
                    UserPersonalInfo info = Session.getInstance(Level2ActivitySettingUserInfoEdit.this).getUserPersonalInfo();
                    info.user.profileImageUrl = url.profileImageUrl;
                    showProfileImage(info);
                } else {
                    ElseUtils.network_error(Level2ActivitySettingUserInfoEdit.this);
                }
            }

            @Override
            public void onFailure(Call<UserProfileUrl> call, Throwable t) {
                ElseUtils.network_error(Level2ActivitySettingUserInfoEdit.this);
            }
        });
    }

    CompoundButton.OnCheckedChangeListener mOnCheckListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            int id = buttonView.getId();
            if (buttonView == mMarriedBtn) {
                if (isChecked) {
                    mNotMarriedBtn.setChecked(false);
                    married = UserPersonalInfo.MARRIED;
                    showChildLayout(UserPersonalInfo.MARRIED.equalsIgnoreCase(married));
                } else {
                    married = UserPersonalInfo.NOT_SELECT;
                    showChildLayout(UserPersonalInfo.MARRIED.equalsIgnoreCase(married));
                }
            } else if (buttonView == mNotMarriedBtn) {
                if (isChecked) {
                    mMarriedBtn.setChecked(false);
                    married = UserPersonalInfo.SINGLE;
                    showChildLayout(UserPersonalInfo.MARRIED.equalsIgnoreCase(married));
                } else {
                    married = UserPersonalInfo.NOT_SELECT;
                    showChildLayout(UserPersonalInfo.MARRIED.equalsIgnoreCase(married));
                }
            }
            checkButtonState();
        }
    };

    private void showChildLayout(boolean bool) {
        if (bool) {
            mChildLayout.setVisibility(View.VISIBLE);
            mChildDivider.setVisibility(View.VISIBLE);
        } else {
            mChildLayout.setVisibility(View.GONE);
            mChildDivider.setVisibility(View.GONE);
        }
    }

    private void setChildCount(int count) {
        child = count;
        mBtn.setEnabled(true);
        switch (count) {
            case 0:
                mChild1.setSelected(false);
                mChild2.setSelected(false);
                mChild3.setSelected(false);
                mChild4.setSelected(false);
                mChild5.setSelected(false);
                mChild6.setSelected(false);
                break;
            case 1:
                mChild1.setSelected(true);
                mChild2.setSelected(false);
                mChild3.setSelected(false);
                mChild4.setSelected(false);
                mChild5.setSelected(false);
                mChild6.setSelected(false);
                break;
            case 2:
                mChild1.setSelected(true);
                mChild2.setSelected(true);
                mChild3.setSelected(false);
                mChild4.setSelected(false);
                mChild5.setSelected(false);
                mChild6.setSelected(false);
                break;
            case 3:
                mChild1.setSelected(true);
                mChild2.setSelected(true);
                mChild3.setSelected(true);
                mChild4.setSelected(false);
                mChild5.setSelected(false);
                mChild6.setSelected(false);
                break;
            case 4:
                mChild1.setSelected(true);
                mChild2.setSelected(true);
                mChild3.setSelected(true);
                mChild4.setSelected(true);
                mChild5.setSelected(false);
                mChild6.setSelected(false);
                break;
            case 5:
                mChild1.setSelected(true);
                mChild2.setSelected(true);
                mChild3.setSelected(true);
                mChild4.setSelected(true);
                mChild5.setSelected(true);
                mChild6.setSelected(false);
                break;
            case 6:
                mChild1.setSelected(true);
                mChild2.setSelected(true);
                mChild3.setSelected(true);
                mChild4.setSelected(true);
                mChild5.setSelected(true);
                mChild6.setSelected(true);
                break;
        }
        checkButtonState();
    }

    private void checkButtonState() {
        boolean enable = false;
        if (isPhoneNumberUpdate) {
            enable = true;
        }
        if (isAvatarProfileUpdate) {
            enable = true;
        }
        if (backup_income != income) {
            enable = true;
        }
        if (backup_married != null && !backup_married.equalsIgnoreCase(married)) {
            enable = true;
        }
        if (backup_child != child) {
            enable = true;
        }

        mBtn.setEnabled(enable);
    }

    private void showCertRealName() {
        Intent intent = new Intent(Level2ActivitySettingUserInfoEdit.this, Level4ActivitySettingUserInfoUpdate.class);
        startActivityForResult(intent, REQUEST_CERT_REALNAME);
    }

    private void updateUserInfo() {
        DialogUtils.showLoading(this);
        DayliApi api = ServiceGenerator.createServiceDayli(DayliApi.class);
        String token = DbManager.getInstance().getUserDayliToken(this, Session.getInstance(this).getUserId());
        UserPersonalInfoEdit info = new UserPersonalInfoEdit();
        info.children = child;
        info.marriedYn = married;
        info.monthlyIncome = income;
        Call<Result> call = api.updateUserInfo(token, info);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    Toast.makeText(getBase(), "정보 수정이 완료되었습니다.", Toast.LENGTH_LONG).show();
                    refreshUserInfo();
                } else {
                    ElseUtils.network_error(Level2ActivitySettingUserInfoEdit.this);
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(Level2ActivitySettingUserInfoEdit.this);
            }
        });
    }

    private void refreshUserInfo() {
        DialogUtils.showLoading(this);
        Session.getInstance(this).refreshUserPersonalInfo(new Session.OnUserInfoUpdateCompleteInterface() {

            @Override
            public void complete(UserPersonalInfo info) {
                if (info != null) {
                    setData(info);
                }
            }

            @Override
            public void error(ErrorMessage message) {
                switch (message) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showTokenExpire();
                        break;
                }
            }

        });
    }

    public void onBtnClickNaviMotion(View view) {
        finish();
    }

    private Uri convertImage(String path) {
        int degrees = ElseUtils.getExifOrientation(path);

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opt);
        opt.inSampleSize = ElseUtils.calculateInSampleSize(opt, 400, 400);
        opt.inJustDecodeBounds = false;

        Bitmap orgImage = BitmapFactory.decodeFile(path, opt);

        orgImage = ElseUtils.GetRotatedBitmap(orgImage, degrees);
        String filename = "Profile_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        return ElseUtils.saveImage(this, orgImage, filename);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CERT_REALNAME) {
            if (requestCode == RESULT_OK) {
                refreshUserInfo();
                isPhoneNumberUpdate = true;
            }
        } else if (requestCode == ACTIVITY_REQUEST_ALBUM_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    Uri uri = data.getData();
                    Uri convertUri = convertImage(ElseUtils.getPath(this, uri));
                    uploadFile(convertUri);
                    isAvatarProfileUpdate = true;
                }
            }
        }
        checkButtonState();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_activity_setting_userinfo_edit_round_avatar:
                pickPhoto();
                break;
            case R.id.iv_setting_userinfo_child1:
                if (child == 1) {
                    setChildCount(0);
                } else {
                    setChildCount(1);
                }
                break;
            case R.id.iv_setting_userinfo_child2:
                setChildCount(2);
                break;
            case R.id.iv_setting_userinfo_child3:
                setChildCount(3);
                break;
            case R.id.iv_setting_userinfo_child4:
                setChildCount(4);
                break;
            case R.id.iv_setting_userinfo_child5:
                setChildCount(5);
                break;
            case R.id.iv_setting_userinfo_child6:
                setChildCount(6);
                break;
            case R.id.tv_activity_setting_userinfo_edit_phone_num:
                showCertRealName();
                break;
            case R.id.btn_confirm:
                updateUserInfo();
                break;
        }
    }
}
