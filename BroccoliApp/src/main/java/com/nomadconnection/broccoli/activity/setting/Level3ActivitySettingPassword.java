package com.nomadconnection.broccoli.activity.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Splash;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.constant.ErrorMessage;

import static com.nomadconnection.broccoli.constant.AnalyticsName.FindPinResultConfirm;
import static com.nomadconnection.broccoli.constant.AnalyticsName.SettingRegPinChangeConfirm;

/**
 * Created by YelloHyunminJang on 16. 2. 2..
 * 사용자 암호 설정
 */
public class Level3ActivitySettingPassword extends BaseActivity implements View.OnClickListener {

    enum Status {
        PWD_WANTED,
        PWD_MATCH
    }

    private LoginData mData;
    private String mUserPwd;
    private String mConfirm;
    private EditText mSecretEditText;
    private ImageView mIndicator1, mIndicator2, mIndicator3, mIndicator4, mIndicator5, mIndicator6;
    private BaseInputConnection mInputConnection;
    private TextView mCompareGuide;
    private Status mStatus = Status.PWD_WANTED;
    private boolean isRestPassword = false;
    private boolean isFindPin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_setting_password);
        if (getIntent() != null) {
            Intent intent = getIntent();
            isRestPassword = intent.hasExtra(Const.RESET_PASSWORD);
            mStatus = Status.PWD_WANTED;
            isFindPin = intent.hasExtra(Const.FIND_PIN);
            if (isFindPin) {
                sendTracker(AnalyticsName.FindPin);
                sendTracker(AnalyticsName.FindPinResult);
            }
            if (!isFindPin) {
                sendTracker(AnalyticsName.SettingRegPinChange);
            }
        }
        init();
        initComponent();
    }

    private void init() {
        DialogUtils.showLoading(this);
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                mData = DbManager.getInstance().getUserInfoDataSet(getApplicationContext(), str, Session.getInstance(getApplicationContext()).getUserId());
                DialogUtils.dismissLoading();
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void initComponent() {
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        TextView title = (TextView) findViewById(R.id.tv_navi_img_txt_title);
        title.setText(R.string.setting_password_title);
        mIndicator1 = (ImageView) findViewById(R.id.screen_lock_num1_imgview);
        mIndicator2 = (ImageView) findViewById(R.id.screen_lock_num2_imgview);
        mIndicator3 = (ImageView) findViewById(R.id.screen_lock_num3_imgview);
        mIndicator4 = (ImageView) findViewById(R.id.screen_lock_num4_imgview);
        mIndicator5 = (ImageView) findViewById(R.id.screen_lock_num5_imgview);
        mIndicator6 = (ImageView) findViewById(R.id.screen_lock_num6_imgview);
        mCompareGuide = (TextView) findViewById(R.id.screen_lock_detail_text);

        findViewById(R.id.screen_lock_key_num_0).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_1).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_2).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_3).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_4).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_5).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_6).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_7).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_8).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_9).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_back).setOnClickListener(this);
        findViewById(R.id.screen_lock_key_num_cancel).setOnClickListener(this);
        mSecretEditText = (EditText) findViewById(R.id.secret_edittext);
        mInputConnection = new BaseInputConnection(mSecretEditText, true);
        mSecretEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showLengthIndicator(s.length());
                if (s.length() == 6) {
                    if (mStatus == Status.PWD_WANTED) {
                        enterCompareMode(s.toString());
                    } else {
                        mConfirm = s.toString();
                        if (comparePwd()) {
                            changePassword(mConfirm);
                        } else {
                            mSecretEditText.setText(null);
                            mCompareGuide.setText(getString(R.string.setting_password_guide_miss_match));
                        }
                    }
                }
            }
        });
        if (isRestPassword) {
            mSecretEditText.setText(null);
            mCompareGuide.setText(R.string.setting_password_guide_wanted);
        }
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        final String mStr = getString(R.string.setting_password_change_cancel);
        DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
                            onBackPressed();
                        }
                    }
                },
                new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fl_fragment_membership_next:
                break;
            case R.id.screen_lock_key_num_0:
                mSecretEditText.setText(mSecretEditText.getText()+"0");
                break;
            case R.id.screen_lock_key_num_1:
                mSecretEditText.setText(mSecretEditText.getText()+"1");
                break;
            case R.id.screen_lock_key_num_2:
                mSecretEditText.setText(mSecretEditText.getText() + "2");
                break;
            case R.id.screen_lock_key_num_3:
                mSecretEditText.setText(mSecretEditText.getText() + "3");
                break;
            case R.id.screen_lock_key_num_4:
                mSecretEditText.setText(mSecretEditText.getText() + "4");
                break;
            case R.id.screen_lock_key_num_5:
                mSecretEditText.setText(mSecretEditText.getText() + "5");
                break;
            case R.id.screen_lock_key_num_6:
                mSecretEditText.setText(mSecretEditText.getText() + "6");
                break;
            case R.id.screen_lock_key_num_7:
                mSecretEditText.setText(mSecretEditText.getText() + "7");
                break;
            case R.id.screen_lock_key_num_8:
                mSecretEditText.setText(mSecretEditText.getText() + "8");
                break;
            case R.id.screen_lock_key_num_9:
                mSecretEditText.setText(mSecretEditText.getText() + "9");
                break;
            case R.id.screen_lock_key_num_back:
                Editable text = mSecretEditText.getText();
                if (text.length()-1 > 0) {
                    mSecretEditText.setText(text.delete(text.length()-2,text.length()-1));
                } else {
                    mSecretEditText.setText("");
                }
                break;
            case R.id.screen_lock_key_num_cancel:
                cancel();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            final String mStr = getString(R.string.setting_password_change_cancel);
            DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
                                onBackPressed();
                            }
                        }
                    },
                    new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showLengthIndicator(int count) {
        switch (count) {
            case 0:
                mIndicator1.setImageResource(R.drawable.shape_row_indicator_off);
                mIndicator2.setImageResource(R.drawable.shape_row_indicator_off);
                mIndicator3.setImageResource(R.drawable.shape_row_indicator_off);
                mIndicator4.setImageResource(R.drawable.shape_row_indicator_off);
                mIndicator5.setImageResource(R.drawable.shape_row_indicator_off);
                mIndicator6.setImageResource(R.drawable.shape_row_indicator_off);
                break;
            case 1:
                mIndicator1.setImageResource(R.drawable.shape_row_indicator_on);
                mIndicator2.setImageResource(R.drawable.shape_row_indicator_off);
                break;
            case 2:
                mIndicator2.setImageResource(R.drawable.shape_row_indicator_on);
                mIndicator3.setImageResource(R.drawable.shape_row_indicator_off);
                break;
            case 3:
                mIndicator3.setImageResource(R.drawable.shape_row_indicator_on);
                mIndicator4.setImageResource(R.drawable.shape_row_indicator_off);
                break;
            case 4:
                mIndicator4.setImageResource(R.drawable.shape_row_indicator_on);
                mIndicator5.setImageResource(R.drawable.shape_row_indicator_off);
                break;
            case 5:
                mIndicator5.setImageResource(R.drawable.shape_row_indicator_on);
                mIndicator6.setImageResource(R.drawable.shape_row_indicator_off);
                break;
            case 6:
                mIndicator6.setImageResource(R.drawable.shape_row_indicator_on);
                break;
        }
    }

    private boolean processCompareDatabase(String currentPwd) {
        boolean bool = false;
        if (mData != null) {
            if (mData.getPinNumber().equals(currentPwd)) {
                bool = true;
            }
        }
        return bool;
    }

    private void enterCompareMode(String password) {
        if (isFindPin) {
            sendTracker(FindPinResultConfirm);
        } else {
            sendTracker(SettingRegPinChangeConfirm);
        }
        mUserPwd = password;
        mStatus = Status.PWD_MATCH;
        mSecretEditText.setText(null);
        mCompareGuide.setText(R.string.setting_password_guide_match);
    }

    private boolean comparePwd() {
        boolean bool = false;
        if (mUserPwd != null) {
            if (mUserPwd.equals(mConfirm)) {
                bool = true;
            }
        }
        return bool;
    }

    private void changePassword(String password) {
        DialogUtils.showLoading(this);
        final LoginData loginData = new LoginData();
        loginData.setUserId(Session.getInstance(this).getUserId());
        loginData.setPinNumber(password);
        Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                DialogUtils.dismissLoading();
                try {
                    DbManager.getInstance().updatePinNumber(Level3ActivitySettingPassword.this, str,
                            Session.getInstance(Level3ActivitySettingPassword.this).getUserId(), loginData.getPinNumber());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Toast.makeText(getBase(), "암호가 변경되었습니다.", Toast.LENGTH_LONG).show();
                    if (isRestPassword) {
                        ActivityCompat.finishAffinity(Level3ActivitySettingPassword.this);
                        Session.getInstance(getApplicationContext()).setLock(false);
                        startActivity(new Intent(Level3ActivitySettingPassword.this, Splash.class));
                    } else {
                        finish();
                    }
                }
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void cancel() {
        mConfirm = null;
        mUserPwd = null;
        switch (mStatus){
            case PWD_WANTED:
                break;
            case PWD_MATCH:
                mStatus = Status.PWD_WANTED;
                mCompareGuide.setText(getString(R.string.setting_password_guide_wanted));
                break;
        }
        mSecretEditText.setText(null);

    }
}
