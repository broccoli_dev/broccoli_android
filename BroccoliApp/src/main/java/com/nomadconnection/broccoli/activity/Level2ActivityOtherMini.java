package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Etc.BodyMoneyInfo;
import com.nomadconnection.broccoli.data.Etc.RequestEtcMoneyEdit;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.fragment.FragmentOtherMini0;
import com.nomadconnection.broccoli.fragment.FragmentOtherMini1;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level2ActivityOtherMini extends BaseActivity implements InterfaceEditOrDetail {

	private static final String TAG = Level2ActivityOtherMini.class.getSimpleName();
	private static final String TAG0 = FragmentOtherMini0.class.getSimpleName();
	private static final String TAG1 = FragmentOtherMini1.class.getSimpleName();

	public static int mCurrentFragmentIndex 	= 0;	// default
	private final int FRAGMENT_ONE 			= 0;		// 추가
	private final int FRAGMENT_TWO 			= 1;		// 상세

	/* Layout */
	private Animation mAnimTitle;


	/* getParcelableExtra */
	private ArrayList<BodyMoneyInfo> mAlInfoDataList;

	/* refresh 유지 */
	public static int mSideMonth = 2;
	public static int mPageInx;							// Default 현재


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_other_mini);
		sendTracker(AnalyticsName.CalendarList);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onBackPressed() {
		onBtnClickNaviMotion(findViewById(R.id.ll_navi_motion_back));
//		super.onBackPressed();
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == APP.ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_RESULT_CODE_SUCCESS){
//			getSupportFragmentManager().beginTransaction().replace(R.id.fl_level_2_activity_other_mini_container, new FragmentOtherMini1(mAlInfoDataList)).commitAllowingStateLoss();
			getSupportFragmentManager().beginTransaction().replace(R.id.fl_level_2_activity_other_mini_container, new FragmentOtherMini1()).commitAllowingStateLoss();
		}
		else if(resultCode == APP.ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_RESULT_CODE_CANCLE){
		}
	}




//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try
		{
			switch (_view.getId()) {
				case R.id.tv_navi_motion_title:
					if (mCurrentFragmentIndex == 0)
						break;

				case R.id.ll_navi_motion_back:
					if (mCurrentFragmentIndex == 1) goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_MAIN_4_RESULT_CODE_SUCCESS);
					else if (mCurrentFragmentIndex == 0) {
						String mStr = "머니 캘린더 등록\n취소하시겠습니까?";
						DialogUtils.showDlgBaseTwoButton(Level2ActivityOtherMini.this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
											mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
//						mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentFragmentIndex == 1) { mCurrentFragmentIndex = 0; fragmentReplace(mCurrentFragmentIndex, "in_right"); afterMotion("else"); }
					else if (mCurrentFragmentIndex == 0) {
						if (checkValueIsFail()) return;
						DialogUtils.showLoading(Level2ActivityOtherMini.this);											// 자산 로딩
						ServiceGenerator.createService(EtcApi.class).editMoney(
								new RequestEtcMoneyEdit(
										Session.getInstance(Level2ActivityOtherMini.this).getUserId(),
										"0",        //등록
										FragmentOtherMini0.mTitle.getText().toString(),                                                        //mInfoDataOtherMini.mTxt2,        // _Title
										FragmentOtherMini0.mBtn1Title.getText().toString(),                                                                            // _TransactionId
										TransFormatUtils.transRemoveComma(FragmentOtherMini0.mCost.getText().toString()),                            // mInfoDataOtherMini.mTxt5,		// _Amt
										TransFormatUtils.transDialogRadioTypeNameToCode(FragmentOtherMini0.mBtn2Title.getText().toString()),        // mInfoDataOtherMini.mTxt3,		// _PayType
										TransFormatUtils.transRemoveDot(FragmentOtherMini0.mBtn3Title.getText().toString()),                        //mInfoDataOtherMini.mTxt4,        // _PayDate
										TransFormatUtils.transDialogRadioAlarmNameToCode(FragmentOtherMini0.mBtn4Title.getText().toString())    // mInfoDataOtherMini.mTxt9        // _RepeatType
								)
						).enqueue(new Callback<Result>() {
							@Override
							public void onResponse(Call<Result> call, Response<Result> response) {
								DialogUtils.dismissLoading();											// 자산 로딩
								if (APP._SAMPLE_MODE_)
									NextStepAdd(AssetSampleTestDataJson.GetSampleData18());
								else NextStepAdd(response.body());
							}

							@Override
							public void onFailure(Call<Result> call, Throwable t) {
								DialogUtils.dismissLoading();											// 자산 로딩
								if (APP._SAMPLE_MODE_) {
									NextStepAdd(AssetSampleTestDataJson.GetSampleData18());
								} else
									ElseUtils.network_error(Level2ActivityOtherMini.this);
							}
						});
//						mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
					}
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}




	private void afterMotion(String _Divider){
		if(_Divider.equals("else")){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG0).mTitle);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("완료");
			complete_button_enable_disable(false);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_03_0_0_400_fill);
		}
		else{	// default
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG1).mTitle);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("추가");
			complete_button_enable_disable(true);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_03_0_0_0_400_fill);
		}
		((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);
	}







	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//			}

			/* getParcelableExtra */
			mAlInfoDataList = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_1);

			if(mAlInfoDataList == null){
				bResult = false;
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG1).mTitle);
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) ((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("추가");
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);

			/* Layout */
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mPageInx = 10;										// Range 0~11 , Default 10
//			mPageInx = 10+mSideMonth;										// Range 0~11 , Default 10
			mCurrentFragmentIndex = 1;
			fragmentReplace(mCurrentFragmentIndex, "");
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}







//=========================================================================================//
// Fragment Navi
//=========================================================================================//
	/** Fragment 변경 **/
	private void fragmentReplace(int reqNewFragmentIndex, String _Anim)
	{
		try
		{
			Fragment newFragment = null;
			newFragment = getFragment(reqNewFragmentIndex);

			// replace fragment
			final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			if (_Anim.length() != 0 && _Anim.equals("in_right")) transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out);
			if (_Anim.length() != 0 && _Anim.equals("out_right")) transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
			transaction.replace(R.id.fl_level_2_activity_other_mini_container, newFragment);
//			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commitAllowingStateLoss();

		}
		catch (Exception e)
		{}
	}

	/** Fragment Get **/
	private Fragment getFragment(int idx)
	{
		Fragment newFragment = null;

		switch (idx) {
			case FRAGMENT_ONE:	// 추가/편집
				newFragment = new FragmentOtherMini0();
				break;

			case FRAGMENT_TWO:	// 상세
				newFragment = new FragmentOtherMini1();
//				newFragment = new FragmentOtherMini1(mAlInfoDataList);
				break;

			default:
				break;
		}

		return newFragment;
	}



//===============================================================================//
// Value Check
//===============================================================================//
	/**  Value check **/
	private Boolean checkValueIsFail(){
		boolean mResult = false;

		if (FragmentOtherMini0.mTitle.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityOtherMini.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentOtherMini0.mBtn1Title.getText().toString().equals("선택")){
			Toast.makeText(Level2ActivityOtherMini.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentOtherMini0.mBtn2Title.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityOtherMini.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentOtherMini0.mCost.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityOtherMini.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentOtherMini0.mBtn3Title.getText().toString().equals("선택")){
			Toast.makeText(Level2ActivityOtherMini.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentOtherMini0.mBtn4Title.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityOtherMini.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}

		return  mResult;
	}

//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep Add **/
	private void NextStepAdd(Result _Result){
		if (_Result != null && _Result.getCode().equals("100")){
			mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
		}
	}



//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int _resultCod){
		Intent intent = getIntent();
		setResult(_resultCod, intent);
		finish();
	}

	private void complete_button_enable_disable(boolean value){
		if(value){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}


	@Override
	public void onEdit() {
		complete_button_enable_disable(true);
	}

	@Override
	public void onDetail() {

	}

	@Override
	public void onError() {
		complete_button_enable_disable(false);
	}

	@Override
	public void onRefresh() {

	}
}
