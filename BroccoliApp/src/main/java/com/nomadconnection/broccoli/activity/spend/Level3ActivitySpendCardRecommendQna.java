package com.nomadconnection.broccoli.activity.spend;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;

/**
 * Created by YelloHyunminJang on 2017. 3. 24..
 */

public class Level3ActivitySpendCardRecommendQna extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_spend_card_recommend_qna);
        init();
    }

    private void init() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_card_recommend_qna_title);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
    }
}
