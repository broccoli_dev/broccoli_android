package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lumensoft.ks.KSCertificate;
import com.lumensoft.ks.KSCertificateLoader;
import com.lumensoft.ks.KSException;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.CertPasswordActivity;
import com.nomadconnection.broccoli.activity.GuideAuthCopyActivity;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.AdtListViewCertList;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoliraonsecure.KSW_CertItem;
import com.nomadconnection.broccoliraonsecure.KSW_CertListManager;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by YelloHyunminJang on 2017. 2. 21..
 */

public class Level3ActivitySettingFinancialSelectNewCert extends BaseActivity {

    private ImageView mImage;
    private TextView mText;
    private TextView mHeader;
    private String mExpire;
    private View mEmptyLayout;
    private ListView mListView;
    private Vector<?> userCerts = null;
    private AdtListViewCertList mAdapter;
    private ArrayList<KSW_CertItem> mCertList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_setting_financial_renewal);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.CertReplace);
        initWidget();
        initData();
    }

    private void initWidget() {
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView) findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_financial_renewal_title);
        mImage = (ImageView) findViewById(R.id.setting_financial_renewal_image_guide);
        mImage.setImageResource(R.drawable.list_info_renewal_2);
        mText = (TextView) findViewById(R.id.setting_financial_renewal_text_guide);
        mText.setText(R.string.setting_financial_renewal_step2_guide);
        mHeader = (TextView) findViewById(R.id.setting_financial_renewal_header);
        mHeader.setText(R.string.setting_financial_renewal_step2_header);
        mEmptyLayout = findViewById(R.id.fl_empty_layout);
        mListView = (ListView) findViewById(R.id.setting_financial_renewal_list);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                
            }
        });
        findViewById(R.id.fl_cert_copy_guide_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent guideAuthIntent = new Intent(getActivity(), GuideAuthCopyActivity.class);
                startActivity(guideAuthIntent);
            }
        });
    }

    private void initData() {
        loadCertListInApp(false);
    }

    private void loadCertListInApp(boolean loadApp) {
        try {
            // 앱내 인증서 보기 체크 선택/해제 시 리스트 초기
            if (userCerts != null) {
                userCerts.clear();
                if (mCertList != null)
                    mCertList.clear();
            }

            // 인증서 읽기
            if (loadApp)
                // 앱내 저장된 NPKI / GPKI 인증서 로드
                userCerts = KSCertificateLoader.getCertificateListfromAppWithGpkiAsVector(getBase());
            else
                // SD카드 내 저장된 NPKI / GPKI 인증서 로드
                userCerts = KSCertificateLoader.getCertificateListfromSDCardWithGpkiAsVector(getBase());

        } catch (KSException e) {
            Toast.makeText(getApplicationContext(), "인증서 목록 초기화 실패 인증서 목록 생성에 실패하였습니다.", Toast.LENGTH_LONG).show();
        }

        if (userCerts == null || userCerts.size() == 0) {
            mEmptyLayout.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.INVISIBLE);
        } else {
            mEmptyLayout.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
            mCertList = new ArrayList<KSW_CertItem>();
            for (int i = 0; i < userCerts.size(); i++) {
                try {
                    mCertList.add(new KSW_CertItem((KSCertificate) userCerts.elementAt(i)));
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(this, "인증서 목록 초기화 실패 인증서 목록 생성에 실패하였습니다.", Toast.LENGTH_LONG).show();
                }
            }
            mAdapter = new AdtListViewCertList(this,
                    mCertList);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    KSCertificate userCert = (KSCertificate) userCerts.elementAt(position);
                    // 선택한 인증서는 KSW_CertListManager.setSelectedCert에 setting
                    KSW_CertListManager.setSelectedCert(userCert);
                    String certNM = userCert.getSubjectName();

                    if (userCert.isExpired()) {
                        Toast.makeText(getBase(), "기간이 만료된 인증서입니다.", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        mExpire = userCert.getExpiredTime();
                    }

                    if (certNM != null) {
                        Intent intent = new Intent(getBase(), CertPasswordActivity.class);
                        intent.putExtra(Const.CERT_NAME, certNM);
                        startActivityForResult(intent, 0);
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            if (data != null) {
                Intent intent = new Intent();
                intent.putExtra(Const.CERT_NAME, data.getStringExtra(Const.CERT_NAME));
                intent.putExtra(Const.CERT_PWD, data.getStringExtra(Const.CERT_PWD));
                intent.putExtra(Const.CERT_VALIDDATE, mExpire);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

}
