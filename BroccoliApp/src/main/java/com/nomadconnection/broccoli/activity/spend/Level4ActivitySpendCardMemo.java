package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendCardUserListData;
import com.nomadconnection.broccoli.utils.DialogUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 3. 17..
 */

public class Level4ActivitySpendCardMemo extends BaseActivity {

    public static final String RESULT_MEMO = "RESULT_MEMO";
    private EditText mMemo;
    private TextView mTitle;
    private TextView mCountView;
    private SpendCardUserListData mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_spend_card_memo);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SpendCardInfoMemo);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_card_detail_title);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setText(R.string.common_complete);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
        mCountView = (TextView) findViewById(R.id.tv_spend_card_counting);
        mTitle = (TextView) findViewById(R.id.tv_spend_card_name);
        mMemo = (EditText) findViewById(R.id.et_spend_card_memo);
        mMemo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mCountView.setText(String.valueOf(s.length()));
            }
        });
    }

    private void initData() {
        Intent intent = getIntent();
        mData = (SpendCardUserListData) intent.getSerializableExtra(Const.DATA);
        mTitle.setText(mData.card.cardName);
        mMemo.setText(mData.memo);
    }

    private void setMemo(final String memo) {
        DialogUtils.showLoading(this);
        HashMap<String, Object> map = new HashMap<>();
        map.put("cardId", mData.cardId);
        map.put("memo", memo);
        Call<Result> call = ServiceGenerator.createService(SpendApi.class).setCardMemo(map);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if(response.body() != null && response.body().isOk()){
                    Intent intent = new Intent();
                    intent.putExtra(RESULT_MEMO, memo);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    @Override
    public void onBtnClickNaviMotion(View view) {
        super.onBtnClickNaviMotion(view);
        switch (view.getId()) {
            case R.id.ll_navi_motion_else:
                setMemo(mMemo.getText().toString());
                break;
        }
    }
}
