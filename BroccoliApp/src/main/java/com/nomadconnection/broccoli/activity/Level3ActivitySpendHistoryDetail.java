package com.nomadconnection.broccoli.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataAssetCredit;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.DeleteSpendMonthly;
import com.nomadconnection.broccoli.data.Spend.EditSpendMonthly;
import com.nomadconnection.broccoli.data.Spend.SpendMonthlyInfo;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level3ActivitySpendHistoryDetail extends BaseActivity implements View.OnClickListener{

	private static final String TAG = Level3ActivitySpendHistoryDetail.class.getSimpleName();

	public int mCurrentStateIndex = 0;		// default 0: 상세 , 1: 편집

	/* Get Parcel Data */
	private SpendMonthlyInfo mInfoDataSpendHistory;
	private ArrayList<InfoDataAssetCredit> mAlInfoDataList;
	
	/* Layout */
	private Animation mAnimTitle;
	private Animation mAnimSpinnerLayout;
	private Animation mAnimSpinnerText;
	private Animation mAnimSpinnerEditText;
	private Animation mAnimDelete;
	private Button mDelete;
	Animation.AnimationListener myAnimationListener = new Animation.AnimationListener() {
		public void onAnimationEnd(Animation animation) {
			mDelete.setVisibility(View.GONE); //애니메 끝나면 사라저버려!
			mDelete.clearAnimation();
		}
		@Override
		public void onAnimationRepeat(Animation animation) {
		}
		@Override
		public void onAnimationStart(Animation animation) {
		}
	};
	private RelativeLayout mSpendMemoirs;
	private TextView mSpendSumText;
	private EditText mSpendSum;
	private TextView mSpendBreakdownText;
	private EditText mSpendBreakdown;
	private LinearLayout mSpendDataLL;
	private TextView mSpendDateText;
	private TextView mSpendDate;
	private TextView mSpendWayText;
	private TextView mSpendWay;
	private RelativeLayout mSpendCategoryRL;
	private TextView mSpendCategoryText;
	private ImageView mSpendCategoryIcon;
	private EditText mSpendMemo;
	private int mSpendCategorySelect = -1;
	private String mTextWatcherResult = "";
	private boolean mCancel = false;
	private String mPeriod = EditSpendMonthly.ONETIME;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_spend_history_detail);
		sendTracker(AnalyticsName.Level3ActivitySpendHistoryDetail);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if(mCurrentStateIndex == 1){
			switch (v.getId()){
				case R.id.rl_spend_history_inc_detail_category_select_bg:
					String mStr4 = getString(R.string.spend_category_select);
					DialogUtils.showDlgBaseGridOrList(this, new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
							if (position != -1) {
								if (id == R.id.btn_dialog_base_always) {
									mPeriod = EditSpendMonthly.ALWAYS;
								} else {
									mPeriod = EditSpendMonthly.ONETIME;
								}
								final TypedArray SpendCategoryArrayIcon = getResources().obtainTypedArray(R.array.spend_category_list_img);
								String[] SpendCategoryArray = getResources().getStringArray(R.array.spend_category);
								mSpendCategorySelect = position;
								mSpendCategoryText.setText(SpendCategoryArray[position + 1]);
								mSpendCategoryIcon.setImageResource(SpendCategoryArrayIcon.getResourceId(position + 1, -1));
								complete_button_enable_disable();
							}
						}
					} ,
							new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_G, mStr4, getString(R.string.common_cancel)), mSpendCategorySelect);
					break;

				case R.id.btn_level_3_activity_spend_history_detail_delete:
					final String mStr = getString(R.string.spend_popup_delete);
					DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
										spend_history_delete();
									}
								}
							},
							new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
					break;

				case R.id.ll_spend_history_inc_date_spinner_layout:
					Calendar calendar = Calendar.getInstance();
					String time = mSpendDate.getText().toString();
					long currentTime = calendar.getTimeInMillis()+60*1000*60;
					SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd  a hh:mm");
					Date targetDate = null;
					try {
						targetDate = format.parse(time);
						calendar.setTime(targetDate);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					final String[] tempPickerDate = new String[]{time.substring(0, 10), time.substring(10)};
					final Dialog dialog = new Dialog(getActivity());
					dialog.setContentView(R.layout.dialog_time_picker);
					final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
					datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
						@Override
						public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							String date = year+"."+ TransFormatUtils.transZeroPadding2(Integer.toString(monthOfYear+1))+"."+ TransFormatUtils.transZeroPadding2(Integer.toString(dayOfMonth));
							tempPickerDate[0] = date;
						}
					});
					Calendar nowTime = Calendar.getInstance();
					if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
						datePicker.setMaxDate(currentTime);
					} else {
						datePicker.setMaxDate(currentTime);
					}
					nowTime.add(Calendar.MONTH, -6);
					datePicker.setMinDate(nowTime.getTimeInMillis());
					final TimePicker timePicker = (TimePicker) dialog.findViewById(R.id.timePicker);
					Calendar pastTime = Calendar.getInstance();
					pastTime.setTime(targetDate);
					timePicker.setIs24HourView(true);
					timePicker.setCurrentHour(pastTime.get(Calendar.HOUR_OF_DAY));
					timePicker.setCurrentMinute(pastTime.get(Calendar.MINUTE));
					timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
						@Override
						public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
							String hour = TransFormatUtils.transZeroPadding2(String.valueOf(hourOfDay))+TransFormatUtils.transZeroPadding2(String.valueOf(minute));
							hour = ElseUtils.getHourMinute(hour);
							tempPickerDate[1] = hour;
						}
					});
					dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							if (datePicker.getVisibility() == View.GONE) {
								datePicker.setVisibility(View.VISIBLE);
								timePicker.setVisibility(View.GONE);
								dialog.findViewById(R.id.btn_next).setVisibility(View.VISIBLE);
								dialog.findViewById(R.id.btn_confirm).setVisibility(View.GONE);
							} else {
								dialog.dismiss();
							}
						}
					});
					dialog.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mSpendDate.setText(tempPickerDate[0]+tempPickerDate[1]);
							complete_button_enable_disable();
							dialog.dismiss();
						}
					});
					dialog.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							if (datePicker.getVisibility() == View.VISIBLE) {
								datePicker.setVisibility(View.GONE);
								timePicker.setVisibility(View.VISIBLE);
								v.setVisibility(View.GONE);
								dialog.findViewById(R.id.btn_confirm).setVisibility(View.VISIBLE);
							}
						}
					});
					dialog.show();
//					Calendar calendar = Calendar.getInstance();
//					DatePickerDialog datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
//						@Override
//						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//							mSpendDate.setText(year+"."+TransFormatUtils.transZeroPadding2(Integer.toString(monthOfYear+1)) +"."+TransFormatUtils.transZeroPadding2(Integer.toString(dayOfMonth)));
//							complete_button_enable_disable();
//						}
//					}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//					calendar.add(Calendar.MONTH, -6);
//					datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
//					datePickerDialog.show();
					break;
			}
		}
		else {
			switch (v.getId()){
				case R.id.rl_spend_history_inc_detail_category_select_bg:
					String mStr4 = getString(R.string.spend_category_select);
					DialogUtils.showDlgBaseGridOrList(this, new AdapterView.OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
									if (position != -1) {
										if (id == R.id.btn_dialog_base_always) {
											mPeriod = EditSpendMonthly.ALWAYS;
										} else {
											mPeriod = EditSpendMonthly.ONETIME;
										}
										final TypedArray SpendCategoryArrayIcon = getResources().obtainTypedArray(R.array.spend_category_list_img);
										String[] SpendCategoryArray = getResources().getStringArray(R.array.spend_category);
										mSpendCategorySelect = position;
										mSpendCategoryText.setText(SpendCategoryArray[position + 1]);
										mSpendCategoryIcon.setImageResource(SpendCategoryArrayIcon.getResourceId(position + 1, -1));
										spend_history_add();
										complete_button_enable_disable();
									}
								}
							} ,
							new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_G, mStr4, getString(R.string.common_cancel)), mSpendCategorySelect);
					break;
			}
		}
	}

//=========================================================================================//
// ToolBar - motion
//=========================================================================================//

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if (mCurrentStateIndex == 0) { finish(); }
			else if (mCurrentStateIndex == 1) {
				final String mStr = getString(R.string.spend_popup_history_edit_cancel);
				DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
									mCancel = true;
									mCurrentStateIndex = 0;
									afterMotion("back");
								}
							}
						},
						new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try
		{
			switch (_view.getId()) {

				case R.id.tv_navi_motion_title:
				case R.id.ll_navi_motion_back:
					if (mCurrentStateIndex == 0) { finish(); }
					else if (mCurrentStateIndex == 1) {
						final String mStr = getString(R.string.spend_popup_history_edit_cancel);
						DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
											mCancel = true;
											mCurrentStateIndex = 0;
											afterMotion("back");
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentStateIndex == 0) {
						if(mCancel){
							mCancel = false;
						}
						mCurrentStateIndex = 1;
						afterMotion("else");
						sendTracker(AnalyticsName.Level3ActivitySpendHistoryDetailEdit);
					} else if (mCurrentStateIndex == 1) {
						if(mSpendBreakdown.length() != 0){
							spend_history_add();
							mCurrentStateIndex = 0;
							afterMotion("back");
						}
						else {
							Toast.makeText(Level3ActivitySpendHistoryDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
						}
					}
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}
	
	private void afterMotion(String _Divider){
//		DivierLayout();
		int count = 0;
		if(_Divider.equals("else")){
			count = 0;
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_35_0_0_400_fill);
//			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_002_0_0_0_400_fill);
			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_layout_right);
			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_left);
			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_left);
			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_down);
		}
		else{	// default
			count = 400;
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_35_0_0_0_400_fill);
//			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_002_0_0_400_fill);
			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_layout_left);
			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_right);
			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_up);
			mAnimDelete.setAnimationListener(myAnimationListener);
		}

		((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);

		if("Y".equals(mInfoDataSpendHistory.getEditYn())){
			mSpendMemoirs.setVisibility(View.VISIBLE);
			mDelete.startAnimation(mAnimDelete);
			mDelete.setVisibility(View.VISIBLE);
		}
		else {
			mSpendMemoirs.setVisibility(View.GONE);
			mDelete.startAnimation(mAnimDelete);
			mDelete.setVisibility(View.VISIBLE);
		}

		mSpendSum.startAnimation(mAnimSpinnerEditText);
		mSpendBreakdown.startAnimation(mAnimSpinnerEditText);

		mSpendDataLL.startAnimation(mAnimSpinnerLayout);
		mSpendDate.startAnimation(mAnimSpinnerText);
		mSpendWay.startAnimation(mAnimSpinnerEditText);
		mSpendCategoryRL.startAnimation(mAnimSpinnerEditText);


		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				DivierLayout();
			}
		}, count);
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getParcelableExtra */
			mInfoDataSpendHistory = (SpendMonthlyInfo) getIntent().getSerializableExtra(Const.SPEND_HISTORY_DETAIL);
			if(mInfoDataSpendHistory == null)
				bResult = false;
			else {
				mPeriod = mInfoDataSpendHistory.getPeriod();
			}

		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(R.color.main_2_layout_bg);
			mDelete = (Button)findViewById(R.id.btn_level_3_activity_spend_history_detail_delete);
			mDelete.setOnClickListener(this);

			/* Layout */
			mSpendMemoirs = (RelativeLayout)findViewById(R.id.rl_level_3_activity_spend_history_memoirs);
			mSpendSumText = (TextView)findViewById(R.id.et_spend_history_inc_detail_sum_text);
			mSpendSum = (EditText)findViewById(R.id.et_spend_history_inc_detail_sum_value);
			mSpendSum.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					if(!s.toString().equals(mTextWatcherResult)){
						if("".equals(s.toString())){
							mSpendSum.setHint(getString(R.string.common_input_sum_hint));
						}
						else {
							try {
								// The comma in the format specifier does the trick
								mTextWatcherResult = String.format("%,d", Long.parseLong(s.toString().replaceAll(",", "")));
							}
							catch (NumberFormatException e) {
								Log.e("broccoli", "afterTextChanged() : " + e.toString());
							}
							mSpendSum.setText(mTextWatcherResult);
							mSpendSum.setSelection(mTextWatcherResult.length());
						}
						complete_button_enable_disable();
					}
				}
			});
			mSpendBreakdownText = (TextView)findViewById(R.id.tv_spend_history_inc_detail_breakdown_text);
			mSpendBreakdown = (EditText)findViewById(R.id.et_spend_history_inc_detail_breakdown);
			mSpendBreakdown.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					if(s.length() == 0){
						mSpendBreakdown.setHint(getString(R.string.common_content_input));
					}
					complete_button_enable_disable();
				}
			});
			mSpendDataLL = (LinearLayout)findViewById(R.id.ll_spend_history_inc_date_spinner_layout);
			if("Y".equals(mInfoDataSpendHistory.getEditYn())){
				mSpendDataLL.setOnClickListener(this);
			}
			mSpendDateText = (TextView)findViewById(R.id.tv_spend_history_inc_date_title);
			mSpendDate = (TextView)findViewById(R.id.tv_spend_history_inc_date_spinner_text);
			mSpendWayText = (TextView)findViewById(R.id.tv_spend_history_inc_detail_spend_way_text);
			mSpendWay = (TextView)findViewById(R.id.tv_spend_history_inc_detail_spend_way_value);
			mSpendCategoryRL = (RelativeLayout)findViewById(R.id.rl_spend_history_inc_detail_category_select_bg);
			mSpendCategoryRL.setOnClickListener(this);

			mSpendCategoryText = (TextView)findViewById(R.id.tv_spend_history_inc_detail_category_select);
			mSpendCategoryIcon = (ImageView)findViewById(R.id.imv_spend_history_inc_detail_icon);
			mSpendMemo = (EditText) findViewById(R.id.ed_spend_history_inc_detail_memo);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCurrentStateIndex = 0;    // 상세화면
			DivierLayout();

			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
			mAnimSpinnerText.setDuration(0);
			mAnimSpinnerText.setFillAfter(true);

			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_right);
			mAnimSpinnerEditText.setDuration(0);
			mAnimSpinnerEditText.setFillAfter(true);

			int typePay = Integer.parseInt(mInfoDataSpendHistory.getPayType());
			int typeCategory = Integer.parseInt(mInfoDataSpendHistory.getCategoryId());
			String[] payType = getResources().getStringArray(R.array.spend_pay_type);
			String[] categoryType = getResources().getStringArray(R.array.spend_category);
			TypedArray spend_list_img = getResources().obtainTypedArray(R.array.spend_category_list_img);
			spend_list_img.getResourceId(typeCategory, -1);
			mSpendCategorySelect = typeCategory - 1;// 인덱스 맞추기 위해 -1

			if("Y".equals(mInfoDataSpendHistory.getEditYn())){
				mSpendMemoirs.setVisibility(View.VISIBLE);
			}
			else {
				mSpendMemoirs.setVisibility(View.GONE);
			}
			mSpendWay.setText(payType[5]);

			mSpendSum.setText(ElseUtils.getStringDecimalFormat(mInfoDataSpendHistory.getSum()));
			mSpendBreakdown.setText(mInfoDataSpendHistory.getOpponent());
			mSpendDate.setText(ElseUtils.getSpendDate(mInfoDataSpendHistory.getExpenseDate()));
			if(typePay > 5){
			}
			else {
				mSpendWay.setText(payType[typePay]);
			}
			mSpendCategoryText.setText(categoryType[typeCategory]);
			mSpendMemo.setText(mInfoDataSpendHistory.getMemo());
			mSpendCategoryIcon.setImageResource(spend_list_img.getResourceId(typeCategory, -1));
			spend_list_img.recycle();

			mSpendSum.startAnimation(mAnimSpinnerEditText);
			mSpendBreakdown.startAnimation(mAnimSpinnerEditText);
			mSpendDate.startAnimation(mAnimSpinnerText);
			mSpendWay.startAnimation(mAnimSpinnerEditText);
			mSpendCategoryRL.startAnimation(mAnimSpinnerEditText);

//			mDelete.startAnimation(mAnimDelete);
			mDelete.setVisibility(View.GONE);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void DivierLayout(){
		if (mCurrentStateIndex == 0){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.spend_history_detail));
			((TextView) findViewById(R.id.tv_navi_motion_else)).setText(getString(R.string.common_edit));
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			mSpendSumText.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
			mSpendSum.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
			mSpendBreakdown.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
			mSpendDateText.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
			mSpendDate.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
			mSpendWayText.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
			mSpendWay.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
			mSpendSum.setEnabled(false);
			mSpendBreakdown.setEnabled(false);
			mSpendDataLL.setEnabled(false);
			mSpendMemo.setEnabled(false);
			mSpendSum.setBackgroundResource(0);
			mSpendBreakdown.setBackgroundResource(0);
			mSpendDataLL.setBackgroundResource(R.drawable.selector_spinner_bg_open);
			if("".equals(mSpendDate.getText().toString())){
				mSpendDate.setText(ElseUtils.getSpendDate(mInfoDataSpendHistory.getExpenseDate()));
			}
			else {
				mSpendDate.setText(mSpendDate.getText().toString());
			}

			dataReload();// cancel data set
		}
		else{
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.spend_history_edit));
			((TextView) findViewById(R.id.tv_navi_motion_else)).setText(getString(R.string.common_complete));
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);

			if("Y".equals(mInfoDataSpendHistory.getEditYn())) // 수기 입력
			{
				mSpendSumText.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				mSpendSum.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				mSpendBreakdown.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				mSpendDateText.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				mSpendDate.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				mSpendWayText.setTextColor(getResources().getColor(R.color.common_rgb_191_191_191));
				mSpendWay.setTextColor(getResources().getColor(R.color.common_rgb_191_191_191));
				mSpendSum.setEnabled(true);
				mSpendBreakdown.setEnabled(true);
				mSpendDataLL.setEnabled(true);
				mSpendMemo.setEnabled(true);
				mSpendSum.setBackgroundResource(R.drawable.selector_edittext_mode_bg);
				mSpendBreakdown.setBackgroundResource(R.drawable.selector_edittext_mode_bg);
				mSpendDataLL.setBackgroundResource(R.drawable.selector_spinner_bg);
				mSpendDate.setText(ElseUtils.getDate(mInfoDataSpendHistory.getExpenseDate()));
			}
			else {
				mSpendSumText.setTextColor(getResources().getColor(R.color.common_rgb_191_191_191));
				mSpendSum.setTextColor(getResources().getColor(R.color.common_rgb_191_191_191));
				mSpendBreakdown.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				mSpendDateText.setTextColor(getResources().getColor(R.color.common_rgb_191_191_191));
				mSpendDate.setTextColor(getResources().getColor(R.color.common_rgb_191_191_191));
				mSpendWayText.setTextColor(getResources().getColor(R.color.common_rgb_191_191_191));
				mSpendWay.setTextColor(getResources().getColor(R.color.common_rgb_191_191_191));
				mSpendSum.setEnabled(false);
				mSpendBreakdown.setEnabled(true);
				mSpendDataLL.setEnabled(false);
				mSpendMemo.setEnabled(true);
				mSpendSum.setBackgroundResource(0);
				mSpendBreakdown.setBackgroundResource(R.drawable.selector_edittext_mode_bg);
				mSpendDataLL.setBackgroundResource(0);
				mSpendDate.setText(ElseUtils.getDate(mInfoDataSpendHistory.getExpenseDate()));

			}
		}
	}

//=========================================================================================//
// Spend History Add API
//=========================================================================================//
	private void spend_history_add() {
		DialogUtils.showLoading(this);
		long now = System.currentTimeMillis();
		Date date = new Date(now);
		SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
		SimpleDateFormat checkDate = new  SimpleDateFormat("yyyy.MM.dd", Locale.KOREA);
		DecimalFormat decimalFormat = new DecimalFormat("00");
		EditSpendMonthly mEditSpendMonthly = new EditSpendMonthly();
		int category = mSpendCategorySelect + 1;
		String sum = mSpendSum.getText().toString().replaceAll(",", "");

		mEditSpendMonthly.setUserId(Session.getInstance(this).getUserId());
		mEditSpendMonthly.setExpenseId(mInfoDataSpendHistory.getExpenseId());

		if("Y".equals(mInfoDataSpendHistory.getEditYn())){
			if(checkDate.format(date).equals(mSpendDate.getText().toString())){
				mEditSpendMonthly.setExpenseDate(dateFormat.format(date));
			}
			else {
				mEditSpendMonthly.setExpenseDate(ElseUtils.getServerDate(mSpendDate.getText().toString()));
			}
		}
		else {
			mEditSpendMonthly.setExpenseDate(mInfoDataSpendHistory.getExpenseDate());
		}
		mEditSpendMonthly.setOpponent(mSpendBreakdown.getText().toString());
		if(category == 25)
		{
			mEditSpendMonthly.setCategoryId("25");
		}
		else {
			mEditSpendMonthly.setCategoryId(decimalFormat.format(category));
		}

		mEditSpendMonthly.setPayType(mInfoDataSpendHistory.getPayType());
		mEditSpendMonthly.setEditYn(mInfoDataSpendHistory.getEditYn());
		mEditSpendMonthly.setSum(sum);
		mEditSpendMonthly.setMemo(mSpendMemo.getText().toString().trim());
		mEditSpendMonthly.setPeriodType(mPeriod);

		mInfoDataSpendHistory = mEditSpendMonthly; // 완료시 정보 기본 데이터에 저장

		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<Result> call = api.editSpend(mEditSpendMonthly);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {

					} else {
						ElseUtils.network_error(getBase());
					}
				} else {
					ElseUtils.network_error(getBase());
				}
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(Level3ActivitySpendHistoryDetail.this);
			}
		});
		findViewById(R.id.ed_spend_history_inc_detail_memo_parent).requestFocus();
	}

//=========================================================================================//
// Spend History Delete API
//=========================================================================================//
	private void spend_history_delete() {
		DeleteSpendMonthly mDeleteSpendMonthly = new DeleteSpendMonthly();
		mDeleteSpendMonthly.setUserId(Session.getInstance(this).getUserId());
		mDeleteSpendMonthly.setExpenseId(mInfoDataSpendHistory.getExpenseId());
		mDeleteSpendMonthly.setEditYn(mInfoDataSpendHistory.getEditYn());

		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<Result> call = api.deleteSpend(mDeleteSpendMonthly);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						finish();
					} else {
						ElseUtils.network_error(getBase());
					}
				} else {
					ElseUtils.network_error(getBase());
				}
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				ElseUtils.network_error(Level3ActivitySpendHistoryDetail.this);
			}
		});
	}

	private void dataReload(){
		if(mCancel){
			int typePay = Integer.parseInt(mInfoDataSpendHistory.getPayType());
			int typeCategory = Integer.parseInt(mInfoDataSpendHistory.getCategoryId());
			String[] payType = getResources().getStringArray(R.array.spend_pay_type);
			String[] categoryType = getResources().getStringArray(R.array.spend_category);
			TypedArray spend_list_img = getResources().obtainTypedArray(R.array.spend_category_list_img);
			spend_list_img.getResourceId(typeCategory, -1);
			mSpendCategorySelect = typeCategory - 1;// 인덱스 맞추기 위해 -1

			if("Y".equals(mInfoDataSpendHistory.getEditYn())){
				mSpendMemoirs.setVisibility(View.VISIBLE);
			}
			else {
				mSpendMemoirs.setVisibility(View.GONE);
			}
			mSpendWay.setText(payType[5]);

			mSpendSum.setText(ElseUtils.getStringDecimalFormat(mInfoDataSpendHistory.getSum()));
			mSpendBreakdown.setText(mInfoDataSpendHistory.getOpponent());
			mSpendMemo.setText(mInfoDataSpendHistory.getMemo());
			mSpendDate.setText(ElseUtils.getSpendDate(mInfoDataSpendHistory.getExpenseDate()));
			if(typePay > 5){
			}
			else {
				mSpendWay.setText(payType[typePay]);
			}
			mSpendCategoryText.setText(categoryType[typeCategory]);
			mSpendCategoryIcon.setImageResource(spend_list_img.getResourceId(typeCategory, -1));
			spend_list_img.recycle();
		}
		else {

		}
	}


	private void complete_button_enable_disable(){
		if(empty_check()){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}

	private boolean empty_check(){
		if(mSpendSum.length() == 0 || mSpendCategorySelect == -1 || mSpendBreakdown.length() == 0){
			return false;
		}
		return true;
	}
}
