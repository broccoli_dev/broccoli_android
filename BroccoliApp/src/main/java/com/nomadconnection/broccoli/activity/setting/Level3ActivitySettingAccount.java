package com.nomadconnection.broccoli.activity.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;


public class Level3ActivitySettingAccount extends BaseActivity {

	private static final String TAG = Level3ActivitySettingAccount.class.getSimpleName();

	/* Layout */
	private TextView mUserEmail;
	private View mUserLogout;

	/**  Button Click Listener **/
	private OnClickListener mBtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.rl_activity_setting_dayli_user_logoout:
					logout();
					break;
				default:
					break;
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_setting_daylipass_id);
		sendTracker(AnalyticsName.SettingRegEmail);
		initWidget();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mUserEmail != null) {
			mUserEmail.post(new Runnable() {
				@Override
				public void run() {
					Session.getInstance(getBase()).refreshUserPersonalInfo(new Session.OnUserInfoUpdateCompleteInterface() {
						@Override
						public void complete(UserPersonalInfo info) {
							showUserEmail(info);
						}

						@Override
						public void error(ErrorMessage message) {
							switch (message) {
								case ERROR_713_NO_SUCH_USER:
								case ERROR_10007_SESSION_NOT_FOUND:
								case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
								case ERROR_20003_MISMATCH_ACCESS_TOKEN:
								case ERROR_20004_NO_SUCH_USER:
									showTokenExpire();
									break;
							}
						}
					});
				}
			});
		}
	}

	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		finish();
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.setting_user_account));
			findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));

			/* Layout */
			mUserEmail = (TextView) findViewById(R.id.tv_activity_setting_dayli_email);
			mUserLogout = findViewById(R.id.rl_activity_setting_dayli_user_logoout);

			mUserLogout.setOnClickListener(mBtnClickListener);
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void showUserEmail(UserPersonalInfo info) {
		if (info != null) {
			mUserEmail.setText(info.user.email);
			if (info.user.emailCertYn) {
				mUserEmail.setTextColor(getResources().getColor(R.color.common_subtext_color));
			} else {
				mUserEmail.setTextColor(getResources().getColor(R.color.common_subtext_color));
			}
		}
	}

	private void logout() {
		CommonDialogTextType dialogTextType = new CommonDialogTextType(this, new int[] {R.string.common_no, R.string.common_yes});
		dialogTextType.addTextView("정말로 로그아웃 하시겠어요?");
		dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				switch (id) {
					case R.string.common_yes:
						requestLogout();
						break;
					default:
						dialog.dismiss();
						break;
				}
			}
		});
		dialogTextType.show();
	}

	private void requestLogout() {
		startActivity(new Intent(this, LogoutCompleteActivity.class));
		finish();
	}
}
