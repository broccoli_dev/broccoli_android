package com.nomadconnection.broccoli.activity.membership;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.setting.SettingCompleteActivity;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.data.Dayli.DayliChangePwdInfo;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.BroccoliLoginFilter;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2016. 12. 16..
 */

public class MembershipResetPwdActivity extends BaseActivity {

    public static final String MODE_RESET = "RESET";
    public static final String MODE_CHANGE = "CHANGE";
    public static final String FROM_LOGIN = "FROM_LOGIN";

    private EditText mPwd;
    private EditText mPwdCheck;
    private TextView mResult;
    private View mBtn;

    private boolean isPwdAvailable = false;
    private boolean isPwdMatched = false;
    private boolean isResetMode = false;
    private boolean isFromLogin = false;

    private String mOldPwd = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_reset_password);
        init();
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

    private void init() {
        Intent intent = getIntent();
        if (intent.hasExtra(MODE_RESET)) {
            isResetMode = true;
            sendTracker(AnalyticsName.FindPwResult);
        } else {
            mOldPwd = intent.getStringExtra(Const.DATA);
            isResetMode = false;
            ((TextView)findViewById(R.id.btn_confirm)).setText("확인");
            sendTracker(AnalyticsName.SettingRegPwChangeOn);
        }
        if (!intent.hasExtra(FROM_LOGIN)) {
            findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        } else {
            isFromLogin = true;
        }

        if (isResetMode) {
            ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.membership_reset_pwd_title);
            ((TextView)findViewById(R.id.tv_activity_reset_pwd_subtitle)).setText(R.string.membership_reset_pwd_guide);
        } else {
            ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.membership_reset_change_pwd_title);
            ((TextView)findViewById(R.id.tv_activity_reset_pwd_subtitle)).setText(R.string.membership_reset_change_pwd_guide);
        }
        mPwd = (EditText) findViewById(R.id.et_activity_membership_password);
        mPwdCheck = (EditText) findViewById(R.id.et_activity_membership_password_check);
        mResult = (TextView) findViewById(R.id.tv_activity_membership_password_result);

        InputFilter[] filters = new InputFilter[]{new BroccoliLoginFilter(new BroccoliLoginFilter.OnInputListener() {
            @Override
            public void isAllowed(BroccoliLoginFilter.InputResult allowed) {
                isPwdAvailable = false;
                switch (allowed) {
                    case ALLOWED:
                        isPwdAvailable = true;
                        mResult.setText("사용 가능한 비밀번호");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                        break;
                    case NEED_INCLUDE_NUMBER:
                    case NEED_INCLUDE_CHAR:
                        mResult.setText("영문과 숫자를 함께 사용해야 합니다.");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case NOT_ALLOWED_CHARACTER:
                        mResult.setText("!@#$%^&*()만 입력 가능합니다.");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case NOT_ENOUGH_LENGTH:
                        mResult.setText("8~20자리까지 입력 가능합니다.");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case TOO_LONG_LENGTH:
                        mResult.setText("8~20자리까지 입력 가능합니다.");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                }
            }
        })};
        mPwd.setFilters(filters);

        mPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwdCheck = mPwdCheck.getText().toString();
                isPwdMatched = false;
                if (isPwdAvailable && s.length() != 0 && pwdCheck.length() > 0) {
                    if (pwdCheck.equalsIgnoreCase(s.toString())) {
                        isPwdMatched = true;
                        mResult.setText("비밀번호 일치");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    } else {
                        mResult.setText("비밀번호가 일치하지 않습니다.");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    }
                }
                checkButtonStatus();
            }
        });

        mPwdCheck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwd = mPwd.getText().toString();
                isPwdMatched = false;
                if (isPwdAvailable && s.length() != 0) {
                    if (pwd.equalsIgnoreCase(s.toString())) {
                        isPwdMatched = true;
                        mResult.setText("비밀번호 일치");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    } else {
                        mResult.setText("비밀번호가 일치하지 않습니다.");
                        mResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    }
                }
                checkButtonStatus();
            }
        });

        mBtn = findViewById(R.id.btn_confirm);
        mBtn.setEnabled(false);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isResetMode) {
                    resetPwd();
                } else {
                    changePwd();
                }
            }
        });
    }

    private void checkButtonStatus() {
        boolean ret = true;
        if (!isPwdMatched) {
            ret = false;
        }
        if (ret) {
            mBtn.setEnabled(true);
        } else {
            mBtn.setEnabled(false);
        }
    }

    //실명인증 후 사용된다.
    private void resetPwd() {
        DialogUtils.showLoading(this);
        String pwd = mPwd.getText().toString();
        DayliApi api = ServiceGenerator.createServiceDayli(DayliApi.class);
        HashMap<String, Object> body = new HashMap<>();
        body.put("deviceId", ElseUtils.getDeviceUUID(this));
        body.put("newPassword", pwd);
        Call<Result> call = api.dayliPasswordReset(body);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    pwdResetComplete();
                } else {
                    ErrorMessage message = ErrorUtils.parseError(response);
                    switch (message) {
                        case ERROR_713_NO_SUCH_USER:
                            Toast.makeText(getBase(), "가입되지 않은 사용자입니다. 새로 회원가입해주세요.", Toast.LENGTH_LONG).show();
                            break;
                        case ERROR_719_QUITE_USER:
                            Toast.makeText(getBase(), "탈퇴 회원은 탈퇴일로부터 5일 이후 재가입 가능합니다.", Toast.LENGTH_LONG).show();
                            break;
                        case ERROR_1011_CERT_SESSION_EXPIRE:
                            Toast.makeText(getBase(), "인증시간이 만료되었습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                            break;
                        case ERROR_1012_CERT_NOT_FINISH:
                            Toast.makeText(getBase(), "인증에 실패하였습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getBase());
            }
        });
    }

    //실명인증 없이 사용
    private void changePwd() {
        DialogUtils.showLoading(this);
        final String pwd = mPwd.getText().toString();
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                DialogUtils.dismissLoading();
                LoginData data = DbManager.getInstance().getUserInfoDataSet(getBase(), str, Session.getInstance(getBase()).getUserId());
                changePwdProcess(pwd, data);
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void changePwdProcess(String renewalPwd, LoginData data) {
        DayliChangePwdInfo info = new DayliChangePwdInfo();
        info.setNewPassword(renewalPwd);
        info.setOldPassword(mOldPwd);
        info.setUserId(data.getDayliId());
        DayliApi api = ServiceGenerator.createServiceDayli(DayliApi.class);
        Call<Result> call = api.dayliChangePassword(data.getDayliToken(), info);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    pwdChangeComplete();
                } else {
                    ErrorMessage message = ErrorUtils.parseError(response);
                    switch (message) {
                        case ERROR_701_INCORRECT_ACCOUNT:
                            break;
                        case ERROR_719_QUITE_USER:
                            break;
                        case ERROR_713_NO_SUCH_USER:
                            break;
                        case ERROR_717_NOT_REG_APP:
                            break;
                        case ERROR_716_TOKEN_EXPIRE:
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getBase());
            }
        });
    }

    private void pwdChangeComplete() {
        Intent intent = new Intent(MembershipResetPwdActivity.this, SettingCompleteActivity.class);
        intent.putExtra(Const.TITLE, "비밀번호 변경 완료");
        intent.putExtra(Const.CONTENT, "비밀번호 변경이 완료되었습니다.");
        startActivity(intent);
        finish();
    }

    private void pwdResetComplete() {
        Intent intent = new Intent(MembershipResetPwdActivity.this, SettingCompleteActivity.class);
        intent.putExtra(Const.TITLE, "비밀번호 재설정 완료");
        intent.putExtra(Const.CONTENT, "비밀번호 재설정이 완료되었습니다.");
        startActivity(intent);
        finish();
    }

}
