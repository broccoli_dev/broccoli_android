package com.nomadconnection.broccoli.activity.setting;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.BroccoliLoginFilter;

/**
 * Created by YelloHyunminJang on 2016. 12. 16..
 */

public class Level4SettingDayliPwdChangeActivity extends BaseActivity {

    private EditText mPassWard;
    private EditText mPassWardCheck;
    private TextView mPassWardResult;
    private View mBtn;
    private boolean isPwdAvailable = false;
    private boolean isPwdMatched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_setting_dayli_change_password);
        sendTracker(AnalyticsName.SettingRegPwChange);
        init();
    }

    private void init() {

        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText("비밀번호 변경하기");

        mPassWard = (EditText) findViewById(R.id.et_activity_membership_password);
        mPassWardCheck = (EditText) findViewById(R.id.et_activity_membership_password_check);
        mPassWardResult = (TextView) findViewById(R.id.tv_activity_membership_password_result);
        mBtn = findViewById(R.id.btn_confirm);

        InputFilter[] filters = new InputFilter[]{new BroccoliLoginFilter(new BroccoliLoginFilter.OnInputListener() {
            @Override
            public void isAllowed(BroccoliLoginFilter.InputResult allowed) {
                isPwdAvailable = false;
                switch (allowed) {
                    case ALLOWED:
                        isPwdAvailable = true;
                        mPassWardResult.setText("사용 가능한 비밀번호");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                        break;
                    case NEED_INCLUDE_NUMBER:
                    case NEED_INCLUDE_CHAR:
                        mPassWardResult.setText("영문과 숫자를 함께 사용해야 합니다.");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case NOT_ALLOWED_CHARACTER:
                        mPassWardResult.setText("!@#$%^&*()만 입력 가능합니다.");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case NOT_ENOUGH_LENGTH:
                        mPassWardResult.setText("8~20자리까지 입력 가능합니다.");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case TOO_LONG_LENGTH:
                        mPassWardResult.setText("8~20자리까지 입력 가능합니다.");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                }
            }
        })};
        mPassWard.setFilters(filters);

        mPassWard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwdCheck = mPassWardCheck.getText().toString();
                isPwdMatched = false;
                if (isPwdAvailable && s.length() != 0 && pwdCheck.length() > 0) {
                    if (pwdCheck.equalsIgnoreCase(s.toString())) {
                        isPwdMatched = true;
                        mPassWardResult.setText("비밀번호 일치");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    } else {
                        mPassWardResult.setText("비밀번호가 일치하지 않습니다.");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    }
                }
                checkButtonStatus();
            }
        });

        mPassWardCheck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwd = mPassWard.getText().toString();
                isPwdMatched = false;
                if (isPwdAvailable && s.length() != 0) {
                    if (pwd.equalsIgnoreCase(s.toString())) {
                        isPwdMatched = true;
                        mPassWardResult.setText("비밀번호 일치");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    } else {
                        mPassWardResult.setText("비밀번호가 일치하지 않습니다.");
                        mPassWardResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPassWardResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    }
                }
                checkButtonStatus();
            }
        });

        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm();
            }
        });
    }

    private void checkButtonStatus() {
        boolean ret = true;
        if (!isPwdMatched) {
            ret = false;
        }
        if (ret) {
            mBtn.setEnabled(true);
        } else {
            mBtn.setEnabled(false);
        }
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

    private void confirm() {

    }
}
