package com.nomadconnection.broccoli.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Asset.AssetCarInfo;
import com.nomadconnection.broccoli.data.Asset.AssetEstateInfo;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Asset.DeleteAssetCar;
import com.nomadconnection.broccoli.data.Asset.DeleteAssetEstate;
import com.nomadconnection.broccoli.data.Asset.EditAssetCar;
import com.nomadconnection.broccoli.data.Asset.EditAssetEstate;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarCode;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarMarketPrice;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarSubCode;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarYear;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarMakerCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarMarketPrice;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarSubCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarYear;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level3ActivityAssetOtherDetail extends BaseActivity {

	private static final String TAG = Level3ActivityAssetOtherDetail.class.getSimpleName();

	public static int mHouseOrCarIndex = 0;		// default 0: 부동산 , 1: 차량
	public static int mCurrentStateIndex = 0;		// default 0: 상세 , 1: 편집

	/* Layout*/
	private Animation mAnimTitle;
	private Animation mAnimSpinnerLayout;
	private Animation mAnimSpinnerText;
	private Animation mAnimSpinnerEditText;
	private Animation mAnimDelete;
	private Button mDelete;

	/* Layout House*/
	private LinearLayout mHouseLayout;
	private LinearLayout mHouseSpinner1;
	private LinearLayout mHouseSpinner2;
	private EditText mHouseInfo0;
	private TextView mHouseInfo1;
	private TextView mHouseInfo2;
	private EditText mHouseInfo3;
	private String mTextWatcherHouseResult = "";

	private ArrayList<String> mHouseSpinner1Data;
	private ArrayList<String> mHouseSpinner2Data;
//	private int mHouseSpinner1Pos;
//	private int mHouseSpinner2Pos;

	/* Layout Car*/
	private LinearLayout mCarLayout;
	private LinearLayout mCarSpinner1;
	private LinearLayout mCarSpinner2;
	private LinearLayout mCarSpinner3;
	private LinearLayout mCarSpinner4;
	private TextView mCarInfo1;
	private TextView mCarInfo2;
	private TextView mCarInfo3;
	private TextView mCarInfo4;
	private EditText mCarInfo5;
//	private String mTextWatcherCarResult = "";

	private ArrayList<String> mCarSpinner1Data;
	private ArrayList<String> mCarSpinner2Data;
	private ArrayList<String> mCarSpinner3Data;
	private ArrayList<String> mCarSpinner4Data;
	private int mCarSpinner1Pos;
	private int mCarSpinner2Pos;
	private int mCarSpinner3Pos;
	private int mCarSpinner4Pos;

	/* getParcelableExtra */
	private AssetEstateInfo mInfoDataHouse;
	private AssetCarInfo mInfoDataCar;

//	/* Backup Data */
//	private AssetEstateInfo mBackupInfoDataHouse;
//	private AssetCarInfo mBackupInfoDataCar;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_asset_other_detail);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		onBtnClickNaviMotion(findViewById(R.id.ll_navi_motion_back));
//		super.onBackPressed();
	}




//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try {
			switch (_view.getId()) {
				case R.id.tv_navi_motion_title:
					if (mCurrentStateIndex == 1)
						break;

				case R.id.ll_navi_motion_back:
					if (mCurrentStateIndex == 0) { goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_SUCCESS); }
					else if (mCurrentStateIndex == 1) {
						String mStr = "기타 재산 수정\n취소하시겠습니까?";
						DialogUtils.showDlgBaseTwoButton(Level3ActivityAssetOtherDetail.this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
											resetData(); mCurrentStateIndex = 0; afterMotion("back");
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
//						resetData(); mCurrentStateIndex = 0; afterMotion("back");
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentStateIndex == 0) { mCurrentStateIndex = 1; afterMotion("else");}
					else if (mCurrentStateIndex == 1) {
						if (mHouseOrCarIndex == 0){
							DialogUtils.showLoading(Level3ActivityAssetOtherDetail.this);											// 자산 로딩
							Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).editEstate(
//									new EditAssetEstate(Session.getInstance(this).getUserId(), mInfoDataHouse.mAssetEstateId,
//											mHouseInfo0.getText().toString(), TransFormatUtils.transZeroPadding2(mInfoDataHouse.mAssetEstateType),
//											TransFormatUtils.transZeroPadding2(mInfoDataHouse.mAssetEstateDealType), TransFormatUtils.transRemoveComma(mHouseInfo3.getText().toString()))
									new EditAssetEstate(Session.getInstance(this).getUserId(), mInfoDataHouse.mAssetEstateId,
											mHouseInfo0.getText().toString(), TransFormatUtils.transZeroPadding2(TransFormatUtils.transEstateTypeNameToPos(mHouseInfo1.getText().toString())),
											TransFormatUtils.transZeroPadding2(TransFormatUtils.transDealTypeNameToPos(mHouseInfo2.getText().toString())), TransFormatUtils.transRemoveComma(mHouseInfo3.getText().toString()))
							);
							mCall.enqueue(new Callback<Result>() {
								@Override
								public void onResponse(Call<Result> call, Response<Result> response) {
									DialogUtils.dismissLoading();											// 자산 로딩
									if (response.isSuccessful() && response.body() != null)
										NextStepEditHouse(response.body());
								}

								@Override
								public void onFailure(Call<Result> call, Throwable t) {
									DialogUtils.dismissLoading();											// 자산 로딩
									ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
								}
							});
						}
						else{
							DialogUtils.showLoading(Level3ActivityAssetOtherDetail.this);											// 자산 로딩
							Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).editCar(
									new EditAssetCar(Session.getInstance(this).getUserId(), mInfoDataCar.mAssetCarId,
											mCarInfo1.getText().toString(), mCarInfo2.getText().toString(),
											mCarInfo3.getText().toString(), mCarInfo4.getText().toString())
							);
							mCall.enqueue(new Callback<Result>() {
								@Override
								public void onResponse(Call<Result> call, Response<Result> response) {
									DialogUtils.dismissLoading();											// 자산 로딩
									if (response.isSuccessful() && response.body() != null) {
										NextStepEditCar(response.body());
									}
								}

								@Override
								public void onFailure(Call<Result> call, Throwable t) {
									DialogUtils.dismissLoading();											// 자산 로딩
									ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
								}
							});
						}
					}
					break;

				default:
					break;
			}
		} catch(Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}




	private void afterMotion(String _Divider){
		int count = 0;
		if(_Divider.equals("else")){
			count = 0;
			if(mHouseOrCarIndex == 0){
				mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_35_0_0_400_fill);
				mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_left);
			}
			else {
				mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_04_0_0_400_fill);
				mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_left_09);
			}
			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_layout_right);
			//mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_left);
			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_left);
			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_down);
		} else{	// default
			count = 400;
			if(mHouseOrCarIndex == 0){
				mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_35_0_0_0_400_fill);
				mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
			}
			else {
				mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_04_0_0_0_400_fill);
				mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right_09);
			}
			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_layout_left);
			//mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_right);
			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_up);
			mAnimDelete.setAnimationListener(myAnimationListener);
		}

		((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);
		mDelete.startAnimation(mAnimDelete);
		mDelete.setVisibility(View.VISIBLE);
		if (mHouseOrCarIndex == 0){
			mHouseSpinner1.startAnimation(mAnimSpinnerLayout);
			mHouseSpinner2.startAnimation(mAnimSpinnerLayout);

			mHouseInfo1.startAnimation(mAnimSpinnerText);
			mHouseInfo2.startAnimation(mAnimSpinnerText);
			mHouseInfo0.startAnimation(mAnimSpinnerEditText);
			mHouseInfo3.startAnimation(mAnimSpinnerEditText);
		}
		else{
			mCarSpinner1.startAnimation(mAnimSpinnerLayout);
			mCarSpinner2.startAnimation(mAnimSpinnerLayout);
			mCarSpinner3.startAnimation(mAnimSpinnerLayout);
			mCarSpinner4.startAnimation(mAnimSpinnerLayout);

			mCarInfo1.startAnimation(mAnimSpinnerText);
			mCarInfo2.startAnimation(mAnimSpinnerText);
			mCarInfo3.startAnimation(mAnimSpinnerText);
			mCarInfo4.startAnimation(mAnimSpinnerText);
//			mCarInfo5.startAnimation(mAnimSpinnerEditText);
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				DivierLayout();
			}
		}, count);
	}







//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mInfoDataHouse = getIntent().getParcelableExtra(APP.INFO_DATA_ASSET_OTHER_HOUSE_DEPTH_2);
			mInfoDataCar = getIntent().getParcelableExtra(APP.INFO_DATA_ASSET_OTHER_CAR_DEPTH_2);

			if(mInfoDataHouse == null && mInfoDataCar == null) bResult = false;
			else{
				if (getIntent().getStringExtra(APP.INFO_DATA_ASSET_OTHER_HOUSE_OR_CAR_DEPTH_2).equals("부동산")) {
					mHouseOrCarIndex = 0;
					sendTracker(AnalyticsName.Level3ActivityAssetOtherDetailRE);
				} else {
					mHouseOrCarIndex = 1;
					sendTracker(AnalyticsName.Level3ActivityAssetOtherDetailCAR);
				}
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout */
			mDelete = (Button)findViewById(R.id.btn_level_3_activity_asset_other_detail_delete);

			/* Layout House*/
			mHouseLayout = (LinearLayout)findViewById(R.id.ll_asset_other_inc_house);
			mHouseSpinner1 = (LinearLayout)findViewById(R.id.ll_asset_other_inc_house_spinner_layout_1);
			mHouseSpinner2 = (LinearLayout)findViewById(R.id.ll_asset_other_inc_house_spinner_layout_2);
			mHouseInfo1 = (TextView)findViewById(R.id.tv_asset_other_inc_house_spinner_text_1);
			mHouseInfo2 = (TextView)findViewById(R.id.tv_asset_other_inc_house_spinner_text_2);
			mHouseInfo0 = (EditText)findViewById(R.id.et_asset_other_inc_house_edittext_0);
			mHouseInfo0.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete();
				}
			});
			mHouseInfo3 = (EditText)findViewById(R.id.et_asset_other_inc_house_edittext_1);
			mHouseInfo3.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(!s.toString().equals(mTextWatcherHouseResult)){
						if (s.toString().length() == 0) mTextWatcherHouseResult = "0";
						else mTextWatcherHouseResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
						mHouseInfo3.setText(mTextWatcherHouseResult);
						mHouseInfo3.setSelection(mTextWatcherHouseResult.length());
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete();
				}
			});

			/* Layout Car*/
			mCarLayout = (LinearLayout)findViewById(R.id.ll_asset_other_inc_car);
			mCarSpinner1 = (LinearLayout)findViewById(R.id.ll_asset_other_inc_car_spinner_layout_1);
			mCarSpinner2 = (LinearLayout)findViewById(R.id.ll_asset_other_inc_car_spinner_layout_2);
			mCarSpinner3 = (LinearLayout)findViewById(R.id.ll_asset_other_inc_car_spinner_layout_3);
			mCarSpinner4 = (LinearLayout)findViewById(R.id.ll_asset_other_inc_car_spinner_layout_4);
			mCarInfo1 = (TextView)findViewById(R.id.tv_asset_other_inc_car_spinner_text_1);
			mCarInfo2 = (TextView)findViewById(R.id.tv_asset_other_inc_car_spinner_text_2);
			mCarInfo3 = (TextView)findViewById(R.id.tv_asset_other_inc_car_spinner_text_3);
			mCarInfo4 = (TextView)findViewById(R.id.tv_asset_other_inc_car_spinner_text_4);
			mCarInfo5 = (EditText)findViewById(R.id.et_asset_other_inc_car_edittext_1);
//			mCarInfo5.addTextChangedListener(new TextWatcher() {
//				@Override
//				public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
//
//				@Override
//				public void onTextChanged(CharSequence s, int start, int before, int count) {
//					if(!s.toString().equals(mTextWatcherCarResult)){
//						if (s.toString().length() == 0) mTextWatcherCarResult = "0";
//						else mTextWatcherCarResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
//						mCarInfo5.setText(mTextWatcherCarResult);
//						mCarInfo5.setSelection(mTextWatcherCarResult.length());
//					}
//				}
//
//				@Override
//				public void afterTextChanged(Editable s) { }
//			});

			mHouseSpinner1.setOnClickListener(BtnClickListener);
			mHouseSpinner2.setOnClickListener(BtnClickListener);
			mCarSpinner1.setOnClickListener(BtnClickListener);
			mCarSpinner2.setOnClickListener(BtnClickListener);
			mCarSpinner3.setOnClickListener(BtnClickListener);
			mCarSpinner4.setOnClickListener(BtnClickListener);
			mDelete.setOnClickListener(BtnClickListener);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Title */
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) ((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);

			/* Default Value */
			mCurrentStateIndex = 0;	// 상세화면
			DivierLayout();

			if(mHouseOrCarIndex == 0){
				mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
				mAnimSpinnerText.setDuration(0);
				mAnimSpinnerText.setFillAfter(true);
			}
			else {
				mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right_09);
				mAnimSpinnerText.setDuration(0);
				mAnimSpinnerText.setFillAfter(true);
			}


			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_right);
			mAnimSpinnerEditText.setDuration(0);
			mAnimSpinnerEditText.setFillAfter(true);

//			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_up);
//			mAnimDelete.setDuration(0);
//			mAnimDelete.setFillAfter(true);

			if (mHouseOrCarIndex == 0){
				mHouseSpinner1Data = new ArrayList<String>();
				mHouseSpinner2Data = new ArrayList<String>();
				Collections.addAll(mHouseSpinner1Data, getResources().getStringArray(R.array.estate_type_name));
				Collections.addAll(mHouseSpinner2Data, getResources().getStringArray(R.array.deal_type_name));
//				for (int i = 0; i <mHouseSpinner1Data.size() ; i++) {
//					if (mHouseSpinner1Data.get(i).toString().equals(mInfoDataHouse.mAssetEstateType)) {
//						mHouseSpinner1Pos = i;
//						break;
//					}
//				}
//				for (int i = 0; i <mHouseSpinner2Data.size() ; i++) {
//					if (mHouseSpinner2Data.get(i).toString().equals(mInfoDataHouse.mAssetEstateDealType)) {
//						mHouseSpinner2Pos = i;
//						break;
//					}
//				}
				resetData();
//				mHouseInfo1.setText(TransFormatUtils.transEstateTypePosToName(mInfoDataHouse.mAssetEstateType));
//				mHouseInfo2.setText(TransFormatUtils.transDealTypePosToName(mInfoDataHouse.mAssetEstateDealType));
//				mHouseInfo0.setText(mInfoDataHouse.mAssetEstateName);
//				mHouseInfo0.setSelection(mHouseInfo0.getText().toString().length());
//				mHouseInfo3.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoDataHouse.mAssetEstatePrice));
//				mHouseInfo3.setSelection(mHouseInfo3.getText().toString().length());

//				mHouseSpinner1.startAnimation(mAnimSpinnerLayout);
//				mHouseSpinner2.startAnimation(mAnimSpinnerLayout);

				mHouseInfo1.startAnimation(mAnimSpinnerText);
				mHouseInfo2.startAnimation(mAnimSpinnerText);
				mHouseInfo0.startAnimation(mAnimSpinnerEditText);
				mHouseInfo3.startAnimation(mAnimSpinnerEditText);

			}
			else{
				mCarSpinner1Data = new ArrayList<String>();
				mCarSpinner2Data = new ArrayList<String>();
				mCarSpinner3Data = new ArrayList<String>();
				mCarSpinner4Data = new ArrayList<String>();
//				Collections.addAll(mCarSpinner1Data, getResources().getStringArray(R.array.spinner_asset_other_3));
//				Collections.addAll(mCarSpinner2Data, getResources().getStringArray(R.array.spinner_asset_other_4));
//				Collections.addAll(mCarSpinner3Data, getResources().getStringArray(R.array.spinner_asset_other_5));
//				Collections.addAll(mCarSpinner4Data, getResources().getStringArray(R.array.spinner_asset_other_4));
//				for (int i = 0; i <mCarSpinner1Data.size() ; i++) {
//					if (mCarSpinner1Data.get(i).toString().equals(mInfoDataCar.mAssetCarMakerCode)) {
//						mCarSpinner1Pos = i;
//						break;
//					}
//				}
//				for (int i = 0; i <mCarSpinner2Data.size() ; i++) {
//					if (mCarSpinner2Data.get(i).toString().equals(mInfoDataCar.mAssetCarCode)) {
//						mCarSpinner2Pos = i;
//						break;
//					}
//				}
//				for (int i = 0; i <mCarSpinner3Data.size() ; i++) {
//					if (mCarSpinner3Data.get(i).toString().equals(mInfoDataCar.mAssetCarYear)) {
//						mCarSpinner3Pos = i;
//						break;
//					}
//				}
//				for (int i = 0; i <mCarSpinner4Data.size() ; i++) {
//					if (mCarSpinner4Data.get(i).toString().equals(mInfoDataCar.mAssetCarSubCode)) {
//						mCarSpinner4Pos = i;
//						break;
//					}
//				}
//				mCarInfo1.setText(mCarSpinner1Data.get(mCarSpinner1Pos).toString());
//				mCarInfo2.setText(mCarSpinner2Data.get(mCarSpinner2Pos).toString());
//				mCarInfo3.setText(mCarSpinner3Data.get(mCarSpinner3Pos).toString());
//				mCarInfo4.setText(mCarSpinner4Data.get(mCarSpinner4Pos).toString());

				resetData();
//				mCarInfo1.setText(mInfoDataCar.mAssetCarMakerCode);
//				mCarInfo2.setText(mInfoDataCar.mAssetCarCode);
//				mCarInfo3.setText(mInfoDataCar.mAssetCarYear);
//				mCarInfo4.setText(mInfoDataCar.mAssetCarSubCode);
//				mCarInfo5.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoDataCar.mAssetCarMarketPrice));
////				mCarInfo5.setSelection(mCarInfo5.getText().toString().length());
//				mCarInfo5.setEnabled(false);
//				mCarInfo5.setBackgroundResource(0);

				mCarInfo1.startAnimation(mAnimSpinnerText);
				mCarInfo2.startAnimation(mAnimSpinnerText);
				mCarInfo3.startAnimation(mAnimSpinnerText);
				mCarInfo4.startAnimation(mAnimSpinnerText);
//				mCarInfo5.startAnimation(mAnimSpinnerEditText);
			}
			//mDelete.startAnimation(mAnimDelete);
			mDelete.setVisibility(View.GONE);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
//			if (v.getId() != R.id.btn_level_3_activity_asset_other_detail_delete)v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
			mHouseInfo0.clearFocus();
			mHouseInfo3.clearFocus();
			switch (v.getId()) {

				case R.id.btn_level_3_activity_asset_other_detail_delete:
					if (mCurrentStateIndex == 0) return;
					String mStr = "삭제하시겠어요?";
					DialogUtils.showDlgBaseTwoButton(Level3ActivityAssetOtherDetail.this, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
										DialogUtils.showLoading(Level3ActivityAssetOtherDetail.this);											// 자산 로딩
										if (mHouseOrCarIndex == 0){
											Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).deleteEstate(
													new DeleteAssetEstate(Session.getInstance(Level3ActivityAssetOtherDetail.this).getUserId(), mInfoDataHouse.mAssetEstateId)
											);
											mCall.enqueue(new Callback<Result>() {
												@Override
												public void onResponse(Call<Result> call, Response<Result> response) {
													DialogUtils.dismissLoading();											// 자산 로딩
													if (APP._SAMPLE_MODE_) NextStepDeleteHouse(AssetSampleTestDataJson.GetSampleData10());
													else NextStepDeleteHouse(response.body());
												}

												@Override
												public void onFailure(Call<Result> call, Throwable t) {
													DialogUtils.dismissLoading();											// 자산 로딩
													if (APP._SAMPLE_MODE_) {
														NextStepDeleteHouse(AssetSampleTestDataJson.GetSampleData10());
													}
													else
														ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
														//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("Level3ActivityAssetOtherDetail 서버 통신 실패 - 부동산 삭제");
												}
											});
										}
										else{
											Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).deleteCar(
													new DeleteAssetCar(Session.getInstance(Level3ActivityAssetOtherDetail.this).getUserId(), mInfoDataCar.mAssetCarId)
											);
											mCall.enqueue(new Callback<Result>() {
												@Override
												public void onResponse(Call<Result> call, Response<Result> response) {
													DialogUtils.dismissLoading();											// 자산 로딩
													if (APP._SAMPLE_MODE_) NextStepDeleteCar(AssetSampleTestDataJson.GetSampleData12());
													else NextStepDeleteCar(response.body());
												}

												@Override
												public void onFailure(Call<Result> call, Throwable t) {
													DialogUtils.dismissLoading();											// 자산 로딩
													if (APP._SAMPLE_MODE_) {
														NextStepDeleteCar(AssetSampleTestDataJson.GetSampleData12());
													}
													else
														ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
														//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("Level3ActivityAssetOtherDetail 서버 통신 실패 - 차량 삭제");
												}
											});
										}
									}
								}
							},
							new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
					break;
				case R.id.ll_asset_other_inc_house_spinner_layout_1:
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
					int mTempLeft1 = (ElseUtils.getRect(v).left);
					DialogUtils.showDlgSpinnerList(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
//							mInfoDataHouse.mAssetEstateType = String.valueOf(which+1);
							String mTemp = String.valueOf(which+1);
//							mHouseSpinner1Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
							mHouseInfo1.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
//							mHouseInfo1.setText(TransFormatUtils.transEstateTypePosToName(mInfoDataHouse.mAssetEstateType));
							mHouseInfo1.setText(TransFormatUtils.transEstateTypePosToName(mTemp));
							dialog.dismiss();
							send_interface_complete();
						}
//					}, mHouseSpinner1Data, mTempTop1, mTempLeft1, Integer.valueOf(mInfoDataHouse.mAssetEstateType)-1);
					}, mHouseSpinner1Data, mTempTop1, mTempLeft1, Integer.valueOf(TransFormatUtils.transEstateTypeNameToPos(mHouseInfo1.getText().toString()))-1, new Dialog.OnDismissListener(){

						@Override
						public void onDismiss(DialogInterface dialog) {
							v.setBackgroundResource(R.drawable.selector_spinner_bg);
						}
					});
					break;

				case R.id.ll_asset_other_inc_house_spinner_layout_2:
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					int mTempTop2 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
					int mTempLeft2 = (ElseUtils.getRect(v).left);
					DialogUtils.showDlgSpinnerList(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
//							mInfoDataHouse.mAssetEstateDealType = String.valueOf(which+1);
							String mTemp = String.valueOf(which+1);
//							mHouseSpinner2Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
							mHouseInfo2.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
//							mHouseInfo2.setText(TransFormatUtils.transDealTypePosToName(mInfoDataHouse.mAssetEstateDealType));
							mHouseInfo2.setText(TransFormatUtils.transDealTypePosToName(mTemp));
							dialog.dismiss();
							send_interface_complete();
						}
//					}, mHouseSpinner2Data, mTempTop2, mTempLeft2, Integer.valueOf(mInfoDataHouse.mAssetEstateDealType)-1);
					}, mHouseSpinner2Data, mTempTop2, mTempLeft2, Integer.valueOf(TransFormatUtils.transDealTypeNameToPos(mHouseInfo2.getText().toString()))-1, new Dialog.OnDismissListener(){

						@Override
						public void onDismiss(DialogInterface dialog) {
							v.setBackgroundResource(R.drawable.selector_spinner_bg);
						}
					});
					break;

				case R.id.ll_asset_other_inc_car_spinner_layout_1:
					if (checkStep(1)) return;
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
					mRequestDataByUserId.setUserId(Session.getInstance(Level3ActivityAssetOtherDetail.this).getUserId());
					ServiceGenerator.createService(AssetApi.class).getCarMakerCode(mRequestDataByUserId).enqueue(new Callback<ArrayList<ResponseAssetCarMakerCode>>() {
						@Override
						public void onResponse(Call<ArrayList<ResponseAssetCarMakerCode>> call, Response<ArrayList<ResponseAssetCarMakerCode>> response) {
							ArrayList<ResponseAssetCarMakerCode> mTemp;
							if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData21();
							else mTemp = response.body();
							mCarSpinner1Data.removeAll(mCarSpinner1Data);
							mCarSpinner1Pos = 0;
							for (int i = 0; i < mTemp.size(); i++) {
								mCarSpinner1Data.add(i, mTemp.get(i).mAssetCarMakerCode);
								if (mCarInfo1.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarMakerCode)) mCarSpinner1Pos = i;
							}
							int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
							int mTempLeft1 = (ElseUtils.getRect(v).left);
							DialogUtils.showDlgSpinnerList_220(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mCarSpinner1Pos = which;
//									v.setBackgroundResource(R.drawable.selector_spinner_bg);
//									v.setStatus(true);
									mCarInfo1.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
									mCarInfo1.setText(mCarSpinner1Data.get(mCarSpinner1Pos));
									valueReset(1);
									dialog.dismiss();
									send_interface_complete();
								}
							}, mCarSpinner1Data, mTempTop1, mTempLeft1, mCarSpinner1Pos, new Dialog.OnDismissListener() {

								@Override
								public void onDismiss(DialogInterface dialog) {
									v.setBackgroundResource(R.drawable.selector_spinner_bg);
								}
							});
						}

						@Override
						public void onFailure(Call<ArrayList<ResponseAssetCarMakerCode>> call, Throwable t) {
							if (APP._SAMPLE_MODE_) {
								ArrayList<ResponseAssetCarMakerCode> mTemp;
								mTemp = AssetSampleTestDataJson.GetSampleData21();
								mCarSpinner1Data.removeAll(mCarSpinner1Data);
								for (int i = 0; i < mTemp.size(); i++) {
									mCarSpinner1Data.add(i, mTemp.get(i).mAssetCarMakerCode);
								}
								int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
								int mTempLeft1 = (ElseUtils.getRect(v).left);
								DialogUtils.showDlgSpinnerList_220(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										mCarSpinner1Pos = which;
//										v.setBackgroundResource(R.drawable.selector_spinner_bg);
//										v.setStatus(true);
										mCarInfo1.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
										mCarInfo1.setText(mCarSpinner1Data.get(mCarSpinner1Pos));
										valueReset(1);
										dialog.dismiss();
										send_interface_complete();
									}
								}, mCarSpinner1Data, mTempTop1, mTempLeft1, mCarSpinner1Pos, new Dialog.OnDismissListener() {

									@Override
									public void onDismiss(DialogInterface dialog) {
										v.setBackgroundResource(R.drawable.selector_spinner_bg);
									}
								});
							} else
								ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
								//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("Level3ActivityAssetOtherDetail 서버 통신 실패 : 차량 제조사 조회");

						}
					});

					break;

				case R.id.ll_asset_other_inc_car_spinner_layout_2:
					if (checkStep(2)) return;
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					ServiceGenerator.createService(AssetApi.class).getCarCode(new RequestAssetCarCode(Session.getInstance(Level3ActivityAssetOtherDetail.this).getUserId(), mCarInfo1.getText().toString())).enqueue(new Callback<ArrayList<ResponseAssetCarCode>>() {
						@Override
						public void onResponse(Call<ArrayList<ResponseAssetCarCode>> call, Response<ArrayList<ResponseAssetCarCode>> response) {
							ArrayList<ResponseAssetCarCode> mTemp;
							if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData22();
							else mTemp = response.body();
							mCarSpinner2Data.removeAll(mCarSpinner2Data);
							mCarSpinner2Pos = 0;
							for (int i = 0; i < mTemp.size(); i++) {
								mCarSpinner2Data.add(i, mTemp.get(i).mAssetCarCarCode);
								if (mCarInfo2.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarCarCode)) mCarSpinner2Pos = i;
							}
							int mTempTop2 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
							int mTempLeft2 = (ElseUtils.getRect(v).left);
							DialogUtils.showDlgSpinnerList_220(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mCarSpinner2Pos = which;
//									v.setBackgroundResource(R.drawable.selector_spinner_bg);
//									v.setStatus(true);
									mCarInfo2.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
									mCarInfo2.setText(mCarSpinner2Data.get(mCarSpinner2Pos));
									valueReset(2);
									dialog.dismiss();
									send_interface_complete();
								}
							}, mCarSpinner2Data, mTempTop2, mTempLeft2, mCarSpinner2Pos, new Dialog.OnDismissListener() {

								@Override
								public void onDismiss(DialogInterface dialog) {
									v.setBackgroundResource(R.drawable.selector_spinner_bg);
								}
							});

						}

						@Override
						public void onFailure(Call<ArrayList<ResponseAssetCarCode>> call, Throwable t) {
							if (APP._SAMPLE_MODE_) {
								ArrayList<ResponseAssetCarCode> mTemp;
								mTemp = AssetSampleTestDataJson.GetSampleData22();
								mCarSpinner2Data.removeAll(mCarSpinner2Data);
								mCarSpinner2Pos = 0;
								for (int i = 0; i < mTemp.size(); i++) {
									mCarSpinner2Data.add(i, mTemp.get(i).mAssetCarCarCode);
									if (mCarInfo2.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarCarCode)) mCarSpinner2Pos = i;
								}
								int mTempTop2 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
								int mTempLeft2 = (ElseUtils.getRect(v).left);
								DialogUtils.showDlgSpinnerList_220(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										mCarSpinner2Pos = which;
//										v.setBackgroundResource(R.drawable.selector_spinner_bg);
//										v.setStatus(true);
										mCarInfo2.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
										mCarInfo2.setText(mCarSpinner2Data.get(mCarSpinner2Pos));
										valueReset(2);
										dialog.dismiss();
									}
								}, mCarSpinner2Data, mTempTop2, mTempLeft2, mCarSpinner2Pos, new Dialog.OnDismissListener() {

									@Override
									public void onDismiss(DialogInterface dialog) {
										v.setBackgroundResource(R.drawable.selector_spinner_bg);
									}
								});
							} else
								ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
								//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("Level3ActivityAssetOtherDetail 서버 통신 실패 : 차량 대표차명 조회");
						}
					});
//					int mTempTop4 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
//					int mTempLeft4 = (ElseUtils.getRect(v).left);
//					DialogUtils.showDlgSpinnerList(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener(){
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							mCarSpinner2Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
//							mCarInfo2.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
//							mCarInfo2.setText(mCarSpinner2Data.get(mCarSpinner2Pos));
//							dialog.dismiss();
//						}
//					}, mCarSpinner2Data, mTempTop4, mTempLeft4, mCarSpinner2Pos);
					break;

				case R.id.ll_asset_other_inc_car_spinner_layout_3:
					if (checkStep(4)) return;
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					ServiceGenerator.createService(AssetApi.class).getCarYear(new RequestAssetCarYear(Session.getInstance(Level3ActivityAssetOtherDetail.this).getUserId(), mCarInfo1.getText().toString(), mCarInfo2.getText().toString(), mCarInfo4.getText().toString())).enqueue(new Callback<ArrayList<ResponseAssetCarYear>>() {
						@Override
						public void onResponse(Call<ArrayList<ResponseAssetCarYear>> call, Response<ArrayList<ResponseAssetCarYear>> response) {
							ArrayList<ResponseAssetCarYear> mTemp;
							if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData24();
							else mTemp = response.body();
							mCarSpinner3Data.removeAll(mCarSpinner3Data);
							mCarSpinner3Pos = 0;
							for (int i = 0; i < mTemp.size(); i++) {
								mCarSpinner3Data.add(i, mTemp.get(i).mAssetCarMadeYear);
								if (mCarInfo3.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarMadeYear)) mCarSpinner3Pos = i;
							}
							int mTempTop3 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
							int mTempLeft3 = (ElseUtils.getRect(v).left);
							DialogUtils.showDlgSpinnerList_220(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mCarSpinner3Pos = which;
//									v.setBackgroundResource(R.drawable.selector_spinner_bg);
//									v.setStatus(true);
									mCarInfo3.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
									mCarInfo3.setText(mCarSpinner3Data.get(mCarSpinner3Pos));
									valueReset(4);
									dialog.dismiss();
									getCarMarketPrice();
									send_interface_complete();
								}
							}, mCarSpinner3Data, mTempTop3, mTempLeft3, mCarSpinner3Pos, new Dialog.OnDismissListener() {

								@Override
								public void onDismiss(DialogInterface dialog) {
									v.setBackgroundResource(R.drawable.selector_spinner_bg);
								}
							});

						}

						@Override
						public void onFailure(Call<ArrayList<ResponseAssetCarYear>> call, Throwable t) {
							if (APP._SAMPLE_MODE_) {
								ArrayList<ResponseAssetCarYear> mTemp;
								mTemp = AssetSampleTestDataJson.GetSampleData24();
								mCarSpinner3Data.removeAll(mCarSpinner3Data);
								mCarSpinner3Pos = 0;
								for (int i = 0; i < mTemp.size(); i++) {
									mCarSpinner3Data.add(i, mTemp.get(i).mAssetCarMadeYear);
									if (mCarInfo3.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarMadeYear)) mCarSpinner3Pos = i;
								}
								int mTempTop3 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
								int mTempLeft3 = (ElseUtils.getRect(v).left);
								DialogUtils.showDlgSpinnerList_220(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										mCarSpinner3Pos = which;
//										v.setBackgroundResource(R.drawable.selector_spinner_bg);
//										v.setStatus(true);
										mCarInfo3.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
										mCarInfo3.setText(mCarSpinner3Data.get(mCarSpinner3Pos));
										valueReset(4);
										dialog.dismiss();
										getCarMarketPrice();
									}
								}, mCarSpinner3Data, mTempTop3, mTempLeft3, mCarSpinner3Pos, new Dialog.OnDismissListener() {

									@Override
									public void onDismiss(DialogInterface dialog) {
										v.setBackgroundResource(R.drawable.selector_spinner_bg);
									}
								});
							} else
								ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
								//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("Level3ActivityAssetOtherDetail 서버 통신 실패 : 차량 연식 조회");

						}
					});
//					int mTempTop5 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
//					int mTempLeft5 = (ElseUtils.getRect(v).left);
//					DialogUtils.showDlgSpinnerList(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener(){
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							mCarSpinner3Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
//							mCarInfo3.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
//							mCarInfo3.setText(mCarSpinner3Data.get(mCarSpinner3Pos));
//							dialog.dismiss();
//						}
//					}, mCarSpinner3Data, mTempTop5, mTempLeft5, mCarSpinner3Pos);
					break;

				case R.id.ll_asset_other_inc_car_spinner_layout_4:
					if (checkStep(3)) return;
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					ServiceGenerator.createService(AssetApi.class).getCarSubCode(new RequestAssetCarSubCode(Session.getInstance(Level3ActivityAssetOtherDetail.this).getUserId(), mCarInfo1.getText().toString(), mCarInfo2.getText().toString())).enqueue(new Callback<ArrayList<ResponseAssetCarSubCode>>() {
						@Override
						public void onResponse(Call<ArrayList<ResponseAssetCarSubCode>> call, Response<ArrayList<ResponseAssetCarSubCode>> response) {
							ArrayList<ResponseAssetCarSubCode> mTemp;
							if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData23();
							else mTemp = response.body();
							mCarSpinner4Data.removeAll(mCarSpinner4Data);
							mCarSpinner4Pos = 0;
							for (int i = 0; i < mTemp.size(); i++) {
								mCarSpinner4Data.add(i, mTemp.get(i).mAssetCarSubCode);
								if (mCarInfo4.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarSubCode)) mCarSpinner4Pos = i;
							}
							int mTempTop4 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
							int mTempLeft4 = (ElseUtils.getRect(v).left);
							DialogUtils.showDlgSpinnerList_220(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mCarSpinner4Pos = which;
//									v.setBackgroundResource(R.drawable.selector_spinner_bg);
//									v.setStatus(true);
									mCarInfo4.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
									mCarInfo4.setText(mCarSpinner4Data.get(mCarSpinner4Pos));
									valueReset(3);
									dialog.dismiss();
									send_interface_complete();
								}
							}, mCarSpinner4Data, mTempTop4, mTempLeft4, mCarSpinner4Pos, new Dialog.OnDismissListener() {

								@Override
								public void onDismiss(DialogInterface dialog) {
									v.setBackgroundResource(R.drawable.selector_spinner_bg);
								}
							});
						}

						@Override
						public void onFailure(Call<ArrayList<ResponseAssetCarSubCode>> call, Throwable t) {
							if (APP._SAMPLE_MODE_) {
								ArrayList<ResponseAssetCarSubCode> mTemp;
								mTemp = AssetSampleTestDataJson.GetSampleData23();
								mCarSpinner4Data.removeAll(mCarSpinner4Data);
								mCarSpinner4Pos = 0;
								for (int i = 0; i < mTemp.size(); i++) {
									mCarSpinner4Data.add(i, mTemp.get(i).mAssetCarSubCode);
									if (mCarInfo4.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarSubCode)) mCarSpinner4Pos = i;
								}
								int mTempTop4 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
								int mTempLeft4 = (ElseUtils.getRect(v).left);
								DialogUtils.showDlgSpinnerList_220(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										mCarSpinner4Pos = which;
//										v.setBackgroundResource(R.drawable.selector_spinner_bg);
//										v.setStatus(true);
										mCarInfo4.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
										mCarInfo4.setText(mCarSpinner4Data.get(mCarSpinner4Pos));
										valueReset(3);
										dialog.dismiss();
									}
								}, mCarSpinner4Data, mTempTop4, mTempLeft4, mCarSpinner4Pos, new Dialog.OnDismissListener() {

									@Override
									public void onDismiss(DialogInterface dialog) {
										v.setBackgroundResource(R.drawable.selector_spinner_bg);
									}
								});
							} else
								ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
								//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("Level3ActivityAssetOtherDetail 서버 통신 실패 : 차량 세부차명 조회");
						}
					});
//					int mTempTop6 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level3ActivityAssetOtherDetail.this).top);
//					int mTempLeft6 = (ElseUtils.getRect(v).left);
//					DialogUtils.showDlgSpinnerList(Level3ActivityAssetOtherDetail.this, new Dialog.OnClickListener(){
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							mCarSpinner4Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
//							mCarInfo4.setTextAppearance(Level3ActivityAssetOtherDetail.this, R.style.CS_Text_15_898989);
//							mCarInfo4.setText(mCarSpinner4Data.get(mCarSpinner4Pos));
//							dialog.dismiss();
//						}
//					}, mCarSpinner4Data, mTempTop6, mTempLeft6, mCarSpinner4Pos);
					break;

				default:
					break;
			}

		}
	};






//===============================================================================//
// Protocol (MarketPrice : 차량 시세)
//===============================================================================//
	private void getCarMarketPrice(){
		DialogUtils.showLoading(Level3ActivityAssetOtherDetail.this);											// 자산 로딩
		ServiceGenerator.createService(AssetApi.class).getCarMarketPrice(new RequestAssetCarMarketPrice(Session.getInstance(Level3ActivityAssetOtherDetail.this).getUserId(), mCarInfo1.getText().toString(), mCarInfo2.getText().toString(), mCarInfo4.getText().toString(), mCarInfo3.getText().toString())).enqueue(new Callback<ResponseAssetCarMarketPrice>() {
			@Override
			public void onResponse(Call<ResponseAssetCarMarketPrice> call, Response<ResponseAssetCarMarketPrice> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ResponseAssetCarMarketPrice mTemp;
				if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData25();
				else mTemp = response.body();
				mCarInfo5.setText(TransFormatUtils.getDecimalFormatRecvString(mTemp.mAssetCarMarketPrice));
				send_interface_complete();
			}

			@Override
			public void onFailure(Call<ResponseAssetCarMarketPrice> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (APP._SAMPLE_MODE_) {
					ResponseAssetCarMarketPrice mTemp;
					mTemp = AssetSampleTestDataJson.GetSampleData25();
					mCarInfo5.setText(TransFormatUtils.getDecimalFormatRecvString(mTemp.mAssetCarMarketPrice));
				} else
					ElseUtils.network_error(Level3ActivityAssetOtherDetail.this);
					//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("Level3ActivityAssetOtherDetail 서버 통신 실패 : 차량 세부차명 조회");

			}
		});
	}





	//===============================================================================//
// Reset Init
//===============================================================================//
	/* Reset */
	private void valueReset(int _Step){
		switch (_Step){
			case 1:
//				mCarInfo2.setText("선택");
//				mCarInfo4.setText("선택");
//				mCarInfo3.setText("선택");
				mCarInfo2.setText("");
				mCarInfo4.setText("");
				mCarInfo3.setText("");
				mCarInfo5.setText("");
				break;

			case 2:
//				mCarInfo4.setText("선택");
//				mCarInfo3.setText("선택");
				mCarInfo4.setText("");
				mCarInfo3.setText("");
				mCarInfo5.setText("");
				break;

			case 3:
//				mCarInfo3.setText("선택");
				mCarInfo3.setText("");
				mCarInfo5.setText("");
				break;

			case 4:
				break;

			default:
				break;
		}
	}


	//===============================================================================//
// Step Check
//===============================================================================//
	/*Step Check*/
	private Boolean checkStep(int _Step) {
		boolean mResult = false;
		switch (_Step){
			case 1:
				break;

			case 2:
//				if (mCarInfo1.getText().toString().equalsIgnoreCase("선택")) {
				if (mCarInfo1.getText().toString().length() == 0) {
					//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("제조사를 선택해 주세요");
					Toast.makeText(Level3ActivityAssetOtherDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					mResult = true;
				}
				break;

			case 3:
//				if (mCarInfo1.getText().toString().equalsIgnoreCase("선택")) {
				if (mCarInfo1.getText().toString().length() == 0) {
					//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("제조사를 선택해 주세요");
					Toast.makeText(Level3ActivityAssetOtherDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					mResult = true;
				}
//				else if (mCarInfo2.getText().toString().equalsIgnoreCase("선택")) {
				else if (mCarInfo2.getText().toString().length() == 0) {
					//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("대표차명을 선택해 주세요");
					Toast.makeText(Level3ActivityAssetOtherDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					mResult = true;
				}
				break;

			case 4:
//				if (mCarInfo1.getText().toString().equalsIgnoreCase("선택")) {
				if (mCarInfo1.getText().toString().length() == 0) {
					Toast.makeText(Level3ActivityAssetOtherDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("제조사를 선택해 주세요");
					mResult = true;
				}
//				else if (mCarInfo2.getText().toString().equalsIgnoreCase("선택")) {
				else if (mCarInfo2.getText().toString().length() == 0) {
					Toast.makeText(Level3ActivityAssetOtherDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("대표차명을 선택해 주세요");
					mResult = true;
				}
//				else if (mCarInfo4.getText().toString().equalsIgnoreCase("선택")) {
				else if (mCarInfo4.getText().toString().length() == 0) {
					Toast.makeText(Level3ActivityAssetOtherDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("세부차명을 선택해 주세요");
					mResult = true;
				}
				break;

			default:
				break;
		}


		return mResult;
	}











	private void DivierLayout(){

		if (mHouseOrCarIndex == 0){
			mHouseLayout.setVisibility(View.VISIBLE);
			mCarLayout.setVisibility(View.GONE);
			if (mCurrentStateIndex == 0){
				((TextView)findViewById(R.id.tv_navi_motion_title)).setText("부동산 상세");
				((TextView)findViewById(R.id.tv_navi_motion_else)).setText("편집");
				((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
				((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
				mHouseSpinner1.setEnabled(false);
				mHouseSpinner2.setEnabled(false);
				mHouseInfo0.setEnabled(false);
				mHouseInfo3.setEnabled(false);
				mHouseSpinner1.setBackgroundResource(R.drawable.selector_spinner_bg_open);
				mHouseSpinner2.setBackgroundResource(R.drawable.selector_spinner_bg_open);
				mHouseInfo0.setBackgroundResource(0);
				mHouseInfo3.setBackgroundResource(0);
			}
			else{
				((TextView)findViewById(R.id.tv_navi_motion_title)).setText("부동산 편집");
				((TextView)findViewById(R.id.tv_navi_motion_else)).setText("저장");
				((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
				((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
				mHouseSpinner1.setEnabled(true);
				mHouseSpinner2.setEnabled(true);
				mHouseInfo0.setEnabled(true);
				mHouseInfo3.setEnabled(true);
				mHouseSpinner1.setBackgroundResource(R.drawable.selector_spinner_bg);
				mHouseSpinner2.setBackgroundResource(R.drawable.selector_spinner_bg);
				mHouseInfo0.setBackgroundResource(R.drawable.selector_edittext_mode_bg);
				mHouseInfo3.setBackgroundResource(R.drawable.selector_edittext_mode_bg);
			}
		}
		else{
			mHouseLayout.setVisibility(View.GONE);
			mCarLayout.setVisibility(View.VISIBLE);
			if (mCurrentStateIndex == 0){
				((TextView)findViewById(R.id.tv_navi_motion_title)).setText("차량 상세");
				((TextView)findViewById(R.id.tv_navi_motion_else)).setText("편집");
				((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
				((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
				mCarSpinner1.setEnabled(false);
				mCarSpinner2.setEnabled(false);
				mCarSpinner3.setEnabled(false);
				mCarSpinner4.setEnabled(false);
//				mCarInfo5.setEnabled(false);
				mCarSpinner1.setBackgroundResource(R.drawable.selector_spinner_bg_open);
				mCarSpinner2.setBackgroundResource(R.drawable.selector_spinner_bg_open);
				mCarSpinner3.setBackgroundResource(R.drawable.selector_spinner_bg_open);
				mCarSpinner4.setBackgroundResource(R.drawable.selector_spinner_bg_open);
				mCarInfo5.setBackgroundResource(0);
			}
			else{
				((TextView)findViewById(R.id.tv_navi_motion_title)).setText("차량 편집");
				((TextView)findViewById(R.id.tv_navi_motion_else)).setText("저장");
				((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
				((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
				mCarSpinner1.setEnabled(true);
				mCarSpinner2.setEnabled(true);
				mCarSpinner3.setEnabled(true);
				mCarSpinner4.setEnabled(true);
//				mCarInfo5.setEnabled(true);
				mCarSpinner1.setBackgroundResource(R.drawable.selector_spinner_bg);
				mCarSpinner2.setBackgroundResource(R.drawable.selector_spinner_bg);
				mCarSpinner3.setBackgroundResource(R.drawable.selector_spinner_bg);
				mCarSpinner4.setBackgroundResource(R.drawable.selector_spinner_bg);
				mCarInfo5.setBackgroundResource(R.drawable.selector_edittext_bg);
			}
		}

	}



	private void resetData() {
		switch (mHouseOrCarIndex){
			case 0:
				mHouseInfo1.setText(TransFormatUtils.transEstateTypePosToName(mInfoDataHouse.mAssetEstateType));
				mHouseInfo2.setText(TransFormatUtils.transDealTypePosToName(mInfoDataHouse.mAssetEstateDealType));
				mHouseInfo0.setText(mInfoDataHouse.mAssetEstateName);
				mHouseInfo0.setSelection(mHouseInfo0.getText().toString().length());
				mHouseInfo3.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoDataHouse.mAssetEstatePrice));
				mHouseInfo3.setSelection(mHouseInfo3.getText().toString().length());
				break;

			case 1:
				mCarInfo1.setText(mInfoDataCar.mAssetCarMakerCode);
				mCarInfo2.setText(mInfoDataCar.mAssetCarCode);
				mCarInfo3.setText(mInfoDataCar.mAssetCarYear);
				mCarInfo4.setText(mInfoDataCar.mAssetCarSubCode);
				mCarInfo5.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoDataCar.mAssetCarMarketPrice));
//				mCarInfo5.setSelection(mCarInfo5.getText().toString().length());
				mCarInfo5.setEnabled(false);
//				mCarInfo5.setBackgroundResource(0);
				break;

			default:
				break;
		}

	}




//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep Edit House **/
	private void NextStepEditHouse(Result _Result){
		if (_Result.getCode().equals("100")){
			//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("부동산 수정 완료");
			mCurrentStateIndex = 0; afterMotion("back");
		}
		else{
			//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("부동산 수정 실패");
		}
	}

	/**  NextStep Delete House **/
	private void NextStepDeleteHouse(Result _Result){
		if (_Result.getCode().equals("100")){
			//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("부동산 삭제 완료");
			goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_SUCCESS);
//			finish();
		}
		else{
			//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("부동산 삭제 실패");
		}
	}


	/**  NextStep Edit Car **/
	private void NextStepEditCar(Result _Result){
		if (_Result.getCode().equals("100")){
			//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("차량 수정 완료");
			mCurrentStateIndex = 0; afterMotion("back");
		}
		else{
			//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("차량 수정 실패");
		}
	}

	/**  NextStep Delete Car **/
	private void NextStepDeleteCar(Result _Result){
		if (_Result.getCode().equals("100")){
			//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("차량 삭제 완료");
			goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_SUCCESS);
//			finish();
		}
		else{
			//CustomToast.getInstance(Level3ActivityAssetOtherDetail.this).showShort("차량 삭제 실패");
		}
	}


//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int _resultCod){
		Intent intent = getIntent();
		setResult(_resultCod, intent);
		finish();
	}

	Animation.AnimationListener myAnimationListener = new Animation.AnimationListener() {
		public void onAnimationEnd(Animation animation) {
			mDelete.setVisibility(View.GONE); //애니메 끝나면 사라저버려!
			mDelete.clearAnimation();
		}
		@Override
		public void onAnimationRepeat(Animation animation) {
		}
		@Override
		public void onAnimationStart(Animation animation) {
		}
	};


	private void send_interface_complete(){
		if(empty_check()){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}

	private boolean empty_check(){
		if (mHouseOrCarIndex == 0) {
			if(mHouseInfo0.length() == 0 || mHouseInfo1.getText().toString().equals("선택")
					|| mHouseInfo2.getText().toString().equals("선택") || mHouseInfo3.length() == 0 || mHouseInfo3.getText().toString().equals("0")){
				return false;
			}
		} else {
			if(mCarInfo1.length() == 0 || mCarInfo2.length() == 0
					|| mCarInfo3.length() == 0 || mCarInfo4.length() == 0 || mCarInfo5.length() == 0){
				return false;
			}
		}
		return true;
	}

}
