package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.SpendCardOption;
import com.nomadconnection.broccoli.data.Spend.SpendCardRecommendInternalOption;

import static com.nomadconnection.broccoli.constant.SpendCardOption.AnnualFeeRange.FEE_BELOW_100000;
import static com.nomadconnection.broccoli.constant.SpendCardOption.AnnualFeeRange.FEE_BELOW_30000;
import static com.nomadconnection.broccoli.constant.SpendCardOption.AnnualFeeRange.FEE_MORE_100000;
import static com.nomadconnection.broccoli.constant.SpendCardOption.AnnualFeeRange.FEE_NONE;
import static com.nomadconnection.broccoli.constant.SpendCardOption.AnnualFeeRange.FEE_TOTAL;

/**
 * Created by YelloHyunminJang on 2017. 3. 23..
 */

public class Level3ActivitySpendCardRecommendOption extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    private CheckBox mCreditCardBox;
    private CheckBox mCheckCardBox;
    private CheckBox mDiscountBox;
    private CheckBox mPointBox;
    private RadioGroup mOptionFee;

    private SpendCardRecommendInternalOption mOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_spend_card_recommend_option);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SpendCardRecmdFilter);
        Intent intent = getIntent();
        mOption = (SpendCardRecommendInternalOption) intent.getSerializableExtra(Const.DATA);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_card_recommend_option);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
        findViewById(R.id.btn_confirm).setOnClickListener(onConfirmListener);
        mCreditCardBox = (CheckBox) findViewById(R.id.cb_spend_card_recommend_option_credit);
        mCheckCardBox = (CheckBox) findViewById(R.id.cb_spend_card_recommend_option_check);
        mDiscountBox = (CheckBox) findViewById(R.id.cb_spend_card_recommend_option_discount);
        mPointBox = (CheckBox) findViewById(R.id.cb_spend_card_recommend_option_point);
        mCreditCardBox.setOnCheckedChangeListener(this);
        mCheckCardBox.setOnCheckedChangeListener(this);
        mDiscountBox.setOnCheckedChangeListener(this);
        mPointBox.setOnCheckedChangeListener(this);
        mOptionFee = (RadioGroup) findViewById(R.id.rg_spend_card_recommend_option_annual_fee);
        mOptionFee.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_spend_card_recommend_option_fee_none:
                        mOption.annulFee = FEE_NONE;
                        break;
                    case R.id.rb_spend_card_recommend_option_fee_below_3:
                        mOption.annulFee = FEE_BELOW_30000;
                        break;
                    case R.id.rb_spend_card_recommend_option_fee_below_10:
                        mOption.annulFee = FEE_BELOW_100000;
                        break;
                    case R.id.rb_spend_card_recommend_option_fee_more_10:
                        mOption.annulFee = FEE_MORE_100000;
                        break;
                    case R.id.rb_spend_card_recommend_option_fee_total:
                        mOption.annulFee = FEE_TOTAL;
                        break;
                }
            }
        });
    }

    private void initData() {
        switch (mOption.cardType) {
            case ALL:
                mCreditCardBox.setChecked(true);
                mCheckCardBox.setChecked(true);
                break;
            case CREDIT_CARD:
                mCreditCardBox.setChecked(true);
                mCheckCardBox.setChecked(false);
                break;
            case CHECK_CARD:
                mCreditCardBox.setChecked(false);
                mCheckCardBox.setChecked(true);
                break;
        }
        switch (mOption.optionDetailType) {
            case DISCOUNT_POINT:
                mDiscountBox.setChecked(true);
                mPointBox.setChecked(true);
                break;
            case DISCOUNT:
                mDiscountBox.setChecked(true);
                mPointBox.setChecked(false);
                break;
            case POINT:
                mDiscountBox.setChecked(false);
                mPointBox.setChecked(true);
                break;
            case MILEAGE:
                break;
        }
        switch (mOption.annulFee) {
            case FEE_NONE:
                mOptionFee.check(R.id.rb_spend_card_recommend_option_fee_none);
                break;
            case FEE_BELOW_30000:
                mOptionFee.check(R.id.rb_spend_card_recommend_option_fee_below_3);
                break;
            case FEE_BELOW_100000:
                mOptionFee.check(R.id.rb_spend_card_recommend_option_fee_below_10);
                break;
            case FEE_MORE_100000:
                mOptionFee.check(R.id.rb_spend_card_recommend_option_fee_more_10);
                break;
            case FEE_TOTAL:
                mOptionFee.check(R.id.rb_spend_card_recommend_option_fee_total);
                break;
        }
    }

    private void complete() {
        Intent intent = new Intent();
        intent.putExtra(Const.DATA, mOption);
        setResult(RESULT_OK, intent);
        finish();
    }

    private View.OnClickListener onConfirmListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            complete();
        }
    };

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            switch (buttonView.getId()) {
                case R.id.cb_spend_card_recommend_option_credit:
                    switch (mOption.cardType) {
                        case ALL:
                            break;
                        case CREDIT_CARD:
                            break;
                        case CHECK_CARD:
                            mOption.cardType = SpendCardOption.CardType.ALL;
                            break;
                    }
                    break;
                case R.id.cb_spend_card_recommend_option_check:
                    switch (mOption.cardType) {
                        case ALL:
                            break;
                        case CREDIT_CARD:
                            mOption.cardType = SpendCardOption.CardType.ALL;
                            break;
                        case CHECK_CARD:
                            break;
                    }
                    break;
                case R.id.cb_spend_card_recommend_option_discount:
                    switch (mOption.optionDetailType) {
                        case DISCOUNT_POINT:
                            break;
                        case DISCOUNT:
                            break;
                        case POINT:
                            mOption.optionDetailType = SpendCardOption.BenefitType.DISCOUNT_POINT;
                            break;
                        case MILEAGE:
                            break;
                    }
                    break;
                case R.id.cb_spend_card_recommend_option_point:
                    switch (mOption.optionDetailType) {
                        case DISCOUNT_POINT:
                            break;
                        case DISCOUNT:
                            mOption.optionDetailType = SpendCardOption.BenefitType.DISCOUNT_POINT;
                            break;
                        case POINT:
                            break;
                        case MILEAGE:
                            break;
                    }
                    break;
            }
        } else {
            switch (buttonView.getId()) {
                case R.id.cb_spend_card_recommend_option_credit:
                    switch (mOption.cardType) {
                        case ALL:
                            mOption.cardType = SpendCardOption.CardType.CHECK_CARD;
                            break;
                        case CREDIT_CARD:
                            mOption.cardType = SpendCardOption.CardType.CREDIT_CARD;
                            buttonView.setChecked(true);
                            break;
                        case CHECK_CARD:
                            break;
                    }
                    break;
                case R.id.cb_spend_card_recommend_option_check:
                    switch (mOption.cardType) {
                        case ALL:
                            mOption.cardType = SpendCardOption.CardType.CREDIT_CARD;
                            break;
                        case CREDIT_CARD:
                            break;
                        case CHECK_CARD:
                            mOption.cardType = SpendCardOption.CardType.CHECK_CARD;
                            buttonView.setChecked(true);
                            break;
                    }
                    break;
                case R.id.cb_spend_card_recommend_option_discount:
                    switch (mOption.optionDetailType) {
                        case DISCOUNT_POINT:
                            mOption.optionDetailType = SpendCardOption.BenefitType.POINT;
                            break;
                        case DISCOUNT:
                            mOption.optionDetailType = SpendCardOption.BenefitType.DISCOUNT;
                            buttonView.setChecked(true);
                            break;
                        case POINT:
                            break;
                        case MILEAGE:
                            break;
                    }
                    break;
                case R.id.cb_spend_card_recommend_option_point:
                    switch (mOption.optionDetailType) {
                        case DISCOUNT_POINT:
                            mOption.optionDetailType = SpendCardOption.BenefitType.DISCOUNT;
                            break;
                        case DISCOUNT:
                            break;
                        case POINT:
                            mOption.optionDetailType = SpendCardOption.BenefitType.POINT;
                            buttonView.setChecked(true);
                            break;
                        case MILEAGE:
                            break;
                    }
                    break;
            }
        }
    }
}
