package com.nomadconnection.broccoli.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.fragment.FragmentIntro;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.service.BatchService;
import com.nomadconnection.broccoli.service.ScrapingModule;
import com.nomadconnection.broccoli.service.data.ScrapRequester;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.pixplicity.multiviewpager.MultiViewPager;

import java.util.ArrayList;

public class Intro extends BaseActivity {
	
	private static MultiViewPager	mViewPager = null;

	private LinearLayout mLoadingLayout;
	private ImageView mLoading;
	private View mIntroIndicatorLayout;
	private ImageView mIndicator1, mIndicator2, mIndicator3, mIndicator4;
	
	private static int 			mTotalPageNum 	= 0;
	private static int 			mCurrentPageIdx	= 0;

	final Messenger mMessenger = new Messenger(new IncomingHandler());
	private ProcessMode mMode = ProcessMode.COLLECT_IDLE;
	private boolean isConnection = false;
	private boolean isScraping = false;
	private Messenger			mService;
	private ServiceConnection 	mServiceConnection;

    
    private ArrayList<Fragment> mALFragments;
    
//=======================================================================================//
// override method	
//=======================================================================================//	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.intro);
		//sendTracker(AnalyticsName.Intro);
		initBinderService();
		
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}

	}
	
	@Override
	protected void onDestroy() {		
		super.onDestroy();
		finishBinderService();
		hideLoadingProgress();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			return  false;
		}
		return super.onKeyDown(keyCode, event);
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			Intent intent = new Intent(Intro.this.getIntent());
//			mBackCheck = intent.getStringExtra(Const.DATA);
//			if(mBackCheck == null){
//				bResult=false;
//			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			mLoadingLayout = (LinearLayout) findViewById(R.id.intro_loading_layout);
			mLoading = (ImageView)findViewById(R.id.intro_loading_small);
			TextView mLoadingText = (TextView)findViewById(R.id.intro_loading_text);
			mLoadingText.setTypeface(Typeface.SANS_SERIF);

			mIntroIndicatorLayout = (View) findViewById(R.id.intro_indicator_layout);
			mIndicator1 = (ImageView) findViewById(R.id.intro_num1_imgview);
			mIndicator2 = (ImageView) findViewById(R.id.intro_num2_imgview);
			mIndicator3 = (ImageView) findViewById(R.id.intro_num3_imgview);
			mIndicator4 = (ImageView) findViewById(R.id.intro_num4_imgview);

			mALFragments = new ArrayList<Fragment>();
			mALFragments.add(new FragmentIntro(0));
			mALFragments.add(new FragmentIntro(1));
			mALFragments.add(new FragmentIntro(2));
			mALFragments.add(new FragmentIntro(3));
//			mALFragments.add(new FragmentIntro(4));

			mTotalPageNum = mALFragments.size();
    		
			mViewPager = (MultiViewPager) findViewById(R.id.pager_body_intro);
			mViewPager.setAdapter(new AdtFragmentPager(getSupportFragmentManager(), mALFragments));
			mViewPager.setOffscreenPageLimit(2);
			mViewPager.setOnPageChangeListener(mPageChangeListener);
			mViewPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.common_dimens_32));
			showLengthIndicator(0);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void showLengthIndicator(int count) {
		switch (count) {
			case 0:
				mIndicator1.setImageResource(R.drawable.shape_intro_indicator_on);
				mIndicator2.setImageResource(R.drawable.shape_intro_indicator_off);
				break;
			case 1:
				mIndicator1.setImageResource(R.drawable.shape_intro_indicator_off);
				mIndicator2.setImageResource(R.drawable.shape_intro_indicator_on);
				mIndicator3.setImageResource(R.drawable.shape_intro_indicator_off);
				break;
			case 2:
				mIndicator2.setImageResource(R.drawable.shape_intro_indicator_off);
				mIndicator3.setImageResource(R.drawable.shape_intro_indicator_on);
				mIndicator4.setImageResource(R.drawable.shape_intro_indicator_off);
				break;
			case 3:
				mIndicator3.setImageResource(R.drawable.shape_intro_indicator_off);
				mIndicator4.setImageResource(R.drawable.shape_intro_indicator_on);
				break;
		}
	}


//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}
	
	
	
//===============================================================================//
// View Pager
//===============================================================================//

	/**  View Page Change Listener **/
	private OnPageChangeListener mPageChangeListener = new OnPageChangeListener() {

		@Override
		public void onPageSelected(int position) {
			showLengthIndicator(position);
			if (position == mTotalPageNum-1) {
//				Intent intent = new Intent();
//				setResult(RESULT_OK, intent);
//				finish();
			} else {
//				mButtonLayout.setVisibility(View.INVISIBLE);
			}
			mCurrentPageIdx = position;
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
//			mCurrentPageIdx = 0;
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	};


//=======================================================================================//
// Adapter	
//=======================================================================================//
	public class AdtFragmentPager extends FragmentPagerAdapter{
		
		private ArrayList<Fragment> mFragments;

		public AdtFragmentPager(FragmentManager fm, ArrayList<Fragment> _ALFragments) {
			super(fm);
			mFragments = _ALFragments;
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return mFragments.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mFragments.size();
		}

}


	private void showLoadingProgress() {
		if (isScraping) {
			mLoadingLayout.setVisibility(View.VISIBLE);
			AnimationDrawable frameAnimation = (AnimationDrawable) mLoading.getDrawable();
			frameAnimation.setCallback(mLoading);
			frameAnimation.setVisible(true, true);
			frameAnimation.start();
		}
	}

	private void hideLoadingProgress() {
		AnimationDrawable frameAnimation = (AnimationDrawable) mLoading.getDrawable();
		frameAnimation.setCallback(mLoading);
		frameAnimation.setVisible(false, false);
		frameAnimation.stop();
		mLoadingLayout.setVisibility(View.GONE);
	}

	private void checkScrapState(ArrayList<ScrapRequester> list) {
		if (list.size() > 0) {
			isScraping = true;
			showLoadingProgress();
		} else {
			isScraping = false;
			hideLoadingProgress();
		}
	}

	private void initBinderService() {
		mServiceConnection = new ServiceConnection() {

			@Override
			public void onServiceDisconnected(ComponentName name) {
				mService = null;
				isConnection = false;
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				isConnection = true;
				mService = new Messenger(service);
				registerMessenger();
				requestQueueList();
			}
		};

		if(ElseUtils.isServiceRunning(this, BatchService.class.getName()) == false) {
			ComponentName compName = new ComponentName(getPackageName(), BatchService.class.getName());
			startService(new Intent().setComponent(compName));
		}
		bindService(new Intent(this, BatchService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	private void registerMessenger() {
		if (isConnection) {
			try {
				// Give it some value as an example.
				Message msg = Message.obtain(null,
						BatchService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	private void requestQueueList() {
		if (isConnection) {
			try {
				// Give it some value as an example.
				Message msg = Message.obtain(null,
						BatchService.MSG_REQUEST_QUEUE, this.hashCode(), 0);
				mService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	/** Binder Service End **/
	private void finishBinderService() {
		if (isConnection) {
			if (mService != null) {
				try {
					Message msg = Message.obtain(null,
							BatchService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

			unbindService(mServiceConnection);
			isConnection = false;
		}
	}

	class IncomingHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case BatchService.MSG_UPLOAD_STATUS:
//                    Log.d("test", "MSG_UPLOAD_STATUS : "+msg.arg1);
					switch (msg.arg1) {
						case ScrapingModule.STATUS_COMPLETE:
							if (mMode == ProcessMode.COLLECT_FIRST_THIS_MONTH) {

							} else {

							}
							break;
						case ScrapingModule.STATUS_PREPARE:
							break;
						case ScrapingModule.STATUS_REQUEST:
							break;
						case ScrapingModule.STATUS_SCRAPING:
							//String message = (String) msg.obj;
							break;
						case ScrapingModule.STATUS_UPLOADING:
							break;
					}
					break;
				case BatchService.MSG_UPLOAD_COMPLETE:
					break;
				case BatchService.MSG_UPLOAD_MODE:
					int mode = msg.arg1;
					ProcessMode[] modes = ProcessMode.values();
					mMode = modes[mode];
//                    Log.d("test", "MSG_UPLOAD_MODE : "+mMode);
					switch (mMode) {
						case COLLECT_FIRST_THIS_MONTH:
							break;
						case COLLECT_FIRST_PAST_MONTH:
						case COLLECT_DAILY:
						case COLLECT_INDIVIDUAL:
						case COLLECT_RETRY:
							break;
						case COLLECT_IDLE:
							break;
					}
					break;
				case BatchService.MSG_REQUEST_QUEUE:
				case BatchService.MSG_BANK_COMPLETE:
					ArrayList<ScrapRequester> list = (ArrayList<ScrapRequester>) msg.obj;
					checkScrapState(list);
					break;
				default:
					super.handleMessage(msg);
					break;
			}
		}
	}
    
}
