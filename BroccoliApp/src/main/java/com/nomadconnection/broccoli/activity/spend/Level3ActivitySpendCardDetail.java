package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.RequestCardList;
import com.nomadconnection.broccoli.data.Spend.ResponseCardList;
import com.nomadconnection.broccoli.data.Spend.SpendCardDetail;
import com.nomadconnection.broccoli.data.Spend.SpendCardDetailBenefitInternalTotal;
import com.nomadconnection.broccoli.data.Spend.SpendCardUserListData;
import com.nomadconnection.broccoli.fragment.spend.FragmentSpendCardBenefit;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.fragment.spend.FragmentSpendCardList.ENABLE_COMPANY;
import static com.nomadconnection.broccoli.fragment.spend.FragmentSpendCardList.RECOMMEND_YN;

/**
 * Created by YelloHyunminJang on 2017. 3. 20..
 */

public class Level3ActivitySpendCardDetail extends BaseActivity implements View.OnClickListener, InterfaceFragmentInteraction {

    public static final int REQUEST_CODE_MEMO = 1;

    private ImageView mLogo;
    private TextView mTitle;
    private TextView mMemo;
    private View mMemoBtn;
    private ProgressBar mGraph;
    private TextView mUsed;
    private TextView mRequirement;
    private TextView mBenefitFirstTitle;
    private TextView mBenefitFirstValue;
    private TextView mBenefitSecondTitle;
    private TextView mBenefitSecondValue;
    private ImageView mAnnualFeeBtn;
    private TextView mAnnualFee;
    private TextView mLimit;
    private View mBtn;
    private FragmentSpendCardBenefit mBenefitFragment;
    private NestedScrollView mScrollView;
    private int mRecommendYn = 0;
    private List<String> mEnableCompany;

    private SpendCardUserListData mData;
    private SpendCardDetail mDetailData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_spend_card_detail);

        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SpendCardInfoDetail);
        initWidget();
        initData();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_card_detail_title);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setText(R.string.spend_card_detail_resetting);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));

        mScrollView = (NestedScrollView) findViewById(R.id.sv_spend_card_detail);
        mScrollView.setNestedScrollingEnabled(true);
        mScrollView.setFillViewport(true);
        mLogo = (ImageView) findViewById(R.id.iv_spend_card_detail_logo);
        mTitle = (TextView) findViewById(R.id.tv_spend_card_detail_title);
        mMemo = (TextView) findViewById(R.id.tv_spend_card_detail_memo);
        mMemoBtn = findViewById(R.id.fl_spend_card_detail_memo_btn);
        mGraph = (ProgressBar) findViewById(R.id.progress_spend_card_graph);
        mUsed = (TextView) findViewById(R.id.tv_row_spend_card_used);
        mRequirement = (TextView) findViewById(R.id.tv_row_spend_card_requirement);
        mBenefitFirstTitle = (TextView) findViewById(R.id.tv_spend_card_detail_benefit_first_title);
        mBenefitFirstValue = (TextView) findViewById(R.id.tv_spend_card_detail_benefit_first_value);
        mBenefitSecondTitle = (TextView) findViewById(R.id.tv_spend_card_detail_benefit_second_title);
        mBenefitSecondValue = (TextView) findViewById(R.id.tv_spend_card_detail_benefit_second_value);
        mAnnualFeeBtn = (ImageView) findViewById(R.id.iv_spend_card_detail_fee_more_btn);
        mAnnualFee = (TextView) findViewById(R.id.tv_spend_card_detail_fee);
        mLimit = (TextView) findViewById(R.id.tv_spend_card_detail_limit);
        mBtn = findViewById(R.id.btn_confirm);
        mMemoBtn.setOnClickListener(this);
        mAnnualFeeBtn.setOnClickListener(this);
        mBtn.setOnClickListener(this);
    }

    private void initData() {
        Intent intent = getIntent();
        mRecommendYn = intent.getIntExtra(RECOMMEND_YN, 0);
        mEnableCompany = (ArrayList<String>) intent.getSerializableExtra(ENABLE_COMPANY);
        mData = (SpendCardUserListData) intent.getSerializableExtra(Const.DATA);
        mLogo.setImageResource(ElseUtils.createCardLogoItem(mData.cmpnyCode));
        mTitle.setText(mData.card.cardName);
        mMemo.setText(mData.memo);
        mUsed.setText(getResources().getString(R.string.spend_card_usage)+" "+ElseUtils.getDecimalFormat(mData.expenseAmt));
        mRequirement.setText(getResources().getString(R.string.spend_card_need)+" "+ ElseUtils.getDecimalFormat(mData.card.requirement));
        mAnnualFee.setText(ElseUtils.getDecimalFormat(mData.card.internalAnnualFee == 0 ? mData.card.foreignAnnualFee:mData.card.internalAnnualFee));
        mLimit.setText(ElseUtils.getDecimalFormat(mData.maxLimitAmt));

        float value = 0;
        if (mData.card.requirement != 0) {
            value = (long)( (double)mData.expenseAmt / (double)mData.card.requirement * 100.0 );
        } else if (mData.card.requirement == 0 && mData.expenseAmt == 0) {
            value = 0;
        } else {
            value = 999;
        }
        if (0 >= value) {
            mUsed.setTextColor(getResources().getColor(R.color.common_subtext_color));
            mGraph.setProgress(0);
        } else if (value < 100) {
            mUsed.setTextColor(getResources().getColor(R.color.spend_card_progress_font_color));
            mGraph.setProgress(((int) value));
            Drawable drawable = mGraph.getProgressDrawable();
            if (drawable instanceof LayerDrawable) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
                shapeDrawable.setColorFilter(mGraph.getResources().getColor(R.color.spend_card_graph_progress_color), PorterDuff.Mode.SRC_IN);
            } else if (drawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                gradientDrawable.setColor(mGraph.getResources().getColor(R.color.spend_card_graph_progress_color));
            }
        } else {
            mUsed.setTextColor(getResources().getColor(R.color.spend_card_graph_over_color));
            mGraph.setProgress(mGraph.getMax());
            Drawable drawable = mGraph.getProgressDrawable();
            if (drawable instanceof LayerDrawable) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
                shapeDrawable.setColorFilter(mGraph.getResources().getColor(R.color.spend_card_graph_over_color), PorterDuff.Mode.SRC_IN);
            } else if (drawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                gradientDrawable.setColor(mGraph.getResources().getColor(R.color.spend_card_graph_over_color));
            }
        }

        getData();
    }

    private void getData() {
        DialogUtils.showLoading(this);
        String cardCode = mData.card.cardCode;
        if (cardCode == null) {
            cardCode = mData.cardCode;
        }
        Call<JsonElement> call = ServiceGenerator.createService(SpendApi.class).getMyCardDetail(String.valueOf(mData.cardId), cardCode);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                JsonObject object = element.getAsJsonObject();
                if (object.has("benefitList")) {
                    mDetailData = new Gson().fromJson(object, SpendCardDetail.class);
                    setData(mDetailData);
                } else {
                    setData(null);
                    ErrorUtils.parseError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void setData(SpendCardDetail data) {
        boolean isEnableCompany = false;
        if (mEnableCompany != null && mEnableCompany.size() > 0) {
            if (mEnableCompany.contains(mData.cmpnyCode)) {
                isEnableCompany = true;
            }
        }
        if (mRecommendYn > 0 && isEnableCompany && (mDetailData != null && mDetailData.benefit != null)) {
            mBtn.setVisibility(View.VISIBLE);
        } else {
            mBtn.setVisibility(View.GONE);
        }
        if (data != null) {
            if (data.arr_annualFeeList == null || data.arr_annualFeeList.size() == 0) {
                mAnnualFeeBtn.setVisibility(View.GONE);
            } else {
                mAnnualFeeBtn.setVisibility(View.VISIBLE);
            }

            if (mDetailData.benefit != null) {
                Integer discount = mDetailData.benefit.get("discount");
                Integer point = mDetailData.benefit.get("point");
                Integer mileage = mDetailData.benefit.get("mileage");

                if (mData.card.mileageYn > 0) {
                    mBenefitFirstTitle.setText(getResources().getString(R.string.spend_card_detail_mileage));
                    mBenefitFirstValue.setText(ElseUtils.getDecimalFormat(mileage));
                    mBenefitSecondTitle.setVisibility(View.GONE);
                    mBenefitSecondValue.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBenefitFirstValue.getLayoutParams();
                    if (params != null) {
                        params.addRule(RelativeLayout.CENTER_IN_PARENT);
                        params.removeRule(RelativeLayout.ALIGN_PARENT_TOP);
                    }
                } else {
                    mBenefitSecondTitle.setVisibility(View.VISIBLE);
                    mBenefitSecondValue.setVisibility(View.VISIBLE);
                    mBenefitFirstTitle.setText(getResources().getString(R.string.spend_card_detail_discount));
                    mBenefitFirstValue.setText(ElseUtils.getDecimalFormat(discount));
                    mBenefitSecondTitle.setText(getResources().getString(R.string.spend_card_detail_point));
                    mBenefitSecondValue.setText(ElseUtils.getDecimalFormat(point));
                }
            } else {
                if (mData.card.mileageYn > 0) {
                    mBenefitFirstTitle.setText(getResources().getString(R.string.spend_card_detail_mileage));
                    mBenefitFirstValue.setText("-");
                    mBenefitSecondTitle.setVisibility(View.GONE);
                    mBenefitSecondValue.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBenefitFirstValue.getLayoutParams();
                    if (params != null) {
                        params.addRule(RelativeLayout.CENTER_IN_PARENT);
                        params.removeRule(RelativeLayout.ALIGN_PARENT_TOP);
                    }
                    mBenefitFirstValue.setLayoutParams(params);
                } else {
                    mBenefitFirstTitle.setText(getResources().getString(R.string.spend_card_detail_discount));
                    mBenefitFirstValue.setText("-");
                    mBenefitSecondTitle.setText(getResources().getString(R.string.spend_card_detail_point));
                    mBenefitSecondValue.setText("-");
                }
            }

            SpendCardDetailBenefitInternalTotal benefitInternalTotal = new SpendCardDetailBenefitInternalTotal();
            benefitInternalTotal.cardBenefitAffiliate = data.benefitAffiliateList;
            benefitInternalTotal.cardBenefitList = data.benefitList;
            showBenefit(benefitInternalTotal);
        } else {
            mAnnualFeeBtn.setVisibility(View.GONE);
            if (mData.card.mileageYn > 0) {
                mBenefitFirstTitle.setText(getResources().getString(R.string.spend_card_detail_mileage));
                mBenefitFirstValue.setText("-");
                mBenefitSecondTitle.setVisibility(View.GONE);
                mBenefitSecondValue.setVisibility(View.GONE);
            } else {
                mBenefitFirstTitle.setText(getResources().getString(R.string.spend_card_detail_discount));
                mBenefitFirstValue.setText("-");
                mBenefitSecondTitle.setText(getResources().getString(R.string.spend_card_detail_point));
                mBenefitSecondValue.setText("-");
            }
        }
    }

    private void showBenefit(SpendCardDetailBenefitInternalTotal data) {
        if (data.cardBenefitList != null && data.cardBenefitList.size() > 0 ||
                data.cardBenefitAffiliate != null && data.cardBenefitAffiliate.size() > 0) {
            mBenefitFragment = new FragmentSpendCardBenefit();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Const.DATA, data);
            mBenefitFragment.setArguments(bundle);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            ft.replace(R.id.fl_fragment_benefit, mBenefitFragment, mBenefitFragment.getTagName());
            ft.commitAllowingStateLoss();
        }
    }

    private void editMemo() {
        Intent intent = new Intent(this, Level4ActivitySpendCardMemo.class);
        intent.putExtra(Const.DATA, mData);
        startActivityForResult(intent, REQUEST_CODE_MEMO);
    }

    private List<ResponseCardList> mCardInfoList;

    private void getCardList(String companyCode){
        DialogUtils.showLoading(getActivity());	//소비 로딩 추가
        RequestCardList mRequestCardList = new RequestCardList();
        mRequestCardList.setUserId(Session.getInstance(getActivity()).getUserId());
        mRequestCardList.setCompanyCode(companyCode);

        SpendApi api = ServiceGenerator.createService(SpendApi.class);
        Call<List<ResponseCardList>> call = api.getCardList(mRequestCardList);
        call.enqueue(new Callback<List<ResponseCardList>>() {
            @Override
            public void onResponse(Call<List<ResponseCardList>> call, Response<List<ResponseCardList>> response) {
                DialogUtils.dismissLoading();	//소비 로딩 추가
                mCardInfoList = new ArrayList<ResponseCardList>();
                ResponseCardList mNoneData = new ResponseCardList();
                mNoneData.setCardCode("");
                mNoneData.setCardName(getActivity().getString(R.string.common_text_no));
                mCardInfoList.add(mNoneData);
                for (int r = 0; r < response.body().size(); r++) {
                    mCardInfoList.add(response.body().get(r));
                }

                showCardList();//리스트 팝업 부르기
            }

            @Override
            public void onFailure(Call<List<ResponseCardList>> call, Throwable t) {
                DialogUtils.dismissLoading();	//소비 로딩 추가
                ElseUtils.network_error(getActivity());
            }
        });
    }


    private void changeCardMatching(String cardId, String cardCode) {
        DialogUtils.dismissLoading();	//소비 로딩 추가
        HashMap<String, Object> map = new HashMap<>();
        map.put("cardId", Long.valueOf(cardId));
        map.put("cardCode", cardCode);

        SpendApi api = ServiceGenerator.createService(SpendApi.class);
        Call<Result> call = api.setMatchingCard(map);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();	//소비 로딩 추가
                if(response.body().isOk()){
                    setResult(RESULT_OK);
                } else {
                    Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();	//소비 로딩 추가
                ElseUtils.network_error(getActivity());
            }
        });

    }

    private void showCardList(){
        DialogUtils.showDialogCardSelectList(getActivity(), mCardInfoList, null, new ExpandableListView.OnChildClickListener(){

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ResponseCardList mCardList = (ResponseCardList) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);
                String cardId;
                if (id == -1) {
                    cardId = String.valueOf(mData.cardId);
                    changeCardMatching(cardId, "");
                } else {
                    cardId = String.valueOf(mData.cardId);
                    changeCardMatching(cardId, mCardList.getCardCode());
                }
                Level3ActivitySpendCardDetail.this.setResult(RESULT_OK);
                finish();
                return false;
            }

        }).show();
    }

    private void showCardRecommend() {
        Intent intent = new Intent(this, Level2ActivitySpendCardTabList.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(Level2ActivitySpendCardTabList.REQUEST_SHOW_RECOMMEND_CARD, mData.cardId);
        startActivity(intent);
    }

    private void showAnnualFeeDialog() {
        DialogUtils.showDialogCardAnnualFee(this, mDetailData.arr_annualFeeList).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_MEMO) {
            if (resultCode == RESULT_OK) {
                String memo = data.getStringExtra(Level4ActivitySpendCardMemo.RESULT_MEMO);
                mData.memo = memo;
                mMemo.setText(memo);
                setResult(RESULT_OK);
            }
        }
    }

    @Override
    public void onBtnClickNaviMotion(View view) {
        super.onBtnClickNaviMotion(view);
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_motion_else:
                getCardList(mData.cmpnyCode);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fl_spend_card_detail_memo_btn:
                editMemo();
                break;
            case R.id.iv_spend_card_detail_fee_more_btn:
                showAnnualFeeDialog();
                break;
            case R.id.btn_confirm:
                showCardRecommend();
                break;
        }
    }

    @Override
    public void reset(boolean flag) {

    }

    @Override
    public boolean canScrollUp() {
        return false;
    }

    @Override
    public boolean canScrollDown() {
        boolean ret = false;
        if (mScrollView != null) {
            ret = ViewCompat.canScrollVertically(mScrollView, 1);
        }
        return ret;
    }
}
