package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;

/**
 * Created by YelloHyunminJang on 2017. 1. 4..
 */

public class Level4ActivitySettingEmailSendComplete extends BaseActivity {

    public static final String EMAIL = "EMAIL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_send_email_complete);
        init();
    }

    private void init() {
        findViewById(R.id.ll_navi_img_txt_back).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_user_account);

        TextView email = (TextView) findViewById(R.id.tv_activity_setting_email_complete);
        View btn = findViewById(R.id.btn_confirm);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra(EMAIL)) {
            String certMail = intent.getStringExtra(EMAIL);
            email.setText(certMail);
        }
    }
}
