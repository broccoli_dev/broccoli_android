package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;

/**
 * Created by YelloHyunminJang on 2016. 12. 26..
 */

public class SettingCompleteActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_complete);
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });

        Intent intent = getIntent();
        String title = intent.getStringExtra(Const.TITLE);
        String content = intent.getStringExtra(Const.CONTENT);
        setTitle(title);
        setContent(content);

        sendTracker(AnalyticsName.FindPwResultConfirm);
    }

    private void setTitle(String title) {
        ((TextView) findViewById(R.id.tv_navi_img_txt_title)).setText(title);
    }

    private void setContent(String content) {
        ((TextView) findViewById(R.id.tv_activity_common_complete)).setText(content);
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        switch (view.getId()) {
            case R.id.ll_navi_img_txt_back:
                setResult(RESULT_OK);
                finish();
                break;
            default:
                break;
        }
    }

}
