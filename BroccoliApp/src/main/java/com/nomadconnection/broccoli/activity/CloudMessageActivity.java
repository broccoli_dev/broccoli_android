package com.nomadconnection.broccoli.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;

/**
 * Created by YelloHyunminJang on 2016. 10. 4..
 */

public class CloudMessageActivity extends BaseActivity implements View.OnClickListener {

    public static final String PARAMETER_TITLE = "title";
    public static final String PARAMETER_MESSAGE = "message";

    private View mTitleLayout;
    private TextView mTitle;
    private TextView mContent;
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                // 키잠금 해제하기
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                // 화면 켜기
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_FULLSCREEN);
        WindowManager.LayoutParams winLayoutParam = new WindowManager.LayoutParams();
        winLayoutParam.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        winLayoutParam.dimAmount = 0.5f;
        getWindow().setAttributes(winLayoutParam);
        setContentView(R.layout.activity_cloud_message_dialog);

        init();
    }

    private void init() {
        initWidget();
    }

    private void initWidget() {
        mTitleLayout = findViewById(R.id.ll_activity_alert_dialog_title_layout);
        mTitle = (TextView)findViewById(R.id.tv_activity_alert_dialog_title);
        mContent =(TextView)findViewById(R.id.tv_activity_alert_dialog_content);
        mButton = (Button)findViewById(R.id.btn_activity_alert_dialog_click);
        mButton.setOnClickListener(this);
    }

    private void initData() {
        Bundle bun = getIntent().getExtras();
        String title = bun.getString(PARAMETER_TITLE);
        String content = bun.getString(PARAMETER_MESSAGE);

        if (title == null || title.isEmpty()) {
            mTitle.setText("알림");
        } else {
            mTitle.setText(title);
        }
        mTitle.setTypeface(Typeface.SANS_SERIF);
        mContent.setTypeface(Typeface.SANS_SERIF);
        mButton.setText(getResources().getString(R.string.common_confirm));
        mButton.setTypeface(Typeface.SANS_SERIF);
        mContent.setText(content);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_activity_alert_dialog_click:
                finish();
                break;
        }
    }
}
