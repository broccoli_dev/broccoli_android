package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetCategory;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetData;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetSettingData;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendBudgetSetting.SHOW_LOOKUP_ACTIVITY;

/**
 * Created by YelloHyunminJang on 2017. 2. 28..
 */

public class Level4ActivitySpendBudgetSettingGroupCategory extends BaseActivity implements View.OnClickListener {

    private TextView mTotalBudget;
    private LinearLayout mContentView;
    private View mGraph;
    private TextView mCurrentBudget;
    private View mBtn;

    private String mTargetYearMonth;
    private long mTotalBudgetValue = 0;
    private ArrayList<SpendBudgetSettingData> mSelectList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_spend_budget_setting_large_category);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SpendBudgetAddDetail);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_budget_setting_title);
        findViewById(R.id.tv_navi_motion_else).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundResource(R.color.main_2_layout_bg);
        mTotalBudget = (TextView) findViewById(R.id.tv_category_total_budget);
        mContentView = (LinearLayout) findViewById(R.id.ll_category_setting);
        mGraph = findViewById(R.id.ll_spend_budget_current_graph_amount);
        mCurrentBudget = (TextView) findViewById(R.id.tv_spend_budget_current_value);
        mBtn = findViewById(R.id.btn_confirm);
        mBtn.setOnClickListener(this);
    }

    private void initData() {
        Intent intent = getIntent();
        mTargetYearMonth = intent.getStringExtra(Const.BUDGET_TARGET_MONTH);
        String totalBudget = intent.getStringExtra(Const.SPEND_TOTAL_BUDGET);
        mSelectList = (ArrayList<SpendBudgetSettingData>) intent.getSerializableExtra(Const.SPEND_BUDGET_SELECT_DATA);
        mTotalBudget.setText(ElseUtils.getStringDecimalFormat(totalBudget));
        mTotalBudgetValue = Long.parseLong(totalBudget.replaceAll(",", ""));
        mBtn.setEnabled(false);

        createData(mSelectList);
    }

    private void createData(ArrayList<SpendBudgetSettingData> data) {
        ArrayList<SpendBudgetSettingData> list = new ArrayList<>();
        list.addAll(data);

        showContent();
    }

    private void showContent() {
        mContentView.removeAllViews();
        for (int i = 0; i < mSelectList.size(); i++) {
            ViewHolder holder = new ViewHolder();
            View view = this.getLayoutInflater().inflate(R.layout.row_list_item_spend_budget_setting_large_category, mContentView, false);
            holder.mTitle = (TextView) view.findViewById(R.id.tv_row_spend_budget_title);
            holder.mValue = (EditText) view.findViewById(R.id.et_row_spend_budget_category);
            holder.mWatcher = new IndexTextWatcher(holder);
            holder.mWatcher.setIndex(i);
            view.setTag(holder);

            holder.mTitle.setText(mSelectList.get(i).title);
            String expense = mSelectList.get(i).expense;
            long expenseValue = Long.valueOf(expense);
            expenseValue = Math.round(expenseValue/10000.0);
            holder.mValue.setHint(ElseUtils.getDecimalFormat(expenseValue));
            holder.mValue.setText(mSelectList.get(i).budget);
            holder.mValue.addTextChangedListener(holder.mWatcher);
            mContentView.addView(view);
        }
    }

    private class ViewHolder {
        private TextView mTitle;
        private EditText mValue;
        private IndexTextWatcher mWatcher;
    }

    private class IndexTextWatcher implements TextWatcher {
        private int index = 0;
        private String strAmount = "";
        private ViewHolder holder;

        public IndexTextWatcher(ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            SpendBudgetSettingData data = mSelectList.get(getIndex());
            String text = s.toString();
            if (!strAmount.equalsIgnoreCase(text)) {
                strAmount = makeStringComma(text.replaceAll(",", ""));
                holder.mValue.setText(strAmount);
            }
            if (text != null) {
                data.budget = text.replaceAll(",", "");
                mObserver.onChanged();
            }
            if (s.length() > 0) {
                Selection.setSelection(s, s.length());
            }
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        protected String makeStringComma(String str) {
            if (str.length() == 0)
                return "";
            long value = Long.parseLong(str);
            DecimalFormat format = new DecimalFormat("###,###");
            return format.format(value);
        }
    }

    private DataSetObserver mObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            List<SpendBudgetSettingData> list = mSelectList;
            int count = 0;
            long currentValue = 0;
            if (list != null) {
                for (int i=0; i < list.size(); i++) {
                    String budget = list.get(i).budget;
                    if (budget != null) {
                        budget = budget.replaceAll(",", "");
                        if (list.get(i).budget != null && !TextUtils.isEmpty(budget)) {
                            currentValue += Long.valueOf(budget);
                            count++;
                        }
                    }
                }
            }

            currentValue = (long) (currentValue * 10000.0);
            long leftValue = mTotalBudgetValue - currentValue;
            if (leftValue > 0) {
                mCurrentBudget.setText(ElseUtils.getDecimalFormat(leftValue)+" 남음");
            } else if (leftValue == 0) {
                mCurrentBudget.setText(ElseUtils.getDecimalFormat(leftValue)+" 남음");
            } else {
                mCurrentBudget.setText(ElseUtils.getDecimalFormat(Math.abs(leftValue))+" 초과");
            }

            setGraph(currentValue, leftValue);

            if (count == list.size()) {
                mBtn.setEnabled(true);
            } else {
                mBtn.setEnabled(false);
            }
        }
    };

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_confirm:
                registerBudget();
                break;
        }
    }

    private void setGraph(long current, long left) {
        Animation animation = new AlphaAnimation(0.2f, 1);
        animation.setDuration(200);

        if (current == 0) {
            if (mTotalBudgetValue != 0) {
                float value = (float) ((double)current/(double)mTotalBudgetValue);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraph.getLayoutParams();
                params.weight = value;
                mGraph.setLayoutParams(params);
                Drawable drawable = mGraph.getBackground();
                if (left > 0) {
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                    } else if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ShapeDrawable shapeDrawable = (ShapeDrawable) layerDrawable.getDrawable(0);
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.common_white));
                    }
                } else {
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    } else if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ShapeDrawable shapeDrawable = (ShapeDrawable) layerDrawable.getDrawable(0);
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    }
                }
            }
        } else {
            mGraph.setVisibility(View.VISIBLE);
            mGraph.setAnimation(animation);
            if (mTotalBudgetValue != 0) {
                float value = (float) ((double)current/(double)mTotalBudgetValue);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraph.getLayoutParams();
                params.weight = value;
                mGraph.setLayoutParams(params);
                Drawable drawable = mGraph.getBackground();
                if (left > 0) {
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                    } else if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ShapeDrawable shapeDrawable = (ShapeDrawable) layerDrawable.getDrawable(0);
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.common_white));
                    }
                } else {
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    } else if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ShapeDrawable shapeDrawable = (ShapeDrawable) layerDrawable.getDrawable(0);
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_graph_over));
                    }
                }
            }
        }
    }

    private void registerBudget() {
        long highestSum = mTotalBudgetValue;
        List<SpendBudgetSettingData> list = mSelectList;
        List<SpendBudgetCategory> categoryList = new ArrayList<>();
        long currentValue = 0;
        if (list != null) {
            for (int i=0; i < list.size(); i++) {
                String budget = list.get(i).budget;
                if (budget != null) {
                    budget = budget.replaceAll(",", "");
                    if (list.get(i).budget != null && !TextUtils.isEmpty(budget)) {
                        currentValue += Long.valueOf(budget);
                        SpendBudgetCategory category = new SpendBudgetCategory();
                        category.categoryGroup = list.get(i).index;
                        category.sum = Long.valueOf(list.get(i).budget)*10000;
                        categoryList.add(category);
                    }
                }
            }
        }
        currentValue = (long) (currentValue * 10000.0);
        if (highestSum < currentValue) {
            highestSum = currentValue;
        }
        SpendBudgetData data = new SpendBudgetData();
        data.baseMonth = mTargetYearMonth;
        data.totalSum = highestSum;
        data.categoryList = categoryList;
        Call<Result> call = ServiceGenerator.createService(SpendApi.class).setBudget(data);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if(getActivity() == null || getActivity().isFinishing()){
                    return;
                }

                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        Result result = response.body();
                        if (result != null && result.isOk()) {
                            Intent intent = new Intent(getBase(), Level2ActivitySpendBudgetSetting.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra(SHOW_LOOKUP_ACTIVITY, true);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getBase(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 다시 등록해 주세요.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        ErrorMessage message = ErrorUtils.parseError(response);
                        switch (message) {
                            case ERROR_713_NO_SUCH_USER:
                                Toast.makeText(getBase(), "가입되지 않은 사용자입니다. 새로 회원가입해주세요.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_719_QUITE_USER:
                                Toast.makeText(getBase(), "탈퇴 회원은 탈퇴일로부터 5일 이후 재가입 가능합니다.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_1011_CERT_SESSION_EXPIRE:
                                Toast.makeText(getBase(), "인증시간이 만료되었습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_1012_CERT_NOT_FINISH:
                                Toast.makeText(getBase(), "인증에 실패하였습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                ElseUtils.network_error(getBase());
            }
        });
    }
}
