package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 1. 3..
 */

public class Level3SettingInputCode extends BaseActivity {

    private EditText mEditText;
    private Button mBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_setting_input_code);
        sendTracker(AnalyticsName.SettingRegCodeInput);
        init();
    }

    private void init() {
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_user_account);
        mBtn = (Button) findViewById(R.id.btn_confirm);
        mBtn.setEnabled(false);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
        mEditText = (EditText) findViewById(R.id.et_activity_setting_input_code);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mBtn.setEnabled(true);
                } else {
                    mBtn.setEnabled(false);
                }
            }
        });
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

    private void next() {
        final String code = mEditText.getText().toString();
        DayliApi api = ServiceGenerator.createServiceDayli(DayliApi.class);
        String token = DbManager.getInstance().getUserDayliToken(this, Session.getInstance(this).getUserId());
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", code);
        Call<Result> call = api.sendCouponCode(token, map);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response != null && (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK))) {
                    showComplete();
                } else {
                    Toast.makeText(Level3SettingInputCode.this, "코드 등록에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    private void showComplete() {
        Intent intent = new Intent(this, SettingCompleteActivity.class);
        intent.putExtra(Const.TITLE, "코드 등록 완료");
        intent.putExtra(Const.CONTENT, "코드 등록이 완료되었습니다.");
        startActivity(intent);
        finish();
    }
}
