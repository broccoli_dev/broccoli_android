package com.nomadconnection.broccoli.activity.membership;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.fragment.FragmentMembershipUpdateStep0;
import com.nomadconnection.broccoli.activity.base.BaseActivity;

/**
 * Created by YelloHyunminJang on 2016. 12. 13..
 */

public class OldMemberUpdateActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_membership_update);

        init();
    }

    private void init() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(R.id.ll_activity_membership_update_fragment_area, new FragmentMembershipUpdateStep0());
        ft.commit();
    }
}
