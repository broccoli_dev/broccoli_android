package com.nomadconnection.broccoli.activity.spend;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.spend.AdtExpListViewSpendBudgetFavoriteCategory;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.CustomExpandableListView;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.BudgetCategory;
import com.nomadconnection.broccoli.constant.BudgetGroupCategory;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetFavorite;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 2. 28..
 */

public class Level2ActivitySpendBudgetFavoriteCategory extends BaseActivity {

    private static final int MAX_FAVORITE_COUNT = 3;

    private CustomExpandableListView mExListView;
    private AdtExpListViewSpendBudgetFavoriteCategory mAdapter;
    private ArrayList<BudgetGroupCategory> mGroupList = new ArrayList<>();
    private ArrayList<ArrayList<SpendBudgetFavorite>> mChildList = new ArrayList<>();
    private List<Integer> mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_spend_budget_favorite_category);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SpendBudgetBookmark);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_budget_favorite_category_title);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundResource(R.color.main_2_layout_bg);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setText(R.string.common_complete);
        mExListView = (CustomExpandableListView) findViewById(R.id.elv_category_setting);
        mAdapter = new AdtExpListViewSpendBudgetFavoriteCategory(this, mGroupList, mChildList);
        WrapperExpandableListAdapter wrapperExpandableListAdapter = new WrapperExpandableListAdapter(mAdapter);
        mExListView.setAdapter(wrapperExpandableListAdapter);
        mExListView.setSelectionAfterHeaderView();
        mExListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                SpendBudgetFavorite favorite = mAdapter.getChild(groupPosition, childPosition);
                if (favorite.isFavorite) {
                    favorite.isFavorite = false;
                    mAdapter.notifyDataSetChanged();
                } else {
                    if (isPossibleCheckFavorite()) {
                        favorite.isFavorite = true;
                        mAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getBase(), getString(R.string.spend_budget_favorite_category_reg_fail), Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });
    }

    private void initData() {
        getData();
    }

    private void getData() {
        DialogUtils.showLoading(this);
        Call<JsonElement> call = ServiceGenerator.createService(SpendApi.class).getFavoriteCategory();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                JsonObject object = element.getAsJsonObject();
                JsonArray array = object.getAsJsonArray("categoryIds");
                if (array != null) {
                    Type listType = new TypeToken<List<Integer>>() {}.getType();
                    mData = new Gson().fromJson(array, listType);
                    setData(mData);
                } else {
                    ErrorUtils.parseError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void setData(List<Integer> list) {
        mGroupList.clear();
        mChildList.clear();
        BudgetGroupCategory[] group = BudgetGroupCategory.values();
        for (BudgetGroupCategory groupCategory : group) {
            if (groupCategory.ordinal() != 0) {
                mGroupList.add(groupCategory);
                ArrayList<SpendBudgetFavorite> detailList = new ArrayList<>();
                BudgetCategory[] detailCategory = groupCategory.child();
                for (BudgetCategory detail : detailCategory) {
                    if (detail.ordinal() != 0) {
                        SpendBudgetFavorite favorite = new SpendBudgetFavorite();
                        favorite.id = detail;
                        favorite.isFavorite = list.contains(detail.ordinal());
                        detailList.add(favorite);
                    }
                }
                mChildList.add(detailList);
            }
        }
        mAdapter.setData(mGroupList, mChildList);
        mAdapter.notifyDataSetChanged();
        for (int i = 0; i <mGroupList.size() ; i++) {
            mExListView.expandGroup(i);
        }
    }

    private boolean isPossibleCheckFavorite() {
        boolean possible = false;
        int checkedCount = 0;
        for (int i=0; i < mChildList.size(); i++) {
            ArrayList<SpendBudgetFavorite> list = mChildList.get(i);
            for (SpendBudgetFavorite item : list) {
                if (item.isFavorite) {
                    checkedCount++;
                }
            }
        }
        if (checkedCount >= MAX_FAVORITE_COUNT) {
            possible = false;
        } else {
            possible = true;
        }

        return possible;
    }

    private void sendFavorite() {
        List<Long> favoriteList = new ArrayList<>();
        for (int i=0; i < mChildList.size(); i++) {
            ArrayList<SpendBudgetFavorite> list = mChildList.get(i);
            for (SpendBudgetFavorite favorite : list) {
                if (favorite.isFavorite) {
                    favoriteList.add((long) favorite.id.ordinal());
                }
            }
        }
        HashMap<String, List<Long>> hashMap = new HashMap<>();
        hashMap.put("categoryIds", favoriteList);
        Call<Result> resultCall = ServiceGenerator.createService(SpendApi.class).setFavoriteCategory(hashMap);
        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body() != null && response.message().equalsIgnoreCase(Const.OK)) {
                    if (!response.body().isOk()) {
                        Toast.makeText(getBase(), getString(R.string.spend_budget_favorite_category_reg_error), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getBase(), getString(R.string.spend_budget_favorite_category_reg_success), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                ElseUtils.network_error(getBase());
            }
        });
    }

    public void onBtnClickNaviMotion(View view) {
        super.onBtnClickNaviMotion(view);
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_motion_else:
                sendFavorite();
                break;
        }
    }

}
