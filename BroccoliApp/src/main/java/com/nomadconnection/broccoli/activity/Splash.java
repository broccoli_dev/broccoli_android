package com.nomadconnection.broccoli.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.igaworks.IgawCommon;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.membership.LoginActivity;
import com.nomadconnection.broccoli.activity.membership.OldMemberUpdateActivity;
import com.nomadconnection.broccoli.api.FileDownloadService;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.ServiceUrl;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.CdnAppInfoData;
import com.nomadconnection.broccoli.data.InfoDataAppVersion;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.data.Session.OldUserData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.dialog.DialogBaseTwoButton;
import com.nomadconnection.broccoli.main.BroccoliRegistrationIntentService;
import com.nomadconnection.broccoli.service.BatchService;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.session.LoginManager;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.Crypto;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.NetStatusChecker;
import com.nomadconnection.broccoli.utils.PermissionUtils;
import com.nomadconnection.broccoli.utils.Root;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Splash extends BaseActivity {

	private static final String TAG = "Splash";

	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private static final int RESULT_WIFI_SETTING = 1;
	private static final int RESULT_PERMISSION_SETTING = 2;
	private static final int RESULT_LOCK_SCREEN = 3;

	public static final boolean ISTEST = false;
	public static boolean mTerminate = false;
	/* Test */
	DialogBaseTwoButton mPopup;
	/* Layout */
	private ImageView 			myAnimation;
	/* GCM */
	private String				mUserGcmId;
	/* Service */
	private boolean				mIsConnection = false;
	private Messenger			mService;    // 연결 타입 서비스
	private ServiceConnection 	mServiceConnection;
	private BroadcastReceiver mRegistrationBroadcastReceiver;
	/* Check Step */
	private boolean mCheckVersion = false;
	private boolean mCheckNotice = false;
	private boolean mCheckEvent = false;
	private boolean mCheckGcm = false;
	private boolean mCheckTimeOver = false;
	private boolean mCheckUpdate = false;
	private boolean mCheckInit = false;
	private boolean mCheckAdId = false;
// ===============================================================================//
// Main Looper Handler
// ===============================================================================//
	/* Main Looper Handler */
	Handler mLoopHandler = new Handler(Looper.getMainLooper()) {
		@Override
		public void handleMessage(Message inputMessage) {
			switch(inputMessage.what){
			case APP.APP_STEP_CHECK_RUN :
				if (!mCheckVersion || !mCheckNotice || !mCheckEvent || !mCheckGcm || !mCheckUpdate || !mCheckInit || !mCheckAdId || Session.getInstance(getApplicationContext()).isLock()) {
					if (Session.getInstance(getApplicationContext()).isLock()) {
						showLockScreen();
						mLoopHandler.removeMessages(APP.APP_STEP_CHECK_RUN);
					} else {
						Message mMsg = new Message();
						if (!mCheckTimeOver) mMsg.what = APP.APP_STEP_CHECK_RUN;
						else mMsg.what = APP.APP_STEP_CHECK_TIMEOVER;
						mLoopHandler.sendMessageDelayed(mMsg, 1000);
					}
				} else {
					moveNextActivity();
				}
				break;

			case APP.APP_STEP_CHECK_TIMEOVER :
				if (mPopup.isShowing()) mPopup.dismiss();
				CustomToast.getInstance(Splash.this).showShort("Time Over");
				Splash.this.finish();
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		FacebookSdk.sdkInitialize(getApplicationContext());
		IgawCommon.startApplication(Splash.this); //애드브릭스 추가
		sendTracker(AnalyticsName.SPLASH);
//		BATracker.init(this, "359610019423350");
//		BATracker.actionCompleted(this);
		registBroadcastReceiver();
		myAnimation = (ImageView)findViewById(R.id.splash_img);
		final AnimationDrawable myAnimationDrawable = (AnimationDrawable)myAnimation.getDrawable();
		myAnimation.post(new Runnable() {

			@Override
			public void run() {
				myAnimationDrawable.start();
			}
		});
		Session.getInstance(this).setLock(true);
		preInitialize();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Session.getInstance(this).setLockTemporaryDisable(false);
		getInstanceIdToken();
		getAdId();
	}

	@Override
	protected void onStart() {
		initBinderService();
		gcmFilterAdd();
		super.onStart();
	}

	@Override
	protected void onStop() {
		gcmFilterRemove();
		super.onStop();
	}

	@Override
	protected void onDestroy(){
		finishBinderService();
		super.onDestroy();
	}

//===================================================================================//
// DB
//===================================================================================//	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RESULT_WIFI_SETTING) {
			preInitialize();
		} else if (requestCode == RESULT_PERMISSION_SETTING) {
			preInitialize();
		} else if (requestCode == RESULT_LOCK_SCREEN) {
			Message msg = new Message();
			msg.what = APP.APP_STEP_CHECK_RUN;
			mLoopHandler.sendMessageDelayed(msg, 1000);
		}
	}


//===================================================================================//
// Init
//===================================================================================//

	/** local db init **/
	private boolean initDatabase(){
		boolean bResult = true;

		String app_ver = MainApplication.getVersionName();
		if (Session.getInstance(this).isAutoLoginAvailable()) {
			removeEmptyFinancialData();
			//migrationBank();
		}
		APPSharedPrefer.getInstance(this).setPrefString(APP.SP_USER_TOKEN, null);
		checkValidMembership();

		return bResult;
	}

	private void preInitialize() {
		try{
			if (!NetStatusChecker.getInstance().getNetworkConnectStatus(this)) {
				DialogUtils.showDlgBaseOneButtonNoTitle(Splash.this, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
							showSetting();
						}
					}
				}, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_network_error_guide), "확인"));
				return;
			}

			if (ISTEST) {
				mCheckGcm = true;
			} else {
				if (Root.isDeviceRooted()) {
					DialogUtils.showDlgBaseOneButtonNoTitle(Splash.this, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
								finish();
							}
						}
					}, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_warning_root), "확인"));
					return;
				}
			}

			HashMap<String, PermissionUtils.PermissionState> permissionState = new HashMap<>();
			permissionState.put(Manifest.permission.RECEIVE_SMS, null);
			permissionState.put(Manifest.permission.READ_EXTERNAL_STORAGE, null);
			permissionState.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, null);
			permissionState.put(Manifest.permission.READ_PHONE_STATE, null);
			if (PermissionUtils.checkPermission(this, permissionState)) {
				PermissionUtils.showPermissionDialog(this, permissionState, PermissionUtils.MY_PERMISSIONS_REQUEST_BROCCOLI);
				return;
			} else {
				initialize();
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initialize() {
		if(initDatabase()) {
			if(initData()) {
				initApp();
			} else {
				CustomToast.getInstance(this).showShort("디바이스 정보 읽기 오류");
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case PermissionUtils.MY_PERMISSIONS_REQUEST_BROCCOLI: {
				HashMap<String, PermissionUtils.PermissionState> map = new HashMap<>();
				for (int i=0; i < permissions.length; i++) {
					map.put(permissions[i], null);
				}
				if (PermissionUtils.checkPermission(this, map)) {
					PermissionUtils.determineWhichPermissionDialog(map, mListener);
				} else {
					initialize();
				}
				return;
			}
		}
	}

	private PermissionUtils.OnDeterminePermissionListener mListener = new PermissionUtils.OnDeterminePermissionListener() {

		@Override
		public void onEnabled() {
			initialize();
		}

		@Override
		public void onShowRequestDialog(HashMap<String, PermissionUtils.PermissionState> map) {
			CommonDialogTextType dialogTextType = new CommonDialogTextType(Splash.this, new String[] {"설정", "다시 보기"});
			CommonDialogTextType.TextData main = new CommonDialogTextType.TextData(Splash.this, R.string.guide_permission_main);
			main.mTextStyle = Typeface.BOLD;
			main.mTextSize = 18;
			main.mLineSpace = getResources().getDimension(R.dimen.common_dimens_6);
			dialogTextType.addTextView(main);
			CommonDialogTextType.TextData text1 = new CommonDialogTextType.TextData(Splash.this, R.string.guide_permission_sms);
			text1.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
			text1.mLeftIcon = R.drawable.notify_list_icon;
			text1.mMargin = (int) Splash.this.getResources().getDimension(R.dimen.common_dimens_20);
			CommonDialogTextType.TextData text2 = new CommonDialogTextType.TextData(Splash.this, R.string.guide_permission_storage);
			text2.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
			text2.mLeftIcon = R.drawable.notify_list_icon;
			CommonDialogTextType.TextData text3 = new CommonDialogTextType.TextData(Splash.this, R.string.guide_permission_phone);
			text3.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
			text3.mLeftIcon = R.drawable.notify_list_icon;
			dialogTextType.addTextView(text1);
			dialogTextType.addTextView(text2);
			dialogTextType.addTextView(text3);
			ImageView imageView = new ImageView(dialogTextType.getContext());
			imageView.setImageResource(R.drawable.permission_setting1);
			imageView.setScaleType(ImageView.ScaleType.CENTER);
			Button[] buttons = dialogTextType.getButtons();
			buttons[0].setBackground(getResources().getDrawable(R.drawable.selector_permission_button_popup));
			buttons[0].setTextColor(getResources().getColor(R.color.common_argb_60p_255_255_255));
			buttons[1].setBackground(getResources().getDrawable(R.drawable.selector_permission_button_popup));
			buttons[1].setTextColor(getResources().getColor(R.color.common_white));
			dialogTextType.addCustomBody(imageView);
			dialogTextType.setCancelable(false);
			dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int stringresid) {
					if (stringresid == 0) {
						PermissionUtils.showSettingPermission(Splash.this, RESULT_PERMISSION_SETTING);
					} else {
						preInitialize();
					}
				}
			});
			dialogTextType.show();
		}

		@Override
		public void onShowPermissionSettingDialog(HashMap<String, PermissionUtils.PermissionState> map) {
			CommonDialogTextType dialogTextType = new CommonDialogTextType(Splash.this, new String[] {"권한설정 하러가기"});
			CommonDialogTextType.TextData main = new CommonDialogTextType.TextData(Splash.this, R.string.guide_permission_main);
			main.mTextStyle = Typeface.BOLD;
			main.mTextSize = 18;
			main.mLineSpace = getResources().getDimension(R.dimen.common_dimens_6);
			dialogTextType.addTextView(main);
			CommonDialogTextType.TextData text1 = new CommonDialogTextType.TextData(Splash.this, R.string.guide_permission_sms);
			text1.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
			text1.mMargin = (int) Splash.this.getResources().getDimension(R.dimen.common_dimens_20);
			CommonDialogTextType.TextData text2 = new CommonDialogTextType.TextData(Splash.this, R.string.guide_permission_storage);
			text2.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
			CommonDialogTextType.TextData text3 = new CommonDialogTextType.TextData(Splash.this, R.string.guide_permission_phone);
			text3.mGravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
			dialogTextType.addTextView(text1);
			dialogTextType.addTextView(text2);
			dialogTextType.addTextView(text3);
			ImageView imageView = new ImageView(dialogTextType.getContext());
			imageView.setImageResource(R.drawable.permission_setting1);
			imageView.setScaleType(ImageView.ScaleType.CENTER);
			imageView.setMinimumHeight(400);
			Button[] buttons = dialogTextType.getButtons();
			buttons[0].setBackground(getResources().getDrawable(R.drawable.selector_permission_button_popup));
			buttons[0].setTextColor(getResources().getColor(R.color.common_white));
			dialogTextType.addCustomBody(imageView);
			dialogTextType.setCancelable(false);
			dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int stringresid) {
					if (stringresid == 0) {
						PermissionUtils.showSettingPermission(Splash.this, RESULT_PERMISSION_SETTING);
					}
				}
			});
			dialogTextType.show();
		}
	};

	private boolean initData(){
		try {
			mUserGcmId 		= ""; // 초기화
			mTerminate = false;
			FileDownloadService downloadService = FileDownloadService.retrofit.create(FileDownloadService.class);
			Call<ResponseBody> call = downloadService.downloadFileFromCdn(ServiceUrl.getCdnVersion());
			call.enqueue(new Callback<ResponseBody>() {
				@Override
				public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
					if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
						CdnAppInfoData data = null;
						try {
							String body = response.body().string();
							String decBody = Crypto.decCDNAES(body);
							JsonObject jsonObject = new Gson().fromJson(decBody, JsonObject.class);
							data = new Gson().fromJson(jsonObject.get("aos"), CdnAppInfoData.class);
						} catch (IOException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (data != null) {
							String ver = data.version.min_version;
							CdnAppInfoData.NoticeInfo notice = data.notice;
							if (notice == null || !data.notice_yn) {
								mCheckNotice = true;
							} else {
								DialogUtils.showDialogBaseOneButton(Splash.this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										finish();
									}
								}, notice.title, notice.contents);
								DialogUtils.dismissLoading();
								return;
							}
							if (ver != null) {
								String[] server_ver = ver.split("\\.");
								String[] local_ver = MainApplication.getVersionName().split("\\.");
								int flag = ElseUtils.isHigherVersion(local_ver, server_ver);
								if (flag < 0) {
									InfoDataAppVersion version1 = new InfoDataAppVersion();
									version1.mTxt1 = ver;
									version1.mTxt2 = "알림/Broccoli 신버전이 배포되었습니다./확인을 눌러 신규버전을 받아주세요.";
									version1.mTxt3 = "";
									mPopup = DialogUtils.showDlgBaseTwoButton(Splash.this, new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
														ElseUtils.showPlayStore(Splash.this);
													}
												}
											},
											new InfoDataDialog(R.layout.dialog_inc_case_5, "취소", "확인", getString(R.string.str_init_version_mismatch), version1));
									DialogUtils.dismissLoading();
									return;
								} else {
									mCheckVersion = true;
									setServerAddress(data);
									checkOldMembership();
								}
							}
						}
					} else {
						DialogUtils.showDlgBaseOneButtonNoTitle(Splash.this, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
									finish();
								}
							}
						}, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_warning_version_fail), "확인"));
					}
				}

				@Override
				public void onFailure(Call<ResponseBody> call, Throwable t) {

				}
			});
		} catch(Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "initData() : " + e.toString());
			return false;
		}

		return true;
	}

	private void setServerAddress(CdnAppInfoData data) {
		ServiceGenerator.setAddress(data);
	}

	/** 앱 실행 초기화**/
	private void initApp(){

		mCheckEvent = true;

		Message mMsg = new Message();
		mMsg.what = APP.APP_STEP_CHECK_RUN;
		mLoopHandler.sendMessageDelayed(mMsg, APP.APP_STEP_CHECK_DEFAULT_TIME);
	}

	private void removeEmptyFinancialData() {
		Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
			@Override
			public void acquired(String str) {
				ArrayList<BankData> filter = new ArrayList<BankData>();
				List<BankData> list = ScrapDao.getInstance().getBankData(Splash.this, str, Session.getInstance(getApplicationContext()).getUserId());
				if (list != null && list.size() > 0) {
					for (BankData data : list) {
						if (data != null) {
							String pwd = data.getPassWord();
							if (pwd == null || TextUtils.isEmpty(pwd)) {
								filter.add(data);
							}
						}
					}
				}
				if (filter.size() > 0) {
					for (BankData data : filter) {
						ScrapDao.getInstance().deleteBankData(Splash.this, str, Session.getInstance(getApplicationContext()).getUserId(), data);
					}
				}
			}

			@Override
			public void error(ErrorMessage errorMessage) {

			}

			@Override
			public void fail() {

			}
		});
	}

	private void showLockScreen() {
		Intent intent = new Intent(getApplicationContext(),
				LockScreenActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		this.startActivityForResult(intent, RESULT_LOCK_SCREEN);
	}

	/**
	 * 하나은행과 외환은행이 합쳐지므로 인해 DB에 있는 이름을 수정할 필요가 있어서 작업
	 * 최신버전 부터는 할 필요 없어서 지움
	 */
	private void migrationBank() {
		Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {

			@Override
			public void acquired(String str) {
				String keb = Splash.this.getString(R.string.bankname_delete_keb);
				String hana = Splash.this.getString(R.string.bankname_old_hana);
				List<BankData> list = ScrapDao.getInstance().getBankData(Splash.this, str, Session.getInstance(getApplicationContext()).getUserId());
				for (BankData bankData : list) {
					if (keb.equals(bankData.getBankName())) {
						ScrapDao.getInstance().deleteBankData(Splash.this, str, Session.getInstance(getApplicationContext()).getUserId(), keb);
						break;
					}
				}
				for (BankData bankData : list) {
					if (hana.equals(bankData.getBankName())) {
						// bank name is primary key
						BankData data = ScrapDao.getInstance().getBankDataByName(Splash.this, str, hana, Session.getInstance(getApplicationContext()).getUserId());
						data.setBankName(Splash.this.getString(R.string.bankname_new_hana));
						ScrapDao.getInstance().deleteBankData(Splash.this, str, Session.getInstance(getApplicationContext()).getUserId(), hana);
						ScrapDao.getInstance().insertBankData(Splash.this, str, Session.getInstance(getApplicationContext()).getUserId(), data);
						break;
					}
				}
			}

			@Override
			public void error(ErrorMessage errorMessage) {

			}

			@Override
			public void fail() {

			}
		});
	}

	private void checkOldMembership() {
		final String id = DbManager.getInstance().getUserId(this);
		Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
			@Override
			public void acquired(String str) {
				LoginData data = DbManager.getInstance().getUserInfoDataSet(Splash.this, str, id);
				String lastLogin = data.getLastLogin();
				if (lastLogin == null || TextUtils.isEmpty(lastLogin)) {
					checkExistMember(str);
				} else {
					mCheckUpdate = true;
				}
			}

			@Override
			public void error(ErrorMessage errorMessage) {

			}

			@Override
			public void fail() {
				mCheckUpdate = true;
			}
		});
	}

	private void checkValidMembership() {
		final String id = DbManager.getInstance().getErrorUserId(this, LoginData.PIN_ERROR);
		if (id == null) {
			mCheckInit = true;
		} else {
			Session.getInstance(this).logout(id);
			mCheckInit = true;
		}
	}

	/** 액티비티 이동 **/
    private void moveNextActivity(){
    	if (isFinishing()) return;
		if (ISTEST && !Session.getInstance(this).isAutoLoginAvailable()) {
			LoginData data = new LoginData();
//			data.setUserId("33c7bd9e4592c98fa403c966d55bf1f0b6e37b8be1aba639d9b025ef57d8c45a"); //biz hee
//			data.setPassword("888888");
//			data.setDeviceId("E4885B0D-F7B8-4D77-A10A-C42E2D53D867");
			data.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
			data.setType(1);
			data.setPassword("555555");
			data.setUserId("a827b1383b5155a0b841b020c3c6ad857a76b9ab61d530bbf446fdcf57e100a7"); //dev
			data.setDeviceId("96fcc346-8fe8-33fc-9720-17aeebfffd9d");
			APPSharedPrefer.getInstance(this).setPrefString(APP.SETTING_MIGRATION_VER, "1.2.5");
			Session.getInstance(this).setUserInfo(null, data);
			Session.getInstance(this).autoLogin(new LoginManager.OnLoginResultListener() {
				@Override
				public void onLoginSuccess(Response<Result> result, LoginData login) {
					startApp();
				}

				@Override
				public void onLoginFail(int error, Object obj) {

				}

				@Override
				public void onLogout() {

				}
			});
			return;
		}
		if (Session.getInstance(this).isAutoLoginAvailable()) {
			startAutoLogin();
		} else {
			joinMembership();
		}
    }

	private void checkExistMember(String str) {
		DialogUtils.showLoading(this);
		final OldUserData data = new OldUserData();
		LoginData loginData = DbManager.getInstance().getUserInfoDataSet(this, str, Session.getInstance(getBase()).getUserId());
		data.setUserId(loginData.getUserId());
		data.setPassword(loginData.getPinNumber());
		Call<Result> call = ServiceGenerator.createService(UserApi.class).oldMemberExist(data);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response != null) {
						Result result = response.body();
						if (result.isOk() || result.checkUnable()) {
							updateOldMember();
						} else {
							Toast.makeText(getBase(), "브로콜리에 회원정보가 존재하지 않습니다. 새로 회원가입을 해주세요.", Toast.LENGTH_LONG);
							Session.getInstance(getBase()).deleteLoginInfo(data.getUserId());
							getBase().startActivity(new Intent(getBase(), LoginActivity.class));
							getBase().finish();
						}
					}
				}
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();
			}
		});
	}

	private void updateOldMember() {
		Intent intent = new Intent(Splash.this, OldMemberUpdateActivity.class);
		startActivity(intent);
		Splash.this.finish();
	}

	private void joinMembership() {
		Handler handler = new Handler();
		handler.post(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(Splash.this, LoginActivity.class);
				startActivity(intent);
				Splash.this.finish();
			}
		});
	}


	private void clearAndFreshRestart() {
		DialogUtils.showDlgBaseOneButtonNoTitle(Splash.this, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Session.getInstance(getApplicationContext()).logout();
				Session.getInstance(getApplicationContext()).setLock(false);
				APPSharedPrefer.getInstance(getApplicationContext()).removeAllPreferences();
				joinMembership();
			}
		}, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.dialog_service_upgrade), "확인"));
	}

	private void startAutoLogin() {
		Session.getInstance(this).autoLogin(new LoginManager.OnLoginResultListener() {

			@Override
			public void onLoginSuccess(Response<Result> result, LoginData login) {
				startApp();
			}

			@Override
			public void onLoginFail(final int error, Object obj) {
				String content = obj.toString();
				if (error == LoginManager.LOGIN_DISABLE_DESTRUCT) {
					Session.getInstance(getApplicationContext()).logout();
					Session.getInstance(getApplicationContext()).setLock(false);
					APPSharedPrefer.getInstance(getApplicationContext()).removeAllPreferences();
					DialogUtils.showDlgBaseOneButtonNoTitle(Splash.this, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
								joinMembership();
							}
						}
					}, new InfoDataDialog(R.layout.dialog_inc_case_1, content, "확인"));
				} else if (error == LoginManager.LOGIN_DISABLE_SESSION_ERROR) {
					Session.getInstance(getApplicationContext()).logout();
					DialogUtils.showDlgBaseOneButtonNoTitle(Splash.this, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
								joinMembership();
							}
						}
					}, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.membership_text_login_fail_disable_device), "확인"));
				} else {
					Session.getInstance(getApplicationContext()).logout();
					DialogUtils.showDlgBaseOneButtonNoTitle(Splash.this, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
								joinMembership();
							}
						}
					}, new InfoDataDialog(R.layout.dialog_inc_case_1, content, "확인"));
				}
			}

			@Override
			public void onLogout() {

			}
		});
	}

	private void startApp() {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(Splash.this, LoadingActivity.class).setFlags(
						Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(intent);
				Splash.this.finish();
			}
		}, 500);
	}

	private void showSetting() {
		Session.getInstance(this).setLockTemporaryDisable(true);
		Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
		startActivityForResult(intent, RESULT_WIFI_SETTING);
	}


//=========================================================================================//
// Private method - Batch Service
//=========================================================================================//
	/** Binder Service Start **/
	private void initBinderService(){
		mServiceConnection = new ServiceConnection() {

			@Override
			public void onServiceDisconnected(ComponentName name) {
				mService = null;
				mIsConnection = false;
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
//				mService = ((CustomBinder) service).getService();
				mService = new Messenger(service);
				mIsConnection = true;
			}
		};

		if(ElseUtils.isServiceRunning(this, BatchService.class.getName()) == false) {
            ComponentName compName = new ComponentName(getPackageName(), BatchService.class.getName());
            startService(new Intent().setComponent(compName));
        }
	}

	/** Binder Service End **/
	private void finishBinderService(){
		if (mIsConnection) {
			unbindService(mServiceConnection);
			mIsConnection = false;
		}
	}

	private void gcmFilterAdd() {
		LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
				new IntentFilter(IntentDef.REGISTRATION_GENERATING));
		LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
				new IntentFilter(IntentDef.REGISTRATION_COMPLETE));
	}

	private void gcmFilterRemove() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
	}

	public void registBroadcastReceiver(){
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();

				if(action.equals(IntentDef.REGISTRATION_GENERATING)){
					// 액션이 GENERATING일 경우
				} else if(action.equals(IntentDef.REGISTRATION_COMPLETE)){
					String token = intent.getStringExtra("token");
					if(token != null && token.trim().length()>0) {
						mCheckGcm = true;
						mUserGcmId = token;
						MainApplication.mAPPShared.setPrefString(APP.SP_USER_GCM, mUserGcmId);
					}
					else{
						CustomToast.getInstance(context).showShort("GCM ID를 받아오지 못했습니다");
					}
				}

			}
		};
	}

	/**
	 * Instance ID를 이용하여 디바이스 토큰을 가져오는 RegistrationIntentService를 실행한다.
	 */
	public void getInstanceIdToken() {
		if (ISTEST) {
			return;
		}
		if (checkPlayServices()) {
			// Start IntentService to register this application with GCM.
			Intent intent = new Intent(this, BroccoliRegistrationIntentService.class);
			startService(intent);
		}
	}

	/**
	 * Google Play Service를 사용할 수 있는 환경이지를 체크한다.
	 */
	private boolean checkPlayServices() {

		GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
		int resultCode = gApi.isGooglePlayServicesAvailable(this);
		if(resultCode != ConnectionResult.SUCCESS) {
			if(gApi.isUserResolvableError(resultCode)){
				gApi.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Toast.makeText(this, "구글플레이 서비스가 설치되어 있지 않습니다.", Toast.LENGTH_LONG).show();
			}
			return false;
		}
		return true;
	}

	private void getAdId() {
		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				AdvertisingIdClient.Info idInfo = null;
				try {
					idInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
				} catch (GooglePlayServicesNotAvailableException e) {
					e.printStackTrace();
				} catch (GooglePlayServicesRepairableException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				String advertId = null;
				try{
					advertId = idInfo.getId();
				}catch (NullPointerException e){
					e.printStackTrace();
				}

				return advertId;
			}

			@Override
			protected void onPostExecute(String advertId) {
				mCheckAdId = true;
				if (advertId != null) {
					APPSharedPrefer.getInstance(getBase()).setPrefString(APP.SP_USER_ADID, advertId);
				}
			}

		};
		task.execute();
	}
}
