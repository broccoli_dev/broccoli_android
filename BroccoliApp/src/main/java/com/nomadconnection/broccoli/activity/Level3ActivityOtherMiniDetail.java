package com.nomadconnection.broccoli.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Etc.BodyOrgInfo;
import com.nomadconnection.broccoli.data.Etc.RequestEtcMoneyDelete;
import com.nomadconnection.broccoli.data.Etc.RequestEtcMoneyEdit;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.InfoDataOtherMini;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level3ActivityOtherMiniDetail extends BaseActivity {

	private static final String TAG = Level3ActivityOtherMiniDetail.class.getSimpleName();

	public static int mCurrentStateIndex = 0;		// default 0: 상세 , 1: 편집

	/* Divider */
	private boolean mIsAuto;

	/* Layout*/
	private Animation mAnimTitle;
	private Animation mAnimSpinnerLayout;
	private Animation mAnimSpinnerText;
	private Animation mAnimSpinnerEditText;
	private Animation mAnimDelete;
	private Button mDelete;

	/* Layout */
	private LinearLayout mAutoInfo;
	private EditText mTitle;
	private LinearLayout mBtn1;
	private TextView mBtn1Title;
	private TextView mBtn1TitleForAuto;
	private String mBtn1TitleId = "";
	private LinearLayout mBtn2;
	private TextView mBtn2Title;
	private TextView mBtn2TitleForAuto;
	private EditText mCost;
	private String mTextWatcherResult = "";
	private LinearLayout mBtn3;
	private TextView mBtn3Title;
	private LinearLayout mBtn4;
	private TextView mBtn4Title;


	/* Get Parcel Data */
	private InfoDataOtherMini mInfoDataOtherMini;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_other_mini_detail);
		sendTracker(AnalyticsName.CalendarDetail);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		onBtnClickNaviMotion(findViewById(R.id.ll_navi_motion_back));
//		super.onBackPressed();
	}




//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try
		{
			switch (_view.getId()) {
				case R.id.tv_navi_motion_title:
					if (mCurrentStateIndex == 1)
						break;

				case R.id.ll_navi_motion_back:
					if (mCurrentStateIndex == 0) { goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_RESULT_CODE_SUCCESS); }
					else if (mCurrentStateIndex == 1) {
						String mStr = "머니 캘린더 수정\n취소하시겠습니까?";
						DialogUtils.showDlgBaseTwoButton(Level3ActivityOtherMiniDetail.this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
//											mCurrentStateIndex = 0; afterMotion("back");
											resetData(); mCurrentStateIndex = 0; afterMotion("back");
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
//						resetData(); mCurrentStateIndex = 0; afterMotion("back");
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentStateIndex == 0) {
						sendTracker(AnalyticsName.CalendarEdit);
						mCurrentStateIndex = 1;
						afterMotion("else");
					} else if (mCurrentStateIndex == 1) {
						if (checkValueIsFail()) return;
						DialogUtils.showLoading(Level3ActivityOtherMiniDetail.this);											// 로딩
						ServiceGenerator.createService(EtcApi.class).editMoney(
								new RequestEtcMoneyEdit(
										Session.getInstance(Level3ActivityOtherMiniDetail.this).getUserId(),
										mInfoDataOtherMini.mTxt7,        //_CalendarId
										mTitle.getText().toString(),                                                        //mInfoDataOtherMini.mTxt2,        // _Title
										mBtn1Title.getText().toString(),                                                                        // _TransactionId(임시 논의후)
//										mBtn1TitleId,                                                                        // _TransactionId
										TransFormatUtils.transRemoveComma(mCost.getText().toString()),                            // mInfoDataOtherMini.mTxt5,		// _Amt
										TransFormatUtils.transDialogRadioTypeNameToCode(mBtn2Title.getText().toString()),        // mInfoDataOtherMini.mTxt3,		// _PayType
										TransFormatUtils.transRemoveDot(mBtn3Title.getText().toString()),                        //mInfoDataOtherMini.mTxt4,        // _PayDate
										TransFormatUtils.transDialogRadioAlarmNameToCode(mBtn4Title.getText().toString())// mInfoDataOtherMini.mTxt9        // _RepeatType
								)
						).enqueue(new Callback<Result>() {
							@Override
							public void onResponse(Call<Result> call, Response<Result> response) {
								DialogUtils.dismissLoading();                                            // 자산 로딩
								if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
									if (response.body() != null) {
										NextStepEdit(response.body());
									} else {
										ElseUtils.network_error(getBase());
									}
								} else {
									ElseUtils.network_error(getBase());
								}
							}

							@Override
							public void onFailure(Call<Result> call, Throwable t) {
								DialogUtils.dismissLoading();                                            // 자산 로딩
								ElseUtils.network_error(Level3ActivityOtherMiniDetail.this);
								//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("CalendarDetail 서버 통신 실패 - 머니캘린더 수정");
							}
						});

//						mCurrentStateIndex = 0; afterMotion("back");
					}

					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}




	private void afterMotion(String _Divider){
		int count = 0;
		if(_Divider.equals("else")){
			count = 0;
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_03_0_0_400_fill);
			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_layout_right);
			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_left);
//			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_left);
			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_down);
		}
		else{	// default
			count = 400;
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_03_0_0_0_400_fill);
			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_layout_left);
			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
//			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_right);
			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_up);
			mAnimDelete.setAnimationListener(myAnimationListener);
		}

		if (mInfoDataOtherMini.mTxt6.equalsIgnoreCase("PY") || mInfoDataOtherMini.mTxt6.equalsIgnoreCase("PN")) {
			mAnimSpinnerText.cancel();
//			mAnimSpinnerEditText.cancel();
			mAnimTitle.cancel();
		} else {
			mBtn1Title.startAnimation(mAnimSpinnerText);		// group 1
			mBtn2Title.startAnimation(mAnimSpinnerText);		// group 1
			mBtn3Title.startAnimation(mAnimSpinnerText);		// group 1
			mBtn4Title.startAnimation(mAnimSpinnerText);		// group 1
//			mTitle.startAnimation(mAnimSpinnerEditText);
//			mCost.startAnimation(mAnimSpinnerEditText);
			((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);
		}

		mDelete.startAnimation(mAnimDelete);
		mDelete.setVisibility(View.VISIBLE);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				DivierLayout();
			}
		}, count);
	}








	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mInfoDataOtherMini = getIntent().getParcelableExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_2);
			if(mInfoDataOtherMini == null)
				bResult = false;

			if(mInfoDataOtherMini.mTxt10.length() == 0){
				mIsAuto = false; // 자동
			}
			else{
				mIsAuto = true; // 수동
			}

		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) ((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			
			/* Layout */
			mDelete = (Button)findViewById(R.id.btn_level_3_activity_othermini_detail_delete);

			mAutoInfo = (LinearLayout)findViewById(R.id.ll_level_3_activity_othermini_detail_auto_info);
			mTitle = (EditText)findViewById(R.id.et_other_mini_inc_title);
			mBtn1 = (LinearLayout)findViewById(R.id.ll_other_mini_inc_btn_1);
			mBtn1Title = (TextView)findViewById(R.id.tv_other_mini_inc_btn_1_title);
			mBtn1TitleForAuto = (TextView)findViewById(R.id.tv_other_mini_inc_btn_1_title_for_auto);
			mBtn2 = (LinearLayout)findViewById(R.id.ll_other_mini_inc_btn_2);
			mBtn2Title = (TextView)findViewById(R.id.tv_other_mini_inc_btn_2_title);
			mBtn2TitleForAuto = (TextView)findViewById(R.id.tv_other_mini_inc_btn_2_title_for_auto);
			mCost = (EditText)findViewById(R.id.et_other_mini_inc_cost);
			mBtn3 = (LinearLayout)findViewById(R.id.ll_other_mini_inc_btn_3);
			mBtn3Title = (TextView)findViewById(R.id.tv_other_mini_inc_btn_3_title);
			mBtn4 = (LinearLayout)findViewById(R.id.ll_other_mini_inc_btn_4);
			mBtn4Title = (TextView)findViewById(R.id.tv_other_mini_inc_btn_4_title);

			mDelete.setOnClickListener(BtnClickListener);
			mBtn1.setOnClickListener(BtnClickListener);
			mBtn2.setOnClickListener(BtnClickListener);
			mBtn3.setOnClickListener(BtnClickListener);
			mBtn4.setOnClickListener(BtnClickListener);

			mTitle.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					complete_button_enable_disable();
				}
			});

			mCost.addTextChangedListener(new TextWatcher() {

				private String strAmount = "";

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
//					if (!s.toString().equals(mTextWatcherResult)) {
//						if (s.toString().length() == 0) mTextWatcherResult = "0";
//						else
//							mTextWatcherResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
//						if(mTextWatcherResult.equals("0")){
//							mCost.setText(mTextWatcherResult);
//						}
//						else{
//							mCost.setText(mTextWatcherResult);
//							mCost.setSelection(mTextWatcherResult.length());
//						}
//					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					String text = s.toString();
					if (!strAmount.equalsIgnoreCase(text)) {
						if (text != null && !TextUtils.isEmpty(text)) {
							strAmount = makeStringComma(text.replaceAll(",", ""));
						} else {
							strAmount = "0";
						}
						mCost.setText(strAmount);
					}
					if (s.length() > 0) {
						Selection.setSelection(s, s.length());
					}
					complete_button_enable_disable();
				}

				protected String makeStringComma(String str) {
					if (str.length() == 0)
						return "";
					long value = Long.parseLong(str);
					DecimalFormat format = new DecimalFormat("###,###");
					return format.format(value);
				}

			});

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCurrentStateIndex = 0;	// 상세화면
			DivierLayout();

			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
			mAnimSpinnerText.setDuration(0);
			mAnimSpinnerText.setFillAfter(true);

			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_right);
			mAnimSpinnerEditText.setDuration(0);
			mAnimSpinnerEditText.setFillAfter(true);

//			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_up);
//			mAnimDelete.setDuration(0);
//			mAnimDelete.setFillAfter(true);


			resetData();
//			mTitle.setText(mInfoDataOtherMini.mTxt2);
//			mTitle.setSelection(mTitle.getText().toString().length());
////			mBtn1Title.setText(mInfoDataOtherMini.mTxt8);
//			mBtn1Title.setText((mInfoDataOtherMini.mTxt8 == null || mInfoDataOtherMini.mTxt8.equals(""))?"없음":mInfoDataOtherMini.mTxt8);
//			mBtn2Title.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mInfoDataOtherMini.mTxt3) - 1)));
//			mCost.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoDataOtherMini.mTxt5));
//			mCost.setSelection(mCost.getText().toString().length());
//			mBtn3Title.setText(TransFormatUtils.transDateForm(mInfoDataOtherMini.mTxt4));
//			mBtn4Title.setText(TransFormatUtils.transDialogRadioAlarmCodeToName(mInfoDataOtherMini.mTxt9));

			mBtn1Title.startAnimation(mAnimSpinnerText);
			mBtn2Title.startAnimation(mAnimSpinnerText);
			mBtn3Title.startAnimation(mAnimSpinnerText);
			mBtn4Title.startAnimation(mAnimSpinnerText);
			mTitle.startAnimation(mAnimSpinnerEditText);
//			mCost.startAnimation(mAnimSpinnerEditText);
//			mDelete.startAnimation(mAnimDelete);
			mDelete.setVisibility(View.GONE);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
//			v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
			mTitle.clearFocus();
			mCost.clearFocus();
			switch (v.getId()) {

				case R.id.btn_level_3_activity_othermini_detail_delete:
					final String mStr = getString(R.string.common_popup_delete);
					DialogUtils.showDlgBaseTwoButton(Level3ActivityOtherMiniDetail.this, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
										mini_delete();
									}
								}
							},
							new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
//					CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("삭제 완료");
//					finish();
					break;

				case R.id.ll_other_mini_inc_btn_1:
					RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
					mRequestDataByUserId.setUserId(Session.getInstance(Level3ActivityOtherMiniDetail.this).getUserId());

					DialogUtils.showLoading(Level3ActivityOtherMiniDetail.this);											// 자산 로딩
					ServiceGenerator.createService(EtcApi.class).getOrgList(mRequestDataByUserId).enqueue(new Callback<ArrayList<BodyOrgInfo>>() {
						@Override
						public void onResponse(Call<ArrayList<BodyOrgInfo>> call, Response<ArrayList<BodyOrgInfo>> response) {
							DialogUtils.dismissLoading();											// 자산 로딩
							final ArrayList<BodyOrgInfo> mTemp;
							if (APP._SAMPLE_MODE_)
								mTemp = AssetSampleTestDataJson.GetSampleData20();
							else mTemp = response.body();

							String mTempBtn1Title ="선택";
							for (int i = 0; i < mTemp.size(); i++) {
								if (mBtn1Title.getText().toString().equalsIgnoreCase(mTemp.get(i).mBodyOrgOpponent)){
									mTempBtn1Title = mBtn1Title.getText().toString();
									break;
								}
							}

							DialogUtils.showDlgBaseGridOrList(Level3ActivityOtherMiniDetail.this,  new AdapterView.OnItemClickListener() {
										@Override
										public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
											if (position == 0) {
												mBtn1TitleId = "0";
												mBtn1Title.setText("없음");
											}
											else {
												mBtn1TitleId = mTemp.get(position).mBodyOrgId;
												mBtn1Title.setText(mTemp.get(position).mBodyOrgOpponent);
											}
											complete_button_enable_disable();
										}
									},
									new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_L, getResources().getString(R.string.dialog_org_choice_title), getResources().getString(R.string.common_cancel), mTemp), 0, mTempBtn1Title, mBtn1TitleId);
//									new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_L, getResources().getString(R.string.dialog_org_choice_title), getResources().getString(R.string.common_cancel), mTemp), 0, mBtn1Title.getText().toString(), mBtn1TitleId);
//									new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_L, getResources().getString(R.string.dialog_org_choice_title), getResources().getString(R.string.common_cancel), mTemp), 0, mBtn1Title.getText().toString(), mBtn1TitleId);
						}

						@Override
						public void onFailure(Call<ArrayList<BodyOrgInfo>> call, Throwable t) {
							DialogUtils.dismissLoading();											// 자산 로딩
							final ArrayList<BodyOrgInfo> mTemp;
							if (APP._SAMPLE_MODE_) {
								mTemp = AssetSampleTestDataJson.GetSampleData20();
								String mTempBtn1Title ="선택";
								for (int i = 0; i < mTemp.size(); i++) {
									if (mBtn1Title.getText().toString().equalsIgnoreCase(mTemp.get(i).mBodyOrgOpponent)){
										mTempBtn1Title = mBtn1Title.getText().toString();
										break;
									}
								}

								DialogUtils.showDlgBaseGridOrList(Level3ActivityOtherMiniDetail.this, new AdapterView.OnItemClickListener() {
											@Override
											public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
												if (position == 0) {
													mBtn1TitleId = "0";
													mBtn1Title.setText("없음");
												}
												else {
													mBtn1TitleId = mTemp.get(position).mBodyOrgId;
													mBtn1Title.setText(mTemp.get(position).mBodyOrgOpponent);
												}
											}
										},
										new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_L, getResources().getString(R.string.dialog_org_choice_title), getResources().getString(R.string.common_cancel), mTemp), 0, mTempBtn1Title, mBtn1TitleId);
//										new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_L, getResources().getString(R.string.dialog_org_choice_title), getResources().getString(R.string.common_cancel), mTemp), 0, mBtn1Title.getText().toString(), mBtn1TitleId);
							} else
								ElseUtils.network_error(Level3ActivityOtherMiniDetail.this);
								//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("CalendarDetail 서버 통신 실패 - 머니캘린더 청구기관 목록");

						}
					});



					break;

				case R.id.ll_other_mini_inc_btn_2:
					DialogUtils.showDlgBaseOneButton(Level3ActivityOtherMiniDetail.this, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									mBtn2Title.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(which)));
									complete_button_enable_disable();
								}
							},
							new InfoDataDialog(R.layout.dialog_inc_case_3, getResources().getString(R.string.dialog_pay_type_title), getResources().getString(R.string.common_cancel), mBtn2Title.getText().toString()));
					break;

				case R.id.ll_other_mini_inc_btn_3:
					DatePickerDialog.OnDateSetListener callBack1 = new DatePickerDialog.OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear,
											  int dayOfMonth) {
							String msg = String.format("%04d.%02d.%02d", year, monthOfYear+1, dayOfMonth);
							mBtn3Title.setText(msg);
							complete_button_enable_disable();
							//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort(msg);
						}
					};
					int mYear, mMonth, mDay = 0;
					String mTempNow = TransFormatUtils.getDataFormatWhich(1);
					Date mTempNowDate = new Date();
					try {
						mTempNowDate = new SimpleDateFormat("yyyy.MM.dd").parse(mTempNow);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					if (mBtn3Title.getText().toString().equals(getString(R.string.common_input_date_hint))){
						mYear = Integer.valueOf(mTempNow.substring(0,4));
						mMonth = Integer.valueOf(mTempNow.substring(5, 7));
						mDay = Integer.valueOf(mTempNow.substring(8,10));
					}
					else{
						mYear = Integer.valueOf(mBtn3Title.getText().toString().substring(0,4));
						mMonth = Integer.valueOf(mBtn3Title.getText().toString().substring(5,7));
						mDay = Integer.valueOf(mBtn3Title.getText().toString().substring(8,10));
					}

//					new DatePickerDialog(Level3ActivityOtherMiniDetail.this, callBack1, mYear, mMonth-1, mDay).show();
					DatePickerDialog pickerDialog = new DatePickerDialog(Level3ActivityOtherMiniDetail.this, AlertDialog.THEME_HOLO_LIGHT,  callBack1, mYear, mMonth-1, mDay);
					Date mTempMaxDate = new Date();
					try {
						mTempMaxDate = new SimpleDateFormat("yyyy.MM.dd").parse(TransFormatUtils.getNextMonthDay());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					pickerDialog.getDatePicker().setMaxDate((mTempMaxDate.getTime()));
					pickerDialog.show();
					break;

				case R.id.ll_other_mini_inc_btn_4:
					DialogUtils.showDlgBaseOneButton(Level3ActivityOtherMiniDetail.this, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									mBtn4Title.setText(TransFormatUtils.transDialogRadioAlarmPosToName(String.valueOf(which)));
									complete_button_enable_disable();
								}
							},
							new InfoDataDialog(R.layout.dialog_inc_case_3, getResources().getString(R.string.dialog_alarm_repeat_title), getResources().getString(R.string.common_cancel), mBtn4Title.getText().toString()));
					break;

				default:
					break;
			}

		}
	};




	private void DivierLayout(){

		if (mIsAuto){
			mAutoInfo.setVisibility(View.VISIBLE);
			mBtn1TitleForAuto.setTextAppearance(Level3ActivityOtherMiniDetail.this, R.style.CS_Text_15_191191191);
			mBtn2TitleForAuto.setTextAppearance(Level3ActivityOtherMiniDetail.this, R.style.CS_Text_15_191191191);
			mBtn1Title.setTextAppearance(Level3ActivityOtherMiniDetail.this, R.style.CS_Text_15_191191191);
			mBtn2Title.setTextAppearance(Level3ActivityOtherMiniDetail.this, R.style.CS_Text_15_191191191);
		}
		else{
			mAutoInfo.setVisibility(View.GONE);
			mBtn1TitleForAuto.setTextAppearance(Level3ActivityOtherMiniDetail.this, R.style.CS_Text_15_898989);
			mBtn2TitleForAuto.setTextAppearance(Level3ActivityOtherMiniDetail.this, R.style.CS_Text_15_898989);
			mBtn1Title.setTextAppearance(Level3ActivityOtherMiniDetail.this, R.style.CS_Text_15_898989);
			mBtn2Title.setTextAppearance(Level3ActivityOtherMiniDetail.this, R.style.CS_Text_15_898989);
		}

		if (mCurrentStateIndex == 0){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText("머니 캘린더 상세");
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("편집");
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			mBtn1.setEnabled(false);
			mBtn2.setEnabled(false);
			mBtn3.setEnabled(false);
			mBtn4.setEnabled(false);
			mTitle.setEnabled(false);
			mCost.setEnabled(false);
		}
		else{
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText("머니 캘린더 편집");
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("저장");
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
			mBtn1.setEnabled(true);
			mBtn2.setEnabled(true);
			mBtn3.setEnabled(true);
			mBtn4.setEnabled(true);
			mTitle.setEnabled(true);
			mCost.setEnabled(true);

			if (mIsAuto){
				mBtn1.setOnClickListener(null);
				mBtn2.setOnClickListener(null);
				mBtn1.setEnabled(false);
				mBtn2.setEnabled(false);
			}
			else{
				mBtn1.setOnClickListener(BtnClickListener);
				mBtn2.setOnClickListener(BtnClickListener);
			}
			if (mInfoDataOtherMini.mTxt6.equalsIgnoreCase("PY") || mInfoDataOtherMini.mTxt6.equalsIgnoreCase("PN")) {
				((TextView)findViewById(R.id.tv_navi_motion_else)).setText("닫기");
				mBtn1.setEnabled(false);
				mBtn2.setEnabled(false);
				mBtn3.setEnabled(false);
				mBtn4.setEnabled(false);
				mTitle.setEnabled(false);
				mCost.setEnabled(false);
			} else {
				mBtn1.setEnabled(true);
				mBtn2.setEnabled(true);
				mBtn3.setEnabled(true);
				mBtn4.setEnabled(true);
				mTitle.setEnabled(true);
				mCost.setEnabled(true);
			}
		}

	}


	private void resetData() {
		mTitle.setText(mInfoDataOtherMini.mTxt2);
		mTitle.setSelection(mTitle.getText().toString().length());
//			mBtn1Title.setText(mInfoDataOtherMini.mTxt8);
		mBtn1Title.setText((mInfoDataOtherMini.mTxt8 == null || mInfoDataOtherMini.mTxt8.equals(""))?"없음":mInfoDataOtherMini.mTxt8);
		mBtn2Title.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(Integer.valueOf(mInfoDataOtherMini.mTxt3) - 1)));
		mCost.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoDataOtherMini.mTxt5));
		mCost.setSelection(mCost.getText().toString().length());
		mBtn3Title.setText(TransFormatUtils.transDateForm(mInfoDataOtherMini.mTxt4));
		mBtn4Title.setText(TransFormatUtils.transDialogRadioAlarmCodeToName(mInfoDataOtherMini.mTxt9));
		complete_button_enable_disable();
	}




//===============================================================================//
// Value Check
//===============================================================================//
	/**  Value check **/
	private Boolean checkValueIsFail(){
		boolean mResult = false;

		if (mTitle.getText().toString().trim().length() == 0){
			Toast.makeText(Level3ActivityOtherMiniDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("제목을 입력하세요");
			mResult = true;
		}
		else if (mBtn1Title.getText().toString().equals("선택") || mBtn1Title.getText().toString().trim().length() == 0){
			Toast.makeText(Level3ActivityOtherMiniDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("청구기관을 선택하세요");
			mResult = true;
		}
		else if (mBtn2Title.getText().toString().trim().length() == 0){
			Toast.makeText(Level3ActivityOtherMiniDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("납부유형을 선택하세요");
			mResult = true;
		}
		else if (mCost.getText().toString().trim().length() == 0){
			Toast.makeText(Level3ActivityOtherMiniDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("금액을 입력하세요");
			mResult = true;
		}
		else if (mBtn3Title.getText().toString().equals("선택")){
			Toast.makeText(Level3ActivityOtherMiniDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("청구일자를 선택하세요");
			mResult = true;
		}
		else if (mBtn4Title.getText().toString().trim().length() == 0){
			Toast.makeText(Level3ActivityOtherMiniDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("반복을 선택하세요");
			mResult = true;
		}

		return  mResult;
	}


//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep Edit **/
	private void NextStepEdit(Result _Result){
		if (_Result.getCode().equals("100")){
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("머니캘린더 수정 완료");
			mCurrentStateIndex = 0; afterMotion("back");
		}
		else{
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("머니캘린더 수정 실패");
		}
	}

	/**  NextStep Delete **/
	private void NextStepDelete(Result _Result){
		if (_Result.getCode().equals("100")){
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("머니캘린더 삭제 완료");
			goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_RESULT_CODE_SUCCESS);
		}
		else{
			//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("머니캘린더 삭제 실패");
		}
	}

	private void mini_delete(){
		DialogUtils.showLoading(Level3ActivityOtherMiniDetail.this);											// 자산 로딩
		ServiceGenerator.createService(EtcApi.class).deleteMoney(
				new RequestEtcMoneyDelete(Session.getInstance(Level3ActivityOtherMiniDetail.this).getUserId(), mInfoDataOtherMini.mTxt7)
		).enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						NextStepDelete(response.body());
					} else {
						ElseUtils.network_error(getBase());
					}
				} else {
					ElseUtils.network_error(getBase());
				}
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ElseUtils.network_error(Level3ActivityOtherMiniDetail.this);
				//CustomToast.getInstance(Level3ActivityOtherMiniDetail.this).showShort("CalendarDetail 서버 통신 실패 - 머니캘린더 삭제");
			}
		});
	}

//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int _resultCod){
		Intent intent = getIntent();
		setResult(_resultCod, intent);
		finish();
	}

	Animation.AnimationListener myAnimationListener = new Animation.AnimationListener() {
		public void onAnimationEnd(Animation animation) {
			mDelete.setVisibility(View.GONE); //애니메 끝나면 사라저버려!
			mDelete.clearAnimation();
		}
		@Override
		public void onAnimationRepeat(Animation animation) {
		}
		@Override
		public void onAnimationStart(Animation animation) {
		}
	};

	private void complete_button_enable_disable(){
		if(empty_check()){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}


	private boolean empty_check(){
		if(mTitle.getText().toString().trim().length() == 0 || mBtn1Title.getText().toString().equals("선택")
				|| mCost.getText().toString().trim().length() == 0 || mCost.getText().toString().trim().equals("0") || mBtn3Title.getText().toString().equals("선택")){
			return false;
		}
		return true;
	}

}
