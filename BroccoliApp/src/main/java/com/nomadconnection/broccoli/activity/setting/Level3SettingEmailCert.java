package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.api.ServiceGenerator.createServiceDayli;

/**
 * Created by YelloHyunminJang on 2017. 1. 4..
 */

public class Level3SettingEmailCert extends BaseActivity implements View.OnClickListener {

    private EditText mEmailInput;
    private TextView mEmailResult;
    private Button mBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sendTracker(AnalyticsName.SettingRegEmailSend);
        setContentView(R.layout.level_3_activity_setting_email_cert);
        init();
    }

    private void init() {
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.setting_user_account);
        mEmailInput = (EditText) findViewById(R.id.et_activity_setting_email_cert);
        mEmailResult = (TextView) findViewById(R.id.tv_activity_setting_email_result);
        mBtn = (Button) findViewById(R.id.btn_confirm);
        mBtn.setOnClickListener(this);
        mEmailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (ElseUtils.validateEmail(s.toString())) {
                    mBtn.setEnabled(true);
                    mEmailResult.setText(null);
                    mEmailResult.setCompoundDrawablesWithIntrinsicBounds(0, 0,0,0);
                    mEmailResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                } else {
                    mBtn.setEnabled(false);
                    mEmailResult.setText("유효하지 않은 이메일 형식입니다.");
                    mEmailResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                    mEmailResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                }
            }
        });
        UserPersonalInfo info = Session.getInstance(this).getUserPersonalInfo();
        if (info != null && info.user != null) {
            mEmailInput.setText(info.user.email);
        }
    }

    private void checkEmail() {
        mBtn.setEnabled(false);
        DialogUtils.showLoading(this);
        final String email = mEmailInput.getText().toString();

        Call<Result> call = createServiceDayli(DayliApi.class).dayliIdCheck(email);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    mEmailResult.setText(null);
                    mEmailResult.setCompoundDrawablesWithIntrinsicBounds(0, 0,0,0);
                    mEmailResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    sendCertEmail(email);
                } else {
                    ErrorMessage message = ErrorUtils.parseError(response);
                    if (ErrorMessage.ERROR_714_EXIST_EMAIL == message) {
                        if (email.equalsIgnoreCase(Session.getInstance(Level3SettingEmailCert.this).getUserPersonalInfo().user.email)) {
                            sendCertEmail(email);
                        } else {
                            mEmailResult.setText("이미 사용하고 있는 이메일입니다.");
                            mEmailResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                            mEmailResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        }
                    } else if (ErrorMessage.ERROR_719_QUITE_USER == message) {
                        mEmailResult.setText("사용할 수 없는 이메일입니다.");
                        mEmailResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                        mEmailResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    } else {
                        ElseUtils.network_error(getBase());
                    }
                    mBtn.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getBase());
                mBtn.setEnabled(true);
            }
        });
    }

    private void sendCertEmail(final String email) {
        DialogUtils.showLoading(this);
        Session.getInstance(this).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                LoginData data = DbManager.getInstance().getUserInfoDataSet(getBase(), str, Session.getInstance(getBase()).getUserId());
                HashMap<String, Object> body = new HashMap<String, Object>();
                body.put("userId", data.getDayliId());
                body.put("emailCertTo", email);
                Call<Result> call = ServiceGenerator.createServiceDayli(DayliApi.class).dayliEmailCert(data.getDayliToken(), body);
                call.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                            showCertEmailSendComplete(email);
                        } else {
                            Toast.makeText(Level3SettingEmailCert.this, "인증 메일 발송에 실패하였습니다. 잠시 후 재시도 해주세요.", Toast.LENGTH_LONG).show();
                            mBtn.setEnabled(true);
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {
                        DialogUtils.dismissLoading();
                        ElseUtils.network_error(getBase());
                        mBtn.setEnabled(true);
                    }
                });
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showLoginActivity();
                        break;
                }
            }

            @Override
            public void fail() {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getBase());
                mBtn.setEnabled(true);
            }
        });
    }

    private void showCertEmailSendComplete(String email) {
        Intent intent = new Intent(this, Level4ActivitySettingEmailSendComplete.class);
        intent.putExtra(Level4ActivitySettingEmailSendComplete.EMAIL, email);
        startActivity(intent);
        DialogUtils.dismissLoading();
        finish();
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_confirm:
                checkEmail();
                break;
        }
    }
}
