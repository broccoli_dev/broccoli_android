package com.nomadconnection.broccoli.activity.remit;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.data.Remit.ReceiveMoneyData;
import com.nomadconnection.broccoli.data.Remit.RequestRemitReceivedMoney;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitReceived;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitStandByReceivedMoney;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 16. 7. 15..
 */
public class RemitReceiveMoneyActivity extends BaseActivity implements View.OnClickListener {

    private RemitApi mApi;
    private ResponseRemitStandByReceivedMoney mReceiveMoneyData;

    private String title;
    private String mMessage;
    private TextView mTilte;
    private TextView mContent;
    private Button mButton;
    private ListView mListView;
    private View mListParent;
    private List<ReceiveMoneyData> mList;
    private List<ReceiveData> mAdapterList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        sendTracker(AnalyticsName.AlertDialogActivity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                // 키잠금 해제하기
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                // 화면 켜기
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_FULLSCREEN);
        WindowManager.LayoutParams winLayoutParam = new WindowManager.LayoutParams();
        winLayoutParam.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        winLayoutParam.dimAmount = 0.5f;
        getWindow().setAttributes(winLayoutParam);
        setContentView(R.layout.activity_remit_receive_dialog);

        init();
    }

    private void init() {
        mApi = ServiceGenerator.createRemitService(RemitApi.class);
        initWidget();
        getReceivedMoneyData();
    }

    private void initWidget() {
        findViewById(R.id.ll_activity_alert_dialog_title_layout).setVisibility(View.GONE);
        mTilte = (TextView)findViewById(R.id.tv_activity_alert_dialog_title);
        mContent =(TextView)findViewById(R.id.tv_activity_alert_dialog_content);
        mButton = (Button)findViewById(R.id.btn_activity_alert_dialog_click);
        mListView = (ListView)findViewById(R.id.lv_activity_alert_dialog_list);
        mListParent = (View)findViewById(R.id.rl_activity_alert_dialog_list);
        mButton.setOnClickListener(this);
    }

    private void getReceivedMoneyData() {
        RequestDataByUserId id = new RequestDataByUserId();
        id.setUserId(Session.getInstance(getApplicationContext()).getUserId());
        Call<ResponseRemitStandByReceivedMoney> call = mApi.checkReceivedMoney(id);
        call.enqueue(new Callback<ResponseRemitStandByReceivedMoney>() {
            @Override
            public void onResponse(Call<ResponseRemitStandByReceivedMoney> call, Response<ResponseRemitStandByReceivedMoney> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    mReceiveMoneyData = response.body();
                    initData();
                } else {
                    Toast.makeText(getBaseContext(), "실패 : "+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseRemitStandByReceivedMoney> call, Throwable t) {
                Toast.makeText(getBaseContext(), "실패 : "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData() {
        mTilte.setText(title);
        mTilte.setTypeface(Typeface.SANS_SERIF);
        mContent.setTypeface(Typeface.SANS_SERIF);
        mButton.setText(getResources().getString(R.string.common_confirm));
        mButton.setTypeface(Typeface.SANS_SERIF);
        mList = mReceiveMoneyData.getRcvMoneyWaitList();
        if (mList != null) {
            if (mList.size() == 1) {
                mListParent.setVisibility(View.GONE);
                ReceiveMoneyData data = mList.get(0);
                if (data != null) {
                    String name = data.getSenderName();
                    String money = data.getSendMoney();
                    mMessage = String.format(getString(R.string.remit_receive_money_content_single), name, ElseUtils.getStringDecimalFormat(money));
                    mContent.setText(mMessage);
                }
            } else if (mList.size() > 1) {
                mListParent.setVisibility(View.VISIBLE);
                mAdapterList = new ArrayList<>();
                String name = mList.get(0).getSenderName();
                long money = 0;
                int count = 0;
                for (ReceiveMoneyData data : mList) {
                    if (data != null) {
                        String tempName = data.getSenderName();
                        String tempMoney = data.getSendMoney();
                        mAdapterList.add(new ReceiveData(tempName, ElseUtils.getStringDecimalFormat(tempMoney)+"원"));
                        money += Long.valueOf(tempMoney);
                        count++;
                    }
                }
                mMessage = String.format(getString(R.string.remit_receive_money_content_multi), name, String.valueOf(count), ElseUtils.getDecimalFormat(money));
                mContent.setText(mMessage);
                mListView.setAdapter(new AlertListAdapter(getBaseContext(), mAdapterList));
            } else {

            }
        }
    }

    private class ReceiveData {
        private String name;
        private String money;

        public ReceiveData(String name, String money) {
            this.name = name;
            this.money = money;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }
    }

    private class AlertListAdapter extends BaseAdapter {


        private List<ReceiveData> mDataSet;
        private LayoutInflater inflater;
        private Context mContext;

        private AlertListAdapter(Context context, List<ReceiveData> data) {
            mDataSet = data;
            mContext = context;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            int count = 0;
            if (mDataSet != null) {
                count = mDataSet.size();
            }
            return count;
        }

        @Override
        public Object getItem(int position) {
            Object obj = null;
            if (mDataSet != null) {
                obj = mDataSet.get(position);
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.row_item_remit_received, null);
                holder = new ViewHolder();
                holder.mName = (TextView) convertView.findViewById(R.id.tv_row_item_receive_name);
                holder.mMoney = (TextView) convertView.findViewById(R.id.tv_row_item_receive_money);
                holder.mName.setTypeface(Typeface.SANS_SERIF);
                holder.mMoney.setTypeface(Typeface.SANS_SERIF);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.mName.setText(mDataSet.get(position).getName());
            holder.mMoney.setText(mDataSet.get(position).getMoney());
            return convertView;
        }

        private class ViewHolder {
            private TextView mName;
            private TextView mMoney;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_activity_alert_dialog_click:
                sendReceived();
                break;
        }
    }

    private void sendReceived() {
        if (mList != null) {
            RequestRemitReceivedMoney requestRemitReceivedMoney = new RequestRemitReceivedMoney();
            requestRemitReceivedMoney.setUserId(Session.getInstance(this).getUserId());
            String sendIds = new String();
            for (int i=0; i < mList.size(); i++) {
                ReceiveMoneyData data = mList.get(i);
                sendIds += data.getSendId();
                if (i != mList.size()-1) {
                    sendIds += ",";
                }
            }
            requestRemitReceivedMoney.setSendId(sendIds);
            Call<ResponseRemitReceived> call = mApi.sendReceivedMoney(requestRemitReceivedMoney);
            call.enqueue(new Callback<ResponseRemitReceived>() {
                @Override
                public void onResponse(Call<ResponseRemitReceived> call, Response<ResponseRemitReceived> response) {
                    if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                        ResponseRemitReceived body = response.body();
                        String result = body.getResult();
                        if (Const.OK.equalsIgnoreCase(result)) {
                            Toast.makeText(getBaseContext(), body.getSuccessCount()+"건의 송금을 수신완료하였습니다.", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } else {
                        Toast.makeText(getBaseContext(), "실패 : "+response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseRemitReceived> call, Throwable t) {
                    Toast.makeText(getBaseContext(), "실패 : "+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
