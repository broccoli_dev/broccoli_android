package com.nomadconnection.broccoli.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockInfo;
import com.nomadconnection.broccoli.data.Investment.InvestNewsyStockData;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;


public class Level4ActivityAssetStockNewsy extends BaseActivity implements View.OnClickListener {

	private static final String TAG = Level4ActivityAssetStockNewsy.class.getSimpleName();

	/* Get Parcel Data */
	private InvestNewsyStockData mNewsyStockData;
	private ArrayList<View> mDetailViewList = new ArrayList<>();
	private BodyAssetStockInfo mInfoDataHeader;
	private TextView mSubTitleInfo1;
	private TextView mSubTitleInfo2;
	private ImageView mSubTitleInfo3;
	private TextView mSubTitleInfo4;

	private TextView mShowDetail;
	private ImageView mTotalImage;
	private TextView mTotalText;
	private ImageView mMomentumImage;
	private TextView mMomentumText;
	private ImageView mFundamentalImage;
	private TextView mFundamentalText;
	private ImageView mLongTermImage;
	private TextView mLongTermText;
	private ImageView mEnvelopeImage;
	private TextView mEnvelopeText;
	private TextView mTotalChange;
	private TextView mCurrentBeta;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_4_activity_asset_stock_newsy);
		sendTracker(AnalyticsName.InvestNewsyMain);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onBackPressed() {
		onBtnClickNaviMotion(findViewById(R.id.ll_navi_motion_back));
	}





//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try {
			switch (_view.getId()) {

				case R.id.ll_navi_motion_back:
					finish();
					break;
				default:
					break;
			}
		} catch(Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}

	private void showNewsyStockApp() {
		final String appPackageName = "com.newsystock.newsystock"; // getPackageName() from Context or Activity object
		try {
			Intent intent = getPackageManager().getLaunchIntentForPackage(appPackageName);
			if (intent != null) {
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			} else {
				try {
					this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
					this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
				}
			}
		} catch (ActivityNotFoundException e) {
			try {
				this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
			} catch (android.content.ActivityNotFoundException anfe) {
				this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
			}
		}
	}


//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			/* getExtra */
			mInfoDataHeader = getIntent().getParcelableExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER);
			mNewsyStockData = mInfoDataHeader.mBodyNewsy;
		} catch (Exception e) {
			bResult = false;
			e.printStackTrace();
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try {
			mDetailViewList.clear();
			/* Title */
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.asset_stock_newsy_title);

			findViewById(R.id.tv_navi_motion_else).setVisibility(View.GONE);
			findViewById(R.id.iv_navi_motion_back_icon).setVisibility(View.VISIBLE);
			findViewById(R.id.tv_navi_motion_back_txt).setVisibility(View.GONE);
			if (getIntent().hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
				findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_3_layout_bg));
				findViewById(R.id.fl_asset_stock_newsy_subtitle).setBackgroundColor(getResources().getColor(R.color.main_3_layout_bg));
			} else {
				findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_1_layout_bg));
				findViewById(R.id.fl_asset_stock_newsy_subtitle).setBackgroundColor(getResources().getColor(R.color.main_1_layout_bg));
			}

			findViewById(R.id.fl_activity_asset_newsy_more_btn).setOnClickListener(this);

			mSubTitleInfo1 = (TextView) findViewById(R.id.tv_flexible_asset_stock_newsy_subtitle_info_1);
			mSubTitleInfo2 = (TextView) findViewById(R.id.tv_flexible_asset_stock_newsy_subtitle_info_2);
			mSubTitleInfo3 = (ImageView) findViewById(R.id.iv_flexible_asset_stock_newsy_subtitle_info_3);
			mSubTitleInfo4 = (TextView) findViewById(R.id.tv_flexible_asset_stock_newsy_subtitle_info_4);

			mShowDetail = (TextView) findViewById(R.id.tv_activity_asset_newsy_show_detail);
			mShowDetail.setOnClickListener(this);
			mTotalImage = (ImageView) findViewById(R.id.iv_activity_asset_newsy_total_score_image);
			mTotalText = (TextView) findViewById(R.id.tv_activity_asset_newsy_total_score_number);
			mMomentumImage = (ImageView) findViewById(R.id.iv_activity_asset_newsy_momentum_image);
			mMomentumText = (TextView) findViewById(R.id.tv_activity_asset_newsy_momentum_number);
			mFundamentalImage = (ImageView) findViewById(R.id.iv_activity_asset_newsy_fundamental_image);
			mFundamentalText = (TextView) findViewById(R.id.tv_activity_asset_newsy_fundamental_text);
			mLongTermImage = (ImageView) findViewById(R.id.iv_activity_asset_newsy_long_term_image);
			mLongTermText = (TextView) findViewById(R.id.tv_activity_asset_newsy_long_term_number);
			mEnvelopeImage = (ImageView) findViewById(R.id.iv_activity_asset_newsy_envelope_image);
			mEnvelopeText = (TextView) findViewById(R.id.tv_activity_asset_newsy_envelope_number);
			mTotalChange = (TextView) findViewById(R.id.tv_activity_asset_newsy_total_change);
			mCurrentBeta = (TextView) findViewById(R.id.tv_activity_asset_newsy_total_current_beta);

			View view = findViewById(R.id.ll_activity_asset_newsy_total_score_detail);
			mDetailViewList.add(view);
			view = findViewById(R.id.ll_activity_asset_newsy_momentum_detail);
			mDetailViewList.add(view);
			view = findViewById(R.id.ll_activity_asset_newsy_fundamental_detail);
			mDetailViewList.add(view);
			view = findViewById(R.id.ll_activity_asset_newsy_long_term_detail);
			mDetailViewList.add(view);
			view = findViewById(R.id.ll_activity_asset_newsy_envelope_detail);
			mDetailViewList.add(view);
			view = findViewById(R.id.ll_activity_asset_newsy_total_detail);
			mDetailViewList.add(view);

		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			if (mInfoDataHeader != null) {
				mSubTitleInfo1.setText(mInfoDataHeader.mBodyAssetStockName);
			}
			if (mInfoDataHeader != null) {
				mSubTitleInfo2.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoDataHeader.mBodyNowPrice));
				int mTempNowPrice = Integer.valueOf(mInfoDataHeader.mBodyNowPrice);
				int mTempPrevPrice = Integer.valueOf(mInfoDataHeader.mBodyPrevPrice);
				int mTempProfit = mTempNowPrice - mTempPrevPrice;
				if (mTempProfit > 0) {
					mSubTitleInfo3.setVisibility(View.VISIBLE);
					mSubTitleInfo3.setBackgroundResource(R.drawable.up_icon);
				}
				else if (mTempProfit == 0) {
					mSubTitleInfo3.setVisibility(View.GONE);
					mSubTitleInfo3.setBackgroundResource(0);
				}
				else {
					mSubTitleInfo3.setVisibility(View.VISIBLE);
					mSubTitleInfo3.setBackgroundResource(R.drawable.up_icon);
					mSubTitleInfo3.setRotation(180);
				}
				mSubTitleInfo4.setText(TransFormatUtils.getDecimalFormatRecvString(String.valueOf(Math.abs(mTempProfit))) +"  (" + (mTempProfit > 0 ? "+":"-") + String.format("%.1f", ((float)Math.abs(mTempProfit)/mTempPrevPrice)*100) + "%)");
			}
			if (mNewsyStockData != null) {
				setTotalScore(mNewsyStockData.getTotal());
				setMomentum(mNewsyStockData.getMomentum());
				setFundamental(mNewsyStockData.getFundamental());
				setLongTerm(mNewsyStockData.getLongterm());
				setEnvelope(mNewsyStockData.getEnv());
				setTotalChange(mNewsyStockData.getBeta(), mNewsyStockData.getTotalChange());
			}
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void setTotalScore(String score) {
		int value = Integer.parseInt(score);
		if (value <= 19) {
			mTotalImage.setImageResource(R.drawable.weather_a_01);
		} else if (value <= 39) {
			mTotalImage.setImageResource(R.drawable.weather_a_02);
		} else if (value <= 59) {
			mTotalImage.setImageResource(R.drawable.weather_a_03);
		} else if (value <= 79) {
			mTotalImage.setImageResource(R.drawable.weather_a_04);
		} else {
			mTotalImage.setImageResource(R.drawable.weather_a_05);
		}
		mTotalText.setText(score);
	}

	private void setMomentum(String momentum) {
		int value = Integer.parseInt(momentum);
		if (value <= 19) {
			mMomentumImage.setImageResource(R.drawable.weather_b_01);
		} else if (value <= 39) {
			mMomentumImage.setImageResource(R.drawable.weather_b_02);
		} else if (value <= 59) {
			mMomentumImage.setImageResource(R.drawable.weather_b_03);
		} else if (value <= 79) {
			mMomentumImage.setImageResource(R.drawable.weather_b_04);
		} else {
			mMomentumImage.setImageResource(R.drawable.weather_b_05);
		}
		mMomentumText.setText(momentum);
	}

	private void setFundamental(String fundamental) {
		int value = Integer.parseInt(fundamental);
		if (value <= 19) {
			mFundamentalImage.setImageResource(R.drawable.weather_b_01);
		} else if (value <= 39) {
			mFundamentalImage.setImageResource(R.drawable.weather_b_02);
		} else if (value <= 59) {
			mFundamentalImage.setImageResource(R.drawable.weather_b_03);
		} else if (value <= 79) {
			mFundamentalImage.setImageResource(R.drawable.weather_b_04);
		} else {
			mFundamentalImage.setImageResource(R.drawable.weather_b_05);
		}
		mFundamentalText.setText(fundamental);
	}

	private void setLongTerm(String longTerm) {
		int level = Integer.parseInt(longTerm);
		level = level * 100;
		mLongTermImage.getBackground().setLevel(level);
		mLongTermText.setText(longTerm);
	}

	private void setEnvelope(String envelope) {
		int level = Integer.parseInt(envelope);
		level = level * 100;
		mEnvelopeImage.getBackground().setLevel(level);
		mEnvelopeText.setText(envelope);
	}

	private void setTotalChange(String beta, String change) {
		float value = Float.parseFloat(beta);
		String str = null;
		if (value > 1) {
			str = "주가의 등락폭이 시장의 등락폭보다 큽니다.";
		} else if (value == 1) {
			str = "주가의 등락폭이 시장의 등락폭과 동일합니다.";
		} else if (0 < value && value < 1) {
			str = "주가의 등락폭이 시장의 등락폭보다 작습니다.";
		} else if (value == 0) {
			str = "주가의 등락폭이 시장의 등락폭과 관계가 없습니다.";
		} else if (value < 0) {
			str = "주가의 등락폭이 시장의 등락폭보다 반대로 움직입니다.";
		}
		mTotalChange.setText(str);
		mCurrentBeta.setText(mCurrentBeta.getText()+"{"+beta+"}");
	}

	private boolean isToggleDetail = false;

	private void toggleDetail(boolean bool) {
		if (mDetailViewList != null) {
			int visibility = View.VISIBLE;
			if (!bool) {
				visibility = View.GONE;
			}
			isToggleDetail = bool;
			if (isToggleDetail) {
				mShowDetail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.selector_list_close, 0);
			} else {
				mShowDetail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.selector_list_open, 0);
			}
			for (View view : mDetailViewList) {
				if (view != null) {
					view.setVisibility(visibility);
				}
			}
		}
 	}


	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
			case R.id.tv_activity_asset_newsy_show_detail:
				sendTracker(AnalyticsName.InvestNewsyDetail);
				toggleDetail(!isToggleDetail);
				break;
			case R.id.fl_activity_asset_newsy_more_btn:
				showNewsyStockApp();
				break;
		}
	}
}
