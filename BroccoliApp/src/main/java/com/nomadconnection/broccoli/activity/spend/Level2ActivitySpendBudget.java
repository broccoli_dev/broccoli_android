package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.CommonFragmentPagerAdapter;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.CustomPagerSlidingTabStripSide;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetData;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.fragment.FragmentSpendBudget;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.reflect.Type;
import java.text.Collator;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.fragment.FragmentSpendBudget.TARGET_POSITION;

/**
 * Created by YelloHyunminJang on 2017. 2. 28..
 */

public class Level2ActivitySpendBudget extends BaseActivity implements InterfaceEditOrDetail {

    private static final int REQUEST_EDIT = 1;

    private final static int MAX_MONTH = 13;

    private TextView mMonthlyBudgetTitle;
    private TextView mMonthlySpendTitle;
    private TextView mMonthlyBudget;
    private TextView mMonthlySpend;
    private LinearLayout mGraphCurrent;
    private LinearLayout mGraphLeft;
    private TextView mCurrentStatus;
    private ViewPager mViewPager;
    private CustomPagerSlidingTabStripSide mTabs;
    private CommonFragmentPagerAdapter mPagerAdapter;

    private ArrayList<String> mTargetMonth;
    private String[] mTempMonthSpaceBefore;
    private String[] mTempMonthSpaceAfter;
    private String mSelectYearMonth;
    private String mSelectMonth;
    private ArrayList<SpendBudgetData> mDataList;
    private SpendBudgetData mTargetData;
    private int mCurrentIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_spend_budget);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.Level2ActivitySpendBudget);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_budget);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setText(R.string.common_edit);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
        mMonthlyBudgetTitle = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_budget_title);
        mMonthlySpendTitle = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_spend_title);
        mMonthlyBudget = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_budget);
        mMonthlySpend = (TextView) findViewById(R.id.tv_spend_budget_lookup_monthly_total_spend);
        mGraphCurrent = (LinearLayout) findViewById(R.id.ll_spend_budget_current_graph_amount);
        mGraphLeft = (LinearLayout) findViewById(R.id.ll_spend_budget_left_graph_amount);
        mCurrentStatus = (TextView) findViewById(R.id.tv_spend_budget_current_status);
        mViewPager = (ViewPager) findViewById(R.id.viewpager_spend_budget_lookup_body);

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        set_after_space();	//month space add
        final ArrayList<String> pagerTitle = new ArrayList<>();
        pagerTitle.add(dateFormat.format(calendar.getTime()));
        mTargetMonth = new ArrayList<>();
        mTargetMonth.add(ElseUtils.getYearMonth(calendar.getTime()));
        for (int i=1; i < MAX_MONTH; i++) {
            calendar.add(Calendar.MONTH, -1);
            pagerTitle.add(dateFormat.format(calendar.getTime()));
            mTargetMonth.add(ElseUtils.getYearMonth(calendar.getTime()));
        }
        set_before_space(calendar);	//month space add
        Collections.reverse(pagerTitle);
        Collections.reverse(mTargetMonth);

        mPagerAdapter = new CommonFragmentPagerAdapter(getSupportFragmentManager(), new CommonFragmentPagerAdapter.FragmentPagerItemCreateListener() {
            @Override
            public Fragment getItem(List<?> list, int position) {
                BaseFragment fragment = new FragmentSpendBudget();
                Bundle bundle = new Bundle();
                bundle.putSerializable(Const.DATA, mDataList);
                bundle.putInt(TARGET_POSITION, position);
                bundle.putString(Const.BUDGET_TARGET_MONTH, mTargetMonth.get(position));
                fragment.setArguments(bundle);
                return fragment;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return pagerTitle.get(position);
            }
        });
        mViewPager = (ViewPager) findViewById(R.id.viewpager_spend_budget_lookup_body);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(pagerTitle.size());

        mTabs = (CustomPagerSlidingTabStripSide) findViewById(R.id.pager_spend_budget_lookup_tabs);
        mTabs.setSpace(APP.CUSTOM_PAGE_SLIDING_TAB_STRIP_SIDE_TYPE_ELSE, 2, mTempMonthSpaceBefore, mTempMonthSpaceAfter);
        mTabs.setShouldExpand(true);
        mTabs.setViewPager(mViewPager);
        mTabs.setOnPageChangeListener(mPageChangeListener);
    }

    private void initData() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
        Calendar calendar = Calendar.getInstance();
        mSelectYearMonth = dateFormat.format(calendar.getTime());
        mSelectMonth = mSelectYearMonth.substring(4);

        String budget = String.format(getResources().getString(R.string.spend_budget_total_budget_month), mSelectMonth);
        String spend = String.format(getResources().getString(R.string.spend_budget_total_expense_month), mSelectMonth);
        mMonthlyBudgetTitle.setText(budget);
        mMonthlySpendTitle.setText(spend);
        getData();
    }

    private void set_after_space() {
        int monthly;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);

        mTempMonthSpaceAfter = new String[2];
        DecimalFormat decimalFormat = new DecimalFormat("00");

        for(int i = 0; i < mTempMonthSpaceAfter.length; i++){
            calendar.add(Calendar.MONTH, +1);
            monthly = calendar.get(Calendar.MONTH) + 1;
            mTempMonthSpaceAfter[i] = decimalFormat.format(monthly);
        }
    }

    private void set_before_space(Calendar calendar) {
        int monthly;

        mTempMonthSpaceBefore = new String[2];
        DecimalFormat decimalFormat = new DecimalFormat("00");

        for(int i = 1; i >= 0; i--){
            calendar.add(Calendar.MONTH, -1);
            monthly = calendar.get(Calendar.MONTH)+1;
            mTempMonthSpaceBefore[i] = decimalFormat.format(monthly);
        }
    }

    private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            mCurrentIndex = position;
            mSelectYearMonth = mTargetMonth.get(position);
            mSelectMonth = mSelectYearMonth.substring(4);
            setEditable(mSelectYearMonth);
            showTargetMonth();
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    private void getData() {
        DialogUtils.showLoading(getActivity()); 	//소비 로딩 추가

        Call<JsonElement> call = ServiceGenerator.createService(SpendApi.class).getBudgetList();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                JsonObject object = element.getAsJsonObject();
                JsonArray array = object.getAsJsonArray("budgetList");
                if (array != null) {
                    Type listType = new TypeToken<List<SpendBudgetData>>() {}.getType();
                    mDataList = new Gson().fromJson(array, listType);
                    Collections.sort(mDataList, new Comparator<SpendBudgetData>() {
                        @Override
                        public int compare(SpendBudgetData lhs, SpendBudgetData rhs) {
                            return Collator.getInstance().compare(lhs.baseMonth, rhs.baseMonth);
                        }
                    });
                    setData(mDataList);
                } else {
                    ErrorUtils.parseError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading(); 	//소비 로딩 추가
                ElseUtils.network_error(getActivity());
            }
        });
    }

    private void setData(List<SpendBudgetData> data) {
        if (data == null) {
            mCurrentIndex = 0;
        } else if(mCurrentIndex == -1) {
            mCurrentIndex = data.size() - 2;
        }
        mPagerAdapter.setData(data);
        mPagerAdapter.notifyDataSetChanged();
        mTabs.setViewPager(mViewPager);
        showTargetMonth();
        mViewPager.setCurrentItem(mCurrentIndex, false);
        setEditable(mSelectYearMonth);
    }

    private void showTargetMonth() {
        mTargetData = null;
        if (mDataList != null) {
            for (int i=0; i < mDataList.size(); i++) {
                SpendBudgetData budgetData = mDataList.get(i);
                if (budgetData != null) {
                    if (budgetData.baseMonth.equalsIgnoreCase(mSelectYearMonth)) {
                        mCurrentIndex = i;
                        mTargetData = budgetData;
                        break;
                    }
                }
            }

            String budget = String.format(getResources().getString(R.string.spend_budget_total_budget_month), mSelectMonth);
            String spend = String.format(getResources().getString(R.string.spend_budget_total_expense_month), mSelectMonth);
            mMonthlyBudgetTitle.setText(budget);
            mMonthlySpendTitle.setText(spend);
            mMonthlyBudget.setText(ElseUtils.getDecimalFormat(mTargetData.totalSum));
            mMonthlySpend.setText(ElseUtils.getDecimalFormat(mTargetData.totalExpense));
            long currentStatus = mTargetData.totalSum - mTargetData.totalExpense;
            String string = "남음";
            if (currentStatus < 0) {
                string = "초과";
            }
            mCurrentStatus.setText(ElseUtils.getDecimalFormat(Math.abs(currentStatus))+" "+string);

            Animation animation = new AlphaAnimation(0.2f, 1);
            animation.setDuration(200);

            long ratio = (long)( (double)mTargetData.totalExpense / (double)mTargetData.totalSum * 100.0 );
            if (ratio > 999) {
                ratio = 999;
            }

            if (mTargetData.totalExpense == 0) {
                mGraphCurrent.setVisibility(View.GONE);
                LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) mGraphLeft.getLayoutParams();
                amountParams.weight = 1;
                mGraphLeft.setLayoutParams(amountParams);
            } else {
                mGraphCurrent.setVisibility(View.VISIBLE);
                mGraphCurrent.setAnimation(animation);
                float spendValue = 0f;
                if (mTargetData.totalSum != 0) {
                    spendValue = (float) ((double)mTargetData.totalExpense/(double)mTargetData.totalSum);
                } else {
                    spendValue = 1f;
                }
                if (spendValue > 1f) {
                    spendValue = 1f;
                }
                float budgetValue = 1f - spendValue;

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mGraphCurrent.getLayoutParams();
                params.weight = spendValue;
                if (spendValue >= 1f) {
                    params.setMargins(0, 0, 0, 0);
                } else {
                    params.setMargins(0, 0, ElseUtils.convertDp2Px(this, 3), 0);
                }
                mGraphCurrent.setLayoutParams(params);

                LinearLayout.LayoutParams amountParams = (LinearLayout.LayoutParams) mGraphLeft.getLayoutParams();
                amountParams.weight = budgetValue;
                mGraphLeft.setLayoutParams(amountParams);

                Drawable graphLeftBackground = mGraphLeft.getBackground();
                if (graphLeftBackground instanceof ShapeDrawable) {
                    ShapeDrawable shapeDrawable = (ShapeDrawable) graphLeftBackground;
                    shapeDrawable.getPaint().setColor(getResources().getColor(R.color.common_white));
                } else if (graphLeftBackground instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) graphLeftBackground;
                    gradientDrawable.setColor(getResources().getColor(R.color.common_white));
                }

                if(ratio <= 50){
                    Drawable drawable = mGraphCurrent.getBackground();
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage1_color));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage1_color));
                    }
                } else if(ratio <= 100){
                    Drawable drawable = mGraphCurrent.getBackground();
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage2_color));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage2_color));
                    }
                } else {
                    Drawable drawable = mGraphCurrent.getBackground();
                    if (drawable instanceof ShapeDrawable) {
                        ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
                        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage3_color));
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(getResources().getColor(R.color.spend_budget_lookup_graph_bar_stage3_color));
                    }
                }
            }
        }
    }

    private void setEditable(String yearMonth){
        View editBtn = findViewById(R.id.ll_navi_motion_else);
        GregorianCalendar today = new GregorianCalendar() ;
        int currentYear = today.get(today.YEAR);
        int currentMonth = today.get(today.MONTH)+1;

        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(4);
        int targetYear = Integer.valueOf(year);
        int targetMonth = Integer.valueOf(month);

        if (mDataList.get(mCurrentIndex).totalSum != 0) {
            if (currentYear >= targetYear) {
                if (targetMonth >= currentMonth) {
                    editBtn.setEnabled(true);
                    editBtn.setVisibility(View.VISIBLE);
                } else {
                    editBtn.setEnabled(false);
                    editBtn.setVisibility(View.GONE);
                }
            } else {
                editBtn.setVisibility(View.GONE);
                editBtn.setEnabled(false);
            }
        } else {
            editBtn.setEnabled(false);
            editBtn.setVisibility(View.GONE);
        }
    }

    private void editBudget() {
        Intent intent = new Intent(this, Level3ActivitySpendBudgetEdit.class);
        intent.putExtra(Const.BUDGET_TARGET_MONTH, mSelectYearMonth);
        intent.putExtra(Const.DATA, mTargetData);
        startActivityForResult(intent, REQUEST_EDIT);
    }

    public void onBtnClickNaviMotion(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_motion_else:
                editBudget();
                break;
            case R.id.ll_navi_motion_back:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_EDIT) {
            if (resultCode == RESULT_OK) {
                getData();
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            if (intent.hasExtra(Const.BUDGET_TARGET_MONTH)) {
                mSelectYearMonth = intent.getStringExtra(Const.BUDGET_TARGET_MONTH);
            }
        }
        getData();
    }

    @Override
    public void onEdit() {

    }

    @Override
    public void onDetail() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onRefresh() {
        getData();
    }
}
