package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.common.dialog.CommonDialogListType;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2017. 3. 31..
 */

public class DemoSettingActivity extends BaseActivity implements View.OnClickListener {

    private TextView mCurrentTarget;
    private TextView mFinancialText;
    private TextView mAgeBtn;
    private TextView mGenderBtn;
    private View mBtn;
    private int mAgeType = 0;
    private String mGender = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_setting);
        init();
    }

    private void init() {
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.demo_title);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
        mAgeBtn = (TextView) findViewById(R.id.tv_demo_age);
        mAgeBtn.setOnClickListener(this);
        mGenderBtn = (TextView) findViewById(R.id.tv_demo_gender);
        mGenderBtn.setOnClickListener(this);
        mCurrentTarget = (TextView) findViewById(R.id.tv_demo_personal);
        mFinancialText = (TextView) findViewById(R.id.tv_demo_financial_life);
        mBtn = findViewById(R.id.btn_confirm);
        mBtn.setOnClickListener(this);
        mBtn.setEnabled(false);
        mFinancialText.setVisibility(View.GONE);
    }

    private void initData() {
        sendTracker(AnalyticsName.DemoFilter);
    }

    private void showAgeList() {
        final ArrayList<String> ageList = new ArrayList<>();
        for (int i=1; i <= 6; i++) {
            int age = i * 10;
            ageList.add(String.valueOf(age)+"대");
        }
        final CommonDialogListType mAgeDialog= new CommonDialogListType(this, null, new int[]{R.string.common_cancel},  ageList);
        mAgeDialog.setDialogListener(new CommonDialogListType.DialogListTypeListener() {
            @Override
            public void onButtonClick(DialogInterface dialog, int stringResId) {
                mAgeDialog.dismiss();
            }

            @Override
            public void onItemClick(DialogInterface dialog, int itemPosition) {
                mAgeType = (itemPosition +1) * 10;
                mAgeBtn.setText(String.valueOf(mAgeType)+"대");
                showCurrentTarget();
                dialog.dismiss();
            }
        });
        mAgeDialog.show();
    }

    private void showGenderList() {
        final ArrayList<String> genderList = new ArrayList<>();
        for (int i=0; i <= 1; i++) {
            if (i == 0) {
                genderList.add("남자");
            } else {
                genderList.add("여자");
            }
        }
        final CommonDialogListType mAgeDialog= new CommonDialogListType(this, null, new int[]{R.string.common_cancel}, genderList);
        mAgeDialog.setDialogListener(new CommonDialogListType.DialogListTypeListener() {
            @Override
            public void onButtonClick(DialogInterface dialog, int stringResId) {
                mAgeDialog.dismiss();
            }

            @Override
            public void onItemClick(DialogInterface dialog, int itemPosition) {
                switch (itemPosition) {
                    case 0:
                        mGender = "M";
                        break;
                    case 1:
                        mGender = "F";
                        break;
                }
                mGenderBtn.setText((mGender.equalsIgnoreCase("M") ? "남자":"여자"));
                showCurrentTarget();
                dialog.dismiss();
            }
        });
        mAgeDialog.show();
    }

    private void showCurrentTarget() {
        if (mAgeType != 0 && !TextUtils.isEmpty(mGender)) {
            mCurrentTarget.setText(String.valueOf(mAgeType)+"대"+" "+(mGender.equalsIgnoreCase("M") ? "남자":"여자")+" ");
            mFinancialText.setVisibility(View.VISIBLE);
            mBtn.setEnabled(true);
        } else {
            mCurrentTarget.setText(null);
            mFinancialText.setVisibility(View.GONE);
            mBtn.setEnabled(false);
        }
    }

    private void startDemo() {
        Intent intent = new Intent(this, DemoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(Const.DEMO_AGE, mAgeType);
        intent.putExtra(Const.DEMO_GENDER, mGender);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_confirm:
                startDemo();
                break;
            case R.id.tv_demo_age:
                showAgeList();
                break;
            case R.id.tv_demo_gender:
                showGenderList();
                break;
        }
    }
}
