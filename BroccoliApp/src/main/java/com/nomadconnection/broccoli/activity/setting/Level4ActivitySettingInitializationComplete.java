package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.MainActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;

/**
 * Created by YelloHyunminJang on 16. 2. 4..
 */
public class Level4ActivitySettingInitializationComplete extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_setting_broccoli_init_complete);
        sendTracker(AnalyticsName.SettingGuideDropCheck);
        ((RelativeLayout) findViewById(R.id.rl_navi_top_layout)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((LinearLayout) findViewById(R.id.ll_navi_txt_txt_txt_back)).setVisibility(View.GONE);
        ((LinearLayout) findViewById(R.id.ll_navi_txt_txt_txt_else)).setVisibility(View.GONE);
        ((TextView)findViewById(R.id.tv_navi_txt_txt_txt_title)).setText(R.string.setting_member_leave_complete);
        /* Layout */
        Button button = (Button)findViewById(R.id.btn_level_4_activity_setting_init_complete);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_level_4_activity_setting_init_complete:
                member_leave_complete();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        return;
    }

    private void member_leave_complete() {
        Session.getInstance(Level4ActivitySettingInitializationComplete.this).deleteLoginInfo(Session.getInstance(this).getUserId());
        APPSharedPrefer.getInstance(Level4ActivitySettingInitializationComplete.this).removeAllPreferences();
/*
        Intent broadcastIntent = new Intent();
        broadcastIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
        sendBroadcast(broadcastIntent);
*/
        Intent intent = new Intent(Level4ActivitySettingInitializationComplete.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(Const.MEMBER_LEAVE, "member_leave");
        startActivity(intent);
        finish();
    }

}
