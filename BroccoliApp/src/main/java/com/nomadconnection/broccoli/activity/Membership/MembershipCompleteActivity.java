package com.nomadconnection.broccoli.activity.membership;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.LoadingActivity;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancial;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;

/**
 * Created by YelloHyunminJang on 2017. 5. 18..
 */

public class MembershipCompleteActivity extends BaseActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_complete);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.UserRegComplete);
        initWidget();
        initData();
    }

    private void initWidget() {
        findViewById(R.id.btn_skip).setOnClickListener(this);
        findViewById(R.id.btn_confirm).setOnClickListener(this);
    }

    private void initData() {

    }

    private void startFinancialReg() {
        Intent intent = new Intent(this, Level3ActivitySettingFinancial.class);
        intent.putExtra(Const.FROM_MEMBERSHIP, true);
        startActivity(intent);
    }

    private void enterMain() {
        ActivityCompat.finishAffinity(this);
        Intent intent = new Intent(this, LoadingActivity.class);
        intent.putExtra(Const.FROM_MEMBERSHIP, true);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_confirm:
                startFinancialReg();
                break;
            case R.id.btn_skip:
                sendTracker(AnalyticsName.UserBankSkip);
                enterMain();
                break;
        }
    }
}
