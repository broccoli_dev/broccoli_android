package com.nomadconnection.broccoli.activity.spend;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.CustomTabView;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetLastExpense;
import com.nomadconnection.broccoli.data.Spend.SpendMainData;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.annotation.Annotation;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by YelloHyunminJang on 2017. 2. 28..
 */

public class Level2ActivitySpendBudgetSetting extends BaseActivity implements View.OnClickListener {

    public static final String SHOW_LOOKUP_ACTIVITY = "SHOW_LOOKUP_ACTIVITY";

    private static final double BUDGET_UNIT = 10000.0;
    private static final int MAX_MONTH = 2;

    private EditText mTotalAmount;
    private LinearLayout mBudgetType;
    private RadioButton mBudgetRecentBtn;
    private RadioButton mBudgetAverageBtn;
    private RadioButton mBudgetDirectInputBtn;
    private TextView mRecentValue;
    private TextView mRecentAverage;
    private TextView mBudgetResult;
    private TextView mBudgetResultExtend;
    private TabLayout mTabLayout;
    private View mBtn;

    private SpendBudgetLastExpense mData;
    private List<String> mPagerTitle;
    private List<String> targetMonth;
    private InputType mInputType = InputType.DIRECT;
    private boolean isTargetFixed = false;
    private String mTargetYearMonth;

    public enum InputType {
        RECENT,
        AVERAGE,
        DIRECT
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_spend_budget_setting);
        init();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean isShowLookUpActivity = intent.getBooleanExtra(SHOW_LOOKUP_ACTIVITY, false);
        if (isShowLookUpActivity) {
            Intent lookUpIntent = new Intent(this, Level2ActivitySpendBudget.class);
            lookUpIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
            lookUpIntent.putExtra(Const.BUDGET_TARGET_MONTH, mTargetYearMonth);
            startActivity(lookUpIntent);
            finish();
        }
    }

    private void init() {
        if (getIntent().hasExtra(Const.BUDGET_TARGET_MONTH)) {
            isTargetFixed = true;
            mTargetYearMonth = getIntent().getStringExtra(Const.BUDGET_TARGET_MONTH);
        }
        sendTracker(AnalyticsName.SpendBudgetAddGoal);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_budget_setting_title);
        findViewById(R.id.tv_navi_motion_else).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundResource(R.color.main_2_layout_bg);
        mTotalAmount = (EditText) findViewById(R.id.et_spend_budget_total_amount);
        mTotalAmount.clearFocus();
        mBudgetType = (LinearLayout) findViewById(R.id.rg_spend_budget_type);
        mTabLayout = (TabLayout) findViewById(R.id.tab_spend_budget_tabs);
        if (isTargetFixed) {
            findViewById(R.id.rl_spend_budget_setting_tab).setVisibility(View.GONE);
        } else {
            findViewById(R.id.rl_spend_budget_setting_tab).setVisibility(View.VISIBLE);
            mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    CustomTabView customView = (CustomTabView) tab.getCustomView();
                    if (customView != null) {
                        customView.getTabView().setEnabled(true);
                        customView.getTabView().setText(customView.getBackupText()+"월");
                        if (targetMonth != null && targetMonth.size() > tab.getPosition())
                            mTargetYearMonth = targetMonth.get(tab.getPosition());
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    CustomTabView customView = (CustomTabView) tab.getCustomView();
                    if (customView != null) {
                        customView.getTabView().setEnabled(false);
                        customView.getTabView().setText(customView.getBackupText());
                    }
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
        mBudgetResult = (TextView) findViewById(R.id.tv_spend_budget_result);
        mBudgetResultExtend = (TextView) findViewById(R.id.tv_spend_budget_start_text);
        mBudgetRecentBtn = (RadioButton) findViewById(R.id.rb_spend_budget_setting_recent_consumption);
        mBudgetAverageBtn = (RadioButton) findViewById(R.id.rb_spend_budget_setting_average_consumption);
        mBudgetDirectInputBtn = (RadioButton) findViewById(R.id.rb_spend_budget_setting_direct_input);
        mRecentValue = (TextView) findViewById(R.id.tv_spend_budget_setting_recent_value);
        mRecentAverage = (TextView) findViewById(R.id.tv_spend_budget_setting_average_value);
        mBtn = findViewById(R.id.btn_confirm);
        mBudgetRecentBtn.setOnClickListener(this);
        mBudgetAverageBtn.setOnClickListener(this);
        mBudgetDirectInputBtn.setOnClickListener(this);
        mBtn.setOnClickListener(this);
    }

    private void setAfterSpace() {
        int monthly;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);

        String[] after = new String[2];
        DecimalFormat decimalFormat = new DecimalFormat("00");

        for(int i = 0; i < after.length; i++){
            calendar.add(Calendar.MONTH, +1);
            monthly = calendar.get(Calendar.MONTH) + 1;
            after[i] = decimalFormat.format(monthly);
            CustomTabView tabView = new CustomTabView(this);
            tabView.createDisableTabView(after[i]);
            tabView.setDisable(true);
            mTabLayout.addTab(mTabLayout.newTab().setCustomView(tabView));
        }
    }

    private void setBeforeSpace() {
        Calendar calendar =  Calendar.getInstance();
        calendar.add(Calendar.MONTH, -3);
        int monthly;

        DecimalFormat decimalFormat = new DecimalFormat("00");
        String[] before = new String[2];
        for(int i = 0; i < before.length; i++){
            calendar.add(Calendar.MONTH, 1);
            monthly = calendar.get(Calendar.MONTH)+1;
            before[i] = decimalFormat.format(monthly);
            CustomTabView tabView = new CustomTabView(this);
            tabView.createDisableTabView(before[i]);
            tabView.setDisable(true);
            mTabLayout.addTab(mTabLayout.newTab().setCustomView(tabView));
        }
    }


    private void initData() {

        setBeforeSpace();
        DecimalFormat decimalFormat = new DecimalFormat("00");
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH) + 1;
        mPagerTitle = new ArrayList<>();
        mPagerTitle.add(decimalFormat.format(month));
        targetMonth = new ArrayList<>();
        CustomTabView tabView = new CustomTabView(this);
        tabView.createNormalTabView(mPagerTitle.get(0));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(tabView), true);
        if (!isTargetFixed) {
            mTargetYearMonth = ElseUtils.getYearMonth(calendar.getTime());
            targetMonth.add(mTargetYearMonth);
        }
        for (int i = 1; i < MAX_MONTH; i++) {
            calendar.add(Calendar.MONTH, +1);
            month = calendar.get(Calendar.MONTH) + 1;
            mPagerTitle.add(decimalFormat.format(month));
            CustomTabView customTabView = new CustomTabView(this);
            customTabView.createNormalTabView(mPagerTitle.get(1));
            mTabLayout.addTab(mTabLayout.newTab().setCustomView(customTabView));
            targetMonth.add(ElseUtils.getYearMonth(calendar.getTime()));
        }
        setAfterSpace();
        LinearLayout tabStrip = ((LinearLayout)mTabLayout.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            CustomTabView customTabView = (CustomTabView) mTabLayout.getTabAt(i).getCustomView();
            if (customTabView.isDisable()) {
                tabStrip.getChildAt(i).setClickable(false);
                tabStrip.getChildAt(i).setEnabled(false);
            } else {
                tabStrip.getChildAt(i).setClickable(true);
                tabStrip.getChildAt(i).setEnabled(true);
            }
        }
        mBtn.setEnabled(false);
        mTotalAmount.addTextChangedListener(watcher);
        SpendApi api = ServiceGenerator.createService(SpendApi.class);
        Call<SpendBudgetLastExpense> call = api.getBudget();
        call.enqueue(new Callback<SpendBudgetLastExpense>() {
            @Override
            public void onResponse(Call<SpendBudgetLastExpense> call, Response<SpendBudgetLastExpense> response) {
                if(getActivity() == null || getActivity().isFinishing()){
                    return;
                }

                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        mData = response.body();
                        setData(mData);
                    } else {
                        Retrofit retrofit = ServiceGenerator.getRetrofit();
                        Converter<ResponseBody, SpendMainData> converter = retrofit.responseBodyConverter(SpendMainData.class, new Annotation[0]);
                        ErrorMessage message = ErrorUtils.parseError(response);
                        switch (message) {
                            case ERROR_713_NO_SUCH_USER:
                                Toast.makeText(getBase(), "가입되지 않은 사용자입니다. 새로 회원가입해주세요.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_719_QUITE_USER:
                                Toast.makeText(getBase(), "탈퇴 회원은 탈퇴일로부터 5일 이후 재가입 가능합니다.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_1011_CERT_SESSION_EXPIRE:
                                Toast.makeText(getBase(), "인증시간이 만료되었습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_1012_CERT_NOT_FINISH:
                                Toast.makeText(getBase(), "인증에 실패하였습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<SpendBudgetLastExpense> call, Throwable t) {
                ElseUtils.network_error(getBase());
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_confirm:
                selectFavoriteCategory();
                break;
            case R.id.rb_spend_budget_setting_recent_consumption:
                checkRadioButton(InputType.RECENT);
                break;
            case R.id.rb_spend_budget_setting_average_consumption:
                checkRadioButton(InputType.AVERAGE);
                break;
            case R.id.rb_spend_budget_setting_direct_input:
                checkRadioButton(InputType.DIRECT);
                showKeyboard();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onBtnClickNaviMotion(View view) {
        onBackPressed();
    }

    private void setData(SpendBudgetLastExpense data) {
        long amt = Math.round(data.amt/BUDGET_UNIT);
        long average = Math.round(data.averageAmt/BUDGET_UNIT);
        mRecentValue.setText(ElseUtils.getDecimalFormat(amt)+getResources().getString(R.string.spend_card_money_unit));
        mRecentAverage.setText(ElseUtils.getDecimalFormat(average)+getResources().getString(R.string.spend_card_money_unit));
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void checkRadioButton(InputType type) {
        if (mData != null) {
            mBudgetRecentBtn.setChecked(false);
            mBudgetAverageBtn.setChecked(false);
            mBudgetDirectInputBtn.setChecked(false);
            mInputType = type;
            String current = null;
            long target = 0;
            switch (type) {
                case RECENT:
                    mTotalAmount.removeTextChangedListener(watcher);
                    mTotalAmount.setText(ElseUtils.getStringDecimalFormat(String.valueOf(Math.round(mData.amt/BUDGET_UNIT))));
                    mBudgetRecentBtn.setChecked(true);
                    mTotalAmount.addTextChangedListener(watcher);
                    checkButtonState();
                    mBudgetResult.setVisibility(View.GONE);
                    mBudgetResultExtend.setVisibility(View.GONE);
                    break;
                case AVERAGE:
                    mTotalAmount.removeTextChangedListener(watcher);
                    mTotalAmount.setText(ElseUtils.getStringDecimalFormat(String.valueOf(Math.round(mData.averageAmt/BUDGET_UNIT))));
                    mBudgetAverageBtn.setChecked(true);
                    mTotalAmount.addTextChangedListener(watcher);
                    current = mTotalAmount.getText().toString();
                    current = current.replaceAll(",", "");
                    target = mData.amt - (Long.valueOf((current.equalsIgnoreCase("") ? "0":current))*10000);
                    checkButtonState();
                    if (target > 0) {
                        mBudgetResult.setText("지난달 대비 "+ElseUtils.getDecimalFormat(target)+"원 절약");
                    } else {
                        mBudgetResult.setText("지난달 대비 "+ElseUtils.getDecimalFormat(Math.abs(target))+"원 초과");
                    }
                    mBudgetResult.setVisibility(View.VISIBLE);
                    mBudgetResultExtend.setVisibility(View.VISIBLE);
                    break;
                case DIRECT:
                    mBudgetDirectInputBtn.setChecked(true);
                    current = mTotalAmount.getText().toString();
                    current = current.replaceAll(",", "");
                    target = mData.amt - (Long.valueOf((current.equalsIgnoreCase("") ? "0":current))*10000);
                    if (target > 0) {
                        mBudgetResult.setText("지난달 대비 "+ElseUtils.getDecimalFormat(target)+"원 절약");
                    } else {
                        mBudgetResult.setText("지난달 대비 "+ElseUtils.getDecimalFormat(Math.abs(target))+"원 초과");
                    }
                    mBudgetResult.setVisibility(View.VISIBLE);
                    mBudgetResultExtend.setVisibility(View.VISIBLE);
                    checkButtonState();
                    break;
            }
        }
    }

    private void selectFavoriteCategory() {
        String value = mTotalAmount.getText().toString().replaceAll(",", "");
        long fixValue = (long) (Long.valueOf(value) * BUDGET_UNIT);
        value = String.valueOf(fixValue);
        Intent intent = new Intent(this, Level3ActivitySpendBudgetSelectGroupCategory.class);
        intent.putExtra(Const.BUDGET_TARGET_MONTH, mTargetYearMonth);
        intent.putExtra(Const.SPEND_TOTAL_BUDGET, value);
        startActivityForResult(intent, 0);
    }

    private void checkButtonState() {
        if (mTotalAmount.getText().length() > 0) {
            mBtn.setEnabled(true);
        } else {
            mBtn.setEnabled(false);
        }
    }

    private TextWatcher watcher = new TextWatcher() {

        NumberFormat numberFormat = NumberFormat.getInstance(Locale.KOREA);
        String strAmount;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals(strAmount)) {
                strAmount = s.toString();
                strAmount = strAmount.replaceAll("," , "");
                strAmount = setStrDataToComma(strAmount);
                mTotalAmount.removeTextChangedListener(this);
                mTotalAmount.setText(strAmount);
                Selection.setSelection(mTotalAmount.getText(), strAmount.length());
                mTotalAmount.addTextChangedListener(this);
            }
            checkRadioButton(InputType.DIRECT);
            checkButtonState();
        }

        protected String setStrDataToComma(String str) {
            if (str.length() == 0)
                return "";
            long value = Long.parseLong(str);
            DecimalFormat format = new DecimalFormat("###,###");
            return format.format(value);
        }
    };
}
