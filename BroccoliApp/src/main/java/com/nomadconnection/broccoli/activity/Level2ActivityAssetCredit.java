package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.asset.HonestFundWebActivity;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialReg;
import com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendCardTabList;
import com.nomadconnection.broccoli.adapter.AdtExpListViewAssetCredit;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.LogApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.CustomExpandableListView;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Asset.AssetDebtInfo;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCardBill;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendCardBanner;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.adapter.AdtExpListViewAssetCredit.CARD_LOAN;
import static com.nomadconnection.broccoli.adapter.AdtExpListViewAssetCredit.CARD_NORMAL;
import static com.nomadconnection.broccoli.config.APP.INFO_DATA_ASSET_LOAN_DEPTH_1;


public class Level2ActivityAssetCredit extends BaseActivity {

    private static final String TAG = Level2ActivityAssetCredit.class.getSimpleName();

    /* Layout */
    private CustomExpandableListView mListView;
    private AdtExpListViewAssetCredit mAdapter;
    private WrapperExpandableListAdapter mWrapAdapter;

    private ArrayList<GroupData> mGroupList = new ArrayList<>();
    private HashMap<String, ArrayList<ResponseAssetCardBill>> mChildList = new HashMap<>();
    private ArrayList<ResponseAssetCardBill> mAlInfoDataList = new ArrayList<>();
    private ArrayList<AssetDebtInfo> mDebtList;
    private boolean isShowHonest = false;

    public class GroupData {
        public String title;
        public int mType = CARD_NORMAL;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_asset_credit);
        sendTracker(AnalyticsName.Level2ActivityAssetCredit);
        if (parseParam()) {
            if (initWidget()) {
                if (!initData()) {
                    finish();
                }
            } else {
                finish();
            }
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }


//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//

    /**
     * ToolBar click event handler
     **/
    public void onBtnClickNaviImgTxtTxtBar(View _view) {
        try {
            switch (_view.getId()) {

                case R.id.ll_navi_img_txt_txt_back:
                    finish();
                    break;
                case R.id.ll_navi_img_txt_txt_else:
                    Intent setting_financial = new Intent(this, Level3ActivitySettingFinancialReg.class);
                    startActivity(setting_financial);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
        }
    }


    //=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
    private boolean parseParam() {
        boolean bResult = true;
        try {
            isShowHonest = Session.getInstance(this).getAppInfo().honest_fund.display;

            Intent intent = getIntent();
            if (intent.hasExtra(INFO_DATA_ASSET_LOAN_DEPTH_1)) {
                mDebtList = intent.getParcelableArrayListExtra(INFO_DATA_ASSET_LOAN_DEPTH_1);
            }
        } catch (Exception e) {
            bResult = false;
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "parseParam() : " + e.toString());
            return bResult;
        }
        return bResult;
    }


    //=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
    private boolean initWidget() {
        boolean bResult = true;
        try {
            /* Title */
            ((TextView) findViewById(R.id.tv_navi_img_txt_txt_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitle);
            ((TextView) findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_add));
            if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) {
                (findViewById(R.id.rl_navi_img_txt_txt_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
            }
			
			/* Layout */
            mListView = (CustomExpandableListView) findViewById(R.id.lv_level_2_activity_asset_credit_listview);
            mAdapter = new AdtExpListViewAssetCredit(this, mGroupList, mChildList);
            mWrapAdapter = new WrapperExpandableListAdapter(mAdapter);
            mListView.setAdapter(mWrapAdapter);
            mListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView expandableListView, View view, int position, long l) {
                    int viewType = mAdapter.getGroupType(position);
                    switch (viewType) {
                        case CARD_LOAN:
                            showHonestFund();
                            break;
                        case CARD_NORMAL:
                            break;
                    }
                    return true;
                }
            });
            mListView.setOnChildClickListener(ItemClickListener);
            Call<SpendCardBanner> call = ServiceGenerator.createScrapingService(SpendApi.class).getRecommendBanner();
            call.enqueue(new Callback<SpendCardBanner>() {
                @Override
                public void onResponse(Call<SpendCardBanner> call, Response<SpendCardBanner> response) {
                    if (response != null && response.isSuccessful()) {
                        SpendCardBanner banner = response.body();
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            if (banner != null && banner.recommendYn > 0 && banner.diff > 0) {
                                View view = getLayoutInflater().inflate(R.layout.row_list_item_card_banner, null);
                                view.findViewById(R.id.ll_row_spend_card_benefit_layout).setBackgroundColor(getResources().getColor(R.color.main_1_layout_bg));
                                TextView benefit = (TextView) view.findViewById(R.id.tv_row_spend_card_benefit);
                                benefit.setText(ElseUtils.getDecimalFormat(banner.diff));
                                TextView benefitUnit = (TextView) view.findViewById(R.id.tv_row_spend_card_benefit_unit);
                                if (banner.mileageYn > 0) {
                                    benefitUnit.setText(R.string.spend_card_recommend_unit_mileage);
                                } else {
                                    benefitUnit.setText(R.string.spend_card_recommend_unit_won);
                                }
                                view.setTag(banner);
                                view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        sendTracker(AnalyticsName.AssetCreditListCardBanner);
                                        SpendCardBanner banner = (SpendCardBanner) view.getTag();
                                        Intent bannerIntent = new Intent(getActivity(), Level2ActivitySpendCardTabList.class);
                                        bannerIntent.putExtra(Const.DATA, banner);
                                        bannerIntent.putExtra(Level2ActivitySpendCardTabList.REQUEST_SHOW_RECOMMEND_CARD, 0);
                                        startActivity(bannerIntent);
                                    }
                                });
                                mListView.addHeaderView(view, null, true);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<SpendCardBanner> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            bResult = false;
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "initWidget() : " + e.toString());
            return bResult;
        }

        return bResult;
    }


    //=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
    private boolean initData() {
        boolean bResult = true;
        try {
			/* Default Value */
//			refreshData();
        } catch (Exception e) {
            bResult = false;
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "initData() : " + e.toString());
            return bResult;
        }

        return bResult;
    }


//===============================================================================//
// Listener
//===============================================================================//	
    /**
     * Item Click Listener
     **/
    private ExpandableListView.OnChildClickListener ItemClickListener = new ExpandableListView.OnChildClickListener() {

        @Override
        public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {
            NextStep(mAdapter.getChild(groupPosition, childPosition));
            return false;
        }
    };

    private void showDailog() {
        DialogUtils.showDlgBaseOneButtonNoTitle(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainApplication.mAPPShared.setPrefBool(APP.SP_ONLY_ONE_CHECK_ASSET_CARD_DETAIL_ENTER, true);
            }
        }, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_warning_credit_use), getString(R.string.common_confirm)));
    }


//===============================================================================//
// NextStep
//===============================================================================//

    /**
     * NextStep
     **/
    private void NextStep(ResponseAssetCardBill _AssetBankInfo) {
        startActivity(new Intent(Level2ActivityAssetCredit.this, Level3ActivityAssetCreditDetail.class)
                .putExtra(APP.INFO_DATA_ASSET_CREDIT_DEPTH_2_HEADER, _AssetBankInfo)
        );
    }

    private void showHonestFund() {
        sendTracker(AnalyticsName.AssetCreditListLoanBanner);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", Session.getInstance(this).getUserId());
        ServiceGenerator.createService(LogApi.class).sendLogHonestFund(hashMap).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
        Intent intent = new Intent(this, HonestFundWebActivity.class);
        intent.putExtra(Const.DATA, Session.getInstance(this).getAppInfo().honest_fund.url_to);
        startActivity(intent);
    }


    //===============================================================================//
// Refresh
//===============================================================================//
    private void refreshData() {
        DialogUtils.showLoading(Level2ActivityAssetCredit.this);                                            // 자산 로딩
        RequestDataByUserId userId = new RequestDataByUserId();
        userId.setUserId(Session.getInstance(Level2ActivityAssetCredit.this).getUserId());
        Call<JsonElement> call = ServiceGenerator.createService(AssetApi.class).getCardBillList();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();                                            // 자산 로딩
                JsonElement element = response.body();
                if (element != null) {
                    JsonObject object = element.getAsJsonObject();
                    if (object != null) {
                        JsonArray array = object.getAsJsonArray("list");
                        if (array != null) {
                            Type listType = new TypeToken<ArrayList<ResponseAssetCardBill>>() {}.getType();
                            mAlInfoDataList = new Gson().fromJson(array, listType);
                            resetData();
                        }
                    } else {
                        ErrorUtils.parseError(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();                                            // 자산 로딩
                ElseUtils.network_error(Level2ActivityAssetCredit.this);
            }
        });

    }

    private void resetData() {
        mGroupList.clear();
        mChildList.clear();

        ArrayList<String> temp = new ArrayList<>();
        for (ResponseAssetCardBill cardBill : mAlInfoDataList) {
            if (temp.contains(cardBill.payDate) == false) {
                temp.add(cardBill.payDate);

                ArrayList<ResponseAssetCardBill> list = mChildList.get(cardBill.payDate);
                if (list == null) {
                    list = new ArrayList<>();
                    mChildList.put(cardBill.payDate, list);
                }
                list.add(cardBill);
            } else {
                ArrayList<ResponseAssetCardBill> list = mChildList.get(cardBill.payDate);
                list.add(cardBill);
            }
        }

        for (String group : temp) {
            GroupData groupData = new GroupData();
            groupData.title = group;
            mGroupList.add(groupData);
        }

        if (mDebtList != null && isShowHonest) {
            boolean exist = false;
            if (mDebtList.size() > 0) {
                for (AssetDebtInfo info : mDebtList) {
                    if (info.mLoanType == AssetDebtInfo.LOAN_CARD) {
                        exist = true;
                    }
                }
                if (exist) {
                    GroupData data = new GroupData();
                    data.title = "CARD_LOAN";
                    data.mType = CARD_LOAN;
                    mGroupList.add(data);
                    mChildList.put("CARD_LOAN", new ArrayList<ResponseAssetCardBill>());
                }
            }
        }

        mAdapter.setData(mGroupList, mChildList);

        if (mWrapAdapter != null) mWrapAdapter.notifyDataSetChanged();

        if (mAlInfoDataList != null) {
            if (mAlInfoDataList.size() == 0) {
                ((FrameLayout) findViewById(R.id.inc_level_2_activity_asset_credit_empty)).setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
                ((TextView) findViewById(R.id.tv_empty_layout_txt)).setText(getString(R.string.common_empty_credit));
                TextView secondWarning = (TextView) findViewById(R.id.tv_empty_layout_txt_second);
                secondWarning.setText(R.string.common_empty_credit_sub);
                secondWarning.setVisibility(View.VISIBLE);
            } else {
                for (int i = 0; i <mGroupList.size() ; i++) {
                    mListView.expandGroup(i);
                }
                if (!MainApplication.mAPPShared.getPrefBool(APP.SP_ONLY_ONE_CHECK_ASSET_CARD_DETAIL_ENTER, false)) {
                    showDailog();
                }
                ((FrameLayout) findViewById(R.id.inc_level_2_activity_asset_credit_empty)).setVisibility(View.GONE);
            }
        }
    }

}
