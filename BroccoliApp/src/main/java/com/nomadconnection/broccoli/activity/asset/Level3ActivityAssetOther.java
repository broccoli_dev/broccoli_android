package com.nomadconnection.broccoli.activity.asset;

import android.animation.Animator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Asset.AssetEtcInfo;
import com.nomadconnection.broccoli.data.Asset.DeleteAssetCar;
import com.nomadconnection.broccoli.data.Asset.DeleteAssetEstate;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.fragment.FragmentAssetOther0A;
import com.nomadconnection.broccoli.fragment.FragmentAssetOther0B;
import com.nomadconnection.broccoli.fragment.FragmentAssetOtherCustom;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.interf.InterfaceFragmentComplete;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 2. 8..
 */

public class Level3ActivityAssetOther extends BaseActivity implements View.OnClickListener, InterfaceEditOrDetail {

    public static final String OTHER_HOUSE  = "house";
    public static final String OTHER_CAR    = "car";
    public static final String OTHER_CUSTOM = "custom";

    public static final String OTHER_TYPE = "OTHER_TYPE";

    private LinearLayout mContentLayout = null;
    private View mDelete = null;

    private boolean isEditMode = false;
    private AssetEtcInfo mInfo = null;

    private enum AssetType {
        HOUSE,
        CAR,
        CUSTOM
    }

    private AssetType mType = AssetType.HOUSE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_asset_etc_detail);
        init();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_level_3_activity_asset_other_detail_delete:
                String title = "삭제하시겠어요?";
                DialogUtils.showDlgBaseTwoButton(getBase(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
                                    DialogUtils.showLoading(getBase());											// 자산 로딩
                                    if (mType == AssetType.HOUSE){
                                        Call<Result> call = ServiceGenerator.createService(AssetApi.class).deleteEstate(
                                                new DeleteAssetEstate(Session.getInstance(getBase()).getUserId(), mInfo.description)
                                        );
                                        call.enqueue(new Callback<Result>() {
                                            @Override
                                            public void onResponse(Call<Result> call, Response<Result> response) {
                                                DialogUtils.dismissLoading();											// 자산 로딩

                                            }

                                            @Override
                                            public void onFailure(Call<Result> call, Throwable t) {
                                                DialogUtils.dismissLoading();											// 자산 로딩
                                                ElseUtils.network_error(getBase());
                                            }
                                        });
                                    } else if (mType == AssetType.CAR) {
                                        Call<Result> call = ServiceGenerator.createService(AssetApi.class).deleteCar(
                                                new DeleteAssetCar(Session.getInstance(getBase()).getUserId(), mInfo.description)
                                        );
                                        call.enqueue(new Callback<Result>() {
                                            @Override
                                            public void onResponse(Call<Result> call, Response<Result> response) {
                                                DialogUtils.dismissLoading();											// 자산 로딩

                                            }

                                            @Override
                                            public void onFailure(Call<Result> call, Throwable t) {
                                                DialogUtils.dismissLoading();											// 자산 로딩
                                                ElseUtils.network_error(getBase());
                                            }
                                        });
                                    } else {
                                        sendDelete();
                                    }
                                }
                            }
                        },
                        new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", title));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isEditMode) {
            if (!isEditMode) {
                goBackActivityForResult();
            } else if (isEditMode) {
                String str = "기타 재산 수정\n취소하시겠습니까?";
                DialogUtils.showDlgBaseTwoButton(getBase(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
                                    isEditMode = false;
                                    setLayout();
                                    sendReset();
                                }
                            }
                        },
                        new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", str));
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onEdit() {
        complete_button_enable_disable(true);
    }

    @Override
    public void onDetail() {

    }

    @Override
    public void onError() {
        complete_button_enable_disable(false);
    }

    @Override
    public void onRefresh() {

    }

    private void init() {
        parseParam();
        initWidget();
        fragmentReplace("");
        setLayout();
    }

    private void parseParam() {
        Intent intent = getIntent();
        mInfo = intent.getParcelableExtra(Const.DATA);
        String type = intent.getStringExtra(OTHER_TYPE);
        if (type != null) {
            if (type.equalsIgnoreCase(OTHER_HOUSE)) {
                mType = AssetType.HOUSE;
            } else if (type.equalsIgnoreCase(OTHER_CAR)) {
                mType = AssetType.CAR;
            } else {
                mType = AssetType.CUSTOM;
            }
        }
    }

    private void initWidget() {
        mContentLayout = (LinearLayout) findViewById(R.id.ll_asset_other_fragment_area);
        mDelete = findViewById(R.id.btn_level_3_activity_asset_other_detail_delete);

        mDelete.setOnClickListener(this);
        mDelete.setVisibility(View.GONE);
    }


//====================================
    public void onBtnClickNaviMotion(View _view) {
        try {
            switch (_view.getId()) {
                case R.id.ll_navi_motion_back:
                    if (!isEditMode) {
                        goBackActivityForResult();
                    } else if (isEditMode) {
                        String mStr = "기타 재산 수정\n취소하시겠습니까?";
                        DialogUtils.showDlgBaseTwoButton(getBase(), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
                                            isEditMode = false;
                                            setLayout();
                                            sendReset();
                                        }
                                    }
                                },
                                new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
                    }
                    break;

                case R.id.ll_navi_motion_else:
                    if (!isEditMode) {
                        isEditMode = true;
                        setLayout();
                        sendEdit();
                    } else if (isEditMode) {
                        isEditMode = false;
                        setLayout();
                        sendComplete();
                    }
                    break;

                default:
                    break;
            }
        } catch(Exception e) {
            if(APP._DEBUG_MODE_)
                Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
        }
    }


//=========================================================================================//
// Fragment Navi
//=========================================================================================//
    /** Fragment 변경 **/
    private void fragmentReplace(String anim) {
        try {
            Fragment newFragment = null;
            newFragment = getFragment();

            // replace fragment
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (anim.length() != 0 && anim.equals("in_right")) transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out);
            if (anim.length() != 0 && anim.equals("out_right")) transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
            transaction.replace(R.id.ll_asset_other_fragment_area, newFragment);

            // Commit the transaction
            transaction.commitAllowingStateLoss();

        } catch (Exception e) {

        }
    }

    /** Fragment Get **/
    private Fragment getFragment() {
        Fragment newFragment = null;
        Bundle bundle = null;

        switch (mType) {
            case HOUSE:
                bundle = new Bundle();
                newFragment = new FragmentAssetOther0A();
                bundle.putParcelable(Const.DATA, mInfo);
                break;
            case CAR:
                bundle = new Bundle();
                newFragment = new FragmentAssetOther0B();
                bundle.putParcelable(Const.DATA, mInfo);
                break;
            case CUSTOM:
                bundle = new Bundle();
                newFragment = new FragmentAssetOtherCustom();
                bundle.putBoolean(FragmentAssetOtherCustom.MODE_DETAIL, true);
                bundle.putParcelable(Const.DATA, mInfo);
                break;
        }
        newFragment.setArguments(bundle);

        return newFragment;
    }

    private void goBackActivityForResult(){
        Intent intent = getIntent();
        setResult(APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_SUCCESS, intent);
        finish();
    }

    private void setLayout(){

        if (!isEditMode){
            switch (mType) {
                case HOUSE:
                    sendTracker(AnalyticsName.Level3ActivityAssetOtherDetailEditRE);
                    ((TextView)findViewById(R.id.tv_navi_motion_title)).setText("부동산 상세");
                    ((TextView)findViewById(R.id.tv_navi_motion_else)).setText("편집");
                    break;
                case CAR:
                    sendTracker(AnalyticsName.Level3ActivityAssetOtherDetailEditCAR);
                    ((TextView)findViewById(R.id.tv_navi_motion_title)).setText("차량 상세");
                    ((TextView)findViewById(R.id.tv_navi_motion_else)).setText("편집");
                    break;
                case CUSTOM:
                    sendTracker(AnalyticsName.Level3ActivityAssetOtherDetailEdit);
                    ((TextView)findViewById(R.id.tv_navi_motion_title)).setText("기타 상세");
                    ((TextView)findViewById(R.id.tv_navi_motion_else)).setText("편집");
                    break;
            }
            mDelete.animate().translationY(mDelete.getHeight()).setDuration(500).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mDelete.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
            ((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
        }
        else {
            switch (mType) {
                case HOUSE:
                    sendTracker(AnalyticsName.Level3ActivityAssetOtherDetailRE);
                    ((TextView)findViewById(R.id.tv_navi_motion_title)).setText("부동산 편집");
                    ((TextView)findViewById(R.id.tv_navi_motion_else)).setText("저장");
                    break;
                case CAR:
                    sendTracker(AnalyticsName.Level3ActivityAssetOtherDetailCAR);
                    ((TextView)findViewById(R.id.tv_navi_motion_title)).setText("차량 편집");
                    ((TextView)findViewById(R.id.tv_navi_motion_else)).setText("저장");
                    break;
                case CUSTOM:
                    sendTracker(AnalyticsName.Level3ActivityAssetOtherDetail);
                    ((TextView)findViewById(R.id.tv_navi_motion_title)).setText("기타 편집");
                    ((TextView)findViewById(R.id.tv_navi_motion_else)).setText("저장");
                    break;
            }
            mDelete.setVisibility(View.VISIBLE);
            mDelete.animate().translationY(0).setDuration(500).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mDelete.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
            ((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
            ((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
        }
    }

    private void sendComplete() {
        for (Fragment frag : getSupportFragmentManager().getFragments()) {
            if (frag != null) {
                InterfaceFragmentComplete fragment = (InterfaceFragmentComplete) frag;
                if (fragment != null) {
                    fragment.complete();
                }
            }
        }
    }

    private void sendEdit() {
        for (Fragment frag : getSupportFragmentManager().getFragments()) {
            if (frag != null) {
                InterfaceFragmentComplete fragment = (InterfaceFragmentComplete) frag;
                if (fragment != null) {
                    fragment.edit();
                }
            }
        }
    }

    private void sendReset() {
        for (Fragment frag : getSupportFragmentManager().getFragments()) {
            if (frag != null) {
                InterfaceFragmentComplete fragment = (InterfaceFragmentComplete) frag;
                if (fragment != null) {
                    fragment.reset();
                }
            }
        }
    }

    private void sendDelete() {
        for (Fragment frag : getSupportFragmentManager().getFragments()) {
            if (frag != null) {
                InterfaceFragmentComplete fragment = (InterfaceFragmentComplete) frag;
                if (fragment != null) {
                    fragment.delete();
                }
            }
        }
    }

    private void complete_button_enable_disable(boolean value){
        if(value){
            ((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
            ((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
        }
        else {
            ((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
            ((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
        }
    }

}
