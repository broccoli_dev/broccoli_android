package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.LogApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendCardDetailBenefitInternalTotal;
import com.nomadconnection.broccoli.data.Spend.SpendCardRecommendData;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.fragment.spend.FragmentSpendCardBenefit;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 3. 20..
 */

public class Level3ActivitySpendCardRecommendDetail extends BaseActivity implements View.OnClickListener {

    private ImageView mLogo;
    private TextView mTitle;
    private ImageView mAnnualFeeBtn;
    private TextView mAnnualFee;
    private TextView mAnnualFeeForeign;
    private LinearLayout mMileageLayout;
    private LinearLayout mDiscountLayout;
    private LinearLayout mPointLayout;
    private NestedScrollView mScrollView;

    private TextView mDiscount;
    private TextView mPoint;
    private TextView mMileage;
    private TextView mOverdue;
    private TextView mLegal;
    private View mCardRequestBtn;

    private SpendCardRecommendData mData;
    private SpendCardDetailBenefitInternalTotal mDetailBenefitList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_spend_card_recommend_detail);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SpendCardRecmdDetail);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_card_recommend_detail_title);
        ((TextView)findViewById(R.id.tv_navi_motion_else)).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
        mScrollView = (NestedScrollView) findViewById(R.id.sv_spend_card_recommend_detail);
        mScrollView.setNestedScrollingEnabled(true);
        mLogo = (ImageView) findViewById(R.id.iv_spend_card_detail_logo);
        mTitle = (TextView) findViewById(R.id.tv_spend_card_detail_title);
        mAnnualFeeBtn = (ImageView) findViewById(R.id.iv_spend_card_detail_fee_more_btn);
        mAnnualFee = (TextView) findViewById(R.id.tv_spend_card_detail_internal);
        mAnnualFeeForeign = (TextView) findViewById(R.id.tv_spend_card_detail_foreign);
        mMileageLayout = (LinearLayout) findViewById(R.id.ll_spend_card_recommend_mileage_layout);
        mDiscountLayout = (LinearLayout) findViewById(R.id.ll_spend_card_recommend_discount_layout);
        mPointLayout = (LinearLayout) findViewById(R.id.ll_spend_card_recommend_point_layout);
        mDiscount = (TextView) findViewById(R.id.tv_spend_card_recommend_detail_discount);
        mPoint = (TextView) findViewById(R.id.tv_spend_card_recommend_detail_point);
        mMileage = (TextView) findViewById(R.id.tv_spend_card_recommend_detail_mileage);
        mOverdue = (TextView) findViewById(R.id.tv_spend_card_recommend_overdue);
        mLegal = (TextView) findViewById(R.id.tv_spend_card_recommend_legal);
        mCardRequestBtn = findViewById(R.id.btn_confirm);
        mAnnualFeeBtn.setOnClickListener(this);
        mCardRequestBtn.setOnClickListener(this);
    }

    private void initData() {
        mData = (SpendCardRecommendData) getIntent().getSerializableExtra(Const.DATA);
        mLogo.setImageResource(ElseUtils.createCardLogoItem(mData.cardCompany.brCmpnyCode));
        mTitle.setText(mData.card.cardName);
        if (mData.card.mileageYn > 0) {
            mMileageLayout.setVisibility(View.VISIBLE);
            mDiscountLayout.setVisibility(View.GONE);
            mPointLayout.setVisibility(View.GONE);
        } else {
            mMileageLayout.setVisibility(View.GONE);
            mDiscountLayout.setVisibility(View.VISIBLE);
            mPointLayout.setVisibility(View.VISIBLE);
        }
        mMileage.setText(ElseUtils.getDecimalFormat(mData.averageMileage));
        mDiscount.setText(ElseUtils.getDecimalFormat(mData.averageDiscount));
        mPoint.setText(ElseUtils.getDecimalFormat(mData.averagePoint));

        mAnnualFee.setText(ElseUtils.getDecimalFormat(mData.card.internalAnnualFee));
        mAnnualFeeForeign.setText(ElseUtils.getDecimalFormat(mData.card.foreignAnnualFee));
        if (mData.cardDetailList == null || mData.cardDetailList.size() == 0) {
            mAnnualFeeBtn.setVisibility(View.GONE);
        } else {
            mAnnualFeeBtn.setVisibility(View.VISIBLE);
        }
        if (mData.card.overdueMinRate > 0 && mData.card.overdueMaxRate > 0) {
            mOverdue.setText(getString(R.string.spend_card_recommend_detail_guide_due)+"연"+String.format("%.2f", (mData.card.overdueMinRate*100))+"% ~ 연"+String.format("%.2f", (mData.card.overdueMaxRate*100))+"%");
        } else {
            mOverdue.setVisibility(View.GONE);
        }
        if (mData.card.approvedNo == null) {
            mLegal.setVisibility(View.GONE);
        } else {
            mLegal.setText(getString(R.string.spend_card_recommend_detail_guide_legal)+mData.card.approvedNo+"호 ("+mData.card.approvedDate+")");
        }
        if (mData.card.applyUrl == null) {
            mCardRequestBtn.setEnabled(false);
        } else {
            mCardRequestBtn.setEnabled(true);
        }
        getData();
    }

    private void getData() {
        DialogUtils.showLoading(this);
        Call<JsonElement> call = ServiceGenerator.createService(SpendApi.class).getCardBenefitList(mData.card.cardCode);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                JsonObject object = element.getAsJsonObject();
                if (object.has("cardBenefitList")) {
                    mDetailBenefitList = new Gson().fromJson(object, SpendCardDetailBenefitInternalTotal.class);
                    showBenefit(mDetailBenefitList);
                } else {
                    ErrorUtils.parseError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void startOnlineRequest() {
        sendCardRequestLog(mData.card.cardCode);
//        Intent intent = new Intent(this, Level4ActivitySpendCardOnlineRequest.class);
//        intent.putExtra(Const.DATA, mData);
//        startActivity(intent);
        callChrome(mData.card.applyUrl);
    }

    public void callChrome(String url){
        //크롬브라우저 패키지명
        String packageName = "com.android.chrome";

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage(packageName);
        intent.setData(Uri.parse(url));

        //크롬브라우저가 설치되어있으면 호출, 없으면 선택해서
        List<ResolveInfo> activitiesList = getPackageManager().queryIntentActivities(intent, -1);
        if(activitiesList.size() > 0) {
            startActivity(intent);
        } else {
            try {
                Intent pickUpIntent = new Intent(Intent.ACTION_VIEW);
                pickUpIntent.setData(Uri.parse(url));
                startActivity(pickUpIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendCardRequestLog(String cardCode) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("cardCode", cardCode);
        Call<Result> call = ServiceGenerator.createService(LogApi.class).sendLogCardOnlineRequest(map);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    private void showBenefit(SpendCardDetailBenefitInternalTotal data) {
        BaseFragment fragment = new FragmentSpendCardBenefit();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Const.DATA, data);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        //ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        ft.replace(R.id.fl_fragment_benefit, fragment, fragment.getTagName());
        ft.commitAllowingStateLoss();
    }

    private void showAnnualFeeDialog() {
        DialogUtils.showDialogCardAnnualFee(this, mData.cardDetailList).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBtnClickNaviMotion(View view) {
        super.onBtnClickNaviMotion(view);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_spend_card_detail_fee_more_btn:
                showAnnualFeeDialog();
                break;
            case R.id.btn_confirm:
                startOnlineRequest();
                break;
        }
    }
}
