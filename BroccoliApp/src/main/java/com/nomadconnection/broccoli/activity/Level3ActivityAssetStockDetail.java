package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockInfo;
import com.nomadconnection.broccoli.data.Asset.EditAssetStock;
import com.nomadconnection.broccoli.data.Asset.RequestAssetStockDetail;
import com.nomadconnection.broccoli.data.Asset.ResponseStockDetail;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.fragment.FragmentAssetStock0;
import com.nomadconnection.broccoli.fragment.FragmentAssetStockDetail1;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorCode;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level3ActivityAssetStockDetail extends BaseActivity implements InterfaceEditOrDetail{

	private static final String TAG = Level3ActivityAssetStockDetail.class.getSimpleName();
	private static final String TAG0 = FragmentAssetStock0.class.getSimpleName();
	private static final String TAG1 = FragmentAssetStockDetail1.class.getSimpleName();


	public static int mCurrentFragmentIndex = 0;	// default
	private final int FRAGMENT_ONE 		= 0;		// 추가
	private final int FRAGMENT_TWO 		= 1;		// 상세
	
	/* Layout */
	private Animation mAnimTitle;

	/* Get Parcel Data */
	private BodyAssetStockInfo mInfoDataHeader;
	private ResponseStockDetail mInfoData;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_asset_stock_detail);
		sendTracker(AnalyticsName.Level3ActivityAssetStockDetail);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onBackPressed() {
		onBtnClickNaviMotion(findViewById(R.id.ll_navi_motion_back));
//		super.onBackPressed();
	}





//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try {
			switch (_view.getId()) {
				case R.id.tv_navi_motion_title:
					if (mCurrentFragmentIndex == 0)
						break;

				case R.id.ll_navi_motion_back:
					if (mCurrentFragmentIndex == 1) goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_RESULT_CODE_SUCCESS);
					else if (mCurrentFragmentIndex == 0) {
						String mStr = "매수 내역 추가를\n취소하시겠습니까?";
						DialogUtils.showDlgBaseTwoButton(Level3ActivityAssetStockDetail.this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
											mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentFragmentIndex == 1) { mCurrentFragmentIndex = 0; fragmentReplace(mCurrentFragmentIndex, "in_right"); afterMotion("else"); }
					else if (mCurrentFragmentIndex == 0) {
						if (checkValueIsFail()) return;
						DialogUtils.showLoading(Level3ActivityAssetStockDetail.this);											// 자산 로딩
						Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).editStock(
								new EditAssetStock(Session.getInstance(this).getUserId(),
										"0",	// 추가
										Integer.valueOf(TransFormatUtils.transStockNameToCode(FragmentAssetStock0.mInfoType.getText().toString())),
										TransFormatUtils.transRemoveDot(FragmentAssetStock0.mInfoDate.getText().toString()),
										FragmentAssetStock0.mStockSearchReturnData.mStockCode,
										TransFormatUtils.transRemoveComma(FragmentAssetStock0.mInfoValue.getText().toString()),
										TransFormatUtils.transRemoveComma(FragmentAssetStock0.mInfoAmount.getText().toString()),
										TransFormatUtils.transRemoveComma(FragmentAssetStock0.mInfoCost.getText().toString()))
						);
						mCall.enqueue(new Callback<Result>() {
							@Override
							public void onResponse(Call<Result> call, Response<Result> response) {
								DialogUtils.dismissLoading();											// 자산 로딩
								if (response.isSuccessful()) {
									if (response.body() != null) {
										NextStepAdd(response.body());
									} else {
										ElseUtils.network_error(Level3ActivityAssetStockDetail.this);
									}
								} else {
									ElseUtils.network_error(Level3ActivityAssetStockDetail.this);
								}
							}

							@Override
							public void onFailure(Call<Result> call, Throwable t) {
								DialogUtils.dismissLoading();											// 자산 로딩
								ElseUtils.network_error(Level3ActivityAssetStockDetail.this);
							}
						});
					}
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}


	private void afterMotion(String _Divider){
		if(_Divider.equals("else")){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG0).mTitle);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("완료");
			complete_button_enable_disable(false); // 완료버튼 disable
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_04_0_0_400_fill);
		}
		else{	// default
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG1).mTitle);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("추가");
			complete_button_enable_disable(true); // 완료버튼 enable
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_04_0_0_0_400_fill);
		}
		((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);
	}








//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			/* getExtra */
			/* getParcelableExtra */
			mInfoDataHeader = getIntent().getParcelableExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER);
			mInfoData = (ResponseStockDetail) getIntent().getSerializableExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_BODY);
			if(mInfoDataHeader == null || mInfoData == null)
				bResult = false;

		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG1).mTitle);
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) {
				((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			}
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("추가");
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);

			if (getIntent().hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
				findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_3_layout_bg));
			}

			/* Layout */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCurrentFragmentIndex = 1;
			fragmentReplace(mCurrentFragmentIndex, "");
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	
	

//=========================================================================================//
// Fragment Navi
//=========================================================================================//
	/** Fragment 변경 **/
	private void fragmentReplace(int reqNewFragmentIndex, String _Anim)
	{
		try
		{
			Fragment newFragment = null;
			newFragment = getFragment(reqNewFragmentIndex);

			// replace fragment
			final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			if (_Anim.length() != 0 && _Anim.equals("in_right")) transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out);
			if (_Anim.length() != 0 && _Anim.equals("out_right")) transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
			transaction.replace(R.id.fl_level_3_activity_asset_stock_detail_container, newFragment);

			// Commit the transaction
			transaction.commitAllowingStateLoss();

		}
		catch (Exception e)
		{}
	}



	/** Fragment Get **/
	private Fragment getFragment(int idx) {
		Fragment newFragment = null;

		switch (idx) {
		case FRAGMENT_ONE:	// 추가
			newFragment = new FragmentAssetStock0();
			Bundle bundle = newFragment.getArguments();
			if (bundle == null) {
				bundle = new Bundle();
			}
			bundle.putString(FragmentAssetStock0.NAME, mInfoDataHeader.mBodyAssetStockName);
			bundle.putString(FragmentAssetStock0.CODE, mInfoDataHeader.mBodyAssetStockCode);
			newFragment.setArguments(bundle);

			break;

		case FRAGMENT_TWO:	// 상세
			newFragment = new FragmentAssetStockDetail1(mInfoDataHeader, mInfoData);
			break;

		default:
			break;
		}

		return newFragment;
	}





//===============================================================================//
// Value Check
//===============================================================================//
	/**  Value check **/
	private Boolean checkValueIsFail(){
		boolean mResult = false;

		if (FragmentAssetStock0.mInfoName.getText().toString().equals("선택")){
			Toast.makeText(Level3ActivityAssetStockDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityAssetStockDetail.this).showShort("종목명을 입력하세요");
			mResult = true;
		}
		else if (FragmentAssetStock0.mInfoType.getText().toString().equals("선택")){
			Toast.makeText(Level3ActivityAssetStockDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityAssetStockDetail.this).showShort("거래종류를 선택하세요");
			mResult = true;
		}
		else if (FragmentAssetStock0.mInfoDate.getText().toString().equals("선택")){
			Toast.makeText(Level3ActivityAssetStockDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityAssetStockDetail.this).showShort("일자를 선택하세요");
			mResult = true;
		}
		else if (FragmentAssetStock0.mInfoValue.getText().toString().trim().length() == 0){
			Toast.makeText(Level3ActivityAssetStockDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//CustomToast.getInstance(Level3ActivityAssetStockDetail.this).showShort("금액을 입력하세요");
			mResult = true;
		}
		else if (FragmentAssetStock0.mInfoAmount.getText().toString().trim().length() == 0){
			Toast.makeText(Level3ActivityAssetStockDetail.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			//ustomToast.getInstance(Level3ActivityAssetStockDetail.this).showShort("수량을 입력하세요");
			mResult = true;
		}

		return  mResult;
	}


//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep Add **/
	private void NextStepAdd(Result result){
		if (result.getCode().equals("100")){
			refreshData();
		} else if (ErrorCode.ERROR_CODE_605_STOCK_MARKET_CLOSED.equalsIgnoreCase(result.getCode())){
			Toast.makeText(this, getString(R.string.common_stock_add_error), Toast.LENGTH_SHORT).show();
		}
	}






//===============================================================================//
// Refresh
//===============================================================================//
	private void refreshData(){
		DialogUtils.showLoading(Level3ActivityAssetStockDetail.this);											// 자산 로딩
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(this).getUserId());

		Call<ResponseStockDetail> mCall = ServiceGenerator.createService(AssetApi.class).getStockDetail(
				new RequestAssetStockDetail(Session.getInstance(this).getUserId(),
						mInfoDataHeader.mBodyAssetStockCode)
		);

		mCall.enqueue(new Callback<ResponseStockDetail>() {
			@Override
			public void onResponse(Call<ResponseStockDetail> call, Response<ResponseStockDetail> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				mInfoData = response.body();
				mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
			}

			@Override
			public void onFailure(Call<ResponseStockDetail> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ElseUtils.network_error(Level3ActivityAssetStockDetail.this);
			}
		});
	}




//	private void refreshDataSet(){
//		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
//		mRequestDataByUserId.setUserId(Session.getInstance(this).getUserId());
//
//		Call<AssetStockInfo> mCall = ServiceGenerator.createService(AssetApi.class).getStockList(mRequestDataByUserId);
//		mCall.enqueue(new Callback<AssetStockInfo>() {
//			@Override
//			public void onResponse(Response<AssetStockInfo> response) {
//				AssetStockInfo mTemp;
//				if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData14();
//				else mTemp = response.body();
//				mAlInfoDataList.removeAll(mAlInfoDataList);
//				mAlInfoDataList.addAll(mTemp.mAssetStockList);
//				mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
//			}
//
//			@Override
//			public void onFailure(Throwable t) {
//				AssetStockInfo mTemp;
//				if (APP._SAMPLE_MODE_) {
//					mTemp = AssetSampleTestDataJson.GetSampleData14();
//					mAlInfoDataList.removeAll(mAlInfoDataList);
//					mAlInfoDataList.addAll(mTemp.mAssetStockList);
//					mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
//				}
//				else CustomToast.getInstance(Level2ActivityAssetStock.this).showShort("Level2ActivityAssetStock 서버 통신 실패 : 주식 리스트");
//			}
//		});
//	}


//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int _resultCod){
		Intent intent = getIntent();
		setResult(_resultCod, intent);
		finish();
	}




	//=========================================================================================//
// Interface
//=========================================================================================//
	@Override
	public void onEdit() {
		// TODO Auto-generated method stub
//		mCurrentFragmentIndex = 1;
//		fragmentReplace(mCurrentFragmentIndex, "");
		complete_button_enable_disable(true); // 완료버튼 enable
	}

	@Override
	public void onDetail() {
		// TODO Auto-generated method stub
		mCurrentFragmentIndex = 0;
		fragmentReplace(mCurrentFragmentIndex, "");
	}

	@Override
	public void onError() {
		// TODO Auto-generated method stub
//		mCurrentFragmentIndex = 0;
//		fragmentReplace(mCurrentFragmentIndex);
		complete_button_enable_disable(false); // 완료버튼 disable
	}

	@Override
	public void onRefresh() {

	}


	private void complete_button_enable_disable(boolean value){
		if(value){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}
}
