package com.nomadconnection.broccoli.activity.setting;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Spend.Qna;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;


public class Level4ActivitySettingQnaDetail extends BaseActivity{

	private static final String TAG = Level4ActivitySettingQnaDetail.class.getSimpleName();

	
	/* Get Parcel Data */
	private Qna mInfoDataSettingFan;
	
	/* Layout */
	private TextView mTitle;
	private ImageView mNewIcon;
	private TextView mTime;
	private TextView mContents;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_4_activity_setting_notice_detail);
		sendTracker(AnalyticsName.SettingFAQDetail);

		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}



	
	
	

//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {    	
		try
		{
			switch (_view.getId()) {
				
			case R.id.ll_navi_img_txt_back:
				finish();
				break;
				
			default:
				break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());			
		}
	}


	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mInfoDataSettingFan = (Qna)getIntent().getSerializableExtra(Const.DATA);
			if(mInfoDataSettingFan == null)
				bResult = false;
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout) findViewById(R.id.rl_navi_img_bg)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.setting_faq_title_detail));
			
			/* Layout */
			mTitle = (TextView)findViewById(R.id.tv_level_4_activity_setting_notice_detail_title);
			mNewIcon = (ImageView)findViewById(R.id.iv_level_4_activity_setting_notice_detail_new_icon);
			mTime = (TextView)findViewById(R.id.tv_level_4_activity_setting_notice_detail_time);
			mContents = (TextView)findViewById(R.id.tv_level_4_activity_setting_notice_detail_contents);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mTitle.setText(mInfoDataSettingFan.getQuestion());
			mTime.setVisibility(View.GONE);
//			mTime.setText(mInfoDataSettingNotice.mTxt2);
			mContents.setText(mInfoDataSettingFan.getAnswer());
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	
	

		

}
