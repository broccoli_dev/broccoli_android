package com.nomadconnection.broccoli.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockInfo;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockList;
import com.nomadconnection.broccoli.data.Asset.DeleteAssetStock;
import com.nomadconnection.broccoli.data.Asset.EditAssetStock;
import com.nomadconnection.broccoli.data.Asset.ResponseSearchAssetStock;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorCode;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level4ActivityAssetStockHistoryDetail extends BaseActivity {

	private static final String TAG = Level4ActivityAssetStockHistoryDetail.class.getSimpleName();

	public static int mCurrentStateIndex = 0;		// default 0: 상세 , 1: 편집
	/* DataClass */
	public static ResponseSearchAssetStock mStockSearchReturnData;
	/* Layout*/
	private Animation mAnimTitle;
	private Animation mAnimSpinnerLayout;
	private Animation mAnimSpinnerText;
	private Animation mAnimSpinnerEditText;
	private Animation mAnimDelete;
	private Button mDelete;
	Animation.AnimationListener myAnimationListener = new Animation.AnimationListener() {
		public void onAnimationEnd(Animation animation) {
			mDelete.setVisibility(View.GONE); //애니메 끝나면 사라저버려!
			mDelete.clearAnimation();
		}
		@Override
		public void onAnimationRepeat(Animation animation) {
		}
		@Override
		public void onAnimationStart(Animation animation) {
		}
	};
	/* Layout */
	private LinearLayout mStockName;
	private LinearLayout mStockType;
	private LinearLayout mDate;
	private TextView mInfoName;
	private String mInfoNamemStockCode="";
	private TextView mInfoType;
	private TextView mInfoDate;
	private EditText mInfoValue;
	private EditText mInfoAmount;
	private TextView mInfoCost;
//	private EditText mInfoCost;
	private String mTextWatcherValueResult = "";
	private String mTextWatcherAmountResult = "";
	private String mTextWatcherCostResult = "";
	private ArrayList<String> mSpinnerStockTypeData;
	/* Get Parcel Data */
	private BodyAssetStockInfo mInfoDataHeader;
	private BodyAssetStockList mInfoData;
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
//			if (v.getId() != R.id.btn_level_4_activity_asset_stock_history_detail_delete)v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
//			if (v.getId() == R.id.ll_asset_stock_inc_spinner_layout_1)v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
			mInfoValue.clearFocus();
			mInfoAmount.clearFocus();
			switch (v.getId()) {

				case R.id.btn_level_4_activity_asset_stock_history_detail_delete:
					if (mCurrentStateIndex == 0) return;
//					DialogUtils.showLoading(Level4ActivityAssetStockHistoryDetail.this);											// 자산 로딩
					String mStr = "매수 내역을\n 삭제하시겠습니까?";
					DialogUtils.showDlgBaseTwoButton(Level4ActivityAssetStockHistoryDetail.this, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
										DialogUtils.showLoading(Level4ActivityAssetStockHistoryDetail.this);											// 자산 로딩
										ServiceGenerator.createService(AssetApi.class).deleteStock(
												new DeleteAssetStock(Session.getInstance(Level4ActivityAssetStockHistoryDetail.this).getUserId(), mInfoData.mBodyAssetStockDetailTransId)
										).enqueue(new Callback<Result>() {
											@Override
											public void onResponse(Call<Result> call, Response<Result> response) {
												DialogUtils.dismissLoading();											// 자산 로딩
												if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
													if (response.body() != null) {
														NextStepDelete(response.body());
													} else {
														ElseUtils.network_error(getBase());
													}
												} else {
													ElseUtils.network_error(getBase());
												}
											}

											@Override
											public void onFailure(Call<Result> call, Throwable t) {
												DialogUtils.dismissLoading();											// 자산 로딩
												ElseUtils.network_error(Level4ActivityAssetStockHistoryDetail.this);
											}
										});
									}
								}
							},
							new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
//					ServiceGenerator.createService(AssetApi.class).deleteStock(
//							new DeleteAssetStock(Session.getInstance(Level4ActivityAssetStockHistoryDetail.this).getUserId(), mInfoData.mBodyAssetStockDetailTransId)
//					).enqueue(new Callback<Result>() {
//						@Override
//						public void onResponse(Response<Result> response) {
//							if (APP._SAMPLE_MODE_) NextStepDelete(AssetSampleTestDataJson.GetSampleData7());
//							else NextStepDelete(response.body());
//						}
//
//						@Override
//						public void onFailure(Throwable t) {
//							if (APP._SAMPLE_MODE_) {
//								NextStepDelete(AssetSampleTestDataJson.GetSampleData7());
//							}
//							else CustomToast.getInstance(Level4ActivityAssetStockHistoryDetail.this).showShort("Level4ActivityAssetStockHistoryDetail 서버 통신 실패 - 주식 내역 삭제");
//						}
//					});
					break;

				case R.id.ll_asset_stock_inc_button_layout_1:
					Intent intent = new Intent(Level4ActivityAssetStockHistoryDetail.this, Level3ActivityAssetStockSearch.class);
					if (getBase().getIntent().hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
						intent.putExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND, true);
					}
					startActivityForResult(intent, APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_REQUEST_CODE);
					break;

				case R.id.ll_asset_stock_inc_spinner_layout_1:
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(Level4ActivityAssetStockHistoryDetail.this).top);
					int mTempLeft1 = (ElseUtils.getRect(v).left);
					DialogUtils.showDlgSpinnerList(Level4ActivityAssetStockHistoryDetail.this, new Dialog.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mInfoData.mBodyAssetStockDetailTransType = (which+1);
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
							mInfoType.setTextAppearance(Level4ActivityAssetStockHistoryDetail.this, R.style.CS_Text_15_898989);
							mInfoType.setText(TransFormatUtils.transStockCodeToName(String.valueOf(mInfoData.mBodyAssetStockDetailTransType)));
							dialog.dismiss();
						}
					}, mSpinnerStockTypeData, mTempTop1, mTempLeft1, (mInfoData.mBodyAssetStockDetailTransType-1), new Dialog.OnDismissListener(){

						@Override
						public void onDismiss(DialogInterface dialog) {
							v.setBackgroundResource(R.drawable.selector_spinner_bg);
						}
					});
					break;

				case R.id.ll_asset_stock_inc_spinner_layout_2:
					DatePickerDialog.OnDateSetListener callBack1 = new DatePickerDialog.OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear,
											  int dayOfMonth) {
							String msg = String.format("%04d.%02d.%02d", year, monthOfYear+1, dayOfMonth);
							mInfoDate.setText(msg);
							send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
							//CustomToast.getInstance(Level4ActivityAssetStockHistoryDetail.this).showShort(msg);
						}
					};
					int mYear, mMonth, mDay = 0;
					String mTempNow = TransFormatUtils.getDataFormatWhich(1);
					Date mTempNowDate = new Date();
					try {
						mTempNowDate = new SimpleDateFormat("yyyy.MM.dd").parse(mTempNow);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					if (mInfoDate.getText().toString().equals("선택")){
						mYear = Integer.valueOf(mTempNow.substring(0,4));
						mMonth = Integer.valueOf(mTempNow.substring(5,7));
						mDay = Integer.valueOf(mTempNow.substring(8,10));
					}
					else{
						mYear = Integer.valueOf(mInfoDate.getText().toString().substring(0,4));
						mMonth = Integer.valueOf(mInfoDate.getText().toString().substring(5,7));
						mDay = Integer.valueOf(mInfoDate.getText().toString().substring(8,10));
					}

					DatePickerDialog pickerDialog = new DatePickerDialog(Level4ActivityAssetStockHistoryDetail.this, AlertDialog.THEME_HOLO_LIGHT, callBack1, mYear, mMonth-1, mDay);
					pickerDialog.getDatePicker().setMaxDate(mTempNowDate.getTime());		// 처음에 현재 이전 날자 클릭 막아 달라고 했다고, 2016-02-12에 이후 막아달라고 요청
					pickerDialog.show();
					break;

				default:
					break;
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_4_activity_asset_stock_history_detail);

		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}





//=========================================================================================//
// ToolBar - motion
//=========================================================================================//

	@Override
	public void onBackPressed() {
		onBtnClickNaviMotion(findViewById(R.id.ll_navi_motion_back));
//		super.onBackPressed();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_REQUEST_CODE) {
			if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_SUCCESS){
				mStockSearchReturnData = (ResponseSearchAssetStock) data.getParcelableExtra(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_DATA);
				if (mStockSearchReturnData != null){
					mInfoName.setText(mStockSearchReturnData.mStockName);
					mInfoNamemStockCode = mStockSearchReturnData.mStockCode;
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			}
			else if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_CANCLE){
			}
		}
	}

	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try
		{
			switch (_view.getId()) {
				case R.id.tv_navi_motion_title:
					if (mCurrentStateIndex == 1)
						break;

				case R.id.ll_navi_motion_back:
					if (mCurrentStateIndex == 0) { goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_SUCCESS); }
					else if (mCurrentStateIndex == 1) {
						String mStr = "매수 내역 편집을\n취소하시겠습니까?";
						DialogUtils.showDlgBaseTwoButton(Level4ActivityAssetStockHistoryDetail.this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
											resetData(); mCurrentStateIndex = 0; afterMotion("back");
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
//						resetData(); mCurrentStateIndex = 0; afterMotion("back");
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentStateIndex == 0) { mCurrentStateIndex = 1; afterMotion("else");}
					else if (mCurrentStateIndex == 1) {
						DialogUtils.showLoading(Level4ActivityAssetStockHistoryDetail.this);											// 자산 로딩
						Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).editStock(
								new EditAssetStock(Session.getInstance(this).getUserId(),
										mInfoData.mBodyAssetStockDetailTransId,	// 수정
										Integer.valueOf(TransFormatUtils.transStockNameToCode(mInfoType.getText().toString())),
										TransFormatUtils.transRemoveDot(mInfoDate.getText().toString()),
										mInfoNamemStockCode,
//										mInfoDataHeader.mBodyAssetStockCode,
										TransFormatUtils.transRemoveComma(mInfoValue.getText().toString()),
										TransFormatUtils.transRemoveComma(mInfoAmount.getText().toString()),
										TransFormatUtils.transRemoveComma(mInfoCost.getText().toString()))
						);
						mCall.enqueue(new Callback<Result>() {
							@Override
							public void onResponse(Call<Result> call, Response<Result> response) {
								DialogUtils.dismissLoading();											// 자산 로딩
								if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
									if (response.body() != null) {
										NextStepEdit(response.body());
									} else {
										ElseUtils.network_error(getBase());
									}
								} else {
									ElseUtils.network_error(getBase());
								}
							}

							@Override
							public void onFailure(Call<Result> call, Throwable t) {
								DialogUtils.dismissLoading();											// 자산 로딩
								if (APP._SAMPLE_MODE_) {
									NextStepEdit(AssetSampleTestDataJson.GetSampleData6());
								}
								else
									ElseUtils.network_error(Level4ActivityAssetStockHistoryDetail.this);
							}
						});
					}
					break;

				default:
					break;
			}
		} catch(Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}

	private void afterMotion(String _Divider){
		int count = 0;
		if(_Divider.equals("else")){
			count = 0;
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_03_0_0_400_fill);
			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_layout_right);
			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_left);
			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_left);
			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_down);
		}
		else{	// default
			count = 400;
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_03_0_0_0_400_fill);
			mAnimSpinnerLayout = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_layout_left);
			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_right);
			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_up);
			mAnimDelete.setAnimationListener(myAnimationListener);
		}

		((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);
		mDelete.startAnimation(mAnimDelete);
		mDelete.setVisibility(View.VISIBLE);
//		mStockName.startAnimation(mAnimSpinnerLayout);			// group 1
//		mStockType.startAnimation(mAnimSpinnerLayout);			// group 1
//		mDate.startAnimation(mAnimSpinnerLayout);				// group 1

		mInfoName.startAnimation(mAnimSpinnerText);				// group 1
		mInfoType.startAnimation(mAnimSpinnerText);				// group 1
		mInfoDate.startAnimation(mAnimSpinnerText);				// group 1
		mInfoValue.startAnimation(mAnimSpinnerEditText);
		mInfoAmount.startAnimation(mAnimSpinnerEditText);
		mInfoCost.startAnimation(mAnimSpinnerText);				// group 1
//		mInfoCost.startAnimation(mAnimSpinnerEditText);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				DivierLayout();
			}
		}, count);
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mInfoDataHeader = getIntent().getParcelableExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER);
			mInfoData = (BodyAssetStockList) getIntent().getSerializableExtra(APP.INFO_DATA_ASSET_STOCK_HISTORY_PARCEL);
			if(mInfoDataHeader == null || mInfoData == null)
				bResult = false;

			mInfoNamemStockCode = mInfoDataHeader.mBodyAssetStockCode;
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout */
			mDelete = (Button)findViewById(R.id.btn_level_4_activity_asset_stock_history_detail_delete);

			/* Layout 선언 */
			mStockName = (LinearLayout)findViewById(R.id.ll_asset_stock_inc_button_layout_1);
			mStockType = (LinearLayout)findViewById(R.id.ll_asset_stock_inc_spinner_layout_1);
			mDate = (LinearLayout)findViewById(R.id.ll_asset_stock_inc_spinner_layout_2);
			mInfoName = (TextView)findViewById(R.id.tv_asset_stock_inc_button_text_1);
			mInfoType = (TextView)findViewById(R.id.tv_asset_stock_inc_spinner_text_1);
			mInfoDate = (TextView)findViewById(R.id.tv_asset_stock_inc_spinner_text_2);
			mInfoValue = (EditText)findViewById(R.id.et_asset_stock_inc_edittext_1);
			mInfoAmount = (EditText)findViewById(R.id.et_asset_stock_inc_edittext_2);
			mInfoCost = (TextView)findViewById(R.id.tv_asset_stock_inc_spinner_text_3);
//			mInfoCost = (EditText)findViewById(R.id.et_asset_stock_inc_edittext_3);

			mStockName.setOnClickListener(BtnClickListener);
//			mStockType.setOnClickListener(BtnClickListener);
			mDate.setOnClickListener(BtnClickListener);
			mDelete.setOnClickListener(BtnClickListener);

			mInfoValue.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (!s.toString().equals(mTextWatcherValueResult)) {
						if (s.toString().length() == 0) mTextWatcherValueResult = "0";
						else
							mTextWatcherValueResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));

						if(mTextWatcherValueResult.equals("0")){
							mInfoValue.setHint("가격입력");
						}
						else {
							mInfoValue.setText(mTextWatcherValueResult);
							mInfoValue.setSelection(mTextWatcherValueResult.length());
						}

						String s1 = mInfoValue.getText().toString().replaceAll(",", "");
						String s2 = mInfoAmount.getText().toString().replaceAll(",", "");
						long m1 = 0;
						if (s1.length() != 0) m1 = Long.valueOf(s1);
						long m2 = 0;
						if (s2.length() != 0) m2 = Long.valueOf(s2);
						String m3 = TransFormatUtils.mDecimalFormat.format(Long.parseLong(String.valueOf(m1 * m2)));
						mInfoCost.setText(m3);
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			});

			mInfoAmount.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (!s.toString().equals(mTextWatcherAmountResult)) {
						if (s.toString().length() == 0) mTextWatcherAmountResult = "0";
						else
							mTextWatcherAmountResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));

						if(mTextWatcherAmountResult.equals("0")){
							mInfoAmount.setHint(getString(R.string.common_input_amount_hint));
						}
						else {
							mInfoAmount.setText(mTextWatcherAmountResult);
							mInfoAmount.setSelection(mTextWatcherAmountResult.length());
						}

						String s1 = mInfoValue.getText().toString().replaceAll(",", "");
						String s2 = mInfoAmount.getText().toString().replaceAll(",", "");
						long m1 = 0;
						if (s1.length() != 0) m1 = Long.parseLong(s1);
						long m2 = 0;
						if (s2.length() != 0) m2 = Long.parseLong(s2);
						String m3 = TransFormatUtils.mDecimalFormat.format(Long.parseLong(String.valueOf(m1 * m2)));
						mInfoCost.setText(m3);
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			});

//			mInfoCost.addTextChangedListener(new TextWatcher() {
//				@Override
//				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//				}
//
//				@Override
//				public void onTextChanged(CharSequence s, int start, int before, int count) {
//					if (!s.toString().equals(mTextWatcherCostResult)) {
//						if (s.toString().length() == 0) mTextWatcherCostResult = "0";
//						else
//							mTextWatcherCostResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
//						mInfoCost.setText(mTextWatcherCostResult);
//						mInfoCost.setSelection(mTextWatcherCostResult.length());
//					}
//				}
//
//				@Override
//				public void afterTextChanged(Editable s) {
//				}
//			});
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Title */
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) {
				((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			}

			/* Default Value */
			mCurrentStateIndex = 0;	// 상세화면
			DivierLayout();

			mAnimSpinnerText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_spinner_text_right);
			mAnimSpinnerText.setDuration(0);
			mAnimSpinnerText.setFillAfter(true);

			mAnimSpinnerEditText = AnimationUtils.loadAnimation(this, R.anim.translate_animation_edit_right);
			mAnimSpinnerEditText.setDuration(0);
			mAnimSpinnerEditText.setFillAfter(true);

//			mAnimDelete = AnimationUtils.loadAnimation(this, R.anim.translate_animation_bottom_btn_up);
//			mAnimDelete.setDuration(0);
//			mAnimDelete.setFillAfter(true);

			mSpinnerStockTypeData = new ArrayList<String>();
			Collections.addAll(mSpinnerStockTypeData, getResources().getStringArray(R.array.spinner_asset_stock_1));

			resetData();
//			mInfoName.setText(mInfoDataHeader.mBodyAssetStockName);
//			mInfoType.setText(TransFormatUtils.transStockCodeToName(String.valueOf(mInfoData.mBodyAssetStockDetailTransType)));
//			mInfoDate.setText(TransFormatUtils.transAddDot(mInfoData.mBodyAssetStockDetailTransDate));
//			mInfoValue.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mBodyAssetStockDetailPrice));
//			mInfoValue.setSelection(mInfoValue.getText().toString().length());
//			mInfoAmount.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mBodyAssetStockDetailAmount));
//			mInfoAmount.setSelection(mInfoAmount.getText().toString().length());
//			mInfoCost.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mBodyAssetStockDetailSum));
//			mInfoCost.setEnabled(false);

			mInfoName.startAnimation(mAnimSpinnerText);
			mInfoType.startAnimation(mAnimSpinnerText);
			mInfoDate.startAnimation(mAnimSpinnerText);
			mInfoValue.startAnimation(mAnimSpinnerEditText);
			mInfoAmount.startAnimation(mAnimSpinnerEditText);
			mInfoCost.startAnimation(mAnimSpinnerText);
			mInfoCost.startAnimation(mAnimSpinnerEditText);
//			mDelete.startAnimation(mAnimDelete);
			mDelete.setVisibility(View.GONE);

			if (getIntent().hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
				findViewById(R.id.rl_navi_motion_bg).setBackgroundColor(getResources().getColor(R.color.main_3_layout_bg));
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void DivierLayout(){

		if (mCurrentStateIndex == 0){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText("매수 내역 상세");
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("편집");
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			mStockName.setEnabled(false);
//			mStockType.setEnabled(false);
			mDate.setEnabled(false);
			mInfoValue.setEnabled(false);
			mInfoAmount.setEnabled(false);
//			mInfoCost.setEnabled(false);
//			mStockName.setBackgroundResource(R.drawable.selector_spinner_bg_open);				// group 1
//			mStockType.setBackgroundResource(R.drawable.selector_spinner_bg_open);
//			mDate.setBackgroundResource(R.drawable.selector_spinner_bg_open);					// group 1
			mInfoValue.setBackgroundResource(0);
			mInfoAmount.setBackgroundResource(0);
//			mInfoCost.setBackgroundResource(0);
		}
		else{
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText("매수 내역 편집");
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("저장");
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
			mStockName.setEnabled(true);					// group 1
//			mStockType.setEnabled(true);
			mDate.setEnabled(true);							// group 1
			mInfoValue.setEnabled(true);
			mInfoAmount.setEnabled(true);
//			mInfoCost.setEnabled(true);
//			mStockName.setBackgroundResource(R.drawable.selector_spinner_bg);			// group 1
//			mStockType.setBackgroundResource(R.drawable.selector_spinner_bg);
//			mDate.setBackgroundResource(R.drawable.selector_spinner_bg);				// group 1
			mInfoValue.setBackgroundResource(R.drawable.selector_edittext_bg);
			mInfoAmount.setBackgroundResource(R.drawable.selector_edittext_bg);
//			mInfoCost.setBackgroundResource(R.drawable.selector_edittext_bg);
		}

	}




//===============================================================================//
// NextStep
//===============================================================================//

	private void resetData() {
		mInfoName.setText(mInfoDataHeader.mBodyAssetStockName);
		mInfoType.setText(TransFormatUtils.transStockCodeToName(String.valueOf(mInfoData.mBodyAssetStockDetailTransType)));
		mInfoDate.setText(TransFormatUtils.transAddDot(mInfoData.mBodyAssetStockDetailTransDate));
		mInfoValue.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mBodyAssetStockDetailPrice));
		mInfoValue.setSelection(mInfoValue.getText().toString().length());
		mInfoAmount.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mBodyAssetStockDetailAmount));
		mInfoAmount.setSelection(mInfoAmount.getText().toString().length());
		mInfoCost.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mBodyAssetStockDetailSum));
	}

	/**  NextStep Edit **/
	private void NextStepEdit(Result _Result){
		if (_Result.getCode().equals("100")){
			//CustomToast.getInstance(Level4ActivityAssetStockHistoryDetail.this).showShort("주식 내역 수정 완료");
			mCurrentStateIndex = 0; afterMotion("back");
		} else if (ErrorCode.ERROR_CODE_605_STOCK_MARKET_CLOSED.equalsIgnoreCase(_Result.getCode())) {
			Toast.makeText(Level4ActivityAssetStockHistoryDetail.this, getString(R.string.common_stock_add_error), Toast.LENGTH_SHORT).show();
		}
	}

	/**  NextStep Delete **/
	private void NextStepDelete(Result _Result){
		if (_Result.getCode().equals("100")){
			goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_DELETE);
		}
		else{
			//CustomToast.getInstance(Level4ActivityAssetStockHistoryDetail.this).showShort("주식 내역 삭제 실패");
		}
	}

//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int _resultCod){
		Intent intent = getIntent();
		setResult(_resultCod, intent);
		finish();
	}


	private void send_interface_complete(){
		if(empty_check()){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}

	private boolean empty_check(){
		if(mInfoName.getText().toString().equals("종목 검색") || mInfoDate.getText().toString().equals(getString(R.string.common_input_date_hint_2))
				|| mInfoValue.length() == 0  || mInfoAmount.length() == 0 || mInfoCost.length() == 0
				|| mInfoValue.getText().toString().equals("가격입력") || mInfoAmount.getText().toString().equals(getString(R.string.common_input_amount_hint))){
			return false;
		}
		return true;
	}


}
