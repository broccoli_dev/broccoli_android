package com.nomadconnection.broccoli.activity.remit;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.BaseInputConnection;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RemitResult;
import com.nomadconnection.broccoli.data.Remit.RequestDataPW;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoliraonsecure.TransKeyManager;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCtrl;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RemitRegisterPasswordActivity extends BaseActivity implements ITransKeyActionListener, ITransKeyActionListenerEx {

	private static final String TAG = RemitRegisterPasswordActivity.class.getSimpleName();


	private static final String EDIT_TEXT = "mEditText";
	private static final int PASSWORD_MINIMUM_LENGTH = 4;

	/* Layout */
	private TextView mPasswordGuide;
	private LinearLayout mPassWordLayout;
	private ImageView mPassWordOne;
	private ImageView mPassWordTwo;
	private ImageView mPassWordThree;
	private ImageView mPassWordFourth;
	private EditText mEditText;
	private BaseInputConnection mInputConnection;
	private TextView tmpTv;
	private String mPwd;
	private boolean mCompareMode = false;
	private String mUserPwd;
	private String mConfirm;
	private boolean	mPasswordReset = false;

	private TransKeyCtrl mTransKey;
	private boolean isTransKeyShowing = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remit_register_password);
//		sendTracker(AnalyticsName.Level2ActivitySpendHistory);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			onBackPressed();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onBackPressed() {
		if( isTransKeyShowing){
			mTransKey.finishTransKey(true);
			isTransKeyShowing = false;
			return;
		}
		super.onBackPressed();
	}

	//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviTxtTxtTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {
				case R.id.ll_navi_txt_txt_txt_back:
					super.onBackPressed();
					break;
				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}

	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			if (getIntent().getExtras() != null) {
				mPasswordReset = getIntent().getBooleanExtra(Const.REMIT_PASSWORD, false);
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout)findViewById(R.id.rl_navi_top_layout)).setBackgroundResource(R.color.main_4_layout_bg);
			((TextView)findViewById(R.id.tv_navi_txt_txt_txt_else)).setVisibility(View.GONE);

			/* Layout */
			mPasswordGuide = (TextView)findViewById(R.id.tv_activity_remit_register_password_guide);
			mPassWordLayout = (LinearLayout)findViewById(R.id.ll_activity_remit_register_password);
			mPassWordLayout.setOnClickListener(BtnClickListener);
			mPassWordOne = (ImageView)findViewById(R.id.iv_activity_remit_register_password_lock_num1);
			mPassWordTwo = (ImageView)findViewById(R.id.iv_activity_remit_register_password_lock_num2);
			mPassWordThree = (ImageView)findViewById(R.id.iv_activity_remit_register_password_lock_num3);
			mPassWordFourth = (ImageView)findViewById(R.id.iv_activity_remit_register_password_lock_num4);
			mEditText = (EditText)findViewById(R.id.et_activity_remit_register_password_secret_text);
			mInputConnection = new BaseInputConnection(mEditText, true);
			mEditText.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					int count = s.length();
					switch (count) {
						case 0:
							mPassWordOne.setVisibility(View.GONE);
							mPassWordTwo.setVisibility(View.GONE);
							mPassWordThree.setVisibility(View.GONE);
							mPassWordFourth.setVisibility(View.GONE);
							break;
						case 1:
							mPassWordOne.setVisibility(View.VISIBLE);
							mPassWordTwo.setVisibility(View.GONE);
							mPassWordThree.setVisibility(View.GONE);
							mPassWordFourth.setVisibility(View.GONE);
							break;
						case 2:
							mPassWordOne.setVisibility(View.VISIBLE);
							mPassWordTwo.setVisibility(View.VISIBLE);
							mPassWordThree.setVisibility(View.GONE);
							mPassWordFourth.setVisibility(View.GONE);
							break;
						case 3:
							mPassWordOne.setVisibility(View.VISIBLE);
							mPassWordTwo.setVisibility(View.VISIBLE);
							mPassWordThree.setVisibility(View.VISIBLE);
							mPassWordFourth.setVisibility(View.GONE);
							break;
						case 4:
							mPassWordOne.setVisibility(View.VISIBLE);
							mPassWordTwo.setVisibility(View.VISIBLE);
							mPassWordThree.setVisibility(View.VISIBLE);
							mPassWordFourth.setVisibility(View.VISIBLE);
							break;
					}
				}
			});

			// secure keyboard
			mTransKey = TransKeyManager.getInstance(this).initTransKeyPad(
					this,
					0,
					TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER,
					TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_LAST_IMAGE,
					"텍스트입력 16자",
					"텍스트입력 16자 입력하세요",
					25,
					"최대 글자 16자를 입력하셨습니다.",
					5,
					true,
					(FrameLayout)findViewById(R.id.keypadContainer),
					mEditText,
					null,
					null,
					null,
					(RelativeLayout)findViewById(R.id.keypadBallon),
					null,false, this, this);
//			nfilter.registerReceiver();
			mEditText.setInputType(0);
			PasswordTransformationMethod ptm = new PasswordTransformationMethod();
			mEditText.setTransformationMethod(ptm);
			mEditText.post(new Runnable() {
				@Override
				public void run() {
					if (!isTransKeyShowing) {
						onShowKeyboard(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
					}
				}
			});

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			if(mPasswordReset){
				((TextView)findViewById(R.id.tv_navi_txt_txt_txt_title)).setText(getString(R.string.remit_password_reset));
				mPasswordGuide.setText(getString(R.string.remit_password_reset_guide));
			}
			else {
				((TextView)findViewById(R.id.tv_navi_txt_txt_txt_title)).setText(getString(R.string.remit_register_text_password));
				mPasswordGuide.setText(getString(R.string.remit_register_text_step_guide_fourth));
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//	
	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.ll_activity_remit_register_password:
				if (!isTransKeyShowing) {
					onShowKeyboard(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
				}
				break;

			default:
				break;
			}

		}
	};


	private void onShowKeyboard(int type) {
		isTransKeyShowing = true;
		mTransKey.showKeypad(type);
	}

	private void processKeypad() {
//		이전 데이터 처리 방식 새로운 걸로 교체 필요
//		if( nFilterTO.getPlainLength() > 0){
//			//리턴 값을 해당 TextView에 넣는다.
//			if( new String(  nFilterTO.getFieldName() ).equals(EDIT_TEXT) ){
//				mEditText.setText( new String( nFilterTO.getDummyData() ) );
//				mPwd = new String(plainDataByte_char);
//				tmpTv = (TextView)findViewById(R.id.nf_num_tmp_editText);
//				tmpTv.setText( new String( nFilterTO.getDummyData() ) );
//
//
//				if(mPwd.length() == PASSWORD_MINIMUM_LENGTH){
//					if(!mCompareMode){
//						enterCompareMode(mPwd);
//						nfilter.nFilterClose(View.GONE);
//						if (nfilter.isNFilterViewVisibility() != View.VISIBLE) {
//							onShowKeyboard();
//							tmpTv.setText("");
//						}
//					}
//					else {
//						mConfirm = mPwd;
//						if (comparePwd()) {
//							nfilter.nFilterClose(View.GONE);
//							if(mPasswordReset){
//								chgSendPW();
//							}
//							else {
//								Intent intent = new Intent();
//								intent.putExtra(Const.REMIT_PASSWORD, mConfirm);
//								setResult(RESULT_OK, intent);
//								finish();
//							}
//
//						} else {
//							mPasswordGuide.setText(getString(R.string.membership_text_pwd_not_matched_guide));
////								mEditText.setText("");
//						}
//					}
//				}
//			}
//		}
//		else{
//			//리턴 값을 해당 TextView에 넣는다.
//			if( new String(  nFilterTO.getFieldName() ).equals(EDIT_TEXT) ){
//				mEditText.setText( "" );
//
//				// 입력필드가 가상키보드에 가려서 보이지 않을 경우
//				// 임시로 값을 보여주는 editText
//				// nfilter_char_key_view.xml 32라인에서 직접 수정 가능
//				tmpTv = (TextView)findViewById(R.id.nf_num_tmp_editText);
//				tmpTv.setText("" );
//			}
//		}
	}

	private void enterCompareMode(String password) {
		mUserPwd = password;
		mCompareMode = true;
		mEditText.setText("");
		mPasswordGuide.setText(getString(R.string.membership_text_compare_guide));
	}

	private boolean comparePwd() {
		boolean bool = false;
		if (mUserPwd != null) {
			if (mUserPwd.equals(mConfirm)) {
				bool = true;
			}
		}
		return bool;
	}

	private void chgSendPW() {
		DialogUtils.showLoading(this);
		RequestDataPW mRequestDataPW = new RequestDataPW();
		mRequestDataPW.setUserId(Session.getInstance(this).getUserId());
		mRequestDataPW.setAccountPW(mConfirm);

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<RemitResult> call = api.chgSendPW(mRequestDataPW);
		call.enqueue(new Callback<RemitResult>() {
			@Override
			public void onResponse(Call<RemitResult> call, Response<RemitResult> response) {
				DialogUtils.dismissLoading();
				if ("OK".equals(response.body().getResult())) {
					Toast.makeText(RemitRegisterPasswordActivity.this, "", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					setResult(RESULT_OK, intent);
					finish();
				} else {
					Toast.makeText(RemitRegisterPasswordActivity.this, getString(R.string.remit_password_reset_fail), Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(Call<RemitResult> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(RemitRegisterPasswordActivity.this);
			}
		});
	}

	@Override
	public void done(Intent intent) {

	}

	@Override
	public void cancel(Intent intent) {

	}

	@Override
	public void input(int i) {

	}
}
