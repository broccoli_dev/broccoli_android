package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igaworks.adbrix.IgawAdbrix;
import com.lumensoft.ks.KSCertificate;
import com.lumensoft.ks.KSCertificateManager;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoliraonsecure.KSW_CertListManager;
import com.nomadconnection.broccoliraonsecure.TransKeyManager;
import com.softsecurity.transkey.Global;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

import java.util.Calendar;

/**
 * Created by YelloHyunminJang on 16. 1. 25..
 */
public class CertPasswordActivity extends BaseActivity implements View.OnClickListener, View.OnTouchListener, ITransKeyActionListener, ITransKeyActionListenerEx {

    private static final int PASSWORD_MINIMUM_LENGTH = 8;
    private static final int PASSWORD_ERROR_MAX_COUNT = 5;

    private TransKeyCtrl mTransKey;
    private boolean isTransKeyShowing = false;

    private EditText mEditText;
    private TextView tmpTv;
    private String mPwd;
    private View mBtn;
    private View mSkip;

    private KSCertificate userCert = null;
    private String mCertNm = null;
    private String passWord;
    private byte[] mSecureKey;
    private byte[] mCipher;
    private boolean fromMembership = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cert_password);
        sendTracker(AnalyticsName.CertLogin);

        Intent intent = getIntent();
        if (intent != null) {
            mCertNm = intent.getStringExtra(Const.CERT_NAME);
            fromMembership = intent.getBooleanExtra(Const.FROM_MEMBERSHIP, false);
        }

        findViewById(R.id.ll_cert_input_layout).setOnClickListener(this);
        findViewById(R.id.ll_edittext_click_layout).setOnClickListener(this);
        (findViewById(R.id.et_cert_password)).findViewById(R.id.keyscroll).setOnClickListener(this);
        mEditText = (EditText) findViewById(R.id.et_cert_password).findViewById(R.id.editText);
        mEditText.setOnClickListener(this);
        mEditText.setOnTouchListener(this);
        findViewById(R.id.rl_navi_top_layout).setBackgroundColor(Color.BLACK);
        //title menu back button
        findViewById(R.id.ll_navi_txt_txt_txt_back).setOnClickListener(this);
        //title menu right etc btn not using
        findViewById(R.id.ll_navi_txt_txt_txt_else).setVisibility(View.GONE);
        mSkip = findViewById(R.id.btn_membership_skip);
        mSkip.setEnabled(true);
        mSkip.setOnClickListener(this);
        mBtn = findViewById(R.id.btn_fragment_membership_next);
        mBtn.setEnabled(false);
        mBtn.setOnClickListener(this);
        TextView title = (TextView) findViewById(R.id.tv_navi_txt_txt_txt_title);
        title.setTextColor(Color.WHITE);
        title.setText(R.string.membership_text_cert_pwd_title);

        mTransKey = TransKeyManager.getInstance(this).initTransKeyPad(
                this,
                0,
                TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER,
                TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_LAST_IMAGE,
                "텍스트입력",
                getString(R.string.membership_text_cert_pwd_hint),
                100,
                "최대 글자 100자를 입력하셨습니다.",
                5,
                true,
                (FrameLayout)findViewById(R.id.keypadContainer),
                (EditText)(findViewById(R.id.et_cert_password)).findViewById(R.id.editText),
                (HorizontalScrollView)(findViewById(R.id.et_cert_password)).findViewById(R.id.keyscroll),
                (LinearLayout)(findViewById(R.id.et_cert_password)).findViewById(R.id.keylayout),
                (ImageButton)(findViewById(R.id.et_cert_password)).findViewById(R.id.clearall),
                (RelativeLayout)findViewById(R.id.keypadBallon),
                null,false, this, this);


        mEditText.setInputType(0);
        PasswordTransformationMethod ptm = new PasswordTransformationMethod();
        mEditText.setTransformationMethod(ptm);
        mEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isTransKeyShowing) {
                    onShowKeyboard(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                }
            }
        });
        mEditText.post(new Runnable() {
            @Override
            public void run() {
                onShowKeyboard(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
            }
        });

        if (fromMembership) {
            mSkip.setVisibility(View.VISIBLE);
        } else {
            mSkip.setVisibility(View.GONE);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        mTransKey.showKeypad_changeOrientation();
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if( isTransKeyShowing){
            DialogUtils.showLoading(this);
            mTransKey.finishTransKey(true);
            DialogUtils.dismissLoading();
            isTransKeyShowing = false;
            return;
        }
        super.onBackPressed();
    }

    private void onShowKeyboard(int keyPadType) {
        isTransKeyShowing = true;
        mTransKey.showKeypad(keyPadType);
    }

    private void makeValue() {
        userCert = KSW_CertListManager.getSelectedCert();
        passWord = mEditText.getText().toString();

//        try {
//            if (bundle.getString("CATEGORY").equals("sign")) {
//                if (bundle.getString("SIGN_TYPE").equals("sign_cms")) {
////					userCert = null;
//                    signResult = KSSign.cmsSign(userCert, bundle.getString("PLAINTEXT"), passWord);
//                    vidResult = KSCertificateManager.getRandom(userCert, passWord);
////					signResult = KSSign.cmsByteSign(userCert, bundle.getString("PLAINTEXT").getBytes(), passWord.getBytes());
////					vidResult = KSCertificateManager.getRandom(userCert, passWord.getBytes());
//                } else if (bundle.getString("SIGN_TYPE").equals("sign_simple")) {
//                    signResult = KSSign.briefSign(userCert, bundle.getString("PLAINTEXT"), passWord);
//                    userCertPubKey = userCert.getCertByteArray();
//                    vidResult = KSCertificateManager.getRandom(userCert, passWord);
//                } else if (bundle.getString("SIGN_TYPE").equals("koscom_full")) {
//                    signResult = KSSign.koscomSign(userCert, bundle.getString("PLAINTEXT"), passWord);
//                } else if (bundle.getString("SIGN_TYPE").equals("koscom_simple")) {
//                    signResult = KSSign.koscomBriefSign(userCert, bundle.getString("PLAINTEXT"), passWord);
//                } else if (bundle.getString("SIGN_TYPE").equals("xwvid")) {
//                    byte[] serverCert = null;
//                    // 서버인증서 Pem디코딩
//                    try {
//                        serverCert = KSPem.decode(pemEncodedServerCert);
//                    } catch (KSException e) {
//                        Log.e("raon", "서버인증서 널이다!!!");
//                    }
//
//                    signResult = KSSign.cmsSign(userCert, bundle.getString("PLAINTEXT"), passWord);
//                    vidResult = KSSign.xwVid(userCert, passWord, "", serverCert);
//                }
//
//                if (signResult != null && signResult.length != 0) {
//                    bundle.putString("signResult", new String(KSBase64.encode(signResult)));
//
//                    if (vidResult != null && vidResult.length != 0) {
//                        bundle.putString("vidResult", new String(KSBase64.encode(vidResult)));
//                    }
//
//                    if (userCertPubKey != null && userCertPubKey.length !=0) {
//                        bundle.putString("userCertPubKey", KSPem.encode("CERTIFICATE", userCertPubKey));
//                    }
//                    intent.putExtras(bundle);
//
//                    setResult(RESULT_OK, intent); // 성공했다는 결과값을 보내면서 데이터 꾸러미를 지고 있는 intent를 함께 전달한다.
//                    finish();
//                } else {
//                    errMsg = "값생성에 실패하였습니다.";
//                    handler.sendMessage(handler.obtainMessage(-1));
//                }
//            } else if (bundle.getString("CATEGORY").equals("certcopy") && bundle.getString("FUNCTION").equals("icrs_export")) {
//                if (KSCertificateManager.checkPwd(userCert, passWord)) {
//                    Intent intent = new Intent(KSW_Activity_Pwd.this, KSW_Activity_ICRSExportCert.class);
//                    intent.putExtra("BUNDLE", bundle);
//                    startActivity(intent);
//                    finish();
//                } else {
//                    dialog("비밀번호 확인 실패", "비밀번호가 올바르지 않습니다.").show();
//                }
//            }
//        } catch (KSException e) {
//            if (e.getMessage().equals(Integer.toString(KSException.KS_INVALID_PASSWORD))) {
//                errMsg = "인증서 비밀번호가 일치하지 않습니다.";
//                signResult = null;
//            } else {
//                errMsg = e.getMessage();
//                signResult = null;
//            }
//        }
    }

    public byte[] toByteArray(String hexStr) {
        int len = hexStr.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexStr.charAt(i), 16) << 4) + Character
                    .digit(hexStr.charAt(i + 1), 16));
        }
        return data;
    }

    private int mCount = 0;
    private void processCert() {
        int overCount = ElseUtils.getCertOverCount(this);
        mCount = ElseUtils.getCertCount(this);
        userCert = KSW_CertListManager.getSelectedCert();

        Intent intent = new Intent();
        if (KSCertificateManager.checkBytePwd(userCert, mCipher)) {
            Toast.makeText(this, "인증서 비밀번호가 확인 되었습니다.", Toast.LENGTH_LONG).show();
            IgawAdbrix.firstTimeExperience(AnalyticsName.BankRegComplete);
            ElseUtils.setCertOverTime(this, null);
            ElseUtils.setCertOverCount(this, 0);
            ElseUtils.setCertCount(this, 0);
            intent.putExtra(Const.CERT_NAME, userCert.getPath());
            intent.putExtra(Const.CERT_PWD, passWord);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            mCount++;
            ElseUtils.setCertCount(this, mCount);
            if (mCount >= PASSWORD_ERROR_MAX_COUNT) {
                overCount++;
                if (overCount >= 2) {
                    ElseUtils.setCertOverCount(this, overCount);
                    ElseUtils.setCertCount(this, 0);
                    showPopup(getString(R.string.common_warning_cert_pwd_mismatch_overtime_title), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which){
                            ActivityCompat.finishAffinity(CertPasswordActivity.this);
                        }
                    });
                } else {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                    calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                    long time = calendar.getTimeInMillis();
                    ElseUtils.setCertOverTime(this, String.valueOf(time));
                    ElseUtils.setCertOverCount(this, overCount);
                    ElseUtils.setCertCount(this, 0);
                    showPopup(getString(R.string.common_warning_cert_pwd_mismatch_title) + " 당일 사용이 제한됩니다.", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.finishAffinity(CertPasswordActivity.this);
                        }
                    });
                }
            } else {
                showPopup(getString(R.string.common_warning_cert_pwd_mismatch_title)+" 5회(현재 "+(5-mCount)+"회 남음) 이상 오류 시 사용이 제한될 수 있습니다."
                        , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }
            return;
        }
    }

    private void enterMain() {
        ActivityCompat.finishAffinity(CertPasswordActivity.this);
        Intent intent = new Intent(this, LoadingActivity.class);
        intent.putExtra(Const.FROM_MEMBERSHIP, true);
        startActivity(intent);
    }

    private void showPopup(String str, DialogInterface.OnClickListener dialog) {
        DialogUtils.showDlgBaseOneButtonNoTitle(this, dialog, new InfoDataDialog(R.layout.dialog_inc_case_1, str, "확인"));
    }

    public void onBtnClickNaviTxtTxtTxtBar(View view) {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_navi_txt_txt_txt_back:
                onBackPressed();
                break;
            case R.id.btn_membership_skip:
                enterMain();
                break;
            case R.id.btn_fragment_membership_next:
                mTransKey.done();
                processCert();
                break;
            case R.id.ll_edittext_click_layout:
            case R.id.ll_cert_input_layout:
            case R.id.keyscroll:
                if (!isTransKeyShowing) {
                    onShowKeyboard(TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);
                }
                break;
        }
    }

    TransKeyCipher mTkc = new TransKeyCipher("SEED");

    @Override
    public void done(Intent data) {
        isTransKeyShowing = false;
        String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
        String cipherDataex = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA_EX);
        String cipherDataexp = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA_EX_PADDING);
        String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);

        byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
        int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

        if (iRealDataLength == 0)
            return;

        StringBuffer plainData = null;
        String cipherStr = null;
        try {
            mTkc.setSecureKey(secureKey);
            mSecureKey = secureKey;
            APPSharedPrefer.getInstance(this).setPrefString(APP.PUBLIC_KEY, toHexString(secureKey));
            cipherStr = mTkc.getPBKDF2DataEncryptCipherData(cipherData);
// 			String test1 = mTkc.getPBKDF2DataEncryptCipherDataEx(cipherDataex);
//			String test2 = mTkc.getPBKDF2DataEncryptCipherDataExWithPadding(cipherDataexp);

            byte pbPlainData[] = new byte[iRealDataLength];
            if (mTkc.getDecryptCipherData(cipherData, pbPlainData)) {
//                plainData = new StringBuffer(new String(pbPlainData));
                mCipher = pbPlainData.clone();
                for(int i=0;i<pbPlainData.length;i++)
                    pbPlainData[i]=0x01;
            } else {
                // 복호화 실패
                plainData = new StringBuffer("plainData decode fail...");
            }

        } catch (Exception e) {
            if(Global.debug) Log.d("STACKTRACE", e.getStackTrace().toString());
        }

        passWord = cipherStr;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public String toHexString(byte[] bytes) {
        char[] hexChar = new char[bytes.length *2];
        for (int i=0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            hexChar[i * 2] = hexArray[v >>> 4];
            hexChar[i * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChar);
    }

    @Override
    public void cancel(Intent data) {
        isTransKeyShowing = false;
        mTransKey.ClearAllData();
    }

    @Override
    public void input(int type) {
        if (mTransKey.getInputLength() >= PASSWORD_MINIMUM_LENGTH) {
            mBtn.setEnabled(true);
        } else {
            mBtn.setEnabled(false);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
