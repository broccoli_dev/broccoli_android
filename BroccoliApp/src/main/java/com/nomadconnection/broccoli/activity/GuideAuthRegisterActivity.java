package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;

/**
 * Created by YelloHyunminJang on 16. 9. 20..
 */
public class GuideAuthRegisterActivity extends BaseActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_register_guide);
        sendTracker(AnalyticsName.GuideAuthRegister);
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.guide_auth_register));
        findViewById(R.id.ll_guide_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_guide_btn:
                showGuide();
                break;
        }
    }

    public void onBtnClickNaviImgTxtBar(View v) {
        onBackPressed();
    }

    private void showGuide() {
        Intent intent = new Intent(this, GuideWebActivity.class);
        intent.putExtra(Const.DATA, "http://blog.naver.com/my_broccoli/220806234616");
        startActivity(intent);
    }
}
