package com.nomadconnection.broccoli.activity.membership;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;

/**
 * Created by YelloHyunminJang on 2017. 1. 6..
 */

public class MembershipIdHintActivity extends BaseActivity {

    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_email_hint);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.FindEmailResult);
        findViewById(R.id.ll_navi_img_txt_back).setVisibility(View.GONE);
        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText("아이디 찾기");
        Intent intent = getIntent();
        String name = null;
        String email = null;
        if (intent != null) {
            name = intent.getStringExtra(NAME);
            email = intent.getStringExtra(EMAIL);
        }

        TextView nameView = (TextView) findViewById(R.id.tv_activity_membership_name);
        TextView emailView = (TextView) findViewById(R.id.tv_activity_membership_email);
        nameView.setText(name);
        emailView.setText(email);
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
