package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.common.dialog.CommonDialogRadioType;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetCarInfo;
import com.nomadconnection.broccoli.data.Asset.AssetEstateInfo;
import com.nomadconnection.broccoli.data.Asset.AssetEtcInfo;
import com.nomadconnection.broccoli.data.Asset.AssetOtherInfo;
import com.nomadconnection.broccoli.data.Asset.EditAssetCar;
import com.nomadconnection.broccoli.data.Asset.EditAssetEstate;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.fragment.FragmentAssetOther0A;
import com.nomadconnection.broccoli.fragment.FragmentAssetOther0B;
import com.nomadconnection.broccoli.fragment.FragmentAssetOtherCustom;
import com.nomadconnection.broccoli.fragment.FragmentAssetOtherListView;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.interf.InterfaceFragmentComplete;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level2ActivityAssetOther extends BaseActivity implements InterfaceEditOrDetail {

	private static final String TAG = Level2ActivityAssetOther.class.getSimpleName();
	private static final String TAG0A = FragmentAssetOther0A.class.getSimpleName();
	private static final String TAG0B = FragmentAssetOther0B.class.getSimpleName();
	private static final String TAG0C = FragmentAssetOtherCustom.class.getSimpleName();
	private static final String TAG1 = FragmentAssetOtherListView.class.getSimpleName();

	public enum EtcType {
		HOUSE,
		CAR,
		CUSTOM
	}

	public int mCurrentFragmentIndex 	= 0;		// default
	private final int FRAGMENT_ONE 			= 0;		// 추가
	private final int FRAGMENT_TWO 			= 1;		// 상세
	private static EtcType mType = EtcType.CAR;

	/* getParcelableExtra */
	private ArrayList<AssetEstateInfo> mAlInfoDataListHouse;
	private ArrayList<AssetCarInfo> mAlInfoDataListCar;
	private ArrayList<AssetEtcInfo> mAlInfoDataListEtc;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_asset_other);
		sendTracker(AnalyticsName.Level2ActivityAssetOther);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onBackPressed() {
		onBtnClickNaviMotion(findViewById(R.id.ll_navi_motion_back));
//		super.onBackPressed();
	}

	
	
	

//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try
		{
			switch (_view.getId()) {
				case R.id.tv_navi_motion_title:
					if (mCurrentFragmentIndex == 0)
						break;

				case R.id.ll_navi_motion_back:
					if (mCurrentFragmentIndex == 1) goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_MAIN_1_RESULT_CODE_SUCCESS);
					else if (mCurrentFragmentIndex == 0) {
						String mStr = "기타 재산 등록\n취소하시겠습니까?";
						DialogUtils.showDlgBaseTwoButton(Level2ActivityAssetOther.this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
											mCurrentFragmentIndex = 1; fragmentReplace(mCurrentFragmentIndex, "out_right"); afterMotion("back");
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, "아니오", "예", mStr));
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentFragmentIndex == 1) {
						CommonDialogRadioType radioType = new CommonDialogRadioType(getBase(), "기타 재산 등록", new String[] {"부동산", "차량", "기타"});
						radioType.setOnDismissListener(new DialogInterface.OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface dialog) {
								if (mCurrentFragmentIndex == 1) {
									if (mAlInfoDataListHouse.size() == 0 && mAlInfoDataListCar.size() == 0 && mAlInfoDataListEtc.size() == 0)
										goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_MAIN_1_RESULT_CODE_SUCCESS);
								}
							}
						});
						radioType.setDialogListener(new CommonDialogRadioType.DialogRadioTypeListener() {
							@Override
							public void onClick(DialogInterface dialog, int checkedresid, int position, Object select) {
								switch (position) {
									case 0:
										mType = EtcType.HOUSE;
										break;
									case 1:
										mType = EtcType.CAR;
										break;
									case 2:
										mType = EtcType.CUSTOM;
										break;
								}
								dialog.dismiss();
								mCurrentFragmentIndex = 0;
								fragmentReplace(mCurrentFragmentIndex, "in_right");
								afterMotion("else");
							}
						});
						radioType.show();
					} else if (mCurrentFragmentIndex == 0) {
						if (mType == EtcType.HOUSE){
							if (checkValueIsFailHouse()) return;
							DialogUtils.showLoading(Level2ActivityAssetOther.this);											// 자산 로딩
							Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).editEstate(
									new EditAssetEstate(Session.getInstance(this).getUserId(), "0",
											FragmentAssetOther0A.mHouseInfo0.getText().toString(), TransFormatUtils.transEstateTypeNameToPos(FragmentAssetOther0A.mHouseInfo1.getText().toString()),
											TransFormatUtils.transDealTypeNameToPos(FragmentAssetOther0A.mHouseInfo2.getText().toString()), TransFormatUtils.transRemoveComma(FragmentAssetOther0A.mHouseInfo3.getText().toString()))
							);
							mCall.enqueue(new Callback<Result>() {
								@Override
								public void onResponse(Call<Result> call, Response<Result> response) {
									DialogUtils.dismissLoading();											// 자산 로딩
									if (response.isSuccessful() && response.body() != null) {
										NextStepAddHouse(response.body());
									} else {
										ElseUtils.network_error(Level2ActivityAssetOther.this);
									}
								}

								@Override
								public void onFailure(Call<Result> call, Throwable t) {
									DialogUtils.dismissLoading();											// 자산 로딩
									ElseUtils.network_error(Level2ActivityAssetOther.this);
								}
							});
						} else if (mType == EtcType.CUSTOM) {
							sendComplete();
						} else {
							if (checkValueIsFailCar()) return;
							DialogUtils.showLoading(Level2ActivityAssetOther.this);											// 자산 로딩
							Call<Result> mCall = ServiceGenerator.createService(AssetApi.class).editCar(
									new EditAssetCar(Session.getInstance(this).getUserId(), "0",
											FragmentAssetOther0B.mCarInfo1.getText().toString(), FragmentAssetOther0B.mCarInfo2.getText().toString(),
											FragmentAssetOther0B.mCarInfo3.getText().toString(), FragmentAssetOther0B.mCarInfo4.getText().toString())
							);
							mCall.enqueue(new Callback<Result>() {
								@Override
								public void onResponse(Call<Result> call, Response<Result> response) {
									DialogUtils.dismissLoading();											// 자산 로딩
									if (response.isSuccessful() && response.body() != null) {
										NextStepAddCar(response.body());
									} else {
										ElseUtils.network_error(Level2ActivityAssetOther.this);
									}
								}

								@Override
								public void onFailure(Call<Result> call, Throwable t) {
									DialogUtils.dismissLoading();											// 자산 로딩
									ElseUtils.network_error(Level2ActivityAssetOther.this);
								}
							});
						}
					}
					break;

				default:
					break;
			}
		} catch(Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}




	private void afterMotion(String _Divider){
		if(_Divider.equals("else")){
			if (mType == EtcType.HOUSE) ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG0A).mTitle);
			else if (mType == EtcType.CAR) ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG0B).mTitle);
			else if (mType == EtcType.CUSTOM) ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG0C).mTitle);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("완료");
			complete_button_enable_disable(false);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
		} else {	// default
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG1).mTitle);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("추가");
			complete_button_enable_disable(true);
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
		}
	}







//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			/* getParcelableExtra */
			mAlInfoDataListHouse = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_ASSET_OTHER_HOUSE_DEPTH_1);
			mAlInfoDataListCar = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_ASSET_OTHER_CAR_DEPTH_1);
			mAlInfoDataListEtc = getIntent().getParcelableArrayListExtra(APP.INFO_DATA_ASSET_OTHER_DEPTH_1);

			if(mAlInfoDataListHouse == null) mAlInfoDataListHouse = new ArrayList<AssetEstateInfo>();
			if(mAlInfoDataListCar == null) mAlInfoDataListCar = new ArrayList<AssetCarInfo>();
			if(mAlInfoDataListEtc == null) mAlInfoDataListEtc = new ArrayList<>();

			if(mAlInfoDataListHouse == null && mAlInfoDataListCar == null && mAlInfoDataListEtc == null){
				bResult = false;
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG1).mTitle);
			if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) ((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText("추가");
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);

			/* Layout */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCurrentFragmentIndex = 1;
			fragmentReplace(mCurrentFragmentIndex, "");


		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// Fragment Navi
//=========================================================================================//
	/** Fragment 변경 **/
	private void fragmentReplace(int reqNewFragmentIndex, String _Anim) {
		try {
			Fragment newFragment = null;
			newFragment = getFragment(reqNewFragmentIndex);

			// replace fragment
			final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			if (_Anim.length() != 0 && _Anim.equals("in_right")) transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out);
			if (_Anim.length() != 0 && _Anim.equals("out_right")) transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
			transaction.replace(R.id.fl_level_2_activity_asset_other_container, newFragment);

			// Commit the transaction
			transaction.commitAllowingStateLoss();

		} catch (Exception e) {

		}
	}

	/** Fragment Get **/
	private Fragment getFragment(int idx) {
		Fragment newFragment = null;
		Bundle bundle = null;

		switch (idx) {
			case FRAGMENT_ONE:	// 추가/편집
				switch (mType) {
					case HOUSE:
						newFragment = new FragmentAssetOther0A();
						break;
					case CAR:
						newFragment = new FragmentAssetOther0B();
						break;
					case CUSTOM:
						newFragment = new FragmentAssetOtherCustom();
						break;
				}
				break;

			case FRAGMENT_TWO:	// 상세
				bundle = new Bundle();
				newFragment = new FragmentAssetOtherListView();
				bundle.putParcelableArrayList(FragmentAssetOtherListView.ESTATE_DATA , mAlInfoDataListHouse);
				bundle.putParcelableArrayList(FragmentAssetOtherListView.CAR_DATA , mAlInfoDataListCar);
				bundle.putParcelableArrayList(FragmentAssetOtherListView.CUSTOM_DATA , mAlInfoDataListEtc);
				newFragment.setArguments(bundle);
				break;

			default:
				break;
		}

		return newFragment;
	}

	private void sendComplete() {
		for (Fragment frag : getSupportFragmentManager().getFragments()) {
			if (frag != null) {
				InterfaceFragmentComplete fragment = (InterfaceFragmentComplete) frag;
				if (fragment != null) {
					fragment.complete();
				}
			}
		}
	}


//===============================================================================//
// Value Check
//===============================================================================//
	/**  Value check House **/
	private Boolean checkValueIsFailHouse(){
		boolean mResult = false;

		if (FragmentAssetOther0A.mHouseInfo0.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetOther.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetOther0A.mHouseInfo1.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetOther.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetOther0A.mHouseInfo2.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetOther.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetOther0A.mHouseInfo3.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetOther.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}

		return  mResult;
	}

	/**  Value check Car **/
	private Boolean checkValueIsFailCar(){
		boolean mResult = false;

		if (FragmentAssetOther0B.mCarInfo1.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetOther.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetOther0B.mCarInfo2.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetOther.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetOther0B.mCarInfo3.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetOther.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}
		else if (FragmentAssetOther0B.mCarInfo3.getText().toString().trim().length() == 0){
			Toast.makeText(Level2ActivityAssetOther.this, getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
			mResult = true;
		}

		return  mResult;
	}

//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep Add House **/
	private void NextStepAddHouse(Result result){
		if (result.getCode().equals("100")){
			refreshData();
		}
	}

	/**  NextStep Add Car **/
	private void NextStepAddCar(Result _Result){
		if (_Result.getCode().equals("100")){
			refreshData();
		}
	}





//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int _resultCod){
		Intent intent = getIntent();
		setResult(_resultCod, intent);
		finish();
	}


	private void refreshData(){
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(this).getUserId());

		DialogUtils.showLoading(Level2ActivityAssetOther.this);											// 자산 로딩
		Call<AssetOtherInfo> mCall = ServiceGenerator.createService(AssetApi.class).getOtherList(mRequestDataByUserId);
		mCall.enqueue(new Callback<AssetOtherInfo>() {
			@Override
			public void onResponse(Call<AssetOtherInfo> call, Response<AssetOtherInfo> response) {
				DialogUtils.dismissLoading();                                            // 자산 로딩
				AssetOtherInfo mTemp = response.body();
				mAlInfoDataListHouse.removeAll(mAlInfoDataListHouse);
				mAlInfoDataListCar.removeAll(mAlInfoDataListCar);
				mAlInfoDataListEtc.removeAll(mAlInfoDataListEtc);
				mAlInfoDataListHouse.addAll(mTemp.mMainAssetRealEstate);
				mAlInfoDataListCar.addAll(mTemp.mMainAssetCar);
				mAlInfoDataListEtc.addAll(mTemp.mMainAssetEtc);
				mCurrentFragmentIndex = 1;
				fragmentReplace(mCurrentFragmentIndex, "out_right");
				afterMotion("back");
			}

			@Override
			public void onFailure(Call<AssetOtherInfo> call, Throwable t) {
				DialogUtils.dismissLoading();                                            // 자산 로딩
				ElseUtils.network_error(Level2ActivityAssetOther.this);
			}
		});
	}



//=========================================================================================//
// Interface
//=========================================================================================//
	@Override
	public void onEdit() {
//		mCurrentFragmentIndex = 0;
//		fragmentReplace(mCurrentFragmentIndex, "");
		complete_button_enable_disable(true);
	}

	@Override
	public void onDetail() {
		mCurrentFragmentIndex = 1;
		fragmentReplace(mCurrentFragmentIndex, "");
	}

	@Override
	public void onError() {
		complete_button_enable_disable(false);
	}

	@Override
	public void onRefresh() {
		refreshData();
	}

	private void complete_button_enable_disable(boolean value){
		if(value){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}
}
