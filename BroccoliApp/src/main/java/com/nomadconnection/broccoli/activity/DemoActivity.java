package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.membership.LoginActivity;
import com.nomadconnection.broccoli.adapter.PagerAdapter;
import com.nomadconnection.broccoli.common.CustomTabView;
import com.nomadconnection.broccoli.common.SmartTabLayout;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogCustomType;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.fragment.demo.FragmentDemoAsset;
import com.nomadconnection.broccoli.fragment.demo.FragmentDemoEtc;
import com.nomadconnection.broccoli.fragment.demo.FragmentDemoSpend;
import com.nomadconnection.broccoli.fragment.demo.FragmentDemoStock;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.utils.DialogUtils;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2017. 3. 31..
 */

public class DemoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;
    private FrameLayout mTabParent;
    private SmartTabLayout tabs;
    private PagerAdapter mMainPagerAdapter;
    private ViewPager mViewPager;
    private View mHeader;
    private View mPopupGuide;
    private int mCurrentPageInx;
    private TextView mGuideText;
    private NavigationView mNavigationView;
    private ImageButton mFloatBtn;

    /* Pager Title */
    private String[] mPagerTitle;
    private ArrayList<Fragment> alFragment;
    private Handler mColorChangeHandler = new Handler() {

        @Override
        public void dispatchMessage(Message msg) {
            pageScrolling(msg.what, msg.arg1);
        }
    };
    private int mAge = 10;
    private String mGender = "M";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        init();
    }

    private void init() {
        initWidget();
        initData();
        showWelcomePopup();
    }

    private void initWidget() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mHeader = findViewById(R.id.navi_title);
        mGuideText = (TextView) mHeader.findViewById(R.id.main_loading_scraping_time);
        mPopupGuide = findViewById(R.id.demo_popup_guide_text);

        /* PagerFragment Tab Items */
        mPagerTitle = getResources().getStringArray(R.array.pager_fragment_tab_items);

        Intent intent = getIntent();
        mAge = intent.getIntExtra(Const.DEMO_AGE, 10);
        mGender = intent.getStringExtra(Const.DEMO_GENDER);

        Bundle bundle = new Bundle();
        bundle.putInt(Const.DEMO_AGE, mAge);
        bundle.putString(Const.DEMO_GENDER, mGender);
        Fragment fragment = null;

        alFragment = new ArrayList<Fragment>();
        fragment = new FragmentDemoAsset();
        fragment.setArguments(bundle);
        alFragment.add(fragment);
        fragment = new FragmentDemoSpend();
        fragment.setArguments(bundle);
        alFragment.add(fragment);
        fragment = new FragmentDemoStock();
        fragment.setArguments(bundle);
        alFragment.add(fragment);
        fragment = new FragmentDemoEtc();
        fragment.setArguments(bundle);
        alFragment.add(fragment);


        /* Pager */
        mMainPagerAdapter = new PagerAdapter(getSupportFragmentManager(), alFragment, mPagerTitle);
        mViewPager = (ViewPager) findViewById(R.id.pager_body);
        mViewPager.setAdapter(mMainPagerAdapter);
        mViewPager.setOffscreenPageLimit(alFragment.size());
        mViewPager.addOnPageChangeListener(mPageChangeListener);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) mViewPager.getLayoutParams();
            mViewPager.setLayoutParams(params);
        }

        mTabParent = (FrameLayout) findViewById(R.id.tabs_parent);
        tabs = (SmartTabLayout) findViewById(R.id.tabs);
        tabs.setCustomTabView(new SmartTabLayout.TabProvider() {
            @Override
            public View createTabView(ViewGroup container, int position, android.support.v4.view.PagerAdapter adapter) {
                CustomTabView customTabView = new CustomTabView(DemoActivity.this);
                customTabView.createTabView(container, position, adapter);
                return customTabView;
            }
        });
        tabs.setOnPageChangeListener(mPageChangeListener);
        tabs.setViewPager(mViewPager);
        for (int i = 0; i < tabs.getTabCount(); i++) {
            CustomTabView tab = (CustomTabView) tabs.getTabAt(i);
            if (i == 0) {
                tab.getTabView().setEnabled(true);
            }
            ViewGroup layout = (ViewGroup) tab.getParent();
            layout.setBackground(null);
        }
        tabs.setOnTabClickListener(new SmartTabLayout.OnTabClickListener() {
            @Override
            public void onTabClicked(int position) {
                if (DemoActivity.this != null) {
                    switch (position) {
                        case 0:
                            sendTracker(AnalyticsName.DemoAsset);
                            break;
                        case 1:
                            sendTracker(AnalyticsName.DemoSpend);
                            break;
                        case 2:
                            sendTracker(AnalyticsName.DemoInvest);
                            break;
                        case 3:
                            sendTracker(AnalyticsName.DemoEtc);
                            break;
                        default:
                            break;
                    }
                }
                mViewPager.setCurrentItem(position, false);
                CustomTabView tab = (CustomTabView) tabs.getTabAt(position);
                if (tab != null) {
                    tab.getTabView().setEnabled(true);
                }
                Fragment frg = mMainPagerAdapter.getItem(position);
                frg.onResume();

                if (frg != null)
                    ((InterfaceFragmentInteraction) frg).reset(true);
            }

            @Override
            public void onTabReSelected(int position) {

            }

            @Override
            public void onTabUnSelected(int position) {
                CustomTabView tab = (CustomTabView) tabs.getTabAt(position);
                if (tab != null) {
                    tab.getTabView().setEnabled(false);
                }
                Fragment frg = mMainPagerAdapter.getItem(position);

                if (frg != null)
                    ((InterfaceFragmentInteraction) frg).reset(true);
            }
        });
        mHeader.setAlpha(0.9f);
        mTabParent.setAlpha(0.9f);

        findViewById(R.id.btn_pop_menu).setOnClickListener(onClickListener);
        findViewById(R.id.btn_close).setOnClickListener(onClickListener);

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        mFloatBtn = (ImageButton) findViewById(R.id.fab_demo_start_btn);
        mFloatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DemoActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                DemoActivity.this.startActivity(intent);
                DemoActivity.this.finish();
            }
        });
    }

    private void initData() {

        setNavigationView(mNavigationView);

        String guide = String.valueOf(mAge) + "대 " + (mGender.equalsIgnoreCase("M") ? getResources().getString(R.string.demo_male) : getResources().getString(R.string.demo_female));
        guide += getResources().getString(R.string.demo_financial_guide);
        mGuideText.setText(guide);
        mGuideText.setVisibility(View.VISIBLE);
    }

    public void onBtnClickNaviMotion(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_motion_else:
                break;
            case R.id.ll_navi_motion_back:
                onBackPressed();
                break;
        }
    }

    private void pageScrolling(int _Page, int color) {
        if (color != 0) {
            mDrawerLayout.setBackgroundColor(color);
            mHeader.setBackgroundColor(color);
            mTabParent.setBackgroundColor(color);
            for (int i = 0; i < mViewPager.getChildCount(); i++) {
                mViewPager.getChildAt(i).setBackgroundColor(color);
            }
        }
    }

    private void setNavigationView(NavigationView view) {
        View header = view.getHeaderView(0);

        TextView name = (TextView) findViewById(R.id.tv_main_name);
        TextView email = (TextView) findViewById(R.id.tv_main_email);

        name.setText(String.valueOf(mAge) + "대 " + (mGender.equalsIgnoreCase("M") ? getResources().getString(R.string.demo_male) : getResources().getString(R.string.demo_female)));

        View demoChange = header.findViewById(R.id.ll_demo_mode_change);
        View demoFinish = header.findViewById(R.id.ll_demo_finish);

        demoChange.setOnClickListener(mBtnClickListener);
        demoFinish.setOnClickListener(mBtnClickListener);
    }

    private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int _position) {
            mCurrentPageInx = _position;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            final float[] from = new float[3];
            final float[] to = new float[3];
            final float[] hsv = new float[3];                  // transition color
            if (position == 0) {
                Color.colorToHSV((getResources().getColor(R.color.main_1_layout_bg)), from);   // from
                Color.colorToHSV((getResources().getColor(R.color.main_2_layout_bg)), to);     // to
            } else if (position == 1) {
                Color.colorToHSV((getResources().getColor(R.color.main_2_layout_bg)), from);   // from
                Color.colorToHSV((getResources().getColor(R.color.main_3_layout_bg)), to);     // to
            } else if (position == 2) {
                Color.colorToHSV((getResources().getColor(R.color.main_3_layout_bg)), from);   // from
                Color.colorToHSV((getResources().getColor(R.color.main_4_layout_bg)), to);     // to
            } else if (position == 3) {
                Color.colorToHSV((getResources().getColor(R.color.main_4_layout_bg)), from);   // from
                Color.colorToHSV((getResources().getColor(R.color.main_4_layout_bg)), to);     // to
            }

            hsv[0] = from[0] + (to[0] - from[0]) * positionOffset;
            hsv[1] = from[1] + (to[1] - from[1]) * positionOffset;
            hsv[2] = from[2] + (to[2] - from[2]) * positionOffset;

            Message mMsg = new Message();
            mMsg.what = position;
            mMsg.arg1 = Color.HSVToColor(hsv);
            mColorChangeHandler.sendMessage(mMsg);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            int position = tabs.getSelectedTabPosition();
            CustomTabView tab = (CustomTabView) tabs.getTabAt(position);
            switch (state) {
                case ViewPager.SCROLL_STATE_IDLE:
                    tab.getTabView().setEnabled(true);
                    break;
                default:
                    tab.getTabView().setEnabled(false);
                    break;
            }
        }
    };

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_pop_menu:
                    DialogUtils.showDialogDemoFinish(DemoActivity.this);
                    break;

                case R.id.btn_close:
                    sendTracker(AnalyticsName.DemoSetting);
                    mDrawerLayout.openDrawer(GravityCompat.END);
                    break;

                default:
                    break;
            }
        }
    };

    private View.OnClickListener mBtnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = null;
            switch (v.getId()) {
                case R.id.ll_demo_mode_change:
                    intent = new Intent(DemoActivity.this, DemoSettingActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;
                case R.id.ll_demo_finish:
                    intent = new Intent(DemoActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                    break;
                default:
                    break;
            }

        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.END);
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            mAge = intent.getIntExtra(Const.DEMO_AGE, 10);
            mGender = intent.getStringExtra(Const.DEMO_GENDER);

            Bundle bundle = new Bundle();
            bundle.putInt(Const.DEMO_AGE, mAge);
            bundle.putString(Const.DEMO_GENDER, mGender);
            Fragment fragment = null;

            alFragment.clear();
            alFragment = new ArrayList<Fragment>();
            fragment = new FragmentDemoAsset();
            fragment.setArguments(bundle);
            alFragment.add(fragment);
            fragment = new FragmentDemoSpend();
            fragment.setArguments(bundle);
            alFragment.add(fragment);
            fragment = new FragmentDemoStock();
            fragment.setArguments(bundle);
            alFragment.add(fragment);
            fragment = new FragmentDemoEtc();
            fragment.setArguments(bundle);
            alFragment.add(fragment);

            mMainPagerAdapter = new PagerAdapter(getSupportFragmentManager(), alFragment, mPagerTitle);
            mViewPager = (ViewPager) findViewById(R.id.pager_body);
            mViewPager.setAdapter(mMainPagerAdapter);
            mViewPager.setOffscreenPageLimit(alFragment.size());
            mViewPager.addOnPageChangeListener(mPageChangeListener);
            mMainPagerAdapter.notifyDataSetChanged();

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) mViewPager.getLayoutParams();
                mViewPager.setLayoutParams(params);
            }

            mTabParent = (FrameLayout) findViewById(R.id.tabs_parent);
            tabs = (SmartTabLayout) findViewById(R.id.tabs);
            tabs.setCustomTabView(new SmartTabLayout.TabProvider() {
                @Override
                public View createTabView(ViewGroup container, int position, android.support.v4.view.PagerAdapter adapter) {
                    CustomTabView customTabView = new CustomTabView(DemoActivity.this);
                    customTabView.createTabView(container, position, adapter);
                    return customTabView;
                }
            });
            tabs.setOnPageChangeListener(mPageChangeListener);
            tabs.setViewPager(mViewPager);
            for (int i = 0; i < tabs.getTabCount(); i++) {
                CustomTabView tab = (CustomTabView) tabs.getTabAt(i);
                if (i == 0) {
                    tab.getTabView().setEnabled(true);
                }
                ViewGroup layout = (ViewGroup) tab.getParent();
                layout.setBackground(null);
            }
            tabs.setOnTabClickListener(new SmartTabLayout.OnTabClickListener() {
                @Override
                public void onTabClicked(int position) {
                    mViewPager.setCurrentItem(position, false);
                    CustomTabView tab = (CustomTabView) tabs.getTabAt(position);
                    if (tab != null) {
                        if (tab.getTabView() != null) {
                            tab.getTabView().setEnabled(true);
                        }
                    }
                    Fragment frg = mMainPagerAdapter.getItem(position);
                    frg.onResume();

                    if (frg != null)
                        ((InterfaceFragmentInteraction) frg).reset(true);
                }

                @Override
                public void onTabReSelected(int position) {

                }

                @Override
                public void onTabUnSelected(int position) {
                    CustomTabView tab = (CustomTabView) tabs.getTabAt(position);
                    if (tab != null) {
                        if (tab.getTabView() != null) {
                            tab.getTabView().setEnabled(false);
                        }
                    }
                    Fragment frg = mMainPagerAdapter.getItem(position);

                    if (frg != null)
                        ((InterfaceFragmentInteraction) frg).reset(true);
                }
            });

            findViewById(R.id.btn_pop_menu).setOnClickListener(onClickListener);
            findViewById(R.id.btn_close).setOnClickListener(onClickListener);

            mNavigationView = (NavigationView) findViewById(R.id.nav_view);
            mNavigationView.setNavigationItemSelectedListener(this);

            initData();
        }
    }

    public void sendTracker(String name) {
        Tracker tracker = MainApplication.get().getDefaultTracker();
        tracker.setScreenName(name);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void showWelcomePopup() {
        if (!this.isFinishing()) {
            int iconRes = 0;
            if (mGender.equalsIgnoreCase("M")) {
                switch (mAge) {
                    case 10:
                        iconRes = R.drawable.icon_face_10_m;
                        break;
                    case 20:
                    case 30:
                        iconRes = R.drawable.icon_face_2030_m;
                        break;
                    case 40:
                    case 50:
                        iconRes = R.drawable.icon_face_4050_m;
                        break;
                    case 60:
                        iconRes = R.drawable.icon_face_60_m;
                        break;
                }
            } else {
                switch (mAge) {
                    case 10:
                        iconRes = R.drawable.icon_face_10_f;
                        break;
                    case 20:
                    case 30:
                        iconRes = R.drawable.icon_face_2030_f;
                        break;
                    case 40:
                    case 50:
                        iconRes = R.drawable.icon_face_4050_f;
                        break;
                    case 60:
                        iconRes = R.drawable.icon_face_60_f;
                        break;
                }
            }
            String age = String.valueOf(mAge);
            String gender = mGender.equalsIgnoreCase("M") ? getResources().getString(R.string.demo_male) : getResources().getString(R.string.demo_female);
            String title = String.format(getString(R.string.demo_welcome_dialog_title), age, gender);
            String content = String.format(getString(R.string.demo_welcome_dialog_content), age, gender);
            View body = getLayoutInflater().inflate(R.layout.custom_dialog_demo_welcome, null);
            ImageView iconView = (ImageView) body.findViewById(R.id.iv_demo_welcome_dialog_icon);
            iconView.setImageResource(iconRes);
            TextView contentView = (TextView) body.findViewById(R.id.tv_demo_welcome_dialog_title);
            contentView.setText(content);
            CommonDialogCustomType dialogCustomType = new CommonDialogCustomType(this, title, new int[]{R.string.demo_welcome_dialog_btn});
            dialogCustomType.addCustomBody(body);
            dialogCustomType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            dialogCustomType.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    mPopupGuide.setVisibility(View.VISIBLE);
                    mPopupGuide.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mPopupGuide.setVisibility(View.GONE);
                        }
                    }, 3000);
                }
            });
            dialogCustomType.show();
        }
    }

}
