package com.nomadconnection.broccoli.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.igaworks.adbrix.IgawAdbrix;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.service.BatchService;
import com.nomadconnection.broccoli.service.ScrapingModule;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.utils.ElseUtils;

/**
 * Created by YelloHyunminJang on 16. 1. 31..
 */
public class LoadingActivity extends BaseActivity implements View.OnClickListener{


    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private ProcessMode mMode = ProcessMode.COLLECT_IDLE;
    private boolean isConnection = false;
    private boolean isResponse = false;
    private boolean isScrapingFinish = false;
    private boolean fromMembership = false;
    private boolean endTutorial = false;
    private Messenger			mService;
    private ServiceConnection 	mServiceConnection;
    private TextView mStatus;
    private TextView mNotice;
    private Handler mHandler = new Handler();
    private Runnable slideRunnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(LoadingActivity.this, Intro.class);
            startActivityForResult(intent, 0);
            isResponse = true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        sendTracker(AnalyticsName.LoadingActivity);
        mStatus = (TextView) findViewById(R.id.tv_activity_loading_guide_1);
        mStatus.setVisibility(View.GONE);
        mNotice = (TextView) findViewById(R.id.tv_activity_loading_guide_2);
        mNotice.setVisibility(View.GONE);
        ImageView loading = (ImageView) findViewById(R.id.loading_img);
        AnimationDrawable frameAnimation = (AnimationDrawable) loading.getDrawable();
        frameAnimation.setCallback(loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        startLogin();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy(){
        finishBinderService();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            endTutorial = true;
            if(isScrapingFinish){
                startAnimation();
                mStatus.setText(getString(R.string.loading_status_complete));
            }
            else {
                mStatus.setText(getString(R.string.loading_guide_str2));
            }
        }
    }

    private void startLogin() {
        Intent intent = getIntent();
        fromMembership = intent.getBooleanExtra(Const.FROM_MEMBERSHIP, false);
        if (fromMembership) {
            initBinderService();
        } else {
            initBinderService();
        }
    }

    private void initBinderService(){
        mServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
                isConnection = false;
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
//				mService = ((CustomBinder) service).getService();
                isConnection = true;
                mService = new Messenger(service);
                sendUploadStatusMsg();
            }
        };

        if(ElseUtils.isServiceRunning(this, BatchService.class.getName()) == false) {
            ComponentName compName = new ComponentName(getPackageName(), BatchService.class.getName());
            startService(new Intent().setComponent(compName));
        }
        bindService(new Intent(this, BatchService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void sendUploadStatusMsg() {
        if (isConnection) {
            try {
                Message msg = Message.obtain(null,
                        BatchService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

                // Give it some value as an example.
                msg = Message.obtain(null,
                        BatchService.MSG_UPLOAD_STATUS, this.hashCode(), 0);
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /** Binder Service End **/
    private void finishBinderService(){
        if (isConnection) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            BatchService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            unbindService(mServiceConnection);
            isConnection = false;
        }
    }

    private void startAnimation(){
        if (endTutorial && isScrapingFinish) {
            ImageView loading = (ImageView) findViewById(R.id.loading_img);
            loading.setImageDrawable(getResources().getDrawable(R.drawable.scraping_complete));
            AnimationDrawable frameAnimation = (AnimationDrawable) loading.getDrawable();
            frameAnimation.setCallback(loading);
            frameAnimation.setVisible(true, true);
            int duration = frameAnimation.getDuration(0);
            int frames = frameAnimation.getNumberOfFrames();
            long time = duration * frames;
            frameAnimation.start();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finishLoading();
                }
            }, time);
        }
    }

    private void finishLoading() {
        IgawAdbrix.firstTimeExperience(AnalyticsName.TutorialComplete);
        Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
        if (fromMembership) {
            intent.putExtra(Const.FROM_MEMBERSHIP, true);
        }
        startActivity(intent);
        finish();
    }

    class IncomingHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case BatchService.MSG_UPLOAD_STATUS:
                    switch (msg.arg1) {
                        case ScrapingModule.STATUS_COMPLETE:
                            if (fromMembership) {
                                isScrapingFinish = true;
                                if(isResponse){
                                    startAnimation();
                                    mStatus.setText(getString(R.string.loading_status_complete));
                                }
                            } else {
                                Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
                                if (fromMembership) {
                                    intent.putExtra(Const.FROM_MEMBERSHIP, true);
                                }
                                startActivity(intent);
                                finish();
                            }
                            break;
                        case ScrapingModule.STATUS_PREPARE:
//                            mStatus.setText("서비스 로딩중입니다.");
                            if(fromMembership){
                                mStatus.setVisibility(View.VISIBLE);
                                mNotice.setVisibility(View.VISIBLE);
                                mHandler.removeCallbacks(slideRunnable);
                                mHandler.postDelayed(slideRunnable, 5000);
                            }
                            break;
                        case ScrapingModule.STATUS_REQUEST:
                            break;
                        case ScrapingModule.STATUS_SCRAPING:
                            break;
                        case ScrapingModule.STATUS_UPLOADING:
                            break;
                    }
                    break;
                case BatchService.MSG_UPLOAD_COMPLETE:
                    if (fromMembership) {
                        isScrapingFinish = true;
                        if(isResponse){
                            startAnimation();
                            mStatus.setText(getString(R.string.loading_status_complete));
                        }
                    } else {
                        Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
                        if (fromMembership) {
                            intent.putExtra(Const.FROM_MEMBERSHIP, true);
                        }
                        startActivity(intent);
                        finish();
                    }
                    break;
                case BatchService.MSG_UPLOAD_MODE:
                    int mode = msg.arg1;
                    ProcessMode[] modes = ProcessMode.values();
                    mMode = modes[mode];
                    switch (mMode) {
                        case COLLECT_FIRST_THIS_MONTH:
                            break;
                        case COLLECT_FIRST_PAST_MONTH:
                        case COLLECT_DAILY:
                        case COLLECT_INDIVIDUAL:
                        case COLLECT_RETRY:
                        case COLLECT_LOGIN:
                            Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
                            if (fromMembership) {
                                intent.putExtra(Const.FROM_MEMBERSHIP, true);
                            }
                            startActivity(intent);
                            finish();
                            break;
                        case COLLECT_IDLE:
                            break;
                    }
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    }

}
