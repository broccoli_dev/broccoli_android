package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.fragment.FragmentOtherChallAdd;
import com.nomadconnection.broccoli.fragment.FragmentOtherChallList;
import com.nomadconnection.broccoli.interf.InterfaceChallenge;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;


public class Level2ActivityOtherChall extends BaseActivity implements InterfaceEditOrDetail, InterfaceChallenge {

	private static final String TAG = Level2ActivityOtherChall.class.getSimpleName();

	public static int mCurrentFragmentIndex = 0;	// default
	private final int FRAGMENT_ONE 		= 0;		// 추가, 편집
	private final int FRAGMENT_TWO 		= 1;		// 상세

	private ArrayList<EtcChallengeInfo> mAlInfoDataList;

	/* Layout */
	private Animation mAnimTitle;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_other_chall);

		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			if (mCurrentFragmentIndex == 1) finish();
			else if (mCurrentFragmentIndex == 0) {
				final String mStr = getString(R.string.other_challenge_popup_cancel);
				DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
									if (mAlInfoDataList.size() != 0) {
										mCurrentFragmentIndex = 1;
										fragmentReplace(mCurrentFragmentIndex, "out_right");
										afterMotion("back");
									} else {
										finish();
									}
								}
							}
						},
						new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviMotion(View _view) {
		try
		{
			switch (_view.getId()) {
				case R.id.tv_navi_motion_title:
				case R.id.ll_navi_motion_back:
					if (mCurrentFragmentIndex == 1) finish();
					else if (mCurrentFragmentIndex == 0) {
						final String mStr = getString(R.string.other_challenge_popup_cancel);
						DialogUtils.showDlgBaseTwoButton(this, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
											if(mAlInfoDataList.size() != 0){
												mCurrentFragmentIndex = 1;
												fragmentReplace(mCurrentFragmentIndex, "out_right");
												afterMotion("back");
											}
											else {
												finish();
											}
										}
									}
								},
								new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
					}
					break;

				case R.id.ll_navi_motion_else:
					if (mCurrentFragmentIndex == 1) {
						mCurrentFragmentIndex = 0;
						fragmentReplace(mCurrentFragmentIndex, "in_right");
						afterMotion("else");
					}
					else if (mCurrentFragmentIndex == 0) {
						((FragmentOtherChallAdd) getSupportFragmentManager().findFragmentByTag(String.valueOf(mCurrentFragmentIndex))).onComplete();
					}
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}


	private void afterMotion(String _Divider){
		if(_Divider.equals("else")){
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.other_challenge_add));
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText(getString(R.string.common_complete));
			complete_button_enable_disable(false); // 완료버튼 disable
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.VISIBLE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_0_35_0_0_400_fill);
		}
		else{	// default
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.other_challenge));
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText(R.string.common_add);
			complete_button_enable_disable(true); // 완료버튼 enable
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			mAnimTitle = AnimationUtils.loadAnimation(this, R.anim.translate_animation_35_0_0_0_400_fill);
		}
		((TextView)findViewById(R.id.tv_navi_motion_title)).startAnimation(mAnimTitle);
	}
	
	
	
	
	
	
	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			if (getIntent().getExtras() != null) {
				mAlInfoDataList = (ArrayList<EtcChallengeInfo>) getIntent().getSerializableExtra(Const.DATA);
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout)findViewById(R.id.rl_navi_motion_bg)).setBackgroundResource(R.color.main_4_layout_bg);
			((TextView)findViewById(R.id.tv_navi_motion_title)).setText(getString(R.string.other_challenge));
			((TextView)findViewById(R.id.tv_navi_motion_else)).setText(getString(R.string.common_add));
			((ImageView)findViewById(R.id.iv_navi_motion_back_icon)).setVisibility(View.VISIBLE);
			((TextView)findViewById(R.id.tv_navi_motion_back_txt)).setVisibility(View.GONE);
			
			/* Layout */
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			if(mAlInfoDataList.size() != 0){
				mCurrentFragmentIndex = 1;
				fragmentReplace(mCurrentFragmentIndex, "");
			}
			else {
				afterMotion("else");
				mCurrentFragmentIndex = 0;
				fragmentReplace(mCurrentFragmentIndex, "in_right");
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//	
	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

//			case R.id.ll_activity_setting_touch_id:
//				break;
				
//			case R.id.ll_activity_setting_push_msg:
//				break;
				
			default:
				break;
			}

		}
	};


//=========================================================================================//
// Fragment Navi
//=========================================================================================//
	/** Fragment 변경 **/
	private void fragmentReplace(int reqNewFragmentIndex , String _Anim)
	{
		try
		{
			Fragment newFragment = null;
			newFragment = getFragment(reqNewFragmentIndex);

			// replace fragment
			final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			if (_Anim.length() != 0 && _Anim.equals("in_right")) transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out);
			if (_Anim.length() != 0 && _Anim.equals("out_right")) transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
			transaction.replace(R.id.fl_level_2_activity_other_chall_container, newFragment, String.valueOf(reqNewFragmentIndex));

			// Commit the transaction
			transaction.commitAllowingStateLoss();

		}
		catch (Exception e)
		{}
	}

	/** Fragment Get **/
	private Fragment getFragment(int idx)
	{
		Fragment newFragment = null;

		switch (idx) {
			case FRAGMENT_ONE:	// 추가/편집
				newFragment = new FragmentOtherChallAdd();
				break;

			case FRAGMENT_TWO:	// 상세
				newFragment = new FragmentOtherChallList();
				break;

			default:
				break;
		}

		return newFragment;
	}



	//=========================================================================================//
// Interface
//=========================================================================================//
	@Override
	public void onEdit() {
		// TODO Auto-generated method stub
		complete_button_enable_disable(true); // 완료버튼 enable
	}

	@Override
	public void onDetail() {
		// TODO Auto-generated method stub
		mCurrentFragmentIndex = 1;
		fragmentReplace(mCurrentFragmentIndex, "");
	}

	@Override
	public void onError() {
		// TODO Auto-generated method stub
//		mCurrentFragmentIndex = 0;
//		fragmentReplace(mCurrentFragmentIndex);
		complete_button_enable_disable(false); // 완료버튼 disable
	}

	@Override
	public void onRefresh() {

	}


	@Override
	public void onSaveComplete() {
		if(mAlInfoDataList.size() != 0 ){
			mCurrentFragmentIndex = 1;
			fragmentReplace(mCurrentFragmentIndex, "out_right");
			afterMotion("back");
		}
		else {
			finish();
		}

	}

	@Override
	public void onSaveError() {
		ElseUtils.network_error(Level2ActivityOtherChall.this);
	}

	@Override
	public void onDeleteComplete() {

	}

	private void complete_button_enable_disable(boolean value){
		if(value){
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(true);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(true);
		}
		else {
			((LinearLayout)findViewById(R.id.ll_navi_motion_else)).setEnabled(false);
			((TextView)findViewById(R.id.tv_navi_motion_else)).setEnabled(false);
		}
	}
}
