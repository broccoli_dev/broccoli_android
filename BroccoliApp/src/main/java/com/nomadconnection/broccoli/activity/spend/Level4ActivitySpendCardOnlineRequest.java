package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Spend.SpendCardRecommendData;

/**
 * Created by YelloHyunminJang on 2017. 1. 17..
 */

public class Level4ActivitySpendCardOnlineRequest extends BaseActivity {

    private SpendCardRecommendData mData;

    private TextView mTitle;
    private WebView mWebView;
    private ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_spend_card_online_request_webview);
        init();
    }

    private void init() {
        String url = null;

        Intent intent = getIntent();
        if (intent.hasExtra(Const.DATA)) {
            mData = (SpendCardRecommendData) intent.getSerializableExtra(Const.DATA);
        }

        mTitle = (TextView) findViewById(R.id.tv_navi_img_txt_title);
        mWebView = (WebView) findViewById(R.id.wb_guide_view);
        mProgress = (ProgressBar) findViewById(R.id.pb_guide_view);

        mTitle.setText(mData.cardCompany.companyName+" 온라인 신청");

        // 웹뷰에서 자바스크립트실행가능
        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.loadUrl(mData.card.applyUrl);
        // WebViewClient 지정
        mWebView.setWebViewClient(new Level4ActivitySpendCardOnlineRequest.WebViewClientClass());
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                mProgress.setProgress(newProgress);
            }
        });
    }

    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains("goBroccoli")) {
                view.stopLoading();
                finish();
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mProgress.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    public void onBtnClickNaviImgTxtBar(View view) {
        finish();
    }
}
