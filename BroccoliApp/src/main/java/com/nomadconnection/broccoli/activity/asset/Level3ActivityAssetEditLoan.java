package com.nomadconnection.broccoli.activity.asset;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtExpListViewAssetEditLoan;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Asset.AssetDebtInfo;
import com.nomadconnection.broccoli.data.Asset.BodyBankActive;
import com.nomadconnection.broccoli.data.Asset.RequestAssetBankActive;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 2. 7..
 */

public class Level3ActivityAssetEditLoan extends BaseActivity {

    private ExpandableListView mExpListView;
    private ArrayList<Integer> mGroupList = null;
    private ArrayList<ArrayList<AssetDebtInfo>> mChildList = null;
    private ArrayList<AssetDebtInfo> mAlInfoDataList;
    private HashMap<String, Boolean> mChangeList = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_2_activity_asset_loan);
        sendTracker(AnalyticsName.AssetLoanEdit);
        if (parseParam()){
            if (initWidget()) {
                if (!initData()){
                    finish();
                }
            }
        }
    }

    private boolean parseParam() {
        boolean ret = true;
        if (getIntent() != null) {
            mAlInfoDataList = getIntent().getParcelableArrayListExtra(Const.DATA);
            mGroupList = new ArrayList<Integer>();
            mChildList = new ArrayList<ArrayList<AssetDebtInfo>>();
        }

        if(mAlInfoDataList == null){
            ret = false;
        }

        setData();
        return ret;
    }

    private boolean initWidget() {
        boolean ret = true;
        try {
			/* Title */
            ((TextView)findViewById(R.id.tv_navi_img_txt_txt_title)).setText("대출");
            ((TextView)findViewById(R.id.tv_navi_img_txt_txt_else)).setText(getString(R.string.common_complete));
            ((RelativeLayout)findViewById(R.id.rl_navi_img_txt_txt_bg)).setBackgroundResource(R.color.main_1_layout_bg);

			/* Layout */
            mExpListView = (ExpandableListView)findViewById(R.id.elv_fragment_asset_loan_1_explistview);
            mExpListView.setAdapter(new AdtExpListViewAssetEditLoan(this, mGroupList, mChildList, new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    AssetDebtInfo info = (AssetDebtInfo) buttonView.getTag();
                    if (info.mLoanType == AssetDebtInfo.LOAN_CARD) {
                        Toast.makeText(getBase(), "장기카드대출은 편집할 수 없습니다.", Toast.LENGTH_LONG).show();
                    } else {
                        info.toggleActive(isChecked);
                        mChangeList.put(String.valueOf(info.mAssetDebtId), isChecked);
                    }
                }
            }));

			/* group list click 시 */
            mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });

			/* Child list click 시 */
            mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                                            int groupPosition, int childPosition, long id) {
                    AssetDebtInfo info = mChildList.get(groupPosition).get(childPosition);
                    if (info.mLoanType == AssetDebtInfo.LOAN_CARD) {
                        Toast.makeText(getBase(), "장기카드대출은 편집할 수 없습니다.", Toast.LENGTH_LONG).show();
                    }
                    if (info == null) return true;
                    return false;
                }
            });

            if (mAlInfoDataList != null){
                if (mAlInfoDataList.size() == 0) {
                    mExpListView.setVisibility(View.GONE);
                    ((FrameLayout)findViewById(R.id.inc_fragment_asset_loan_1_empty)).setVisibility(View.VISIBLE);
                    ((ImageView)findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
                    ((TextView)findViewById(R.id.tv_empty_layout_txt)).setText(getString(R.string.common_empty_das));
                } else {
                    mExpListView.setVisibility(View.VISIBLE);
                    ((FrameLayout)findViewById(R.id.inc_fragment_asset_loan_1_empty)).setVisibility(View.GONE);
                }
            }
        }
        catch(Exception e) {
            ret = false;
        }
        return ret;
    }

    /**
     * initial method
     * <p> open child view </p>
     * @return
     */
    private boolean initData() {
        boolean ret = true;
        try {
            for (int i = 0; i <mGroupList.size() ; i++) {
                mExpListView.expandGroup(i);
            }

        } catch(Exception e) {
            ret = false;
            e.printStackTrace();
        }
        return ret;
    }

    private void setData() {

        mChangeList.clear();

        if (mAlInfoDataList != null) {
            for (int i = 0; i < mAlInfoDataList.size(); i++) {
                if (!mGroupList.contains(mAlInfoDataList.get(i).mLoanType)){
                    mGroupList.add(mAlInfoDataList.get(i).mLoanType);
                }
            }

            ArrayList<AssetDebtInfo> infoList;
            for (int i = 0; i < mGroupList.size() ; i++) {
                infoList = new ArrayList<AssetDebtInfo>();
                for (int j = 0; j < mAlInfoDataList.size(); j++) {
                    if (mGroupList.get(i) == mAlInfoDataList.get(j).mLoanType){
                        AssetDebtInfo info = mAlInfoDataList.get(j);
                        try {
                            infoList.add(info.clone());
                        } catch (CloneNotSupportedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mChildList.add(infoList);
            }
        }
    }

    public void onBtnClickNaviImgTxtTxtBar(View _view) {
        try {
            switch (_view.getId()) {
                case R.id.ll_navi_img_txt_txt_back:
                    finish();
                    break;
                case R.id.ll_navi_img_txt_txt_else:
                    sendEditList();
                    break;
                default:
                    break;
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void sendEditList() {
        if (mChangeList != null && mChangeList.size() > 0) {
            Set<String> set = mChangeList.keySet();
            Iterator<String> infoIterator = set.iterator();
            final List<BodyBankActive> list = new ArrayList<>();
            while (infoIterator.hasNext()) {
                BodyBankActive bankActive = new BodyBankActive();
                String id = infoIterator.next();
                bankActive.setAccountId(id);
                bankActive.setActiveYn(mChangeList.get(id) ? "Y":"N");
                list.add(bankActive);
            }
            RequestAssetBankActive assetBankActive = new RequestAssetBankActive();
            assetBankActive.setUserId(Session.getInstance(getApplicationContext()).getUserId());
            assetBankActive.setActive(list);
            Call call = ServiceGenerator.createScrapingService(AssetApi.class).setBankActiveRequest(assetBankActive);
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if (response.isSuccessful()) {
                        Result result = response.body();
                        if (result != null) {
                            if (!result.isOk()) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.common_warning_server_link_error), Toast.LENGTH_LONG).show();
                            } else {
                                for (int i=0; i < mAlInfoDataList.size(); i++) {
                                    AssetDebtInfo info = mAlInfoDataList.get(i);
                                    String id = info.mAssetDebtId;
                                    for (int j=0; j < list.size(); j++) {
                                        BodyBankActive bankActive = list.get(j);
                                        String targetId = bankActive.getAccountId();
                                        if (id == targetId) {
                                            info.mAssetDebtActiveYn = bankActive.getActiveYn();
                                        }
                                    }
                                }
                                Intent intent = new Intent();
                                intent.putExtra(Const.DATA, mAlInfoDataList);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.common_warning_server_link_error), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

}
