package com.nomadconnection.broccoli.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialReg;
import com.nomadconnection.broccoli.adapter.AdtListViewNotice;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.BodyMoneyInfo;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.Etc.EtcNoticeInfo;
import com.nomadconnection.broccoli.data.Etc.RequestEtcMoneyList;
import com.nomadconnection.broccoli.data.Etc.RequestEtcNoticeRead;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendMainData;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.io.Serializable;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Level2ActivityNotice extends BaseActivity {

	private static final String TAG = Level2ActivityNotice.class.getSimpleName();

	/* Layout */
	private LinearLayout mEmpty;
	private ImageView mEmptyIcon;
	private TextView mEmptyTitle;

	private ListView mListView;
	private AdtListViewNotice mAdapter;
	private List<EtcNoticeInfo> mAlInfoDataList;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_2_activity_etc_notice);

		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		notice_get_list();
		super.onResume();
	}

//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {    	
		try
		{
			switch (_view.getId()) {

			case R.id.ll_navi_img_txt_back:
				finish();
				break;

			default:
				break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());			
		}
	}
	
	
	
	
	
	
	
//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			//notice_get_list();
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//				mAlInfoDataList = (ArrayList<SpendCardInfo>) getIntent().getSerializableExtra(Const.DATA);
//			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout) findViewById(R.id.rl_navi_img_bg)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
			((TextView) findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.other_notice));

			/* Layout */
			mEmpty = (LinearLayout)findViewById(R.id.ll_empty_layout_layout);
			mEmptyIcon = (ImageView)findViewById(R.id.iv_empty_layout_icon);
			mEmptyTitle = (TextView)findViewById(R.id.tv_empty_layout_txt);

			mListView = (ListView)findViewById(R.id.lv_level_2_activity_etc_notice_listview);
			mAdapter = new AdtListViewNotice(this, mAlInfoDataList);	// test  임시
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(ItemClickListener);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


	
	
//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mEmpty.setVisibility(View.GONE);
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//	
	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

//			case R.id.ll_activity_setting_touch_id:
//				break;
				
//			case R.id.ll_activity_setting_push_msg:
//				break;
				
			default:
				break;
			}

		}
	};

	/**  Item Click Listener **/
	private AdapterView.OnItemClickListener ItemClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {
			if (mAlInfoDataList.get(position) == null) {	// test 임시
				return;
			}
			long now = System.currentTimeMillis();
			Date date = new Date(now);
			SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyyMM");
			EtcNoticeInfo mEtcNoticeInfo = mAlInfoDataList.get(position);
			String category = null;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			boolean needPreviousMonth = false;
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			if (day == 1) {
				needPreviousMonth = true;
			}
			if (needPreviousMonth) {
				calendar.add(Calendar.MONTH, -1);
			}
			date = calendar.getTime();
			String srt_date = CurDateFormat.format(date);

			switch (mEtcNoticeInfo.getCategoryCode()){
				case "0101": //결제 카테고리 미분류(기본)
				case "0102": //결제 카테고리 미분류(지난 달)
					category = "25";
					Intent intent = new Intent(Level2ActivityNotice.this, Level3ActivitySpendCategoryDetail.class);
					intent.putExtra(Const.SPEND_CATEGORY_DATE, srt_date);
					intent.putExtra(Const.SPEND_CATEGORY_CODE, category);
					startActivity(intent);
					break;
				case "0103": //월초 예산 설정 알림
					break;
//				case "0104": //총 예산 근접 알림
//					spend_budget_popup(getResources().getString(R.string.common_do_check));
//					break;
//				case "0105": //총 예산 초과 알림
//				case "0106": //카테고리별 예산 초과 알림
//					spend_budget_popup(getResources().getString(R.string.common_do_change));
//					break;

				case "0201": //투자자산 확인 유도
					Intent main = new Intent(Level2ActivityNotice.this, MainActivity.class).setFlags(
							Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					main.putExtra(Const.MAIN_INVEST, "Invest");
					startActivity(main);
					break;

				case "0301": //금융기관 등록 후 최초 설정 (3개월 데이터 기반)
				case "0305": //미납 알림
					etc_money_popup(getResources().getString(R.string.common_do_check));
					break;
				case "0302": //일정 예정 알림 (1일 전)
				case "0303": //일정 예정 알림 (당일)
				case "0304": //납부 완료 알림
					get_etc_money_list_data();
					break;

				case "0401": //진행 시
				case "0402": //챌린지 달성 시
				case "0403": //계좌 연동 촉구 시
					etc_challenge_popup(getResources().getString(R.string.common_do_check));
					break;

				case "0703":
					Intent setting_financial = new Intent(Level2ActivityNotice.this, Level3ActivitySettingFinancialReg.class);
					startActivity(setting_financial);
					break;
				default:
					break;
			}
		}
	};


	private void notice_get_list() {
		DialogUtils.showLoading(this);
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(this).getUserId());

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<List<EtcNoticeInfo>> call = api.listNotice(mRequestDataByUserId);
		call.enqueue(new Callback<List<EtcNoticeInfo>>() {
			@Override
			public void onResponse(Call<List<EtcNoticeInfo>> call, Response<List<EtcNoticeInfo>> response) {
				DialogUtils.dismissLoading();
				mAlInfoDataList = new ArrayList<EtcNoticeInfo>();
				mAlInfoDataList = response.body();

				if(mAlInfoDataList != null && mAlInfoDataList.size() != 0){
					mEmpty.setVisibility(View.GONE);
					mListView.setVisibility(View.VISIBLE);
					Collections.sort(mAlInfoDataList, myComparator);
					Collections.reverse(mAlInfoDataList);
					mAdapter.setData((ArrayList<EtcNoticeInfo>) mAlInfoDataList);
					refreshAdt(null);
				}
				else {
					String title = "알림 내역이 없습니다.";
					mEmpty.setVisibility(View.VISIBLE);
					mListView.setVisibility(View.GONE);
					mEmptyIcon.setImageResource(R.drawable.empty_icon_noti);
					mEmptyTitle.setText(title);
				}
			}

			@Override
			public void onFailure(Call<List<EtcNoticeInfo>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(Level2ActivityNotice.this);
			}
		});
	}

	private void notice_read_check() {
		List<String> mReadSet = new ArrayList<String>();
		for(int i = 0; i < mAlInfoDataList.size(); i++){
			String mReadId = mAlInfoDataList.get(i).getNotificationId();
			mReadSet.add(mReadId);
		}

		RequestEtcNoticeRead mRequestEtcNoticeRead = new RequestEtcNoticeRead();
		mRequestEtcNoticeRead.setUserId(Session.getInstance(this).getUserId());
		mRequestEtcNoticeRead.setList(mReadSet);

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<Result> call = api.readNotice(mRequestEtcNoticeRead);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {

			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {

			}
		});
	}

	public void refreshAdt(String _sort){
		mAdapter.notifyDataSetChanged();
		notice_read_check();
	}

	private void spend_budget_popup(final String mStr){
		DialogUtils.showDlgBaseTwoButton(Level2ActivityNotice.this, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
							get_spend_main_data();
						}
					}
				},
				new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
	}

	private void etc_money_popup(final String mStr){
		DialogUtils.showDlgBaseTwoButton(Level2ActivityNotice.this, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
							get_etc_money_list_data();
						}
					}
				},
				new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
	}


	private void etc_challenge_popup(final String mStr){
		DialogUtils.showDlgBaseTwoButton(Level2ActivityNotice.this, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
							get_etc_challenge_list_data();
						}
					}
				},
				new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_no), getString(R.string.common_yes), mStr));
	}


	private void get_spend_main_data(){
		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<SpendMainData> call = api.getSpendMain();
		call.enqueue(new Callback<SpendMainData>() {
			@Override
			public void onResponse(Call<SpendMainData> call, Response<SpendMainData> response) {
//				Intent spend_budget = new Intent(Level2ActivityNotice.this, Level2ActivitySpendBudget.class);
//				spend_budget.putExtra(Const.SPEND_BUDGET, response.body().getBudgetInfo());
//				startActivity(spend_budget);
			}

			@Override
			public void onFailure(Call<SpendMainData> call, Throwable t) {
			}
		});
	}


	private void get_etc_money_list_data(){
		long now = System.currentTimeMillis();
		Date date = new Date(now);
		SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyyMM");
		String mDate = CurDateFormat.format(date);

		DialogUtils.showLoading(this);
		RequestEtcMoneyList setData =
				new RequestEtcMoneyList(Session.getInstance(Level2ActivityNotice.this).getUserId(), mDate);

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<ArrayList<BodyMoneyInfo>> call = api.getCalendarList(setData);
		call.enqueue(new Callback<ArrayList<BodyMoneyInfo>>() {
			@Override
			public void onResponse(Call<ArrayList<BodyMoneyInfo>> call, Response<ArrayList<BodyMoneyInfo>> response) {
				DialogUtils.dismissLoading();
				Intent money_calendar = new Intent(Level2ActivityNotice.this, Level2ActivityOtherMini.class);
				money_calendar.putExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_1,response.body());
				startActivity(money_calendar);
			}

			@Override
			public void onFailure(Call<ArrayList<BodyMoneyInfo>> call, Throwable t) {
				DialogUtils.dismissLoading();
			}
		});
	}


	private void get_etc_challenge_list_data(){
		RequestDataByUserId userId = new RequestDataByUserId();
		userId.setUserId(Session.getInstance(Level2ActivityNotice.this).getUserId());

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<List<EtcChallengeInfo>> call = api.listChallenge(userId);
		call.enqueue(new Callback<List<EtcChallengeInfo>>() {
			@Override
			public void onResponse(Call<List<EtcChallengeInfo>> call, Response<List<EtcChallengeInfo>> response) {
				Intent etc_challenge = new Intent(Level2ActivityNotice.this, Level2ActivityOtherChall.class);
				etc_challenge.putExtra(Const.DATA, (Serializable)response.body());
				startActivity(etc_challenge);
			}

			@Override
			public void onFailure(Call<List<EtcChallengeInfo>> call, Throwable t) {

			}
		});
	}

	//Comparator 를 만든다.
	private final static Comparator<EtcNoticeInfo> myComparator= new Comparator<EtcNoticeInfo>()  {
		private final Collator collator = Collator.getInstance();

		@Override
		public int compare(EtcNoticeInfo object1, EtcNoticeInfo object2) {
			return collator.compare(object1.getDate(), object2.getDate());
		}
	};
}
