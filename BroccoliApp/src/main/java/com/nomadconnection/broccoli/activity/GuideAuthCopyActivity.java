package com.nomadconnection.broccoli.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 16. 9. 20..
 */
public class GuideAuthCopyActivity extends BaseActivity implements View.OnClickListener {

    private static final int ID_PASSWORD = 2;

    private ArrayList<SelectBankData> mSelectedBankData;
    private boolean fromMembership = false;
    private boolean isContainBank = false;
    private View mIdLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_copy_guide);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.CertHowPwCopy);
        initWidget();
        initData();
    }

    private void initWidget() {
        findViewById(R.id.rl_navi_img_bg).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.guide_auth_copy));
        mIdLogin = findViewById(R.id.tv_cert_id_login);
        mIdLogin.setOnClickListener(this);
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            if(intent.getExtras() != null) {
                mSelectedBankData = (ArrayList<SelectBankData>) intent.getSerializableExtra(Const.DATA);
                fromMembership = intent.getBooleanExtra(Const.FROM_MEMBERSHIP, false);
            }
        }
        if (mSelectedBankData != null) {
            for (int i=0; i < mSelectedBankData.size(); i++) {
                SelectBankData data = mSelectedBankData.get(i);
                if (BankData.TYPE_BANK.equalsIgnoreCase(data.getType())) {
                    isContainBank = true;
                }
            }
            if (!isContainBank) {
                mIdLogin.setVisibility(View.VISIBLE);
            } else {
                mIdLogin.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ll_guide_btn:
                showGuide();
                break;
            case R.id.tv_cert_id_login:
                Intent intent = new Intent(this, IdPasswordLoginActivity.class);
                intent.putExtra(Const.FROM_MEMBERSHIP, fromMembership);
                intent.putExtra(Const.DATA, mSelectedBankData);
                startActivityForResult(intent, ID_PASSWORD);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ID_PASSWORD) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    public void onBtnClickNaviImgTxtBar(View v) {
        onBackPressed();
    }

    private void showGuide() {
        Intent intent = new Intent(this, GuideWebActivity.class);
        startActivity(intent);
    }
}
