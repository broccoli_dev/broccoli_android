package com.nomadconnection.broccoli.activity.remit;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.remit.AdtExpListViewRemitHistory;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RemitBank;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemitHistoryActivity extends BaseActivity implements View.OnClickListener {

	private static final String TAG = RemitHistoryActivity.class.getSimpleName();

	/* Layout */
	private RadioGroup mRadioGroup;
	private ExpandableListView mExpListView;
	private AdtExpListViewRemitHistory mAdapter;
	private FrameLayout mConfirm;
	private ArrayList<RemitBank> mAlInfoDataList;
	private ArrayList<String> mGroupList = null;
	private ArrayList<ArrayList<String>> mChildList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remit_history);

		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
//			case R.id.ll_remit_possible_bank_back:
//				onBackPressed();
//				break;
//
//			case R.id.fl_activity_remit_possible_bank_confirm:
//				onBackPressed();
//				break;
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}


//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {

				case R.id.ll_navi_img_txt_back:
					onBackPressed();
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}


//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//				mAlInfoDataList = (ArrayList<EtcChallengeBank>) getIntent().getSerializableExtra(Const.DATA);
//			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



	//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout)findViewById(R.id.rl_navi_img_bg)).setBackgroundResource(R.color.main_4_layout_bg);
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(R.string.remit_history);

			/* Layout */
			mRadioGroup = (RadioGroup)findViewById(R.id.rg_activity_remit_history_group);
			mRadioGroup.setOnCheckedChangeListener(RadioBtnCheckedChangeListener);

			mExpListView = (ExpandableListView)findViewById(R.id.elv_activity_remit_history_list);
			mAdapter = new AdtExpListViewRemitHistory(this, mGroupList, mChildList);	// test  임시
			mExpListView.setAdapter(mAdapter);

			/* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return true;
				}
			});

			/* Child list click 시 */
			mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
											int groupPosition, int childPosition, long id) {
//					AssetBankInfo mTemp = null;
//					mTemp = mChildList.get(groupPosition).get(childPosition);
//					if (mTemp == null) return true;
//
//					NextStep(mTemp, new ArrayList<ResponseAssetBankDetail>());
					return false;
				}
			});

		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


//===============================================================================//
// Listener
//===============================================================================//
	/** Radio Button Click Listener **/
	private RadioGroup.OnCheckedChangeListener RadioBtnCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			switch (checkedId) {
				case R.id.rb_activity_remit_history_1:
					refreshAdt("all");
					break;

				case R.id.rb_activity_remit_history_2:
					refreshAdt("in");
					break;

				case R.id.rb_activity_remit_history_3:
					refreshAdt("out");
					break;
			}
		}
	};


	//=========================================================================================//
// Refresh Adapter
//=========================================================================================//
	public void refreshAdt(String _sort){
		mAdapter.sortData(_sort);
		mAdapter.notifyDataSetChanged();
	}


//=========================================================================================//
// get bank list API
//=========================================================================================//
	private void get_possible_bank_list(){
		DialogUtils.showLoading(this);
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(this).getUserId());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ArrayList<RemitBank>> call = api.getRemitBank(mRequestDataByUserId);
		call.enqueue(new Callback<ArrayList<RemitBank>>() {
			@Override
			public void onResponse(Call<ArrayList<RemitBank>> call, Response<ArrayList<RemitBank>> response) {
				DialogUtils.dismissLoading();
//				mAlInfoDataList = new ArrayList<RemitBank>();
//				mAlInfoDataList = response.body();
//				if (mAlInfoDataList.size() != 0) {
//					mAdapter.setData(mAlInfoDataList);
//					mAdapter.notifyDataSetChanged();
//				} else {
//
//				}
			}

			@Override
			public void onFailure(Call<ArrayList<RemitBank>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(RemitHistoryActivity.this);

			}
		});
	}
}
