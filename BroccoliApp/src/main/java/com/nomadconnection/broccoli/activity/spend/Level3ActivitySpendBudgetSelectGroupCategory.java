package com.nomadconnection.broccoli.activity.spend;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.spend.AdtSpendBudgetSelectGroupCategory;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetCategory;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetSettingData;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 2. 28..
 */

public class Level3ActivitySpendBudgetSelectGroupCategory extends BaseActivity implements View.OnClickListener {

    private TextView mTotalBudget;
    private View mBtn;
    private ListView mListView;
    private AdtSpendBudgetSelectGroupCategory mAdapter;
    private String mTargetYearMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_spend_budget_select_large_category);
        init();
    }

    private void init() {
        sendTracker(AnalyticsName.SpendBudgetAddCategory);
        mTargetYearMonth = getIntent().getStringExtra(Const.BUDGET_TARGET_MONTH);
        initWidget();
        initData();
    }

    private void initWidget() {
        ((TextView)findViewById(R.id.tv_navi_motion_title)).setText(R.string.spend_budget_setting_title);
        findViewById(R.id.tv_navi_motion_else).setVisibility(View.GONE);
        findViewById(R.id.rl_navi_motion_bg).setBackgroundResource(R.color.main_2_layout_bg);
        mTotalBudget = (TextView) findViewById(R.id.tv_category_total_budget);
        mListView = (ListView) findViewById(R.id.lv_category_select);
        mBtn = findViewById(R.id.btn_confirm);
        mBtn.setOnClickListener(this);
    }

    private void initData() {
        Intent intent = getIntent();
        String totalBudget = intent.getStringExtra(Const.SPEND_TOTAL_BUDGET);
        mTotalBudget.setText(ElseUtils.getStringDecimalFormat(totalBudget));
        mBtn.setEnabled(false);

        mListView.setOnItemClickListener(mListener);
        mAdapter = new AdtSpendBudgetSelectGroupCategory(this);
        mAdapter.registerDataSetObserver(mObserver);
        mListView.setAdapter(mAdapter);
        getData();
    }

    private void getData() {
        Call<JsonObject> call = ServiceGenerator.createService(SpendApi.class).getCategoryExpense();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(getActivity() == null || getActivity().isFinishing()){
                    return;
                }

                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        JsonObject object = response.body();
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<SpendBudgetCategory>>() {}.getType();
                        List<SpendBudgetCategory> realData = gson.fromJson(object.getAsJsonArray("list"), listType);
                        createGroupCategory(realData);
                    } else {
                        ErrorMessage message = ErrorUtils.parseError(response);
                        switch (message) {
                            case ERROR_713_NO_SUCH_USER:
                                Toast.makeText(getBase(), "가입되지 않은 사용자입니다. 새로 회원가입해주세요.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_719_QUITE_USER:
                                Toast.makeText(getBase(), "탈퇴 회원은 탈퇴일로부터 5일 이후 재가입 가능합니다.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_1011_CERT_SESSION_EXPIRE:
                                Toast.makeText(getBase(), "인증시간이 만료되었습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                                break;
                            case ERROR_1012_CERT_NOT_FINISH:
                                Toast.makeText(getBase(), "인증에 실패하였습니다. 실명인증을 다시해주십시오.", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                ElseUtils.network_error(getBase());
            }
        });
    }

    private void createGroupCategory(List<SpendBudgetCategory> categoryList) {
        ArrayList<SpendBudgetSettingData> list = new ArrayList<>();
        String[] groupCategory = getResources().getStringArray(R.array.spend_budget_group_category);
        //미설정 예산은 안 보여 주기때문에
        for (int i=1; i < groupCategory.length; i++) {
            String expense = null;
            CategoryChecker : for (int j=0; j < categoryList.size(); j++) {
                int id = categoryList.get(j).categoryGroup;
                if (id == i) {
                    expense = String.valueOf(categoryList.get(j).expense);
                    break CategoryChecker;
                }
            }
            String subTitle = null;
            switch (i) {
                case 1:
                    subTitle = getString(R.string.spend_budget_select_food_subtitle);
                    break;
                case 2:
                    subTitle = getString(R.string.spend_budget_select_beauty_subtitle);
                    break;
                case 3:
                    subTitle = getString(R.string.spend_budget_select_shopping_subtitle);
                    break;
                case 4:
                    subTitle = getString(R.string.spend_budget_select_hobby_subtitle);
                    break;
                case 5:
                    subTitle = getString(R.string.spend_budget_select_study_subtitle);
                    break;
                case 6:
                    subTitle = getString(R.string.spend_budget_select_etc_subtitle);
                    break;
                case 7:
                    subTitle = getString(R.string.spend_budget_select_fixed_subtitle);
                    break;
            }

            SpendBudgetSettingData data = new SpendBudgetSettingData();
            data.index = i;
            data.title = groupCategory[i];
            data.subTitle = subTitle;
            data.expense = expense;
            list.add(data);
        }
        mAdapter.setData(list);
        mAdapter.notifyDataSetChanged();
    }

    private ListView.OnItemClickListener mListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            SpendBudgetSettingData data = (SpendBudgetSettingData) mAdapter.getItem(position);
            data.isChecked = !data.isChecked;
            mAdapter.notifyDataSetChanged();
        }
    };

    private DataSetObserver mObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            List<SpendBudgetSettingData> list =  mAdapter.getDataList();
            int count = 0;
            if (list != null) {
                for (int i=0; i < list.size(); i++) {
                    if (list.get(i).isChecked) {
                        count++;
                    }
                }
            }

            if (count > 0) {
                mBtn.setEnabled(true);
            } else {
                mBtn.setEnabled(false);
            }
        }
    };

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_confirm:
                settingCategoryBudget();
                break;
        }
    }

    private void settingCategoryBudget() {
        ArrayList<SpendBudgetSettingData> selectList = new ArrayList<>();
        List<SpendBudgetSettingData> list =  mAdapter.getDataList();
        if (list != null) {
            for (int i=0; i < list.size(); i++) {
                if (list.get(i).isChecked) {
                    selectList.add(list.get(i));
                }
            }
        }
        Intent dataIntent = getIntent();
        Intent intent = new Intent(this, Level4ActivitySpendBudgetSettingGroupCategory.class);
        intent.putExtra(Const.SPEND_TOTAL_BUDGET, dataIntent.getStringExtra(Const.SPEND_TOTAL_BUDGET));
        intent.putExtra(Const.SPEND_BUDGET_SELECT_DATA, selectList);
        intent.putExtra(Const.BUDGET_TARGET_MONTH, mTargetYearMonth);
        startActivityForResult(intent, 0);
    }
}
