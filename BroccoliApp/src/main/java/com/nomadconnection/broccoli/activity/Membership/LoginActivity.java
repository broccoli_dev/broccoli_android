package com.nomadconnection.broccoli.activity.membership;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.DemoSettingActivity;
import com.nomadconnection.broccoli.activity.MainActivity;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Dayli.DayliLoginData;
import com.nomadconnection.broccoli.data.Dayli.ResponseLogin;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.data.Session.ResponseBroccoli;
import com.nomadconnection.broccoli.data.Session.UserData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static com.igaworks.impl.CommonFrameworkImpl.getContext;

/**
 * Created by YelloHyunminJang on 2016. 12. 14..
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private View mJoinMemberBtn;
    private View mLoginBtn;
    private EditText mInputId;
    private EditText mInputPwd;
    private TextView mLoginResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    private void init() {
        initWidget();
    }

    private void initWidget() {
        sendTracker(AnalyticsName.LoginScreen);
        mJoinMemberBtn = findViewById(R.id.login_join_btn);
        mJoinMemberBtn.setOnClickListener(this);
        mLoginResult = (TextView) findViewById(R.id.tv_login_result);
        mLoginBtn = findViewById(R.id.login_do_login_btn);
        mLoginBtn.setOnClickListener(this);
        mInputId = (EditText) findViewById(R.id.ed_login_id);
        mInputId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!ElseUtils.validateEmail(s.toString())) {
                    mLoginResult.setText("유효하지 않은 이메일 형식입니다.");
                    mLoginResult.setTextColor(Color.argb(255, 244, 18, 185));
                    mLoginBtn.setEnabled(false);
                } else {
                    mLoginResult.setText(null);
                    mLoginResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    mLoginResult.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mLoginBtn.setEnabled(true);
                }
            }
        });
        mInputPwd = (EditText) findViewById(R.id.ed_login_pwd);
        mInputPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        findViewById(R.id.login_find_id_btn).setOnClickListener(this);
        findViewById(R.id.login_find_pwd_btn).setOnClickListener(this);
        findViewById(R.id.demo_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.login_join_btn:
                joinMembership();
                break;
            case R.id.login_do_login_btn:
                doLogin();
                break;
            case R.id.login_find_id_btn:
                findId();
                break;
            case R.id.login_find_pwd_btn:
                findPwd();
                break;
            case R.id.demo_btn:
                showDemo();
                break;
        }
    }

    private void doLogin() {
        String id = mInputId.getText().toString();
        String pwd = mInputPwd.getText().toString();

        if ((id == null || id.isEmpty()) || (pwd == null || pwd.isEmpty())) {
            mLoginResult.setText("이메일 또는 비밀번호를 입력해 주세요.");
            mLoginResult.setTextColor(Color.argb(255, 244, 18, 185));
            mInputId.requestFocus();
            return;
        } else {
            DialogUtils.showLoading(this);
            DayliApi dayliApi = ServiceGenerator.createServiceDayli(DayliApi.class);
            DayliLoginData dayliLoginData = new DayliLoginData();
            dayliLoginData.setEmail(id);
            dayliLoginData.setPassword(pwd);
            Call<ResponseLogin> call = dayliApi.dayliLogin(dayliLoginData);
            call.enqueue(new Callback<ResponseLogin>() {
                @Override
                public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                    DialogUtils.dismissLoading();
                    if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                        ResponseLogin login = response.body();
                        long dpId = login.getUserId();
                        String token = response.headers().get(Const.TOKEN);
                        doBroccoliLogin(dpId, token);
                    } else {
                        Converter<ResponseBody, Result> converter = ServiceGenerator.getRetrofit().responseBodyConverter(Result.class, new Annotation[0]);
                        try {
                            Result result = converter.convert(response.errorBody());
                            if (ErrorMessage.ERROR_701_INCORRECT_ACCOUNT.code().equalsIgnoreCase(result.getCode())) {
                                mLoginResult.setText("이메일 또는 비밀번호가 일치하지 않습니다.");
                                mLoginResult.setTextColor(Color.argb(255, 244, 18, 185));
                                mInputId.requestFocus();
                            } else if (ErrorMessage.ERROR_713_NO_SUCH_USER.code().equalsIgnoreCase(result.getCode())) {
                                mLoginResult.setText("등록되지 않은 이메일입니다.");
                                mLoginResult.setTextColor(Color.argb(255, 244, 18, 185));
                                mInputId.requestFocus();
                            } else if (ErrorMessage.ERROR_719_QUITE_USER.code().equalsIgnoreCase(result.getCode())) {
                                mLoginResult.setText("탈퇴 회원은 탈퇴일로부터 5일 이후 재가입이 가능합니다.");
                                mLoginResult.setTextColor(Color.argb(255, 244, 18, 185));
                                mInputId.requestFocus();
                            } else {
                                ElseUtils.network_error(LoginActivity.this);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseLogin> call, Throwable t) {
                    DialogUtils.dismissLoading();
                    ElseUtils.network_error(LoginActivity.this);
                }
            });
        }
    }

    private void doBroccoliLogin(final long dpId, final String dpToken) {
        UserApi userApi = ServiceGenerator.createService(UserApi.class);
        UserData data = new UserData();
        data.setDpId(dpId);
        data.setDpAccessToken(dpToken);
        data.setOsType(1);
        data.setDeviceId(ElseUtils.getDeviceUUID(this));
        data.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
        data.setAdId(APPSharedPrefer.getInstance(getBase()).getPrefString(APP.SP_USER_ADID));
        Call<ResponseBroccoli> call = userApi.broccoliLogin(data);
        call.enqueue(new Callback<ResponseBroccoli>() {
            @Override
            public void onResponse(Call<ResponseBroccoli> call, Response<ResponseBroccoli> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    ResponseBroccoli result = response.body();
                    if (result != null) {
                        if (result.isOk() || result.checkUnable()) {
                            final String broccoliToken = response.headers().get(Const.TOKEN);
                            final String id = result.userId;
                            final String sign = result.signedRequest;
                            final int days = result.scrapingDays;
                            Session.getInstance(LoginActivity.this).acquirePublicFromId(id, new Session.OnKeyInterface() {
                                @Override
                                public void acquired(String str) {
                                    try {
                                        LoginData loginData = DbManager.getInstance().getUserInfoDataSet(getApplicationContext(), str, id);
                                        loginData.setUserId(id);
                                        loginData.setPassword(sign);
                                        loginData.setDayliId(dpId);
                                        loginData.setDayliToken(dpToken);
                                        loginData.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
                                        loginData.setType(1); // android
                                        loginData.setDeviceId(ElseUtils.getDeviceUUID(getApplicationContext()));
                                        loginData.setLogin(LoginData.TRUE);
                                        Date current = Calendar.getInstance().getTime();
                                        loginData.setLastLogin(ElseUtils.getSimpleDate(current));
                                        Session.getInstance(getApplicationContext()).setToken(broccoliToken);
                                        if (loginData.getPinNumber() != null && !TextUtils.isEmpty(loginData.getPinNumber())) {
                                            Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_START);
                                            intent.putExtra(Const.SCRAPING_DAY, days);
                                            getBase().sendBroadcast(intent);
                                            simpleLogin(str, loginData);
                                        } else {
                                            showPinNumberSetting(loginData, days);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void error(ErrorMessage errorMessage) {
                                    switch (errorMessage) {
                                        case ERROR_713_NO_SUCH_USER:
                                        case ERROR_10007_SESSION_NOT_FOUND:
                                        case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                                        case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                                        case ERROR_20004_NO_SUCH_USER:
                                            showLoginActivity();
                                            break;
                                    }
                                }

                                @Override
                                public void fail() {
                                    Toast.makeText(getApplicationContext(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 비밀번호를 재입력 해주십시오.", Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            String code = result.getCode();
                            if (ErrorMessage.ERROR_20004_NO_SUCH_USER.code().equalsIgnoreCase(code)) {
                                addMembership(dpId, dpToken);
                            } else {
                                Toast.makeText(getApplicationContext(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 비밀번호를 재입력 해주십시오.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 비밀번호를 재입력 해주십시오.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBroccoli> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(LoginActivity.this);
            }
        });
    }

    private void addMembership(final long dpid, final String dpToken) {
        DialogUtils.showLoading(getBase());
        UserData data = new UserData();
        data.setDpId(dpid);
        data.setDpAccessToken(dpToken);
        data.setOsType(1);
        data.setDeviceId(ElseUtils.getDeviceUUID(getBase()));
        data.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
        data.setAdId(APPSharedPrefer.getInstance(getBase()).getPrefString(APP.SP_USER_ADID));
        Call<ResponseBroccoli> call = ServiceGenerator.createService(UserApi.class).userAdd(data);
        call.enqueue(new Callback<ResponseBroccoli>() {
            @Override
            public void onResponse(Call<ResponseBroccoli> call, Response<ResponseBroccoli> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    ResponseBroccoli result = response.body();
                    if (result != null) {
                        final String broccoliToken = response.headers().get(Const.TOKEN);
                        final String id = result.userId;
                        final String sign = result.signedRequest;
                        final int days = result.scrapingDays;
                        try {
                            Session.getInstance(getBase()).acquirePublicFromId(id, new Session.OnKeyInterface() {
                                @Override
                                public void acquired(final String str) {
                                    try {
                                        LoginData loginData = DbManager.getInstance().getUserInfoDataSet(getContext(), str, id);
                                        loginData.setUserId(id);
                                        loginData.setPassword(sign);
                                        loginData.setDayliId(dpid);
                                        loginData.setDayliToken(dpToken);
                                        loginData.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
                                        loginData.setType(1); // android
                                        loginData.setDeviceId(ElseUtils.getDeviceUUID(getContext()));
                                        loginData.setLogin(LoginData.TRUE);
                                        Date current = Calendar.getInstance().getTime();
                                        loginData.setLastLogin(ElseUtils.getSimpleDate(current));
                                        DbManager.getInstance().insertOrUpdateUserInfoData(getContext(), str, loginData);
                                        Session.getInstance(getContext()).setToken(broccoliToken);
                                        if (loginData.getPinNumber() != null && !TextUtils.isEmpty(loginData.getPinNumber())) {
                                            simpleLogin(str, loginData);
                                            Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_START);
                                            intent.putExtra(Const.SCRAPING_DAY, days);
                                            getBase().sendBroadcast(intent);
                                        } else {
                                            showPinNumberSetting(loginData, days);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    DialogUtils.dismissLoading();
                                }

                                @Override
                                public void error(ErrorMessage errorMessage) {
                                    switch (errorMessage) {
                                        case ERROR_713_NO_SUCH_USER:
                                        case ERROR_10007_SESSION_NOT_FOUND:
                                        case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                                        case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                                        case ERROR_20004_NO_SUCH_USER:
                                            showLoginActivity();
                                            break;
                                    }
                                }

                                @Override
                                public void fail() {
                                    DialogUtils.dismissLoading();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    DialogUtils.dismissLoading();
                } else {
                    DialogUtils.dismissLoading();
                    Toast.makeText(getContext(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 비밀번호를 재입력 해주십시오.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBroccoli> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getBase());
            }
        });
    }

    private void simpleLogin(String key, LoginData data) {
        try {
            DbManager.getInstance().insertOrUpdateUserInfoData(this, key, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Session.getInstance(this).setSimpleUserInfo(data, new Session.OnUserInfoUpdateCompleteInterface() {
            @Override
            public void complete(UserPersonalInfo info) {
                if (info != null) {
                    Intent intent = new Intent(getBase(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void error(ErrorMessage message) {
                switch (message) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        showTokenExpire();
                        break;
                }
            }
        });
    }

    private void showPinNumberSetting(LoginData data, int days) {
        Intent intent = new Intent(this, MembershipSetPinNumberActivity.class);
        intent.putExtra(Const.DATA, data);
        intent.putExtra(Const.SCRAPING_DAY, days);
        startActivity(intent);
        finish();
    }

    private void joinMembership() {
        Intent intent = new Intent(this, MembershipActivity.class);
        startActivity(intent);
        finish();
    }

    private void findId() {
        Intent intent = new Intent(this, MembershipFindIdActivity.class);
        startActivity(intent);
    }

    private void findPwd() {
        Intent intent = new Intent(this, MembershipFindPwdActivity.class);
        intent.putExtra(MembershipResetPwdActivity.FROM_LOGIN, true);
        startActivity(intent);
    }

    private void showDemo() {
        Intent intent = new Intent(this, DemoSettingActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
}
