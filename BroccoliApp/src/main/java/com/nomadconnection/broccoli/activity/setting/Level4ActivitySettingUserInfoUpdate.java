package com.nomadconnection.broccoli.activity.setting;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.fragment.FragmentMembershipPersonal;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;

/**
 * Created by YelloHyunminJang on 16. 8. 18..
 */
public class Level4ActivitySettingUserInfoUpdate extends BaseActivity {

    private TextView mCancelBtn;
    private TextView mSubTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_4_activity_setting_userinfo_change);
        sendTracker(AnalyticsName.SettingUserinfoEdit);
        init();
    }

    private void init() {
        mCancelBtn = (TextView) findViewById(R.id.tv_navi_motion_title);
        mSubTitle = (TextView) findViewById(R.id.tv_navi_motion_else);
        mSubTitle.setText(R.string.common_complete);
        findViewById(R.id.ll_navi_img_txt_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        UserPersonalInfo info = Session.getInstance(this).getUserPersonalInfo();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        BaseFragment baseFragment = new FragmentMembershipPersonal();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Const.IS_UPDATE_MODE, true);
        bundle.putSerializable(Const.DATA, info);
        baseFragment.setArguments(bundle);
        ft.add(R.id.fragment_area, baseFragment);
        ft.commit();
    }

    public void onBtnClickNaviMotion(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_navi_img_txt_else:
                break;
        }
    }
}
