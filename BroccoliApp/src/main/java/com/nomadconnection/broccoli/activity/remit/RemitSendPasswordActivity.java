package com.nomadconnection.broccoli.activity.remit;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataChangeMoney;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendPhone;
import com.nomadconnection.broccoli.fragment.remit.FragmentRemitPersonalCertify;
import com.nomadconnection.broccoli.fragment.remit.FragmentRemitSendPassword;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.Const;


public class RemitSendPasswordActivity extends BaseActivity implements InterfaceEditOrDetail{

	private static final String TAG = RemitSendPasswordActivity.class.getSimpleName();


	public static int mCurrentFragmentIndex = 0;	// default
	private final int FRAGMENT_REMIT_SEND_FAIL 		= 0;	// 비번 재설정
	private final int FRAGMENT_REMIT_SEND_PASSWORD 	= 1;	// 비번 입력후 송금

	/* Layout */
	private int mRemitPageStatus; // true: 전화번호 false: 계좌번호
	private RemitAccount mRemitAccount;
	private RequestDataSendAccount mRequestDataSendAccount;
	private RequestDataSendPhone mRequestDataSendPhone;
	private RequestDataChangeMoney mRequestDataChangeMoney;
	private String mRemitSendSum = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remit_send_password);
//		sendTracker(AnalyticsName.Level2ActivitySpendHistory);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if (mCurrentFragmentIndex == 1) {
				finish();
			}
			else if (mCurrentFragmentIndex == 0) {
				mCurrentFragmentIndex = 1;
				fragmentReplace(mCurrentFragmentIndex, "out_right");
			}
		}
		return super.onKeyDown(keyCode, event);
	}

//=========================================================================================//
// ToolBar - motion
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {
				case R.id.ll_navi_img_txt_back:
					if (mCurrentFragmentIndex == 1 ) {
						finish();
					}
					else if (mCurrentFragmentIndex == 0) {
						mCurrentFragmentIndex = 1;
						fragmentReplace(mCurrentFragmentIndex, "out_right");
					}
					break;

				default:
					break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}


//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			if (getIntent().getExtras() != null) {
				mRemitPageStatus = getIntent().getIntExtra(Const.REMIT_PAGE_STATUS, APP.REMIT_SEND_PHONE);
				if(mRemitPageStatus == APP.REMIT_SEND_PHONE){
					mRequestDataSendPhone = (RequestDataSendPhone)getIntent().getSerializableExtra(Const.DATA);
				}
				else if(mRemitPageStatus == APP.REMIT_SEND_ACCOUNT) {
					mRequestDataSendAccount = (RequestDataSendAccount)getIntent().getSerializableExtra(Const.DATA);
				}
				else {
					mRequestDataChangeMoney = (RequestDataChangeMoney)getIntent().getSerializableExtra(Const.DATA);
				}

				if(getIntent().hasExtra(Const.REMIT_ACCOUNT_INFO)){
				mRemitAccount = (RemitAccount)getIntent().getSerializableExtra(Const.REMIT_ACCOUNT_INFO);
			}
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



	//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout)findViewById(R.id.rl_navi_img_bg)).setBackgroundResource(R.color.main_4_layout_bg);
			if(mRemitPageStatus == APP.REMIT_CHANGE_MONEY){
				((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.remit_charge));
			}
			else {
				((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(getString(R.string.remit_service));
			}

			/* Layout */

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




	//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCurrentFragmentIndex = 1;
			fragmentReplace(mCurrentFragmentIndex, "");
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


//=========================================================================================//
// Fragment Navi
//=========================================================================================//
	/** Fragment 변경 **/
	private void fragmentReplace(int reqNewFragmentIndex, String _Anim)
	{
		try
		{
			Fragment newFragment = null;
			newFragment = getFragment(reqNewFragmentIndex);

			// replace fragment
			final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			if (_Anim.length() != 0 && _Anim.equals("in_right")) transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out);
			if (_Anim.length() != 0 && _Anim.equals("out_right")) transaction.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
			transaction.replace(R.id.fl_activity_remit_send_password_container, newFragment, String.valueOf(reqNewFragmentIndex));

			// Commit the transaction
			transaction.commitAllowingStateLoss();

		}
		catch (Exception e)
		{}
	}

	/** Fragment Get **/
	private Fragment getFragment(int idx)
	{
		Fragment newFragment = null;

		switch (idx) {
			case FRAGMENT_REMIT_SEND_FAIL:	// 비번 재설정
				newFragment = new FragmentRemitPersonalCertify();
				break;

			case FRAGMENT_REMIT_SEND_PASSWORD:	// 비번 입력후 송금
				Bundle bundle = new Bundle();
				newFragment = new FragmentRemitSendPassword();
				if(mRemitPageStatus == APP.REMIT_SEND_PHONE){
					bundle.putInt(Const.REMIT_PAGE_STATUS, APP.REMIT_SEND_PHONE);
					bundle.putSerializable(Const.DATA, mRequestDataSendPhone);
					bundle.putSerializable(Const.REMIT_ACCOUNT_INFO, mRemitAccount);
					newFragment.setArguments(bundle);
				}
				else if(mRemitPageStatus == APP.REMIT_SEND_ACCOUNT) {
					bundle.putInt(Const.REMIT_PAGE_STATUS, APP.REMIT_SEND_ACCOUNT);
					bundle.putSerializable(Const.DATA, mRequestDataSendAccount);
					bundle.putSerializable(Const.REMIT_ACCOUNT_INFO, mRemitAccount);
					newFragment.setArguments(bundle);
				}
				else {
					bundle.putInt(Const.REMIT_PAGE_STATUS, APP.REMIT_CHANGE_MONEY);
					bundle.putSerializable(Const.DATA, mRequestDataChangeMoney);
					newFragment.setArguments(bundle);
				}
				break;

			default:
				break;
		}

		return newFragment;
	}



//=========================================================================================//
// Interface
//=========================================================================================//
	@Override
	public void onEdit() {
		mCurrentFragmentIndex = 0;
		fragmentReplace(mCurrentFragmentIndex, "in_right");
	}

	@Override
	public void onDetail() {
		mCurrentFragmentIndex = 1;
		fragmentReplace(mCurrentFragmentIndex, "out_right");
	}

	@Override
	public void onError() {

	}

	@Override
	public void onRefresh() {

	}
}
