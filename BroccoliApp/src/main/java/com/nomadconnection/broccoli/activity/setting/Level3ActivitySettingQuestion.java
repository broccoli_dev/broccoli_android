package com.nomadconnection.broccoli.activity.setting;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;


public class Level3ActivitySettingQuestion extends BaseActivity {

	private static final String TAG = Level3ActivitySettingQuestion.class.getSimpleName();

	
	/* Layout */
	private EditText mEmail;
	private EditText mContents;
	private Button mSend;
	private TextView mMaxCheck;
	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.btn_level_3_activity_setting_question_send:
				email_send();
				finish();
				break;

			default:
				break;
			}

		}
	};
	private TextWatcher addressWatcher = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(s.length() > 0 && mContents.length() > 0){
				mSend.setEnabled(true);
			}
			else {
				mSend.setEnabled(false);
			}
		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	};
	private TextWatcher contentWatcher = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			String inputNum = String.valueOf(s.length());
			mMaxCheck.setText(inputNum + "/200자");
			if(s.length() > 0 && mEmail.length() > 0){
				mSend.setEnabled(true);
			}
			else {
				mSend.setEnabled(false);
			}
		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	};



	
	
	

//=========================================================================================//
// ToolBar - Image/Text
//=========================================================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level_3_activity_setting_question);
		sendTracker(AnalyticsName.SettingQuestion);
		if(parseParam()){
			if(initWidget()) {
				if(!initData()){
					finish();
				}
			}
			else{
				finish();
			}
		}
		else{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	/**  ToolBar click event handler **/
	public void onBtnClickNaviImgTxtBar(View _view) {
		try
		{
			switch (_view.getId()) {

			case R.id.ll_navi_img_txt_back:
				finish();
				break;

			default:
				break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
		}
	}

	

//===============================================================================//
// Listener
//===============================================================================//	

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget() {
		boolean bResult = true;
		try
		{
			/* Title */
			((RelativeLayout) findViewById(R.id.rl_navi_img_bg)).setBackgroundColor(getResources().getColor(R.color.common_rgb_51_51_51));
			((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText("문의하기");

			/* Layout */
			mEmail = (EditText)findViewById(R.id.et_level_3_activity_setting_question_email);
			mEmail.addTextChangedListener(addressWatcher);
			mContents = (EditText)findViewById(R.id.et_level_3_activity_setting_question_contents);
			mContents.setHorizontallyScrolling(false);
			mContents.addTextChangedListener(contentWatcher);
			mMaxCheck = (TextView)findViewById(R.id.tv_level3_activity_setting_max_check);
			mSend = (Button)findViewById(R.id.btn_level_3_activity_setting_question_send);
			mSend.setOnClickListener(BtnClickListener);
			mSend.setClickable(true);
			mSend.setEnabled(false);

			//mSend.setOnClickListener(BtnClickListener);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			UserPersonalInfo info = Session.getInstance(this).getUserPersonalInfo();
			if (info != null && info.user != null) {
				mEmail.setText(info.user.email);
			}
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void email_send()
	{
		Intent it = new Intent(Intent.ACTION_SEND);
		it.setType("plain/text");

		// 수신인 주소 - tos배열의 값을 늘릴 경우 다수의 수신자에게 발송됨
		String[] tos = { "broccoli@daylifg.com" };
		it.putExtra(Intent.EXTRA_EMAIL, tos);
		it.putExtra(Intent.EXTRA_SUBJECT, "[브로콜리] 문의 사항");
		it.putExtra(Intent.EXTRA_TEXT, mContents.getText().toString() + device_info());
		startActivity(it);
	}

	private String device_info(){
		String device = Build.MODEL;
		String os_ver = Build.VERSION.RELEASE;
		String app_ver = MainApplication.getVersionName();
		String info = null;
		info =  "\n\n" + "Device : " + device + "\n"
				+ "브로콜리 APP version : " + app_ver +"\n" + "OS version : " + os_ver +"\n";
		return info;
	}
}
