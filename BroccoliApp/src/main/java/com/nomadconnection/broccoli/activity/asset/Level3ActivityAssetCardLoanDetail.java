package com.nomadconnection.broccoli.activity.asset;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewAssetCardLoan;
import com.nomadconnection.broccoli.common.FloatingActionButton;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Asset.ResponseCardLoanDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.Const;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2017. 2. 6..
 */

public class Level3ActivityAssetCardLoanDetail extends BaseActivity implements View.OnClickListener {

    private static final String TAG = Level3ActivityAssetCardLoanDetail.class.getSimpleName();

    private FloatingActionButton mFloatingButton;
    private ListView mListView;
    private AdtListViewAssetCardLoan mAdapter;

    private ArrayList<ResponseCardLoanDetail> mDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_3_activity_asset_cardloan_detail);
        sendTracker(AnalyticsName.AssetCardLoanDetail);
        init();
    }

    private void init() {

        ((TextView)findViewById(R.id.tv_navi_img_txt_title)).setText(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitle);
        if (MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg != 0) (findViewById(R.id.rl_navi_img_bg)).setBackgroundResource(MainApplication.Screen.getInstance().getScreenInfo(TAG).mTitleBg);

        Intent intent = getIntent();
        mDataList = (ArrayList<ResponseCardLoanDetail>) intent.getSerializableExtra(Const.DATA);

        if (mDataList.size() > 0) {
            findViewById(R.id.ll_empty_layout_layout).setVisibility(View.GONE);
        } else {
            findViewById(R.id.ll_empty_layout_layout).setVisibility(View.VISIBLE);
        }

        mListView = (ListView) findViewById(R.id.lv_fragment_asset_loan_detail_a_listview);
        mAdapter = new AdtListViewAssetCardLoan(this, mDataList);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        mFloatingButton = (FloatingActionButton)findViewById(R.id.fab_asset_loan_button);
        mFloatingButton.attachToListView(mListView);
        mFloatingButton.setShadow(false);
        mFloatingButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

    }

    public void onBtnClickNaviImgTxtBar(View view) {
        onBackPressed();
    }
}
