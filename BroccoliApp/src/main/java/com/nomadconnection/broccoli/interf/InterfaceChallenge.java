package com.nomadconnection.broccoli.interf;


public interface InterfaceChallenge {
	public void onSaveComplete();
	public void onSaveError();
	public void onDeleteComplete();
}
