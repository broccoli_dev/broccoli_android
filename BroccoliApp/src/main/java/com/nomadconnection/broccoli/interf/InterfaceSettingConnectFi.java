package com.nomadconnection.broccoli.interf;


public interface InterfaceSettingConnectFi {
	public void onRegisterOn();
	public void onRegisterOff();
    public void onCertificationLogin();
}
