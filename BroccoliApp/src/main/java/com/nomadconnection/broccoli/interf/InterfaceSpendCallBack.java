package com.nomadconnection.broccoli.interf;


public interface InterfaceSpendCallBack {
	public void onCallBackComplete();
	public void onCallBackCompleteBudget(boolean value);
}
