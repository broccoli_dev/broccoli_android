package com.nomadconnection.broccoli.interf;

/**
 * Created by YelloHyunminJang on 2017. 3. 16..
 */

public interface InterfaceSpendBudgetList {

    boolean isGroupCategory();
    boolean isUnclassified();
    int getCategoryId();
    long getExpense();
    long getBudget();
    long getLeftExpense();
    long getLeftBudget();
}
