package com.nomadconnection.broccoli.interf;

/**
 * Created by YelloHyunminJang on 16. 6. 21..
 */
public interface InterfaceScrollable {
    boolean canScroll();
}
