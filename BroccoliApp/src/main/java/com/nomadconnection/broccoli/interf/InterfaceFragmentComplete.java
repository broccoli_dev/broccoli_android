package com.nomadconnection.broccoli.interf;

/**
 * Created by YelloHyunminJang on 2017. 2. 1..
 */

public interface InterfaceFragmentComplete {
    void complete();
    void reset();
    void edit();
    void delete();
}
