package com.nomadconnection.broccoli.interf;


import com.nomadconnection.broccoli.data.Remit.RemitBankAccount;
import com.nomadconnection.broccoli.data.Remit.RemitContent;

public interface InterfaceRemitSend {
	public void onRemitSendPhone(RemitContent remitContent);
	public void onRemitSendAccount(RemitBankAccount remitBankAccount);
    public void onRemitSendSet(int value, String sum); // true : 전화번호 송금  //false : 계좌번호송금
}
