package com.nomadconnection.broccoli.interf;

/**
 * Created by YelloHyunminJang on 2017. 3. 21..
 */

public interface OnRefreshListener {
    void refresh();
}
