package com.nomadconnection.broccoli.interf;

/**
 * Created by YelloHyunminJang on 16. 5. 19..
 */
public interface InterfaceFragmentInteraction {
    void reset(boolean flag);
    boolean canScrollUp();
    boolean canScrollDown();
}
