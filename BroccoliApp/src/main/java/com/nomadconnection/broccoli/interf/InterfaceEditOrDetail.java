package com.nomadconnection.broccoli.interf;


public interface InterfaceEditOrDetail {
	public void onEdit();
	public void onDetail();
    public void onError();
	public void onRefresh();
}
