package com.nomadconnection.broccoli.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;


public class DialogNoticeTwoButton extends Dialog implements OnClickListener {

//	private LinearLayout	mIncLayout;
	private Button 			mCancelBtn;
	private Button 			mConfirmBtn;
	private View			mLine;
	private CheckBox		mCheckBox;

	private Activity mActivity;
	private OnClickListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;


//	InterFaceDialog mInterFaceDialog;


//===================================================================//
// constructor
//===================================================================//

	public DialogNoticeTwoButton(Activity _activity, OnClickListener _btnClickListener, InfoDataDialog _infoDataDialog) {
		super(_activity);
		this.mActivity = _activity;
		this.mBtnClickListener = _btnClickListener;
		this.mInfoDataDialog = _infoDataDialog;
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			
			setContentView(R.layout.dialog_base_two_button);
			
			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);
			
			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseTwoButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;
		
		/* Include */
		View mAddView = mActivity.getLayoutInflater().inflate(mInfoDataDialog.mLayout, null);
		((LinearLayout)findViewById(R.id.ll_dialog_base_two_button_inc)).addView(mAddView);
		
		/* Layout 속성 */
		mCancelBtn		= (Button)findViewById(R.id.btn_dialog_base_two_button_cancel);
		mConfirmBtn		= (Button)findViewById(R.id.btn_dialog_base_two_button_confirm);
		mLine			= (View)findViewById(R.id.view_dialog_base_two_button_divider);
		mCheckBox		= (CheckBox) findViewById(R.id.checkbox_dialog_show_never_again);
		mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

			}
		});
		
		mCancelBtn.setOnClickListener(this);
		mConfirmBtn.setOnClickListener(this);
		
		
		/* Data Set */
		if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_em_notice) {
			if (mInfoDataDialog.mNotice.reOpenYn) {
				mCancelBtn.setVisibility(View.VISIBLE);
				mLine.setVisibility(View.VISIBLE);
				mCancelBtn.setText(mInfoDataDialog.mTwoBtnButtonTextL);
			} else {
				mCancelBtn.setVisibility(View.GONE);
				mLine.setVisibility(View.GONE);
			}
			mConfirmBtn.setText(mInfoDataDialog.mTwoBtnButtonTextR);

			((TextView)findViewById(R.id.tv_dialog_inc_case_em_notice_title)).setText(mInfoDataDialog.mNotice.title);

//			if(mInfoDataDialog.mNotice.getPostDate() != null){
//				String date = ElseUtils.getDateSettingsNotice(mInfoDataDialog.mNotice.getPostDate());
//				((TextView) findViewById(R.id.tv_dialog_inc_case_em_notice_date)).setText(date);
//			}

			((TextView)findViewById(R.id.tv_dialog_inc_case_em_notice_content)).setText(mInfoDataDialog.mNotice.contents);

			((TextView) findViewById(R.id.tv_dialog_inc_case_em_notice_term)).setVisibility(View.GONE);
//			if(mInfoDataDialog.mNotice.getPostFrom().equals("")){
//				((TextView) findViewById(R.id.tv_dialog_inc_case_em_notice_term)).setVisibility(View.GONE);
//			}
//			else {
//				String termDate = mInfoDataDialog.mNotice.getPostFrom() + "~" + mInfoDataDialog.mNotice.getPostTo();
//				((TextView) findViewById(R.id.tv_dialog_inc_case_em_notice_term)).setVisibility(View.VISIBLE);
//				((TextView) findViewById(R.id.tv_dialog_inc_case_em_notice_term)).setText(termDate);
//			}
		} else {

		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_dialog_base_two_button_cancel:
			if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_em_notice) {
				dismiss();
				mBtnClickListener.onClick(this, APP.DIALOG_BASE_TWO_BTN_CANCEL);
			}
			else{
				dismiss();
			}
			break;
			
		case R.id.btn_dialog_base_two_button_confirm:
			if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_em_notice) {
				mBtnClickListener.onClick(this, APP.DIALOG_BASE_TWO_BTN_CONFIRM);
				dismiss();
			}
			else{
				dismiss();
			}
			break;
			
		default:
			break;
		}
	}
	
	
	
}
