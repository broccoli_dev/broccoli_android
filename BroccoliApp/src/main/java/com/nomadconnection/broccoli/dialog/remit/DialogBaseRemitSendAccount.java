package com.nomadconnection.broccoli.dialog.remit;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendPhone;
import com.nomadconnection.broccoli.utils.ElseUtils;


public class DialogBaseRemitSendAccount extends Dialog implements OnClickListener {

	private Button 			mBtn;
	private TextView		mRemitSum;
	private TextView		mRemitReceive;
	private TextView		mRemitReceiveAccount;
	private LinearLayout	mRemitAccount;
	private TextView		mRemitForward;
	private TextView		mRemitForwardTime;
	private LinearLayout	mRemitWallet;
	private TextView		mRemitWalletSum;
	private TextView		mRemitWalletTime;

	private Activity mActivity;
	private OnClickListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;
	private RequestDataSendAccount mRequestDataSendAccount;


//	InterFaceDialog mInterFaceDialog;



//===================================================================//
// constructor
//===================================================================//

	public DialogBaseRemitSendAccount(Activity _activity, OnClickListener _btnClickListener, InfoDataDialog _infoDataDialog, RequestDataSendAccount _requestDataSendAccount) {
		super(_activity);
		this.mActivity = _activity;
		this.mBtnClickListener = _btnClickListener;
		this.mInfoDataDialog = _infoDataDialog;
		this.mRequestDataSendAccount = _requestDataSendAccount;
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			
			setContentView(R.layout.dialog_base_one_button_no_title);

			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);

			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseOneButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;
		
		
		/* Include */
		View mAddView = mActivity.getLayoutInflater().inflate(mInfoDataDialog.mLayout, null);
		((LinearLayout)findViewById(R.id.ll_dialog_base_one_button_no_title_inc)).addView(mAddView);

		/* Layout 속성 */
		mBtn		= (Button)findViewById(R.id.btn_dialog_base_one_button_no_title_click);

		mRemitSum		= (TextView)findViewById(R.id.tv_dialog_inc_remit_send_account_cost);
		mRemitReceive	= (TextView)findViewById(R.id.tv_dialog_inc_remit_send_account_receive_name);
		mRemitReceiveAccount = (TextView)findViewById(R.id.tv_dialog_inc_remit_send_account_receive_account);

		mRemitAccount = (LinearLayout)findViewById(R.id.ll_dialog_inc_remit_send_account_f);
		mRemitForward = (TextView)findViewById(R.id.tv_dialog_inc_remit_send_account_withdraw_account);
		mRemitForwardTime = (TextView)findViewById(R.id.tv_dialog_inc_remit_send_account_withdraw_time);

		mRemitWallet = (LinearLayout)findViewById(R.id.ll_dialog_inc_remit_send_account_wallet);
		mRemitWalletSum = (TextView)findViewById(R.id.tv_dialog_inc_remit_send_account_wallet_cost);
		mRemitWalletTime = (TextView)findViewById(R.id.tv_dialog_inc_remit_send_account_wallet_time);

		mBtn.setText(mInfoDataDialog.mTwoBtnButtonTextL);
		mBtn.setOnClickListener(this);
		
		
		/* Data Set */
		if (mInfoDataDialog.mLayout == R.layout.dialog_inc_remit_send_account) {
			mRemitSum.setText(ElseUtils.getStringDecimalFormat(mRequestDataSendAccount.getSendMoney()));
			mRemitReceive.setText(mRequestDataSendAccount.getOpponent());
			mRemitReceiveAccount.setText(mRequestDataSendAccount.getAccountNumber());

			if("A".equals(mRequestDataSendAccount.getSendKind())){
				mRemitAccount.setVisibility(View.VISIBLE);
				mRemitWallet.setVisibility(View.GONE);
				mRemitForward.setText(mInfoDataDialog.mCase2Text1);
				mRemitForwardTime.setText(ElseUtils.getDateNotice(mInfoDataDialog.mCase2Text2));
			}
			else {
				mRemitAccount.setVisibility(View.GONE);
				mRemitWallet.setVisibility(View.VISIBLE);
				mRemitWalletSum.setText(ElseUtils.getStringDecimalFormat(mInfoDataDialog.mCase2Text1));
				mRemitWalletTime.setText(ElseUtils.getDateNotice(mInfoDataDialog.mCase2Text2));
			}

		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.btn_dialog_base_one_button_no_title_click:
			mBtnClickListener.onClick(this, APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK);
			dismiss();
			break;

		default:
			break;
		}
	}
}
