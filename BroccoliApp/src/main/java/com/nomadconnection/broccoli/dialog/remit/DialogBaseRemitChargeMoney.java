package com.nomadconnection.broccoli.dialog.remit;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendPhone;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitChargeMoney;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;


public class DialogBaseRemitChargeMoney extends Dialog implements OnClickListener {

	private Button 			mBtn;
	private TextView		mRemitChargeSum;
	private TextView		mRemitWalletSum;
	private TextView		mRemitForward;
	private TextView		mRemitForwardTime;

	private Activity mActivity;
	private OnClickListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;
	private ResponseRemitChargeMoney mResponseRemitChargeMoney;


//	InterFaceDialog mInterFaceDialog;



//===================================================================//
// constructor
//===================================================================//

	public DialogBaseRemitChargeMoney(Activity _activity, OnClickListener _btnClickListener, InfoDataDialog _infoDataDialog, ResponseRemitChargeMoney _responseRemitChargeMoney) {
		super(_activity);
		this.mActivity = _activity;
		this.mBtnClickListener = _btnClickListener;
		this.mInfoDataDialog = _infoDataDialog;
		this.mResponseRemitChargeMoney = _responseRemitChargeMoney;
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			
			setContentView(R.layout.dialog_base_one_button_no_title);

			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);

			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseOneButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;
		
		
		/* Include */
		View mAddView = mActivity.getLayoutInflater().inflate(mInfoDataDialog.mLayout, null);
		((LinearLayout)findViewById(R.id.ll_dialog_base_one_button_no_title_inc)).addView(mAddView);

		/* Layout 속성 */
		mBtn		= (Button)findViewById(R.id.btn_dialog_base_one_button_no_title_click);

		mRemitChargeSum		= (TextView)findViewById(R.id.tv_dialog_inc_remit_charge_cost);
		mRemitWalletSum	= (TextView)findViewById(R.id.tv_dialog_inc_remit_charge_wallet_cost);
		mRemitForward = (TextView)findViewById(R.id.tv_dialog_inc_remit_charge_account);
		mRemitForwardTime = (TextView)findViewById(R.id.tv_dialog_inc_remit_charge_account_time);

		mBtn.setText(mInfoDataDialog.mTwoBtnButtonTextL);
		mBtn.setOnClickListener(this);
		
		
		/* Data Set */
		if (mInfoDataDialog.mLayout == R.layout.dialog_inc_remit_charge) {
			String account = TransFormatUtils.transBankCodeToName(mResponseRemitChargeMoney.getCompanyCode())
					+ " " + mResponseRemitChargeMoney.getAccountNumber();
			mRemitChargeSum.setText(ElseUtils.getStringDecimalFormat(mInfoDataDialog.mCase2Text1));
			mRemitWalletSum.setText(ElseUtils.getStringDecimalFormat(mResponseRemitChargeMoney.getBalance()));
			mRemitForward.setText(account);
			mRemitForwardTime.setText(ElseUtils.getDateNotice(mResponseRemitChargeMoney.getDate()));

		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.btn_dialog_base_one_button_no_title_click:
			mBtnClickListener.onClick(this, APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK);
			dismiss();
			break;

		default:
			break;
		}
	}
}
