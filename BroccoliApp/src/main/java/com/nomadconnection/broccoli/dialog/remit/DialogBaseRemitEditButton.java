package com.nomadconnection.broccoli.dialog.remit;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitContent;
import com.nomadconnection.broccoli.utils.ElseUtils;


public class DialogBaseRemitEditButton extends Dialog implements OnClickListener {

//	private LinearLayout	mIncLayout;
	private Button 			mCancelBtn;
	private Button 			mConfirmBtn;
	private View			mDivider;

	private Activity mActivity;
	private DialogInputTypeListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;

	private EditText		mName;
	private EditText		mNumber;
	private String			mTextWatcherResult="";


//	InterFaceDialog mInterFaceDialog;


//===================================================================//
// constructor
//===================================================================//

	public DialogBaseRemitEditButton(Activity _activity, DialogInputTypeListener _btnClickListener, InfoDataDialog _infoDataDialog) {
		super(_activity);
		this.mActivity = _activity;
		this.mBtnClickListener = _btnClickListener;
		this.mInfoDataDialog = _infoDataDialog;
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			
			setContentView(R.layout.dialog_base_two_button);
			
			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);
			
			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseTwoButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;
		
		/* Include */
		final View mAddView = mActivity.getLayoutInflater().inflate(mInfoDataDialog.mLayout, null);
		((LinearLayout)findViewById(R.id.ll_dialog_base_two_button_inc)).addView(mAddView);
		
		/* Layout 속성 */
		mCancelBtn		= (Button)findViewById(R.id.btn_dialog_base_two_button_cancel);
		mConfirmBtn		= (Button)findViewById(R.id.btn_dialog_base_two_button_confirm);
		mDivider 		= (View)findViewById(R.id.view_dialog_base_two_button_divider);
		
		mCancelBtn.setOnClickListener(this);
		mConfirmBtn.setOnClickListener(this);
		
		
		/* Data Set */
		if (mInfoDataDialog.mLayout == R.layout.dialog_inc_remit_case_1) {
			mName = (EditText)findViewById(R.id.et_dialog_inc_remit_case_1_name);
			mNumber = (EditText)findViewById(R.id.et_dialog_inc_remit_case_1_phone);
			mName.setText(mInfoDataDialog.mCase2Text1);

			if(mInfoDataDialog.mTwoBtnButtonTextL.equals("")){
				mCancelBtn.setVisibility(View.GONE);
				mDivider.setVisibility(View.GONE);
				mConfirmBtn.setText(mInfoDataDialog.mTwoBtnButtonTextR);
				mNumber.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {

					}

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {

					}

					@Override
					public void afterTextChanged(Editable s) {
						if (!s.toString().equals(mTextWatcherResult)) {
							if ("".equals(s.toString())) {
								mNumber.setHint(mActivity.getString(R.string.remit_register_text_third_phone_hint));
							} else {
								if(s.length() < 4){
									mTextWatcherResult = s.toString().replaceAll("-","");
								}
								else {
									mTextWatcherResult = PhoneNumberUtils.formatNumber(s.toString().replaceAll("-", ""));
								}
								mNumber.setText(mTextWatcherResult);
								mNumber.setSelection(mTextWatcherResult.length());
							}
						}
					}
				});
			}
			else {
				mCancelBtn.setVisibility(View.VISIBLE);
				mDivider.setVisibility(View.VISIBLE);
				mCancelBtn.setText(mInfoDataDialog.mTwoBtnButtonTextL);
				mConfirmBtn.setText(mInfoDataDialog.mTwoBtnButtonTextR);
//				mName.setBackgroundResource(0);
				mNumber.setBackgroundResource(0);
				mNumber.setFocusable(false);
				mNumber.setClickable(false);
				String tempNumber = mInfoDataDialog.mCase2Text2.replaceAll("-", "");
				mNumber.setText(ElseUtils.makePhoneNumber(tempNumber));
			}
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_dialog_base_two_button_cancel:
			if (mInfoDataDialog.mLayout == R.layout.dialog_inc_remit_case_1) {
				dismiss();
			}
			else{
				dismiss();
			}
			break;
			
		case R.id.btn_dialog_base_two_button_confirm:
			if (mInfoDataDialog.mLayout == R.layout.dialog_inc_remit_case_1) {
				if(mName.length() != 0 && mNumber.length() != 0){
					mBtnClickListener.onClick(this, APP.DIALOG_BASE_TWO_BTN_CONFIRM, getEditString());
					dismiss();
				}
				else {
					Toast.makeText(mActivity, mActivity.getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
				}

			}
			else{
				dismiss();
			}
			break;
			
		default:
			break;
		}
	}

	public interface DialogInputTypeListener {
		void onClick(DialogInterface dialog, int stringresid, RemitContent text);
	}

	public RemitContent getEditString(){
		RemitContent mRemitContent = new RemitContent();
		mRemitContent.setHostName(((EditText)findViewById(R.id.et_dialog_inc_remit_case_1_name)).getText().toString());
		mRemitContent.setTelNo(((EditText) findViewById(R.id.et_dialog_inc_remit_case_1_phone)).getText().toString());
		return mRemitContent;
	}


	
	
	
}
