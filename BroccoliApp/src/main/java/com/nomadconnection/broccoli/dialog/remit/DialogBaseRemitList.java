package com.nomadconnection.broccoli.dialog.remit;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewDlgBank;
import com.nomadconnection.broccoli.adapter.remit.AdtListViewDlgBankName;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeBank;
import com.nomadconnection.broccoli.data.InfoDataDialog;

import java.util.ArrayList;
import java.util.List;


public class DialogBaseRemitList extends Dialog implements OnClickListener, AdapterView.OnItemClickListener {

//	private LinearLayout	mIncLayout;
	private TextView 		mTitle;
	private Button 			mBtn;

	private Activity mActivity;
	private AdapterView.OnItemClickListener mItemClickListener;
	private OnClickListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;
	private int mSelectValue;
	private ArrayList<String> mBankNameList;


//	InterFaceDialog mInterFaceDialog;



//===================================================================//
// constructor
//===================================================================//

	public DialogBaseRemitList(Activity _activity, AdapterView.OnItemClickListener _itemClickListener, InfoDataDialog _infoDataDialog, int _selectValue, ArrayList<String> _BankNameList) {
		super(_activity);
		this.mActivity = _activity;
		this.mItemClickListener = _itemClickListener;
		this.mInfoDataDialog = _infoDataDialog;
		this.mSelectValue = _selectValue;
		this.mBankNameList = _BankNameList;
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			if (mInfoDataDialog.mLayout == APP.DIALOG_BASE_LIST_LARGE) {
				setContentView(R.layout.dialog_base_list);
			}
			else {
				setContentView(R.layout.dialog_base_list_240);
			}
			
			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);
			
			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseOneButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;

		/* Layout 속성 */
		if (mInfoDataDialog.mLayout == APP.DIALOG_BASE_LIST_LARGE) {
			mTitle		= (TextView)findViewById(R.id.tv_dialog_base_list_title);
			mBtn		= (Button)findViewById(R.id.btn_dialog_base_list_click);

			mTitle.setText(mInfoDataDialog.mOneBtnTitle);
			mBtn.setText(mInfoDataDialog.mOneBtnButtonText);
			mBtn.setOnClickListener(this);
		}
		else {
			mTitle		= (TextView)findViewById(R.id.tv_dialog_base_one_button_title);
			mBtn		= (Button)findViewById(R.id.btn_dialog_base_one_button_click);

			mTitle.setText(mInfoDataDialog.mOneBtnTitle);
			mBtn.setText(mInfoDataDialog.mOneBtnButtonText);
			mBtn.setOnClickListener(this);

		}
		
		/* Data Set */
		if(mInfoDataDialog.mLayout == APP.DIALOG_BASE_LIST_LARGE){
			AdtListViewDlgBankName adapter= new AdtListViewDlgBankName(mActivity, mBankNameList);
			((ListView)findViewById(R.id.lv_dialog_inc_case_3_listview)).setAdapter(adapter);
			((ListView)findViewById(R.id.lv_dialog_inc_case_3_listview)).setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			((ListView)findViewById(R.id.lv_dialog_inc_case_3_listview)).setOnItemClickListener(this);
			adapter.setSelectValue(mSelectValue);
		}
		else {
			AdtListViewDlgBankName adapter= new AdtListViewDlgBankName(mActivity, mBankNameList);
			((ListView)findViewById(R.id.lv_dialog_inc_case_3_listview)).setAdapter(adapter);
			((ListView)findViewById(R.id.lv_dialog_inc_case_3_listview)).setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			((ListView)findViewById(R.id.lv_dialog_inc_case_3_listview)).setOnItemClickListener(this);
			adapter.setSelectValue(mSelectValue);
		}
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			
		case 123123:
		case R.id.btn_dialog_base_list_click:
		case R.id.btn_dialog_base_one_button_click:
			dismiss();
//			mInterFaceDialog.onOneButtonClick();
//			mBtnClickListener.onClick(this, APP.DIALOG_BASE_ONE_BTN_CANCEL);
			break;
			
		default:
			break;
		}
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mItemClickListener.onItemClick(parent, view, position, id);
		dismiss();
		//Toast.makeText(mActivity, "position : " + position, Toast.LENGTH_SHORT).show();
	}
}
