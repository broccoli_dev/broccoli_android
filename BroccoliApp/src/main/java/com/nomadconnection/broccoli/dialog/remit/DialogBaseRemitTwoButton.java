package com.nomadconnection.broccoli.dialog.remit;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Splash;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitContent;
import com.nomadconnection.broccoli.utils.ElseUtils;


public class DialogBaseRemitTwoButton extends Dialog implements OnClickListener {

//	private LinearLayout	mIncLayout;
	private Button 			mCancelBtn;
	private Button 			mConfirmBtn;

	private Activity mActivity;
	private OnClickListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;


//	InterFaceDialog mInterFaceDialog;


//===================================================================//
// constructor
//===================================================================//

	public DialogBaseRemitTwoButton(Activity _activity, OnClickListener _btnClickListener, InfoDataDialog _infoDataDialog) {
		super(_activity);
		this.mActivity = _activity;
		this.mBtnClickListener = _btnClickListener;
		this.mInfoDataDialog = _infoDataDialog;
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			
			setContentView(R.layout.dialog_base_two_button);
			
			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);
			
			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseTwoButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;
		
		/* Include */
		View mAddView = mActivity.getLayoutInflater().inflate(mInfoDataDialog.mLayout, null);
		((LinearLayout)findViewById(R.id.ll_dialog_base_two_button_inc)).addView(mAddView);
		
		/* Layout 속성 */
		mCancelBtn		= (Button)findViewById(R.id.btn_dialog_base_two_button_cancel);
		mConfirmBtn		= (Button)findViewById(R.id.btn_dialog_base_two_button_confirm);
		
		mCancelBtn.setOnClickListener(this);
		mConfirmBtn.setOnClickListener(this);
		
		
		/* Data Set */
		if (mInfoDataDialog.mLayout == R.layout.dialog_inc_remit_case_2) {
			mCancelBtn.setText(mInfoDataDialog.mTwoBtnButtonTextL);
			mConfirmBtn.setText(mInfoDataDialog.mTwoBtnButtonTextR);
			((TextView)findViewById(R.id.tv_dialog_inc_remit_case_2_account_holder)).setText(mInfoDataDialog.mCase2Text1);
			((TextView)findViewById(R.id.tv_dialog_inc_remit_case_2_account_number)).setText(mInfoDataDialog.mCase2Text2);
		}

	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_dialog_base_two_button_cancel:
			if (mInfoDataDialog.mLayout == R.layout.dialog_inc_remit_case_1) {
				dismiss();
			}
			else{
				dismiss();
			}
//			mInterFaceDialog.onTwoButtonCancel();
//			mBtnClickListener.onClick(this, APP.DIALOG_BASE_TWO_BTN_CANCEL);
			break;
			
		case R.id.btn_dialog_base_two_button_confirm:
			if (mInfoDataDialog.mLayout == R.layout.dialog_inc_remit_case_1) {
				mBtnClickListener.onClick(this, APP.DIALOG_BASE_TWO_BTN_CONFIRM);
				dismiss();
			}
			else{
				mBtnClickListener.onClick(this, APP.DIALOG_BASE_TWO_BTN_CONFIRM);
				dismiss();
			}
//			mInterFaceDialog.onTwoButtonConfirm();
//			mBtnClickListener.onClick(this, APP.DIALOG_BASE_TWO_BTN_CONFIRM);
			break;
			
		default:
			break;
		}
	}
}
