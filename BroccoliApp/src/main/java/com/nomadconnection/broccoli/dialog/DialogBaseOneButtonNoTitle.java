package com.nomadconnection.broccoli.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;


public class DialogBaseOneButtonNoTitle extends Dialog implements OnClickListener {

	private Button 			mBtn;

	private Activity mActivity;
	private OnClickListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;


//	InterFaceDialog mInterFaceDialog;



//===================================================================//
// constructor
//===================================================================//

	public DialogBaseOneButtonNoTitle(Activity _activity, OnClickListener _btnClickListener, InfoDataDialog _infoDataDialog) {
		super(_activity);
		this.mActivity = _activity;
		this.mBtnClickListener = _btnClickListener;
		this.mInfoDataDialog = _infoDataDialog;
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			
			setContentView(R.layout.dialog_base_one_button_no_title);

			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);

			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseOneButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;
		
		
		/* Include */
		View mAddView = mActivity.getLayoutInflater().inflate(mInfoDataDialog.mLayout, null);
		((LinearLayout)findViewById(R.id.ll_dialog_base_one_button_no_title_inc)).addView(mAddView);

		/* Layout 속성 */
		mBtn		= (Button)findViewById(R.id.btn_dialog_base_one_button_no_title_click);

		mBtn.setText(mInfoDataDialog.mOneBtnButtonText);
		mBtn.setOnClickListener(this);
		
		
		/* Data Set */
		if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_1) {
			((TextView)findViewById(R.id.tv_dialog_inc_case_1_text)).setText(mInfoDataDialog.mOneBtnTitle);
		}
		else if(mInfoDataDialog.mLayout == R.layout.dialog_inc_case_9){
			String tempText = String.format(mActivity.getResources().getString(R.string.dialog_broccoli_register_init), mInfoDataDialog.mOneBtnTitle);
			((TextView) findViewById(R.id.tv_dialog_inc_case_9_text_1)).setText(tempText);
		}
		
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.btn_dialog_base_one_button_no_title_click:
			mBtnClickListener.onClick(this, APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK);
			dismiss();
			break;

		default:
			break;
		}
	}
}
