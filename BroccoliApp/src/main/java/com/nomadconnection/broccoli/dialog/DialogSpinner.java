package com.nomadconnection.broccoli.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewSpinner;

import java.util.ArrayList;

public class DialogSpinner extends Dialog{

	private Activity 								mActivity;
	private AdtListViewSpinner						mAdapter;
	private ListView								mListView;
	private ArrayList<String>						mData;
	private int									mPositioin;
	private Dialog.OnClickListener					mBtnClickListener;
	
	
	public DialogSpinner(Activity _activity, OnClickListener _btnClickListener, ArrayList<String> _data, int _pos) {
		super(_activity);
		this.mActivity = _activity;
		this.mData			= _data;
		this.mPositioin		= _pos;
		this.mBtnClickListener		= _btnClickListener;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			this.requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.dialog_spinner);
			
			init();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void init(){
		/* 리스트뷰세팅 */
		mListView = (ListView)this.findViewById(R.id.lv_dialog_spinner_list);
		mAdapter = new AdtListViewSpinner(this, mData, mPositioin);
		mListView.setAdapter(mAdapter);
		
		/* 리스트뷰 포커스 세팅 */
		mListView.setSelection(mData.indexOf(mPositioin));

		//이벤트 세팅
		mListView.setOnItemClickListener(ListViewOnItemClickListener);

	}


	//리스트뷰이벤트
	private OnItemClickListener ListViewOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
			mBtnClickListener.onClick(DialogSpinner.this, position);
//			dismiss();
		}

	};
	
	public void onBackPressed() {
		dismiss();
	};
	
}
