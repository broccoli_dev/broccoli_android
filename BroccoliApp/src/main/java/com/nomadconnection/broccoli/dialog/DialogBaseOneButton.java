package com.nomadconnection.broccoli.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListDlgRadio;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class DialogBaseOneButton extends Dialog implements OnClickListener {
	
//	private LinearLayout	mIncLayout;
	private TextView 		mTitle;
	private TextView 		mRemainTimer;
	private Button 			mBtn;
	
	private Activity mActivity;
	private OnClickListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;
	
	
//	InterFaceDialog mInterFaceDialog;
	

	
//===================================================================//
// constructor
//===================================================================//
	
	public DialogBaseOneButton(Activity _activity, OnClickListener _btnClickListener, InfoDataDialog _infoDataDialog) {
		super(_activity);
		this.mActivity = _activity;
		this.mBtnClickListener = _btnClickListener;
		this.mInfoDataDialog = _infoDataDialog;
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			
//			setContentView(R.layout.dialog_base_one_button);

			if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_3 || mInfoDataDialog.mLayout == R.layout.dialog_inc_case_6) {
				setContentView(R.layout.dialog_base_one_button_240);
			}
			else{
				setContentView(R.layout.dialog_base_one_button);
			}
			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);

			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseOneButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;
		
		
		/* Include */
		View mAddView = mActivity.getLayoutInflater().inflate(mInfoDataDialog.mLayout, null);
//		((LinearLayout)findViewById(R.id.ll_dialog_base_one_button_inc)).setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		((LinearLayout)findViewById(R.id.ll_dialog_base_one_button_inc)).addView(mAddView);

		/* Layout 속성 */
		mTitle		= (TextView)findViewById(R.id.tv_dialog_base_one_button_title);
		mBtn		= (Button)findViewById(R.id.btn_dialog_base_one_button_click);
		
		mTitle.setText(mInfoDataDialog.mOneBtnTitle);
		mBtn.setText(mInfoDataDialog.mOneBtnButtonText);
		mBtn.setOnClickListener(this);
		
		
		/* Data Set */
		if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_3) {
			int mSetPeriod = 0;
			if (mInfoDataDialog.mOneBtnTitle.equals(mActivity.getResources().getString(R.string.dialog_alarm_repeat_title))){
//				int mTempSetPeriod = MainApplication.mAPPShared.getPrefInt("refresh_time");
				mSetPeriod = Integer.valueOf(TransFormatUtils.transDialogRadioAlarmNameToPos(mInfoDataDialog.mCase3Text));
				String[] mRadioPeriod = mActivity.getResources().getStringArray(R.array.dlg_radio_period_name);
				List<String> mListRadio = new ArrayList<String>(Arrays.asList(mRadioPeriod));

				AdtListDlgRadio adapter= new AdtListDlgRadio(mActivity, mSetPeriod , mHandlerRadio, mListRadio);
				((ListView)findViewById(R.id.lv_dialog_inc_case_3_listview)).setAdapter(adapter);
			}
			else if (mInfoDataDialog.mOneBtnTitle.equals(mActivity.getResources().getString(R.string.dialog_pay_type_title))){
				mSetPeriod = Integer.valueOf(TransFormatUtils.transDialogRadioTypeNameToPos(mInfoDataDialog.mCase3Text));
				String[] mRadioPeriod = mActivity.getResources().getStringArray(R.array.dlg_radio_type);
				List<String> mListRadio = new ArrayList<String>(Arrays.asList(mRadioPeriod));

				AdtListDlgRadio adapter= new AdtListDlgRadio(mActivity, mSetPeriod , mHandlerRadio, mListRadio);
				((ListView)findViewById(R.id.lv_dialog_inc_case_3_listview)).setAdapter(adapter);
			}

		}
		else if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_6) {
			((LinearLayout)findViewById(R.id.ll_dialog_inc_case_6_house)).setOnClickListener(this);
			((LinearLayout)findViewById(R.id.ll_dialog_inc_case_6_car)).setOnClickListener(this);
			((RadioButton)findViewById(R.id.rb_dialog_inc_case_6_house)).setFocusable(false);
			((RadioButton)findViewById(R.id.rb_dialog_inc_case_6_house)).setClickable(false);

			((RadioButton)findViewById(R.id.rb_dialog_inc_case_6_car)).setFocusable(false);
			((RadioButton)findViewById(R.id.rb_dialog_inc_case_6_car)).setClickable(false);
		}
		else if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_7) {
			((LinearLayout)findViewById(R.id.ll_dialog_base_one_button_title_layout)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.tv_dialog_inc_case_7_text_1)).setText(mInfoDataDialog.mCase1Text);
		} else if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_1) {
			mTitle.setTextSize(15);
		} else if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_10) {
			mRemainTimer = (TextView) findViewById(R.id.tv_dialog_inc_case_10_text);
			startCountDown();
		}
//		else if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_4) {
////			mBtn.setVisibility(View.GONE);
////			Button btn = new Button(mActivity);
////			btn.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, mBtn.getHeight()));
////			btn.setBackground(mBtn.getBackground());
////			btn.setText("test");
////			btn.setTextColor(mBtn.getTextColors());
////			btn.setId(123123);
////			btn.setOnClickListener(this);
////			((LinearLayout)findViewById(R.id.ll_dialog_base_one_button_inc)).addView(btn);
//			
//			String[] mCategory = mActivity.getResources().getStringArray(R.array.category_list);
//			List<String> mListCategory = new ArrayList<String>(Arrays.asList(mCategory));
//			
//			AdtGridViewDlgCategory adapter= new AdtGridViewDlgCategory(mActivity, mListCategory);
//			((GridView)findViewById(R.id.gv_dialog_inc_case_4_list)).setAdapter(adapter);
//		}
		
	}

	private CountDownTimer mCountDownTimer;
	private StringBuffer mBuffer = new StringBuffer();

	private void startCountDown() {
		cancelTimer();
		String time = ElseUtils.getLockDownTime(getOwnerActivity());
		long releaseTime = Long.valueOf(time);
		long currentTime = Calendar.getInstance().getTimeInMillis();
		long remainTime = releaseTime - currentTime;
		if (remainTime > 0) {
			if (mRemainTimer != null) {
				mCountDownTimer = new CountDownTimer(remainTime, 1000) {
					@Override
					public void onTick(long millisUntilFinished) {
						int days = (int) ((millisUntilFinished / 1000) / 86400);
						int hours = (int) (((millisUntilFinished / 1000) - (days
								* 86400)) / 3600);

						int mins = (int) (((millisUntilFinished / 1000) - ((days
								* 86400) + (hours * 3600))) / 60);
						int secs = (int) ((millisUntilFinished / 1000) % 60);
						if (mBuffer.length() > 0)
							mBuffer.delete(0, mBuffer.length());
						if (mins > 0) {
							mBuffer.append(mins);
							mBuffer.append("분");
						}
						mBuffer.append(secs);
						mBuffer.append("초");
						mRemainTimer.setText(mBuffer.toString());
					}

					@Override
					public void onFinish() {
						mRemainTimer.setText("재시작해주세요.");
					}
				};
				mCountDownTimer.start();
			}
		}
	}

	private void cancelTimer() {
		if (mCountDownTimer != null) {
			mCountDownTimer.cancel();
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			
		case 123123:
		case R.id.btn_dialog_base_one_button_click:
//			if (mInfoDataDialog.mLayout == R.layout.dialog_inc_case_7 &&
//					mInfoDataDialog.mCase1Text.equalsIgnoreCase("선결제를 하거나 연체가 된 경우 카드 데이터가 정상적으로 보이지 않을 수 있습니다.")) {
//				MainApplication.mAPPShared.setPrefBool(APP.SP_ONLY_ONE_CHECK_ASSET_CARD_DETAIL_ENTER, true);
//			}
			dismiss();
//			mInterFaceDialog.onOneButtonClick();
//			mBtnClickListener.onClick(this, APP.DIALOG_BASE_ONE_BTN_CANCEL);
			break;
		case R.id.ll_dialog_inc_case_6_house:
			mBtnClickListener.onClick(this, APP.DIALOG_ASSET_OTHER_HOUSE);
			((RadioButton)findViewById(R.id.rb_dialog_inc_case_6_house)).setChecked(true);
			break;

		case R.id.ll_dialog_inc_case_6_car:
			mBtnClickListener.onClick(this, APP.DIALOG_ASSET_OTHER_CAR);
			((RadioButton)findViewById(R.id.rb_dialog_inc_case_6_car)).setChecked(true);
			break;

		default:
			break;
		}
	}
	
	private Handler mHandlerRadio = new Handler(){

		@Override
		public void dispatchMessage(Message msg) {
			
			int index = msg.what;
			mBtnClickListener.onClick(DialogBaseOneButton.this, index);
//			MainApplication.mAPPShared.setPrefInt("refresh_time", index);
			dismiss();
		}
	};
	
	
}
