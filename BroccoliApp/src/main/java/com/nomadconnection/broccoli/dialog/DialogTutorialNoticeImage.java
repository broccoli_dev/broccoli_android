package com.nomadconnection.broccoli.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;


public class DialogTutorialNoticeImage extends Dialog  implements View.OnClickListener{

//	private Handler mHandler;
//	private Runnable mRunnable;

	private LinearLayout	mLLTutorial;
	private ImageView		mTutorialImage;

	private Activity mActivity;
	private OnClickListener mBtnClickListener;


//===================================================================//
// constructor
//===================================================================//

	public DialogTutorialNoticeImage(Activity _activity, OnClickListener _btnClickListener) {
		super(_activity);
		this.mActivity = _activity;
		this.mBtnClickListener = _btnClickListener;
	}




//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			getWindow().clearFlags(LayoutParams.FLAG_DIM_BEHIND);

			setContentView(R.layout.dialog_tutorial_notice_image);
			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);

			init();

		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseOneButton onCreate() : " + e.toString());
		}
	}


	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {

		/* Layout 속성 */
		mLLTutorial			= (LinearLayout)findViewById(R.id.ll_dialog_tutorial_notice_image);
		mLLTutorial.setOnClickListener(this);

		mTutorialImage		= (ImageView)findViewById(R.id.dialog_tutorial_notice_img);

//		mRunnable = new Runnable() {
//			@Override
//			public void run() {
//				mBtnClickListener.onClick(DialogTutorialNoticeImage.this, APP.DIALOG_BASE_TWO_BTN_CONFIRM);
//				dismiss();
//			}
//		};

//		mHandler = new Handler();
//		mHandler.postDelayed(mRunnable, 3000);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.ll_dialog_tutorial_notice_image:
				mBtnClickListener.onClick(this, APP.DIALOG_BASE_TWO_BTN_CONFIRM);
				dismiss();
				break;
		}

	}
}
