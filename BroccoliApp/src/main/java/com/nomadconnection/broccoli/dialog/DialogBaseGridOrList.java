package com.nomadconnection.broccoli.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtGridViewDlgCategory;
import com.nomadconnection.broccoli.adapter.AdtGridViewDlgOrg;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.BodyOrgInfo;
import com.nomadconnection.broccoli.data.InfoDataDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DialogBaseGridOrList extends Dialog implements OnClickListener, AdapterView.OnItemClickListener {
	
//	private LinearLayout	mIncLayout;
	private TextView 		mTitle;
	private Button 			mBtn;
	
	private Activity mActivity;
	private AdapterView.OnItemClickListener mItemClickListener;
	private OnClickListener mBtnClickListener;
	private InfoDataDialog mInfoDataDialog;
	private int mSelectValue;
	private String mSelectStr1 = "";
	private String mSelectStr2 = "";
	private boolean isSelectMode = false;
	private int mCurrentValue = -1;
	
//	InterFaceDialog mInterFaceDialog;
	

	
//===================================================================//
// constructor
//===================================================================//
	
	public DialogBaseGridOrList(Activity _activity, AdapterView.OnItemClickListener _itemClickListener, InfoDataDialog _infoDataDialog, int _selectValue) {
		super(_activity);
		this.mActivity = _activity;
		this.mItemClickListener = _itemClickListener;
		this.mInfoDataDialog = _infoDataDialog;
		this.mSelectValue = _selectValue;
	}

	public DialogBaseGridOrList(Activity _activity, AdapterView.OnItemClickListener _itemClickListener, InfoDataDialog _infoDataDialog, int _selectValue, String _selectStr1, String _selectStr2) {
		super(_activity);
		this.mActivity = _activity;
		this.mItemClickListener = _itemClickListener;
		this.mInfoDataDialog = _infoDataDialog;
		this.mSelectValue = _selectValue;
		this.mSelectStr1 = _selectStr1;
		this.mSelectStr2 = _selectStr2;
	}

	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			
			setContentView(R.layout.dialog_base_grid_or_list);
			
			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);
			
			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseOneButton onCreate() : " + e.toString());
		}
	}

	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {
		
		/* InterFaceDialog */
//		mInterFaceDialog = (InterFaceDialog) mActivity;
		
		
		/* Layout 속성 */
		mTitle		= (TextView)findViewById(R.id.tv_dialog_base_grid_or_list_title);
		mBtn		= (Button)findViewById(R.id.btn_dialog_base_grid_or_list_click);
		
		mTitle.setText(mInfoDataDialog.mOneBtnTitle);
		mBtn.setText(mInfoDataDialog.mOneBtnButtonText);
		mBtn.setOnClickListener(this);
		
		
		/* Data Set */
		if (mInfoDataDialog.mLayout == APP.DIALOG_BASE_GRID_OR_LIST_G) {
			String[] mCategory;
			isSelectMode = true;
			mCurrentValue = mSelectValue;

			mBtn.setVisibility(View.GONE);
			findViewById(R.id.btn_dialog_base_guide).setVisibility(View.VISIBLE);
			((LinearLayout)findViewById(R.id.ll_dialog_base_grid_or_list_sub_title_layout)).setVisibility(View.GONE);
			((GridView)findViewById(R.id.gv_dialog_base_grid_or_list_layout)).setNumColumns(mActivity.getResources().getInteger(R.integer.grid_num_cols));
			((GridView)findViewById(R.id.gv_dialog_base_grid_or_list_layout)).setColumnWidth(126);
			((GridView)findViewById(R.id.gv_dialog_base_grid_or_list_layout)).setPadding(87, 0, 87, 0);
			findViewById(R.id.btn_dialog_base_layout_category).setVisibility(View.VISIBLE);
			if(mSelectValue == 24){
				mCategory = mActivity.getResources().getStringArray(R.array.category_list);
			}
			else {
				mCategory = mActivity.getResources().getStringArray(R.array.category_list_16);
			}
			List<String> mListCategory = new ArrayList<String>(Arrays.asList(mCategory));
			
			AdtGridViewDlgCategory adapter= new AdtGridViewDlgCategory(mActivity, mListCategory);
			GridView gridView = (GridView)findViewById(R.id.gv_dialog_base_grid_or_list_layout);
			gridView.setAdapter(adapter);
			gridView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			gridView.setOnItemClickListener(this);
			adapter.setSelectValue(mSelectValue);
			Button btnOnetime = (Button) findViewById(R.id.btn_dialog_base_onetime);
			Button btnAlways = (Button) findViewById(R.id.btn_dialog_base_always);
			btnOnetime.setOnClickListener(this);
			btnAlways.setOnClickListener(this);
			if (mSelectValue == 24) {
				btnOnetime.setEnabled(false);
				btnOnetime.setTextColor(getContext().getResources().getColor(R.color.dialog_base_btn_txt_press));
				btnAlways.setEnabled(false);
				btnAlways.setTextColor(getContext().getResources().getColor(R.color.dialog_base_btn_txt_press));
			} else {
				btnOnetime.setEnabled(true);
				btnOnetime.setTextColor(getContext().getResources().getColorStateList(R.color.selector_dialog_base_btn_text_color_change));
				btnAlways.setEnabled(true);
				btnAlways.setTextColor(getContext().getResources().getColorStateList(R.color.selector_dialog_base_btn_text_color_change));
			}
		}
		else if (mInfoDataDialog.mLayout == APP.DIALOG_BASE_GRID_OR_LIST_L) {
			((LinearLayout)findViewById(R.id.ll_dialog_base_grid_or_list_sub_title_layout)).setVisibility(View.VISIBLE);
			((GridView)findViewById(R.id.gv_dialog_base_grid_or_list_layout)).setNumColumns(mActivity.getResources().getInteger(R.integer.list_num_cols));

			mInfoDataDialog.mArrBodyOrgInfo.add(0, new BodyOrgInfo("", "", "없음", 0, "0"));
			AdtGridViewDlgOrg adapter= new AdtGridViewDlgOrg(mActivity, mInfoDataDialog.mArrBodyOrgInfo);
			((GridView)findViewById(R.id.gv_dialog_base_grid_or_list_layout)).setAdapter(adapter);
			((GridView)findViewById(R.id.gv_dialog_base_grid_or_list_layout)).setOnItemClickListener(this);
//			adapter.setSelectValue(mSelectValue);
			adapter.setSelectStr(mSelectStr1, mSelectStr2);
		}
		
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case 123123:
			case R.id.btn_dialog_base_grid_or_list_click:
				dismiss();
	//			mInterFaceDialog.onOneButtonClick();
	//			mBtnClickListener.onClick(this, APP.DIALOG_BASE_ONE_BTN_CANCEL);
				break;
			case R.id.btn_dialog_base_onetime:
				mItemClickListener.onItemClick(null, v, mCurrentValue, R.id.btn_dialog_base_onetime);
				dismiss();
				break;
			case R.id.btn_dialog_base_always:
				mItemClickListener.onItemClick(null, v, mCurrentValue, R.id.btn_dialog_base_always);
				dismiss();
				break;
			default:
				break;
		}
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (isSelectMode) {
			mCurrentValue = position;
			((AdtGridViewDlgCategory)parent.getAdapter()).setSelectValue(mCurrentValue);
			((AdtGridViewDlgCategory)parent.getAdapter()).notifyDataSetChanged();
			Button btnOnetime = (Button) findViewById(R.id.btn_dialog_base_onetime);
			Button btnAlways = (Button) findViewById(R.id.btn_dialog_base_always);
			if (mCurrentValue == 24) {
				btnOnetime.setEnabled(false);
				btnOnetime.setTextColor(getContext().getResources().getColor(R.color.dialog_base_btn_txt_press));
				btnAlways.setEnabled(false);
				btnAlways.setTextColor(getContext().getResources().getColor(R.color.dialog_base_btn_txt_press));
			} else {
				btnOnetime.setEnabled(true);
				btnOnetime.setTextColor(getContext().getResources().getColorStateList(R.color.selector_dialog_base_btn_text_color_change));
				btnAlways.setEnabled(true);
				btnAlways.setTextColor(getContext().getResources().getColorStateList(R.color.selector_dialog_base_btn_text_color_change));
			}
		} else {
			mItemClickListener.onItemClick(parent, view, position, id);
			dismiss();
		}
	}
}
