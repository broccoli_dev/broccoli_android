package com.nomadconnection.broccoli.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;


public class DialogBaseLoading extends Dialog {

	private ImageView		mLoadingImage;
//	private TextView 		mLoadingText;
//	private Button 			mBtn;


//===================================================================//
// constructor
//===================================================================//

	public DialogBaseLoading(Context context) {
		super(context);
	}
	
	
	
	
//==============================================================//
// pubilc method
//==============================================================//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			LayoutParams lpWindow = new LayoutParams();
			lpWindow.flags = LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0f;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			getWindow().setAttributes(lpWindow);
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

			setContentView(R.layout.dialog_base_loading);
			/* width match 세팅 */
			LayoutParams params = this.getWindow().getAttributes();
		    params.width = LayoutParams.MATCH_PARENT;
		    params.height = LayoutParams.WRAP_CONTENT;
		    this.getWindow().setAttributes((LayoutParams) params);

			init();
		} catch (Exception e) {
			if(APP._DEBUG_MODE_)
				Log.e("", "DialogBaseOneButton onCreate() : " + e.toString());
		}
	}


	
//==============================================================//
// private method
//==============================================================//		
	
	private void init() {

		/* Layout 속성 */
		mLoadingImage		= (ImageView)findViewById(R.id.dialog_loading_img);
//		mLoadingText		= (TextView)findViewById(R.id.dialog_loading_status_title);

		AnimationDrawable frameAnimation = (AnimationDrawable) mLoadingImage.getDrawable();
		frameAnimation.setCallback(mLoadingImage);
		frameAnimation.setVisible(true, true);
		frameAnimation.start();

//		mLoadingText.setText(mActivity.getResources().getString(R.string.common_loading));
		
	}
}
