package com.nomadconnection.broccoli.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.utils.Crypto;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class ScrapDao extends DatabaseDao {

    private static final String WHERE_ID_EQUALS = ScrapDBColumn.BankDBColumns._ID + " =?";

    private static ScrapDao mInstance;

    public static synchronized ScrapDao getInstance() {
        if (mInstance == null) {
            mInstance = new ScrapDao();
        }
        return mInstance;
    }

    public long insertBankData(Context context, String key, String id, BankData bank) {
        if (key == null || bank == null|| (bank.getBankName() == null || TextUtils.isEmpty(bank.getBankName()))) {
            return -1;
        }
        ContentValues values = new ContentValues();
        try {
            values.put(ScrapDBColumn.BankDBColumns._ID, Crypto.encrypt(key, id+bank.getBankName()));
            values.put(ScrapDBColumn.BankDBColumns.NAME, Crypto.encrypt(key, bank.getBankName()));
            values.put(ScrapDBColumn.BankDBColumns.CODE, Crypto.encrypt(key, bank.getBankCode()));
            values.put(ScrapDBColumn.BankDBColumns.SERIAL, Crypto.encrypt(key, bank.getCertSerial()));
            values.put(ScrapDBColumn.BankDBColumns.PWD, Crypto.encrypt(key, bank.getPassWord()));
            values.put(ScrapDBColumn.BankDBColumns.TYPE, Crypto.encrypt(key, bank.getType()));
            values.put(ScrapDBColumn.BankDBColumns.VALID_TIME, Crypto.encrypt(key, bank.getValidDate()));
            values.put(ScrapDBColumn.BankDBColumns.TIME_UPLOAD, Crypto.encrypt(key, bank.getUploaded()));
            values.put(ScrapDBColumn.BankDBColumns.STATUS, Crypto.encrypt(key, bank.getStatus()));
            values.put(ScrapDBColumn.BankDBColumns.LOGIN_METHOD, Crypto.encrypt(key, bank.getLoginMethod()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getWritableDatabase(context).insert(ScrapDBColumn.BankDBColumns.TABLE_BANK, null, values);
    }

    public long updateBankData(Context context, String key, String id, BankData bank) {
        ContentValues values = new ContentValues();
        try {
            values.put(ScrapDBColumn.BankDBColumns._ID, Crypto.encrypt(key, id+bank.getBankName()));
            values.put(ScrapDBColumn.BankDBColumns.NAME, Crypto.encrypt(key, bank.getBankName()));
            values.put(ScrapDBColumn.BankDBColumns.CODE, Crypto.encrypt(key, bank.getBankCode()));
            values.put(ScrapDBColumn.BankDBColumns.SERIAL, Crypto.encrypt(key, bank.getCertSerial()));
            values.put(ScrapDBColumn.BankDBColumns.PWD, Crypto.encrypt(key, bank.getPassWord()));
            values.put(ScrapDBColumn.BankDBColumns.TYPE, Crypto.encrypt(key, bank.getType()));
            values.put(ScrapDBColumn.BankDBColumns.VALID_TIME, Crypto.encrypt(key, bank.getValidDate()));
            values.put(ScrapDBColumn.BankDBColumns.TIME_UPLOAD, Crypto.encrypt(key, bank.getUploaded()));
            values.put(ScrapDBColumn.BankDBColumns.STATUS, Crypto.encrypt(key, bank.getStatus()));
            values.put(ScrapDBColumn.BankDBColumns.LOGIN_METHOD, Crypto.encrypt(key, bank.getLoginMethod()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        long result = 0;
        try {
            result = getWritableDatabase(context).update(ScrapDBColumn.BankDBColumns.TABLE_BANK, values,
                    WHERE_ID_EQUALS,
                    new String[]{String.valueOf(Crypto.encrypt(key, id+bank.getBankName()))});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public int deleteBankData(Context context, String key, String id, BankData bankData) {
        int result = -1;
        try {
            result = getWritableDatabase(context).delete(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                    WHERE_ID_EQUALS, new String[]{Crypto.encrypt(key, id+bankData.getBankName()) + ""});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public int deleteBankData(Context context, String key, String id, String name) {
        int result = -1;
        try {
            result =  getWritableDatabase(context).delete(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                    WHERE_ID_EQUALS, new String[]{Crypto.encrypt(key, id+name) + ""});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public int getBankCount(Context context) {
        int count = 0;

        Cursor cursor = getReadableDatabase(context).query(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                null, null, null, null, null,
                null);
        count = cursor.getCount();
        return count;
    }

    public BankData getBankDataByName(Context context, String key, String name, String id) {
        BankData bank = new BankData();
        String encName = null;
        try {
            encName = Crypto.encrypt(key, id+name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Cursor cursor = getReadableDatabase(context).query(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                null, WHERE_ID_EQUALS, new String[]{encName}, null, null,
                null);


        while (cursor.moveToNext()) {
            try {
                bank.setId(Crypto.decrypt(key, cursor.getString(0)));
                bank.setBankName(Crypto.decrypt(key, cursor.getString(1)));
                bank.setBankCode(Crypto.decrypt(key, cursor.getString(2)));
                bank.setCertSerial(Crypto.decrypt(key, cursor.getString(3)));
                bank.setPassWord(Crypto.decrypt(key, cursor.getString(4)));
                bank.setType(Crypto.decrypt(key, cursor.getString(5)));
                bank.setValidDate(Crypto.decrypt(key, cursor.getString(6)));
                bank.setUploaded(Crypto.decrypt(key, cursor.getString(7)));
                bank.setStatus(Crypto.decrypt(key, cursor.getString(8)));
                bank.setLoginMethod(Crypto.decrypt(key, cursor.getString(9)));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return bank;
    }

    public List<BankData> getBankData(Context context, String key, String id) {
        List<BankData> bankList = new ArrayList<BankData>();
        Cursor cursor = getReadableDatabase(context).query(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                null, null, null, null, null,
                null);

        while (cursor.moveToNext()) {
            BankData bank = new BankData();
            try {
                String decId = Crypto.decrypt(key, cursor.getString(0));
                if (decId.contains(id)) {
                    bank.setId(Crypto.decrypt(key, cursor.getString(0)));
                    bank.setBankName(Crypto.decrypt(key, cursor.getString(1)));
                    bank.setBankCode(Crypto.decrypt(key, cursor.getString(2)));
                    bank.setCertSerial(Crypto.decrypt(key, cursor.getString(3)));
                    bank.setPassWord(Crypto.decrypt(key, cursor.getString(4)));
                    bank.setType(Crypto.decrypt(key, cursor.getString(5)));
                    bank.setValidDate(Crypto.decrypt(key, cursor.getString(6)));
                    bank.setUploaded(Crypto.decrypt(key, cursor.getString(7)));
                    bank.setStatus(Crypto.decrypt(key, cursor.getString(8)));
                    bank.setLoginMethod(Crypto.decrypt(key, cursor.getString(9)));
                    bankList.add(bank);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bankList;
    }

    public List<BankData> getValidBankData(Context context, String key, String id) {
        List<BankData> bankList = new ArrayList<BankData>();
        Cursor cursor = getReadableDatabase(context).query(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                null, null, null, null, null,
                null);

        while (cursor.moveToNext()) {
            String validDate = null;
            String status = null;
            try {
                validDate = Crypto.decrypt(key, cursor.getString(6));
                status = Crypto.decrypt(key, cursor.getString(8));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!ElseUtils.isExpireDate(validDate)
                    && (!BankData.FAIL_AUTH.equals(status) || BankData.FAIL_PASSWORD.equalsIgnoreCase(status))) {
                BankData bank = new BankData();
                try {
                    String decId = Crypto.decrypt(key, cursor.getString(0));
                    if (decId.contains(id)) {
                        bank.setId(Crypto.decrypt(key, cursor.getString(0)));
                        bank.setBankName(Crypto.decrypt(key, cursor.getString(1)));
                        bank.setBankCode(Crypto.decrypt(key, cursor.getString(2)));
                        bank.setCertSerial(Crypto.decrypt(key, cursor.getString(3)));
                        bank.setPassWord(Crypto.decrypt(key, cursor.getString(4)));
                        bank.setType(Crypto.decrypt(key, cursor.getString(5)));
                        bank.setValidDate(Crypto.decrypt(key, cursor.getString(6)));
                        bank.setUploaded(Crypto.decrypt(key, cursor.getString(7)));
                        bank.setStatus(Crypto.decrypt(key, cursor.getString(8)));
                        bank.setLoginMethod(Crypto.decrypt(key, cursor.getString(9)));
                        bankList.add(bank);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return bankList;
    }

    public List<BankData> getOldBankData(Context context, String key) {
        List<BankData> bankList = new ArrayList<BankData>();
        Cursor cursor = getReadableDatabase(context).query(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                null, null, null, null, null,
                null);

        while (cursor.moveToNext()) {
            BankData bank = new BankData();
            try {
                bank.setId(Crypto.decrypt(key, cursor.getString(0)));
                bank.setBankName(Crypto.decrypt(key, cursor.getString(1)));
                bank.setBankCode(Crypto.decrypt(key, cursor.getString(2)));
                bank.setCertSerial(Crypto.decrypt(key, cursor.getString(3)));
                bank.setPassWord(Crypto.decrypt(key, cursor.getString(4)));
                bank.setType(Crypto.decrypt(key, cursor.getString(5)));
                bank.setValidDate(Crypto.decrypt(key, cursor.getString(6)));
                bank.setUploaded(Crypto.decrypt(key, cursor.getString(7)));
                bank.setStatus(Crypto.decrypt(key, cursor.getString(8)));
                bank.setLoginMethod(Crypto.decrypt(key, cursor.getString(9)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            bankList.add(bank);
        }
        return bankList;
    }

    public List<BankData> getExpireBankData(Context context, String key) {
        List<BankData> bankList = new ArrayList<BankData>();
        Cursor cursor = getReadableDatabase(context).query(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                null, null, null, null, null,
                null);

        while (cursor.moveToNext()) {
            String validDate = null;
            String status = null;
            try {
                validDate = Crypto.decrypt(key, cursor.getString(6));
                status = Crypto.decrypt(key, cursor.getString(8));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (ElseUtils.isExpireDate(validDate)
                    && BankData.FAIL_AUTH.equals(status)) {
                BankData bank = new BankData();
                try {
                    bank.setId(Crypto.decrypt(key, cursor.getString(0)));
                    bank.setBankName(Crypto.decrypt(key, cursor.getString(1)));
                    bank.setBankCode(Crypto.decrypt(key, cursor.getString(2)));
                    bank.setCertSerial(Crypto.decrypt(key, cursor.getString(3)));
                    bank.setPassWord(Crypto.decrypt(key, cursor.getString(4)));
                    bank.setType(Crypto.decrypt(key, cursor.getString(5)));
                    bank.setValidDate(Crypto.decrypt(key, cursor.getString(6)));
                    bank.setUploaded(Crypto.decrypt(key, cursor.getString(7)));
                    bank.setStatus(Crypto.decrypt(key, cursor.getString(8)));
                    bank.setLoginMethod(Crypto.decrypt(key, cursor.getString(9)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                bankList.add(bank);
            }
        }
        return bankList;
    }

    public void removeBankData(Context context) {
        List<BankData> bankList = new ArrayList<BankData>();
        Cursor cursor = getReadableDatabase(context).query(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                null, null, null, null, null,
                null);

        while (cursor.moveToNext()) {
            BankData bank = new BankData();
            try {
                bank.setId(cursor.getString(0));
                bank.setBankName(cursor.getString(1));
                bank.setBankCode(cursor.getString(2));
                bank.setCertSerial(cursor.getString(3));
                bank.setPassWord(cursor.getString(4));
                bank.setType(cursor.getString(5));
                bank.setValidDate(cursor.getString(6));
                bank.setUploaded(cursor.getString(7));
                bank.setStatus(cursor.getString(8));
                bank.setLoginMethod(cursor.getString(9));
            } catch (Exception e) {
                e.printStackTrace();
            }
            bankList.add(bank);
        }

        for (BankData bankData : bankList){
            try {
                getWritableDatabase(context).delete(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                        WHERE_ID_EQUALS, new String[]{String.valueOf(bankData.getId())});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void removeBankData(Context context, String key, String id) {
        List<BankData> bankList = new ArrayList<BankData>();
        Cursor cursor = getReadableDatabase(context).query(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                null, null, null, null, null,
                null);

        while (cursor.moveToNext()) {
            BankData bank = new BankData();
            try {
                bank.setId(cursor.getString(0));
                bank.setBankName(cursor.getString(1));
                bank.setBankCode(cursor.getString(2));
                bank.setCertSerial(cursor.getString(3));
                bank.setPassWord(cursor.getString(4));
                bank.setType(cursor.getString(5));
                bank.setValidDate(cursor.getString(6));
                bank.setUploaded(cursor.getString(7));
                bank.setStatus(cursor.getString(8));
                bank.setLoginMethod(cursor.getString(9));
                String idBank = Crypto.decrypt(key, bank.getId());
                if (idBank.contains(id)) {
                    bankList.add(bank);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (BankData bankData : bankList){
            try {
                getWritableDatabase(context).delete(ScrapDBColumn.BankDBColumns.TABLE_BANK,
                        WHERE_ID_EQUALS, new String[]{String.valueOf(bankData.getId())});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clearBankData(Context context) throws Exception {
        SQLiteDatabase db = null;

        db = getWritableDatabase(context);

        try {
            db.beginTransaction();
            db.delete(ScrapDBColumn.BankDBColumns.TABLE_BANK, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.endTransaction();
                db.close();
            }
        }
    }

}
