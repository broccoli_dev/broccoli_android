package com.nomadconnection.broccoli.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nomadconnection.broccoli.data.Scrap.ServiceRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 3. 4..
 */
public class ServiceRecordDao extends DatabaseDao {
    private static final String WHERE_ID_EQUALS = ServiceStatusDBColumn.ServiceDBColumns._ID + " =?";

    private static ServiceRecordDao mInstance;

    public synchronized static ServiceRecordDao getInstance() {
        if (mInstance == null) {
            mInstance = new ServiceRecordDao();
        }
        return mInstance;
    }

    public void insertOrUpdateData(Context context, ServiceRecord record) {
        SQLiteDatabase db = getWritableDatabase(context);
        Cursor cursor = getDataCursor(db);
        try {
            ContentValues values = new ContentValues();
            values.put(ServiceStatusDBColumn.ServiceDBColumns._ID, record.getMode()+record.getName());
            values.put(ServiceStatusDBColumn.ServiceDBColumns.MODE, record.getMode());
            values.put(ServiceStatusDBColumn.ServiceDBColumns.STATUS, record.getStatus());
            values.put(ServiceStatusDBColumn.ServiceDBColumns.NAME, record.getName());
            values.put(ServiceStatusDBColumn.ServiceDBColumns.TYPE, record.getType());
            values.put(ServiceStatusDBColumn.ServiceDBColumns.RESULT, record.getResult());
            values.put(ServiceStatusDBColumn.ServiceDBColumns.TIME, record.getTime());
            values.put(ServiceStatusDBColumn.ServiceDBColumns.COUNT, record.getCount());

            if (cursor == null || cursor.getCount() == 0) {
                db.insertOrThrow(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME, null, values);
            } else {
                boolean isExist = false;
                while (cursor.moveToNext()) {
                    String mode = cursor.getString(cursor.getColumnIndex(ServiceStatusDBColumn.ServiceDBColumns._ID));
                    if (mode != null && mode.equals(record.getId())) {
                        db.update (ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME, values, ServiceStatusDBColumn.ServiceDBColumns._ID+"=?", new String[]{String.valueOf(record.getMode() + record.getName())});
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    db.insertOrThrow(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME, null, values);
                }
            }
        }
        catch (Exception e) {
            throw e;
        }
        finally {
            if (db != null) {
                db.close ();
            }
            if (cursor != null) {
                cursor.close ();
            }
        }
    }

    public Cursor getDataCursor (SQLiteDatabase db) {
        Cursor cur = db.query(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME, null, null, null, null, null, null);
        return cur;
    }

    public long updateData(Context context, ServiceRecord record) {
        ContentValues values = new ContentValues();
        values.put(ServiceStatusDBColumn.ServiceDBColumns._ID, record.getMode()+record.getName());
        values.put(ServiceStatusDBColumn.ServiceDBColumns.MODE, record.getMode());
        values.put(ServiceStatusDBColumn.ServiceDBColumns.STATUS, record.getStatus());
        values.put(ServiceStatusDBColumn.ServiceDBColumns.NAME, record.getName());
        values.put(ServiceStatusDBColumn.ServiceDBColumns.TYPE, record.getType());
        values.put(ServiceStatusDBColumn.ServiceDBColumns.RESULT, record.getResult());
        values.put(ServiceStatusDBColumn.ServiceDBColumns.TIME, record.getTime());
        values.put(ServiceStatusDBColumn.ServiceDBColumns.COUNT, record.getCount());
        long result = getWritableDatabase(context).update(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME, values,
                WHERE_ID_EQUALS,
                new String[]{String.valueOf(record.getMode() + record.getName())});
        return result;
    }

    public int deleteData(Context context, ServiceRecord record) {
        return getWritableDatabase(context).delete(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME,
                WHERE_ID_EQUALS, new String[]{record.getMode()+record.getName() + ""});
    }

    public List<ServiceRecord> getData(Context context) {
        List<ServiceRecord> recordList = new ArrayList<ServiceRecord>();
        Cursor cursor = getReadableDatabase(context).query(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME,
                null, null, null, null, null,
                null);

        try {
            while (cursor.moveToNext()) {
                ServiceRecord record = new ServiceRecord();
                record.setId(cursor.getString(0));
                record.setMode(cursor.getString(1));
                record.setStatus(cursor.getString(2));
                record.setName(cursor.getString(3));
                record.setType(cursor.getString(4));
                record.setResult(cursor.getInt(5));
                record.setTime(cursor.getString(6));
                record.setCount(cursor.getString(7));
                recordList.add(record);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close ();
            }
        }

        return recordList;
    }

    public List<ServiceRecord> getSelectedModeData(Context context, String mode) {
        List<ServiceRecord> recordList = new ArrayList<ServiceRecord>();
        Cursor cursor = getReadableDatabase(context).query(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME,
                null, null, null, null, null,
                null);

        try {
            while (cursor.moveToNext()) {
                String target = cursor.getString(1);
                if (target.equals(mode)) {
                    ServiceRecord record = new ServiceRecord();
                    record.setId(cursor.getString(0));
                    record.setMode(target);
                    record.setStatus(cursor.getString(2));
                    record.setName(cursor.getString(3));
                    record.setType(cursor.getString(4));
                    record.setResult(cursor.getInt(5));
                    record.setTime(cursor.getString(6));
                    record.setCount(cursor.getString(7));
                    recordList.add(record);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close ();
            }
        }

        return recordList;
    }

    public List<ServiceRecord> getSelectedBankData(Context context, String mode, String name) {
        List<ServiceRecord> recordList = new ArrayList<ServiceRecord>();
        Cursor cursor = getReadableDatabase(context).query(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME,
                null, null, null, null, null,
                null);

        while (cursor.moveToNext()) {
            String target = cursor.getString(1);
            String target2 = cursor.getString(3);
            if (target.equals(mode) && target2.equals(name)) {
                ServiceRecord record = new ServiceRecord();
                record.setId(cursor.getString(0));
                record.setMode(target);
                record.setStatus(cursor.getString(2));
                record.setName(cursor.getString(3));
                record.setType(cursor.getString(4));
                record.setResult(cursor.getInt(5));
                record.setTime(cursor.getString(6));
                record.setCount(cursor.getString(7));
                recordList.add(record);
            }
        }

        if (cursor != null) {
            cursor.close ();
        }

        return recordList;
    }

    public void clearData(Context context) throws Exception {
        SQLiteDatabase db = null;

        db = getWritableDatabase(context);

        try {
            db.beginTransaction();
            db.delete(ServiceStatusDBColumn.ServiceDBColumns.TABLE_NAME, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                db.endTransaction();
                db.close();
            }
        }
    }
}
