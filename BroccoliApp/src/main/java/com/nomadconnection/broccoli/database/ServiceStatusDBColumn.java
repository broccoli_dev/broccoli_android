package com.nomadconnection.broccoli.database;

import android.provider.BaseColumns;

/**
 * Created by YelloHyunminJang on 16. 1. 22..
 */
public class ServiceStatusDBColumn {

    public static class ServiceDBColumns implements BaseColumns {
        public static final String MODE = "mode";
        public static final String STATUS = "status";
        public static final String NAME = "name";
        public static final String TYPE = "type";
        public static final String RESULT = "result";
        public static final String TIME = "recent_time";
        public static final String COUNT = "count";

        public static final String TABLE_NAME 	        = "serviceDB";

        public static final String TIME_SORT_ORDER_ASC 	= TIME + " ASC";
        public static final String TIME_SORT_ORDER_DESC = TIME + " DESC";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = TIME + " ASC";

        public static final String CREATE_SERVICE =
                "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME +" ( "+ _ID +" TEXT PRIMARY KEY, "
                        + MODE + " TEXT, "
                        + STATUS +" TEXT, "
                        + NAME +" TEXT, "
                        + TYPE +" TEXT, "
                        + RESULT +" INTEGER, "
                        + TIME +" TEXT, "
                        + COUNT +" TEXT "
                        + ");";
    }

}
