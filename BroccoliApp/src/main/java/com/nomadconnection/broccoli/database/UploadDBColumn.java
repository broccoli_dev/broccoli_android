package com.nomadconnection.broccoli.database;

import android.provider.BaseColumns;

/**
 * Created by YelloHyunminJang on 16. 1. 22..
 */
public class UploadDBColumn {

    public static class BankUploadColumns implements BaseColumns {
        public static final String NAME					= "name";
        public static final String CODE					= "code";
        public static final String NUMBER               = "number";
        public static final String UPLOAD_ID            = "upload_id";
        public static final String TYPE                 = "type";
        public static final String TIME_CREATE			= "createTime";
        public static final String TIME_UPLOAD			= "uploadTime";

        public static final String TABLE_NAME 	        = "bankUploadDB";

        public static final String TIME_SORT_ORDER_ASC 	= TIME_CREATE + " ASC";
        public static final String TIME_SORT_ORDER_DESC = TIME_CREATE + " DESC";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = TIME_CREATE + " ASC";

        public static final String CREATE_BANK =
                "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME +" ( "+ _ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + CODE +" TEXT, "
                        + NAME +" TEXT, "
                        + NUMBER +" TEXT, "
                        + UPLOAD_ID +" TEXT, "
                        + TYPE +" TEXT, "
                        + TIME_CREATE +" TEXT, "
                        + TIME_UPLOAD +" TEXT "
                        + ");";
    }

    public static class CardUploadColumns implements BaseColumns {
        public static final String NAME					= "name";
        public static final String CODE					= "code";
        public static final String SERIAL				= "serial";
        public static final String PWD					= "pwd";
        public static final String TIME_CREATE			= "createTime";
        public static final String TIME_UPLOAD			= "uploadTime";

        public static final String TABLE_NAME 	        = "cardUploadDB";

        public static final String TIME_SORT_ORDER_ASC 	= TIME_CREATE + " ASC";
        public static final String TIME_SORT_ORDER_DESC = TIME_CREATE + " DESC";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = TIME_CREATE + " ASC";


        public static final String CREATE_CARD =
                "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME +" ( "+ _ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + CODE +" TEXT, "
                        + NAME +" TEXT, "
                        + SERIAL +" TEXT, "
                        + PWD +" TEXT, "
                        + TIME_CREATE +" TEXT, "
                        + TIME_UPLOAD +" TEXT "
                        + ");";
    }

}
