package com.nomadconnection.broccoli.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by YelloHyunminJang on 15. 9. 21..
 */
public class DatabaseDao {

    protected SQLiteDatabase mDatabase;
    private BroccoliDBHelper mDbHelper;

    public void openDBHelper(Context context) {
        mDbHelper = new BroccoliDBHelper(context, DBConstant.DB_NAME, null, DBConstant.DB_VERSION);
    }

    public SQLiteDatabase getWritableDatabase(Context context) {
        if (mDbHelper == null) {
            openDBHelper(context);
        }
        return mDbHelper.getWritableDatabase();
    }

    public SQLiteDatabase getReadableDatabase(Context context) {
        if (mDbHelper == null) {
            openDBHelper(context);
        }
        return mDbHelper.getReadableDatabase();
    }

}
