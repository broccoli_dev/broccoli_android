package com.nomadconnection.broccoli.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.nomadconnection.broccoli.database.DBConstant.TABLE_PERSONAL;

public class BroccoliDBHelper extends SQLiteOpenHelper {

    public BroccoliDBHelper(Context context, String db_name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, db_name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBConstant.CREATE_BROCCOLI);
        db.execSQL(ScrapDBColumn.BankDBColumns.CREATE_BANK);
        db.execSQL(ServiceStatusDBColumn.ServiceDBColumns.CREATE_SERVICE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            dropTablePersonal(db);
            addScrapDbColumnLoginType(db);
            extendUserTable(db);
            extendBankTable(db);
        } else if (oldVersion < 3) {
            addScrapDbColumnLoginType(db);
            extendUserTable(db);
            extendBankTable(db);
        } else if (oldVersion < 4) {
            extendUserTable(db);
            extendBankTable(db);
        }
    }


    /**
     * Upgrade 컬럼 추가 example
     *
     * @param db
     */
    private void addExampleChangeable(SQLiteDatabase db) {
        //db.execSQL("ALTER TABLE " + TABLE_USER + " ADD COLUMN " + MYZIPSA_CHANGEABLE + " INTEGER;");
    }

    private void dropTablePersonal(SQLiteDatabase db) {
        String sql = "DROP TABLE IF EXISTS "+TABLE_PERSONAL;
        db.execSQL(sql);
        onCreate(db);
    }

    private void addScrapDbColumnLoginType(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE "+ ScrapDBColumn.BankDBColumns.TABLE_BANK +" ADD COLUMN "+ ScrapDBColumn.BankDBColumns.LOGIN_METHOD +" TEXT DEFAULT 'cert';");
    }

    private void extendUserTable(SQLiteDatabase db) {
        try {
            db.execSQL(DBConstant.CREATE_BROCCOLI_USER_MIGRATION_STEP1);
            db.execSQL(DBConstant.CREATE_BROCCOLI_USER_MIGRATION_STEP2);
            db.execSQL(DBConstant.CREATE_BROCCOLI_USER_MIGRATION_STEP3);
            db.execSQL(DBConstant.CREATE_BROCCOLI_USER_MIGRATION_STEP4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void extendBankTable(SQLiteDatabase db) {
        try {
            db.execSQL(ScrapDBColumn.BankDBColumns.CREATE_BANK_MIGRATION);
            db.execSQL(ScrapDBColumn.BankDBColumns.CREATE_BANK_MIGRATION_STEP2);
            db.execSQL(ScrapDBColumn.BankDBColumns.CREATE_BANK_MIGRATION_STEP3);
            db.execSQL(ScrapDBColumn.BankDBColumns.CREATE_BANK_MIGRATION_STEP4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
