package com.nomadconnection.broccoli.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.utils.Crypto;


/**
 * Created by YelloHyunminJang on 15. 9. 1..
 */
public class DbManager {
	private static BroccoliDBHelper mDbHelper;
	private static final String WHERE_ID_EQUALS = DBConstant.USERID + " =?";
	private static String USER_ORDER_BY = DBConstant.USERID + " DESC";
	private static DbManager mInstance;

	public static DbManager getInstance() {
		if (mInstance == null) {
			mInstance = new DbManager();
		}
		return mInstance;
	}

	public void openDBHelper (Context context) {
		mDbHelper = new BroccoliDBHelper(context, DBConstant.DB_NAME, null, DBConstant.DB_VERSION);
	}

	public SQLiteDatabase getWritableDatabase (Context context) {
		if (mDbHelper == null) {
			openDBHelper (context);
		}
		return mDbHelper.getWritableDatabase();
	}

	public SQLiteDatabase getReadableDatabase (Context context) {
		if (mDbHelper == null) {
			openDBHelper (context);
		}
		return mDbHelper.getReadableDatabase();
	}

	public String getUserId(Context context) {
		String userId = null;

		SQLiteDatabase db = getReadableDatabase(context);
		Cursor cursor = null;
		try {
			cursor = db.query(DBConstant.TABLE_USER,
				new String[] {DBConstant.USERID, DBConstant.LOGINED}, null, null, null, null, USER_ORDER_BY);

			if (cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					String id = cursor.getString(0);
					String login = Crypto.decrypt(APP.APP_CODE, cursor.getString(1));
					if (DBConstant.TRUE.equalsIgnoreCase(login)) {
						userId = Crypto.decrypt(APP.APP_CODE, id);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}

		return userId;
	}

	public String getErrorUserId(Context context, String status) {
		String userId = null;

		SQLiteDatabase db = getReadableDatabase(context);
		Cursor cursor = null;
		try {
			cursor = db.query(DBConstant.TABLE_USER,
					new String[] {DBConstant.USERID, DBConstant.LOGINED}, null, null, null, null, USER_ORDER_BY);

			if (cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					String id = cursor.getString(0);
					String login = Crypto.decrypt(APP.APP_CODE, cursor.getString(1));
					if (DBConstant.FAIL_PIN_ERROR.equalsIgnoreCase(login)) {
						userId = Crypto.decrypt(APP.APP_CODE, id);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}

		return userId;
	}

	public String[] getLoginEncryptInfo(Context context) {
		String[] info = new String[3];

		SQLiteDatabase db = getReadableDatabase(context);
		Cursor cursor = null;
		try {
			cursor = db.query(DBConstant.TABLE_USER,
					new String[] {DBConstant.USERID, DBConstant.PASSWD, DBConstant.LOGINED}, null, null, null, null, USER_ORDER_BY);

			if (cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					String login = Crypto.decrypt(APP.APP_CODE, cursor.getString(2));
					if (DBConstant.TRUE.equalsIgnoreCase(login)) {
						info[0] = cursor.getString(0);
						info[1] = cursor.getString(1);
						info[2] = cursor.getString(2);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}

		return info;
	}

	public String[] getLoginInfo (Context context, String key) {
		String[] info = new String[3];

		SQLiteDatabase db = getReadableDatabase(context);
		Cursor cursor = null;
		try {
			cursor = db.query(DBConstant.TABLE_USER,
				new String[] {DBConstant.USERID, DBConstant.PASSWD, DBConstant.LOGINED}, null, null, null, null, USER_ORDER_BY);

			if (cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					String login = Crypto.decrypt(APP.APP_CODE, cursor.getString(2));
					if (DBConstant.TRUE.equalsIgnoreCase(login)) {
						info[0] = Crypto.decrypt(APP.APP_CODE, cursor.getString(0));
						info[1] = Crypto.decrypt(APP.APP_CODE, cursor.getString(1));
						info[2] = Crypto.decrypt(APP.APP_CODE, cursor.getString(2));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}

		return info;
	}

	public String[] isOldMember(Context context) {
		String[] info = new String[3];

		SQLiteDatabase db = getReadableDatabase(context);
		Cursor cursor = null;
		try {
			cursor = db.query(DBConstant.TABLE_USER,
					new String[] {DBConstant.USERID, DBConstant.LOGINED, DBConstant.LASTLOGIN}, null, null, null, null, USER_ORDER_BY);

			if (cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					String login = Crypto.decrypt(APP.APP_CODE, cursor.getString(1));
					if (DBConstant.TRUE.equalsIgnoreCase(login)) {
						info[0] = Crypto.decrypt(APP.APP_CODE, cursor.getString(0));
						info[1] = Crypto.decrypt(APP.APP_CODE, cursor.getString(1));
						info[2] = Crypto.decrypt(APP.APP_CODE, cursor.getString(2));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}

		return info;
	}

	public void insertOrUpdateUserInfoData (Context context, String key, LoginData login)
		throws Exception {
		if (login != null) {
			SQLiteDatabase db = getWritableDatabase (context);
			Cursor userCursor = getUserInfo(db);
			try {
				ContentValues row = new ContentValues();

				row.put(DBConstant.USERID, Crypto.encrypt(APP.APP_CODE, login.getUserId()));
				row.put(DBConstant.PASSWD, Crypto.encrypt(APP.APP_CODE, login.getPassword()));
				row.put(DBConstant.DAYLIPASSID, login.getDayliId());
				row.put(DBConstant.DAYLIPASSTOKEN, Crypto.encrypt(APP.APP_CODE, login.getDayliToken()));
				row.put(DBConstant.PINNUMBER, Crypto.encrypt(key, login.getPinNumber()));
				row.put(DBConstant.USERPUSHKEY, Crypto.encrypt(key, login.getPushKey()));
				row.put(DBConstant.LOGINED, Crypto.encrypt(APP.APP_CODE, login.isLogin()));
				row.put(DBConstant.LASTLOGIN, Crypto.encrypt(APP.APP_CODE, login.getLastLogin()));

				if (userCursor == null || userCursor.getCount() == 0) {
					db.insertOrThrow (DBConstant.TABLE_USER, null, row);
				} else {
					boolean exist = false;
					while (userCursor.moveToNext()) {
						String dbID = userCursor.getString(0);
						String id = Crypto.decrypt(APP.APP_CODE, dbID);
						if (login.getUserId().equalsIgnoreCase(id)) {
							exist = true;
							db.update (DBConstant.TABLE_USER, row, " "+DBConstant.USERID+" =?", new String[] {String.valueOf(dbID)});
						}
					}
					if (!exist) {
						db.insertOrThrow(DBConstant.TABLE_USER, null, row);
					}
				}
			}
			catch (Exception e) {
				throw e;
			}
			finally {
				if (db != null) {
					db.close ();
				}
				if (userCursor != null) {
					userCursor.close ();
				}
			}
		}
	}

	public Cursor getUserInfo (Context context) {
		SQLiteDatabase db = getReadableDatabase (context);
		Cursor cur = db.query (DBConstant.TABLE_USER, null, null, null, null, null, null);
		return cur;
	}

	public Cursor getUserInfo (SQLiteDatabase db) {
		Cursor cur = db.query(DBConstant.TABLE_USER, null, null, null, null, null, null);
		return cur;
	}

	public void updatePinNumber(Context context, String key, String id, String pinnum) throws Exception {
		SQLiteDatabase db = null;

		db = getWritableDatabase (context);

		try {
			Cursor userCursor = getUserInfo (context);
			if (userCursor == null || userCursor.getCount () == 0) {
				throw new Exception("UserInfoTable does not exist");
			} else {

			}
			userCursor.close ();

			db.beginTransaction ();

			db.execSQL ("UPDATE " + DBConstant.TABLE_USER +
					" SET " +
					DBConstant.PINNUMBER + "='" + Crypto.encrypt(key, pinnum) + "'" +
					" WHERE " + DBConstant.USERID + "='" + Crypto.encrypt(APP.APP_CODE, id) + "' ;");

			db.setTransactionSuccessful ();
		} finally {
			if (db != null) {
				db.endTransaction ();
				db.close ();
			}
		}
	}

	public void updateDb(Context context, String key, LoginData data) throws Exception {
		SQLiteDatabase db = null;

		db = getWritableDatabase(context);

		try {
			Cursor userCursor = getUserInfo(context);
			String id = data.getUserId();
			if (userCursor == null || userCursor.getCount () == 0) {
				throw new Exception("UserInfoTable does not exist");
			}
			userCursor.close ();

			db.beginTransaction ();

			db.execSQL ("UPDATE " + DBConstant.TABLE_USER +
				" SET " +
				DBConstant.USERID + "='" + Crypto.encrypt(APP.APP_CODE, data.getUserId()) + "'," +
				DBConstant.PASSWD + "='" + Crypto.encrypt(APP.APP_CODE, data.getPassword()) + "'," +
				DBConstant.DAYLIPASSID + "='" + data.getDayliId() + "'," +
				DBConstant.DAYLIPASSTOKEN + "='" + Crypto.encrypt(APP.APP_CODE, data.getDayliToken()) + "'," +
				DBConstant.PINNUMBER + "='" + Crypto.encrypt(key, data.getPinNumber()) + "'," +
				DBConstant.USERPUSHKEY + "='" + Crypto.encrypt(key, data.getPushKey()) + "'," +
				DBConstant.LOGINED  + "='" + Crypto.encrypt(APP.APP_CODE, data.isLogin()) + "'," +
				DBConstant.LASTLOGIN + "='" + Crypto.encrypt(APP.APP_CODE, data.getLastLogin()) + "' " +
				" WHERE " + DBConstant.USERID + "='" + Crypto.encrypt(APP.APP_CODE, data.getUserId()) + "' ;");
			db.setTransactionSuccessful ();
		} finally {
			if (db != null) {
				db.endTransaction ();
				db.close ();
			}
		}
	}

	public String getUserDayliToken(Context context, String userId) {
		SQLiteDatabase db = getReadableDatabase (context);
		String token = null;
		Cursor c = null;

		try {
			String query = "SELECT " +
					DBConstant.USERID + ", " +
					DBConstant.PASSWD + ", " +
					DBConstant.DAYLIPASSID + ", " +
					DBConstant.DAYLIPASSTOKEN + ", " +
					DBConstant.PINNUMBER + ", " +
					DBConstant.USERPUSHKEY + ", " +
					DBConstant.LOGINED + ", "+
					DBConstant.LASTLOGIN + " " +
					" FROM " + DBConstant.TABLE_USER + ";";

			c = db.rawQuery(query, null);

			while (c.moveToNext()) {
				String id = Crypto.decrypt(APP.APP_CODE, c.getString(0));
				if (userId.equalsIgnoreCase(id)) {
					token = Crypto.decrypt(APP.APP_CODE, c.getString(3));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close ();

			if (db != null)
				db.close ();
		}

		return token;
	}

	public LoginData getUserInfoDataSet(Context context, String key, String userId) {
		LoginData data = new LoginData();
		SQLiteDatabase db = getReadableDatabase (context);

		Cursor c = null;

		try {
			String query = "SELECT " +
				DBConstant.USERID + ", " +
				DBConstant.PASSWD + ", " +
					DBConstant.DAYLIPASSID + ", " +
					DBConstant.DAYLIPASSTOKEN + ", " +
					DBConstant.PINNUMBER + ", " +
					DBConstant.USERPUSHKEY + ", " +
					DBConstant.LOGINED + ", "+
					DBConstant.LASTLOGIN + " " +
				" FROM " + DBConstant.TABLE_USER + ";";

			c = db.rawQuery(query, null);

			while (c.moveToNext()) {
				String id = Crypto.decrypt(APP.APP_CODE, c.getString(0));
				if (userId.equalsIgnoreCase(id)) {
					data.setUserId(id);
					data.setPassword(Crypto.decrypt(APP.APP_CODE, c.getString(1)));
					data.setDayliId(c.getLong(2));
					data.setDayliToken(Crypto.decrypt(APP.APP_CODE, c.getString(3)));
					data.setPinNumber(Crypto.decrypt(key, c.getString(4)));
					data.setPushKey(Crypto.decrypt(key, c.getString(5)));
					data.setLogin(Crypto.decrypt(APP.APP_CODE, c.getString(6)));
					data.setLastLogin(Crypto.decrypt(APP.APP_CODE, c.getString(7)));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close ();

			if (db != null)
				db.close ();
		}

		return data;
	}

	public long getDayliId(Context context, String userId) {
		long dayliId = -1;

		SQLiteDatabase db = getReadableDatabase(context);
		Cursor cursor = null;
		try {
			cursor = db.query(DBConstant.TABLE_USER,
					new String[] {DBConstant.USERID, DBConstant.DAYLIPASSID, DBConstant.LOGINED}, null, null, null, null, USER_ORDER_BY);

			if (cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					String login = Crypto.decrypt(APP.APP_CODE, cursor.getString(2));
					if (DBConstant.TRUE.equalsIgnoreCase(login)) {
						String id = Crypto.decrypt(APP.APP_CODE, cursor.getString(0));
						if (userId.equalsIgnoreCase(id)) {
							dayliId = cursor.getLong(1);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}

		return dayliId;
	}

	public long updateLoginStatus(Context context, String userId, String status) throws Exception {
		long ret = -1;

		SQLiteDatabase db = null;

		db = getWritableDatabase (context);

		try {
			Cursor userCursor = getUserInfo (context);
			if (userCursor == null || userCursor.getCount () == 0) {
				throw new Exception("UserInfoTable does not exist");
			} else {

			}
			userCursor.close ();

			db.beginTransaction ();

			db.execSQL ("UPDATE " + DBConstant.TABLE_USER +
					" SET " +
					DBConstant.LOGINED + "='" + Crypto.encrypt(APP.APP_CODE, status) + "'" +
					" WHERE " + DBConstant.USERID + "='" + Crypto.encrypt(APP.APP_CODE, userId) + "' ;");

			db.setTransactionSuccessful ();
		} finally {
			if (db != null) {
				db.endTransaction ();
				db.close ();
			}
		}

		return ret;
	}

	public void logoutByUser(Context context, String id, String time) {
		SQLiteDatabase db = null;

		db = getWritableDatabase(context);

		try {
			Cursor userCursor = getUserInfo(context);
			if (userCursor == null || userCursor.getCount () == 0) {
				throw new Exception("UserInfoTable does not exist");
			}
			userCursor.close ();

			db.beginTransaction ();

			db.execSQL ("UPDATE " + DBConstant.TABLE_USER +
					" SET " +
					DBConstant.LOGINED  + "='" + Crypto.encrypt(APP.APP_CODE, DBConstant.FALSE) + "'," +
					DBConstant.LASTLOGIN + "='" + Crypto.encrypt(APP.APP_CODE, time) + "'" +
					" WHERE " + DBConstant.USERID + "='" + Crypto.encrypt(APP.APP_CODE, id) + "' ;");

			db.setTransactionSuccessful ();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.endTransaction ();
				db.close ();
			}
		}
	}

	public int deleteUserData(Context context, String id) {
		int result = -1;
		try {
			result =  getWritableDatabase(context).delete(DBConstant.TABLE_USER,
					WHERE_ID_EQUALS, new String[]{Crypto.encrypt(APP.APP_CODE, id) + ""});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void deleteUserInfoColumn (Context context, String id) throws Exception {
		SQLiteDatabase db = null;

		db = getWritableDatabase (context);

		try
		{
			db.beginTransaction ();

			db.execSQL ("DELETE FROM " + DBConstant.TABLE_USER +
				" WHERE " + DBConstant.USERID + "=" + Crypto.encrypt(APP.APP_CODE, id) + " ;");

			db.setTransactionSuccessful ();
		}
		finally
		{
			if (db != null)
			{
				db.endTransaction ();
				db.close ();
			}
		}
	}

	public void deleteUserInfoDB (Context context) throws Exception {
		SQLiteDatabase db = null;

		db = getWritableDatabase (context);

		try {
			db.beginTransaction();
			db.delete(DBConstant.TABLE_USER, null, null);
			db.setTransactionSuccessful();
		} finally {
			if (db != null) {
				db.endTransaction ();
				db.close ();
			}
		}
	}

	public void deletePersonalInfo(Context context) throws Exception {
		SQLiteDatabase db = null;

		db = getWritableDatabase (context);

		try {
			db.beginTransaction();
			db.delete(DBConstant.TABLE_PERSONAL, null, null);
			db.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.endTransaction ();
				db.close ();
			}
		}
	}

}
