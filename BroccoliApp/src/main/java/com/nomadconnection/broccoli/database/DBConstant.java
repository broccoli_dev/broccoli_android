package com.nomadconnection.broccoli.database;

public class DBConstant {
    public static final int    DB_VERSION = 4;
    public static final String DB_NAME = "broccoli.db";
    public static final String TABLE_USER = "broccoli";
    public static final String TABLE_USER_EXTEND = "broccoliextend";

    public static final String _ID = "_id";
    public static final String USERID = "userID";         //ID
    public static final String PASSWD = "appPasswd";        //패스워드
    public static final String PINNUMBER = "pinNumber";
    public static final String USERPUSHKEY = "pushKey";     //pushkey
    public static final String LASTLOGIN = "lastlogin";             //토큰
    public static final String DAYLIPASSID = "dpId";
    public static final String DAYLIPASSTOKEN = "dpAccessToken";
    public static final String LOGINED = "logined";

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String FAIL_PIN_ERROR = "pin_error";
    public static final String FAIL_MEMBERSHIP_ERROR = "membership_error";

    public static final String CREATE_BROCCOLI =
            "CREATE TABLE IF NOT EXISTS "+ TABLE_USER +" ( "
                    + USERID +" TEXT PRIMARY KEY,  "
                    + PASSWD +" TEXT, "
                    + DAYLIPASSID +" INTEGER, "
                    + DAYLIPASSTOKEN +" TEXT, "
                    + PINNUMBER +" TEXT, "
                    + USERPUSHKEY +" TEXT, "
                    + LOGINED +" TEXT, "
                    + LASTLOGIN +" TEXT "+
                    ");";


    public static final String CREATE_BROCCOLI_USER_MIGRATION_STEP1 =
            "CREATE TABLE "+TABLE_USER_EXTEND+"( "
                    + USERID +" TEXT PRIMARY KEY,  "
                    + PASSWD +" TEXT, "
                    + DAYLIPASSID +" INTEGER, "
                    + DAYLIPASSTOKEN +" TEXT, "
                    + PINNUMBER +" TEXT, "
                    + USERPUSHKEY +" TEXT, "
                    + LOGINED +" TEXT DEFAULT '"+"QUNCRDFEMkREMTNBMEU4OUQ0NjM1QTVGN0ZCMjQwRkE="+"', "
                    + LASTLOGIN +" TEXT "+
                    ");";
    public static final String CREATE_BROCCOLI_USER_MIGRATION_STEP2 = "INSERT INTO "+TABLE_USER_EXTEND+" ("+USERID+", "+PINNUMBER+", "+USERPUSHKEY+")" +
                    "   SELECT "+USERID+", "+PASSWD+", "+USERPUSHKEY+" FROM "+TABLE_USER+";";
    public static final String CREATE_BROCCOLI_USER_MIGRATION_STEP3 = "DROP TABLE "+TABLE_USER;
    public static final String CREATE_BROCCOLI_USER_MIGRATION_STEP4 = "ALTER TABLE "+TABLE_USER_EXTEND+" RENAME TO "+TABLE_USER+";";


    public static final String TABLE_PERSONAL = "personal";

    public static final String NAME = "name";
    public static final String BIRTHDATE = "birthDate";
    public static final String DEVICEID = "deviceId";
    public static final String PHONENUM = "phoneNum";

    public static final String CREATE_PERSONAL =
            "CREATE TABLE IF NOT EXISTS "+ TABLE_PERSONAL +" ( "+ _ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + NAME +" TEXT, "
                    + BIRTHDATE +" TEXT, "
                    + DEVICEID +" TEXT, "
                    + PHONENUM +" TEXT "
                    + ");";
}

