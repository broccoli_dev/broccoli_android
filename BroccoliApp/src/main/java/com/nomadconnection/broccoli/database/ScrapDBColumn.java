package com.nomadconnection.broccoli.database;

import android.provider.BaseColumns;

/**
 * Created by YelloHyunminJang on 16. 1. 22..
 */
public class ScrapDBColumn {

    public static class BankDBColumns implements BaseColumns {
        public static final String NAME					= "name";
        public static final String CODE					= "code";
        public static final String SERIAL				= "serial";
        public static final String PWD					= "pwd";
        public static final String TYPE                 = "type";
        public static final String VALID_TIME           = "valid";
        public static final String TIME_UPLOAD			= "uploadTime";
        public static final String STATUS               = "status";
        public static final String LOGIN_METHOD         = "loginmethod";
        public static final String USER_ID              = "userId";

        public static final String TABLE_BANK = "bankDB";
        public static final String TABLE_BANK_EXTEND = "bankDBextend";

        public static final String TIME_SORT_ORDER_ASC 	= VALID_TIME + " ASC";
        public static final String TIME_SORT_ORDER_DESC = VALID_TIME + " DESC";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = VALID_TIME + " ASC";

        public static final String CREATE_BANK =
                "CREATE TABLE IF NOT EXISTS "+ TABLE_BANK +" ( " + _ID + " TEXT PRIMARY KEY ,  "
                        + NAME +" TEXT , "
                        + CODE +" TEXT, "
                        + SERIAL +" TEXT, "
                        + PWD +" TEXT, "
                        + TYPE +" TEXT, "
                        + VALID_TIME +" TEXT, "
                        + TIME_UPLOAD +" TEXT, "
                        + STATUS +" TEXT, "
                        + LOGIN_METHOD + " TEXT, "
                        + USER_ID + " TEXT "
                        + ");";


        public static final String CREATE_BANK_MIGRATION =
                "CREATE TABLE IF NOT EXISTS "+ TABLE_BANK_EXTEND +" ( "+ _ID +" TEXT PRIMARY KEY , "
                        + NAME +" TEXT , "
                        + CODE +" TEXT, "
                        + SERIAL +" TEXT, "
                        + PWD +" TEXT, "
                        + TYPE +" TEXT, "
                        + VALID_TIME +" TEXT, "
                        + TIME_UPLOAD +" TEXT, "
                        + STATUS +" TEXT, "
                        + LOGIN_METHOD + " TEXT "
                        + ");";



        public static final String CREATE_BANK_MIGRATION_STEP2 = "INSERT INTO "+TABLE_BANK_EXTEND+" ("+_ID+", "+NAME+", "+CODE+", "+SERIAL+", "+PWD+", "+TYPE+", "+VALID_TIME+", "+TIME_UPLOAD+", "+STATUS+", "+LOGIN_METHOD+")" +
                "   SELECT "+NAME+", "+NAME+", "+CODE+", "+SERIAL+", "+PWD+", "+TYPE+", "+VALID_TIME+", "+TIME_UPLOAD+", "+STATUS+", "+LOGIN_METHOD+" FROM "+TABLE_BANK+";";
        public static final String CREATE_BANK_MIGRATION_STEP3 = "DROP TABLE "+TABLE_BANK;
        public static final String CREATE_BANK_MIGRATION_STEP4 = "ALTER TABLE "+TABLE_BANK_EXTEND+" RENAME TO "+TABLE_BANK+";";
    }

}
