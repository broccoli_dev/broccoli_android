package com.nomadconnection.broccoli.session;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.data.Session.ResponseBroccoli;
import com.nomadconnection.broccoli.data.Session.UserData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.NetStatusChecker;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 15. 9. 7..
 */
public class LoginManager {

    public interface OnLoginResultListener {

        void onLoginSuccess(Response<Result> result, LoginData login);

	    /**
	     * 로그인 실패
	     * @param error 에러 코드
	     * @param obj 에러 메시지
	     */
	    void onLoginFail (int error, Object obj);

        /**
         */
        void onLogout();
    }

    public static final int				LOGIN_ENABLE	 				= 0;
    public static final int				LOGIN_DISABLE_NOT_ENOUGH_INFO 	= -1;
    public static final int				LOGIN_DISABLE_ROAMING			= -2;
    public static final int				LOGIN_DISABLE_AIRPLAIN			= -3;
    public static final int				LOGIN_DISABLE_NETWORK			= -4;
    public static final int				LOGIN_DISABLE_UNKNOWN			= -5;
    public static final int				LOGIN_DISABLE_DESTRUCT			= -6;
    public static final int				LOGIN_DISABLE_SESSION_ERROR 	= -7;

    public static final int             LOGIN_SUCCESS                   = 1;
    private Context mContext = null;
    private OnLoginResultListener mListener = null;

    DbManager dbManager;

    public LoginManager(Context context, OnLoginResultListener listener, DbManager dbManager) {
        mContext = context;
        mListener = listener;
	    this.dbManager = dbManager;
    }

    public void startAutoLogin() {
        Session.getInstance(mContext).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                String[] info = dbManager.getLoginInfo(mContext, str);

                String id = info[0];
                String pwd = info[1];
                String login = info[2];
                if (checkAutoLoginAvailable(id, pwd)) {
                    int status = checkAvailable(id, pwd);
                    if (status == LOGIN_ENABLE) {
                        loginProcess(id, pwd);
                    } else {
                        String obj = null;
                        switch (status) {
                            case LOGIN_DISABLE_NETWORK:
                                obj = "LOGIN_DISABLE_NETWORK";
                                break;
                            case LOGIN_DISABLE_AIRPLAIN:
                                obj = "LOGIN_DISABLE_AIRPLAIN";
                                break;
                            case LOGIN_DISABLE_ROAMING:
                                obj = "LOGIN_DISABLE_ROAMING";
                                break;
                        }
                        mListener.onLoginFail (status, obj);
                    }
                } else {
                    mListener.onLoginFail (LOGIN_DISABLE_NOT_ENOUGH_INFO, "LOGIN_DISABLE_NOT_ENOUGH_INFO");
                }
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_716_TOKEN_EXPIRE:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        mListener.onLoginFail(LOGIN_DISABLE_SESSION_ERROR, errorMessage.name());
                        break;
                    default:
                        mListener.onLoginFail(LOGIN_DISABLE_UNKNOWN, errorMessage.name());
                        break;
                }
            }

            @Override
            public void fail() {
                mListener.onLoginFail (LOGIN_DISABLE_UNKNOWN, "LOGIN_DISABLE_NOT_ACQUIRE_SERVER_KEY");
            }
        });
    }

    /**
     * check id, pwd from localDB
     * @return
     */
    public boolean checkAutoLoginAvailable(String id, String pwd) {
        boolean ret = true;

        if (id == null || TextUtils.isEmpty(id)) {
            ret = false;
        }
        if (pwd == null || TextUtils.isEmpty(pwd)) {
            ret = false;
        }
        return ret;
    }


    public int checkAvailable(String id, String pwd){
        int result = LOGIN_ENABLE;

        if(null == id || null == pwd){
            result = LOGIN_DISABLE_NOT_ENOUGH_INFO;
        } else if(!NetStatusChecker.isNetworkEnable(mContext)){
            result = LOGIN_DISABLE_NETWORK;
        } else if(NetStatusChecker.isAirPlane(mContext)){
            result = LOGIN_DISABLE_AIRPLAIN;
        }
        return result;
    }

    public void loginProcess(String id, String password) {
        final LoginData login = new LoginData();
        login.setUserId(id);
        login.setPassword(password);
        login.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
        login.setType(1); // android
        login.setDeviceId(ElseUtils.getDeviceUUID(mContext));

        UserData userData = new UserData();
        userData.setUserId(id);
        userData.setPassword(password);
        userData.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
        userData.setOsType(1);
        userData.setDeviceId(ElseUtils.getDeviceUUID(mContext));
        userData.setAdId(APPSharedPrefer.getInstance(mContext).getPrefString(APP.SP_USER_ADID));
        Call<ResponseBroccoli> call = ServiceGenerator.createService(UserApi.class).autoLogin(userData);
        call.enqueue(new Callback<ResponseBroccoli>() {
            @Override
            public void onResponse(Call<ResponseBroccoli> call, Response<ResponseBroccoli> response) {
                ResponseBroccoli result = null;
                if (response != null && response.isSuccessful() && response.body() != null) {
                    result = response.body();

                    if ((result != null) || response.message().equalsIgnoreCase(Const.OK)) {
                        String token = response.headers().get(Const.TOKEN);
                        Session.getInstance(mContext).setToken(token);
                        int day = response.body().scrapingDays;
                        Intent intent = new Intent(IntentDef.INTENT_ACTION_UPLOAD_START);
                        intent.putExtra(Const.SCRAPING_DAY, day);
                        mContext.sendBroadcast(intent);
                        if (mListener != null)
                            mListener.onLoginSuccess(null, login);
                    } else {
                        if (mListener != null)
                            mListener.onLoginFail(LOGIN_DISABLE_UNKNOWN, mContext.getString(R.string.membership_text_login_fail_unknown)+response.body().getDesc());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBroccoli> call, Throwable t) {
                if (mListener != null)
                    mListener.onLoginFail(LOGIN_DISABLE_UNKNOWN, mContext.getString(R.string.membership_text_login_fail_unknown)+t.getMessage());
            }
        });
    }

}
