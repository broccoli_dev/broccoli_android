package com.nomadconnection.broccoli.session;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.activity.LockScreenActivity;
import com.nomadconnection.broccoli.activity.Splash;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.FileDownloadService;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.ServiceUrl;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.constant.SharedPreferenceConstant;
import com.nomadconnection.broccoli.data.CdnAppInfoData;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.KeyDataSet;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.database.ServiceRecordDao;
import com.nomadconnection.broccoli.interf.LifecycleCallbacks;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.Crypto;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.io.IOException;
import java.net.CookieHandler;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.nomadconnection.broccoli.session.LoginManager.LOGIN_DISABLE_UNKNOWN;

/**
 * Created by YelloHyunminJang on 15. 9. 17..
 */
public class Session extends Lock implements LifecycleCallbacks {

	private static final String TAG = Session.class.getSimpleName();

	private static Session mInstance;

	Context mContext;

	DbManager dbManager;

	private String mUserId = null;

	private boolean isLogined = false;

	private boolean isLock = false;

	private boolean isTemporaryDisable = false;

	private LoginManager mLoginMgr;

	private LoginManager.OnLoginResultListener mListener = null;

	private int liveCount;

	private int visibleCount;

	private long lastActive;

	private String mPublic;

	public static String TOKEN;
	private UserPersonalInfo userPersonalInfo = null;
	private CdnAppInfoData appInfo = null;
	private static Object mLock = new Object();

	private LoginManager.OnLoginResultListener mLoginListener = new LoginManager.OnLoginResultListener () {

		@Override
		public void onLoginSuccess(Response<Result> result, LoginData login) {
			onLoginComplete(result, login);
		}

		@Override
		public void onLoginFail(int error, Object obj) {
			if (mListener != null)
				mListener.onLoginFail(error, obj);
		}

		@Override
		public void onLogout() {
			if (mListener != null) {
				mListener.onLogout();
			}
		}
	};

	public String getRequest() {
		return mRequest;
	}

	public void setRequest(String mRequest) {
		this.mRequest = mRequest;
	}

	public interface OnKeyInterface {
		void acquired(String str);
		void error(ErrorMessage errorMessage);
		void fail();
	}

	public interface OnUserInfoUpdateCompleteInterface {
		void complete(UserPersonalInfo info);
		void error(ErrorMessage message);
	}

	public Session(Context context) {
		mContext = context;
		dbManager = DbManager.getInstance();
		liveCount = 0;
		visibleCount = 0;
		BaseActivity.setListener(this);
		CookieHandler.setDefault(new java.net.CookieManager());
		String token = APPSharedPrefer.getInstance(mContext).getPrefString(APP.SP_USER_TOKEN);
		if (token != null) {
			setToken(token);
		}
		getCDNData();
	}

	public static Session getInstance(Context context) {
		if (mInstance == null) {
			synchronized (mLock) {
				mInstance = new Session(context.getApplicationContext());
			}
		}
		return mInstance;
	}

	public void setToken(String token) {
		TOKEN = token;
		APPSharedPrefer.getInstance(mContext).setPrefString(APP.SP_USER_TOKEN, token);
	}

	public void setLogined(boolean bool) {
		isLogined = bool;
	}

	public boolean isLogin() {
		return isLogined;
	}

	public boolean isLock() {
		boolean ret = false;
		if (!isAutoLoginAvailable()) {
			ret = false;
		} else {
			ret = isLock;
		}
		return ret;
	}

	public void setLock(boolean lock) {
		isLock = lock;
	}

	public void setLockTemporaryDisable(boolean flag) {
		isTemporaryDisable = flag;
	}

	public String getUserId() {
		if (mUserId == null || TextUtils.isEmpty(mUserId)) {
			mUserId = DbManager.getInstance().getUserId(mContext);
		}
		return mUserId;
	}

	public void autoLogin(LoginManager.OnLoginResultListener listener) {
		mListener = listener;
		if (mLoginMgr == null)
			mLoginMgr = new LoginManager (mContext, mLoginListener, dbManager);
		mLoginMgr.startAutoLogin();
	}

	public void login(String id, String pwd, LoginManager.OnLoginResultListener listener) {
		mListener = listener;
		if (mLoginMgr == null)
			mLoginMgr = new LoginManager (mContext, mLoginListener, dbManager);
		mLoginMgr.loginProcess(id, pwd);
	}

	public void logout() {
		setLogined(false);
		try {
			Date date = Calendar.getInstance().getTime();
			APPSharedPrefer.getInstance(mContext).setPrefBool(APP.SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER, false);
			APPSharedPrefer.getInstance(mContext).setPrefString(APP.SP_USER_TOKEN, null);
			String simpleDate = ElseUtils.getSimpleDate(date);
			dbManager.logoutByUser(mContext, getUserId(), simpleDate);
			ElseUtils.setLockDownTime(getApplicationContext(), null);
			ElseUtils.setLockOverCount(getApplicationContext(), 0);
			ServiceRecordDao.getInstance().clearData(mContext);
		} catch (Exception e) {
			e.printStackTrace ();
		}
		setLogined(false);
	}

	public void logout(String userId) {
		setLogined(false);
		try {
			Date date = Calendar.getInstance().getTime();
			APPSharedPrefer.getInstance(mContext).setPrefBool(APP.SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER, false);
			APPSharedPrefer.getInstance(mContext).setPrefString(APP.SP_USER_TOKEN, null);
			String simpleDate = ElseUtils.getSimpleDate(date);
			dbManager.logoutByUser(mContext, userId, simpleDate);
			ElseUtils.setLockDownTime(getApplicationContext(), null);
			ElseUtils.setLockOverCount(getApplicationContext(), 0);
			ServiceRecordDao.getInstance().clearData(mContext);
		} catch (Exception e) {
			e.printStackTrace ();
		}
		setLogined(false);
	}

	public void deleteLoginInfo(final String id) {
		try {
			dbManager.deleteUserData(mContext, id);
			APPSharedPrefer.getInstance(mContext).setPrefBool(APP.SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER, false);
			APPSharedPrefer.getInstance(mContext).setPrefString(APP.SP_USER_TOKEN, null);
			ElseUtils.setLockDownTime(getApplicationContext(), null);
			ElseUtils.setLockOverCount(getApplicationContext(), 0);
			ScrapDao.getInstance().removeBankData(mContext, getRequest(), id);
			ServiceRecordDao.getInstance().clearData(mContext);
		} catch (Exception e) {
			e.printStackTrace();
		}
		setLogined(false);
	}

	private String mRequest;

	public synchronized void acquirePublicFromId(String id, final OnKeyInterface listener) {
		if (id != null && !TextUtils.isEmpty(id)) {
			UserApi api = ServiceGenerator.createService(UserApi.class);
			RequestDataByUserId requestId = new RequestDataByUserId();
			requestId.setUserId(id);
			Call<KeyDataSet> call = api.getSecureKey(requestId);
			call.enqueue(new Callback<KeyDataSet>() {
				@Override
				public void onResponse(Call<KeyDataSet> call, Response<KeyDataSet> response) {
					if (response.isSuccessful()) {
						KeyDataSet dataSet = response.body();
						if (dataSet != null && dataSet.getKey() != null) {
							if (listener != null) {
								String key = response.body().getKey();
								if (key == null || TextUtils.isEmpty(key)) {
									listener.fail();
								} else {
									mRequest = key;
									listener.acquired(key);
								}
							}
						} else if (dataSet != null && dataSet.getKey() == null && !dataSet.isOk()) {
							if (listener != null) {
								String key = response.body().getKey();
								if (key == null || TextUtils.isEmpty(key)) {
									ErrorMessage errorMessage = ErrorUtils.parseError(response);
									if (listener != null)
										listener.error(errorMessage);
								} else {
									mRequest = key;
									listener.acquired(key);
								}
							}
						} else {
							ErrorMessage errorMessage = ErrorUtils.parseError(response);
							if (listener != null)
								listener.error(errorMessage);
						}
					} else {

					}
				}

				@Override
				public void onFailure(Call<KeyDataSet> call, Throwable t) {
					if (listener != null) {
						listener.fail();
					}
				}
			});
		} else {
			if (listener != null) {
				listener.fail();
			}
		}
	}

	public void acquirePublic(OnKeyInterface listener) {
		String id = dbManager.getUserId(mContext);
		acquirePublicFromId(id, listener);
	}

	public boolean isAutoLoginAvailable () {
		if (mLoginMgr == null)
			mLoginMgr = new LoginManager (mContext, mLoginListener, dbManager);
		String[] info = dbManager.getLoginEncryptInfo(mContext);
		String id = info[0];
		String pwd = info[1];
		return mLoginMgr.checkAutoLoginAvailable(id, pwd);
	}

	public boolean isOldMember() {
		String[] info = dbManager.isOldMember(mContext);
		String lastLogin = info[2];
		boolean bool = false;
		if (lastLogin == null) {
			bool = true;
		}
		return bool;
	}

	public void onLoginComplete (Response<Result> result, LoginData login) {
		setUserInfo(result, login);
	}

	public void setSimpleUserInfo(LoginData data, final OnUserInfoUpdateCompleteInterface listener) {
		mUserId = data.getUserId();

		DayliApi api = ServiceGenerator.createServiceDayli(DayliApi.class);
		Call<JsonObject> call = api.getUserInfo(data.getDayliToken());
		call.enqueue(new Callback<JsonObject>() {
			@Override
			public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
				if (response != null && response.isSuccessful() && response.body() != null) {
					JsonObject object = response.body();
					userPersonalInfo = new Gson().fromJson(object, UserPersonalInfo.class);
					if (listener != null) {
						listener.complete(userPersonalInfo);
					}
				} else {
					userPersonalInfo = null;
					if (listener != null) {
						ErrorMessage message = ErrorUtils.parseError(response);
						listener.error(message);
					}
				}
			}

			@Override
			public void onFailure(Call<JsonObject> call, Throwable t) {
				userPersonalInfo = null;
				if (listener != null) {
					listener.error(ErrorMessage.ERROR_99999_UNKNOWN_ERROR);
				}
			}
		});
	}

	public void setUserInfo(final Response<Result> result, final LoginData login) {
		if (login == null) {
			clearSession();
			return;
		}

		mUserId = login.getUserId();

		if ((TextUtils.isEmpty(mUserId))) {
			clearSession();
			return;
		}

		acquirePublicFromId(mUserId, new OnKeyInterface() {
			@Override
			public void acquired(String str) {
				try {
					getUserPersonalInfo(str, new OnUserInfoUpdateCompleteInterface() {
						@Override
						public void complete(UserPersonalInfo info) {
							setLogined(true);
							if (mListener != null)
								mListener.onLoginSuccess(result, login);
						}

						@Override
						public void error(ErrorMessage message) {
							mListener.onLoginFail(LOGIN_DISABLE_UNKNOWN, message);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void error(ErrorMessage errorMessage) {
				switch (errorMessage) {
					case ERROR_713_NO_SUCH_USER:
					case ERROR_10007_SESSION_NOT_FOUND:
					case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
					case ERROR_20003_MISMATCH_ACCESS_TOKEN:
					case ERROR_20004_NO_SUCH_USER:
						break;
				}
			}

			@Override
			public void fail() {
				if (mListener != null)
					mListener.onLoginFail(LOGIN_DISABLE_UNKNOWN, result);
			}
		});
	}

	public UserPersonalInfo getUserPersonalInfo() {
		return userPersonalInfo;
	}

	public void refreshUserPersonalInfo(final OnUserInfoUpdateCompleteInterface listener) {
		acquirePublicFromId(getUserId(), new OnKeyInterface() {
			@Override
			public void acquired(String str) {
				try {
					getUserPersonalInfo(str, listener);
					setLogined(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void error(ErrorMessage errorMessage) {
				switch (errorMessage) {
					case ERROR_713_NO_SUCH_USER:
					case ERROR_10007_SESSION_NOT_FOUND:
					case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
					case ERROR_20003_MISMATCH_ACCESS_TOKEN:
					case ERROR_20004_NO_SUCH_USER:
						listener.error(errorMessage);
						break;
				}
			}

			@Override
			public void fail() {
				if (listener != null) {
					listener.error(ErrorMessage.ERROR_99999_UNKNOWN_ERROR);
				}
			}
		});
	}

	private void getUserPersonalInfo(final String key, final OnUserInfoUpdateCompleteInterface listener) {
		DayliApi api = ServiceGenerator.createServiceDayli(DayliApi.class);
		LoginData data = DbManager.getInstance().getUserInfoDataSet(mContext, key, Session.getInstance(mContext).getUserId());
		Call<JsonObject> call = api.getUserInfo(data.getDayliToken());
		call.enqueue(new Callback<JsonObject>() {
			@Override
			public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
				if (response != null && response.isSuccessful() && response.body() != null) {

					JsonObject object = response.body();
					userPersonalInfo = new Gson().fromJson(object, UserPersonalInfo.class);
					if (listener != null) {
						listener.complete(userPersonalInfo);
					}
					mRequest = key;
				} else {
					userPersonalInfo = null;
					if (listener != null) {
						ErrorMessage message = ErrorUtils.parseError(response);
						listener.error(message);
					}
				}
			}

			@Override
			public void onFailure(Call<JsonObject> call, Throwable t) {
				userPersonalInfo = null;
				if (listener != null) {
					listener.error(ErrorMessage.ERROR_99999_UNKNOWN_ERROR);
				}
			}
		});
	}

	private void clearSession() {
		setLogined(false);
		try {
			dbManager.deleteUserInfoDB(mContext);
			ScrapDao.getInstance().clearBankData(mContext);
			ServiceRecordDao.getInstance().clearData(mContext);
		} catch (Exception e) {
			e.printStackTrace ();
		}
	}

	public CdnAppInfoData getAppInfo() {
		if (appInfo == null || appInfo.version == null) {
			String data = APPSharedPrefer.getInstance(mContext).getPrefString(SharedPreferenceConstant.CDN_INIT_KEY);
			appInfo = new Gson().fromJson(data, CdnAppInfoData.class);
		}
		return appInfo;
	}

	public void setCdnAppInfoData(CdnAppInfoData data) {
		appInfo = data;
		APPSharedPrefer.getInstance(mContext).setPrefString(SharedPreferenceConstant.CDN_INIT_KEY, new Gson().toJson(data));
		ServiceGenerator.setAddress(data);
	}

	public void getCDNData() {
			FileDownloadService downloadService = FileDownloadService.retrofit.create(FileDownloadService.class);
			final Call<ResponseBody> call = downloadService.downloadFileFromCdn(ServiceUrl.getCdnVersion());
			AsyncTask task = new AsyncTask() {

				@Override
				protected void onPreExecute() {
					super.onPreExecute();
				}

				@Override
				protected Object doInBackground(Object[] params) {
					try {
						Response<ResponseBody> response = call.execute();
						if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
							try {
								String body = response.body().string();
								String decBody = Crypto.decCDNAES(body);
								JsonObject jsonObject = new Gson().fromJson(decBody, JsonObject.class);
								appInfo = new Gson().fromJson(jsonObject.get("aos"), CdnAppInfoData.class);
								setCdnAppInfoData(appInfo);
							} catch (IOException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				}
			};
			task.execute();
//			(new Callback<ResponseBody>() {
//				@Override
//				public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//					synchronized (mLock) {
//						if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
//							try {
//								String body = response.body().string();
//								String decBody = Crypto.decCDNAES(body);
//								JsonObject jsonObject = new Gson().fromJson(decBody, JsonObject.class);
//								appInfo = new Gson().fromJson(jsonObject.get("aos"), CdnAppInfoData.class);
//								setCdnAppInfoData(appInfo);
//							} catch (IOException e) {
//								e.printStackTrace();
//							} catch (Exception e) {
//								e.printStackTrace();
//							}
//						}
//						mLock.notifyAll();
//					}
//				}
//
//				@Override
//				public void onFailure(Call<ResponseBody> call, Throwable t) {
//					synchronized (mLock) {
//						mLock.notifyAll();
//					}
//				}
//			});
	}

	private boolean isIgnoredActivity(Activity activity) {
		String clazzName = activity.getClass().getName();

		// ignored activities
		if (mIgnoredActivities.contains(clazzName)) {
//			Log.d(TAG, "ignore activity " + clazzName);
			return true;
		}

		return false;
	}

	private boolean shouldShowLockScreen(Activity activity) {

		if (mIgnoredActivities.contains(activity.getClass().getName())) {
			return false;
		}

		// already unlock
		if (activity instanceof LockScreenActivity
				|| activity instanceof Splash) {
			return false;
		}

		if (isOldMember()) {
			return false;
		}

		if (!isPasscodeSet()) {
			return false;
		}

		if (isTemporaryDisable) {
			return false;
		}

		// no enough timeout
//		long passedTime = System.currentTimeMillis() - lastActive;
//		if (lastActive > 0 && passedTime <= mLockTimeOut) {
////			Log.d(TAG, "no enough timeout " + passedTime + " for "
////					+ mLockTimeOut);
//			return false;
//		}

		// start more than one page
		if (visibleCount > 1) {
			return false;
		}

		isLock = true;

		return true;
	}

	public boolean isAlive() {
		return (liveCount != 0);
	}

	public boolean isForeground() {
		return (visibleCount != 0);
	}

	@Override
	public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
//		if (isIgnoredActivity(activity)) {
//			return;
//		}
	}

	@Override
	public void onActivityStarted(Activity activity) {
//		if (isIgnoredActivity(activity)) {
//			return;
//		}

		visibleCount++;
	}

	@Override
	public void onActivityResumed(Activity activity) {
//		if (isIgnoredActivity(activity)) {
//			return;
//		}

		if (shouldShowLockScreen(activity)) {
			Intent intent = new Intent(activity.getApplicationContext(),
					LockScreenActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.getApplication().startActivity(intent);
		}

		lastActive = 0;
	}

	@Override
	public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//		if (isIgnoredActivity(activity)) {
//			return;
//		}

		liveCount++;
	}

	@Override
	public void onActivityPaused(Activity activity) {
//		if (isIgnoredActivity(activity)) {
//			return;
//		}
	}

	@Override
	public void onActivityStopped(Activity activity) {
//		if (isIgnoredActivity(activity)) {
//			return;
//		}

		visibleCount--;
		if (visibleCount == 0) {
			lastActive = System.currentTimeMillis();
//			Log.d(TAG, "set last active " + lastActive);
		}
	}

	@Override
	public void onActivityDestroyed(Activity activity) {
//		if (isIgnoredActivity(activity)) {
//			return;
//		}

		liveCount--;
		if (liveCount == 0) {
			lastActive = System.currentTimeMillis();
//			Log.d(TAG, "set last active " + lastActive);
		}
	}

	@Override
	public void enable() {

	}

	@Override
	public void disable() {

	}

	@Override
	public boolean setPasscode(String passcode) {
		return false;
	}

	@Override
	public boolean checkPasscode(String passcode) {
		return false;
	}

	@Override
	public boolean isPasscodeSet() {
		return isAutoLoginAvailable();
	}
}
