package com.nomadconnection.broccoli.session;

import java.util.HashSet;

public abstract class Lock {
	public static final int ENABLE_PASSLOCK = 0;
	public static final int DISABLE_PASSLOCK = 1;
	public static final int CHANGE_PASSWORD = 2;
	public static final int UNLOCK_PASSWORD = 3;

	public static final String MESSAGE = "message";
	public static final String TYPE = "type";

	public static final int DEFAULT_TIMEOUT = 0;

	protected int mLockTimeOut;
	protected HashSet<String> mIgnoredActivities;

	public void setTimeout(int timeout) {
		this.mLockTimeOut = timeout;
	}

	public Lock() {
		mIgnoredActivities = new HashSet<String>();
		mLockTimeOut = DEFAULT_TIMEOUT;
	}

	public void addIgnoredActivity(Class<?> clazz) {
		String clazzName = clazz.getName();
		this.mIgnoredActivities.add(clazzName);
	}

	public void removeIgnoredActivity(Class<?> clazz) {
		String clazzName = clazz.getName();
		this.mIgnoredActivities.remove(clazzName);
	}

	public abstract void enable();

	public abstract void disable();

	public abstract boolean setPasscode(String passcode);

	public abstract boolean checkPasscode(String passcode);

	public abstract boolean isPasscodeSet();
}
