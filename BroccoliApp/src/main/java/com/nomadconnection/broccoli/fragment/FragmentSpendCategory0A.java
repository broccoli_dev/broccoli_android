package com.nomadconnection.broccoli.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivitySpendCategoryDetail;
import com.nomadconnection.broccoli.adapter.AdtListViewSpendCategoryA;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Spend.BodySpendCategoryList;
import com.nomadconnection.broccoli.data.Spend.RequestDataByDate;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentSpendCategory0A extends BaseFragment {

	/* Layout */

	private LinearLayout mEmpty;
	private ImageView mEmptyIcon;
	private TextView mEmptyTitle;
	private ListView mListView;
	private String mTargetMonth;
	private AdtListViewSpendCategoryA mAdapter;
	private List<BodySpendCategoryList> mAlInfoDataList;
//	private String mSelectDate;


	public FragmentSpendCategory0A() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//View v = inflater.inflate(R.layout.fragment_asset_credit_detail_1_a, null);
		View v = inflater.inflate(R.layout.fragment_spend_category_0_a, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		requestDataSet();
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			Bundle bundle = getArguments();
			if (bundle != null) {
				mTargetMonth = bundle.getString(Const.DATA);
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			mEmpty = (LinearLayout)_v.findViewById(R.id.ll_empty_layout_layout);
			mEmptyIcon = (ImageView)_v.findViewById(R.id.iv_empty_layout_icon);
			mEmptyTitle = (TextView)_v.findViewById(R.id.tv_empty_layout_txt);

			mListView = (ListView)_v.findViewById(R.id.lv_fragment_spend_category_0_a_listview);
    		mAdapter = new AdtListViewSpendCategoryA(getActivity(), mAlInfoDataList);
    		mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(ItemClickListener);
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mEmpty.setVisibility(View.GONE);
			requestDataSet();
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}
	
	
	
//=========================================================================================//
// Refresh Adapter
//=========================================================================================//
	public void refreshAdt(String _sort){
		mAdapter.notifyDataSetChanged();
	}


	/**  Item Click Listener **/
	private AdapterView.OnItemClickListener ItemClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {
			if (mAlInfoDataList.get(position) == null) {	// test 임시
				return;
			}

			Intent intent = new Intent(getActivity(), Level3ActivitySpendCategoryDetail.class);
			intent.putExtra(Const.SPEND_CATEGORY_DATE, mTargetMonth);
			intent.putExtra(Const.SPEND_CATEGORY_CODE, mAlInfoDataList.get(position).getCategoryId());
			startActivity(intent);
		}
	};


	public void requestDataSet() {
		RequestDataByDate userDate = new RequestDataByDate();
		userDate.setUserId(Session.getInstance(getActivity()).getUserId());
		userDate.setDate(mTargetMonth);


		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<List<BodySpendCategoryList>> call = api.getCategory(userDate);
		call.enqueue(new Callback<List<BodySpendCategoryList>>() {
			@Override
			public void onResponse(Call<List<BodySpendCategoryList>> call, Response<List<BodySpendCategoryList>> response) {
				if (getActivity() != null && !getActivity().isFinishing()) {
					mAlInfoDataList = new ArrayList<BodySpendCategoryList>();
					mAlInfoDataList = response.body();
					mAdapter.setData((ArrayList<BodySpendCategoryList>) mAlInfoDataList);
					refreshAdt(null);

					if(mAlInfoDataList.size() != 0){
						mEmpty.setVisibility(View.GONE);
						mListView.setVisibility(View.VISIBLE);
					}
					else {
						String title = getActivity().getResources().getString(R.string.spend_empty);
						mEmpty.setVisibility(View.VISIBLE);
						mListView.setVisibility(View.GONE);
						mEmptyIcon.setImageResource(R.drawable.empty_icon_nohistory);
						mEmptyTitle.setText(title);
					}
				}
			}

			@Override
			public void onFailure(Call<List<BodySpendCategoryList>> call, Throwable t) {
				if (getActivity() != null && !getActivity().isFinishing())
					ElseUtils.network_error(getActivity());
			}
		});
	}

}

