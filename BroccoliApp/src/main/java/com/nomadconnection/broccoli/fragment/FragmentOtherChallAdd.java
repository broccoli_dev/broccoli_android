package com.nomadconnection.broccoli.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.common.dialog.CommonDialogListType;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeBank;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.Etc.RequestEtcChallengeBank;
import com.nomadconnection.broccoli.data.Etc.RequestEtcChallengeDelete;
import com.nomadconnection.broccoli.data.Etc.RequestEtcChallengeSave;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.interf.InterfaceChallenge;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ImageUtil;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentOtherChallAdd extends BaseFragment implements OnClickListener{

	private static String TAG = "FragmentOtherChallAdd";
	private static final int GALLERY = 1;
	private static final int CAMERA = 2;
	private Uri mImageCaptureUri;

	InterfaceEditOrDetail mInterFaceEditOrDetail;
	InterfaceChallenge mInterfaceChallenge;
	/* Layout */
	private ImageView mChallImage;
	private ImageView mChallImageChange;
	private LinearLayout mChallCategoryLL;
	private TextView mChallCategory;
	private EditText mChallTitle;
	private LinearLayout mChallTargetDateEndLL;
	private TextView mChallTargetDateEnd;
	private EditText mChallTargetSum;
	private LinearLayout mChallBankLinkLL;
	private TextView mChallBankLinkName;
	private TextView mChallBankLinkAccount;
	private EditText mChallMonthlySum;
	private EtcChallengeInfo mEtcChallengeInfo = null;
	private List<EtcChallengeBank> mEtcChallengeBankList;
	private int mImageChange = 0;
	private int mCategorySelect = -1;
	private int mBankSelect = 0;
	private String[] mTextWatcherResult;
	private String mImgPath;
	private Calendar mCalendar;


	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.ll_navi_img_txt_txt_back:
				getActivity().finish();
				break;

			case R.id.ll_navi_img_txt_txt_else:
				mInterFaceEditOrDetail.onDetail();
				break;

			default:
				break;
			}

		}
	};

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
			mInterfaceChallenge = (InterfaceChallenge) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_other_chall_add, null);
		((BaseActivity) getActivity()).sendTracker(AnalyticsName.FragmentOtherChallAdd);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		return v;
	}

	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onClick(View v) {
		//Calendar calendar = Calendar.getInstance();
		switch (v.getId()){
			case R.id.imv_fragment_other_chall_add:
				photo_add_dialog();
//				dialog_challenge_image_change();
				break;
			case R.id.ll_fragment_other_chall_add_category:
				dialog_challenge_type();
				break;
			case R.id.ll_fragment_other_chall_add_target_date_end:
				final Calendar calendar = Calendar.getInstance();
				if(mEtcChallengeInfo == null){
					DatePickerDialog EndDatePickerDialog = new DatePickerDialog(this.getActivity(), AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							mChallTargetDateEnd.setText(year+"."+(monthOfYear+1)+"."+dayOfMonth);
							if(mChallTargetSum.getText().length() != 0){
								String month = autoMonthlySum(mChallTargetSum.getText().toString().replaceAll(",", ""));
								mChallMonthlySum.setText(month);
								send_interface_complete();
							}
						}
					}, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
					// Set calendar to 1 day next from today
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					// Set calendar to 1 month next
					calendar.add(Calendar.MONTH, 1);
					EndDatePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
					EndDatePickerDialog.show();
				}
				else {
					String getDate = mEtcChallengeInfo.getEndDate();
					int year =Integer.parseInt(getDate.substring(0,4));
					int month = Integer.parseInt(getDate.substring(4,6));
					int day = Integer.parseInt(getDate.substring(6,8));
					mCalendar.set(year, month-1, day);
					DatePickerDialog EndDatePickerDialog = new DatePickerDialog(this.getActivity(), AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							mChallTargetDateEnd.setText(year+"."+(monthOfYear+1)+"."+dayOfMonth);
							if(mChallTargetSum.getText().length() != 0){
								String month = autoMonthlySum(mChallTargetSum.getText().toString().replaceAll(",", ""));
								mChallMonthlySum.setText(month);
								send_interface_complete();
							}
						}
					}, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
					// Set calendar to 1 day next from today
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					// Set calendar to 1 month next
					calendar.add(Calendar.MONTH, 1);
					EndDatePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
					EndDatePickerDialog.show();
				}
				break;
			case R.id.ll_fragment_other_chall_add_bank_link:
				challenge_bank_link(true);
				break;

		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode != getActivity().RESULT_OK) {
			return;
		}

		switch (requestCode){
			case CAMERA:
					// 이미지 Path 취득
					mImgPath = mImageCaptureUri.getPath();
					updateImageView();
				break;

			case GALLERY:
				if(data != null) {
					// 앨범인 경우
					Uri mImageUri = data.getData();

					// 이미지 Path 취득
					mImgPath = getPath(mImageUri);
					updateImageView();
				}
				break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Session.getInstance(getActivity()).setLockTemporaryDisable(false);
	}

	public void onComplete(){
		View view = getActivity().getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
		if(emptyCheck()){
			challenge_save();
		}
		else {
			Toast.makeText(getActivity(),getActivity().getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
		}

	}
	
	public void onDelete(){
		challenge_delete();
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			mCalendar = Calendar.getInstance();
			mCalendar.add(Calendar.MONTH, 3); // 3을 더한다.

			mTextWatcherResult = new String[2];
			Intent intent = new Intent(getActivity().getIntent());
			if(intent.hasExtra(Const.CHALLENGE_INFO)){
				mEtcChallengeInfo = (EtcChallengeInfo)intent.getSerializableExtra(Const.CHALLENGE_INFO);
				challenge_bank_link(false);
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mChallImage = (ImageView)_v.findViewById(R.id.lmv_fragment_other_chall_add_image);
			mChallImageChange = (ImageView)_v.findViewById(R.id.imv_fragment_other_chall_add);
			mChallImageChange.setOnClickListener(this);
			mChallCategoryLL = (LinearLayout)_v.findViewById(R.id.ll_fragment_other_chall_add_category);
			mChallCategoryLL.setOnClickListener(this);
			mChallCategory = (TextView)_v.findViewById(R.id.tv_fragment_other_chall_add_category);
			mChallTitle = (EditText)_v.findViewById(R.id.et_fragment_other_chall_add_subject);
			mChallTitle.addTextChangedListener(new CustomTextWatcher(mChallTitle));
			mChallTargetDateEndLL = (LinearLayout)_v.findViewById(R.id.ll_fragment_other_chall_add_target_date_end);
			mChallTargetDateEndLL.setOnClickListener(this);
			mChallTargetDateEnd = (TextView)_v.findViewById(R.id.tv_fragment_other_chall_add_target_date_end);
			mChallTargetSum = (EditText)_v.findViewById(R.id.et_fragment_other_chall_add_target_sum);
			mChallTargetSum.addTextChangedListener(new CustomTextWatcher(mChallTargetSum));
			mChallBankLinkLL = (LinearLayout)_v.findViewById(R.id.ll_fragment_other_chall_add_bank_link);
			mChallBankLinkLL.setOnClickListener(this);
			mChallBankLinkName = (TextView)_v.findViewById(R.id.tv_fragment_other_chall_add_bank_link_name);
			mChallBankLinkAccount = (TextView)_v.findViewById(R.id.tv_fragment_other_chall_add_bank_link_account);
			mChallMonthlySum = (EditText)_v.findViewById(R.id.et_fragment_other_chall_add_month_sum);
			mChallMonthlySum.addTextChangedListener(new CustomTextWatcher(mChallMonthlySum));
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}
	



//===============================================================================//
// Button
//===============================================================================//	

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			if(mEtcChallengeInfo != null){
				String[] mChallengeType = getActivity().getResources().getStringArray(R.array.challenge_type);
				int count = Integer.parseInt(mEtcChallengeInfo.getChallengeType());
				mCategorySelect = count-1;
				mChallCategory.setText(mChallengeType[count-1]);
				mChallTitle.setText(mEtcChallengeInfo.getTitle());
				mChallTargetDateEnd.setText(ElseUtils.getBirthDay(mEtcChallengeInfo.getEndDate()));
				mChallTargetSum.setText(mEtcChallengeInfo.getGoalAmt());
				mChallMonthlySum.setText(mEtcChallengeInfo.getMonthlyAmt());
				if("".equals(mEtcChallengeInfo.getAccountId())){
					mChallBankLinkAccount.setVisibility(View.GONE);
					mChallBankLinkName.setText(getActivity().getResources().getString(R.string.other_challenge_detail_bank_link_need));
				}

				if(! "".equals(mEtcChallengeInfo.getImagePath())){
					mImgPath = mEtcChallengeInfo.getImagePath();
					if(ImageUtil.fileCheck(mImgPath)){
						updateImageView();
					}
				}
			}
			else {
				String endDate = Integer.toString(mCalendar.get(Calendar.YEAR))
						+ "." + Integer.toString(mCalendar.get(Calendar.MONTH)+1) + "." + Integer.toString(mCalendar.get(Calendar.DAY_OF_MONTH)) ;
				mChallBankLinkAccount.setVisibility(View.GONE);
				mChallCategory.setHintTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				mChallCategory.setHint(getString(R.string.common_category_select));
				mChallTitle.setHint(getString(R.string.common_title_input));
//				mChallTargetDateEnd.setHintTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
//				mChallTargetDateEnd.setHint(getString(R.string.common_end_date));
				mChallTargetDateEnd.setText(endDate);
				mChallTargetSum.setHint(getString(R.string.common_input_sum_hint));
				mChallBankLinkName.setHintTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
				mChallBankLinkName.setHint(getString(R.string.other_challenge_detail_bank_link_need));
				mChallMonthlySum.setHint(getString(R.string.common_input_sum_hint));
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//===============================================================================//
// spend_budget_watcher
//===============================================================================//

//===============================================================================//
// Bank Link get API
//===============================================================================//
	private void challenge_bank_link(final boolean value){
		DialogUtils.showLoading(getActivity());
		RequestEtcChallengeBank mRequestEtcChallengeBank = new RequestEtcChallengeBank();
		mRequestEtcChallengeBank.setUserId(Session.getInstance(getActivity()).getUserId());
		mRequestEtcChallengeBank.setAccountType(0);

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<List<EtcChallengeBank>> call = api.challengeBankLink(mRequestEtcChallengeBank);
		call.enqueue(new Callback<List<EtcChallengeBank>>() {
			@Override
			public void onResponse(Call<List<EtcChallengeBank>> call, Response<List<EtcChallengeBank>> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						mEtcChallengeBankList = new ArrayList<EtcChallengeBank>();
						mEtcChallengeBankList = response.body();

						EtcChallengeBank mEtcChallengeBank = new EtcChallengeBank();
						mEtcChallengeBank.setAccountId("");
						mEtcChallengeBank.setCompanyName("");
						mEtcChallengeBank.setAccountName(getResources().getString(R.string.common_text_no));
						mEtcChallengeBank.setAccountNum("");
						mEtcChallengeBank.setAddedYn("");
						mEtcChallengeBankList.add(0, mEtcChallengeBank);

						if (value) {
							dialog_challenge_bank_link();
						} else {
							for (int b = 0; b < mEtcChallengeBankList.size(); b++) {
								if (mEtcChallengeBankList.get(b).getAccountId().equals(mEtcChallengeInfo.getAccountId())) {
									if ("".equals(mEtcChallengeInfo.getAccountId())) {
										mChallBankLinkAccount.setVisibility(View.GONE);
										mChallBankLinkName.setText(getActivity().getResources().getString(R.string.other_challenge_detail_bank_link_need));
									} else {
										mChallBankLinkAccount.setVisibility(View.VISIBLE);
										String name = mEtcChallengeBankList.get(b).getCompanyName() + " " + mEtcChallengeBankList.get(b).getAccountName();
										mChallBankLinkName.setText(name);
										mChallBankLinkAccount.setText(mEtcChallengeBankList.get(b).getAccountNum());
										mBankSelect = b;
									}
								}
							}
						}
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<List<EtcChallengeBank>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void dialog_challenge_type() {
		String mStr4 = getString(R.string.common_category_select);
		DialogUtils.showDlgBaseList(getActivity(), new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						String[] mChallengeType = getActivity().getResources().getStringArray(R.array.challenge_type);
						String bank = mChallengeType[position];
						mChallCategory.setText(bank);
						mCategorySelect = position;
						send_interface_complete();
					}
				},
				new InfoDataDialog(APP.DIALOG_CHALLENGE_TYPE_L, mStr4, getString(R.string.common_cancel)), mCategorySelect);
	}

	private void dialog_challenge_bank_link(){
		String mStr4 = getString(R.string.other_challenge_detail_bank_link);
		DialogUtils.showDlgBaseListBank(getActivity(), new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						if (position == 0) {
							mChallBankLinkAccount.setVisibility(View.GONE);
							mChallBankLinkName.setText(getActivity().getResources().getString(R.string.other_challenge_detail_bank_link_need));
						} else {
							mChallBankLinkAccount.setVisibility(View.VISIBLE);
							EtcChallengeBank mTempBank = (EtcChallengeBank) mEtcChallengeBankList.get(position);
							String BankName = mTempBank.getCompanyName() + " " + mTempBank.getAccountName();
							mChallBankLinkName.setText(BankName);
							mChallBankLinkAccount.setText(mTempBank.getAccountNum());
						}
						mBankSelect = position;
					}
				},
				new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_L, mStr4, getString(R.string.common_cancel)), mBankSelect, mEtcChallengeBankList);
	}

//===============================================================================//
// Challenge Save API
//===============================================================================//
	private void challenge_save(){
		DecimalFormat decimalFormat = new DecimalFormat("00");
		DialogUtils.showLoading(getActivity());
		int challengeId = mCategorySelect +1;
		String dateEnd = ElseUtils.dateUploadFormat(mChallTargetDateEnd.getText().toString());

		RequestEtcChallengeSave mRequestEtcChallengeSave = new RequestEtcChallengeSave();
		mRequestEtcChallengeSave.setUserId(Session.getInstance(getActivity()).getUserId());
		if(mEtcChallengeInfo != null){
			mRequestEtcChallengeSave.setChallengeId(mEtcChallengeInfo.getChallengeId());
		}
		else {
			mRequestEtcChallengeSave.setChallengeId("0");
		}
		mRequestEtcChallengeSave.setChallengeType(decimalFormat.format(challengeId));
		mRequestEtcChallengeSave.setTitle(mChallTitle.getText().toString());
		mRequestEtcChallengeSave.setGoalDate(dateEnd);
		if(mEtcChallengeBankList != null){
			mRequestEtcChallengeSave.setAccountId(bank_link_getId());
		}
		else {
			mRequestEtcChallengeSave.setAccountId("");
		}

		mRequestEtcChallengeSave.setGoalAmt(mChallTargetSum.getText().toString().replaceAll(",", ""));
		mRequestEtcChallengeSave.setMonthlyAmt(mChallMonthlySum.getText().toString().replaceAll(",", ""));
		if(mImgPath != null){
			mRequestEtcChallengeSave.setImagePath(mImgPath);
		}
		else {
			mRequestEtcChallengeSave.setImagePath("");
		}

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<Result> call = api.challengeSave(mRequestEtcChallengeSave);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();
				mInterfaceChallenge.onSaveComplete();
			}


			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();
				mInterfaceChallenge.onSaveError();
			}
		});
	}


	private void dialog_challenge_image_change(){
		Session.getInstance(getActivity()).setLockTemporaryDisable(true);
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
		intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, GALLERY);
	}

	private String bank_link_getId(){
		String result = "";
		for(int a = 0; a < mEtcChallengeBankList.size(); a++){
			if(mBankSelect == a){
				result = mEtcChallengeBankList.get(a).getAccountId();
			}
		}
		return result;
	}

	//===============================================================================//
// Challenge Save API
//===============================================================================//
	private void challenge_delete(){
		RequestEtcChallengeDelete mRequestEtcChallengeDelete = new RequestEtcChallengeDelete();
		mRequestEtcChallengeDelete.setUserId(Session.getInstance(getActivity()).getUserId());
		mRequestEtcChallengeDelete.setChallengeId(mEtcChallengeInfo.getChallengeId());

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<Result> call = api.challengeDelete(mRequestEtcChallengeDelete);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response != null) {
						mInterfaceChallenge.onDeleteComplete();
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();
				mInterfaceChallenge.onSaveError();
			}
		});
	}

	private String getPath(Uri uri) {
		String res = null;
		String[] projection = { MediaStore.Images.Media.DATA};

		Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
		if(cursor.moveToFirst()){
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			res = cursor.getString(column_index);
		}
		cursor.close();

		return res;
	}

	private void updateImageView() {
		DisplayMetrics dm = getActivity().getApplicationContext().getResources().getDisplayMetrics();
		int width = dm.widthPixels;
		int degree = ImageUtil.GetExifOrientation(mImgPath);
		Bitmap backgroundBitmap = ImageUtil.loadBackgroundBitmap(getActivity(), mImgPath);
		Bitmap rotateBitmap = ImageUtil.GetRotatedBitmap(backgroundBitmap, degree);
		Bitmap resizeBitmap = ImageUtil.resizeBitmap(rotateBitmap, width, 609);
		Bitmap cropBitmap = ImageUtil.cropCenterBitmap(resizeBitmap, 400, 400);
		Bitmap roundBitmap = ImageUtil.getRoundedCornerBitmap(cropBitmap);
		mChallImage.setImageBitmap(roundBitmap);
		backgroundBitmap.recycle();
	}

	private boolean emptyCheck(){
		if(mChallTargetDateEnd.getText().length() != 0
				&& mChallTitle.getText().length() != 0
				&& mChallMonthlySum.getText().length() != 0
				&& mChallTargetSum.getText().length() != 0
				&& mChallCategory.getText().length() != 0 ){
			return true;

		}
		else {
			return false;
		}
	}

	private String autoMonthlySum(String goalAmt){
		String monthlySum = null;
		long mGoalAmt = Integer.parseInt(goalAmt);
		long monthSum = Math.round(mGoalAmt / monthTermCalculator());
		monthlySum = Long.toString(monthSum);
		return monthlySum;
	}

	private int monthTermCalculator(){
		//String endDate = mChallTargetDateEnd.getText().toString().replaceAll("\\.", "");
		String[] endDate = mChallTargetDateEnd.getText().toString().split("\\.");

		int month = 0;
		int strtYear;
		int strtMonth;
		int endYear;
		int endMonth;
		if(mEtcChallengeInfo != null){
			strtYear = Integer.parseInt(mEtcChallengeInfo.getStartDate().substring(0, 4));
			strtMonth = Integer.parseInt(mEtcChallengeInfo.getStartDate().substring(4, 6));

			endYear = Integer.parseInt(endDate[0]);
			endMonth = Integer.parseInt(endDate[1]);

			month = (endYear - strtYear)* 12 + (endMonth - strtMonth);
		}
		else {
			long now = System.currentTimeMillis();
			Date date = new Date(now);
			SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyyMMdd");
			String strCurDate = CurDateFormat.format(date);

			strtYear = Integer.parseInt(strCurDate.substring(0, 4));
			strtMonth = Integer.parseInt(strCurDate.substring(4, 6));

			endYear = Integer.parseInt(endDate[0]);
			endMonth = Integer.parseInt(endDate[1]);

			month = (endYear - strtYear)* 12 + (endMonth - strtMonth);
		}

		return month;
	}

	private void send_interface_complete(){
		if(emptyCheck()){
			mInterFaceEditOrDetail.onEdit();
		}
		else {
			mInterFaceEditOrDetail.onError();
		}
	}

	private void photo_add_dialog(){
		Session.getInstance(getActivity()).setLockTemporaryDisable(true);
		final ArrayList<String> photo_list = new ArrayList<>();
		//photo_list.add(getString(R.string.common_camera));
		photo_list.add(getString(R.string.common_album));
		final CommonDialogListType photoDialog= new CommonDialogListType(getContext(), null, new int[]{R.string.common_cancel},  photo_list);
		photoDialog.setDialogListener(new CommonDialogListType.DialogListTypeListener() {
			@Override
			public void onButtonClick(DialogInterface dialog, int stringResId) {
				photoDialog.dismiss();
			}

			@Override
			public void onItemClick(DialogInterface dialog, int itemPosition) {
				dialog.dismiss();
				switch (itemPosition){
//					case 0: // 카메라
//						Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//						// 임시로 사용할 파일의 경로를 생성
//						String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
//						mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));
//
//						camera.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
//						// 특정기기에서 사진을 저장못하는 문제가 있어 다음을 주석처리 합니다.
//						//intent.putExtra("return-data", true);
//						startActivityForResult(camera, CAMERA);
////						Intent camera = new Intent();
////						camera.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
////						mImageCaptureUri = getAuthImgUri();
////						camera.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
////						startActivityForResult(camera, CAMERA);
//						break;
					case 0: //앨범
						Intent intent = new Intent(Intent.ACTION_PICK);
						intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
						intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						startActivityForResult(intent, GALLERY);
						break;
				}

			}
		});
		photoDialog.show();
	}

	private class CustomTextWatcher implements TextWatcher {
		private EditText mEditText;

		public CustomTextWatcher(EditText editText) {
			mEditText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			String current = s.toString().replaceAll(",", "");

			switch (mEditText.getId()) {
				case R.id.et_fragment_other_chall_add_target_sum:
					if (!s.toString().equals(mTextWatcherResult[0])) {
						try {
							if("".equals(current)){
								mChallTargetSum.setHint(getString(R.string.common_input_sum_hint));
								if(mChallMonthlySum.length() == 1){
									mChallMonthlySum.setText("");
								}
							}
							else{
								// The comma in the format specifier does the trick
								mTextWatcherResult[0] = String.format("%,d", Long.parseLong(current));
								mChallTargetSum.setText(mTextWatcherResult[0]);
								mChallTargetSum.setSelection(mTextWatcherResult[0].length());
								String month = autoMonthlySum(mChallTargetSum.getText().toString().replaceAll(",", ""));
								mChallMonthlySum.setText(month);
							}
							send_interface_complete();
						} catch (NumberFormatException e) {
							Log.e("broccoli", "afterTextChanged[0] : " + e.toString());
						}
					}
					break;

				case R.id.et_fragment_other_chall_add_month_sum:
					if (!s.toString().equals(mTextWatcherResult[1])) {
						try {
							if("".equals(current)){
								mChallMonthlySum.setHint(getString(R.string.common_input_sum_hint));
							}
							else {
								// The comma in the format specifier does the trick
								mTextWatcherResult[1] = String.format("%,d", Long.parseLong(current));
								mChallMonthlySum.setText(mTextWatcherResult[1]);
								mChallMonthlySum.setSelection(mTextWatcherResult[1].length());
							}
							send_interface_complete();
						} catch (NumberFormatException e) {
							Log.e("broccoli", "afterTextChanged[0] : " + e.toString());
						}
					}
					break;

				case R.id.et_fragment_other_chall_add_subject:
					if(s.length() == 0){
						mChallTitle.setHint(getString(R.string.common_title_input));
					}
					send_interface_complete();
					break;

				default:
					break;
			}
		}
	}
}
