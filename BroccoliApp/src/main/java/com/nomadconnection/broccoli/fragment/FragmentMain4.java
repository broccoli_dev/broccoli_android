package com.nomadconnection.broccoli.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level2ActivityOtherChall;
import com.nomadconnection.broccoli.activity.Level2ActivityOtherMini;
import com.nomadconnection.broccoli.adapter.AdtListViewMain4;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Etc.BodyMoneyInfo;
import com.nomadconnection.broccoli.data.Etc.EtcMainData;
import com.nomadconnection.broccoli.data.InfoDataMain4;
import com.nomadconnection.broccoli.data.RequestMainData;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.constant.ErrorCode;
import com.nomadconnection.broccoli.constant.ErrorMessage;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FragmentMain4 extends BaseFragment implements InterfaceFragmentInteraction {

	private static String TAG = FragmentMain4.class.getSimpleName();
	
	/* Layout */
	public ListView mListView;
	private AdtListViewMain4 mAdapter;
	private ArrayList<InfoDataMain4> mAlInfoDataList;

	/*Data Class*/
	private EtcMainData mEtcMainData;
	
//	private SwipeRefreshLayout mSwipeRefresh;
//	InterFaceRefreshCallBack mInterFaceRefreshCallBack;
//	
//	@Override
//	public void onAttach(Activity activity) {
//		super.onAttach(activity);
//		
//		// This makes sure that the container activity has implemented
//		// the callback interface. If not, it throws an exception
//		try {
//			mInterFaceRefreshCallBack = (InterFaceRefreshCallBack) activity;
//		} catch (ClassCastException e) {
//			throw new ClassCastException(activity.toString()
//					+ " must implement InterFaceRefreshCallBack");
//		}
//	}
	/**  Item Click Listener **/
	private OnItemClickListener ItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			if (position != 0 && mAlInfoDataList.get(position) == null) {
				return;
			}

			switch (mAlInfoDataList.get(position).mTxt1) {
				case "챌린지":
					Intent Challenge = new Intent(getActivity(), Level2ActivityOtherChall.class);
					Challenge.putExtra(Const.DATA, (Serializable) mEtcMainData.mMainEtcChallengeInfo);
					//Challenge.putParcelableArrayListExtra(Const.DATA, mAlInfoDataList);
					startActivity(Challenge);
					break;

				case "머니 캘린더":
					startActivityForResult(new Intent(getActivity(), Level2ActivityOtherMini.class)
							.putExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_1, new ArrayList<BodyMoneyInfo>())
							, APP.ACTIVIYT_FOR_RESULT_MAIN_4_REQUEST_CODE);
					break;

				default:
					break;
			}

//			switch (position) {
//			case 0:
//				Intent Challenge = new Intent(getActivity(), Level2ActivityOtherChall.class);
//				Challenge.putExtra(Const.DATA, (Serializable) mEtcMainData.mMainEtcChallengeInfo);
//				//Challenge.putParcelableArrayListExtra(Const.DATA, mAlInfoDataList);
//				startActivity(Challenge);
//				break;
//
//			case 1:
//				startActivityForResult(new Intent(getActivity(), Level2ActivityOtherMini.class)
//						.putExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_1, new ArrayList<BodyMoneyInfo>())
//						, APP.ACTIVIYT_FOR_RESULT_MAIN_4_REQUEST_CODE);
////				ServiceGenerator.createService(EtcApi.class).getCalendarList(
////						new RequestEtcMoneyList(Session.getInstance(getActivity()).getUserId(), TransFormatUtils.getDataFormatWhich(3))
//////						new RequestEtcMoneyList(Session.getInstance(getActivity()).getUserId(), mAlInfoDataList.get(position).mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.get(0).mBodyMoneyPayDate)
////				).enqueue(new Callback<ArrayList<BodyMoneyInfo>>() {
////					@Override
////					public void onResponse(Response<ArrayList<BodyMoneyInfo>> response) {
////						ArrayList<BodyMoneyInfo> mTemp = new ArrayList<BodyMoneyInfo>();
////						if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData17();
////						else mTemp = response.body();
////
////						Collections.sort(mTemp, new Comparator<BodyMoneyInfo>() {
////							@Override
////							public int compare(BodyMoneyInfo lhs, BodyMoneyInfo rhs) {
////								int a = Integer.valueOf(lhs.mBodyMoneyPayDate);
////								int b = Integer.valueOf(rhs.mBodyMoneyPayDate);
////								return (b > a ? -1 : 1);
////							}
////						});
////
////						startActivityForResult(new Intent(getActivity(), Level2ActivityOtherMini.class)
////								.putExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_1, mTemp)
////								, APP.ACTIVIYT_FOR_RESULT_MAIN_4_REQUEST_CODE);
////					}
////
////					@Override
////					public void onFailure(Throwable t) {
////						if (APP._SAMPLE_MODE_) {
////							ArrayList<BodyMoneyInfo> mTemp = new ArrayList<BodyMoneyInfo>();
////							mTemp = AssetSampleTestDataJson.GetSampleData17();
////							Collections.sort(mTemp, new Comparator<BodyMoneyInfo>() {
////								@Override
////								public int compare(BodyMoneyInfo lhs, BodyMoneyInfo rhs) {
////									int a = Integer.valueOf(lhs.mBodyMoneyPayDate);
////									int b = Integer.valueOf(rhs.mBodyMoneyPayDate);
////									return (b > a ? -1 : 1);
////								}
////							});
////							startActivityForResult(new Intent(getActivity(), Level2ActivityOtherMini.class)
////									.putExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_1, mTemp)
////									, APP.ACTIVIYT_FOR_RESULT_MAIN_4_REQUEST_CODE);
////						} else
////							CustomToast.getInstance(getActivity()).showShort("FragmentMain4 서버 통신 실패 : 머니캘린더 리스트");
////					}
////				});
//
//				break;
//
//			default:
//				break;
//			}
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_main4, null);

		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		return v;
	}

	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == APP.ACTIVIYT_FOR_RESULT_MAIN_4_REQUEST_CODE) {
			if(resultCode == APP.ACTIVIYT_FOR_RESULT_MAIN_4_RESULT_CODE_SUCCESS){
				refreshData();
			}
			else if(resultCode == APP.ACTIVIYT_FOR_RESULT_MAIN_4_RESULT_CODE_CANCLE){
			}
		}
	}

	@Override
	public void onResume() {
		refreshData();
		super.onResume();
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}
			mAlInfoDataList = new ArrayList<InfoDataMain4>();
//			mAlInfoDataList.addAll(SampleTestData.GetSampleDataMain4());
//			mEtcMainData = AssetSampleTestDataJson.GetSampleData16();

			refreshData();

//			RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
//			mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());
//
//			Call<EtcMainData> mCall = ServiceGenerator.createService(EtcApi.class).getEtcMain(mRequestDataByUserId);
//			mCall.enqueue(new Callback<EtcMainData>() {
//				@Override
//				public void onResponse(Response<EtcMainData> response) {
//					if (APP._SAMPLE_MODE_)
//						mEtcMainData = AssetSampleTestDataJson.GetSampleData16();
//					else mEtcMainData = response.body();
//						resetData();
//				}
//
//				@Override
//				public void onFailure(Throwable t) {
//					if (APP._SAMPLE_MODE_) {
//						mEtcMainData = AssetSampleTestDataJson.GetSampleData16();
//						resetData();
//					} else
//						CustomToast.getInstance(getActivity()).showShort("FragmentMain4 서버 통신 실패 : 매인");
//				}
//			});


		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Layout */
    		mListView = (ListView)_v.findViewById(R.id.lv_fragment_main_4_listview);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				mListView.setNestedScrollingEnabled(true);
			}

    		mAdapter = new AdtListViewMain4(getActivity(), mAlInfoDataList);
    		mListView.setAdapter(mAdapter);
    		mListView.setOnItemClickListener(ItemClickListener);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//	

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}





//===============================================================================//
// Refresh
//===============================================================================//

	/**  Refresh **/
	private void refreshData(){
		RequestMainData requestMainData = new RequestMainData();
		requestMainData.setUserId(Session.getInstance(getActivity()).getUserId());
		requestMainData.setDeviceId(ElseUtils.getDeviceUUID(getContext()));

		Call<EtcMainData> mCall = ServiceGenerator.createService(EtcApi.class).getEtcMain(requestMainData);
		mCall.enqueue(new Callback<EtcMainData>() {
			@Override
			public void onResponse(Call<EtcMainData> call, Response<EtcMainData> response) {
				if (response.body() != null) {
					mEtcMainData = response.body();
					String code = mEtcMainData.getCode();
					if (mEtcMainData != null && (code == null || code.isEmpty())) {
						resetData();
					} else {
						if (ErrorMessage.ERROR_716_TOKEN_EXPIRE.code().equalsIgnoreCase(mEtcMainData.getCode())
								|| ErrorMessage.ERROR_10007_SESSION_NOT_FOUND.code().equalsIgnoreCase(mEtcMainData.getCode())
								|| ErrorMessage.ERROR_10008_PWD_CHANGED_SESSION_EXPIRED.code().equalsIgnoreCase(mEtcMainData.getCode())
								|| ErrorMessage.ERROR_20003_MISMATCH_ACCESS_TOKEN.code().equalsIgnoreCase(mEtcMainData.getCode())) {
							memberLeaveComplete();
						} else {
							ElseUtils.network_error(getActivity());
						}
					}
				} else {
					Retrofit retrofit = ServiceGenerator.getRetrofit();
					Converter<ResponseBody, EtcMainData> converter = retrofit.responseBodyConverter(EtcMainData.class, new Annotation[0]);
					try {
						EtcMainData data = converter.convert(response.errorBody());
						if (ErrorCode.ERROR_CODE_701_LOGIN_INCORRECT_DEVICE.equalsIgnoreCase(data.getCode())) {
							memberLeaveComplete();
						} else {
							ElseUtils.network_error(getActivity());
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(Call<EtcMainData> call, Throwable t) {
				if (APP._SAMPLE_MODE_) {
					mEtcMainData = AssetSampleTestDataJson.GetSampleData16();
					resetData();
				} else
					ElseUtils.network_error(getActivity());
					//CustomToast.getInstance(getActivity()).showShort("FragmentMain4 서버 통신 실패 : 매인");
			}
		});
	}
	/**  Refresh **/
	private void resetData(){
		if (mAlInfoDataList != null) {
			mAlInfoDataList.removeAll(mAlInfoDataList);

			if (mEtcMainData == null ||
					!mEtcMainData.isComplete()) {
				CustomToast.makeText(getContext(), "데이터에 이상이 있습니다. 잠시 후 재시도 해주세요.", Toast.LENGTH_SHORT).show();
				return;
			}

			mAlInfoDataList.add(new InfoDataMain4("챌린지", ((mEtcMainData.mMainEtcChallengeInfo != null && mEtcMainData.mMainEtcChallengeInfo.size() != 0) ? "y" : "n"),
//			mAlInfoDataList.add(new InfoDataMain4("챌린지", ((mEtcMainData.mMainEtcChallengeInfo != null) ? "y" : "n"),
					"떠나자~ 유럽으로!", "1,950,000", "떠나자~ 미국으로!", "2,000,000", "떠나자~ 한국으로!", "3,000,000",
					"", "", "", "", "", "",
					"", "", "", "", "", "",
					"", "", "", "", "", "",
					"", "", "", "", "", "", mEtcMainData));
			mAlInfoDataList.add(new InfoDataMain4("머니 캘린더", ((mEtcMainData.mMainEtcMoneyInfo != null && mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.size() != 0) ? "y" : "n"),
//			mAlInfoDataList.add(new InfoDataMain4("머니 캘린더", ((mEtcMainData.mMainEtcMoneyInfo != null) ? "y" : "n"),
					"", "", "", "", "", "",
					"오늘", "월세", "자동납부", "매월31일", "550,000", "확정",
					"내일", "G마켓", "게좌이체", "내일", "20,000", "확정",
					"08", "삼성카드 결제", "자동납부", "매월31일", "1,560,000", "예상",
					"11", "하나카드 결제", "자동납부", "매월31일", "760,000", "예상", mEtcMainData));

			mAdapter.notifyDataSetChanged();
		}

	}

	@Override
	public void reset(boolean flag) {
		if (mListView != null) {
			mListView.setSelection(0);
		}
	}

	@Override
	public boolean canScrollUp() {
		boolean ret = false;
		if (mListView != null) {
			ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
		}
		return ret;
	}

	@Override
	public boolean canScrollDown() {
		return false;
	}

//===============================================================================//
// Handler
//===============================================================================//	
//	private Handler mGetDataTaskHandler = new Handler(){
//
//		@Override
//		public void dispatchMessage(Message msg) {
//			switch (msg.what) {
//			case APP.PTR_GETDATATASK_HANDLER_SUCCESS:
//				mInterFaceRefreshCallBack.onRefreshFinish();
//				mSwipeRefresh.setRefreshing(false);
//				break;
//
//			case APP.PTR_GETDATATASK_HANDLER_FAIL:
//				mInterFaceRefreshCallBack.onRefreshFinish();
//				mSwipeRefresh.setRefreshing(false);
//				break;
//			}
//
//		}
//	};




}
