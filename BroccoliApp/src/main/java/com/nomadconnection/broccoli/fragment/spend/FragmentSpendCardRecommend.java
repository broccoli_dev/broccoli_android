package com.nomadconnection.broccoli.fragment.spend;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.spend.Level3ActivitySpendCardRecommendDetail;
import com.nomadconnection.broccoli.activity.spend.Level3ActivitySpendCardRecommendOption;
import com.nomadconnection.broccoli.adapter.CommonAdapter;
import com.nomadconnection.broccoli.adapter.CommonPagerAdapter;
import com.nomadconnection.broccoli.api.LogApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.PageIndicatorGuide;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.SpendCardOption;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendCardRecommendData;
import com.nomadconnection.broccoli.data.Spend.SpendCardRecommendInternalOption;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.nomadconnection.broccoli.constant.SpendCardOption.BenefitType.DISCOUNT_POINT;

/**
 * Created by YelloHyunminJang on 2017. 3. 20..
 */

public class FragmentSpendCardRecommend extends BaseFragment {

    public static final String SHOW_SPECIFIC_CARD = "SHOW_SPECIFIC_CARD";
    public static final int REQUEST_CODE_OPTION = 1;

    private TextView mDiscountBtn;
    private TextView mMileageBtn;
    private TextView mOptionBtn;
    private PageIndicatorGuide mIndicator;
    private ViewPager mViewPager;
    private ListView mListView;
    private View mEmptyView;
    private CommonPagerAdapter mSwipeAdapter;
    private CommonAdapter mAdapter;
    private LayoutInflater mInflater;
    private InterfaceEditOrDetail mListener;

    private long mCardId = 0;
    private int mSelectMyCardIndex = 0;
    private SpendCardRecommendInternalOption mOption;
    private ArrayList<SpendCardRecommendData> mClassifiedCardList;
    private ArrayList<SpendCardRecommendData> mRecommendCardList;

    private ForegroundColorSpan colorTextSpan;
    private AbsoluteSizeSpan sizeSpan;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spend_card_recommend, null);
        init(view, savedInstanceState);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(Const.DATA, mOption);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (InterfaceEditOrDetail) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement InterfaceEditOrDetail");
        }
    }

    private void init(View view, Bundle save) {
        ((BaseActivity)getActivity()).sendTracker(AnalyticsName.SpendCardRecmdList);
        initWidget(view);
        initData(save);
    }

    private void initWidget(View view) {
        mInflater = LayoutInflater.from(getActivity());
        colorTextSpan = new ForegroundColorSpan(getResources().getColor(R.color.spend_card_point_font_color));
        sizeSpan = new AbsoluteSizeSpan((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 17, getResources().getDisplayMetrics()));
        mIndicator = (PageIndicatorGuide) view.findViewById(R.id.spend_card_page_indicator);
        mViewPager = (ViewPager) view.findViewById(R.id.vp_spend_card_my_card);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mSelectMyCardIndex = position;
                mOption.cardId = mClassifiedCardList.get(mSelectMyCardIndex).cardId;
                mIndicator.check(position);
                showList(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mListView = (ListView) view.findViewById(R.id.lv_spend_card_recommend);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int fixPosition = position;
                if (mListView.getHeaderViewsCount() > 0) {
                    fixPosition = position - mListView.getHeaderViewsCount();
                }
                SpendCardRecommendData data = mRecommendCardList.get(fixPosition);
                sendLog(data.card.cardCode);
                Intent intent = new Intent(getActivity(), Level3ActivitySpendCardRecommendDetail.class);
                intent.putExtra(Const.DATA, data);
                startActivity(intent);
            }
        });
        if (getActivity() != null && !getActivity().isFinishing()) {
            View header = getActivity().getLayoutInflater().inflate(R.layout.row_list_item_spend_card_recommend_header, null);
            mListView.addHeaderView(header, null, false);
        }
        mDiscountBtn = (TextView) view.findViewById(R.id.tv_spend_card_discount_point);
        mMileageBtn = (TextView) view.findViewById(R.id.tv_spend_card_mileage);
        mOptionBtn = (TextView) view.findViewById(R.id.tv_spend_card_option);
        mEmptyView = view.findViewById(R.id.ll_empty_layout_layout);
        mDiscountBtn.setOnClickListener(mOptionClickListener);
        mMileageBtn.setOnClickListener(mOptionClickListener);
        mOptionBtn.setOnClickListener(mOptionClickListener);
        mSwipeAdapter = new CommonPagerAdapter(pagerItemCreateListener);
        mViewPager.setAdapter(mSwipeAdapter);
        mAdapter = new CommonAdapter(itemCreateListener);
        mListView.setAdapter(mAdapter);
        mAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
            }
        });
    }

    private void initData(Bundle save) {
        Bundle bundle = getArguments();
        mCardId = bundle.getLong(SHOW_SPECIFIC_CARD, 0);
        mClassifiedCardList = (ArrayList<SpendCardRecommendData>) bundle.getSerializable(Const.DATA);
        if (mClassifiedCardList != null) {
            Collections.sort(mClassifiedCardList, new Comparator<SpendCardRecommendData>() {
                @Override
                public int compare(SpendCardRecommendData lhs, SpendCardRecommendData rhs) {
                    int value = 0;
                    if (lhs.averageExpense > rhs.averageExpense) {
                        value = -1;
                    } else if (lhs.averageExpense < rhs.averageExpense) {
                        value = 1;
                    }
                    return value;
                }
            });
        }
        long expense = 0;
        if (mClassifiedCardList != null) {
            for (int i = 0; i < mClassifiedCardList.size(); i++) {
                SpendCardRecommendData data = mClassifiedCardList.get(i);
                if (mCardId == 0) {
                    if (expense < mClassifiedCardList.get(i).averageExpense) {
                        expense = mClassifiedCardList.get(i).averageExpense;
                        mSelectMyCardIndex = i;
                    }
                } else if (data.cardId == mCardId) {
                    mSelectMyCardIndex = i;
                    break;
                }
            }
        }

        if (mClassifiedCardList != null && mClassifiedCardList.size() > 0) {
            initOption();
            mIndicator.setPageTotalCount(mClassifiedCardList.size(), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, getResources().getDisplayMetrics()));
            mSwipeAdapter.setData(mClassifiedCardList);
            mSwipeAdapter.notifyDataSetChanged();
            mIndicator.check(mSelectMyCardIndex);
            mViewPager.setCurrentItem(mSelectMyCardIndex, false);
            mListView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
            if (save != null) {
                SpendCardRecommendInternalOption option = (SpendCardRecommendInternalOption) save.getSerializable(Const.DATA);
                if (option != null) {
                    mOption.optionDetailType = option.optionDetailType;
                    mOption.annulFee = option.annulFee;
                    mOption.benefitType = option.benefitType;
                    mOption.cardType = option.cardType;
                }
            }

            switch (mOption.benefitType) {
                case DISCOUNT_POINT:
                case DISCOUNT:
                case POINT:
                    mDiscountBtn.setSelected(true);
                    mMileageBtn.setSelected(false);
                    break;
                case MILEAGE:
                    mDiscountBtn.setSelected(false);
                    mMileageBtn.setSelected(true);
                    break;
            }
        } else {
            mListView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }

        getRecommendData();
    }

    private void initOption() {
        mOption = new SpendCardRecommendInternalOption();
        mOption.cardId = mClassifiedCardList.get(mSelectMyCardIndex).cardId;
        mOption.cardType = SpendCardOption.CardType.ALL;
        mOption.optionDetailType = DISCOUNT_POINT;
        mOption.benefitType = DISCOUNT_POINT;
        mOption.annulFee = SpendCardOption.AnnualFeeRange.FEE_TOTAL;
    }

    private void showList(int index) {
        if (mClassifiedCardList != null && mClassifiedCardList.size() > 0) {
            mViewPager.setCurrentItem(index, false);
            mListView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);

            switch (mOption.benefitType) {
                case DISCOUNT_POINT:
                case DISCOUNT:
                case POINT:
                    mDiscountBtn.setSelected(true);
                    mMileageBtn.setSelected(false);
                    break;
                case MILEAGE:
                    mDiscountBtn.setSelected(false);
                    mMileageBtn.setSelected(true);
                    break;
            }
        } else {
            mListView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }

        getRecommendData();
    }

    private void sendLog(String cardCode) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("cardCode", cardCode);
        Call<Result> call = ServiceGenerator.createService(LogApi.class).sendLogRecommendListClick(map);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    private void getRecommendData() {
        if (mOption == null || (mClassifiedCardList ==  null || mClassifiedCardList.size() == 0)) {
            return;
        }
        DialogUtils.showLoading(getActivity());
        int benefit = DISCOUNT_POINT.ordinal();
        switch (mOption.benefitType) {
            case DISCOUNT_POINT:
            case DISCOUNT:
            case POINT:
                benefit = mOption.optionDetailType.ordinal();
                break;
            case MILEAGE:
                benefit = mOption.benefitType.ordinal();
                break;
        }
        Call<JsonElement> call = ServiceGenerator.createService(SpendApi.class).getRecommendList(
                mOption.cardId, mOption.cardType.ordinal(), benefit, mOption.annulFee.ordinal());
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                if (element != null) {
                    JsonObject object = element.getAsJsonObject();
                    JsonArray array = object.getAsJsonArray("recommendList");
                    if (array != null) {
                        Type listType = new TypeToken<ArrayList<SpendCardRecommendData>>() {}.getType();
                        mRecommendCardList = new Gson().fromJson(array, listType);
                        setData(mRecommendCardList);
                    } else {
                        ErrorUtils.parseError(response);
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void setData(List<SpendCardRecommendData> list) {
        if (list == null || list.size() == 0) {
            mListView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mListView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        }
        mAdapter.setData(list);
        mAdapter.notifyDataSetChanged();
    }

    private String[] cardArray;
    private String[] cardCode;

    private String createCardTitle(String code) {
        if (cardArray == null) {
            cardArray = getResources().getStringArray(R.array.card_name);
        }
        if (cardCode == null) {
            cardCode = getResources().getStringArray(R.array.card_code);
        }
        String title = "";
        int index = Arrays.asList(cardCode).indexOf(code);
        if (index >= 0) {
            title = cardArray[index];
        } else {
            title = "";
        }
        return title;
    }

    private CommonPagerAdapter.PagerItemCreateListener pagerItemCreateListener = new CommonPagerAdapter.PagerItemCreateListener() {
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LinearLayout layout = (LinearLayout) View.inflate(getActivity().getApplicationContext(), R.layout.row_list_item_spend_card_recommend_compare, null);
            TextView counting = (TextView) layout.findViewById(R.id.tv_spend_card_counting);
            TextView title = (TextView) layout.findViewById(R.id.tv_spend_card_title);
            TextView benefitTitle = (TextView) layout.findViewById(R.id.tv_row_spend_card_total_benefit_title);
            TextView benefit = (TextView) layout.findViewById(R.id.tv_spend_card_benefit);
            TextView unit = (TextView) layout.findViewById(R.id.tv_spend_card_unit);
            TextView warning = (TextView) layout.findViewById(R.id.tv_spend_card_warning);

            SpendCardRecommendData recommendData = null;
            if (mClassifiedCardList.size() >= position) {
                counting.setText(String.valueOf(position+1)+"/"+String.valueOf(mClassifiedCardList.size()));
                recommendData = mClassifiedCardList.get(position);
                title.setText(recommendData.card.cardName.trim());
                if (recommendData.card.mileageYn > 0) {
                    benefit.setText(ElseUtils.getDecimalFormat((recommendData.averageMileage)));
                    unit.setText(R.string.spend_card_recommend_unit_mileage);
                    benefitTitle.setText(R.string.spend_card_recommend_row_monthly_average_for_mileage);
                    warning.setVisibility(View.GONE);
                } else {
                    long currentBenefit = 0;
                    switch (mOption.optionDetailType) {
                        case DISCOUNT_POINT:
                            currentBenefit = recommendData.averageTotalSum;
                            break;
                        case DISCOUNT:
                            currentBenefit = recommendData.averageTotalDiscount;
                            break;
                        case POINT:
                            currentBenefit = recommendData.averageTotalPoint;
                            break;
                        case MILEAGE:
                            break;
                    }
                    if (currentBenefit < 0) {
                        warning.setVisibility(View.VISIBLE);
                    } else {
                        warning.setVisibility(View.GONE);
                    }
                    benefit.setText(ElseUtils.getDecimalFormat(currentBenefit));
                    benefitTitle.setText(R.string.spend_card_recommend_row_monthly_average);
                    unit.setText(R.string.spend_card_recommend_unit_won);
                }
            }

            ((ViewPager)container).addView(layout, 0);

            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((LinearLayout) object);
        }
    };

    private CommonAdapter.AdapterItemCreateListener itemCreateListener = new CommonAdapter.AdapterItemCreateListener() {

        @Override
        public View getView(List<?> list, int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_list_item_spend_card_recommend, null);
                holder = new ViewHolder();
                holder.mCompanyTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_card_company_name);
                holder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_card_title);
                holder.mBest = (TextView) convertView.findViewById(R.id.tv_row_spend_card_best);
                holder.mType = (TextView) convertView.findViewById(R.id.tv_row_spend_card_type);
                holder.mBenefitTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_card_total_benefit_title);
                holder.mBenefit = (TextView) convertView.findViewById(R.id.tv_row_spend_card_total_benefit);
                holder.mBenefitUnit = (TextView) convertView.findViewById(R.id.tv_row_spend_card_total_unit);
                holder.mSpace = convertView.findViewById(R.id.ll_row_spend_card_additional_space);
                holder.mAdditionalBenefitLayout = convertView.findViewById(R.id.ll_row_spend_card_additional_benefit);
                holder.mAdditionalBenefit = (TextView) convertView.findViewById(R.id.tv_row_spend_card_additional_benefit);
                holder.mAdditionalBenefitUnit = (TextView) convertView.findViewById(R.id.tv_row_spend_card_additional_unit);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            SpendCardRecommendData recommendData = (SpendCardRecommendData) mAdapter.getItem(position);

//            if (position == 0) {
//                holder.mBest.setVisibility(View.VISIBLE);
//            } else {
//                holder.mBest.setVisibility(View.GONE);
//            }
            if (recommendData.cardCompany != null) {
                holder.mCompanyTitle.setText(createCardTitle(recommendData.cardCompany.brCmpnyCode));
            }
            if (recommendData.card != null) {
                holder.mTitle.setText(recommendData.card.cardName.trim());
                if (recommendData.card.cardType == 1) {
                    holder.mType.setText(R.string.spend_card_recommend_row_type_credit);
                    holder.mType.setTextColor(getResources().getColor(R.color.common_green_color));
                    holder.mType.setBackground(getResources().getDrawable(R.drawable.shape_tag_a_bg_1));
                } else {
                    holder.mType.setText(R.string.spend_card_recommend_row_type_check);
                    holder.mType.setTextColor(getResources().getColor(R.color.tag_a_2_txt_color));
                    holder.mType.setBackground(getResources().getDrawable(R.drawable.shape_tag_a_bg_2));
                }
            }
            if (recommendData.card.mileageYn > 0) {
                holder.mBenefitTitle.setText(R.string.spend_card_recommend_row_monthly_average_for_mileage);
                holder.mBenefit.setText(ElseUtils.getDecimalFormat(recommendData.averageMileage));
                holder.mBenefitUnit.setText(R.string.spend_card_recommend_unit_mileage);
                long addBenefit = recommendData.averageMileage - mClassifiedCardList.get(mSelectMyCardIndex).averageMileage;
                if (mClassifiedCardList.get(mSelectMyCardIndex).card.mileageYn > 0) {
                    holder.mSpace.setVisibility(View.VISIBLE);
                    holder.mAdditionalBenefitLayout.setVisibility(View.VISIBLE);
                    holder.mAdditionalBenefit.setText(ElseUtils.getDecimalFormat(addBenefit));
                    holder.mAdditionalBenefitUnit.setText(R.string.spend_card_recommend_unit_mileage);
                } else {
                    holder.mSpace.setVisibility(View.GONE);
                    holder.mAdditionalBenefitLayout.setVisibility(View.GONE);
                }
            } else {
                long benefit = 0;
                long currentBenefit = 0;
                switch (mOption.optionDetailType) {
                    case DISCOUNT_POINT:
                        benefit = recommendData.averageTotalSum;
                        currentBenefit = mClassifiedCardList.get(mSelectMyCardIndex).averageTotalSum;
                        break;
                    case DISCOUNT:
                        benefit = recommendData.averageTotalDiscount;
                        currentBenefit = mClassifiedCardList.get(mSelectMyCardIndex).averageTotalDiscount;
                        break;
                    case POINT:
                        benefit = recommendData.averageTotalPoint;
                        currentBenefit = mClassifiedCardList.get(mSelectMyCardIndex).averageTotalPoint;
                        break;
                    case MILEAGE:
                        break;
                }
                holder.mBenefitTitle.setText(R.string.spend_card_recommend_row_monthly_average);
                holder.mBenefit.setText(ElseUtils.getDecimalFormat(benefit));
                holder.mBenefitUnit.setText(R.string.spend_card_recommend_unit_won);
                long addBenefit = benefit - currentBenefit;
                if (mClassifiedCardList.get(mSelectMyCardIndex).card.mileageYn == 0) {
                    holder.mSpace.setVisibility(View.VISIBLE);
                    holder.mAdditionalBenefitLayout.setVisibility(View.VISIBLE);
                    holder.mAdditionalBenefit.setText(ElseUtils.getDecimalFormat(addBenefit));
                    holder.mAdditionalBenefitUnit.setText(R.string.spend_card_recommend_unit_won);
                } else {
                    holder.mSpace.setVisibility(View.GONE);
                    holder.mAdditionalBenefitLayout.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        class ViewHolder {
            private TextView mCompanyTitle;
            private TextView mTitle;
            private TextView mBest;
            private TextView mType;
            private TextView mBenefitTitle;
            private TextView mBenefit;
            private TextView mBenefitUnit;
            private View mSpace;
            private View mAdditionalBenefitLayout;
            private TextView mAdditionalBenefit;
            private TextView mAdditionalBenefitUnit;
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_OPTION) {
            if (resultCode == RESULT_OK) {
                mOption = (SpendCardRecommendInternalOption) data.getSerializableExtra(Const.DATA);
                getRecommendData();
                mSwipeAdapter.notifyDataSetChanged();
            }
        }
    }

    private View.OnClickListener mOptionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_spend_card_discount_point:
                    if (mOption != null) {
                        mDiscountBtn.setSelected(true);
                        mMileageBtn.setSelected(false);
                        mOption.benefitType = DISCOUNT_POINT;
                        getRecommendData();
                    }
                    break;
                case R.id.tv_spend_card_mileage:
                    if (mOption != null) {
                        mDiscountBtn.setSelected(false);
                        mMileageBtn.setSelected(true);
                        mOption.benefitType = SpendCardOption.BenefitType.MILEAGE;
                        getRecommendData();
                    }
                    break;
                case R.id.tv_spend_card_option:
                    if (mOption != null) {
                        Intent intent = new Intent(getActivity(), Level3ActivitySpendCardRecommendOption.class);
                        intent.putExtra(Const.DATA, mOption);
                        startActivityForResult(intent, REQUEST_CODE_OPTION);
                    }
                    break;
            }
        }
    };



    public boolean canScrollUp() {
        boolean ret = false;
        if (mListView != null) {
            ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
        }
        return ret;
    }

    public boolean canScrollDown() {
        boolean ret = false;
        if (mListView != null) {
            ret = ViewCompat.canScrollVertically(mListView, 1);
        }
        return ret;
    }
}
