package com.nomadconnection.broccoli.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.LockScreenActivity;
import com.nomadconnection.broccoli.activity.MainActivity;
import com.nomadconnection.broccoli.session.Session;

/**
 * Created by YelloHyunminJang on 2016. 12. 13..
 */

public class FragmentMembershipUpdateStep4 extends BaseFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_membership_update_step4, null);

        init(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init(View view) {
        View btn = view.findViewById(R.id.btn_confirm);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
    }

    private void next() {
        Intent mainIntent = new Intent(getActivity(), MainActivity.class);
        startActivity(mainIntent);
        Session.getInstance(getActivity()).setLock(true);
        Intent intent = new Intent(getActivity(),
                LockScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
        getActivity().finish();
    }
}
