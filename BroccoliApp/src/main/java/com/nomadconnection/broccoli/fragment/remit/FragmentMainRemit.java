package com.nomadconnection.broccoli.fragment.remit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.remit.RemitAccountEditActivity;
import com.nomadconnection.broccoli.activity.remit.RemitChargeActivity;
import com.nomadconnection.broccoli.activity.remit.RemitHistoryActivity;
import com.nomadconnection.broccoli.activity.remit.RemitPossibleBankActivity;
import com.nomadconnection.broccoli.activity.remit.RemitSendActivity;
import com.nomadconnection.broccoli.adapter.remit.AdtListViewMainRemit;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.RemitResult;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccount;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitStandByReceivedMoney;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMainRemit extends BaseFragment implements InterfaceFragmentInteraction, View.OnClickListener{

	private static String TAG = FragmentMainRemit.class.getSimpleName();
	
	/* Layout */
	private FrameLayout mServiceInfomation;
	private FrameLayout mServiceStart;
	private LinearLayout mFlexibleArea;
	public ListView mListView;
	private AdtListViewMainRemit mAdapter;
	private ArrayList<ResponseRemitAccount> mAlInfoDataList;

	/*Data Class*/
	private ResponseRemitAccount mRemitMainData;
	
//	private SwipeRefreshLayout mSwipeRefresh;
//	InterFaceRefreshCallBack mInterFaceRefreshCallBack;
//	
//	@Override
//	public void onAttach(Activity activity) {
//		super.onAttach(activity);
//		
//		// This makes sure that the container activity has implemented
//		// the callback interface. If not, it throws an exception
//		try {
//			mInterFaceRefreshCallBack = (InterFaceRefreshCallBack) activity;
//		} catch (ClassCastException e) {
//			throw new ClassCastException(activity.toString()
//					+ " must implement InterFaceRefreshCallBack");
//		}
//	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_main_remit, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			check_account_use();
		}
	}

	@Override
	public void onResume() {
		check_account_use();
		super.onResume();
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.fl_remit_service_register_start:
				Intent Register = new Intent(getActivity(), RemitPossibleBankActivity.class);
				startActivityForResult(Register, 0);
				break;
		}

	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
//			if (getIntent().getExtras() != null) {
//			}
			mAlInfoDataList = new ArrayList<ResponseRemitAccount>();

			check_account_use();
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Layout */
			mServiceInfomation = (FrameLayout)_v.findViewById(R.id.fl_remit_service_register);
			mServiceStart = (FrameLayout)_v.findViewById(R.id.fl_remit_service_register_start);
			mServiceStart.setOnClickListener(this);


    		mListView = (ListView)_v.findViewById(R.id.lv_fragment_main_remit_list);
    		mAdapter = new AdtListViewMainRemit(getActivity(), mAlInfoDataList, mRemitAccountAdd);

			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mFlexibleArea = (LinearLayout) inflater.inflate(R.layout.flexible_area_main_4, null);
			mListView.addHeaderView(mFlexibleArea);

    		mListView.setAdapter(mAdapter);
    		mListView.setOnItemClickListener(ItemClickListener);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				mListView.setNestedScrollingEnabled(true);
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
//			check_account_use();
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//	
	/**  Item Click Listener **/
	private OnItemClickListener ItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (position != 0 && mAlInfoDataList.get(position-1) == null) {
				return;
			}
			switch (position) {
				case 0:
					break;
				case 1:
					Intent remit_edit = new Intent(getActivity(), RemitAccountEditActivity.class);
					remit_edit.putExtra(Const.DATA, mRemitMainData);
					startActivity(remit_edit);
					break;
				case 2:
					Intent remit_charge = new Intent(getActivity(), RemitChargeActivity.class);
					remit_charge.putExtra(Const.REMIT_MAIN_DATA, mRemitMainData);
					startActivity(remit_charge);
					break;
				case 3:
					Intent remit_go = new Intent(getActivity(), RemitSendActivity.class);
					remit_go.putExtra(Const.REMIT_MAIN_DATA, mRemitMainData);
					startActivity(remit_go);
					break;
				case 4:
					Intent remit_history = new Intent(getActivity(), RemitHistoryActivity.class);
					startActivity(remit_history);
					break;
			}
		}
	};


//===============================================================================//
// Check Account Use
//===============================================================================//
	private void check_account_use(){
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		Call<RemitResult> mCall = ServiceGenerator.createRemitService(RemitApi.class).getChkAccountUse(mRequestDataByUserId);
		mCall.enqueue(new Callback<RemitResult>() {
			@Override
			public void onResponse(Call<RemitResult> call, Response<RemitResult> response) {
				DialogUtils.dismissLoading();
				if(response.body().getResult() != null && Const.OK.equalsIgnoreCase(response.body().getResult())){
					mServiceInfomation.setVisibility(View.GONE);
					mListView.setVisibility(View.VISIBLE);
//					refreshData();
					resetData();
				}
				else {
					mServiceInfomation.setVisibility(View.VISIBLE);
					mListView.setVisibility(View.GONE);
					resetData();
				}
			}

			@Override
			public void onFailure(Call<RemitResult> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});

	}



//===============================================================================//
// Refresh
//===============================================================================//
	/**  Refresh **/
	private void refreshData(){
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		Call<ResponseRemitAccount> mCall = ServiceGenerator.createRemitService(RemitApi.class).getRemitMain(mRequestDataByUserId);
		mCall.enqueue(new Callback<ResponseRemitAccount>() {
			@Override
			public void onResponse(Call<ResponseRemitAccount> call, Response<ResponseRemitAccount> response) {
				DialogUtils.dismissLoading();
				if(response.body().getResult() != null && response.body().getResult().equalsIgnoreCase(Const.OK)){
					mRemitMainData = response.body();
					mRemitMainData.setRemit(getString(R.string.remit_service));
					mRemitMainData.setRemitHistory(getString(R.string.remit_history));

					if(mRemitMainData != null){
						mServiceInfomation.setVisibility(View.GONE);
						mListView.setVisibility(View.VISIBLE);
						if(mAlInfoDataList != null){
							mAlInfoDataList.removeAll(mAlInfoDataList);
						}
						mAlInfoDataList.add(mRemitMainData);
						mAlInfoDataList.add(mRemitMainData);
						mAlInfoDataList.add(mRemitMainData);
						mAlInfoDataList.add(mRemitMainData);
						mAdapter.setData(mAlInfoDataList);
						mAdapter.notifyDataSetChanged();
					}
					else {
						mServiceInfomation.setVisibility(View.VISIBLE);
						mListView.setVisibility(View.GONE);
					}

					userRcvWaitMoneylistForApp();
				}
				else {
					mServiceInfomation.setVisibility(View.VISIBLE);
					mListView.setVisibility(View.GONE);
				}

			}

			@Override
			public void onFailure(Call<ResponseRemitAccount> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());

				mServiceInfomation.setVisibility(View.VISIBLE);
				mListView.setVisibility(View.GONE);
			}
		});
	}


//===============================================================================//
// Check Receive Wait Money List
//===============================================================================//
	private void userRcvWaitMoneylistForApp(){
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		Call<ResponseRemitStandByReceivedMoney> mCall = ServiceGenerator.createRemitService(RemitApi.class).checkReceivedMoney(mRequestDataByUserId);
		mCall.enqueue(new Callback<ResponseRemitStandByReceivedMoney>() {
			@Override
			public void onResponse(Call<ResponseRemitStandByReceivedMoney> call, Response<ResponseRemitStandByReceivedMoney> response) {
				DialogUtils.dismissLoading();
				if(response.body().getResult() != null && response.body().getResult().equalsIgnoreCase(Const.OK)){
					if(response.body().getRcvMoneyWaitList() != null && response.body().getRcvMoneyWaitList().size() != 0){
						DialogUtils.showReceivedMoneyDialog(getActivity(), 200, getActivity().toString());
					}
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitStandByReceivedMoney> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private View.OnClickListener mRemitAccountAdd = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v != null) {
				Intent remit_edit = new Intent(getActivity(), RemitAccountEditActivity.class);
				remit_edit.putExtra(Const.DATA, mRemitMainData);
				startActivity(remit_edit);
			}
		}
	};

	/**  Refresh **/
	private void resetData(){
		mRemitMainData = new ResponseRemitAccount();
		ArrayList<RemitAccount> arrayList = new ArrayList<RemitAccount>();
		RemitAccount mRemitAccount = new RemitAccount();
		mRemitAccount.setAccountId("1");
		mRemitAccount.setCompanyCode("0004");
		mRemitAccount.setAccountName("계좌이름");
		mRemitAccount.setBalance("10000000");
		mRemitAccount.setAccountNumber("000000000000");
		mRemitAccount.setStandardDate("201607132102");
		arrayList.add(mRemitAccount);

		mRemitMainData.setAccountInfo(arrayList);
		mRemitMainData.setAccountCount("1");
		mRemitMainData.setWalletMoney("20000000");
		mRemitMainData.setRemit(getString(R.string.remit_service));
		mRemitMainData.setRemitHistory(getString(R.string.remit_history));

		if(mRemitMainData != null){
			mServiceInfomation.setVisibility(View.GONE);
			mListView.setVisibility(View.VISIBLE);
			mAlInfoDataList.add(mRemitMainData);
			mAlInfoDataList.add(mRemitMainData);
			mAlInfoDataList.add(mRemitMainData);
			mAlInfoDataList.add(mRemitMainData);
			mAdapter.setData(mAlInfoDataList);
			mAdapter.notifyDataSetChanged();
		}
		else {
			mServiceInfomation.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.GONE);
		}
//		if (mAlInfoDataList != null) {
//			mAlInfoDataList.removeAll(mAlInfoDataList);
//
//			mAlInfoDataList.add(new InfoDataMain4("챌린지", ((mEtcMainData.mMainEtcChallengeInfo != null && mEtcMainData.mMainEtcChallengeInfo.size() != 0) ? "y" : "n"),
////			mAlInfoDataList.add(new InfoDataMain4("챌린지", ((mEtcMainData.mMainEtcChallengeInfo != null) ? "y" : "n"),
//					"떠나자~ 유럽으로!", "1,950,000", "떠나자~ 미국으로!", "2,000,000", "떠나자~ 한국으로!", "3,000,000",
//					"", "", "", "", "", "",
//					"", "", "", "", "", "",
//					"", "", "", "", "", "",
//					"", "", "", "", "", "", mEtcMainData));
//			mAlInfoDataList.add(new InfoDataMain4("머니 캘린더", ((mEtcMainData.mMainEtcMoneyInfo != null && mEtcMainData.mMainEtcMoneyInfo.mEtcMoneyList.size() != 0) ? "y" : "n"),
////			mAlInfoDataList.add(new InfoDataMain4("머니 캘린더", ((mEtcMainData.mMainEtcMoneyInfo != null) ? "y" : "n"),
//					"", "", "", "", "", "",
//					"오늘", "월세", "자동납부", "매월31일", "550,000", "확정",
//					"내일", "G마켓", "게좌이체", "내일", "20,000", "확정",
//					"08", "삼성카드 결제", "자동납부", "매월31일", "1,560,000", "예상",
//					"11", "하나카드 결제", "자동납부", "매월31일", "760,000", "예상", mEtcMainData));
//
//			mAdapter.notifyDataSetChanged();
//		}

	}

	@Override
	public void reset(boolean flag) {

	}

	@Override
	public boolean canScrollUp() {
		boolean ret = false;
		if (mListView != null) {
			ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
		}
		return ret;
	}

	@Override
	public boolean canScrollDown() {
		return false;
	}


//===============================================================================//
// Handler
//===============================================================================//	
//	private Handler mGetDataTaskHandler = new Handler(){
//
//		@Override
//		public void dispatchMessage(Message msg) {
//			switch (msg.what) {
//			case APP.PTR_GETDATATASK_HANDLER_SUCCESS:
//				mInterFaceRefreshCallBack.onRefreshFinish();
//				mSwipeRefresh.setRefreshing(false);
//				break;
//
//			case APP.PTR_GETDATATASK_HANDLER_FAIL:
//				mInterFaceRefreshCallBack.onRefreshFinish();
//				mSwipeRefresh.setRefreshing(false);
//				break;
//			}
//
//		}
//	};




}
