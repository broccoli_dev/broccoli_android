package com.nomadconnection.broccoli.fragment.remit;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.remit.AdtListViewRemitAccountEdit;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.RemitResult;
import com.nomadconnection.broccoli.data.Remit.RequestDataAccountDel;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccount;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentRemitAccountEdit extends BaseFragment implements AdapterView.OnItemClickListener{

	/* Layout */
	private Bundle mArgument;
	private LinearLayout mEmpty;
	private ImageView mEmptyIcon;
	private TextView mEmptyTitle;

	private ListView mListView;
	private String mTargetMonth;
	private AdtListViewRemitAccountEdit mAdapter;
	private ArrayList<RemitAccount> mAlInfoDataList;


	public FragmentRemitAccountEdit() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//View v = inflater.inflate(R.layout.fragment_asset_credit_detail_1_a, null);
		View v = inflater.inflate(R.layout.fragment_remit_account_edit, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		requestDataSet();
	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (mAlInfoDataList.get(position) == null) {	// test 임시
			return;
		}

//		Intent intent = new Intent(getActivity(), Level3ActivitySpendHistoryDetail.class);
//		intent.putExtra(Const.SPEND_HISTORY_DETAIL, mAlInfoDataList.get(position));
//		intent.putExtra(APP.INFO_DATA_ASSET_CREDIT_PARCEL, mAlInfoDataList.get(position));	// test 임시
//		startActivity(intent);
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			mArgument = getArguments();
//			if (mArgument != null) {
//				ResponseRemitAccount responseRemitAccount = (ResponseRemitAccount)mArgument.getSerializable(Const.DATA);
//				if(responseRemitAccount.getAccountInfo().size() != 0) {
//					mAlInfoDataList = responseRemitAccount.getAccountInfo();
//				}
//			}
			/* getExtra */
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			mEmpty = (LinearLayout)_v.findViewById(R.id.ll_remit_empty_layout);
			mEmptyIcon = (ImageView)_v.findViewById(R.id.iv_empty_layout_icon);
			mEmptyTitle = (TextView)_v.findViewById(R.id.tv_remit_empty_layout_txt);

			mListView = (ListView)_v.findViewById(R.id.lv_fragment_remit_account_edit_list);
    		mAdapter = new AdtListViewRemitAccountEdit(getActivity(), mAlInfoDataList, mRemitAccountDel);
    		mListView.setAdapter(mAdapter);
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			mEmpty.setVisibility(View.GONE);
			/* Default Value */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}
	
	
	
//=========================================================================================//
// Refresh Adapter
//=========================================================================================//

	public void requestDataSet() {
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ResponseRemitAccount> call = api.getRemitMain(mRequestDataByUserId);
		call.enqueue(new Callback<ResponseRemitAccount>() {
			@Override
			public void onResponse(Call<ResponseRemitAccount> call, Response<ResponseRemitAccount> response) {
				DialogUtils.dismissLoading();
				if(response.body().getResult() != null && response.body().getResult().equalsIgnoreCase(Const.OK)){
					mAlInfoDataList = new ArrayList<RemitAccount>();
					mAlInfoDataList = (ArrayList<RemitAccount>)response.body().getAccountInfo();
					if(mAlInfoDataList != null && mAlInfoDataList.size() != 0){
						mEmpty.setVisibility(View.GONE);
						mListView.setVisibility(View.VISIBLE);
						mAdapter.setData(mAlInfoDataList);
						mAdapter.notifyDataSetChanged();
					}
					else{
						mEmpty.setVisibility(View.VISIBLE);
						mListView.setVisibility(View.GONE);
					}
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitAccount> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	public void setRemitAccountDel(String delAccountId) {
		DialogUtils.showLoading(getActivity());
		RequestDataAccountDel mRequestDataAccountDel = new RequestDataAccountDel();
		mRequestDataAccountDel.setUserId(Session.getInstance(getActivity()).getUserId());
		mRequestDataAccountDel.setAccountId(delAccountId);

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<RemitResult> call = api.delUserAccount(mRequestDataAccountDel);
		call.enqueue(new Callback<RemitResult>() {
			@Override
			public void onResponse(Call<RemitResult> call, Response<RemitResult> response) {
				DialogUtils.dismissLoading();
				if("OK".equals(response.body().getResult())){
					requestDataSet();
				}
				else {
					Toast.makeText(getActivity(), response.body().getResult(), Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(Call<RemitResult> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	public void refreshAdt(String _sort){
		mAdapter.notifyDataSetChanged();
	}

	private View.OnClickListener mRemitAccountDel = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v != null) {
				final RemitAccount data = (RemitAccount) v.getTag();
				final String mStr = getString(R.string.common_do_register_cancel);
				DialogUtils.showDlgBaseTwoButton(getActivity(), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
									setRemitAccountDel(data.getAccountId());
								}
							}
						},
						new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_cancel), getString(R.string.common_confirm), mStr));
			}
		}
	};
}
