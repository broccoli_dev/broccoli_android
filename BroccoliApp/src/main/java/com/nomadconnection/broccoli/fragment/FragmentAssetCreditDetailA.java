package com.nomadconnection.broccoli.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetCreditDetail;
import com.nomadconnection.broccoli.adapter.AdtListViewAssetCreditA;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.BodyAssetCardDetail;
import com.nomadconnection.broccoli.data.Spend.AssetCardPaymentTypeEnum;

import java.util.ArrayList;

public class FragmentAssetCreditDetailA extends BaseFragment {

	private static String TAG = FragmentAssetCreditDetailA.class.getSimpleName();

	/* Layout */
	private FrameLayout mNoDataLayout;
	private ListView mListView;
	private AdtListViewAssetCreditA mAdapter;
//	private LinearLayout mHeaderView;
//	private TextView mHeaderCost;

	/* Data Class */
	private ArrayList<BodyAssetCardDetail> mAlInfoDataList = new ArrayList<>();

	private Level3ActivityAssetCreditDetail.SortType mSort = Level3ActivityAssetCreditDetail.SortType.TOTAL;

	public FragmentAssetCreditDetailA() {
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_asset_credit_detail_a, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }







//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
		} catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			mNoDataLayout = (FrameLayout)_v.findViewById(R.id.inc_fragment_asset_credit_detail_a_empty);
			mListView = (ListView)_v.findViewById(R.id.lv_fragment_asset_credit_detail_a_listview);
//			mHeaderView = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.row_expandable_listview_item_child_asset_header, null);
//			mHeaderCost = (TextView) mHeaderView.findViewById(R.id.tv_row_item_asset_card_cost);
//			mListView.addHeaderView(mHeaderView);
    		mAdapter = new AdtListViewAssetCreditA(getActivity(), mAlInfoDataList);
			mAdapter.setSort(mSort);
    		mListView.setAdapter(mAdapter);

			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			refreshData();
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}
	
	
	
//=========================================================================================//
// Refresh Adapter
//=========================================================================================//
	public void refreshAdt(){
		mAdapter.notifyDataSetChanged();
	}

	public void refreshData(){
		if (mAlInfoDataList == null || mAlInfoDataList.size() == 0) {
			if (mNoDataLayout != null){
				mNoDataLayout.setVisibility(View.VISIBLE);
				((ImageView)mNoDataLayout.findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
				((TextView)mNoDataLayout.findViewById(R.id.tv_empty_layout_txt)).setText("사용 내역이 없습니다.");
			}
		}
		else {
			int count = 0;
			int onceCount = 0;
			int splitCount = 0;
			int loanCount = 0;
			long onceValue = 0;
			long splitValue = 0;
			long loanValue = 0;
			long totalValue = 0;
			long value = 0;

			for (int i=0; i < mAlInfoDataList.size(); i++) {
				BodyAssetCardDetail detail = mAlInfoDataList.get(i);
				totalValue += detail.realAmount;
				if (AssetCardPaymentTypeEnum.SPLIT == detail.paymentType
					|| AssetCardPaymentTypeEnum.SPLIT_ETC == detail.paymentType) {
					splitCount++;
					splitValue += detail.realAmount;
				} else if (AssetCardPaymentTypeEnum.SHORT_LOAN == detail.paymentType
						|| AssetCardPaymentTypeEnum.LONG_LOAN == detail.paymentType) {
					loanCount++;
					loanValue += detail.realAmount;
				} else {
					onceCount++;
					onceValue += detail.realAmount;
				}
			}

			switch (mSort) {
				case TOTAL:
					count = mAlInfoDataList.size();
					value = totalValue;
					break;
				case ONCE:
					count = onceCount;
					value = onceValue;
					break;
				case SPLITTING:
					count = splitCount;
					value = splitValue;
					break;
				case LOAN:
					count = loanCount;
					value = loanValue;
					break;
			}
			if (count == 0) {
				mListView.setVisibility(View.GONE);
				mNoDataLayout.setVisibility(View.VISIBLE);
				((ImageView)mNoDataLayout.findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
				((TextView)mNoDataLayout.findViewById(R.id.tv_empty_layout_txt)).setText("사용 내역이 없습니다.");
			} else {
				if (mNoDataLayout != null) mNoDataLayout.setVisibility(View.GONE);
				if (mListView != null) mListView.setVisibility(View.VISIBLE);
//				switch (mSort) {
//					case TOTAL:
//						mHeaderCost.setText(TransFormatUtils.getDecimalFormat(totalValue));
//						break;
//					case ONCE:
//						mHeaderCost.setText(TransFormatUtils.getDecimalFormat(onceValue));
//						break;
//					case SPLITTING:
//						mHeaderCost.setText(TransFormatUtils.getDecimalFormat(splitValue));
//						break;
//					case LOAN:
//						mHeaderCost.setText(TransFormatUtils.getDecimalFormat(loanValue));
//						break;
//				}
			}
		}
	}


	public void setData(ArrayList<BodyAssetCardDetail> data) {
		this.mAlInfoDataList = data;
		if (mAdapter != null)
			mAdapter.setData(mAlInfoDataList);
	}

	public void setSort(Level3ActivityAssetCreditDetail.SortType sort) {
		mSort = sort;
		if (mAdapter != null) {
			mAdapter.setSort(mSort);
			mAdapter.notifyDataSetChanged();
		}
	}
}
