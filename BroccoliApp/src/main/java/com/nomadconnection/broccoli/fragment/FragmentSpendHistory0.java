package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.PagerAdapter;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Spend.RequestDataByDate;
import com.nomadconnection.broccoli.data.Spend.ResponseSpendDetail;
import com.nomadconnection.broccoli.data.Spend.SpendMainData;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.common.CustomPagerSlidingTabStripSide;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSpendHistory0 extends BaseFragment {

	private static String TAG = "FragmentSpendHistory0";

	private static final int MAX_MONTH = 12;
	private String totalSpend;
	private SpendMainData mSpendMainData;

	/* Layout */
	private TextView mTotalSpend;
	private TextView mTotalSpendCost;

	/* Pager */
	private PagerAdapter mPagerAdapter;
	private ViewPager mViewPager;
	private int mCurrentPageInx;
	private String mSelectMonth;

	/* Pager Title */
	private String[] mTempMonthSpaceBefore;
	private String[] mTempMonthSpaceAfter;
	private List<String> mPagerTitle;
	private List<String> targetMonth;

	/* Child Fragment */
	private ArrayList<Fragment> alFragment;

	//private ArrayList<SpendMonthlyInfo> mAlInfoDataList;
	InterfaceEditOrDetail mInterFaceEditOrDetail;

	public FragmentSpendHistory0() {
		// TODO Auto-generated constructor stub

		/* 넘어온 데이터 날자별 분기(1주일, 1개월, 3개월) */
		// 일단은 그냥 패스
	}
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_spend_history_0, null);

		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		get_spend_main_data();
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			Intent intent = new Intent(getActivity().getIntent());
			totalSpend = intent.getStringExtra(Const.TOTAL_SPEND_SUM);
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Layout 선언 */
			((LinearLayout)_v.findViewById(R.id.ll_contents_area_inc_type_1_layout)).setBackgroundResource(R.color.main_2_layout_bg);

			mTotalSpend = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_1_txt_1);
			mTotalSpendCost = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_1_txt_2);
			mTotalSpendCost.setTypeface(FontClass.setFont(getActivity()));

			mTotalSpend.setText(getString(R.string.spend_total_history));
			mTotalSpendCost.setText(ElseUtils.getStringDecimalFormat(totalSpend));


			/* PagerFragment Tab Items */
			DecimalFormat decimalFormat = new DecimalFormat("00");
			Calendar calendar = Calendar.getInstance();
			int month = calendar.get(Calendar.MONTH)+1;
			set_after_space();	//space month add
			mPagerTitle = new ArrayList<String>();
			mPagerTitle.add(decimalFormat.format(month));
			targetMonth = new ArrayList<String>();
			targetMonth.add(ElseUtils.getYearMonth(calendar.getTime()));
			for (int i=1; i < MAX_MONTH; i++) {
				calendar.add(Calendar.MONTH, -1);
				month = calendar.get(Calendar.MONTH)+1;
				mPagerTitle.add(decimalFormat.format(month));
				targetMonth.add(ElseUtils.getYearMonth(calendar.getTime()));
			}
			set_before_space(calendar);	//space month add
			Collections.reverse(mPagerTitle);
			Collections.reverse(targetMonth);
			//mPagerTitle = getResources().getStringArray(R.array.pager_fragment_tab_items_spend_month);

			alFragment = new ArrayList<Fragment>();
			for (String target : targetMonth) {
				Bundle bundle = new Bundle();
				bundle.putString(Const.DATA, target);
				FragmentSpendHistory0A fragment = new FragmentSpendHistory0A();
				fragment.setArguments(bundle);
				alFragment.add(fragment);
			}

			String[] title = new String[MAX_MONTH];
			title = mPagerTitle.toArray(title);

			/* Pager */
			mPagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager(), alFragment, title);
			mViewPager = (ViewPager) _v.findViewById(R.id.pager_fragment_spend_history_0_body);
			mViewPager.setAdapter(mPagerAdapter);
			mViewPager.setOffscreenPageLimit(alFragment.size());
			mViewPager.setCurrentItem(mPagerTitle.size() - 1);


			CustomPagerSlidingTabStripSide tabs = (CustomPagerSlidingTabStripSide) _v.findViewById(R.id.pager_fragment_spend_history_0_tabs);
			tabs.setSpace(APP.CUSTOM_PAGE_SLIDING_TAB_STRIP_SIDE_TYPE_ELSE, 2, mTempMonthSpaceBefore, mTempMonthSpaceAfter);
			tabs.setShouldExpand(true);
			tabs.setViewPager(mViewPager);
			tabs.setOnPageChangeListener(mPageChangeListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//===============================================================================//
// View Pager
//===============================================================================//

	/**  View Page Change Listener **/
	private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {

		@Override
		public void onPageSelected(int _position) {
			mSelectMonth = targetMonth.get(_position);
			get_spend_main_data();
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	};



//===============================================================================//
// Get Spend main check
//===============================================================================//
	private void get_spend_main_data(){
		DialogUtils.showLoading(getActivity());	//소비 로딩 추가
		long now = System.currentTimeMillis();
		Date date = new Date(now);
		SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyyMM");
		String mSelectDate = CurDateFormat.format(date);
		RequestDataByDate userDate = new RequestDataByDate();
		userDate.setUserId(Session.getInstance(getActivity()).getUserId());
		if(mSelectMonth != null){
			userDate.setDate(mSelectMonth);
		}
		else {
			userDate.setDate(mSelectDate);
		}

		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<ResponseSpendDetail> call = api.getSpendDetail(userDate);
		call.enqueue(new Callback<ResponseSpendDetail>() {
			@Override
			public void onResponse(Call<ResponseSpendDetail> call, Response<ResponseSpendDetail> response) {
				DialogUtils.dismissLoading();	//소비 로딩 추가
				if(response.body().getTotalSum() !=  null && !("null".equals(response.body().getTotalSum()))){
					mTotalSpendCost.setText(ElseUtils.getStringDecimalFormat(response.body().getTotalSum()));
				}
				else{
					mTotalSpendCost.setText("0");
				}
				mPagerAdapter.notifyDataSetChanged();
			}

			@Override
			public void onFailure(Call<ResponseSpendDetail> call, Throwable t) {
				DialogUtils.dismissLoading(); 	//소비 로딩 추가
				ElseUtils.network_error(getActivity());
			}
		});

	}

	private void set_after_space() {
		int monthly;
		Calendar calendar = Calendar.getInstance();

		mTempMonthSpaceAfter = new String[2];
		DecimalFormat decimalFormat = new DecimalFormat("00");

		for(int s = 0; s < mTempMonthSpaceAfter.length; s++){
			calendar.add(Calendar.MONTH, +1);
			monthly = calendar.get(Calendar.MONTH) + 1;
			mTempMonthSpaceAfter[s] = decimalFormat.format(monthly);
		}
	}

	private void set_before_space(Calendar calendar) {
		int monthly;

		mTempMonthSpaceBefore = new String[2];
		DecimalFormat decimalFormat = new DecimalFormat("00");

		for(int s = 1; s >= 0; s--){
			calendar.add(Calendar.MONTH, -1);
			monthly = calendar.get(Calendar.MONTH)+1;
			mTempMonthSpaceBefore[s] = decimalFormat.format(monthly);
		}
	}
}
