package com.nomadconnection.broccoli.fragment.remit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.BaseInputConnection;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.remit.RemitRegisterActivity;
import com.nomadconnection.broccoli.activity.remit.RemitRegisterPasswordActivity;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Remit.RemitBankDetail;
import com.nomadconnection.broccoli.data.Remit.RemitResult;
import com.nomadconnection.broccoli.data.Remit.RequestDataPW;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentRemitRegisterPassword extends BaseFragment implements OnClickListener {

	public static final String TAG = FragmentRemitRegisterPassword.class.getSimpleName();

	private static final String EDIT_TEXT = "mEditText";
	private static final int PASSWORD_MINIMUM_LENGTH = 4;


	private View mView;
	private LinearLayout mPassWordLayout;
	private ImageView mPassWordOne;
	private ImageView mPassWordTwo;
	private ImageView mPassWordThree;
	private ImageView mPassWordFourth;
	private ImageView mPassWordDel;
	private EditText mEditText;
	private String mPwd = "";
	private FrameLayout mBtnComplete;
	private BaseInputConnection mInputConnection;
	private Bundle mArgument;
	private RemitBankDetail mRemitBankDetail;

	public FragmentRemitRegisterPassword() {
		// TODO Auto-generated constructor stub
		setTagName(TAG);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_remit_register_password, null);
		init(mView);
		
		return mView;
	}
	
	void init(final View view) {
		mArgument = getArguments();
		mRemitBankDetail = (RemitBankDetail) mArgument.get(Const.DATA);
		mPassWordLayout = (LinearLayout) view.findViewById(R.id.ll_fragment_remit_register_pass_word);
		mPassWordLayout.setOnClickListener(this);
		mPassWordOne = (ImageView) view.findViewById(R.id.iv_remit_register_lock_num1);
		mPassWordTwo = (ImageView) view.findViewById(R.id.iv_remit_register_lock_num2);
		mPassWordThree = (ImageView) view.findViewById(R.id.iv_remit_register_lock_num3);
		mPassWordFourth = (ImageView) view.findViewById(R.id.iv_remit_register_lock_num4);
		mPassWordDel = (ImageView) view.findViewById(R.id.iv_remit_register_del);
		mPassWordDel.setOnClickListener(this);
		mPassWordDel.setVisibility(View.GONE);
		mEditText = (EditText) view.findViewById(R.id.et_remit_register_secret_text);
		mInputConnection = new BaseInputConnection(mEditText, true);
		mEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				int count = s.length();
				switch (count) {
					case 0:
						mPassWordOne.setVisibility(View.GONE);
						mPassWordTwo.setVisibility(View.GONE);
						mPassWordThree.setVisibility(View.GONE);
						mPassWordFourth.setVisibility(View.GONE);
						break;
					case 1:
						mPassWordOne.setVisibility(View.VISIBLE);
						mPassWordTwo.setVisibility(View.GONE);
						mPassWordThree.setVisibility(View.GONE);
						mPassWordFourth.setVisibility(View.GONE);
						break;
					case 2:
						mPassWordOne.setVisibility(View.VISIBLE);
						mPassWordTwo.setVisibility(View.VISIBLE);
						mPassWordThree.setVisibility(View.GONE);
						mPassWordFourth.setVisibility(View.GONE);
						break;
					case 3:
						mPassWordOne.setVisibility(View.VISIBLE);
						mPassWordTwo.setVisibility(View.VISIBLE);
						mPassWordThree.setVisibility(View.VISIBLE);
						mPassWordFourth.setVisibility(View.GONE);
						break;
					case 4:
						mPassWordOne.setVisibility(View.VISIBLE);
						mPassWordTwo.setVisibility(View.VISIBLE);
						mPassWordThree.setVisibility(View.VISIBLE);
						mPassWordFourth.setVisibility(View.VISIBLE);
						break;
				}
				if (count >= PASSWORD_MINIMUM_LENGTH) {
					mBtnComplete.setEnabled(true);
					mPassWordDel.setVisibility(View.VISIBLE);
				} else {
					mBtnComplete.setEnabled(false);
					mPassWordDel.setVisibility(View.GONE);
				}
			}
		});
		mBtnComplete = (FrameLayout) view.findViewById(R.id.fl_fragment_remit_register_complete);
		mBtnComplete.setOnClickListener(this);
		mBtnComplete.setEnabled(false);
	}
	
	@Override
	public void onStart() {
		super.onStart();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		((RemitRegisterActivity)getActivity()).setStepGuide(1);
		if(mPwd.length() == 0){
			mEditText.setText("");
		}
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		int id = view.getId();
		switch (id) {
			case R.id.fl_fragment_remit_register_complete:
				remitAccountsetPW();
				break;

			case R.id.ll_fragment_remit_register_pass_word:
				Intent intent = new Intent(getActivity(), RemitRegisterPasswordActivity.class);
				startActivityForResult(intent, 0);
				break;
			case R.id.iv_remit_register_del:
				mEditText.setText("");
				break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			if (data != null) {
				mPwd = data.getStringExtra(Const.REMIT_PASSWORD);
				mEditText.setText(mPwd);
			}
			mBtnComplete.setEnabled(true);
		}
	}

	private void confirm() {
		BaseFragment fragment = new FragmentRemitRegisterARS();
		Bundle bundle = new Bundle();
		bundle.putSerializable(Const.DATA, mRemitBankDetail);
		bundle.putBoolean(Const.REMIT_REGISTER, true);
		fragment.setArguments(bundle);
		nextStep(fragment);
	}

	private void nextStep(BaseFragment fragment) {
		try {
			setCurrentFragment(R.id.fragment_remit_area, fragment, fragment.getTagName());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void remitAccountsetPW() {
		DialogUtils.showLoading(getActivity());
		RequestDataPW mRequestDataPW = new RequestDataPW();
		mRequestDataPW.setUserId(Session.getInstance(getActivity()).getUserId());
		mRequestDataPW.setAccountPW(mEditText.getText().toString());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<RemitResult> call = api.setSendMoneyPW(mRequestDataPW);
		call.enqueue(new Callback<RemitResult>() {
			@Override
			public void onResponse(Call<RemitResult> call, Response<RemitResult> response) {
				DialogUtils.dismissLoading();
				if("OK".equals(response.body().getResult())){
					confirm();
				}
				else {
					Toast.makeText(getActivity(), response.body().getResult(), Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(Call<RemitResult> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}
}
