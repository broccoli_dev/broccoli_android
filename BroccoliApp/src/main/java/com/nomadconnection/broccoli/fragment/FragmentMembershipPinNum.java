package com.nomadconnection.broccoli.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.BaseInputConnection;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igaworks.adbrix.IgawAdbrix;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.membership.MembershipActivity;
import com.nomadconnection.broccoli.activity.membership.MembershipCompleteActivity;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Dayli.DayliJoinData;
import com.nomadconnection.broccoli.data.Dayli.ResponseJoin;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.data.Session.MembershipData;
import com.nomadconnection.broccoli.data.Session.ResponseBroccoli;
import com.nomadconnection.broccoli.data.Session.UserData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.service.IntentDef;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMembershipPinNum extends BaseFragment implements OnClickListener {

	public static final String TAG = FragmentMembershipPinNum.class.getSimpleName();

	private Bundle mArgument;
	private String mUserPwd;
	private String mConfirm;
	private EditText mSecretEditText;
	private TextView mPasswordGuide;
	private boolean mCompareMode = false;
	private boolean isJoining = false;
	private ImageView mIndicator1, mIndicator2, mIndicator3, mIndicator4, mIndicator5, mIndicator6;
	private View mNext;
	private BaseInputConnection mInputConnection;
	private DayliApi mDayliApi;

	public FragmentMembershipPinNum() {
		// TODO Auto-generated constructor stub
		setTagName(TAG);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_membership_step_fourth, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.UserPinReg);
		init(view);
		
		return view;
	}
	
	void init(View view) {
		mDayliApi = ServiceGenerator.createServiceDayli(DayliApi.class);
		mArgument = getArguments();
		mIndicator1 = (ImageView) view.findViewById(R.id.screen_lock_num1_imgview);
		mIndicator2 = (ImageView) view.findViewById(R.id.screen_lock_num2_imgview);
		mIndicator3 = (ImageView) view.findViewById(R.id.screen_lock_num3_imgview);
		mIndicator4 = (ImageView) view.findViewById(R.id.screen_lock_num4_imgview);
		mIndicator5 = (ImageView) view.findViewById(R.id.screen_lock_num5_imgview);
		mIndicator6 = (ImageView) view.findViewById(R.id.screen_lock_num6_imgview);

		view.findViewById(R.id.screen_lock_key_num_0).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_1).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_2).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_3).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_4).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_5).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_6).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_7).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_8).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_9).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_back).setOnClickListener(this);
		view.findViewById(R.id.screen_lock_key_num_cancel).setOnClickListener(this);
		mNext = view.findViewById(R.id.fl_fragment_membership_next);
		mNext.setOnClickListener(this);
		mNext.setEnabled(false);
		mPasswordGuide = (TextView) view.findViewById(R.id.tv_fragment_membership_step_fourth_guide);
		mPasswordGuide.setText(getString(R.string.membership_text_step_guide_fourth));
		mSecretEditText = (EditText) view.findViewById(R.id.secret_edittext);
		mInputConnection = new BaseInputConnection(mSecretEditText, true);
		mSecretEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				showLengthIndicator(s.length());
				if (s.length() == 6) {
					if (!mCompareMode) {
						enterCompareMode(s.toString());
					} else {
						mConfirm = s.toString();
						if (comparePwd()) {
							mNext.setEnabled(true);
						} else {
							mNext.setEnabled(false);
							((MembershipActivity)getActivity()).setGuideText(getString(R.string.membership_text_pwd_not_matched_guide));
							mPasswordGuide.setText(getString(R.string.membership_text_pwd_not_matched_guide));
							mSecretEditText.setText(null);
						}
					}
				} else {
					mNext.setEnabled(false);
				}
			}
		});
	}
	
	@Override
	public void onStart() {
		super.onStart();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		((MembershipActivity)getActivity()).setStepGuide(3);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		int id = view.getId();
		switch (id) {
			case R.id.btn_fragment_membership_next:
				break;
			case R.id.screen_lock_key_num_0:
				mSecretEditText.setText(mSecretEditText.getText()+"0");
				break;
			case R.id.screen_lock_key_num_1:
				mSecretEditText.setText(mSecretEditText.getText()+"1");
				break;
			case R.id.screen_lock_key_num_2:
				mSecretEditText.setText(mSecretEditText.getText()+"2");
				break;
			case R.id.screen_lock_key_num_3:
				mSecretEditText.setText(mSecretEditText.getText() + "3");
				break;
			case R.id.screen_lock_key_num_4:
				mSecretEditText.setText(mSecretEditText.getText() + "4");
				break;
			case R.id.screen_lock_key_num_5:
				mSecretEditText.setText(mSecretEditText.getText() + "5");
				break;
			case R.id.screen_lock_key_num_6:
				mSecretEditText.setText(mSecretEditText.getText() + "6");
				break;
			case R.id.screen_lock_key_num_7:
				mSecretEditText.setText(mSecretEditText.getText() + "7");
				break;
			case R.id.screen_lock_key_num_8:
				mSecretEditText.setText(mSecretEditText.getText() + "8");
				break;
			case R.id.screen_lock_key_num_9:
				mSecretEditText.setText(mSecretEditText.getText() + "9");
				break;
			case R.id.screen_lock_key_num_back:
				Editable text = mSecretEditText.getText();
				if (text.length()-1 > 0) {
					mSecretEditText.setText(text.delete(text.length()-2,text.length()-1));
				} else {
					mSecretEditText.setText("");
				}
				break;
			case R.id.screen_lock_key_num_cancel:
				cancel();
				break;
			case R.id.fl_fragment_membership_next:
				if (!isJoining) {
					joinDayliPass();
				}
				break;
		}
	}

	private void showLengthIndicator(int count) {
		switch (count) {
			case 0:
				mIndicator1.setImageResource(R.drawable.shape_row_indicator_off);
				mIndicator2.setImageResource(R.drawable.shape_row_indicator_off);
				mIndicator3.setImageResource(R.drawable.shape_row_indicator_off);
				mIndicator4.setImageResource(R.drawable.shape_row_indicator_off);
				mIndicator5.setImageResource(R.drawable.shape_row_indicator_off);
				mIndicator6.setImageResource(R.drawable.shape_row_indicator_off);
				break;
			case 1:
				mIndicator1.setImageResource(R.drawable.shape_row_indicator_on);
				mIndicator2.setImageResource(R.drawable.shape_row_indicator_off);
				break;
			case 2:
				mIndicator2.setImageResource(R.drawable.shape_row_indicator_on);
				mIndicator3.setImageResource(R.drawable.shape_row_indicator_off);
				break;
			case 3:
				mIndicator3.setImageResource(R.drawable.shape_row_indicator_on);
				mIndicator4.setImageResource(R.drawable.shape_row_indicator_off);
				break;
			case 4:
				mIndicator4.setImageResource(R.drawable.shape_row_indicator_on);
				mIndicator5.setImageResource(R.drawable.shape_row_indicator_off);
				break;
			case 5:
				mIndicator5.setImageResource(R.drawable.shape_row_indicator_on);
				mIndicator6.setImageResource(R.drawable.shape_row_indicator_off);
				break;
			case 6:
				mIndicator6.setImageResource(R.drawable.shape_row_indicator_on);
				break;
		}
	}

	private void enterCompareMode(String password) {
		mUserPwd = password;
		mCompareMode = true;
		mSecretEditText.setText(null);
		((MembershipActivity)getActivity()).setGuideText(getString(R.string.membership_text_compare_guide));
		mPasswordGuide.setText(getString(R.string.membership_text_compare_guide));
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.UserPinRegConfirm);
	}

	private boolean comparePwd() {
		boolean bool = false;
		if (mUserPwd != null) {
			if (mUserPwd.equals(mConfirm)) {
				bool = true;
			}
		}
		return bool;
	}
	
	private void cancel() {
		mConfirm = null;
		mUserPwd = null;
		mCompareMode = false;
		mSecretEditText.setText(null);
		((MembershipActivity)getActivity()).setGuideText(getString(R.string.membership_text_step_guide_fourth));
		mPasswordGuide.setText(getString(R.string.membership_text_step_guide_fourth));
	}

	private void joinDayliPass() {
		removeBankDB();
		DialogUtils.showLoading(getActivity());
		isJoining = true;
		final MembershipData data = (MembershipData) mArgument.getSerializable(Const.DATA);
		APPSharedPrefer.getInstance(getActivity()).setPrefBool(Const.PREFERENCE_SETTING_PUSH, true);

		DayliJoinData joinData = new DayliJoinData();
		joinData.setEmail(data.getEmail());
		joinData.setPassword(data.getPwd());
		joinData.setTelNumber(data.getPhoneNum());
		joinData.setDeviceId(ElseUtils.getDeviceUUID(getActivity()));
		Call<ResponseJoin> joinCall = mDayliApi.dayliPassJoin(joinData);
		joinCall.enqueue(new Callback<ResponseJoin>() {
			@Override
			public void onResponse(Call<ResponseJoin> call, Response<ResponseJoin> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					ResponseJoin result = response.body();
					if (result != null) {
						addMembership(data, result.getUserId(), response.headers().get(Const.TOKEN));
					}
				} else {
					isJoining = false;
					Toast.makeText(getContext(), "아이디 생성 중 문제가 발생하였습니다.", Toast.LENGTH_LONG).show();
				}
			}

			@Override
			public void onFailure(Call<ResponseJoin> call, Throwable t) {
				isJoining = false;
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void addMembership(final MembershipData membershipData, final long dpid, final String dpToken) {
		DialogUtils.showLoading(getActivity());
		UserData data = new UserData();
		data.setDpId(dpid);
		data.setDpAccessToken(dpToken);
		data.setOsType(1);
		data.setDeviceId(ElseUtils.getDeviceUUID(getActivity()));
		data.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
		data.setAdId(APPSharedPrefer.getInstance(getActivity()).getPrefString(APP.SP_USER_ADID));
		Call<ResponseBroccoli> call = ServiceGenerator.createService(UserApi.class).userAdd(data);
		call.enqueue(new Callback<ResponseBroccoli>() {
			@Override
			public void onResponse(Call<ResponseBroccoli> call, Response<ResponseBroccoli> response) {
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					ResponseBroccoli result = response.body();
					isJoining = false;
					if (result != null) {
						final String broccoliToken = response.headers().get(Const.TOKEN);
						final String id = result.userId;
						final String sign = result.signedRequest;
						try {
							Session.getInstance(getActivity()).acquirePublicFromId(id, new Session.OnKeyInterface() {
								@Override
								public void acquired(final String str) {
									try {
										LoginData loginData = DbManager.getInstance().getUserInfoDataSet(getContext(), str, id);
										loginData.setUserId(id);
										loginData.setPassword(sign);
										loginData.setDayliId(dpid);
										loginData.setDayliToken(dpToken);
										loginData.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
										loginData.setType(1); // android
										loginData.setDeviceId(ElseUtils.getDeviceUUID(getContext()));
										loginData.setLogin(LoginData.TRUE);
										loginData.setPinNumber(mUserPwd);
										Date current = Calendar.getInstance().getTime();
										loginData.setLastLogin(ElseUtils.getSimpleDate(current));
										DbManager.getInstance().insertOrUpdateUserInfoData(getContext(), str, loginData);
										Session.getInstance(getContext()).setToken(broccoliToken);
										Session.getInstance(getActivity()).setSimpleUserInfo(loginData, new Session.OnUserInfoUpdateCompleteInterface() {
											@Override
											public void complete(UserPersonalInfo info) {
												if (info != null) {
													insertBankData(str);
													terms_agree(broccoliToken); // 약관동의 항목 서버 올리기
													userRegisterCompleteDialog(id, membershipData.getName());
												}
											}

											@Override
											public void error(ErrorMessage message) {

											}
										});
									} catch (Exception e) {
										e.printStackTrace();
									}
									DialogUtils.dismissLoading();
									isJoining = false;
								}

								@Override
								public void error(ErrorMessage errorMessage) {
									Toast.makeText(getContext(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 재시도 해주세요.", Toast.LENGTH_LONG).show();
								}

								@Override
								public void fail() {
									isJoining = false;
									DialogUtils.dismissLoading();
								}
							});
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					DialogUtils.dismissLoading();
				} else {
					isJoining = false;
					DialogUtils.dismissLoading();
					Toast.makeText(getContext(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 비밀번호를 재입력 해주십시오.", Toast.LENGTH_LONG).show();
				}
			}

			@Override
			public void onFailure(Call<ResponseBroccoli> call, Throwable t) {
				isJoining = false;
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void startLoading(String id) {
		IgawAdbrix.firstTimeExperience(AnalyticsName.PinRegComplete);
		Intent broadcast = new Intent(IntentDef.INTENT_ACTION_UPLOAD_FIRST);
		getActivity().sendBroadcast(broadcast);
//		Intent intent = new Intent(getActivity(), LoadingActivity.class).setFlags(
//				Intent.FLAG_ACTIVITY_CLEAR_TOP
//						| Intent.FLAG_ACTIVITY_SINGLE_TOP);
//		intent.putExtra(Const.NEED_LOGIN, true);
		Intent intent = new Intent(getActivity(), MembershipCompleteActivity.class);
		startActivity(intent);
		getActivity().finish();
	}

	private void insertBankData(String str) {
		MembershipData data = (MembershipData) mArgument.getSerializable(Const.DATA);
		ArrayList<BankData> bankList = data.getBankList();
		if (bankList != null) {
			for (BankData bank : bankList) {
				ScrapDao.getInstance().insertBankData(getActivity(), str, Session.getInstance(getActivity()).getUserId(), bank);
			}
		}
		DialogUtils.dismissLoading();
	}

	private void removeBankDB() {
		try {
			ScrapDao.getInstance().removeBankData(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void userRegisterCompleteDialog(final String tempId, String userName){
		Intent broadcast = new Intent(IntentDef.INTENT_ACTION_UPLOAD_FIRST);
		getActivity().sendBroadcast(broadcast);
		DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						startLoading(tempId);
					}
				},
				new InfoDataDialog(R.layout.dialog_inc_case_9, userName, getResources().getString(R.string.common_confirm)));
	}

	private void terms_agree(String dpToken){	// 약관동의 항목 서버 올리기
		ArrayList<Integer> mAgrees = mArgument.getIntegerArrayList(Const.TERMS_AGREE);
		Call<Result> call = mDayliApi.termsAgree(dpToken, mAgrees);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {

			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {

			}
		});

	}
}
