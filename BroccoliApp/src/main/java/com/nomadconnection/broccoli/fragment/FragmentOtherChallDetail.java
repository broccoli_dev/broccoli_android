package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeBank;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.Etc.RequestEtcChallengeBank;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.ImageUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentOtherChallDetail extends BaseFragment{
	
	private static String TAG = "FragmentOtherChallDetail";
	private final int XDPI = 320;
	private final int XXDPI = 480;
	private final int XXXDPI = 640;
	InterfaceEditOrDetail mInterFaceEditOrDetail;
	/* Layout */
	private ImageView mImage;
	private TextView mTitle;
	private TextView mType;
	private TextView mCurrentSum;
	private TextView mTargetSum;
	private TextView mDueDate;
	private TextView mAccountName;
	private TextView mAccountNumber;
	private TextView mMonthlySum;
	private ProgressBar mRate;
	private TextView mRateValue;

	private EtcChallengeInfo mEtcChallengeInfo;
	private String mImgPath;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_other_chall_detail, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		challenge_info_list_get();
		super.onResume();
	}


	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			Intent intent = new Intent(getActivity().getIntent());
			if(intent.hasExtra(Const.CHALLENGE_INFO)){
				mEtcChallengeInfo = (EtcChallengeInfo)intent.getSerializableExtra(Const.CHALLENGE_INFO);
				challenge_bank_link();
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mImage = (ImageView)_v.findViewById(R.id.imv_fragment_chall_detail_image);
			mTitle = (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_title);
			mType = (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_title_sub);
			mCurrentSum = (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_current_sum_value);
			mCurrentSum.setTypeface(FontClass.setFont(getActivity()));
			mTargetSum = (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_target_sum_value);
			mTargetSum.setTypeface(FontClass.setFont(getActivity()));
			mDueDate =  (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_target_date_value);
			mAccountName =  (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_link_account_name);
			mAccountNumber =  (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_link_account_value);
			mMonthlySum = (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_month_sum_value);
			mRate = (ProgressBar)_v.findViewById(R.id.progressbar_fragment_chall_detail_rate);
			mRateValue = (TextView)_v.findViewById(R.id.tv_fragment_chall_detail_rate_value);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			String[]typeStr = getActivity().getResources().getStringArray(R.array.challenge_type);
			int type;
			long current;
			long target;

			if("".equals(mEtcChallengeInfo.getAccountId())){
				mAccountNumber.setVisibility(View.GONE);
				mAccountName.setText(getActivity().getResources().getString(R.string.other_challenge_detail_bank_link_need));
			}

			if("".equals(mEtcChallengeInfo.getAmt())){
				current = 0;
				target = Long.parseLong(mEtcChallengeInfo.getGoalAmt());
			}
			else {
				current = Long.parseLong(mEtcChallengeInfo.getAmt());
				target = Long.parseLong(mEtcChallengeInfo.getGoalAmt());
			}

			int value = (int)( (double)current / (double)target * 100.0 );

			if (current < 0) {
				value = 0;
			}

			String dueDate = ElseUtils.getBirthDay(mEtcChallengeInfo.getStartDate()) + "~"
					+ ElseUtils.getBirthDay(mEtcChallengeInfo.getEndDate());

			mTitle.setText(mEtcChallengeInfo.getTitle());

			if("".equals(mEtcChallengeInfo.getChallengeType())){
				mType.setText("");
			}
			else {
				type = Integer.parseInt(mEtcChallengeInfo.getChallengeType());
				mType.setText(typeStr[type-1]);
			}
			mCurrentSum.setText(ElseUtils.getDecimalFormat(current));
			mTargetSum.setText(ElseUtils.getDecimalFormat(target));
			mDueDate.setText(dueDate);
			if(mEtcChallengeInfo.getMonthlyAmt() != null){
				mMonthlySum.setText(ElseUtils.getStringDecimalFormat(mEtcChallengeInfo.getMonthlyAmt()));
			}
			else {
				mMonthlySum.setText("0");
			}
			if(value > 100){
				value = 100;
			}

			if(value < 31){
				mRateValue.setText(Integer.toString(value) + "%");
				mRate.setProgressDrawable(getActivity().getResources().getDrawable(R.drawable.custom_progress_chall_rate_1_horizontal));
				mRate.setProgress(value);
			}
			else if (value < 61){
				mRateValue.setText(Integer.toString(value) + "%");
				mRate.setProgressDrawable(getActivity().getResources().getDrawable(R.drawable.custom_progress_chall_rate_2_horizontal));
				mRate.setProgress(value);
			}
			else {
				mRateValue.setText(Integer.toString(value) + "%");
				mRate.setProgressDrawable(getActivity().getResources().getDrawable(R.drawable.custom_progress_chall_rate_3_horizontal));
				mRate.setProgress(value);
			}

			if(! "".equals(mEtcChallengeInfo.getImagePath()) && mEtcChallengeInfo.getImagePath() != null){
				mImgPath = mEtcChallengeInfo.getImagePath();
				if(ImageUtil.fileCheck(mImgPath))
				{
					updateImageView();
				}
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}



//===============================================================================//
// Bank Link get API
//===============================================================================//
	private void challenge_bank_link(){
		DialogUtils.showLoading(getActivity());
		RequestEtcChallengeBank mRequestEtcChallengeBank = new RequestEtcChallengeBank();
		mRequestEtcChallengeBank.setUserId(Session.getInstance(getActivity()).getUserId());
		mRequestEtcChallengeBank.setAccountType(0);

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<List<EtcChallengeBank>> call = api.challengeBankLink(mRequestEtcChallengeBank);
		call.enqueue(new Callback<List<EtcChallengeBank>>() {
			@Override
			public void onResponse(Call<List<EtcChallengeBank>> call, Response<List<EtcChallengeBank>> response) {
				DialogUtils.dismissLoading();
				List<EtcChallengeBank> mEtcChallengeBankList = new ArrayList<EtcChallengeBank>();
				mEtcChallengeBankList = response.body();
				for(int f= 0; f < mEtcChallengeBankList.size(); f++){
					if(mEtcChallengeInfo.getAccountId().equals(mEtcChallengeBankList.get(f).getAccountId())){
						mAccountNumber.setVisibility(View.VISIBLE);
						String name = mEtcChallengeBankList.get(f).getCompanyName() + " " + mEtcChallengeBankList.get(f).getAccountName();
						mAccountName.setText(name);
						mAccountNumber.setText(mEtcChallengeBankList.get(f).getAccountNum());
					}
				}
			}

			@Override
			public void onFailure(Call<List<EtcChallengeBank>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}



	public void challenge_info_list_get() {
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId userId = new RequestDataByUserId();
		userId.setUserId(Session.getInstance(getActivity()).getUserId());

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<List<EtcChallengeInfo>> call = api.listChallenge(userId);
		call.enqueue(new Callback<List<EtcChallengeInfo>>() {
			@Override
			public void onResponse(Call<List<EtcChallengeInfo>> call, Response<List<EtcChallengeInfo>> response) {
				DialogUtils.dismissLoading();
				List<EtcChallengeInfo> mEtcChallengeInfoList = new ArrayList<EtcChallengeInfo>();
				mEtcChallengeInfoList = response.body();
				for(int g = 0; g < mEtcChallengeInfoList.size(); g++){
					if(mEtcChallengeInfo.getChallengeId().equals(mEtcChallengeInfoList.get(g).getChallengeId())){
						mEtcChallengeInfo = mEtcChallengeInfoList.get(g);
						if (getActivity() != null) {
							Intent intent = getActivity().getIntent();
							if (intent != null)
								intent.putExtra(Const.CHALLENGE_INFO, mEtcChallengeInfo);
						}
						challenge_bank_link();
						initData();
					}
				}
			}

			@Override
			public void onFailure(Call<List<EtcChallengeInfo>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void updateImageView() {
		DisplayMetrics dm = getActivity().getApplicationContext().getResources().getDisplayMetrics();
		int width = dm.widthPixels;
		int densityDpi = (int)(dm.density * 160f);
		int height;
		switch (densityDpi){
			case XDPI:
				height = 406;
				break;
			case XXDPI:
				height = 609;
				break;
			case XXXDPI:
				height = 812;
				break;
			default:
				height = 406;
				break;
		}
		int degree = ImageUtil.GetExifOrientation(mImgPath);
		Bitmap backgroundBitmap = ImageUtil.loadBackgroundBitmap(getActivity(), mImgPath);
		Bitmap rotateBitmap = ImageUtil.GetRotatedBitmap(backgroundBitmap, degree);
		Bitmap resizeBitmap = ImageUtil.resizeBitmap(rotateBitmap, width, height);//ImageUtil.resizeBitmapImage(getActivity(), rotateBitmap);
		Bitmap cropBitmap = ImageUtil.cropCenterBitmap(resizeBitmap, width, height);
		mImage.setImageBitmap(cropBitmap);
		backgroundBitmap.recycle();
	}
}
