package com.nomadconnection.broccoli.fragment.remit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.CertListActivity;
import com.nomadconnection.broccoli.activity.remit.RemitRegisterPasswordActivity;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendPhone;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.constant.Const;

public class FragmentRemitPersonalCertify extends BaseFragment{
	
	private static String TAG = "FragmentRemitPersonalCertify";

	InterfaceEditOrDetail mInterfaceEditOrDetail;

	/* Layout */
	private Button mBtnCertify;

	/* Get Parcel Data */
	private Bundle mArgument;
	private RequestDataSendPhone mRequestDataSendPhone;
	private RequestDataSendAccount mRequestDataSendAccount;
	private RemitAccount mRemitAccount;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterfaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_remit_personal_certify, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			if(data != null){
				Intent intent = new Intent(getActivity(), RemitRegisterPasswordActivity.class);
				intent.putExtra(Const.REMIT_PASSWORD, true);
				startActivityForResult(intent, 0);
			}
			else {
				mInterfaceEditOrDetail.onDetail();
			}

		}
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			mArgument = getArguments();
//			Intent intent = new Intent(getActivity().getIntent());
//			if(intent.hasExtra(Const.REMIT_MAIN_DATA)){
//				mRemitMain = (ResponseRemitAccount)intent.getSerializableExtra(Const.REMIT_MAIN_DATA);
//			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mBtnCertify = (Button)_v.findViewById(R.id.btn_fragment_remit_personal_certify_check);
			mBtnCertify.setOnClickListener(BtnClickListener);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


//===============================================================================//
// Listener
//===============================================================================//
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.btn_fragment_remit_personal_certify_check:
					startCert();
					break;

				default:
					break;
			}
		}
	};


	private void startCert() {
		Intent intent = new Intent(getActivity(), CertListActivity.class);
		startActivityForResult(intent, 0);
	}
}
