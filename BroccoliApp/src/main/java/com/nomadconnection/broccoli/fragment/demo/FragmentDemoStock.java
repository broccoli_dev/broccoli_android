package com.nomadconnection.broccoli.fragment.demo;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewMain3;
import com.nomadconnection.broccoli.api.DemoApi;
import com.nomadconnection.broccoli.api.InvestApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.common.CustomListViewPalallax;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Demo.TrialStock;
import com.nomadconnection.broccoli.data.Demo.TrialStockDaily;
import com.nomadconnection.broccoli.data.InfoDataMain3;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.utils.ChartUtils;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.MyMarkerView;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDemoStock extends BaseFragment implements InterfaceFragmentInteraction {

    private static String TAG = FragmentDemoStock.class.getSimpleName();
    /* Layout */
    public CustomListViewPalallax mListView;
    /* Flexible Layout */
    private TextView mInfo1;
    private TextView mInfo2;
    private TextView mInfo5;
    private LinearLayout mInfoBtn1;
    private LinearLayout mInfoBtn2;
    private TextView mSubInfo1;
    private TextView mSubInfo2;
    private TextView mSubInfo3;
    private TextView mSubInfo4;
    private View mFlexibleArea;

    private TrialStock mMainData;
    private AdtListViewMain3 mAdapter;
    private ArrayList<InfoDataMain3> mAlInfoDataList;

    /* Chart */
    private int mLastXIndex = 0;
    private LineChart mChart;
    private ArrayList<String> mLineXEntries;
    private ArrayList<Entry> mLineYEntries;
    private InvestApi mApi;

    private int mAge = 20;
    private String mGender = "M";

    /**
     * Item Click Listener
     **/
    private OnItemClickListener ItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            DialogUtils.showDialogDemoFinish(getActivity());
        }
    };

    private OnChartValueSelectedListener mOnChartValueSelectedListener = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
            if (e == null)
                return;
            mLastXIndex = h.getXIndex();
            if (mLastXIndex == 0){
                mInfo2.setText("0 (0%)");
            } else {
                long mTemp1 = (long)mLineYEntries.get(0).getVal();
                long mTemp2 = (long)mLineYEntries.get(mLastXIndex).getVal();
                float mTemp3 = (mLineYEntries.get(mLastXIndex).getVal() - mLineYEntries.get(0).getVal()) / mLineYEntries.get(0).getVal() * 100;
                long different = mTemp2 - mTemp1;
                String temp = String.valueOf(Math.abs(different));
                if (temp.equalsIgnoreCase("0")){
                    mInfo2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mInfo2.setText("" + TransFormatUtils.getDecimalFormatRecvString(temp) + "  (" + String.format("%.2f", mTemp3) + "%)");
                }
                else {
                    if (different > 0) {
                        mInfo2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stock_white_up, 0, 0, 0);
                        mInfo2.setCompoundDrawablePadding(10);
                    } else {
                        mInfo2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stock_white_down, 0, 0, 0);
                        mInfo2.setCompoundDrawablePadding(10);
                    }
                    String sign = (different > 0 ? "+" : "-");
                    if (different == 0) {
                        sign = "";
                    }
                    mInfo2.setText("" + TransFormatUtils.getDecimalFormatRecvString(temp) + "  (" + sign + String.format("%.2f", Math.abs(mTemp3)) + "%)");
                }
            }
        }

        @Override
        public void onNothingSelected() {
            mChart.highlightValue(mLastXIndex, 0);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main3, null);

        if (parseParam()) {
            initWidget(v);
            initData();
        } else {
            getActivity().finish();
        }

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }


//===============================================================================//
// Listener
//===============================================================================//	

    //=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
    private boolean parseParam() {
        boolean bResult = true;
        try {
            mAlInfoDataList = new ArrayList<InfoDataMain3>();
            mLineXEntries = new ArrayList<String>();
            mLineYEntries = new ArrayList<Entry>();
        } catch (Exception e) {
            bResult = false;
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "parseParam() : " + e.toString());
            return bResult;
        }
        return bResult;
    }

    //=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
    private boolean initWidget(View _v) {
        boolean bResult = true;
        try {
            /* Layout */
            mListView = (CustomListViewPalallax) _v.findViewById(R.id.lv_fragment_main_3_listview);
            mAdapter = new AdtListViewMain3(getActivity(), mAlInfoDataList);


    		/* btn height만큼 채우기 */
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mFlexibleArea = (LinearLayout) inflater.inflate(R.layout.flexible_area_main_3, null);
            mListView.addHeaderView(mFlexibleArea);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(ItemClickListener);


			/* Flexibel Layout */
            mInfo1 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_info_1);
            mInfo2 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_info_2);
            mInfo5 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_info_5);
            mInfo5.setTypeface(FontClass.setFont(getActivity()));
            mInfoBtn1 = (LinearLayout) _v.findViewById(R.id.ll_flexible_area_main_3_info_btn_1);
            mInfoBtn2 = (LinearLayout) _v.findViewById(R.id.ll_flexible_area_main_3_info_btn_2);
            mSubInfo1 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_sub_info_1);
            mSubInfo2 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_sub_info_2);
            mSubInfo3 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_sub_info_3);
            mSubInfo4 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_sub_info_4);
            mSubInfo1.setVisibility(View.GONE);
            mSubInfo2.setVisibility(View.GONE);
            mSubInfo3.setVisibility(View.GONE);
            mSubInfo4.setVisibility(View.GONE);

            mInfoBtn2.setVisibility(View.GONE);
            mInfoBtn1.setVisibility(View.GONE);

			/* Chart */
            mChart = (LineChart) _v.findViewById(R.id.lc_flexible_area_main_3_chart);
            mChart.setDescription("");
            mChart.setNoDataTextDescription("");
            mChart.setNoDataText("");
            mChart.setBackgroundColor(Color.TRANSPARENT);
            mChart.setPinchZoom(false);    // scaling can now only be done on x- and y-axis separately
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setOnChartGestureListener(new OnChartGestureListener() {
                @Override
                public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

                }

                @Override
                public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

                }

                @Override
                public void onChartLongPressed(MotionEvent me) {

                }

                @Override
                public void onChartDoubleTapped(MotionEvent me) {

                }

                @Override
                public void onChartSingleTapped(MotionEvent me) {

                }

                @Override
                public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

                }

                @Override
                public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

                }

                @Override
                public void onChartTranslate(MotionEvent me, float dX, float dY) {

                }
            });
//			mChart.setDrawBarShadow(false);
//			// draw bars behind lines
//			mChart.setDrawOrder(ChartUtils.mDrawOrder);

            MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view_stock);
            mv.setChart(mChart);
            mChart.setMarkerView(mv);
//            mChart.setPadding(50, 0, 0,0);
//            mChart.setPaddingRelative(50,0,0,0);

//			mChart.fitScreen ();
//			mChart.setViewPortOffsets(0f, 0f, 0f, 0f);
//			mChart.setPadding(-20, 0, 20, 0);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);
//			rightAxis.setDrawAxisLine(true);
//			rightAxis.setAxisLineColor(R.color.main_1_layout_bg);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setDrawGridLines(false);
            leftAxis.setDrawAxisLine(false);
            leftAxis.setGridColor(Color.argb((int) (255 * 1), 255, 255, 255));
            leftAxis.setTextColor(Color.argb((int) (255 * 1), 255, 255, 255));
            leftAxis.enableGridDashedLine(10f, 10f, 0f);
            leftAxis.setTextSize(9f);
            leftAxis.setStartAtZero(false);
            leftAxis.setSpaceTop(30f);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setGridColor(Color.argb((int) (255 * 1), 255, 255, 255));
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);
            xAxis.setAxisLineColor(Color.argb((int) (255 * 1), 255, 255, 255));
            xAxis.enableGridDashedLine(10f, 10f, 0f);
            xAxis.setTextSize(9f);
            xAxis.setYOffset(15f);
            xAxis.setXOffset(0);
            xAxis.setTextColor(Color.argb((int) (66 * 1), 255, 255, 255));
			xAxis.setAvoidFirstLastClipping(true);
            xAxis.setLabelsToSkip(30);
//			xAxis.setXOffset(-20f);

            // hide
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.getXAxis().setEnabled(true);
            mChart.getLegend().setEnabled(false);
            mChart.setExtraBottomOffset(5f);

            mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries, mLineYEntries, APP.ASSET_STOCK_HISTORY_CHART_USE));
            mChart.invalidate();

            mChart.setOnChartValueSelectedListener(mOnChartValueSelectedListener);

        } catch (Exception e) {
            bResult = false;
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "initWidget() : " + e.toString());
            return bResult;
        }

        return bResult;
    }

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
    private void initData() {

        Bundle bundle = getArguments();
        mAge = bundle.getInt(Const.DEMO_AGE);
        mGender = bundle.getString(Const.DEMO_GENDER);
        getData();
    }

    private void getData() {
        DialogUtils.showLoading(getActivity());
        Call<JsonElement> call = ServiceGenerator.createServiceForDemo(DemoApi.class).getStockTrial(mAge, mGender);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        JsonElement element = response.body();
                        JsonObject object = element.getAsJsonObject();
                        if (object != null && object.has("stock")) {
                            JsonObject data = object.getAsJsonObject("stock");
                            TrialStock dataSet = new Gson().fromJson(data, TrialStock.class);
                            setData(dataSet);
                        } else {
                            ErrorUtils.parseError(response);
                        }
                    } else {
                        ElseUtils.network_error(getActivity());
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();											// 자산 로딩
                ElseUtils.network_error(getActivity());
            }
        });

    }


//===============================================================================//
//  Data
//===============================================================================//
    private void setData(TrialStock data) {
        mAlInfoDataList.clear();
        mMainData = data;
        if (mMainData == null) {
            CustomToast.makeText(getContext(), "데이터에 이상이 있습니다. 잠시 후 재시도 해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (mMainData != null) {
            long totalSum = data.value;
            InfoDataMain3 dataMain3 = new InfoDataMain3("평균 주식 평가액", "y",
                    ElseUtils.getDecimalFormat(totalSum), "평균 보유 종목 수 :"+" " + String.format("%.2f", mMainData.stockCount) + "개", "", "", "", "", "", null);
            mAlInfoDataList.add(dataMain3);
        }
        Object fund = null;
        if (fund != null) {

        } else {
            //// TODO: 16. 2. 14.
//            InfoDataMain3 dataMain3 = new InfoDataMain3(getString(R.string.common_fund), "N", "", "", "", "", "", "", "");
//            mAlInfoDataList.add(dataMain3);
//            차후 작업
        }

        List<TrialStockDaily> estimate =  mMainData.dailyList;

        mLineXEntries = new ArrayList<>();
        mLineYEntries = new ArrayList<>();
        int count = 0;
        for (TrialStockDaily dailyEstimate : estimate) {
            if (dailyEstimate.price > 0){
                String date = ElseUtils.getMonthDay(dailyEstimate.stockDate);
                mLineXEntries.add(date);
                mLineYEntries.add(new Entry(dailyEstimate.price, count, date));
                count++;
            }
        }
        mLastXIndex = mLineXEntries.size() - 1;
        mChart.getXAxis().setLabelsToSkip(mLineXEntries.size()/3);
//        if (mLineXEntries.size() < 7){
//            for (int i = 0; i < 7-mLineXEntries.size(); i++) {
//                mLineXEntries.add("");
//            }
//        }

        mInfo1.setText("평균 총 평가액");
        mInfo5.setText(ElseUtils.getDecimalFormat(mMainData.value));
        mInfo2.setVisibility(View.GONE);
        mChart.clear();
        mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries, mLineYEntries, APP.ASSET_STOCK_HISTORY_CHART_USE));
        mChart.invalidate();
        if(mLastXIndex != -1){
            mOnChartValueSelectedListener.onValueSelected(new Entry(0, 0), 0, new Highlight(mLastXIndex, 0));
        }
        mChart.highlightValue(mLastXIndex, 0);
        mChart.animateX(1000, Easing.EasingOption.EaseInOutSine);

        mAdapter.setData(mAlInfoDataList);
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void reset(boolean flag) {

    }

    @Override
    public boolean canScrollUp() {
        boolean ret = false;
        if (mListView != null) {
            ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
        }
        return ret;
    }

    @Override
    public boolean canScrollDown() {
        return false;
    }
}
