package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetCredit;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetDaS;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetLoan;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetOther;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetStock;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.AdtListViewMain1;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Asset.AssetBankInfo;
import com.nomadconnection.broccoli.data.Asset.AssetDebtInfo;
import com.nomadconnection.broccoli.data.Asset.AssetMainData;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.InfoDataMain1;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Scrap.BaseChartData;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.ChartUtils;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.MyMarkerView;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMain1 extends BaseFragment implements InterfaceFragmentInteraction {

	//Comparator 를 만든다.
	private final static Comparator<AssetBankInfo> myComparator= new Comparator<AssetBankInfo>()  {
		private final Collator   collator = Collator.getInstance();

		@Override
		public int compare(AssetBankInfo object1, AssetBankInfo object2) {
			return collator.compare(object1.mAssetBankLastTransDate, object2.mAssetBankLastTransDate);
		}
	};

	private static String TAG = FragmentMain1.class.getSimpleName();
	public ListView mListView;
	/* Flexible Layout */
	private TextView mInfo1;
	private LinearLayout mInfo2;
	private LinearLayout mInfo3;
	//	private TextView mInfo2;
//	private TextView mInfo3;
//	private TextView mInfo4;
	private TextView mInfo5;
	private LinearLayout mInfoBtn1;
	private LinearLayout mInfoBtn2;
	/* Layout */
	private int lastTop = 0;
	/* Chart */
	private CombinedData mCombinedData;
	private ArrayList<Entry> mLineEntries;
	private ArrayList<BarEntry> mBarEntries;
//	private ArrayList<BubbleEntry> mBubbleEntries;
	private int mLastXIndex = 0;
	private View mFlexibleArea;
	private CombinedChart mChart;
	private AdtListViewMain1 mAdapter;
	private ArrayList<InfoDataMain1> mAlInfoDataList;
	private AssetMainData mAssetMainData;
	private boolean mCanClick = false;
	/**  Item Click Listener **/
	private OnItemClickListener ItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			if (position != 0 && mAlInfoDataList.get(position-1) == null) {
				return;
			}

			if (position == 0 ) return;

			if (!mCanClick) return;
			RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
			mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

			switch (mAlInfoDataList.get(position-1).mTxt1) {
			case "예금/적금":
				startActivity(new Intent(getActivity(), Level2ActivityAssetDaS.class).putExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_1, mAssetMainData.mMainAssetBank));
//				startActivity(new Intent(getActivity(), TestTestTest1.class));
				break;

			case "신용카드":
				Intent intent = new Intent(getActivity(), Level2ActivityAssetCredit.class);
				intent.putExtra(APP.INFO_DATA_ASSET_LOAN_DEPTH_1, mAssetMainData.mMainAssetDebt);
				startActivity(intent);
				break;

			case "주식":
				((BaseActivity)getActivity()).sendTracker(AnalyticsName.AssetMainClickStock);
				startActivityForResult(new Intent(getActivity(), Level2ActivityAssetStock.class)
						.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_1, mAssetMainData.mMainAssetStock.mAssetStockList)
						, APP.ACTIVIYT_FOR_RESULT_MAIN_1_REQUEST_CODE);
				break;

			case "펀드":
//				Toast.makeText(getActivity(), "펀드 나중에", Toast.LENGTH_SHORT).show();
				break;

			case "대출":
					startActivity(new Intent(getActivity(), Level2ActivityAssetLoan.class).putExtra(APP.INFO_DATA_ASSET_LOAN_DEPTH_1, mAssetMainData.mMainAssetDebt));
				break;

			case "기타재산":
				startActivityForResult(new Intent(getActivity(), Level2ActivityAssetOther.class)
						.putExtra(APP.INFO_DATA_ASSET_OTHER_HOUSE_DEPTH_1, mAssetMainData.mMainAssetRealEstate)
						.putExtra(APP.INFO_DATA_ASSET_OTHER_CAR_DEPTH_1, mAssetMainData.mMainAssetCar)
						.putExtra(APP.INFO_DATA_ASSET_OTHER_DEPTH_1, mAssetMainData.mMainAssetEtc)
						, APP.ACTIVIYT_FOR_RESULT_MAIN_1_REQUEST_CODE);

				break;

			case "기타":
//				startActivity(new Intent(getActivity(), Level2ActivityAssetAdviser.class));
				break;

			default:
				break;
			}
		}
	};
	private OnChartValueSelectedListener mOnChartValueSelectedListener = new OnChartValueSelectedListener() {
		@Override
		public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
			if (e == null)
				return;
			mLastXIndex = h.getXIndex();
			mCombinedData.setData(ChartUtils.generateLineDataCircle(mLineEntries, APP.MAIN_LIST_CHART_USE_1, mLastXIndex));
//			mCombinedData.setData(ChartUtils.generateBubbleData(mBubbleEntries, APP.MAIN_LIST_CHART_USE_1, mLastXIndex));
			mChart.setData(mCombinedData);
			mChart.getMarkerView().getChildAt(0).setVisibility(mLineEntries.get(h.getXIndex()).getVal() == 0 ? View.GONE:View.VISIBLE);
			mChart.invalidate();
		}

		@Override
		public void onNothingSelected() {
			mChart.highlightValue(mLastXIndex, 0);
		}
	} ;

	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

				case R.id.ll_flexible_area_main_1_info_btn_1:
					if (getActivity() != null)
						((BaseActivity)getActivity()).sendTracker(AnalyticsName.AssetGraphBtn);
					mInfoBtn1.setSelected(true);
					mInfoBtn2.setSelected(false);

					if (mAssetMainData == null) return;
					if (mAssetMainData.mMainAssetMonthlyProperty == null) return;

					mInfo1.setText("총 재산");
					mInfo2.setVisibility(View.VISIBLE);
					mInfo3.setVisibility(View.GONE);
					mInfo5.setText(TransFormatUtils.getDecimalFormat(mAssetMainData.mMainAssetMonthlyProperty.get(mAssetMainData.mMainAssetMonthlyProperty.size() - 1).sum));
					refreshChart(mAssetMainData.mMainAssetMonthlyProperty);
					mOnChartValueSelectedListener.onValueSelected(new Entry(0, 0), 0, new Highlight(mLineEntries.size()-1,0));
					break;

				case R.id.ll_flexible_area_main_1_info_btn_2:
					if (getActivity() != null)
						((BaseActivity)getActivity()).sendTracker(AnalyticsName.AssetGraphBtn);
					mInfoBtn1.setSelected(false);
					mInfoBtn2.setSelected(true);

					if (mAssetMainData == null) return;
					if (mAssetMainData.mMainAssetMonthlyDebt == null) return;
					mInfo1.setText("총 부채");
					mInfo2.setVisibility(View.GONE);
					mInfo3.setVisibility(View.VISIBLE);
					mInfo5.setText(TransFormatUtils.getDecimalFormat(mAssetMainData.mMainAssetMonthlyDebt.get(mAssetMainData.mMainAssetMonthlyDebt.size() - 1).sum));
					refreshChart(mAssetMainData.mMainAssetMonthlyDebt);
					mOnChartValueSelectedListener.onValueSelected(new Entry(0, 0), 0, new Highlight(mLineEntries.size()-1,0));
					break;

				case R.id.inc_card_a:
					startActivity(new Intent(getActivity(), Level2ActivityAssetDaS.class).putExtra(APP.INFO_DATA_ASSET_DAS_DEPTH_1, mAssetMainData.mMainAssetBank));
					break;

				default:
					break;
			}

		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_main1, null);

		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		return v;
	}

	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



//===============================================================================//
// Refresh
//===============================================================================//

	@Override
	public void onResume() {
		super.onResume();
		refreshData();
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			mAlInfoDataList = new ArrayList<InfoDataMain1>();
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Layout */
			mListView = (ListView)_v.findViewById(R.id.lv_fragment_main_1_listview);
			mAdapter = new AdtListViewMain1(getActivity(), mAlInfoDataList, BtnClickListener, ItemClickListener);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				mListView.setNestedScrollingEnabled(true);
			}

			LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mFlexibleArea = (LinearLayout)inflater.inflate(R.layout.flexible_area_main_1, null);
			mListView.addHeaderView(mFlexibleArea);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(ItemClickListener);


			/* Flexibel Layout */
			mInfo1 = (TextView)mFlexibleArea.findViewById(R.id.tv_flexible_area_main_1_info_1);
			mInfo2 = (LinearLayout)mFlexibleArea.findViewById(R.id.ll_flexible_area_main_1_info_2);
			mInfo3 = (LinearLayout)mFlexibleArea.findViewById(R.id.ll_flexible_area_main_1_info_3);
			mInfo5 = (TextView)mFlexibleArea.findViewById(R.id.tv_flexible_area_main_1_info_5);
			mInfo5.setTypeface(FontClass.setFont(getActivity()));
			mInfoBtn1 = (LinearLayout)mFlexibleArea.findViewById(R.id.ll_flexible_area_main_1_info_btn_1);
			mInfoBtn2 = (LinearLayout)mFlexibleArea.findViewById(R.id.ll_flexible_area_main_1_info_btn_2);
			mInfoBtn1.setOnClickListener(BtnClickListener);
			mInfoBtn2.setOnClickListener(BtnClickListener);
//			Log.d(TAG, "mInfoBtn1 = ?"+mInfoBtn1);

			/* Chart */
			mChart = (CombinedChart) mFlexibleArea.findViewById(R.id.chart_bar_and_line_chart);
			mChart.setDescription("");
			mChart.setNoDataTextDescription("");
			mChart.setNoDataText("");
			mChart.setBackgroundColor(Color.TRANSPARENT);
//			mChart.setMaxVisibleValueCount(60);	// if more than 60 entries are displayed in the chart, no values will be // drawn
			mChart.setPinchZoom(false);	// scaling can now only be done on x- and y-axis separately
			mChart.setScaleEnabled(false);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			// draw bars behind lines
			mChart.setDrawOrder(ChartUtils.mDrawOrder);

			MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
			mv.setChart(mChart);
			mChart.setMarkerView(mv);

			YAxis rightAxis = mChart.getAxisRight();
			rightAxis.setDrawGridLines(false);

			YAxis leftAxis = mChart.getAxisLeft();
			leftAxis.setDrawGridLines(false);

			XAxis xAxis = mChart.getXAxis();
			xAxis.setDrawAxisLine(false);
			xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
			xAxis.setDrawGridLines(false);
			xAxis.setTextSize(11f);
			xAxis.setYOffset(10);
			xAxis.setTextColor(Color.argb((int) (255 * 1), 255, 255, 255));

			/* Sample Data */
			mCombinedData = new CombinedData(new String[]{"","","","","",""});
//			mCombinedData = new CombinedData(ChartUtils.getMonth(0, 6));

			/* Sample Data */
			mLineEntries = new ArrayList<Entry>();
			mBarEntries = new ArrayList<BarEntry>();
//			mBubbleEntries = new ArrayList<BubbleEntry>();
			for (int i = 0; i < 6 ; i++) {
				mLineEntries.add(new Entry(0, i));
				mBarEntries.add(new BarEntry(0, i));
			}

			mCombinedData.setData(ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_1));
			mCombinedData.setData(ChartUtils.generateBarData(mBarEntries, APP.MAIN_LIST_CHART_USE_1));

//			mLineEntries.add(new Entry(10902000, 0)); mLineEntries.add(new Entry(30602000, 1)); mLineEntries.add(new Entry(20902000, 2));
//			mLineEntries.add(new Entry(40702000, 3)); mLineEntries.add(new Entry(30902000, 4)); mLineEntries.add(new Entry(50902000, 5));
//			mBarEntries.add(new BarEntry(10902000, 0)); mBarEntries.add(new BarEntry(30602000, 1)); mBarEntries.add(new BarEntry(20902000, 2));
//			mBarEntries.add(new BarEntry(40702000, 3)); mBarEntries.add(new BarEntry(30902000, 4)); mBarEntries.add(new BarEntry(50902000, 5));

			// hide
			mChart.getAxisRight().setEnabled(false);
			mChart.getAxisLeft().setEnabled(false);
			mChart.getLegend().setEnabled(false);

			mChart.setData(mCombinedData);
			mChart.invalidate();

			mChart.setOnChartValueSelectedListener(mOnChartValueSelectedListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//===============================================================================//
// Listener
//===============================================================================//

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
//			BtnClickListener.onClick(mInfoBtn1);
			mInfoBtn1.setSelected(true);
//			mLastXIndex = mLineEntries.size()-1;
//			mChart.highlightValue(mLastXIndex,0);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	/**  Refresh **/
	private void refreshData(){
		mCanClick = false;
		DialogUtils.showLoading(getActivity());											// 자산 로딩
		Call<AssetMainData> mCall = ServiceGenerator.createService(AssetApi.class).getAssetMain();
		mCall.enqueue(new Callback<AssetMainData>() {
			@Override
			public void onResponse(Call<AssetMainData> call, Response<AssetMainData> response) {
				mCanClick = true;
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						mAssetMainData = response.body();
						String code = mAssetMainData.getCode();
						if (mAssetMainData != null && (code == null || code.isEmpty())) {
							resetData();
						} else {
							if (ErrorMessage.ERROR_716_TOKEN_EXPIRE.code().equalsIgnoreCase(mAssetMainData.getCode())
									|| ErrorMessage.ERROR_10007_SESSION_NOT_FOUND.code().equalsIgnoreCase(mAssetMainData.getCode())
									|| ErrorMessage.ERROR_10008_PWD_CHANGED_SESSION_EXPIRED.code().equalsIgnoreCase(mAssetMainData.getCode())
									|| ErrorMessage.ERROR_20003_MISMATCH_ACCESS_TOKEN.code().equalsIgnoreCase(mAssetMainData.getCode())) {
								memberLeaveComplete();
							} else {
								ElseUtils.network_error(getActivity());
							}
						}
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<AssetMainData> call, Throwable t) {
				mCanClick = true;
				DialogUtils.dismissLoading();											// 자산 로딩
				if (APP._SAMPLE_MODE_) {
					mAssetMainData = AssetSampleTestDataJson.GetSampleData1();
					resetData();
				}
				else ElseUtils.network_error(getActivity()); //CustomToast.getInstance(getActivity()).showShort("FragmentMain1 서버 통신 실패 : 매인");
			}
		});
	}

	/**  Refresh **/
	private void resetData(){
		if (mAlInfoDataList != null){
			mAlInfoDataList.removeAll(mAlInfoDataList);


			ArrayList<AssetBankInfo> mAssetBankInfo = new ArrayList<AssetBankInfo>();

			long mTempTotalBankSum = 0;
			if (mAssetMainData.mMainAssetBank != null) {
				for (int i = 0; i < mAssetMainData.mMainAssetBank.size(); i++) {
					AssetBankInfo bankInfo = mAssetMainData.mMainAssetBank.get(i);
					if (bankInfo.isChecked()) {
						mAssetBankInfo.add(mAssetMainData.mMainAssetBank.get(i));
						mTempTotalBankSum += Long.valueOf(TransFormatUtils.transNullToCost(bankInfo.mAssetBankSum));
					}
//				if (i == 0) mTempTotalBankAfterValue = Integer.valueOf(TransFormatUtils.transNullToCost(mAssetMainData.mMainAssetBank.get(0).mAssetBankAfterValue));
				}
				Collections.sort(mAssetBankInfo, myComparator);
				Collections.reverse(mAssetBankInfo);
			} else {
				CustomToast.makeText(this.getActivity(), "서버에 통신에 문제가 있습니다.", Toast.LENGTH_SHORT).show();
				return;
			}

			mAlInfoDataList.add(new InfoDataMain1("예금/적금",
					((mAssetBankInfo != null && mAssetBankInfo.size() != 0) ? "y" : "n"),
					((mAssetBankInfo != null && mAssetBankInfo.size() != 0) ? String.valueOf(mTempTotalBankSum) : "0"),
					((mAssetBankInfo != null && mAssetBankInfo.size() != 0) ? mAssetBankInfo.get(0).mAssetBankLastTransDate : ""),
					((mAssetBankInfo != null && mAssetBankInfo.size() != 0) ? mAssetBankInfo.get(0).mAssetBankAfterValue : ""),
//					((mAssetMainData.mMainAssetBank.size() != 0) ? String.valueOf(mTempTotalBankAfterValue) : "0"),
					"", "",
					((mAssetBankInfo != null) ? String.valueOf(mAssetBankInfo.size()) : "0"), "",
					((mAssetBankInfo != null && mAssetBankInfo.size() != 0) ? mAssetBankInfo.get(0).mAssetBankLastTransOpponent : "")));

			InfoDataMain1 dataMain = new InfoDataMain1();
			dataMain.setData(mAssetMainData.mMainAssetCard);
			dataMain.mTxt1 = "신용카드";
			dataMain.mTxt2 = (mAssetMainData.mMainAssetCard != null && mAssetMainData.mMainAssetCard.mAssetCardCount != 0) ? "y" : "n";
			mAlInfoDataList.add(dataMain);

			int mTempCount = 0;
			long mTempSum = 0;
			long mTempPrevSum = 0;
			String mTempProfit = "";
			for (int i = 0; i <mAssetMainData.mMainAssetStock.mAssetStockList.size() ; i++) {
				if (!mAssetMainData.mMainAssetStock.mAssetStockList.get(i).mBodyAssetStockSum.equals("0")){
					mTempCount++;
					mTempPrevSum += Long.valueOf(TransFormatUtils.transNullToCost(mAssetMainData.mMainAssetStock.mAssetStockList.get(i).mBodyAssetStockPreviousSum));
					mTempSum += Long.valueOf(TransFormatUtils.transNullToCost(mAssetMainData.mMainAssetStock.mAssetStockList.get(i).mBodyAssetStockSum));
				}
			}
			long value = mTempSum - mTempPrevSum;
			mTempProfit = String.valueOf(Math.abs(value));
			String sign = (value > 0) ? "+":"-";

			mAlInfoDataList.add(new InfoDataMain1("주식",
					((mAssetMainData.mMainAssetStock.mAssetStockList.size() != 0) ? "y" : "n"),
//					String.valueOf((int) mTempSum),
					String.valueOf(mTempSum),
					((mAssetMainData.mMainAssetStock.mAssetStockList.size() != 0) ? mAssetMainData.mMainAssetStock.mAssetStockDate : ""),
					mTempProfit,
					"", "",
					((mAssetMainData.mMainAssetStock.mAssetStockList.size() != 0) ? String.valueOf(mTempCount) : "0"), "",
					sign+((mTempPrevSum != 0) ? ElseUtils.getPointCut(Double.valueOf(mTempProfit) / (double) mTempPrevSum * 100) : "0")));
//					String.format("%.2f", (float) (mTempSum / mTempPrevSum))));
//					String.valueOf((float) (mTempSum / mTempPrevSum))));

			long mTempTotalDebtSum = 0;
			ArrayList<AssetDebtInfo> assetDebtInfoList = new ArrayList<AssetDebtInfo>();

			if (mAssetMainData.mMainAssetDebt.size() != 0){
				for (int i = 0; i <mAssetMainData.mMainAssetDebt.size() ; i++) {
					AssetDebtInfo info = mAssetMainData.mMainAssetDebt.get(i);
					if (info.mLoanType == AssetDebtInfo.LOAN && info.isChecked()) {
						mTempTotalDebtSum += Long.valueOf(TransFormatUtils.transNullToCost(info.mAssetDebtSum));
						assetDebtInfoList.add(info);
					} else if (info.mLoanType == AssetDebtInfo.LOAN_CARD) {
						mTempTotalDebtSum += Long.valueOf(TransFormatUtils.transNullToCost(info.mAssetDebtSum));
						assetDebtInfoList.add(info);
					}
				}
			}
			mAlInfoDataList.add(new InfoDataMain1("대출",
					((assetDebtInfoList.size() != 0) ? "y" : "n"),
//					((mAssetMainData.mMainAssetDebt.size() != 0) ? TransFormatUtils.transNullToCost(mAssetMainData.mMainAssetDebt.get(0).mAssetDebtSum) : "0"),
					((assetDebtInfoList.size() != 0) ? String.valueOf(mTempTotalDebtSum) : "0"),
					"", "", "", "",
					((assetDebtInfoList.size() != 0) ? String.valueOf(assetDebtInfoList.size()) : "0"), "", ""));

			long mTempTotalRealEstate = 0;
			if (mAssetMainData.mMainAssetRealEstate.size() != 0){
				for (int i = 0; i <mAssetMainData.mMainAssetRealEstate.size() ; i++) {
					mTempTotalRealEstate += Long.valueOf(TransFormatUtils.transNullToCost(mAssetMainData.mMainAssetRealEstate.get(i).mAssetEstatePrice));
				}
			}
			long mTempTotalCar = 0;
			if (mAssetMainData.mMainAssetCar.size() != 0){
				for (int i = 0; i <mAssetMainData.mMainAssetCar.size() ; i++) {
					mTempTotalCar += Long.valueOf(TransFormatUtils.transNullToCost(mAssetMainData.mMainAssetCar.get(i).mAssetCarMarketPrice));
				}
			}
			long tempTotalEtc = 0;
			if (mAssetMainData.mMainAssetEtc.size() != 0){
				for (int i = 0; i <mAssetMainData.mMainAssetEtc.size() ; i++) {
					tempTotalEtc += Long.valueOf(TransFormatUtils.transNullToCost(mAssetMainData.mMainAssetEtc.get(i).value));
				}
			}
			String enable = (mAssetMainData.mMainAssetRealEstate.size() != 0 || mAssetMainData.mMainAssetCar.size() != 0 || mAssetMainData.mMainAssetEtc.size() != 0) ? "y":"n";
			mAlInfoDataList.add(new InfoDataMain1("기타재산",
					enable, "", "",
					((mAssetMainData.mMainAssetRealEstate.size() != 0) ? String.valueOf(mTempTotalRealEstate) : "0"),
					((mAssetMainData.mMainAssetCar.size() != 0) ? String.valueOf(mTempTotalCar) : "0"),
					((mAssetMainData.mMainAssetEtc.size() != 0) ? String.valueOf(tempTotalEtc) : "0"),
					(enable.equalsIgnoreCase("y")
							? String.valueOf(mAssetMainData.mMainAssetRealEstate.size() + mAssetMainData.mMainAssetCar.size() + mAssetMainData.mMainAssetEtc.size()) : "0"), "", ""));

			mAdapter.notifyDataSetChanged();

			if (mInfoBtn1.isSelected()) {
				BtnClickListener.onClick(mInfoBtn1);
			}
			else {
				BtnClickListener.onClick(mInfoBtn2);
			}

//			if (!MainApplication.mAPPShared.getPrefBool(APP.SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER, false)){
//				showMobileData();
//			}
		}

	}



//===============================================================================//
// Chart
//===============================================================================//

	private void showMobileData() {
		DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				MainApplication.mAPPShared.setPrefBool(APP.SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER, true);
			}
		}, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.common_warning_mobile_data), "확인"));
	}

	/**  Refresh chart **/
	private void refreshChart(List<BaseChartData> _BaseChartData){
		mLineEntries.removeAll(mLineEntries);
		mBarEntries.removeAll(mBarEntries);
//		mBubbleEntries.removeAll(mBubbleEntries);
		String[] mMonthP = new String[_BaseChartData.size()];
		ChartUtils.main_asset_value = new ArrayList<Long>();
		for (int i = 0; i < _BaseChartData.size(); i++) {
			mMonthP[i] = _BaseChartData.get(i).date.substring(4);
			ChartUtils.main_asset_value.add(_BaseChartData.get(i).sum);
			mLineEntries.add(new Entry(Long.valueOf(_BaseChartData.get(i).sum), i));
			mBarEntries.add(new BarEntry(Long.valueOf(_BaseChartData.get(i).sum), i));
//			mBubbleEntries.add(new BubbleEntry(i, Integer.valueOf(_BaseChartData.get(i).sum), 1f));
		}
		mCombinedData = new CombinedData(mMonthP);
		mCombinedData.setData(ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_1));
		mCombinedData.setData(ChartUtils.generateBarData(mBarEntries, APP.MAIN_LIST_CHART_USE_1));
//		mChart.animateXY(1500, 1500);
		mChart.animateY(1500);
		mChart.setData(mCombinedData);
		mChart.invalidate();
		mLastXIndex = mLineEntries.size() - 1;
		mChart.highlightValue(mLastXIndex,0);
	}


	@Override
	public void reset(boolean flag) {
		if (mListView != null) {
			mListView.setSelection(0);
		}
	}

	@Override
	public boolean canScrollUp() {
		boolean ret = false;
		if (mListView != null) {
			ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
		}
		return ret;
	}

	@Override
	public boolean canScrollDown() {
		return false;
	}
}
