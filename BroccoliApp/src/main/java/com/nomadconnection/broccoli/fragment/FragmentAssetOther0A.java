package com.nomadconnection.broccoli.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.Collections;

public class FragmentAssetOther0A extends BaseFragment {
	
	private static String TAG = FragmentAssetOther0A.class.getSimpleName();

	/* Layout House*/
	private LinearLayout mHouseLayout;
	private LinearLayout mHouseSpinner1;
	private LinearLayout mHouseSpinner2;
	public static EditText mHouseInfo0;
	public static TextView mHouseInfo1;
	public static TextView mHouseInfo2;
	public static EditText mHouseInfo3;
	private String mTextWatcherResult = "";

	private ArrayList<String> mHouseSpinner1Data;
	private ArrayList<String> mHouseSpinner2Data;
	private int mHouseSpinner1Pos;
	private int mHouseSpinner2Pos;

	InterfaceEditOrDetail mInterFaceEditOrDetail;


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_asset_other_0_a, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.FragmentAssetOther0A);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		ElseUtils.hideSoftKeyboard(mHouseInfo0);
		ElseUtils.hideSoftKeyboard(mHouseInfo3);
	}


	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout House*/
			mHouseLayout = (LinearLayout)_v.findViewById(R.id.ll_asset_other_inc_house);
			mHouseSpinner1 = (LinearLayout)_v.findViewById(R.id.ll_asset_other_inc_house_spinner_layout_1);
			mHouseSpinner2 = (LinearLayout)_v.findViewById(R.id.ll_asset_other_inc_house_spinner_layout_2);
			mHouseInfo1 = (TextView)_v.findViewById(R.id.tv_asset_other_inc_house_spinner_text_1);
			mHouseInfo2 = (TextView)_v.findViewById(R.id.tv_asset_other_inc_house_spinner_text_2);
			mHouseInfo0 = (EditText)_v.findViewById(R.id.et_asset_other_inc_house_edittext_0);
			mHouseInfo0.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송

				}
			});
			mHouseInfo3 = (EditText)_v.findViewById(R.id.et_asset_other_inc_house_edittext_1);
			mHouseInfo3.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(!s.toString().equals(mTextWatcherResult)){
						if (s.toString().length() == 0) mTextWatcherResult = "0";
						else mTextWatcherResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
						mHouseInfo3.setText(mTextWatcherResult);
						mHouseInfo3.setSelection(mTextWatcherResult.length());
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			});

			mHouseSpinner1.setOnClickListener(BtnClickListener);
			mHouseSpinner2.setOnClickListener(BtnClickListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mHouseSpinner1Data = new ArrayList<String>();
			mHouseSpinner2Data = new ArrayList<String>();

			Collections.addAll(mHouseSpinner1Data, getResources().getStringArray(R.array.estate_type_name));
			Collections.addAll(mHouseSpinner2Data, getResources().getStringArray(R.array.deal_type_name));

			mHouseSpinner1Pos = 0;
			mHouseSpinner2Pos = 0;

			mHouseInfo0.setHint("주택자산 명 입력");
//			mHouseInfo1.setText("선택");
//			mHouseInfo2.setText("선택");
//			mHouseInfo1.setHint("선택");
//			mHouseInfo2.setHint("선택");
			mHouseInfo3.setHint(getString(R.string.common_input_sum_hint));
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}







//===============================================================================//
// Listener
//===============================================================================//
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
//			v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
			mHouseInfo0.clearFocus();
			mHouseInfo3.clearFocus();
			switch (v.getId()) {

				case R.id.ll_asset_other_inc_house_spinner_layout_1:
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
					int mTempLeft1 = (ElseUtils.getRect(v).left);
					DialogUtils.showDlgSpinnerList(getActivity(), new Dialog.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mHouseSpinner1Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
							mHouseInfo1.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
							mHouseInfo1.setText(mHouseSpinner1Data.get(mHouseSpinner1Pos));
							send_interface_complete();
							dialog.dismiss();
						}
					}, mHouseSpinner1Data, mTempTop1, mTempLeft1, mHouseSpinner1Pos, new Dialog.OnDismissListener(){

						@Override
						public void onDismiss(DialogInterface dialog) {
							v.setBackgroundResource(R.drawable.selector_spinner_bg);
						}
					});
					break;

				case R.id.ll_asset_other_inc_house_spinner_layout_2:
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					int mTempTop2 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
					int mTempLeft2 = (ElseUtils.getRect(v).left);
					DialogUtils.showDlgSpinnerList(getActivity(), new Dialog.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mHouseSpinner2Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
							mHouseInfo2.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
							mHouseInfo2.setText(mHouseSpinner2Data.get(mHouseSpinner2Pos));
							send_interface_complete();
							dialog.dismiss();
						}
					}, mHouseSpinner2Data, mTempTop2, mTempLeft2, mHouseSpinner2Pos, new Dialog.OnDismissListener(){

						@Override
						public void onDismiss(DialogInterface dialog) {
							v.setBackgroundResource(R.drawable.selector_spinner_bg);
						}
					});
					break;

				default:
					break;
			}

		}
	};


	private void send_interface_complete(){
		if(empty_check()){
			mInterFaceEditOrDetail.onEdit();
		}
		else {
			mInterFaceEditOrDetail.onError();
		}
	}

	private boolean empty_check(){
		if(mHouseInfo0.length() == 0 || mHouseInfo1.getText().toString().equals("선택")
				|| mHouseInfo2.getText().toString().equals("선택") || mHouseInfo3.length() == 0 || mHouseInfo3.getText().toString().equals("0")){
			return false;
		}
		return true;
	}
}
