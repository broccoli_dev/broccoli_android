package com.nomadconnection.broccoli.fragment.remit;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.BaseInputConnection;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataChangeMoney;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendPhone;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitChargeMoney;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitSend;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentRemitSendPassword extends BaseFragment implements View.OnClickListener{
	
	private static String TAG = "FragmentRemitSendPassword";

	private static final String EDIT_TEXT = "mEditText";
	private static final int PASSWORD_MINIMUM_LENGTH = 4;
	
	InterfaceEditOrDetail mInterfaceEditOrDetail;

	/* Layout */
	private View view;
	private Bundle mArgument;
	private LinearLayout mPassWordLayout;
	private ImageView mPassWordOne;
	private ImageView mPassWordTwo;
	private ImageView mPassWordThree;
	private ImageView mPassWordFourth;
	private ImageView mPassWordDel;
	private EditText mEditText;
	private BaseInputConnection mInputConnection;
	private TextView tmpTv;
	private String mCertNm;
	private String mPwd;
	private TextView mPasswordRetry;
	private TextView mPasswordReset;
	private View mPasswordCheck;
	private int mRemitPageStatus;

	/* Get Parcel Data */
	private RequestDataSendPhone mRequestDataSendPhone;
	private RequestDataSendAccount mRequestDataSendAccount;
	private RequestDataChangeMoney mRequestDataChangeMoney;
	private RemitAccount mRemitAccount;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterfaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.fragment_remit_send_password, null);
		
		if(parseParam()){
			if(initWidget(view)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return view;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
//		challenge_info_list_get();
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.ll_fragment_remit_send_password:
//				if (nfilter.isNFilterViewVisibility() != View.VISIBLE) {
//					onShowKeyboard();
//				}
				break;

			case R.id.iv_fragment_remit_send_del:
				mEditText.setText("");
				break;

			case R.id.tv_fragment_remit_password_send_reset:
				mInterfaceEditOrDetail.onEdit();
				break;

			case R.id.btn_fragment_remit_send_password_check:
				if(mRemitPageStatus == APP.REMIT_SEND_PHONE){
					sendMoneyPhoneNo();
				}
				else if(mRemitPageStatus == APP.REMIT_SEND_ACCOUNT) {
					sendMoneyAccount();
				}
				else {
					chargeWallet();
				}
				break;

			default:
				break;

		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			getActivity().finish();
		}
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			mArgument = getArguments();
			mRemitPageStatus = mArgument.getInt(Const.REMIT_PAGE_STATUS, APP.REMIT_SEND_PHONE);
			if(mRemitPageStatus == APP.REMIT_SEND_PHONE){	// 전화 송금
				mRequestDataSendPhone = (RequestDataSendPhone)mArgument.getSerializable(Const.DATA);
			}
			else if(mRemitPageStatus == APP.REMIT_SEND_ACCOUNT) {
				mRequestDataSendAccount = (RequestDataSendAccount)mArgument.getSerializable(Const.DATA);
			}
			else {
				mRequestDataChangeMoney = (RequestDataChangeMoney)mArgument.getSerializable(Const.DATA);
			}
			Intent intent = new Intent(getActivity().getIntent());
			if(intent.hasExtra(Const.REMIT_ACCOUNT_INFO)){
				mRemitAccount = (RemitAccount)intent.getSerializableExtra(Const.REMIT_ACCOUNT_INFO);
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mPassWordLayout = (LinearLayout)_v.findViewById(R.id.ll_fragment_remit_send_password);
			mPassWordLayout.setOnClickListener(this);
			mPassWordOne = (ImageView)_v.findViewById(R.id.iv_fragment_remit_send_lock_num1);
			mPassWordTwo = (ImageView)_v.findViewById(R.id.iv_fragment_remit_send_lock_num2);
			mPassWordThree = (ImageView)_v.findViewById(R.id.iv_fragment_remit_send_lock_num3);
			mPassWordFourth = (ImageView)_v.findViewById(R.id.iv_activity_remit_send_lock_num4);
			mPassWordDel = (ImageView) _v.findViewById(R.id.iv_fragment_remit_send_del);
			mPassWordDel.setOnClickListener(this);
			mPassWordDel.setVisibility(View.GONE);
			mEditText = (EditText)_v.findViewById(R.id.et_fragment_remit_send_secret_text);
//			mInputConnection = new BaseInputConnection(mEditText, true);
			mEditText.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					int count = s.length();
					switch (count) {
						case 0:
							mPassWordOne.setVisibility(View.GONE);
							mPassWordTwo.setVisibility(View.GONE);
							mPassWordThree.setVisibility(View.GONE);
							mPassWordFourth.setVisibility(View.GONE);
							break;
						case 1:
							mPassWordOne.setVisibility(View.VISIBLE);
							mPassWordTwo.setVisibility(View.GONE);
							mPassWordThree.setVisibility(View.GONE);
							mPassWordFourth.setVisibility(View.GONE);
							break;
						case 2:
							mPassWordOne.setVisibility(View.VISIBLE);
							mPassWordTwo.setVisibility(View.VISIBLE);
							mPassWordThree.setVisibility(View.GONE);
							mPassWordFourth.setVisibility(View.GONE);
							break;
						case 3:
							mPassWordOne.setVisibility(View.VISIBLE);
							mPassWordTwo.setVisibility(View.VISIBLE);
							mPassWordThree.setVisibility(View.VISIBLE);
							mPassWordFourth.setVisibility(View.GONE);
							break;
						case 4:
							mPassWordOne.setVisibility(View.VISIBLE);
							mPassWordTwo.setVisibility(View.VISIBLE);
							mPassWordThree.setVisibility(View.VISIBLE);
							mPassWordFourth.setVisibility(View.VISIBLE);
							break;
					}
					if (count >= PASSWORD_MINIMUM_LENGTH) {
						mPasswordCheck.setEnabled(true);
						mPassWordDel.setVisibility(View.VISIBLE);
					} else {
						mPasswordCheck.setEnabled(false);
						mPassWordDel.setVisibility(View.GONE);
					}
				}
			});

			mPasswordRetry = (TextView)_v.findViewById(R.id.tv_fragment_remit_password_send_retry_check);
			mPasswordRetry.setText(String.format(getString(R.string.remit_send_password_reset_retry), "5"));
			mPasswordReset = (TextView)_v.findViewById(R.id.tv_fragment_remit_password_send_reset);
			mPasswordReset.setOnClickListener(this);

			mPasswordCheck = _v.findViewById(R.id.btn_fragment_remit_send_password_check);
			mPasswordCheck.setEnabled(false);
			mPasswordCheck.setOnClickListener(this);

			// secure keyboard
//			nfilter = new NFilter(getActivity());
//			NFilterLOG.debug = false;
//			nfilter.setMaxLength(4);
//			nfilter.setNoPadding(true);
//			nfilter.setPlainDataEnable(true);
//			nfilter.setPublicKey("MDIwGhMABBYCBW4RwnBhU0uv1Xdb8mBQcFrYSeEUBBT3JDVvrQWjMuKftOqro+EVBIkyYg==");
//			nfilter.registerReceiver();
			mEditText.setInputType(0);
			PasswordTransformationMethod ptm = new PasswordTransformationMethod();
			mEditText.setTransformationMethod(ptm);
			mEditText.post(new Runnable() {
				@Override
				public void run() {
					onShowKeyboard();
				}
			});


		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void onShowKeyboard() {
//		nfilter.nFilterClose(View.GONE);
//		nfilter.setFieldName(EDIT_TEXT);
//		nfilter.setOnClickListener(new NFilterOnClickListener() {
//			@Override
//			public void onNFilterClick(NFilterTO nFilterTO) {
//				processKeyBoard(nFilterTO ,view);
//			}
//		});
//		//모든 옵션 설정이 끝나면 호출해준다.
//		nfilter.onViewNFilter(NFilter.KEYPADNUM);
	}

//	private void processKeyBoard(NFilterTO nFilterTO, View _v) {
//		if( nFilterTO.getFocus() == NFilter.NEXTFOCUS ){
//			if( new String(  nFilterTO.getFieldName() ).equals(EDIT_TEXT) ){
//				tmpTv = (TextView)_v.findViewById(R.id.nf_num_tmp_editText);
//				tmpTv.setText("");
//			}
//
//			nfilter.nFilterClose(View.GONE); //nFilter 닫기
//		}else if( nFilterTO.getFocus() == NFilter.PREFOCUS ){
//			if( new String(  nFilterTO.getFieldName() ).equals(EDIT_TEXT) ){
//				tmpTv = (TextView)_v.findViewById(R.id.nf_num_tmp_editText);
//				tmpTv.setText("");
//			}
//
//			nfilter.nFilterClose(View.GONE);
//		}else if( nFilterTO.getFocus() == NFilter.DONEFOCUS ){
//			if( new String(  nFilterTO.getFieldName() ).equals(EDIT_TEXT) ){
//				tmpTv = (TextView)_v.findViewById(R.id.nf_num_tmp_editText);
//				tmpTv.setText("");
//			}
//			nfilter.nFilterClose(View.GONE);
//			if (mEditText.getText().length() >= PASSWORD_MINIMUM_LENGTH) {
//				//processCert(mPwd);
//			}
//		}else{
//			if( nFilterTO.getPlainLength() > 0 ){
//				NFilterLOG.i("padding getFieldName", "getFieldName : " + new String(nFilterTO.getFieldName()));
//				//리턴 값을 해당 TextView에 넣는다.
//				if( new String(  nFilterTO.getFieldName() ).equals(EDIT_TEXT) ){
//					mEditText.setText( new String( nFilterTO.getDummyData() ) );
//					NFilterLOG.i("padding getPlainLength", "getPlainLength : " + nFilterTO.getPlainLength());
//					NFilterLOG.i("padding getDummyData", "getDummyData : " + ( nFilterTO.getDummyData()  ));
//					NFilterLOG.i("padding getEncData", "getEncData : " + ( nFilterTO.getEncData()  ));
//					Log.e("nFilterResult", "AES Enc Data : " + nFilterTO.getAESEncData());
//					String encdata_char = nFilterTO.getEncData();
//					int plndatalength_char = nFilterTO.getPlainLength();
//					String dummydata_char = nFilterTO.getDummyData();
//					String plainNormalData_char = nFilterTO.getPlainNormalData();
//					String plaindata_char = nFilterTO.getPlainData();
//					byte[] plainDataByte_char = NFilterUtils.getInstance().decrypt(plaindata_char);
//					mPwd = new String(plainDataByte_char);
//					//Log.e("nFilter", "nfilter plain data : " + plainDataByte_char);
//					//Log.e("nFilter", "nfilter plain data : " + new String(plainDataByte_char));
//					// 입력필드가 가상키보드에 가려서 보이지 않을 경우
//					// 임시로 값을 보여주는 editText
//					// nfilter_char_key_view.xml 32라인에서 직접 수정 가능
//					tmpTv = (TextView)_v.findViewById(R.id.nf_num_tmp_editText);
//					tmpTv.setText( new String( nFilterTO.getDummyData() ) );
//
//					Log.e("nFilter", "nfilter client encrypt data : " + nFilterTO.getClientEncData());
//					byte[] ss = NFilterUtils.getInstance().nSaferDecrypt(nFilterTO.getClientEncData());
//					Log.e("nFilter", "nfilter client encrypt data : " + new String(ss));
//					for(int i=0; i < ss.length; i++) ss[i] = 0;
//					// ================================================================ //
//
//				}
//			}else{
//				//리턴 값을 해당 TextView에 넣는다.
//				if( new String(  nFilterTO.getFieldName() ).equals(EDIT_TEXT) ){
//					mEditText.setText( "" );
//
//					// 입력필드가 가상키보드에 가려서 보이지 않을 경우
//					// 임시로 값을 보여주는 editText
//					// nfilter_char_key_view.xml 32라인에서 직접 수정 가능
//					tmpTv = (TextView)_v.findViewById(R.id.nf_num_tmp_editText);
//					tmpTv.setText("" );
//				}
//			}
//		}
//	}

	private void sendMoneyAccount() {
		DialogUtils.showLoading(getActivity());
		mRequestDataSendAccount.setSendPW(mPwd);

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ResponseRemitSend> call = api.sendMoneyAccount(mRequestDataSendAccount);
		call.enqueue(new Callback<ResponseRemitSend>() {
			@Override
			public void onResponse(Call<ResponseRemitSend> call, Response<ResponseRemitSend> response) {
				DialogUtils.dismissLoading();
				if(response.body() != null && "OK".equals(response.body().getResult())){
					if("A".equals(mRequestDataSendAccount.getSendKind())){
						String temp = TransFormatUtils.transBankCodeToName(mRemitAccount.getCompanyCode())
								+ " " + mRemitAccount.getAccountNumber();
						dialogSendAccountComplete(temp, response.body().getSendDate());
					}
					else {
						if(response.body().getBalance() != null){
							String balance = response.body().getBalance();
							dialogSendAccountComplete(balance, response.body().getSendDate());
						}
					}

				}
				else {
					if ("pwError".equals(response.body().getResult())) {
						Toast.makeText(getActivity(), getString(R.string.membership_text_pwd_not_matched_guide), Toast.LENGTH_SHORT).show();
						if ("5".equals(response.body().getMsg())) {
							mInterfaceEditOrDetail.onEdit();
						} else {
							int failCount = 5 - Integer.parseInt(response.body().getMsg());
							mPasswordRetry.setText(String.format(getString(R.string.remit_send_password_reset_retry), Integer.toString(failCount)));
						}
					} else if("801".equals(response.body().getCode())) {
						Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
					}
					else {

					}
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitSend> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void sendMoneyPhoneNo() {
		DialogUtils.showLoading(getActivity());
		mRequestDataSendPhone.setSendPW(mPwd);

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ResponseRemitSend> call = api.sendMoneyPhoneNo(mRequestDataSendPhone);
		call.enqueue(new Callback<ResponseRemitSend>() {
			@Override
			public void onResponse(Call<ResponseRemitSend> call, Response<ResponseRemitSend> response) {
				DialogUtils.dismissLoading();
				if (response.body() != null && "OK".equals(response.body().getResult())) {
					if ("A".equals(mRequestDataSendPhone.getSendKind())) {
						String temp = TransFormatUtils.transBankCodeToName(mRemitAccount.getCompanyCode())
								+ " " + mRemitAccount.getAccountNumber();
						dialogSendPhoneComplete(temp, response.body().getSendDate());
					} else {
						if(response.body().getBalance() != null){
							String balance = response.body().getBalance();
							dialogSendPhoneComplete(balance, response.body().getSendDate());
						}
					}

				} else {
					if ("pwError".equals(response.body().getResult())) {
						Toast.makeText(getActivity(), getString(R.string.membership_text_pwd_not_matched_guide), Toast.LENGTH_SHORT).show();
						if ("5".equals(response.body().getMsg())) {
							mInterfaceEditOrDetail.onEdit();
						} else {
							int failCount = 5 - Integer.parseInt(response.body().getMsg());
							mPasswordRetry.setText(String.format(getString(R.string.remit_send_password_reset_retry), Integer.toString(failCount)));
						}
					} else if("801".equals(response.body().getCode())) {
						Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
					}
					else {

					}
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitSend> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void chargeWallet() {
		DialogUtils.showLoading(getActivity());
		mRequestDataChangeMoney.setSendPW(mPwd);

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ResponseRemitChargeMoney> call = api.chargeWallet(mRequestDataChangeMoney);
		call.enqueue(new Callback<ResponseRemitChargeMoney>() {
			@Override
			public void onResponse(Call<ResponseRemitChargeMoney> call, Response<ResponseRemitChargeMoney> response) {
				DialogUtils.dismissLoading();
				if(response.body().getResult() != null && response.body().getResult().equalsIgnoreCase(Const.OK)){
					dialogChargeMoneyComplete(mRequestDataChangeMoney.getChargeMoney(), response.body());
				}
				else {
					if ("pwError".equals(response.body().getResult())) {
						Toast.makeText(getActivity(), getString(R.string.membership_text_pwd_not_matched_guide), Toast.LENGTH_SHORT).show();
						if ("5".equals(response.body().getMsg())) {
							mInterfaceEditOrDetail.onEdit();
						} else {
							int failCount = 5 - Integer.parseInt(response.body().getMsg());
							mPasswordRetry.setText(String.format(getString(R.string.remit_send_password_reset_retry), Integer.toString(failCount)));
						}
					} else if("801".equals(response.body().getCode())) {
						Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
					}
					else {

					}
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitChargeMoney> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}


	private void dialogSendPhoneComplete(String mStr1, String mStr2){
		DialogUtils.showDlgBaseRemitSendPhone(getActivity(), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
							Intent intent = new Intent();
							getActivity().setResult(getActivity().RESULT_OK, intent);
							getActivity().finish();
						}

					}
				},
				new InfoDataDialog(R.layout.dialog_inc_remit_send_phone, getString(R.string.common_confirm), "", mStr1, mStr2), mRequestDataSendPhone);
	}


	private void dialogSendAccountComplete(String mStr1, String mStr2){
		DialogUtils.showDlgBaseRemitSendAccount(getActivity(), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
							Intent intent = new Intent();
							getActivity().setResult(getActivity().RESULT_OK, intent);
							getActivity().finish();
						}

					}
				},
				new InfoDataDialog(R.layout.dialog_inc_remit_send_account, getString(R.string.common_confirm), "", mStr1, mStr2), mRequestDataSendAccount);
	}


	private void dialogChargeMoneyComplete(String mStr1, ResponseRemitChargeMoney responseRemitChargeMoney){
		DialogUtils.showDlgBaseRemitChargeMoney(getActivity(), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
							Intent intent = new Intent();
							getActivity().setResult(getActivity().RESULT_OK, intent);
							getActivity().finish();
						}

					}
				},
				new InfoDataDialog(R.layout.dialog_inc_remit_charge, getString(R.string.common_confirm), "", mStr1, ""), responseRemitChargeMoney);
	}
}
