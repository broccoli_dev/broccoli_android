package com.nomadconnection.broccoli.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.igaworks.adbrix.IgawAdbrix;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.membership.LoginActivity;
import com.nomadconnection.broccoli.activity.membership.MembershipActivity;
import com.nomadconnection.broccoli.activity.setting.Level4ActivitySettingCommonTerm;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Dayli.ReqCertRealNameData;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.data.Session.MembershipData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.BroccoliLoginFilter;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class FragmentMembershipAccount extends BaseFragment implements OnClickListener {

    public static final String TAG = FragmentMembershipAccount.class.getSimpleName();

    private static final String SMS_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private static final int MAX_REQUEST_COUNT = 5;

    private ScrollView mScrollView;
    private EditText mEmail;
    private EditText mPwd;
    private EditText mPwdCheck;
    private TextView mEmailResult;
    private TextView mPwdResult;
    private View mNextBtn;
    private Bundle mArgument;
    private DayliApi mApi;
    private boolean isPwdAvailable = false;
    private boolean isPwdMatched = false;
    private boolean isEmailChecked = false;

    public FragmentMembershipAccount() {
        // TODO Auto-generated constructor stub
        setTagName(TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_membership_step_account, null);
        Bundle bundle = getArguments();
        ((BaseActivity) getActivity()).sendTracker(AnalyticsName.UserEmailReg);
        init(view);

        return view;
    }

    void init(View view) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mApi = ServiceGenerator.createServiceDayli(DayliApi.class);
        mArgument = getArguments();
        mScrollView = (ScrollView) view.findViewById(R.id.sv_fragment_membership_top_layout);
        mEmail = (EditText) view.findViewById(R.id.et_fragment_membership_id);
        mPwd = (EditText) view.findViewById(R.id.et_fragment_membership_pwd);
        mPwdCheck = (EditText) view.findViewById(R.id.et_fragment_membership_pwd_check);;
        mEmailResult = (TextView) view.findViewById(R.id.tv_fragment_membership_id_result);
        mPwdResult = (TextView) view.findViewById(R.id.tv_fragment_membership_pwd_result);


        view.findViewById(R.id.ll_fragment_membership_dayli_layout).setVisibility(View.VISIBLE);
        mNextBtn = view.findViewById(R.id.fl_fragment_membership_next);
        mNextBtn.setVisibility(View.VISIBLE);


        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isEmailChecked = false;
                if (!ElseUtils.validateEmail(s.toString())) {
                    mEmailResult.setText("유효하지 않은 이메일 형식입니다.");
                    mEmailResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                    mEmailResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                } else {
                    isEmailChecked = true;
                    mEmailResult.setText(null);
                    mEmailResult.setCompoundDrawablesWithIntrinsicBounds(0, 0,0,0);
                    mEmailResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                }
                checkButtonStatus();
            }
        });

        InputFilter[] filters = new InputFilter[]{new BroccoliLoginFilter(new BroccoliLoginFilter.OnInputListener() {
            @Override
            public void isAllowed(BroccoliLoginFilter.InputResult allowed) {
                isPwdAvailable = false;
                switch (allowed) {
                    case ALLOWED:
                        isPwdAvailable = true;
                        mPwdResult.setText("사용 가능한 비밀번호");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                        break;
                    case NEED_INCLUDE_CHAR:
                    case NEED_INCLUDE_NUMBER:
                        mPwdResult.setText("영문과 숫자를 함께 사용해야 합니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case NOT_ALLOWED_CHARACTER:
                        mPwdResult.setText("!@#$%^&*()만 입력 가능합니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case NOT_ENOUGH_LENGTH:
                        mPwdResult.setText("8~20자리까지 입력 가능합니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case TOO_LONG_LENGTH:
                        mPwdResult.setText("8~20자리까지 입력 가능합니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                }
            }
        })};
        mPwd.setFilters(filters);

        mPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwdCheck = mPwdCheck.getText().toString();
                isPwdMatched = false;
                if (isPwdAvailable && s.length() != 0 && pwdCheck.length() > 0) {
                    if (pwdCheck.equalsIgnoreCase(s.toString())) {
                        isPwdMatched = true;
                        mPwdResult.setText("비밀번호 일치");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    } else {
                        mPwdResult.setText("비밀번호가 일치하지 않습니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    }
                }
                checkButtonStatus();
            }
        });

        mPwdCheck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwd = mPwd.getText().toString();
                isPwdMatched = false;
                if (isPwdAvailable && s.length() != 0) {
                    if (pwd.equalsIgnoreCase(s.toString())) {
                        isPwdMatched = true;
                        mPwdResult.setText("비밀번호 일치");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    } else {
                        mPwdResult.setText("비밀번호가 일치하지 않습니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    }
                }
                checkButtonStatus();
            }
        });

        mNextBtn.setOnClickListener(this);
        mNextBtn.setEnabled(false);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MembershipActivity) getActivity()).setStepGuide(2);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        Intent intent = null;
        int id = view.getId();
        switch (id) {
            case R.id.fl_fragment_membership_next:
                confirm();
                break;
            case R.id.ll_navi_img_txt_else:
                confirm();
                break;
            case R.id.rl_cert_realname_term_first:
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_cert_term_title1));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/name/%ED%86%B5%EC%8B%A0%EC%82%AC%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80.html");
                startActivity(intent);
                break;
            case R.id.rl_cert_realname_term_second:
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_cert_term_title2));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/name/%EA%B0%9C%EC%9D%B8%EC%A0%95%EB%B3%B4%EC%88%98%EC%A7%91%EC%9D%B4%EC%9A%A9%EC%B7%A8%EA%B8%89%EC%9C%84%ED%83%81%EB%8F%99%EC%9D%98.html");
                startActivity(intent);
                break;
            case R.id.rl_cert_realname_term_third:
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_cert_term_title3));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/name/%EA%B3%A0%EC%9C%A0%EC%8B%9D%EB%B3%84%EC%A0%95%EB%B3%B4%EC%B2%98%EB%A6%AC%EB%8F%99%EC%9D%98.html");
                startActivity(intent);
                break;
            case R.id.rl_cert_realname_term_fourth:
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_cert_term_title4));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/name/%EB%B3%B8%EC%9D%B8%ED%99%95%EC%9D%B8%EC%84%9C%EB%B9%84%EC%8A%A4%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80.html");
                startActivity(intent);
                break;
        }
    }

    private void checkButtonStatus() {
        boolean ret = true;
        if (mEmail.getText().length() <= 5) {
            ret = false;
        }
        if (!isEmailChecked) {
            ret = false;
        }
        if (!isPwdMatched) {
            ret = false;
        }
        if (ret) {
            mNextBtn.setEnabled(true);
        } else {
            mNextBtn.setEnabled(false);
        }
    }

    private void showPopup(String str) {
        DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }, new InfoDataDialog(R.layout.dialog_inc_case_1, str, "확인"));
    }

    private void showPopup(String str, DialogInterface.OnClickListener listener) {
        DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), listener, new InfoDataDialog(R.layout.dialog_inc_case_1, str, "확인"));
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void confirm() {
        DialogUtils.showLoading(getActivity());

        if (!isPwdMatched) {
            Toast.makeText(getActivity(), "비밀번호를 다시 확인해 주세요.", Toast.LENGTH_LONG).show();
            return;
        }

        checkEmail();
    }

    private void updateMobileNum() {
        final ReqCertRealNameData realNameData = new ReqCertRealNameData();
        realNameData.setDeviceId(ElseUtils.getDeviceUUID(getActivity()));
        Session.getInstance(getActivity()).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                LoginData data = DbManager.getInstance().getUserInfoDataSet(getActivity(), str, Session.getInstance(getActivity()).getUserId());
                Call<Result> call = mApi.updateMobileNum(data.getDayliToken(), realNameData);
                call.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                            Toast.makeText(getActivity(), "전화번호 변경이 완료되었습니다.", Toast.LENGTH_LONG).show();
                            setUpdateComplete();
                        } else {
                            ElseUtils.network_error(getActivity());
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {
                        ElseUtils.network_error(getActivity());
                    }
                });
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        memberLeaveComplete();
                        break;
                }
            }

            @Override
            public void fail() {

            }
        });
    }

    private void setUpdateComplete() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    private void checkEmail() {
        String email = mEmail.getText().toString();

        Call<Result> call = mApi.dayliIdCheck(email);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    mEmailResult.setText("사용 가능한 이메일");
                    mEmailResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0,0,0);
                    mEmailResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    passOnData();
                } else {
                    Converter<ResponseBody, Result> converter = ServiceGenerator.getRetrofit().responseBodyConverter(Result.class, new Annotation[0]);
                    try {
                        Result result = converter.convert(response.errorBody());
                        if (ErrorMessage.ERROR_714_EXIST_EMAIL.code().equalsIgnoreCase(result.getCode())
                                || ErrorMessage.ERROR_719_QUITE_USER.code().equalsIgnoreCase(result.getCode())) {
                            mEmailResult.setText("사용할 수 없는 이메일입니다.");
                            mEmailResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0,0,0);
                            mEmailResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                            mEmail.requestFocus();
                        } else {
                            ElseUtils.network_error(getActivity());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void checkExistUser() {
        Call<Result> call = mApi.checkJoined(ElseUtils.getDeviceUUID(getActivity()));
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    Result result = response.body();
                    if (result != null) {
                        passOnData();
                    }
                } else {
                    ErrorMessage message = ErrorUtils.parseError(response);
                    switch (message) {
                        case ERROR_712_EXIST_USER:
                            showPopup("이미 가입하신 사용자입니다. 로그인하여 이용해주세요.", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();
                                }
                            });
                            break;
                        case ERROR_719_QUITE_USER:
                            showPopup("탈퇴회원은 탈퇴일로부터 5일 이후 재가입이 가능합니다.");
                            break;
                        case ERROR_1011_CERT_SESSION_EXPIRE:
                        case ERROR_1012_CERT_NOT_FINISH:
                            showPopup("실명인증 시간이 지났습니다. 실명인증을 다시 해주세요.");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    private void passOnData() {
        IgawAdbrix.firstTimeExperience(AnalyticsName.EmailRegComplete);
        String email = mEmail.getText().toString();
        String pwd = mPwd.getText().toString();
        MembershipData membershipData = (MembershipData) mArgument.getSerializable(Const.DATA);
        membershipData.setEmail(email);
        membershipData.setPwd(pwd);
        if (mArgument == null) {
            mArgument = new Bundle();
        }
        mArgument.putSerializable(Const.DATA, membershipData);
        BaseFragment fragment = new FragmentMembershipPinNum();
        fragment.setArguments(mArgument);
        nextStep(fragment);
    }

    public void setFinish() {
        CustomToast.makeText(getContext(), "사용자 실명정보가 업데이트 되었습니다.", Toast.LENGTH_SHORT).show();
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    private void nextStep(BaseFragment fragment) {
        try {
            setCurrentFragment(R.id.fragment_area, fragment, fragment.getTagName());
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
