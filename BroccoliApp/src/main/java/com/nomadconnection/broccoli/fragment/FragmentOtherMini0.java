package com.nomadconnection.broccoli.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Etc.BodyOrgInfo;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentOtherMini0 extends BaseFragment {
	
	private static String TAG = FragmentOtherMini0.class.getSimpleName();

	/* Layout */
	public static EditText mTitle;
	private LinearLayout mBtn1;
	public static TextView mBtn1Title;
	public static String mBtn1TitleId = "";
	private LinearLayout mBtn2;
	public static TextView mBtn2Title;
	public static EditText mCost;
	private LinearLayout mBtn3;
	public static TextView mBtn3Title;
	private LinearLayout mBtn4;
	public static TextView mBtn4Title;

	
	InterfaceEditOrDetail mInterFaceEditOrDetail;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_other_mini_0, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.CalendarAdd);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
	
	
	
	



//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mTitle = (EditText)_v.findViewById(R.id.et_other_mini_inc_title);
			mBtn1 = (LinearLayout)_v.findViewById(R.id.ll_other_mini_inc_btn_1);
			mBtn1Title = (TextView)_v.findViewById(R.id.tv_other_mini_inc_btn_1_title);
			mBtn2 = (LinearLayout)_v.findViewById(R.id.ll_other_mini_inc_btn_2);
			mBtn2Title = (TextView)_v.findViewById(R.id.tv_other_mini_inc_btn_2_title);
			mCost = (EditText)_v.findViewById(R.id.et_other_mini_inc_cost);
			mBtn3 = (LinearLayout)_v.findViewById(R.id.ll_other_mini_inc_btn_3);
			mBtn3Title = (TextView)_v.findViewById(R.id.tv_other_mini_inc_btn_3_title);
			mBtn4 = (LinearLayout)_v.findViewById(R.id.ll_other_mini_inc_btn_4);
			mBtn4Title = (TextView)_v.findViewById(R.id.tv_other_mini_inc_btn_4_title);

			mBtn1.setOnClickListener(BtnClickListener);
			mBtn2.setOnClickListener(BtnClickListener);
			mBtn3.setOnClickListener(BtnClickListener);
			mBtn4.setOnClickListener(BtnClickListener);

			mTitle.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete();
				}
			});

			mCost.addTextChangedListener(new TextWatcher() {

				private String mTextWatcherResult = "";

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (!s.toString().equals(mTextWatcherResult)) {
						if (s.toString().length() == 0) mTextWatcherResult = "";
						else{
							if (s.toString().equals("-")) mTextWatcherResult = "-";
							else mTextWatcherResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
						}
						mCost.setText(mTextWatcherResult);
						mCost.setSelection(mTextWatcherResult.length());
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete();
				}
			});

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mBtn2Title.setText(TransFormatUtils.transDialogRadioTypePosToName("0"));	// 계좌이체
			mBtn4Title.setText(TransFormatUtils.transDialogRadioAlarmPosToName("0"));
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}







//===============================================================================//
// Listener
//===============================================================================//
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
//			v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
			mTitle.clearFocus();
			mCost.clearFocus();
			switch (v.getId()) {

				case R.id.ll_other_mini_inc_btn_1:
					RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
					mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

					DialogUtils.showLoading(getActivity());											// 자산 로딩
					ServiceGenerator.createService(EtcApi.class).getOrgList(mRequestDataByUserId).enqueue(new Callback<ArrayList<BodyOrgInfo>>() {
						@Override
						public void onResponse(Call<ArrayList<BodyOrgInfo>> call, Response<ArrayList<BodyOrgInfo>> response) {
							DialogUtils.dismissLoading();											// 자산 로딩
							final ArrayList<BodyOrgInfo> mTemp;
							if (APP._SAMPLE_MODE_)
								mTemp = AssetSampleTestDataJson.GetSampleData20();
							else mTemp = response.body();

							DialogUtils.showDlgBaseGridOrList(getActivity(), new AdapterView.OnItemClickListener() {
										@Override
										public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
											if (position == 0) {
												mBtn1TitleId = "0";
												mBtn1Title.setText("없음");
//												mCost.setText("0");
//												mBtn3Title.setText("선택");
											}
											else {
												mBtn1TitleId = mTemp.get(position).mBodyOrgId;
												mBtn1Title.setText(mTemp.get(position).mBodyOrgOpponent);
												mCost.setText(String.valueOf(mTemp.get(position).mBodyOrgSum));
												mBtn3Title.setText(TransFormatUtils.transDateForm(mTemp.get(position).mBodyOrgDate.substring(0,8)));
											}
											send_interface_complete();
										}
									},
									new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_L, getResources().getString(R.string.dialog_org_choice_title), getResources().getString(R.string.common_cancel), mTemp), 0, mBtn1Title.getText().toString(), mBtn1TitleId);
						}

						@Override
						public void onFailure(Call<ArrayList<BodyOrgInfo>> call, Throwable t) {
							DialogUtils.dismissLoading();											// 자산 로딩
							final ArrayList<BodyOrgInfo> mTemp;
							if (APP._SAMPLE_MODE_) {
								mTemp = AssetSampleTestDataJson.GetSampleData20();

								DialogUtils.showDlgBaseGridOrList(getActivity(), new AdapterView.OnItemClickListener() {
											@Override
											public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
												if (position == 0) {
													mBtn1TitleId = "0";
													mBtn1Title.setText("없음");
												}
												else {
													mBtn1TitleId = mTemp.get(position).mBodyOrgId;
													mBtn1Title.setText(mTemp.get(position).mBodyOrgOpponent);
												}
											}
										},
										new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_L, getResources().getString(R.string.dialog_org_choice_title), getResources().getString(R.string.common_cancel), mTemp), 0, mBtn1Title.getText().toString(), mBtn1TitleId);
							} else
								ElseUtils.network_error(getActivity());
								//CustomToast.getInstance(getActivity()).showShort("Level3ActivityOtherMiniDetail 서버 통신 실패 - 머니캘린더 청구기관 목록");

						}
					});
					break;

				case R.id.ll_other_mini_inc_btn_2:
					DialogUtils.showDlgBaseOneButton(getActivity(), new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									mBtn2Title.setText(TransFormatUtils.transDialogRadioTypePosToName(String.valueOf(which)));
									send_interface_complete();
								}
							},
							new InfoDataDialog(R.layout.dialog_inc_case_3, getResources().getString(R.string.dialog_pay_type_title), getResources().getString(R.string.common_cancel), mBtn2Title.getText().toString()));
					break;

				case R.id.ll_other_mini_inc_btn_3:
					DatePickerDialog.OnDateSetListener callBack1 = new DatePickerDialog.OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear,
											  int dayOfMonth) {
							String msg = String.format("%04d.%02d.%02d", year, monthOfYear+1, dayOfMonth);
							mBtn3Title.setText(msg);
							send_interface_complete();
							//CustomToast.getInstance(getActivity()).showShort(msg);
						}
					};
					int mYear, mMonth, mDay = 0;
					String mTempNow = TransFormatUtils.getDataFormatWhich(1);
					if (mBtn3Title.getText().toString().equals(getString(R.string.common_input_date_hint))){
						mYear = Integer.valueOf(mTempNow.substring(0,4));
						mMonth = Integer.valueOf(mTempNow.substring(5, 7));
						mDay = Integer.valueOf(mTempNow.substring(8,10));
					}
					else{
						mYear = Integer.valueOf(mBtn3Title.getText().toString().substring(0,4));
						mMonth = Integer.valueOf(mBtn3Title.getText().toString().substring(5,7));
						mDay = Integer.valueOf(mBtn3Title.getText().toString().substring(8,10));
					}

//					new DatePickerDialog(getActivity(), callBack1, mYear, mMonth-1, mDay).show();
					DatePickerDialog pickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, callBack1, mYear, mMonth-1, mDay);
					Date mTempMaxDate = new Date();
					try {
						mTempMaxDate = new SimpleDateFormat("yyyy.MM.dd").parse(TransFormatUtils.getNextMonthDay());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					pickerDialog.getDatePicker().setMaxDate((mTempMaxDate.getTime()));
					pickerDialog.show();
					break;

				case R.id.ll_other_mini_inc_btn_4:
					DialogUtils.showDlgBaseOneButton(getActivity(), new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									mBtn4Title.setText(TransFormatUtils.transDialogRadioAlarmPosToName(String.valueOf(which)));
									send_interface_complete();
								}
							},
							new InfoDataDialog(R.layout.dialog_inc_case_3, getResources().getString(R.string.dialog_alarm_repeat_title), getResources().getString(R.string.common_cancel), mBtn4Title.getText().toString()));
					break;

				default:
					break;
			}

		}
	};


	private void send_interface_complete(){
		if(empty_check()){
			mInterFaceEditOrDetail.onEdit();
		}
		else {
			mInterFaceEditOrDetail.onError();
		}
	}

	private boolean empty_check(){
		if(mTitle.length() == 0 || mBtn1Title.getText().toString().equals(getString(R.string.common_content_select_hint))
				|| mCost.length() == 0 || (mCost.getText() != null && mCost.getText().toString().equalsIgnoreCase("0")) || mBtn3Title.getText().toString().equals(getString(R.string.common_input_sum_hint))){
			return false;
		}
		return true;
	}


}
