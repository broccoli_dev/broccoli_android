package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Asset.AssetEtcInfo;
import com.nomadconnection.broccoli.data.Asset.EditAssetCustom;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.interf.InterfaceFragmentComplete;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAssetOtherCustom extends BaseFragment implements InterfaceFragmentComplete {

	public static final String MODE_ADD = "add";
	public static final String MODE_DETAIL = "detail";

	private final int ADD_NEW_CUSTOM_ASSET = 0;

	private EditText mEditName;
	private EditText mEditDescription;
	private EditText mEditCount;
	private EditText mEditValue;
	private View mInputGuide;
	private String mValueResult = "";

	private AssetEtcInfo mData;

	private PROCESS_MODE mMode = PROCESS_MODE.ADD;

	InterfaceEditOrDetail mInterFaceEditOrDetail;

	private enum PROCESS_MODE {
		ADD,
		DETAIL,
		EDIT
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_asset_other_custom, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.AssetPropertyAdd);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}


	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		Bundle bundle = getArguments();
		if (bundle != null) {
			if (bundle.getBoolean(MODE_DETAIL)) {
				mMode = PROCESS_MODE.DETAIL;
				mData = bundle.getParcelable(Const.DATA);
			}
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View view) {
		boolean bResult = true;
		try {
			mEditName = (EditText) view.findViewById(R.id.et_asset_other_inc_custom_name);
			mEditDescription = (EditText) view.findViewById(R.id.et_asset_other_inc_custom_description);
			mEditCount = (EditText) view.findViewById(R.id.et_asset_other_inc_custom_count);
			mEditValue = (EditText) view.findViewById(R.id.et_asset_other_inc_custom_value);
			mInputGuide = view.findViewById(R.id.tv_asset_other_inc_input_guide);
			mEditName.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			});
			mEditDescription.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete();
				}
			});
			mEditCount.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			});
			mEditValue.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(!s.toString().equals(mValueResult)){
						if (s.toString().length() == 0) mValueResult = "0";
						else mValueResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
						mEditValue.setText(mValueResult);
						mEditValue.setSelection(mValueResult.length());
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			});

		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			switch (mMode) {
				case ADD:
					mEditName.setEnabled(true);
					mEditDescription.setEnabled(true);
					mEditCount.setEnabled(true);
					mEditValue.setEnabled(true);
					mInputGuide.setVisibility(View.VISIBLE);
					break;
				case DETAIL:
					mEditName.setText(mData.type);
					if (mData.description == null || mData.description.isEmpty()) {
						mEditDescription.setHint("");
					} else {
						mEditDescription.setText(mData.description);
					}
					if (mData.quantity == null || mData.quantity.isEmpty()) {
						mEditCount.setHint("");
					} else {
						mEditCount.setText(mData.quantity);
					}
					if (mData.value == null || mData.value.isEmpty()) {
						mEditValue.setHint("0");
					} else {
						mEditValue.setText(mData.value);
					}
					mEditCount.setText(mData.quantity);
					mEditValue.setText(mData.value);
					mEditName.clearFocus();
					mEditDescription.clearFocus();
					mEditCount.clearFocus();
					mEditValue.clearFocus();
					mEditName.setEnabled(false);
					mEditDescription.setEnabled(false);
					mEditCount.setEnabled(false);
					mEditValue.setEnabled(false);
					mInputGuide.setVisibility(View.GONE);
					break;
				case EDIT:
					mEditName.setText(mData.type);
					mEditDescription.setHint("여행 남은돈");
					mEditDescription.setText(mData.description);
					mEditCount.setHint("100 달러");
					mEditCount.setText(mData.quantity);
					mEditValue.setHint("120,000");
					mEditValue.setText(mData.value);
					mEditName.setEnabled(true);
					mEditDescription.setEnabled(true);
					mEditCount.setEnabled(true);
					mEditValue.setEnabled(true);
					mInputGuide.setVisibility(View.VISIBLE);
					break;
			}
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}







//===============================================================================//
// Listener
//===============================================================================//
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
			mEditName.clearFocus();
			mEditDescription.clearFocus();
			mEditCount.clearFocus();
			mEditValue.clearFocus();
		}
	};


	private void send_interface_complete(){
		if(empty_check()){
			mInterFaceEditOrDetail.onEdit();
		}
		else {
			mInterFaceEditOrDetail.onError();
		}
	}

	private boolean empty_check(){
		if(mEditName.getText().length() == 0 || (mEditCount.getText().length() == 0 && mEditValue.getText().length() == 0)){
			return false;
		}
		return true;
	}

	@Override
	public void complete() {
		DialogUtils.showLoading(getActivity());
		EditAssetCustom custom = new EditAssetCustom();
		custom.userId = Session.getInstance(getActivity()).getUserId();
		custom.type = mEditName.getText().toString();
		custom.description = mEditDescription.getText().toString();
		custom.quantity = mEditCount.getText().toString();
		custom.value = mEditValue.getText().toString().replaceAll(",", "");
		if (mMode == PROCESS_MODE.ADD) {
			custom.etcId = ADD_NEW_CUSTOM_ASSET;
		} else {
			custom.etcId = mData.etcId;
		}
		Call<Result> call = ServiceGenerator.createService(AssetApi.class).editCustomAsset(custom);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (response.isSuccessful() && response.body() != null) {
					if (response.body().isOk()) {
						mInterFaceEditOrDetail.onRefresh();
						if (mMode == PROCESS_MODE.EDIT) {
							mData.type = mEditName.getText().toString();
							mData.description = mEditDescription.getText().toString();
							mData.quantity = mEditCount.getText().toString();
							mData.value = mEditValue.getText().toString().replaceAll(",", "");
							mMode = PROCESS_MODE.DETAIL;
							initData();
						}
					} else {
						mMode = PROCESS_MODE.DETAIL;
						initData();
						Toast.makeText(getContext(), "추가 중 문제가 발생하였습니다. 잠시 후 추가해주세요.", Toast.LENGTH_LONG).show();
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ElseUtils.network_error(getActivity());
			}
		});
	}

	@Override
	public void reset() {
		mMode = PROCESS_MODE.DETAIL;
		initData();
	}

	@Override
	public void edit() {
		mMode = PROCESS_MODE.EDIT;
		initData();
	}

	@Override
	public void delete() {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("userId", Session.getInstance(getActivity()).getUserId());
		hashMap.put("etcId", mData.etcId);
		Call<Result> call = ServiceGenerator.createService(AssetApi.class).deleteCustomAsset(hashMap);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null && response.body().isOk()) {
						sendFinishResult();
					} else {
						if (response.body() != null) {
							Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
						}
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void sendFinishResult() {
		Intent intent = new Intent();
		getActivity().setResult(APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_SUCCESS, intent);
		getActivity().finish();
	}
}
