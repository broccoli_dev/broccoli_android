package com.nomadconnection.broccoli.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarCode;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarMarketPrice;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarSubCode;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarYear;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarMakerCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarMarketPrice;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarSubCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarYear;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAssetOther0B extends BaseFragment {
	
	private static String TAG = FragmentAssetOther0B.class.getSimpleName();

	/* Layout Car*/
	private LinearLayout mCarLayout;
	private LinearLayout mCarSpinner1;
	private LinearLayout mCarSpinner2;
	private LinearLayout mCarSpinner3;
	private LinearLayout mCarSpinner4;
	public static TextView mCarInfo1;
	public static TextView mCarInfo2;
	public static TextView mCarInfo3;
	public static TextView mCarInfo4;
	private EditText mCarInfo5;
//	private String mTextWatcherResult = "";

	private ArrayList<String> mCarSpinner1Data;
	private ArrayList<String> mCarSpinner2Data;
	private ArrayList<String> mCarSpinner3Data;
	private ArrayList<String> mCarSpinner4Data;
	private int mCarSpinner1Pos;
	private int mCarSpinner2Pos;
	private int mCarSpinner3Pos;
	private int mCarSpinner4Pos;

	InterfaceEditOrDetail mInterFaceEditOrDetail;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_asset_other_0_b, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.FragmentAssetOther0B);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
	
	
	
	



//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout Car*/
			mCarLayout = (LinearLayout)_v.findViewById(R.id.ll_asset_other_inc_car);
			mCarSpinner1 = (LinearLayout)_v.findViewById(R.id.ll_asset_other_inc_car_spinner_layout_1);
			mCarSpinner2 = (LinearLayout)_v.findViewById(R.id.ll_asset_other_inc_car_spinner_layout_2);
			mCarSpinner3 = (LinearLayout)_v.findViewById(R.id.ll_asset_other_inc_car_spinner_layout_3);
			mCarSpinner4 = (LinearLayout)_v.findViewById(R.id.ll_asset_other_inc_car_spinner_layout_4);
			mCarInfo1 = (TextView)_v.findViewById(R.id.tv_asset_other_inc_car_spinner_text_1);
			mCarInfo2 = (TextView)_v.findViewById(R.id.tv_asset_other_inc_car_spinner_text_2);
			mCarInfo3 = (TextView)_v.findViewById(R.id.tv_asset_other_inc_car_spinner_text_3);
			mCarInfo4 = (TextView)_v.findViewById(R.id.tv_asset_other_inc_car_spinner_text_4);
			mCarInfo5 = (EditText)_v.findViewById(R.id.et_asset_other_inc_car_edittext_1);
//			mCarInfo5.addTextChangedListener(new TextWatcher() {
//				@Override
//				public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
//
//				@Override
//				public void onTextChanged(CharSequence s, int start, int before, int count) {
//					if(!s.toString().equals(mTextWatcherResult)){
//						if (s.toString().length() == 0) mTextWatcherResult = "0";
//						else mTextWatcherResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
//						mCarInfo5.setText(mTextWatcherResult);
//						mCarInfo5.setSelection(mTextWatcherResult.length());
//					}
//				}
//
//				@Override
//				public void afterTextChanged(Editable s) { }
//			});

			mCarSpinner1.setOnClickListener(BtnClickListener);
			mCarSpinner2.setOnClickListener(BtnClickListener);
			mCarSpinner3.setOnClickListener(BtnClickListener);
			mCarSpinner4.setOnClickListener(BtnClickListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mCarSpinner1Data = new ArrayList<String>();
			mCarSpinner2Data = new ArrayList<String>();
			mCarSpinner3Data = new ArrayList<String>();
			mCarSpinner4Data = new ArrayList<String>();

//			Collections.addAll(mCarSpinner1Data, getResources().getStringArray(R.array.spinner_asset_other_3));
//			Collections.addAll(mCarSpinner2Data, getResources().getStringArray(R.array.spinner_asset_other_4));
//			Collections.addAll(mCarSpinner3Data, getResources().getStringArray(R.array.spinner_asset_other_5));
//			Collections.addAll(mCarSpinner4Data, getResources().getStringArray(R.array.spinner_asset_other_4));

//			mCarSpinner1Pos = 0;
//			mCarSpinner2Pos = 0;
//			mCarSpinner3Pos = 0;
//			mCarSpinner4Pos = 0;

//			mCarInfo1.setText("선택");
//			mCarInfo2.setText("선택");
//			mCarInfo3.setText("선택");
//			mCarInfo4.setText("선택");
//			mCarInfo1.setHint("선택");
//			mCarInfo2.setHint("선택");
//			mCarInfo3.setHint("선택");
//			mCarInfo4.setHint("선택");
			mCarInfo5.setEnabled(false);
//			mCarInfo5.setHint("금액 입력");
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}







//===============================================================================//
// Listener
//===============================================================================//
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
//			v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
			switch (v.getId()) {

				case R.id.ll_asset_other_inc_car_spinner_layout_1:
					if (checkStep(1)) return;
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
					mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());
					DialogUtils.showLoading(getActivity());											// 자산 로딩
					ServiceGenerator.createService(AssetApi.class).getCarMakerCode(mRequestDataByUserId).enqueue(new Callback<ArrayList<ResponseAssetCarMakerCode>>() {
						@Override
						public void onResponse(Call<ArrayList<ResponseAssetCarMakerCode>> call, Response<ArrayList<ResponseAssetCarMakerCode>> response) {
							DialogUtils.dismissLoading();											// 자산 로딩
							ArrayList<ResponseAssetCarMakerCode> mTemp;
							mTemp = response.body();
							mCarSpinner1Data.removeAll(mCarSpinner1Data);
							mCarSpinner1Pos = 0;
							for (int i = 0; i < mTemp.size(); i++) {
								mCarSpinner1Data.add(i, mTemp.get(i).mAssetCarMakerCode);
								if (mCarInfo1.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarMakerCode)) mCarSpinner1Pos = i;
							}
							int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
							int mTempLeft1 = (ElseUtils.getRect(v).left);
							DialogUtils.showDlgSpinnerList_220(getActivity(), new Dialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mCarSpinner1Pos = which;
//									v.setBackgroundResource(R.drawable.selector_spinner_bg);
//									v.setStatus(true);
									mCarInfo1.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
									mCarInfo1.setText(mCarSpinner1Data.get(mCarSpinner1Pos));
									valueReset(1);
									dialog.dismiss();
									send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
								}
							}, mCarSpinner1Data, mTempTop1, mTempLeft1, mCarSpinner1Pos, new Dialog.OnDismissListener(){

								@Override
								public void onDismiss(DialogInterface dialog) {
									v.setBackgroundResource(R.drawable.selector_spinner_bg);
								}
							});
						}

						@Override
						public void onFailure(Call<ArrayList<ResponseAssetCarMakerCode>> call, Throwable t) {
							DialogUtils.dismissLoading();											// 자산 로딩
							if (APP._SAMPLE_MODE_) {
								ArrayList<ResponseAssetCarMakerCode> mTemp;
								mTemp = AssetSampleTestDataJson.GetSampleData21();
								mCarSpinner1Data.removeAll(mCarSpinner1Data);
								mCarSpinner1Pos = 0;
								for (int i = 0; i < mTemp.size(); i++) {
									mCarSpinner1Data.add(i, mTemp.get(i).mAssetCarMakerCode);
									if (mCarInfo1.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarMakerCode)) mCarSpinner1Pos = i;
								}
								int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
								int mTempLeft1 = (ElseUtils.getRect(v).left);
								DialogUtils.showDlgSpinnerList_220(getActivity(), new Dialog.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										mCarSpinner1Pos = which;
//										v.setBackgroundResource(R.drawable.selector_spinner_bg);
//										v.setStatus(true);
										mCarInfo1.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
										mCarInfo1.setText(mCarSpinner1Data.get(mCarSpinner1Pos));
										valueReset(1);
										dialog.dismiss();
										send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
									}
								}, mCarSpinner1Data, mTempTop1, mTempLeft1, mCarSpinner1Pos, new Dialog.OnDismissListener(){

									@Override
									public void onDismiss(DialogInterface dialog) {
										v.setBackgroundResource(R.drawable.selector_spinner_bg);
									}
								});
							} else
								ElseUtils.network_error(getActivity());
								//CustomToast.getInstance(getActivity()).showShort("FragmentAssetOther0B 서버 통신 실패 : 차량 제조사 조회");

						}
					});

//					int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
//					int mTempLeft1 = (ElseUtils.getRect(v).left);
//					DialogUtils.showDlgSpinnerList(getActivity(), new Dialog.OnClickListener(){
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							mCarSpinner1Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
//							mCarInfo1.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
//							mCarInfo1.setText(mCarSpinner1Data.get(mCarSpinner1Pos));
//							dialog.dismiss();
//						}
//					}, mCarSpinner1Data, mTempTop1, mTempLeft1, mCarSpinner1Pos);
					break;

				case R.id.ll_asset_other_inc_car_spinner_layout_2:
					if (checkStep(2)) return;
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					DialogUtils.showLoading(getActivity());											// 자산 로딩
					ServiceGenerator.createService(AssetApi.class).getCarCode(new RequestAssetCarCode(Session.getInstance(getActivity()).getUserId(), mCarInfo1.getText().toString())).enqueue(new Callback<ArrayList<ResponseAssetCarCode>>() {
						@Override
						public void onResponse(Call<ArrayList<ResponseAssetCarCode>> call, Response<ArrayList<ResponseAssetCarCode>> response) {
							DialogUtils.dismissLoading();											// 자산 로딩
							ArrayList<ResponseAssetCarCode> mTemp;
							if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData22();
							else mTemp = response.body();
							mCarSpinner2Data.removeAll(mCarSpinner2Data);
							mCarSpinner2Pos = 0;
							for (int i = 0; i < mTemp.size(); i++) {
								mCarSpinner2Data.add(i, mTemp.get(i).mAssetCarCarCode);
								if (mCarInfo2.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarCarCode)) mCarSpinner2Pos = i;
							}
							int mTempTop2 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
							int mTempLeft2 = (ElseUtils.getRect(v).left);
							DialogUtils.showDlgSpinnerList_220(getActivity(), new Dialog.OnClickListener(){
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mCarSpinner2Pos = which;
//									v.setBackgroundResource(R.drawable.selector_spinner_bg);
//									v.setStatus(true);
									mCarInfo2.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
									mCarInfo2.setText(mCarSpinner2Data.get(mCarSpinner2Pos));
									valueReset(2);
									dialog.dismiss();
									send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
								}
							}, mCarSpinner2Data, mTempTop2, mTempLeft2, mCarSpinner2Pos, new Dialog.OnDismissListener(){

								@Override
								public void onDismiss(DialogInterface dialog) {
									v.setBackgroundResource(R.drawable.selector_spinner_bg);
								}
							});

						}

						@Override
						public void onFailure(Call<ArrayList<ResponseAssetCarCode>> call, Throwable t) {
							DialogUtils.dismissLoading();											// 자산 로딩
							if (APP._SAMPLE_MODE_) {
								ArrayList<ResponseAssetCarCode> mTemp;
								mTemp = AssetSampleTestDataJson.GetSampleData22();
								mCarSpinner2Data.removeAll(mCarSpinner2Data);
								mCarSpinner2Pos = 0;
								for (int i = 0; i < mTemp.size(); i++) {
									mCarSpinner2Data.add(i, mTemp.get(i).mAssetCarCarCode);
									if (mCarInfo2.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarCarCode)) mCarSpinner2Pos = i;
								}
								int mTempTop2 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
								int mTempLeft2 = (ElseUtils.getRect(v).left);
								DialogUtils.showDlgSpinnerList_220(getActivity(), new Dialog.OnClickListener(){
									@Override
									public void onClick(DialogInterface dialog, int which) {
										mCarSpinner2Pos = which;
//										v.setBackgroundResource(R.drawable.selector_spinner_bg);
//										v.setStatus(true);
										mCarInfo2.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
										mCarInfo2.setText(mCarSpinner2Data.get(mCarSpinner2Pos));
										valueReset(2);
										dialog.dismiss();
										send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
									}
								}, mCarSpinner2Data, mTempTop2, mTempLeft2, mCarSpinner2Pos, new Dialog.OnDismissListener(){

									@Override
									public void onDismiss(DialogInterface dialog) {
										v.setBackgroundResource(R.drawable.selector_spinner_bg);
									}
								});
							} else
								ElseUtils.network_error(getActivity());
								//CustomToast.getInstance(getActivity()).showShort("FragmentAssetOther0B 서버 통신 실패 : 차량 대표차명 조회");
						}
					});

//					int mTempTop2 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
//					int mTempLeft2 = (ElseUtils.getRect(v).left);
//					DialogUtils.showDlgSpinnerList(getActivity(), new Dialog.OnClickListener(){
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							mCarSpinner2Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
//							mCarInfo2.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
//							mCarInfo2.setText(mCarSpinner2Data.get(mCarSpinner2Pos));
//							dialog.dismiss();
//						}
//					}, mCarSpinner2Data, mTempTop2, mTempLeft2, mCarSpinner2Pos);
					break;

				case R.id.ll_asset_other_inc_car_spinner_layout_3:
					if (checkStep(4)) return;
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					DialogUtils.showLoading(getActivity());											// 자산 로딩
					ServiceGenerator.createService(AssetApi.class).getCarYear(new RequestAssetCarYear(Session.getInstance(getActivity()).getUserId(), mCarInfo1.getText().toString(), mCarInfo2.getText().toString(), mCarInfo4.getText().toString())).enqueue(new Callback<ArrayList<ResponseAssetCarYear>>() {
						@Override
						public void onResponse(Call<ArrayList<ResponseAssetCarYear>> call, Response<ArrayList<ResponseAssetCarYear>> response) {
							DialogUtils.dismissLoading();											// 자산 로딩
							ArrayList<ResponseAssetCarYear> mTemp;
							if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData24();
							else mTemp = response.body();
							mCarSpinner3Data.removeAll(mCarSpinner3Data);
							mCarSpinner3Pos = 0;
							for (int i = 0; i < mTemp.size(); i++) {
								mCarSpinner3Data.add(i, mTemp.get(i).mAssetCarMadeYear);
								if (mCarInfo3.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarMadeYear)) mCarSpinner3Pos = i;
							}
							int mTempTop3 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
							int mTempLeft3 = (ElseUtils.getRect(v).left);
							DialogUtils.showDlgSpinnerList_220(getActivity(), new Dialog.OnClickListener(){
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mCarSpinner3Pos = which;
//									v.setBackgroundResource(R.drawable.selector_spinner_bg);
//									v.setStatus(true);
									mCarInfo3.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
									mCarInfo3.setText(mCarSpinner3Data.get(mCarSpinner3Pos));
									valueReset(4);
									dialog.dismiss();
									send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
									getCarMarketPrice();
								}
							}, mCarSpinner3Data, mTempTop3, mTempLeft3, mCarSpinner3Pos, new Dialog.OnDismissListener(){

								@Override
								public void onDismiss(DialogInterface dialog) {
									v.setBackgroundResource(R.drawable.selector_spinner_bg);
								}
							});

						}

						@Override
						public void onFailure(Call<ArrayList<ResponseAssetCarYear>> call, Throwable t) {
							DialogUtils.dismissLoading();											// 자산 로딩
							if (APP._SAMPLE_MODE_) {
								ArrayList<ResponseAssetCarYear> mTemp;
								mTemp = AssetSampleTestDataJson.GetSampleData24();
								mCarSpinner3Data.removeAll(mCarSpinner3Data);
								mCarSpinner3Pos = 0;
								for (int i = 0; i < mTemp.size(); i++) {
									mCarSpinner3Data.add(i, mTemp.get(i).mAssetCarMadeYear);
									if (mCarInfo3.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarMadeYear)) mCarSpinner3Pos = i;
								}
								int mTempTop3 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
								int mTempLeft3 = (ElseUtils.getRect(v).left);
								DialogUtils.showDlgSpinnerList_220(getActivity(), new Dialog.OnClickListener(){
									@Override
									public void onClick(DialogInterface dialog, int which) {
										mCarSpinner3Pos = which;
//										v.setBackgroundResource(R.drawable.selector_spinner_bg);
//										v.setStatus(true);
										mCarInfo3.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
										mCarInfo3.setText(mCarSpinner3Data.get(mCarSpinner3Pos));
										valueReset(4);
										dialog.dismiss();
										send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
										getCarMarketPrice();
									}
								}, mCarSpinner3Data, mTempTop3, mTempLeft3, mCarSpinner3Pos, new Dialog.OnDismissListener(){

									@Override
									public void onDismiss(DialogInterface dialog) {
										v.setBackgroundResource(R.drawable.selector_spinner_bg);
									}
								});
							} else
								ElseUtils.network_error(getActivity());
								//CustomToast.getInstance(getActivity()).showShort("FragmentAssetOther0B 서버 통신 실패 : 차량 연식 조회");

						}
					});

//					int mTempTop3 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
//					int mTempLeft3 = (ElseUtils.getRect(v).left);
//					DialogUtils.showDlgSpinnerList(getActivity(), new Dialog.OnClickListener(){
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							mCarSpinner3Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
//							mCarInfo3.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
//							mCarInfo3.setText(mCarSpinner3Data.get(mCarSpinner3Pos));
//							dialog.dismiss();
//						}
//					}, mCarSpinner3Data, mTempTop3, mTempLeft3, mCarSpinner3Pos);
					break;

				case R.id.ll_asset_other_inc_car_spinner_layout_4:
					if (checkStep(3)) return;
					v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
					DialogUtils.showLoading(getActivity());											// 자산 로딩
					ServiceGenerator.createService(AssetApi.class).getCarSubCode(new RequestAssetCarSubCode(Session.getInstance(getActivity()).getUserId(), mCarInfo1.getText().toString(), mCarInfo2.getText().toString())).enqueue(new Callback<ArrayList<ResponseAssetCarSubCode>>() {
						@Override
						public void onResponse(Call<ArrayList<ResponseAssetCarSubCode>> call, Response<ArrayList<ResponseAssetCarSubCode>> response) {
							DialogUtils.dismissLoading();											// 자산 로딩
							ArrayList<ResponseAssetCarSubCode> mTemp;
							if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData23();
							else mTemp = response.body();
							mCarSpinner4Data.removeAll(mCarSpinner4Data);
							mCarSpinner4Pos = 0;
							for (int i = 0; i < mTemp.size(); i++) {
								mCarSpinner4Data.add(i, mTemp.get(i).mAssetCarSubCode);
								if (mCarInfo4.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarSubCode)) mCarSpinner4Pos = i;
							}
							int mTempTop4 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
							int mTempLeft4 = (ElseUtils.getRect(v).left);
							DialogUtils.showDlgSpinnerList_220(getActivity(), new Dialog.OnClickListener(){
								@Override
								public void onClick(DialogInterface dialog, int which) {
									mCarSpinner4Pos = which;
//									v.setBackgroundResource(R.drawable.selector_spinner_bg);
//									v.setStatus(true);
									mCarInfo4.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
									mCarInfo4.setText(mCarSpinner4Data.get(mCarSpinner4Pos));
									valueReset(3);
									dialog.dismiss();
									send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
								}
							}, mCarSpinner4Data, mTempTop4, mTempLeft4, mCarSpinner4Pos, new Dialog.OnDismissListener(){

								@Override
								public void onDismiss(DialogInterface dialog) {
									v.setBackgroundResource(R.drawable.selector_spinner_bg);
								}
							});
						}

						@Override
						public void onFailure(Call<ArrayList<ResponseAssetCarSubCode>> call, Throwable t) {
							DialogUtils.dismissLoading();											// 자산 로딩
							if (APP._SAMPLE_MODE_) {
								ArrayList<ResponseAssetCarSubCode> mTemp;
								mTemp = AssetSampleTestDataJson.GetSampleData23();
								mCarSpinner4Data.removeAll(mCarSpinner4Data);
								mCarSpinner4Pos = 0;
								for (int i = 0; i < mTemp.size(); i++) {
									mCarSpinner4Data.add(i, mTemp.get(i).mAssetCarSubCode);
									if (mCarInfo4.getText().toString().equalsIgnoreCase(mTemp.get(i).mAssetCarSubCode)) mCarSpinner4Pos = i;
								}
								int mTempTop4 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
								int mTempLeft4 = (ElseUtils.getRect(v).left);
								DialogUtils.showDlgSpinnerList_220(getActivity(), new Dialog.OnClickListener(){
									@Override
									public void onClick(DialogInterface dialog, int which) {
										mCarSpinner4Pos = which;
//										v.setBackgroundResource(R.drawable.selector_spinner_bg);
//										v.setStatus(true);
										mCarInfo4.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
										mCarInfo4.setText(mCarSpinner4Data.get(mCarSpinner4Pos));
										valueReset(3);
										dialog.dismiss();
										send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
									}
								}, mCarSpinner4Data, mTempTop4, mTempLeft4, mCarSpinner4Pos, new Dialog.OnDismissListener(){

									@Override
									public void onDismiss(DialogInterface dialog) {
										v.setBackgroundResource(R.drawable.selector_spinner_bg);
									}
								});
							} else
								ElseUtils.network_error(getActivity());
								//CustomToast.getInstance(getActivity()).showShort("FragmentAssetOther0B 서버 통신 실패 : 차량 세부차명 조회");
						}
					});

//					int mTempTop4 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
//					int mTempLeft4 = (ElseUtils.getRect(v).left);
//					DialogUtils.showDlgSpinnerList(getActivity(), new Dialog.OnClickListener(){
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							mCarSpinner4Pos = which;
//							v.setBackgroundResource(R.drawable.selector_spinner_bg);
//							v.setStatus(true);
//							mCarInfo4.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
//							mCarInfo4.setText(mCarSpinner4Data.get(mCarSpinner4Pos));
//							dialog.dismiss();
//						}
//					}, mCarSpinner4Data, mTempTop4, mTempLeft4, mCarSpinner4Pos);
					break;

				default:
					break;
			}

		}
	};




//===============================================================================//
// Protocol (MarketPrice : 차량 시세)
//===============================================================================//
	private void getCarMarketPrice(){
		DialogUtils.showLoading(getActivity());											// 자산 로딩
		ServiceGenerator.createService(AssetApi.class).getCarMarketPrice(new RequestAssetCarMarketPrice(Session.getInstance(getActivity()).getUserId(), mCarInfo1.getText().toString(), mCarInfo2.getText().toString(), mCarInfo4.getText().toString(), mCarInfo3.getText().toString())).enqueue(new Callback<ResponseAssetCarMarketPrice>() {
			@Override
			public void onResponse(Call<ResponseAssetCarMarketPrice> call, Response<ResponseAssetCarMarketPrice> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ResponseAssetCarMarketPrice mTemp;
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						mTemp = response.body();
						mCarInfo5.setText(TransFormatUtils.getDecimalFormatRecvString(mTemp.mAssetCarMarketPrice));
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<ResponseAssetCarMarketPrice> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (APP._SAMPLE_MODE_) {
					ResponseAssetCarMarketPrice mTemp;
					mTemp = AssetSampleTestDataJson.GetSampleData25();
					mCarInfo5.setText(TransFormatUtils.getDecimalFormatRecvString(mTemp.mAssetCarMarketPrice));
				} else
					ElseUtils.network_error(getActivity());
					//CustomToast.getInstance(getActivity()).showShort("FragmentAssetOther0B 서버 통신 실패 : 차량 세부차명 조회");

			}
		});
	}





//===============================================================================//
// Reset Init
//===============================================================================//
	/* Reset */
	private void valueReset(int _Step){
		switch (_Step){
			case 1:
//				mCarInfo2.setText("선택");
//				mCarInfo4.setText("선택");
//				mCarInfo3.setText("선택");
				mCarInfo2.setText("");
				mCarInfo4.setText("");
				mCarInfo3.setText("");
				mCarInfo5.setText("");
				break;

			case 2:
//				mCarInfo4.setText("선택");
//				mCarInfo3.setText("선택");
				mCarInfo4.setText("");
				mCarInfo3.setText("");
				mCarInfo5.setText("");
				break;

			case 3:
//				mCarInfo3.setText("선택");
				mCarInfo3.setText("");
				mCarInfo5.setText("");
				break;

			case 4:
				break;

			default:
				break;
		}
	}


//===============================================================================//
// Step Check
//===============================================================================//
	/*Step Check*/
	private Boolean checkStep(int _Step) {
		boolean mResult = false;
		switch (_Step){
			case 1:
				break;

			case 2:
//				if (mCarInfo1.getText().toString().equalsIgnoreCase("선택")) {
				if (mCarInfo1.getText().toString().length() == 0) {
					Toast.makeText(getActivity(), getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(getActivity()).showShort("제조사를 선택해 주세요");
					mResult = true;
				}
				break;

			case 3:
//				if (mCarInfo1.getText().toString().equalsIgnoreCase("선택")) {
				if (mCarInfo1.getText().toString().length() == 0) {
					Toast.makeText(getActivity(), getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(getActivity()).showShort("제조사를 선택해 주세요");
					mResult = true;
				}
//				else if (mCarInfo2.getText().toString().equalsIgnoreCase("선택")) {
				else if (mCarInfo2.getText().toString().length() == 0) {
					Toast.makeText(getActivity(), getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(getActivity()).showShort("대표차명을 선택해 주세요");
					mResult = true;
				}
				break;

			case 4:
//				if (mCarInfo1.getText().toString().equalsIgnoreCase("선택")) {
				if (mCarInfo1.getText().toString().length() == 0) {
					Toast.makeText(getActivity(), getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(getActivity()).showShort("제조사를 선택해 주세요");
					mResult = true;
				}
//				else if (mCarInfo2.getText().toString().equalsIgnoreCase("선택")) {
				else if (mCarInfo2.getText().toString().length() == 0) {
					Toast.makeText(getActivity(), getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(getActivity()).showShort("대표차명을 선택해 주세요");
					mResult = true;
				}
//				else if (mCarInfo4.getText().toString().equalsIgnoreCase("선택")) {
				else if (mCarInfo4.getText().toString().length() == 0) {
					Toast.makeText(getActivity(), getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
					//CustomToast.getInstance(getActivity()).showShort("세부차명을 선택해 주세요");
					mResult = true;
				}
				break;

			default:
				break;
		}


		return mResult;
	}

	private void send_interface_complete(){
		if(empty_check()){
			mInterFaceEditOrDetail.onEdit();
		}
		else {
			mInterFaceEditOrDetail.onError();
		}
	}

	private boolean empty_check(){
		if(mCarInfo1.length() == 0 || mCarInfo2.length() == 0
				|| mCarInfo3.length() == 0 || mCarInfo4.length() == 0 ){
			return false;
		}
		return true;
	}


}
