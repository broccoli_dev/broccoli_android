package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level2ActivitySpendCategory;
import com.nomadconnection.broccoli.activity.Level2ActivitySpendHistory;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendBudget;
import com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendBudgetSetting;
import com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendCardTabList;
import com.nomadconnection.broccoli.adapter.AdtListViewMain2;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorCode;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Scrap.BaseChartData;
import com.nomadconnection.broccoli.data.Spend.MonthlyChartData;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetInfo;
import com.nomadconnection.broccoli.data.Spend.SpendMainData;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.interf.OnRefreshListener;
import com.nomadconnection.broccoli.utils.ChartUtils;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.MyMarkerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FragmentMain2 extends BaseFragment implements InterfaceFragmentInteraction {

	/* Layout */
	public ListView mListView;
	/* Flexible Layout */
	private TextView mSpendTitle;
	private TextView mSpendCost;
	private CombinedChart mChart;
	private View mFlexibleArea;
	private AdtListViewMain2 mAdapter;
	private SpendMainData mSpendMainData = null;
	private ArrayList<SpendMainData> mAlInfoDataList;
	private int mLastXIndex = 0;
	private CombinedData mCombinedData;
	private ArrayList<Entry> mLineEntries;
	private ArrayList<Entry> mCircleEntries;
	private ArrayList<BarEntry> mBarEntries;

	private View mInfoBtnMonth;
	private View mInfoBtnMulti;
	

	/**  Item Click Listener **/
	private OnItemClickListener ItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			if (position != 0 && mAlInfoDataList.get(position-1) == null) {
				return;
			}

			switch (position) {
			case 0:
				//Toast.makeText(getActivity(), "chart", Toast.LENGTH_SHORT).show();
				break;

			case 1:
				Intent spend_history = new Intent(getActivity(), Level2ActivitySpendHistory.class);
				spend_history.putExtra(Const.TOTAL_SPEND_SUM, mSpendMainData.getBudgetInfo().getExpenseSum());
				startActivity(spend_history);
				break;

			case 2:
				((BaseActivity)getActivity()).sendTracker(AnalyticsName.SpendCardBanner);
				Intent bannderIntent = new Intent(getActivity(), Level2ActivitySpendCardTabList.class);
				bannderIntent.putExtra(Const.DATA, mSpendMainData.getBanner());
				bannderIntent.putExtra(Level2ActivitySpendCardTabList.REQUEST_SHOW_RECOMMEND_CARD, 0);
				startActivity(bannderIntent);
				break;

			case 3:
				SpendBudgetInfo info = mSpendMainData.getBudgetInfo();
				if (info.getBudgetSum() != null) {
					if (info.getBudgetTotalSum() == 0 && info.getNextTotalSum() == 0) {
						Intent spend_budget = new Intent(getActivity(), Level2ActivitySpendBudgetSetting.class);
						startActivity(spend_budget);
					} else {
						Intent spend_budget = new Intent(getActivity(), Level2ActivitySpendBudget.class);
						spend_budget.putExtra(Const.SPEND_BUDGET, info);
						startActivity(spend_budget);
					}
				}
				break;

			case 4:
				startActivity(new Intent(getActivity(), Level2ActivitySpendCategory.class));
				break;

			case 5:
				((BaseActivity)getActivity()).sendTracker(AnalyticsName.SpendWayBtn);
				// do nothing
				break;
			case 6:
				Intent spendCard = new Intent(getActivity(), Level2ActivitySpendCardTabList.class);
				spendCard.putExtra(Const.DATA, mSpendMainData.getBanner());
				startActivity(spendCard);
				break;

			default:
				break;
			}
		}
	};

	private View.OnClickListener mOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			int id = v.getId();
			((BaseActivity)getActivity()).sendTracker(AnalyticsName.SpendGraphBtn);
			switch (id) {
				case R.id.fl_flexible_area_main_2_info_btn_1:
					mInfoBtnMonth.setSelected(true);
					mInfoBtnMulti.setSelected(false);
					chart_data_set();
					break;

				case R.id.fl_flexible_area_main_2_info_btn_2:
					mInfoBtnMonth.setSelected(false);
					mInfoBtnMulti.setSelected(true);
					chart_data_set();
					mOnChartValueSelectedListener.onValueSelected(mLineEntries.get(mLineEntries.size()-1), 0, new Highlight(mLineEntries.size()-1,0));
					break;
			}
		}
	};

	private OnChartValueSelectedListener mOnChartValueSelectedListener = new OnChartValueSelectedListener() {
		@Override
		public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
			if (e == null)
				return;
			mLastXIndex = h.getXIndex();
			mCombinedData.setData(ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_1));
			mChart.setData(mCombinedData);
			mChart.getMarkerView().getChildAt(0).setVisibility(mLineEntries.get(h.getXIndex()).getVal() == 0 ? View.GONE:View.VISIBLE);
			mChart.invalidate();
		}

		@Override
		public void onNothingSelected() {
			mChart.highlightValue(mLastXIndex, 0);
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_main2, null);

		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		//get_spend_main_data(v);

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		Main2_refresh();
	}

	public void Main2_refresh(){
		get_spend_main_data();
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			get_spend_main_data();
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Layout */
    		mListView = (ListView)_v.findViewById(R.id.lv_fragment_main_2_listview);
    		mAdapter = new AdtListViewMain2(getActivity(), mAlInfoDataList, new OnRefreshListener() {
				@Override
				public void refresh() {
					get_spend_main_data();
				}
			});

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				mListView.setNestedScrollingEnabled(true);
			}

    		/* btn height만큼 채우기 */
			LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mFlexibleArea = (LinearLayout)inflater.inflate(R.layout.flexible_area_main_2, null);
			mListView.addHeaderView(mFlexibleArea);
			mInfoBtnMonth = mFlexibleArea.findViewById(R.id.fl_flexible_area_main_2_info_btn_1);
			mInfoBtnMulti = mFlexibleArea.findViewById(R.id.fl_flexible_area_main_2_info_btn_2);
			mInfoBtnMonth.setOnClickListener(mOnClickListener);
			mInfoBtnMulti.setOnClickListener(mOnClickListener);

			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(ItemClickListener);

			/* Flexible Layout */
			mSpendTitle = (TextView)_v.findViewById(R.id.tv_fragment_main_2_title);
			mSpendCost = (TextView)_v.findViewById(R.id.tv_fragment_main_2_cost);
			mSpendCost.setTypeface(FontClass.setFont(getActivity()));

			/* Chart */
			mChart = (CombinedChart) _v.findViewById(R.id.chart_bar_and_line_chart);
			mChart.setDescription("");
			mChart.setNoDataText("");

			mInfoBtnMulti.setSelected(true);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}







//===============================================================================//
// Listener
//===============================================================================//	

//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			Calendar calendar = Calendar.getInstance();
			int monthly = calendar.get(calendar.MONTH)+1;
			String title = String.format(getString(R.string.spend_category_month), Integer.toString(monthly));

			mSpendTitle.setText(title);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

//===============================================================================//
// reset data
//===============================================================================//
	private void reset_data(){
		mAdapter.notifyDataSetChanged();
		if(mSpendMainData.getBudgetInfo() != null && mSpendMainData.getBudgetInfo().getExpenseSum() != null){
			long sumCost = Long.parseLong(mSpendMainData.getBudgetInfo().getExpenseSum());
			mSpendCost.setText(ElseUtils.getDecimalFormat(sumCost));
		}
	}



//===============================================================================//
// chart data set
//===============================================================================//	
	private void chart_data_set() {
		boolean isSingleMonth = true;
		int chartExistLength = -1;
		float budgetValue = 0;
		float totalValue = 0;
		String budgetText = null;
		float averageValue = 0;
		long maxValue = 0;
		String averageText = null;
		String[] chartXValues = new String[0];

		if (mInfoBtnMonth.isSelected()) {// 1개월
			isSingleMonth = true;
		} else {// 6개월 평균
			isSingleMonth = false;
		}

		if (isSingleMonth) {
			if (mSpendMainData.getChartMonthlyProperty() != null) {
				chartExistLength = mSpendMainData.getChartMonthlyProperty().size() - 1;
			}
			if(mSpendMainData.getBudgetInfo()!= null && mSpendMainData.getBudgetInfo().getBudgetSum() != null) {
				totalValue = Float.valueOf(mSpendMainData.getBudgetInfo().getExpenseSum());
				budgetValue = Float.valueOf(mSpendMainData.getBudgetInfo().getBudgetSum());
				budgetText = String.format(getString(R.string.spend_sum_budget), ElseUtils.getDecimalFormat((int) budgetValue));
			}
			chartXValues = ChartUtils.DayOfMonthly(LastDay());
		} else {
			List<MonthlyChartData> list = mSpendMainData.getMonthlyExpense();
			if (list != null) {
				long monthlySumValue = 0;
				int validCount = 0;
				String[] xValues = new String[list.size()];
				chartExistLength = list.size() - 1;
				for (int i=0; i < list.size(); i++) {
					MonthlyChartData data = list.get(i);
					if (data.getMonthlySum() > 0) {
						if (data.getMonthlySum() > maxValue) {
							maxValue = data.getMonthlySum();
						}
						validCount++;
						monthlySumValue += data.getMonthlySum();
					}
					String date = data.getMonthlyDate();
					if (date != null && !TextUtils.isEmpty(date)) {
						xValues[i] = date.substring(4);
					}
				}
				if (monthlySumValue > 0) {
					monthlySumValue = monthlySumValue/validCount;
				}
				averageValue = monthlySumValue;
				double fixValue = Math.floor(monthlySumValue * 10d) / 10d;
				averageText = String.format(getString(R.string.spend_average), ElseUtils.getDecimalFormat((long) fixValue));
				chartXValues = xValues;
			}
		}

		/* Spend Data */
		mCombinedData = new CombinedData(chartXValues);

		mChart.setDescription("");
		mChart.setNoDataText("");
		mChart.setBackgroundColor(Color.TRANSPARENT);
		mChart.setDrawGridBackground(false);
		mChart.setTouchEnabled(false);
		mChart.setScaleEnabled(false);
		mChart.setDrawBarShadow(false);

		// draw bars behind lines
		mChart.setDrawOrder(ChartUtils.mDrawOrder);

		YAxis rightAxis = mChart.getAxisRight();
		if (isSingleMonth) {
			if(budgetValue != 0) {
				LimitLine ll1 = new LimitLine(budgetValue, budgetText);
				ll1.setLineWidth(1f);
				ll1.setLineColor(Color.rgb(255, 255, 255));
				ll1.enableDashedLine(3f, 3f, 0f);
				if(totalValue < budgetValue){
					ll1.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_BOTTOM);
					rightAxis.setAxisMaxValue(budgetValue);
				}
				else {
					ll1.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
					rightAxis.resetAxisMaxValue();
				}
				ll1.setTextColor(Color.rgb(255, 255, 255));
				ll1.setTextSize(13f);

				if(rightAxis.getLimitLines() != null){
					rightAxis.removeAllLimitLines();
				}

				rightAxis.addLimitLine(ll1);
				rightAxis.setDrawGridLines(false);
			}
			else {
				if(rightAxis.getLimitLines() != null){
					rightAxis.removeAllLimitLines();
				}
			}
		} else {
			LimitLine ll1 = new LimitLine(averageValue, averageText);
			ll1.setLineWidth(1f);
			ll1.setLineColor(Color.rgb(255, 255, 255));
			ll1.enableDashedLine(3f, 3f, 0f);
			ll1.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
			ll1.setTextColor(Color.rgb(255, 255, 255));
			ll1.setTextSize(13f);

			if(rightAxis.getLimitLines() != null){
				rightAxis.removeAllLimitLines();
			}

			rightAxis.setAxisMaxValue(maxValue*1.1f);

			rightAxis.addLimitLine(ll1);
			rightAxis.setDrawGridLines(false);
		}

		YAxis leftAxis = mChart.getAxisLeft();
		leftAxis.setDrawGridLines(false);
		if (isSingleMonth) {
			if(totalValue < budgetValue){
				leftAxis.setAxisMaxValue(budgetValue);
			}
			else{
				leftAxis.resetAxisMaxValue();
			}
		} else {
			leftAxis.setAxisMaxValue(maxValue*1.1f);
		}

		XAxis xAxis = mChart.getXAxis();
		xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
		xAxis.setDrawAxisLine(false);
		xAxis.setDrawGridLines(false);
		xAxis.setSpaceBetweenLabels(1);
		xAxis.setTextSize(11f);
		xAxis.setTextColor(Color.argb(90, 255, 255, 255));


		mLineEntries = new ArrayList<>();
		mCircleEntries = new ArrayList<>();
		mBarEntries = new ArrayList<>();

		if(chartExistLength !=  -1){
			for(int i = 0; i < chartXValues.length; i++){
				if (isSingleMonth) {
					if(chartExistLength < i){
						int last = mSpendMainData.getChartMonthlyProperty().size() - 1;
						BaseChartData mChartLastData = mSpendMainData.getChartMonthlyProperty().get(last);
						float mAddSum = Float.valueOf(mChartLastData.sum);
						mBarEntries.add(new BarEntry(mAddSum, i));
					} else {
						BaseChartData mChartData = mSpendMainData.getChartMonthlyProperty().get(i);
						float mSum = Float.valueOf(mChartData.sum);
						mLineEntries.add(new Entry(mSum, i));
						if (chartExistLength == i) {
							mCircleEntries.add(new Entry(mSum, i));
						}
						mBarEntries.add(new BarEntry(mSum, i));
					}
				} else {
					MonthlyChartData mChartData = mSpendMainData.getMonthlyExpense().get(i);
					float mSum = Float.valueOf(mChartData.getMonthlySum());
					mLineEntries.add(new Entry(mSum, i));
					if (chartExistLength == i) {
						mCircleEntries.add(new Entry(mSum, i));
					}
					mBarEntries.add(new BarEntry(mSum, i));
				}

			}

			LineData lineData = null;
			BarData barData = null;
			if (isSingleMonth) {
				lineData = ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_2);
				mChart.setTouchEnabled(false);
				mChart.setMarkerView(null);
				mChart.setOnChartValueSelectedListener(null);
				barData = ChartUtils.generateBarData(mBarEntries, APP.MAIN_LIST_CHART_USE_2);
				barData.setHighlightEnabled(false);
			} else {
				lineData = ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_1);
				MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
				mv.setChart(mChart);
				mChart.setTouchEnabled(true);
				mChart.setMarkerView(mv);
				mChart.setOnChartValueSelectedListener(mOnChartValueSelectedListener);
				barData = ChartUtils.generateBarData(mBarEntries, APP.MAIN_LIST_CHART_USE_1);
				barData.setHighlightEnabled(true);
			}
			if (mCircleEntries.size() > 0) {
				LineDataSet set = new LineDataSet(mCircleEntries, "Circle DataSet");
				set.setColor(Color.argb((int) (255 * 0), 255, 255, 255));
				set.setLineWidth(1.5f);
				set.setCircleSize(5f);
				set.setCircleColor(Color.argb((int) (255 * 1), 255, 255, 255));
				set.setFillColor(Color.rgb(255, 255, 255));
				set.setCircleColorHole(Color.argb((int) (255 * 1), 203, 124, 229));
				set.setDrawCircleHole(true);
				set.setDrawCircles(true);
				set.setDrawHighlightIndicators(false);
				set.setDrawValues(false);
				lineData.addDataSet(set);
			}
			mCombinedData.setData(lineData);
			mCombinedData.setData(barData);

		}

		// hide
		mChart.getAxisRight().setEnabled(false);
		mChart.getAxisLeft().setEnabled(false);
		mChart.getLegend().setEnabled(false);
		mChart.animateY(1500);

		mChart.setData(mCombinedData);
		mChart.notifyDataSetChanged();
		mChart.invalidate();
		if (isSingleMonth) {
			mLastXIndex = mLineEntries.size() - 1;
		}
	}
	
	
	
//===============================================================================//
// Get Monthly last day
//===============================================================================//
	private int LastDay(){
		Calendar calendar = Calendar.getInstance();

		int year = calendar.get(calendar.YEAR);
		int month = calendar.get(calendar.MONTH);
		calendar.set(year, month, 1);

		int DayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return DayOfMonth;
	}




//===============================================================================//
// Get Spend main check
//===============================================================================//
	private void get_spend_main_data(){
		DialogUtils.showLoading(getActivity());
		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<SpendMainData> call = api.getSpendMain();
		call.enqueue(new Callback<SpendMainData>() {
			@Override
			public void onResponse(Call<SpendMainData> call, Response<SpendMainData> response) {
				DialogUtils.dismissLoading();
				if(getActivity() == null || getActivity().isFinishing()){
					return;
				}

				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						mSpendMainData = response.body();
						String code = mSpendMainData.getCode();
						if (mSpendMainData != null && (code == null || code.isEmpty())) {
							if(mSpendMainData != null){
								mAlInfoDataList = new ArrayList<SpendMainData>();
								mAlInfoDataList.add(mSpendMainData);
								mAlInfoDataList.add(mSpendMainData);
								mAlInfoDataList.add(mSpendMainData);
								mAlInfoDataList.add(mSpendMainData);
								mAlInfoDataList.add(mSpendMainData);
								mAlInfoDataList.add(mSpendMainData);

								mAdapter.setData(mAlInfoDataList);
								chart_data_set();
								reset_data();
							}
						} else {
							if (ErrorMessage.ERROR_716_TOKEN_EXPIRE.code().equalsIgnoreCase(mSpendMainData.getCode())
									|| ErrorMessage.ERROR_10007_SESSION_NOT_FOUND.code().equalsIgnoreCase(mSpendMainData.getCode())
									|| ErrorMessage.ERROR_10008_PWD_CHANGED_SESSION_EXPIRED.code().equalsIgnoreCase(mSpendMainData.getCode())
									|| ErrorMessage.ERROR_20003_MISMATCH_ACCESS_TOKEN.code().equalsIgnoreCase(mSpendMainData.getCode())) {
								memberLeaveComplete();
							} else {
								ElseUtils.network_error(getActivity());
							}
						}
					} else {
						Retrofit retrofit = ServiceGenerator.getRetrofit();
						Converter<ResponseBody, SpendMainData> converter = retrofit.responseBodyConverter(SpendMainData.class, new Annotation[0]);
						try {
							mSpendMainData = converter.convert(response.errorBody());
							if (ErrorCode.ERROR_CODE_701_LOGIN_INCORRECT_DEVICE.equalsIgnoreCase(mSpendMainData.getCode())) {
								memberLeaveComplete();
							} else {
								ElseUtils.network_error(getActivity());
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<SpendMainData> call, Throwable t) {
				ElseUtils.network_error(getActivity());
			}
		});
	}

	public void refreshAdt(String _sort){
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void reset(boolean flag) {
		if (mListView != null) {
			mListView.setSelection(0);
		}
	}

	@Override
	public boolean canScrollUp() {
		boolean ret = false;
		if (mListView != null) {
			ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
		}
		return ret;
	}

	@Override
	public boolean canScrollDown() {
		return false;
	}
}
