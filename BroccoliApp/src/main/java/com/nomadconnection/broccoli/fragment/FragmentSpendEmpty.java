package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;

public class FragmentSpendEmpty extends BaseFragment {
	
	private static String TAG = "FragmentSpendEmpty";

	private LinearLayout mEmpty;
	private ImageView mEmptyIcon;
	private TextView mEmptyTitle;
	
	
	InterfaceEditOrDetail mInterFaceEditOrDetail;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_spend_empty, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
	
	
	
	



//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */
			
			/* Layout 선언 */
			mEmpty = (LinearLayout)_v.findViewById(R.id.ll_empty_layout_layout);
			mEmptyIcon = (ImageView)_v.findViewById(R.id.iv_empty_layout_icon);
			mEmptyTitle = (TextView)_v.findViewById(R.id.tv_empty_layout_txt);
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			/* Default Value */
			mEmpty.setVisibility(View.VISIBLE);
			mEmptyTitle.setText("사용 내역이 없습니다.");
			mEmptyIcon.setImageResource(R.drawable.empty_icon_nohistory);
		} catch (Exception e) {
			bResult = false;
			if (APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}
}
