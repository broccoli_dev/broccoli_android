package com.nomadconnection.broccoli.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.setting.Level4ActivitySettingCommonTerm;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.utils.DialogUtils;

import java.util.ArrayList;

/**
 * Created by YelloHyunminJang on 2016. 12. 13..
 */

public class FragmentMembershipUpdateStep2 extends BaseFragment implements View.OnClickListener {

    private boolean flagFirst, flagSecond, flagThird, flagFourth, flagAll;
    private LinearLayout checkFirst, checkSecond, checkThird, checkFourth, checkAll;
    private RelativeLayout moveFirst, moveSecond, moveThird, moveFourth;
    private Bundle mArguments;
    private ArrayList<Integer> mCheckAgree;
    private View mBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_membership_update_step2, null);
        ((BaseActivity)getActivity()).sendTracker(AnalyticsName.ExUserTermAgree);
        init(view);

        return view;
    }


    private void init(View view) {

        if (mArguments == null) {
            mArguments = new Bundle();
        }

        if(mCheckAgree == null){
            mCheckAgree = new ArrayList<>();
        }

        mBtn = view.findViewById(R.id.btn_confirm);
        mBtn.setEnabled(false);
        mBtn.setOnClickListener(this);
        checkAll = (LinearLayout) view.findViewById(R.id.ll_fragment_membership_all);
        checkAll.setOnClickListener(this);
        checkFirst = (LinearLayout) view.findViewById(R.id.ll_fragment_membership_first);
        checkFirst.setOnClickListener(this);
        checkSecond = (LinearLayout) view.findViewById(R.id.ll_fragment_membership_second);
        checkSecond.setOnClickListener(this);
        checkThird = (LinearLayout) view.findViewById(R.id.ll_fragment_membership_third);
        checkThird.setOnClickListener(this);
        checkFourth = (LinearLayout) view.findViewById(R.id.ll_fragment_membership_fourth);
        checkFourth.setOnClickListener(this);
        moveFirst = (RelativeLayout) view.findViewById(R.id.rl_fragment_membership_first);
        moveFirst.setOnClickListener(this);
        moveSecond = (RelativeLayout) view.findViewById(R.id.rl_fragment_membership_second);
        moveSecond.setOnClickListener(this);
        moveThird = (RelativeLayout) view.findViewById(R.id.rl_fragment_membership_third);
        moveThird.setOnClickListener(this);
        moveFourth = (RelativeLayout) view.findViewById(R.id.rl_fragment_membership_fourth);
        moveFourth.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!(flagFirst | flagSecond | flagThird | flagFourth)){
            flagFirst = flagSecond = flagThird = flagFourth = flagAll = false;
        }
    }

    private void nextStep(BaseFragment fragment) {
        try {
            setCurrentFragment(R.id.ll_activity_membership_update_fragment_area, fragment, fragment.getTagName());
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Intent intent = null;
        switch (id) {
            case R.id.ll_fragment_membership_first:
                flagFirst = !flagFirst;
                if (flagFirst) {
                    checkFirst.setSelected(true);
                } else {
                    checkFirst.setSelected(false);
                }
                allCheckFlag();
                checkFlag();
                break;
            case R.id.ll_fragment_membership_second:
                flagSecond = !flagSecond;
                if (flagSecond) {
                    checkSecond.setSelected(true);
                } else {
                    checkSecond.setSelected(false);
                }
                allCheckFlag();
                checkFlag();
                break;
            case R.id.ll_fragment_membership_third:
                flagThird = !flagThird;
                if (flagThird) {
                    checkThird.setSelected(true);
                } else {
                    checkThird.setSelected(false);
                }
                allCheckFlag();
                checkFlag();
                break;
            case R.id.ll_fragment_membership_fourth:
                flagFourth = !flagFourth;
                if (flagFourth) {
                    checkFourth.setSelected(true);
                } else {
                    checkFourth.setSelected(false);
                }
                allCheckFlag();
                checkFlag();
                break;
            case R.id.rl_fragment_membership_first:
                ((BaseActivity)getActivity()).sendTracker(AnalyticsName.SettingGuideDayTerm);
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_text_first_subject1));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/DAYLI%20PASS%20%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80_full.html");
                startActivity(intent);
                break;
            case R.id.rl_fragment_membership_second:
                ((BaseActivity)getActivity()).sendTracker(AnalyticsName.SettingGuideTerm);
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_text_first_subject2));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/%EB%B8%8C%EB%A1%9C%EC%BD%9C%EB%A6%AC%20%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80_full.html");
                startActivity(intent);
                break;
            case R.id.rl_fragment_membership_third:
                ((BaseActivity)getActivity()).sendTracker(AnalyticsName.GuidePersonalAgree);
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_text_first_subject3));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/DAYLI%20PASS%20%EB%B0%8F%20%EB%B8%8C%EB%A1%9C%EC%BD%9C%EB%A6%AC%20%EA%B0%9C%EC%9D%B8%EC%A0%95%EB%B3%B4%20%EC%88%98%EC%A7%91%20%EB%B0%8F%20%EC%9D%B4%EC%9A%A9%EB%8F%99%EC%9D%98_summary.html");
                startActivity(intent);
                break;
            case R.id.rl_fragment_membership_fourth:
                ((BaseActivity)getActivity()).sendTracker(AnalyticsName.GuideSendAgree);
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_text_first_subject4));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/%EA%B0%9C%EC%9D%B8%EC%A0%95%EB%B3%B4%20%EC%A0%9C3%EC%9E%90%20%EC%A0%9C%EA%B3%B5%20%EB%8F%99%EC%9D%98_summary.html");
                startActivity(intent);
                break;
            case R.id.ll_fragment_membership_all:
                flagAll = !flagAll;
                if (flagAll) {
                    checkAll.setSelected(true);
                } else {
                    checkAll.setSelected(false);
                }
                toggleCheck();
                break;
            case R.id.btn_confirm:
                BaseFragment fragment = new FragmentMembershipUpdateStep3();
                mArguments.putIntegerArrayList(Const.TERMS_AGREE, mCheckAgree);
                fragment.setArguments(mArguments);
                nextStep(fragment);
                break;
        }
    }

    private void checkFlag() {
        check_agree();	// 약관 동의 서버에 보낼 데이터 set
        if (flagFirst & flagSecond & flagThird) { //& flagThird & flagFourth
            mBtn.setEnabled(true);
        } else {
            mBtn.setEnabled(false);
        }
    }

    private void allCheckFlag() {
        if (flagFirst & flagSecond & flagThird & flagFourth){
            flagAll = true;
            checkAll.setSelected(true);
        } else {
            flagAll = false;
            checkAll.setSelected(false);
        }
    }

    private void toggleCheck() {
        if (flagFirst & flagSecond & flagThird & flagFourth) {
            flagFirst = flagSecond = flagThird = flagFourth = false;
            checkFirst.setSelected(false);
            checkSecond.setSelected(false);
            checkThird.setSelected(false);
            checkFourth.setSelected(false);
            mBtn.setEnabled(false);
        } else {
            flagFirst = flagSecond = flagThird = flagFourth = true;
            checkFirst.setSelected(true);
            checkSecond.setSelected(true);
            checkThird.setSelected(true);
            checkFourth.setSelected(true);
            mBtn.setEnabled(true);
        }
        check_agree();	// 약관 동의 서버에 보낼 데이터 set
    }

    private void Dialog_Notice(){
        DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
                }
            }
        }, new InfoDataDialog(R.layout.dialog_inc_case_8, null, getResources().getString(R.string.dialog_broccoli_init_button)));
    }

    private void check_agree(){
        if(flagFirst& flagSecond & flagThird & flagFourth){
            mCheckAgree.clear();
            mCheckAgree.add(0,1);
            mCheckAgree.add(1,2);
            mCheckAgree.add(2,3);
            mCheckAgree.add(2,4);
        } else if(flagFirst & flagSecond & flagThird) {
            mCheckAgree.clear();
            mCheckAgree.add(0,1);
            mCheckAgree.add(1,2);
            mCheckAgree.add(2,3);
        }
    }
}
