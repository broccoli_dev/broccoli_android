package com.nomadconnection.broccoli.fragment.remit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.CertListActivity;
import com.nomadconnection.broccoli.activity.remit.RemitRegisterActivity;
import com.nomadconnection.broccoli.activity.setting.Level3ActivitySettingFinancialReg;
import com.nomadconnection.broccoli.adapter.remit.AdtListViewRemitBankList;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Remit.RemitBankDetail;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Scrap.SelectBankData;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentRemitRegisterAccount extends BaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener{
	
	private static String TAG = FragmentRemitRegisterAccount.class.getSimpleName();

	InterfaceEditOrDetail mInterfaceEditOrDetail;

	/* Layout */
	private Bundle mArgument;
	private ListView mListView;
	private LinearLayout mEmpityView;
	private ArrayList<RemitBankDetail> mAlInfoDataList;
	private RemitBankDetail mSelectItem = null;
	private SelectBankData mSelectBankData = null;
	private AdtListViewRemitBankList mAdapter;
	private boolean isCert = false;
	private String mPwd;
	private String mCert;
	private String mValidDate;
	private View mNextBtn;
	private TextView mNextText;
	private Button mCertifyAccount;
	private LinearLayout mAccountGuide;
	private int mPossibleCount = 0;

	
//	@Override
//	public void onAttach(Context context) {
//		super.onAttach(context);
//		// This makes sure that the container activity has implemented
//		// the callback interface. If not, it throws an exception
//		try {
//			mInterfaceEditOrDetail = (InterfaceEditOrDetail) context;
//		} catch (ClassCastException e) {
//			throw new ClassCastException(context.toString()
//					+ " must implement InterfaceEditOrDetail");
//		}
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_remit_register_account, null);
//		((BaseActivity)getActivity()).sendTracker(AnalyticsName.FragmentMembershipFinancial);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		return v;
	}

	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		if(mArgument.getBoolean(Const.REMIT_REGISTER)){
			((RemitRegisterActivity)getActivity()).setStepGuide(0);
		}
//		mSelectItem = null;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
			case R.id.btn_fragment_remit_register_bank:
				if(mAlInfoDataList == null || mAlInfoDataList.size() == mPossibleCount || mAlInfoDataList.size() == 0){
					Intent setting_financial = new Intent(getActivity(), Level3ActivitySettingFinancialReg.class);
					startActivity(setting_financial);
				}
				else {
					startCert();
				}
				break;
			case R.id.fl_fragment_remit_register_first_next:
				if(mAlInfoDataList.size() != 0){
					confirm();
				}
				else {
					Intent setting_financial = new Intent(getActivity(), Level3ActivitySettingFinancialReg.class);
					startActivity(setting_financial);
				}
				break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mSelectItem = mAlInfoDataList.get(position);
		if(mSelectItem != null){
			mSelectBankData = new SelectBankData();
			mSelectBankData.setType(SelectBankData.TYPE_BANK);
			mSelectBankData.setTypeName(getString(R.string.membership_text_second_cert_bank));
			mSelectBankData.setName(mSelectItem.getCompanyName());
			mSelectBankData.setCode(mSelectItem.getCompanyCode());
		}
		mAdapter.setSelectItem(position);
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			isCert = true;
			if (data != null) {
				mPwd = data.getStringExtra(Const.CERT_PWD);
				mCert = data.getStringExtra(Const.CERT_NAME);
				mValidDate = data.getStringExtra(Const.CERT_VALIDDATE);
			}
			mNextBtn.setEnabled(true);
		}
	}

	private void startCert() {
		ArrayList<SelectBankData> selectedList = new ArrayList<SelectBankData>();
		if(mSelectBankData != null){
			selectedList.add(mSelectBankData);
		}

		if (selectedList.size() > 0) {
			Intent intent = new Intent(getActivity(), CertListActivity.class);
			intent.putExtra(Const.DATA, selectedList);
			startActivityForResult(intent, 0);
		} else {
			Toast.makeText(this.getActivity(), R.string.setting_text_warning_not_selected, Toast.LENGTH_LONG).show();
		}
	}

//	private void remitRegisterConfirm() {
//		Intent mRemitPossibleBank = new Intent(getActivity(), RemitPossibleBankActivity.class);
//		startActivity(mRemitPossibleBank);
//	}

	private void confirm() {
		BaseFragment fragment;
		if(mArgument.getBoolean(Const.REMIT_REGISTER)){
			fragment = new FragmentRemitRegisterPassword();
			Bundle bundle = new Bundle();
			bundle.putBoolean(Const.REMIT_REGISTER, true);
			bundle.putSerializable(Const.DATA, mSelectItem);
			fragment.setArguments(bundle);
			nextStep(fragment);
		}
		else {
			fragment = new FragmentRemitRegisterARS();
			Bundle bundle = new Bundle();
			bundle.putBoolean(Const.REMIT_REGISTER, false);
			bundle.putSerializable(Const.DATA, mSelectItem);
			fragment.setArguments(bundle);
			nextStep(fragment);
		}
	}

	private void nextStep(BaseFragment fragment) {
		try {
			if(mArgument.getBoolean(Const.REMIT_REGISTER)){
				setCurrentFragment(R.id.fragment_remit_area, fragment, fragment.getTagName());
			}
			else {
//				mInterfaceEditOrDetail.onEdit();
				setCurrentFragment(R.id.fl_activity_remit_account_edit_container, fragment, fragment.getTagName());
			}

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			/* getExtra */
			get_bank_list();
			mArgument = getArguments();
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try {
			/* Layout */
			mCertifyAccount = (Button)_v.findViewById(R.id.btn_fragment_remit_register_bank);
			mCertifyAccount.setOnClickListener(this);
			mAccountGuide = (LinearLayout)_v.findViewById(R.id.ll_fragment_remit_register_account);
			mNextText = (TextView)_v.findViewById(R.id.tv_fragment_remit_register_first_next);
			mNextBtn = _v.findViewById(R.id.fl_fragment_remit_register_first_next);
			mNextBtn.setOnClickListener(this);
			mNextBtn.setEnabled(false);
			mEmpityView = (LinearLayout)_v.findViewById(R.id.ll_remit_empty_layout);
			mListView = (ListView)_v.findViewById(R.id.lv_fragment_remit_register_list);
			mAdapter = new AdtListViewRemitBankList(getActivity(), mAlInfoDataList);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

			
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
//			sampleData();
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

//=========================================================================================//
// get bank list API
//=========================================================================================//
	private void get_bank_list(){
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ArrayList<RemitBankDetail>> call = api.getRemitBankDetail(mRequestDataByUserId);
		call.enqueue(new Callback<ArrayList<RemitBankDetail>>() {
			@Override
			public void onResponse(Call<ArrayList<RemitBankDetail>> call, Response<ArrayList<RemitBankDetail>> response) {
				DialogUtils.dismissLoading();
				mAlInfoDataList = new ArrayList<RemitBankDetail>();
				mAlInfoDataList = response.body();
				if(mAlInfoDataList.size() != 0){
					mListView.setVisibility(View.VISIBLE);
					mEmpityView.setVisibility(View.GONE);
					for(int i = 0; i < mAlInfoDataList.size(); i++){
						if("N".equals(mAlInfoDataList.get(i).getPossibleSend())){
							mPossibleCount += 1;
						}
					}
					if(mAlInfoDataList.size() == mPossibleCount){
						mCertifyAccount.setText(getString(R.string.remit_register_text_first_account));
						mAccountGuide.setVisibility(View.VISIBLE);
					}
					else {
						mCertifyAccount.setText(getString(R.string.remit_register_text_first_certificate));
						mAccountGuide.setVisibility(View.GONE);
					}
					mAdapter.setData(mAlInfoDataList);
					mAdapter.notifyDataSetChanged();
				}
				else {
					mListView.setVisibility(View.GONE);
					mEmpityView.setVisibility(View.VISIBLE);
					mCertifyAccount.setText(getString(R.string.remit_register_text_first_account));
					mAccountGuide.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onFailure(Call<ArrayList<RemitBankDetail>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());

			}
		});
	}

	private void sampleData(){
		mAlInfoDataList = new ArrayList<RemitBankDetail>();
		RemitBankDetail sample = new RemitBankDetail();
		sample.setCompanyCode("003");
		sample.setCompanyName("기업은행");
		sample.setAccountNo("23309217220002");
		sample.setPossibleSend("Y");
		sample.setAccountID("1234");
		mAlInfoDataList.add(sample);

		if(mAlInfoDataList.size() != 0){
			mListView.setVisibility(View.VISIBLE);
			mEmpityView.setVisibility(View.GONE);
			for(int i = 0; i < mAlInfoDataList.size(); i++){
				if("N".equals(mAlInfoDataList.get(i).getPossibleSend())){
					mPossibleCount += 1;
				}
			}
			if(mAlInfoDataList.size() == mPossibleCount){
				mCertifyAccount.setText(getString(R.string.remit_register_text_first_account));
				mAccountGuide.setVisibility(View.VISIBLE);
			}
			else {
				mCertifyAccount.setText(getString(R.string.remit_register_text_first_certificate));
				mAccountGuide.setVisibility(View.GONE);
			}
			mAdapter.setData(mAlInfoDataList);
			mAdapter.notifyDataSetChanged();
		}
	}
}

