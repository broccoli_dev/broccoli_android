package com.nomadconnection.broccoli.fragment.remit;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.remit.AdtExpListViewRemitSendPhone;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitContent;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitPhoneCurrent;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.dialog.remit.DialogBaseRemitEditButton;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceRemitSend;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentRemitSendPhone extends BaseFragment implements View.OnClickListener{
	
	private static String TAG = "FragmentRemitSendPhone";


	InterfaceRemitSend mInterfaceRemitSend;

	/* Layout */
	private ArrayList<String> mGroupList = null;
	private ArrayList<ArrayList<RemitContent>> mChildList = null;
	private ArrayList<RemitContent> mChildListContent1 = null;
	private ArrayList<RemitContent> mChildListContent2 = null;
	private EditText mRemitSendPhone;
	private ImageView mRemitSendSearch;
	private TextView mRemitSendDirect;
	private Button mRemitSearch;
	private ExpandableListView mExpListView;
	private AdtExpListViewRemitSendPhone mAdapter;
	private ContactListTask mTask;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterfaceRemitSend = (InterfaceRemitSend) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_remit_send_phone, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.iv_fragment_remit_send_phone_search:
				ElseUtils.hideSoftKeyboard(mRemitSendPhone);
				String mSearchText = mRemitSendPhone.getText().toString()
						.toLowerCase(Locale.getDefault());
				mAdapter.filterByName(mSearchText);
				break;

			case R.id.tv_fragment_remit_send_direct_input:
				dialogOneButtonShow("");
				break;
		}

	}


//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			mGroupList = new ArrayList<String>();
			mChildList = new ArrayList<ArrayList<RemitContent>>();
			mChildListContent1 = new ArrayList<RemitContent>();
			mChildListContent2 = new ArrayList<RemitContent>();
			recentlySendTelList();
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mRemitSendPhone = (EditText)_v.findViewById(R.id.et_fragment_remit_send_phone);
			mRemitSendPhone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if ((actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_SEARCH)) {
						String mSearchText = mRemitSendPhone.getText().toString()
								.toLowerCase(Locale.getDefault());
						mAdapter.filterByName(mSearchText);
					}
					return false;
				}
			});
			mRemitSendSearch = (ImageView)_v.findViewById(R.id.iv_fragment_remit_send_phone_search);
			mRemitSendSearch.setOnClickListener(this);
			mRemitSendDirect = (TextView)_v.findViewById(R.id.tv_fragment_remit_send_direct_input);
			mRemitSendDirect.setOnClickListener(this);
			mExpListView = (ExpandableListView)_v.findViewById(R.id.elv_fragment_remit_send_phone_explistview);
			mAdapter = new AdtExpListViewRemitSendPhone(getActivity(), mGroupList, mChildList);
			mExpListView.setAdapter(mAdapter);

	        /* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return true;
				}
			});

			/* Child list click 시 */
			mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
											int groupPosition, int childPosition, long id) {
					final RemitContent mTemp = mChildList.get(groupPosition).get(childPosition);
					if (mTemp == null) return true;
//
					dialogShow(mTemp.getHostName(), mTemp.getTelNo());
//					NextStep(mTemp,  new BodyAssetStockDetail());
					return false;
				}
			});

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			for (int i = 0; i <mGroupList.size() ; i++) {
				mExpListView.expandGroup(i);
			}

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}



//===============================================================================//
// get content list
//===============================================================================//
	private void getContactsInList() {
			// TODO Auto-generated method stub
			mTask = new ContactListTask();
			mTask.execute(new Object());
		}

	private class ContactListTask extends AsyncTask {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
	//		showPB();
		}

		@Override
		protected Object doInBackground(Object[] params) {
			try {
				Cursor phones = getActivity().getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null, null, null, null);
				try {
					mChildListContent2.clear();
				} catch (Exception e) {

				}

				while (phones.moveToNext()) {
					String phoneName = phones
							.getString(phones
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
					String phoneNumber = phones
							.getString(phones
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

					RemitContent dataSet = new RemitContent();
					dataSet.setHostName(phoneName);
					dataSet.setTelNo(phoneNumber);
					mChildListContent2.add(dataSet);
				}
				phones.close();

				Collections.sort(mChildListContent2,
						new  Comparator<RemitContent>() {
							@Override
							public int compare(RemitContent lhs,
											   RemitContent rhs) {
								return lhs.getHostName().compareTo(rhs.getHostName());
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
			return mChildListContent2;
		}

		@Override
		protected void onPostExecute(Object o) {
			super.onPostExecute(o);
			mGroupList.add(getActivity().getString(R.string.remit_send_content));
			mChildList.add(mChildListContent2);
			if(mAdapter != null){
				mAdapter.setData(mGroupList, mChildList);
				for (int i = 0; i <mGroupList.size() ; i++) {
					mExpListView.expandGroup(i);
				}
			}
	//		hidePB();
		}
	}
	private void recentlySendTelList(){
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ResponseRemitPhoneCurrent> call = api.recentlySendTelList(mRequestDataByUserId);
		call.enqueue(new Callback<ResponseRemitPhoneCurrent>() {
			@Override
			public void onResponse(Call<ResponseRemitPhoneCurrent> call, Response<ResponseRemitPhoneCurrent> response) {
				DialogUtils.dismissLoading();
				mChildListContent1 = response.body().getSendTelList();
				if(mChildListContent1 != null && mChildListContent1.size() != 0){
					mGroupList.add(getActivity().getString(R.string.remit_send_current_phone));
					mChildList.add(mChildListContent1);
					getContactsInList();
				}
				else {
					getContactsInList();
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitPhoneCurrent> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}


	private void dialogShow(String mStr1, String mStr2){
		DialogUtils.showDlgRemitEditTwoButton(getActivity(), new DialogBaseRemitEditButton.DialogInputTypeListener() {
			@Override
			public void onClick(DialogInterface dialog, int stringresid, RemitContent text) {
				if (stringresid == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
					mInterfaceRemitSend.onRemitSendPhone(text);
				}

			}
		},
				new InfoDataDialog(R.layout.dialog_inc_remit_case_1, getString(R.string.common_cancel), getString(R.string.common_confirm), mStr1, mStr2));
	}

	private void dialogOneButtonShow(String mStr1){
		DialogUtils.showDlgRemitEditTwoButton(getActivity(), new DialogBaseRemitEditButton.DialogInputTypeListener() {
					@Override
					public void onClick(DialogInterface dialog, int stringresid, RemitContent text) {
						if (stringresid == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
							mInterfaceRemitSend.onRemitSendPhone(text);
						}

					}
				},
				new InfoDataDialog(R.layout.dialog_inc_remit_case_1, "", getString(R.string.common_confirm), mStr1));
	}
}
