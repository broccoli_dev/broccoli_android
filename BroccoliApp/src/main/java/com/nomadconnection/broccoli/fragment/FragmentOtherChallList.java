package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivityOtherChallDetail;
import com.nomadconnection.broccoli.adapter.AdtListViewOtherChallengeInfo;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentOtherChallList extends BaseFragment {
	
	private static String TAG = "FragmentOtherChallList";
	
	
	InterfaceEditOrDetail mInterFaceEditOrDetail;

	/* Layout */
	private ListView mListView;
	private AdtListViewOtherChallengeInfo mAdapter;
	private List<EtcChallengeInfo> mAlInfoDataList;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_other_chall_list, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.FragmentOtherChallList);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		requestDataSet();
		super.onResume();
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mListView = (ListView)_v.findViewById(R.id.lv_fragment_other_chall_list_listview);
			mAdapter = new AdtListViewOtherChallengeInfo(getActivity(), mAlInfoDataList);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(ItemClickListener);
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			requestDataSet();
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	/**  Item Click Listener **/
	private AdapterView.OnItemClickListener ItemClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {
			if (mAlInfoDataList.get(position) == null) {	// test 임시
				return;
			}

			EtcChallengeInfo mEtcChallengeInfo = mAlInfoDataList.get(position);

			Intent intent = new Intent(getActivity(), Level3ActivityOtherChallDetail.class);
			intent.putExtra(Const.CHALLENGE_INFO, mEtcChallengeInfo);
			startActivity(intent);
		}
	};


//=========================================================================================//
// Refresh Adapter
//=========================================================================================//
	public void refreshAdt(String _sort){
		mAdapter.notifyDataSetChanged();
	}


	public void requestDataSet() {
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId userId = new RequestDataByUserId();
		userId.setUserId(Session.getInstance(getActivity()).getUserId());

		EtcApi api = ServiceGenerator.createService(EtcApi.class);
		Call<List<EtcChallengeInfo>> call = api.listChallenge(userId);
		call.enqueue(new Callback<List<EtcChallengeInfo>>() {
			@Override
			public void onResponse(Call<List<EtcChallengeInfo>> call, Response<List<EtcChallengeInfo>> response) {
				DialogUtils.dismissLoading();
				mAlInfoDataList = new ArrayList<EtcChallengeInfo>();
				mAlInfoDataList = response.body();
				mAdapter.setData((ArrayList<EtcChallengeInfo>) mAlInfoDataList);
				refreshAdt(null);

			}

			@Override
			public void onFailure(Call<List<EtcChallengeInfo>> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}
}
