package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level2ActivityAssetStock;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.AdtListViewMain3;
import com.nomadconnection.broccoli.api.InvestApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.common.CustomListViewPalallax;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorCode;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockInfo;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.InfoDataMain3;
import com.nomadconnection.broccoli.data.Investment.InventMainData;
import com.nomadconnection.broccoli.data.Investment.InventMainStock;
import com.nomadconnection.broccoli.data.Investment.InventMainStockDetail;
import com.nomadconnection.broccoli.data.Investment.InvestMainDailyEarningRate;
import com.nomadconnection.broccoli.data.Investment.InvestMainDailyEstimate;
import com.nomadconnection.broccoli.data.RequestMainData;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.ChartUtils;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.MyMarkerView;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FragmentMain3 extends BaseFragment implements InterfaceFragmentInteraction {

    private static String TAG = FragmentMain3.class.getSimpleName();
    /* Layout */
    public CustomListViewPalallax mListView;
    /* Flexible Layout */
    private TextView mInfo1;
    private TextView mInfo2;
    private TextView mInfo5;
    private LinearLayout mInfoBtn1;
    private LinearLayout mInfoBtn2;
    private TextView mSubInfo1;
    private TextView mSubInfo2;
    private TextView mSubInfo3;
    private TextView mSubInfo4;
    private View mFlexibleArea;

    private InventMainData mMainData;
    private AdtListViewMain3 mAdapter;
    private ArrayList<InfoDataMain3> mAlInfoDataList;
    private ArrayList<EtcChallengeInfo> mEtcChallengeInfo;

    /* Chart */
    private int mLastXIndex = 0;
    private LineChart mChart;
    private ArrayList<String> mLineXEntries;
    private ArrayList<Entry> mLineYEntries;
    private ArrayList<String> mLineXEarning;
    private ArrayList<Entry> mLineYEarning;
    private InvestApi mApi;
    /**
     * Item Click Listener
     **/
    private OnItemClickListener ItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            if (position != 0 && mAlInfoDataList.get(position - 1) == null) {
                return;
            }

            switch (position) {
                case 0:
                    //Toast.makeText(getActivity(), "chart", Toast.LENGTH_SHORT).show();
                    break;

                case 1:
                    ((BaseActivity)getActivity()).sendTracker(AnalyticsName.InvestMainClickStock);
                    ArrayList<BodyAssetStockInfo> targetList = new ArrayList<BodyAssetStockInfo>();
                    List<InventMainStockDetail> sourceList = mMainData.getStock().getList();
                    if (sourceList.size() > 0) {
                        for (InventMainStockDetail list : sourceList) {
                            BodyAssetStockInfo info = new BodyAssetStockInfo();
                            info.mBodyAssetStockCode = list.getStockCode();
                            info.mBodyAssetStockName = list.getStockName();
                            info.mBodyAssetStockPreviousSum = list.getPrevSum();
                            info.mBodyAssetStockSum = list.getSum();
                            targetList.add(info);
                        }
                    }
                    startActivityForResult(new Intent(getActivity(), Level2ActivityAssetStock.class)
                            .putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_1, targetList).putExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND, true)
                            , APP.ACTIVIYT_FOR_RESULT_MAIN_1_REQUEST_CODE);
                    break;

                case 2:
//                    Toast.makeText(getActivity(), "나중에", Toast.LENGTH_SHORT).show();
//                    Intent Challenge = new Intent(getActivity(), Level2ActivityOtherChall.class);
//                    Challenge.putExtra(Const.DATA, (Serializable) mMainData.getChallenge());
//                    startActivity(Challenge);
                    break;

                default:
                    break;
            }
        }
    };
    private OnChartValueSelectedListener mOnChartValueSelectedListener = new OnChartValueSelectedListener() {
        @Override
        public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
            if (e == null)
                return;
            mLastXIndex = h.getXIndex();
//            mChart.getMarkerView().setLayoutParams(new LinearLayout.LayoutParams((int) (mChart.getMarkerView().getChildAt(0).getWidth() * 1.5), LinearLayout.LayoutParams.WRAP_CONTENT));
//            if (mLastXIndex == 0){
//                mChart.getMarkerView().setLayoutParams(new LinearLayout.LayoutParams((int)(mChart.getMarkerView().getChildAt(0).getWidth()*1.7), LinearLayout.LayoutParams.WRAP_CONTENT));
//                mChart.getMarkerView().setGravity(Gravity.RIGHT);
//            }
//            else if (mLastXIndex == mLineYEntries.size()-1){
//                mChart.getMarkerView().setLayoutParams(new LinearLayout.LayoutParams((int)(mChart.getMarkerView().getChildAt(0).getWidth()*1.7), LinearLayout.LayoutParams.WRAP_CONTENT));
//                mChart.getMarkerView().setGravity(Gravity.LEFT);
//            }
//            else{
//                mChart.getMarkerView().setLayoutParams(new LinearLayout.LayoutParams((int)(mChart.getMarkerView().getChildAt(0).getWidth()*1), LinearLayout.LayoutParams.WRAP_CONTENT));
//                mChart.getMarkerView().setGravity(Gravity.CENTER);
//            }
            if (mLastXIndex == 0){
                mInfo2.setText("0 (0%)");
            } else {
                long mTemp1 = (long)mLineYEntries.get(0).getVal();
                long mTemp2 = (long)mLineYEntries.get(mLastXIndex).getVal();
                float mTemp3 = (mLineYEntries.get(mLastXIndex).getVal() - mLineYEntries.get(0).getVal()) / mLineYEntries.get(0).getVal() * 100;
                long different = mTemp2 - mTemp1;
                String temp = String.valueOf(Math.abs(different));
                if (temp.equalsIgnoreCase("0")){
                    mInfo2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mInfo2.setText("" + TransFormatUtils.getDecimalFormatRecvString(temp) + "  (" + String.format("%.2f", mTemp3) + "%)");
                }
                else {
                    if (different > 0) {
                        mInfo2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stock_white_up, 0, 0, 0);
                        mInfo2.setCompoundDrawablePadding(10);
                    } else {
                        mInfo2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.stock_white_down, 0, 0, 0);
                        mInfo2.setCompoundDrawablePadding(10);
                    }
                    String sign = (different > 0 ? "+" : "-");
                    if (different == 0) {
                        sign = "";
                    }
                    mInfo2.setText("" + TransFormatUtils.getDecimalFormatRecvString(temp) + "  (" + sign + String.format("%.2f", Math.abs(mTemp3)) + "%)");
                }
            }

//            mChart.getMarkerView().getChildAt(0).setVisibility(mLineEntries.get(h.getXIndex()).getVal() == 0 ? View.GONE:View.VISIBLE);
//            mChart.invalidate();
        }

        @Override
        public void onNothingSelected() {
            mChart.highlightValue(mLastXIndex, 0);
        }
    } ;
    /**
     * Button Click Listener
     **/
    private View.OnClickListener BtnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.ll_flexible_area_main_3_info_btn_1:
                    showInfoTab(true);

//					if (mAssetMainData == null) return;
//					if (mAssetMainData.mMainAssetMonthlyProperty == null) return;
//					refreshChart(mAssetMainData.mMainAssetMonthlyProperty);
                    break;

                case R.id.ll_flexible_area_main_3_info_btn_2:
                    showInfoTab(false);

//					if (mAssetMainData == null) return;
//					if (mAssetMainData.mMainAssetMonthlyDebt == null) return;
//					refreshChart(mAssetMainData.mMainAssetMonthlyDebt);
                    break;

                default:
                    break;
            }

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main3, null);

        if (parseParam()) {
            if (initWidget(v)) {
            } else {
                getActivity().finish();
            }
        } else {
            getActivity().finish();
        }

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }


//===============================================================================//
// Listener
//===============================================================================//	

    //=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
    private boolean parseParam() {
        boolean bResult = true;
        try {
            mAlInfoDataList = new ArrayList<InfoDataMain3>();
//            mAlInfoDataList.addAll(SampleTestData.GetSampleDataMain3());
            mLineXEntries = new ArrayList<String>();
            mLineYEntries = new ArrayList<Entry>();
            mLineXEarning = new ArrayList<String>();
            mLineYEarning = new ArrayList<Entry>();
//            getData();
        } catch (Exception e) {
            bResult = false;
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "parseParam() : " + e.toString());
            return bResult;
        }
        return bResult;
    }

    //=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
    private boolean initWidget(View _v) {
        boolean bResult = true;
        try {
            /* Layout */
            mListView = (CustomListViewPalallax) _v.findViewById(R.id.lv_fragment_main_3_listview);
            mAdapter = new AdtListViewMain3(getActivity(), mAlInfoDataList);


    		/* btn height만큼 채우기 */
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mFlexibleArea = (LinearLayout) inflater.inflate(R.layout.flexible_area_main_3, null);
            mListView.addHeaderView(mFlexibleArea);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(ItemClickListener);


			/* Flexibel Layout */
            mInfo1 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_info_1);
            mInfo2 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_info_2);
            mInfo5 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_info_5);
            mInfo5.setTypeface(FontClass.setFont(getActivity()));
            mInfoBtn1 = (LinearLayout) _v.findViewById(R.id.ll_flexible_area_main_3_info_btn_1);
            mInfoBtn2 = (LinearLayout) _v.findViewById(R.id.ll_flexible_area_main_3_info_btn_2);
            mSubInfo1 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_sub_info_1);
            mSubInfo2 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_sub_info_2);
            mSubInfo3 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_sub_info_3);
            mSubInfo4 = (TextView) _v.findViewById(R.id.tv_flexible_area_main_3_sub_info_4);
            mSubInfo1.setVisibility(View.GONE);
            mSubInfo2.setVisibility(View.GONE);
            mSubInfo3.setVisibility(View.GONE);
            mSubInfo4.setVisibility(View.GONE);

            mInfoBtn2.setVisibility(View.GONE);
            mInfoBtn1.setOnClickListener(BtnClickListener);
//            mInfoBtn2.setOnClickListener(BtnClickListener);

			/* Chart */
            mChart = (LineChart) _v.findViewById(R.id.lc_flexible_area_main_3_chart);
            mChart.setDescription("");
            mChart.setNoDataTextDescription("");
            mChart.setNoDataText("");
//            mChart.setNoDataTextDescription("주식 정보를 추가해 주세요.");
//            mChart.setNoDataText("주식 정보가 없습니다.");
            mChart.setBackgroundColor(Color.TRANSPARENT);
//			mChart.setMaxVisibleValueCount(60);	// if more than 60 entries are displayed in the chart, no values will be // drawn
            mChart.setPinchZoom(false);    // scaling can now only be done on x- and y-axis separately
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setOnChartGestureListener(new OnChartGestureListener() {
                @Override
                public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

                }

                @Override
                public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

                }

                @Override
                public void onChartLongPressed(MotionEvent me) {

                }

                @Override
                public void onChartDoubleTapped(MotionEvent me) {

                }

                @Override
                public void onChartSingleTapped(MotionEvent me) {

                }

                @Override
                public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

                }

                @Override
                public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

                }

                @Override
                public void onChartTranslate(MotionEvent me, float dX, float dY) {

                }
            });
//			mChart.setDrawBarShadow(false);
//			// draw bars behind lines
//			mChart.setDrawOrder(ChartUtils.mDrawOrder);

            MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view_stock);
            mv.setChart(mChart);
            mChart.setMarkerView(mv);
//            mChart.setPadding(50, 0, 0,0);
//            mChart.setPaddingRelative(50,0,0,0);

//			mChart.fitScreen ();
//			mChart.setViewPortOffsets(0f, 0f, 0f, 0f);
//			mChart.setPadding(-20, 0, 20, 0);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);
//			rightAxis.setDrawAxisLine(true);
//			rightAxis.setAxisLineColor(R.color.main_1_layout_bg);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setDrawGridLines(false);
            leftAxis.setDrawAxisLine(false);
            leftAxis.setGridColor(Color.argb((int) (255 * 1), 255, 255, 255));
            leftAxis.setTextColor(Color.argb((int) (255 * 1), 255, 255, 255));
            leftAxis.enableGridDashedLine(10f, 10f, 0f);
            leftAxis.setTextSize(9f);
            leftAxis.setStartAtZero(false);
            leftAxis.setSpaceTop(30f);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setGridColor(Color.argb((int) (255 * 1), 255, 255, 255));
            xAxis.setDrawGridLines(true);
            xAxis.setDrawAxisLine(true);
            xAxis.setAxisLineColor(Color.argb((int) (255 * 1), 255, 255, 255));
            xAxis.enableGridDashedLine(10f, 10f, 0f);
            xAxis.setTextSize(9f);
            xAxis.setYOffset(15f);
            xAxis.setXOffset(0);
            xAxis.setTextColor(Color.argb((int) (66 * 1), 255, 255, 255));
			xAxis.setAvoidFirstLastClipping(true);
            xAxis.setLabelsToSkip(30);
//			xAxis.setXOffset(-20f);

            // hide
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.getXAxis().setEnabled(true);
            mChart.getLegend().setEnabled(false);
            mChart.setExtraBottomOffset(5f);

            mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries, mLineYEntries, APP.ASSET_STOCK_HISTORY_CHART_USE));
            mChart.invalidate();

            mChart.setOnChartValueSelectedListener(mOnChartValueSelectedListener);

        } catch (Exception e) {
            bResult = false;
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "initWidget() : " + e.toString());
            return bResult;
        }

        return bResult;
    }

    //=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
    private boolean initData() {
        boolean bResult = true;
        try {
			/* Default Value */
            mAdapter.setData(mAlInfoDataList);
            mAdapter.notifyDataSetChanged();
            BtnClickListener.onClick(mInfoBtn1);
        } catch (Exception e) {
            bResult = false;
            if (APP._DEBUG_MODE_)
                Log.e("broccoli", "initData() : " + e.toString());
            return bResult;
        }

        return bResult;
    }

    private void getData() {
        RequestMainData requestMainData = new RequestMainData();
        requestMainData.setUserId(Session.getInstance(getActivity()).getUserId());
        requestMainData.setDeviceId(ElseUtils.getDeviceUUID(getContext()));
        mApi = ServiceGenerator.createService(InvestApi.class);
        Call<InventMainData> call = mApi.getInvestMainData(requestMainData);
        call.enqueue(new Callback<InventMainData>() {
            @Override
            public void onResponse(Call<InventMainData> call, Response<InventMainData> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        InventMainData data = response.body();
                        String code = data.getCode();
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            if (data != null && (code == null || code.isEmpty())) {
                                setData(data);
                            } else {
                                if (ErrorMessage.ERROR_716_TOKEN_EXPIRE.code().equalsIgnoreCase(data.getCode())
                                        || ErrorMessage.ERROR_10007_SESSION_NOT_FOUND.code().equalsIgnoreCase(data.getCode())
                                        || ErrorMessage.ERROR_10008_PWD_CHANGED_SESSION_EXPIRED.code().equalsIgnoreCase(data.getCode())
                                        || ErrorMessage.ERROR_20003_MISMATCH_ACCESS_TOKEN.code().equalsIgnoreCase(data.getCode())) {
                                    memberLeaveComplete();
                                } else {
                                    ElseUtils.network_error(getActivity());
                                }
                            }
                        }
                    } else {
                        Retrofit retrofit = ServiceGenerator.getRetrofit();
                        Converter<ResponseBody, InventMainData> converter = retrofit.responseBodyConverter(InventMainData.class, new Annotation[0]);
                        try {
                            InventMainData data = converter.convert(response.errorBody());
                            if (ErrorCode.ERROR_CODE_701_LOGIN_INCORRECT_DEVICE.equalsIgnoreCase(data.getCode())) {
                                memberLeaveComplete();
                            } else {
                                ElseUtils.network_error(getActivity());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<InventMainData> call, Throwable t) {
                ElseUtils.network_error(getActivity());
            }
        });
    }


//===============================================================================//
//  Data
//===============================================================================//
    private void setData(InventMainData data) {
        mAlInfoDataList.clear();
        mMainData = data;
        if (mMainData == null ||
                !mMainData.isComplete()) {
            CustomToast.makeText(getContext(), "데이터에 이상이 있습니다. 잠시 후 재시도 해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }
        InventMainStock stock = data.getStock();
        if (stock != null) {
            List<InventMainStockDetail> list = stock.getList();
            long totalSum = 0;
            long totalPrevSum = 0;
            if (list != null && list.size() > 0) {
                for (InventMainStockDetail detail : list) {
                    String strSum = detail.getSum();
                    String strPrev = detail.getPrevSum();
                    long sum = Long.parseLong(strSum);
                    long prev = Long.parseLong(strPrev);
                    totalSum += sum;
                    totalPrevSum += prev;
                }
                long result = totalSum-totalPrevSum;
                long absResult = Math.abs(result);
                double rate = (double)absResult/(double)totalPrevSum * 100;
                String data5 = ElseUtils.getDecimalFormat(absResult)+" ("+(result > 0 ? "+":"-")+ElseUtils.getPointCut(rate)+"%)";
                InfoDataMain3 dataMain3 = new InfoDataMain3("주식", "y",
                        ElseUtils.getDecimalFormat(totalSum), TransFormatUtils.transDateForm(stock.getDate()), data5, "", "", Integer.toString(list.size()), "", data);
                mAlInfoDataList.add(dataMain3);
            } else {
                InfoDataMain3 dataMain3 = new InfoDataMain3("주식", "N", "", "", "", "", "", "0", "", data);
                mAlInfoDataList.add(dataMain3);
            }
        } else {
            InfoDataMain3 dataMain3 = new InfoDataMain3("주식", "N", "", "", "", "", "", "0", "", data);
            mAlInfoDataList.add(dataMain3);
        }
        Object fund = null;
        if (fund != null) {

        } else {
            //// TODO: 16. 2. 14.
//            InfoDataMain3 dataMain3 = new InfoDataMain3(getString(R.string.common_fund), "N", "", "", "", "", "", "", "");
//            mAlInfoDataList.add(dataMain3);
//            차후 작업
        }

        List<InvestMainDailyEstimate> estimate =  data.getDailyEstimate();
        List<InvestMainDailyEarningRate> earning = data.getDailyEarningRate();

        mLineXEntries = new ArrayList<>();
        mLineYEntries = new ArrayList<>();
        int count = 0;
        for (InvestMainDailyEstimate dailyEstimate : estimate) {
            if (!dailyEstimate.getEstimate().equalsIgnoreCase("0")){
                String date = ElseUtils.getMonthDay(dailyEstimate.getDate());
                mLineXEntries.add(date);
                mLineYEntries.add(new Entry(Float.parseFloat(dailyEstimate.getEstimate()), count, date));
                count++;
            }
        }
        mLastXIndex = mLineXEntries.size() - 1;
        mChart.getXAxis().setLabelsToSkip(mLineXEntries.size()/3);
//        if (mLineXEntries.size() < 7){
//            for (int i = 0; i < 7-mLineXEntries.size(); i++) {
//                mLineXEntries.add("");
//            }
//        }

        mLineXEarning = new ArrayList<>();
        mLineYEarning = new ArrayList<>();
        count = 0;
        for (InvestMainDailyEarningRate dailyEarningRate : earning) {
            mLineXEarning.add(ElseUtils.getMonthDay(dailyEarningRate.getDate()));
            String value = dailyEarningRate.getEarningRate();
            if (value.isEmpty()) {
                value = "0";
            }
            mLineYEarning.add(new Entry(Float.parseFloat(value), count));
            count++;
        }


        initData();
    }

    private void showInfoTab(boolean estimate) {
        if (estimate) {
            mInfoBtn1.setSelected(true);
            mInfoBtn2.setSelected(false);
            mInfo1.setText("총 평가액");
            mInfo5.setText(ElseUtils.getDecimalFormat(Long.parseLong(mMainData.getTotalEstimate())));
            String temp = mMainData.getDifference();
            long difference = Long.parseLong(temp);
            long abs = Math.abs(difference);
            String sign = (difference > 0 ? "+" : "-");
            if (difference == 0) {
                sign = "";
            }
            mInfo2.setText("" + sign + ElseUtils.getDecimalFormat(abs) + " (" + mMainData.getRate() + "%)");
            mChart.clear();
            mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries, mLineYEntries, APP.ASSET_STOCK_HISTORY_CHART_USE));
            mChart.invalidate();
            if(mLastXIndex != -1){
                mOnChartValueSelectedListener.onValueSelected(new Entry(0, 0), 0, new Highlight(mLastXIndex, 0));
            }
            mChart.highlightValue(mLastXIndex, 0);

        } else {
            mInfoBtn1.setSelected(false);
            mInfoBtn2.setSelected(true);
            mInfo1.setText("총 수익률");
            mInfo5.setText(ElseUtils.getDecimalFormat(Long.parseLong(mMainData.getTotalEstimate())));
            String temp = mMainData.getDifference();
            long difference = Long.parseLong(temp);
            long abs = Math.abs(difference);
            String sign = (difference > 0 ? "+" : "-");
            if (difference == 0) {
                sign = "";
            }
            mInfo2.setText("" + sign + ElseUtils.getDecimalFormat(abs) + " (" + mMainData.getRate() + "%)");
            mChart.clear();
            mChart.setData(ChartUtils.generateLineDataFill(mLineXEarning, mLineYEarning, APP.ASSET_STOCK_HISTORY_CHART_USE));
            mChart.invalidate();
        }
        mChart.animateX(1000, Easing.EasingOption.EaseInOutSine);
    }


    @Override
    public void reset(boolean flag) {

    }

    @Override
    public boolean canScrollUp() {
        boolean ret = false;
        if (mListView != null) {
            ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
        }
        return ret;
    }

    @Override
    public boolean canScrollDown() {
        return false;
    }
}
