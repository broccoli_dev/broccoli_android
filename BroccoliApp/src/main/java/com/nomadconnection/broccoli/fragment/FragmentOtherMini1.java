package com.nomadconnection.broccoli.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level2ActivityOtherMini;
import com.nomadconnection.broccoli.adapter.PagerRefreshAdapter;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Etc.BodyMoneyInfo;
import com.nomadconnection.broccoli.data.Etc.RequestEtcMoneyList;
import com.nomadconnection.broccoli.data.InfoDataOtherMini;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.common.CustomPagerSlidingTabStripSide;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentOtherMini1 extends BaseFragment {
	
	private static String TAG = FragmentOtherMini1.class.getSimpleName();

	/* Layout */
	private TextView mSub1Title;
	private TextView mSub1Cost;
	private TextView mSub2Title;
	private TextView mSub2Cost;

	/* Pager */
	private PagerRefreshAdapter mPagerAdapter;
	private ViewPager mViewPager;
//	private CustomViewPagerTouch mViewPager;
//	private int mCurrentPageInx;								// 상위로 이동()

	/* Child Fragment */
	private ArrayList<Fragment> alFragment;
	private HashMap<String, ArrayList<InfoDataOtherMini>> mHMAlInfoDataList;

	/*월*/
	private String[] mTempMonthSpace;
	private String[] mTempMonthSpaceBefore;
	private String[] mTempMonthSpaceAfter;
	private String[] mTempMonth;
	private String[] mMonth;
	private String[] mPagerTitle;
	private HashMap<String, Boolean> mHmFlag;

	public FragmentOtherMini1() {

//		mTempMonth = TransFormatUtils.getRangeMonth(TransFormatUtils.getDataFormatWhich(0), 12);
		mTempMonth = TransFormatUtils.getRangeMonth(TransFormatUtils.getAfterDate(TransFormatUtils.getDataFormatWhich(0), "m", 1), 12);
		mTempMonthSpace = TransFormatUtils.getRangeMonth(TransFormatUtils.getAfterDate(TransFormatUtils.getDataFormatWhich(0), "m", 1+Level2ActivityOtherMini.mSideMonth), 12+Level2ActivityOtherMini.mSideMonth*2);
		mTempMonthSpaceBefore = new String[Level2ActivityOtherMini.mSideMonth];
		mTempMonthSpaceAfter = new String[Level2ActivityOtherMini.mSideMonth];
		for (int i = 0; i <mTempMonthSpace.length ; i++) {
			if (i == 0) mTempMonthSpaceBefore[0] = mTempMonthSpace[i].toString().substring(4, 6);
			else if (i == 1) mTempMonthSpaceBefore[1] = mTempMonthSpace[i].toString().substring(4, 6);
			else if (i == mTempMonthSpace.length-2) mTempMonthSpaceAfter[0] = mTempMonthSpace[i].toString().substring(4, 6);
			else if (i == mTempMonthSpace.length-1) mTempMonthSpaceAfter[1] = mTempMonthSpace[i].toString().substring(4, 6);
		}
		mMonth = new String[mTempMonth.length];
		mPagerTitle = new String[mTempMonth.length];
		mHmFlag = new HashMap<String, Boolean>();
		mHMAlInfoDataList = new HashMap<String, ArrayList<InfoDataOtherMini>>();
		for (int i = 0; i < mTempMonth.length; i++) {
			mMonth[i] = mTempMonth[i].substring(0,6);
			mPagerTitle[i] = mTempMonth[i].substring(4,6);
//			mPagerTitle[i] = mTempMonth[i].substring(4,6)+"월";
			mHmFlag.put(mTempMonth[i].toString(), false);
			mHMAlInfoDataList.put(mTempMonth[i].toString(), new ArrayList<InfoDataOtherMini>());
		}

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_other_mini_1, null);

		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}

		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }







//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}

		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout */
			((LinearLayout)_v.findViewById(R.id.ll_contents_area_inc_type_3_layout)).setBackgroundResource(R.color.main_4_layout_bg);
			mSub1Title= (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_3_txt_1);
			mSub1Cost= (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_3_txt_2);
			mSub1Cost.setTypeface(FontClass.setFont(getActivity()));
			mSub2Title= (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_3_txt_3);
			mSub2Cost= (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_3_txt_4);
			mSub2Cost.setTypeface(FontClass.setFont(getActivity()));

			alFragment = new ArrayList<Fragment>();
			for (int i = 0; i < mTempMonth.length; i++) {
				alFragment.add(new FragmentOtherMini1A(mHMAlInfoDataList.get(mTempMonth[i])));
			}

			/* Pager */
			mPagerAdapter = new PagerRefreshAdapter(getActivity().getSupportFragmentManager(), alFragment, mPagerTitle);
			mViewPager = (ViewPager) _v.findViewById(R.id.pager_fragment_other_mini_1_body);
//			mViewPager = (CustomViewPagerTouch) _v.findViewById(R.id.pager_fragment_other_mini_1_body);
			mViewPager.setAdapter(mPagerAdapter);
			mViewPager.setOffscreenPageLimit(alFragment.size());
//			mViewPager.setPageSetting(mTempMonth.length, CalendarList.mSideMonth);
//			mCurrentPageInx = 0;

			CustomPagerSlidingTabStripSide tabs = (CustomPagerSlidingTabStripSide) _v.findViewById(R.id.pager_fragment_other_mini_1_tabs);
//			CustomPagerSlidingTabStrip tabs = (CustomPagerSlidingTabStrip) _v.findViewById(R.id.pager_fragment_other_mini_1_tabs);
			tabs.setSpace(APP.CUSTOM_PAGE_SLIDING_TAB_STRIP_SIDE_TYPE_ELSE, Level2ActivityOtherMini.mSideMonth, mTempMonthSpaceBefore, mTempMonthSpaceAfter);
			tabs.setShouldExpand(true);
			tabs.setViewPager(mViewPager);
			tabs.setOnPageChangeListener(mPageChangeListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mViewPager.setCurrentItem(Level2ActivityOtherMini.mPageInx);
//			mViewPager.setCurrentItem(alFragment.size()-1);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}





//=========================================================================================//
// Refresh
//=========================================================================================//

	/*refresh Header */
	private void refreshHeader(int _ViewPagerPos){
		long mTempTotal1 = 0;
		long mTempTotal2 = 0;
		mSub1Title.setText("총 예상 고정지출액");
		mSub2Title.setText("남은 고정지출액");

		for (int i = 0; i <mHMAlInfoDataList.get(mTempMonth[_ViewPagerPos]).size() ; i++) {
			mTempTotal1 += Long.valueOf(mHMAlInfoDataList.get(mTempMonth[_ViewPagerPos]).get(i).mTxt5);
			if (TransFormatUtils.compareDate( mHMAlInfoDataList.get(mTempMonth[_ViewPagerPos]).get(i).mTxt4, TransFormatUtils.getDataFormatWhich(0) )){
				mTempTotal2 += Long.valueOf(mHMAlInfoDataList.get(mTempMonth[_ViewPagerPos]).get(i).mTxt5);
			}
		}

		mSub1Cost.setText(TransFormatUtils.getDecimalFormatRecvString(String.valueOf(mTempTotal1)));
		mSub2Cost.setText(TransFormatUtils.getDecimalFormatRecvString(String.valueOf(mTempTotal2)));
	}

	/*refresh*/
	private void refreshData(final int _ViewPagerPos){
		final String mTempDate = mMonth[_ViewPagerPos];
		DialogUtils.showLoading(getActivity());											// 자산 로딩
		ServiceGenerator.createService(EtcApi.class).getCalendarList(
				new RequestEtcMoneyList(Session.getInstance(getActivity()).getUserId(), mTempDate)
		).enqueue(new Callback<ArrayList<BodyMoneyInfo>>() {
			@Override
			public void onResponse(Call<ArrayList<BodyMoneyInfo>> call, Response<ArrayList<BodyMoneyInfo>> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ArrayList<BodyMoneyInfo> mTemp = new ArrayList<BodyMoneyInfo>();
				if (APP._SAMPLE_MODE_) mTemp = AssetSampleTestDataJson.GetSampleData17();
				else mTemp = response.body();
				Collections.sort(mTemp, new Comparator<BodyMoneyInfo>() {
					@Override
					public int compare(BodyMoneyInfo lhs, BodyMoneyInfo rhs) {
						int a = Integer.valueOf(lhs.mBodyMoneyPayDate);
						int b = Integer.valueOf(rhs.mBodyMoneyPayDate);
						return (b > a ? -1 : 1);
					}
				});
				resetData(mTemp, _ViewPagerPos);
			}

			@Override
			public void onFailure(Call<ArrayList<BodyMoneyInfo>> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (APP._SAMPLE_MODE_) {
					ArrayList<BodyMoneyInfo> mTemp = new ArrayList<BodyMoneyInfo>();
					mTemp = AssetSampleTestDataJson.GetSampleData17();
					Collections.sort(mTemp, new Comparator<BodyMoneyInfo>() {
						@Override
						public int compare(BodyMoneyInfo lhs, BodyMoneyInfo rhs) {
							int a = Integer.valueOf(lhs.mBodyMoneyPayDate);
							int b = Integer.valueOf(rhs.mBodyMoneyPayDate);
							return (b > a ? -1 : 1);
						}
					});
					resetData(mTemp, _ViewPagerPos);
				} else
					ElseUtils.network_error(getActivity());
			}
		});

	}

	/*refresh*/
	private void resetData(ArrayList<BodyMoneyInfo> _AlInfoDataListInit, final int _ViewPagerPos){
		mHMAlInfoDataList.get(mTempMonth[_ViewPagerPos]).removeAll(mHMAlInfoDataList.get(mTempMonth[_ViewPagerPos]));
		for (int i = 0; i < _AlInfoDataListInit.size() ; i++) {
			String mGroupStr = "미납";
			if (_AlInfoDataListInit.get(i).mBodyMoneyPayDate.equals(TransFormatUtils.getDataFormatWhich(0))){
				mGroupStr = "오늘";
			}
			else{
				if (TransFormatUtils.compareDate( _AlInfoDataListInit.get(i).mBodyMoneyPayDate, TransFormatUtils.getDataFormatWhich(0) )){
					// 내일, 이번주, 이번주 이후
					mGroupStr = TransFormatUtils.getCompareDateMoneyCalendar(TransFormatUtils.getDataFormatWhich(0), _AlInfoDataListInit.get(i).mBodyMoneyPayDate);
				}
				else{
					//
					String value = _AlInfoDataListInit.get(i).mBodyMoneyExpectOrPaid;
					switch (value){
						case "PY":
							mGroupStr = "완료";
							break;
						case "PN":
							mGroupStr = "미납";
							break;
					}
				}
			}
			mHMAlInfoDataList.get(mTempMonth[_ViewPagerPos]).add(new InfoDataOtherMini(
					mGroupStr,
					_AlInfoDataListInit.get(i).mBodyMoneyTitle,
					_AlInfoDataListInit.get(i).mBodyMoneyPayType,
					_AlInfoDataListInit.get(i).mBodyMoneyPayDate,
					_AlInfoDataListInit.get(i).mBodyMoneyAmt,
					_AlInfoDataListInit.get(i).mBodyMoneyExpectOrPaid,
					_AlInfoDataListInit.get(i).mBodyMoneyCalendarId,
					_AlInfoDataListInit.get(i).mBodyMoneyOpponent,
					_AlInfoDataListInit.get(i).mBodyMoneyRepeatType,
					_AlInfoDataListInit.get(i).mBodyMoneyMatchedTag));

		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (mPagerAdapter != null) mPagerAdapter.notifyDataSetChanged();
				((FragmentOtherMini1A) alFragment.get(_ViewPagerPos)).refreshExpand();
				refreshHeader(_ViewPagerPos);
			}
		}, 200);


	}




//===============================================================================//
// View Pager
//===============================================================================//

	/**  View Page Change Listener **/
	private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {

		@Override
		public void onPageSelected(int _position) {
//			if (_position < CalendarList.mSideMonth || _position > mPagerTitle.length-CalendarList.mSideMonth-1) return;
//			mCurrentPageInx = _position;
//			if (mHmFlag.get(mTempMonth[mCurrentPageInx])) {
//				return;
//			}
//			else mHmFlag.put(mTempMonth[mCurrentPageInx].toString(), true);
//			refreshData(mCurrentPageInx);
			Level2ActivityOtherMini.mPageInx = _position;
			if (mHmFlag.get(mTempMonth[Level2ActivityOtherMini.mPageInx])) {
				refreshHeader(Level2ActivityOtherMini.mPageInx);
				((FragmentOtherMini1A) alFragment.get(Level2ActivityOtherMini.mPageInx)).refreshData();
				return;
			}
			else mHmFlag.put(mTempMonth[Level2ActivityOtherMini.mPageInx].toString(), true);
			refreshData(Level2ActivityOtherMini.mPageInx);
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	};



}
