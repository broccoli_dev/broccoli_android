package com.nomadconnection.broccoli.fragment.demo;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewDemoEtc;
import com.nomadconnection.broccoli.api.DemoApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Demo.TrialEtc;
import com.nomadconnection.broccoli.data.InfoDataMain4;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 4. 4..
 */

public class FragmentDemoEtc extends Fragment implements InterfaceFragmentInteraction {

    public ListView mListView;
    private AdtListViewDemoEtc mAdapter;
    private ArrayList<InfoDataMain4> mAlInfoDataList;

    /*Data Class*/
    private TrialEtc mData;
    private int mAge = 20;
    private String mGender = "M";

    private AdapterView.OnItemClickListener ItemClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            DialogUtils.showDialogDemoFinish(getActivity());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main4, null);

        if(parseParam()){
            if(initWidget(v)) {
                if(!initData()){
                    getActivity().finish();
                }
            }
            else{
                getActivity().finish();
            }
        }
        else{
            getActivity().finish();
        }

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        refreshData();
        super.onResume();
    }

    //=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
    private boolean parseParam(){
        boolean bResult = true;
        try {
            mAlInfoDataList = new ArrayList<InfoDataMain4>();
            refreshData();
        }
        catch (Exception e) {
            bResult = false;
            if(APP._DEBUG_MODE_)
                Log.e("broccoli", "parseParam() : " + e.toString());
            return bResult;
        }
        return bResult;
    }

    //=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
    private boolean initWidget(View _v) {
        boolean bResult = true;
        try
        {
			/* Layout */
            mListView = (ListView)_v.findViewById(R.id.lv_fragment_main_4_listview);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mListView.setNestedScrollingEnabled(true);
            }

            mAdapter = new AdtListViewDemoEtc(getActivity(), mAlInfoDataList);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(ItemClickListener);
        }
        catch(Exception e) {
            bResult = false;
            if(APP._DEBUG_MODE_)
                Log.e("broccoli", "initWidget() : " + e.toString());
            return bResult;
        }

        return bResult;
    }






//===============================================================================//
// Listener
//===============================================================================//

    //=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
    private boolean initData() {
        boolean bResult = true;
        try
        {
            Bundle bundle = getArguments();
            mAge = bundle.getInt(Const.DEMO_AGE);
            mGender = bundle.getString(Const.DEMO_GENDER);
            refreshData();
        }
        catch(Exception e) {
            bResult = false;
            if(APP._DEBUG_MODE_)
                Log.e("broccoli", "initData() : " + e.toString());
            return bResult;
        }

        return bResult;
    }





//===============================================================================//
// Refresh
//===============================================================================//

    /**  Refresh **/
    private void refreshData(){
        DialogUtils.showLoading(getActivity());
        Call<JsonElement> call = ServiceGenerator.createServiceForDemo(DemoApi.class).getEtcTrial(mAge, mGender);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response.body() != null) {
                        JsonElement element = response.body();
                        JsonObject object = element.getAsJsonObject();
                        if (object != null && object.has("etc")) {
                            JsonObject target = object.getAsJsonObject("etc");
                            mData = new Gson().fromJson(target, TrialEtc.class);
                            setData();
                        } else {
                            ErrorUtils.parseError(response);
                        }
                    } else {
                        ElseUtils.network_error(getActivity());
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                DialogUtils.dismissLoading();											// 자산 로딩
                ElseUtils.network_error(getActivity());
            }
        });
    }
    /**  Refresh **/
    private void setData(){
        if (mAlInfoDataList != null) {
            mAlInfoDataList.removeAll(mAlInfoDataList);

            if (mData == null) {
                CustomToast.makeText(getContext(), "데이터에 이상이 있습니다. 잠시 후 재시도 해주세요.", Toast.LENGTH_SHORT).show();
                return;
            }

            mAlInfoDataList.add(new InfoDataMain4("가장 인기있는 챌린지", "y",
                    mData.challengeTitle, String.valueOf(mData.challengeSum),
                    String.valueOf(mData.challengeBalance), "", "", "",
                    "", "", "", "", "", "",
                    "", "", "", "", "", "",
                    "", "", "", "", "", "",
                    "", "", "", "", "", "", null));
            if (mAge >= 20) {
                mAlInfoDataList.add(new InfoDataMain4("머니 캘린더", "y",
                        mData.firstCalendarName, String.valueOf(mData.firstCalendarSum),
                        mData.secondCalendarName, String.valueOf(mData.secondCalendarSum),
                        mData.thirdCalendarName, String.valueOf(mData.thirdCalendarSum),
                        "", "", "", "", "", "",
                        "", "", "", "", "", "",
                        "", "", "", "", "", "",
                        "", "", "", "", "", "", null));
            }

            mAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void reset(boolean flag) {
        if (mListView != null) {
            mListView.setSelection(0);
        }
    }

    @Override
    public boolean canScrollUp() {
        boolean ret = false;
        if (mListView != null) {
            ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
        }
        return ret;
    }

    @Override
    public boolean canScrollDown() {
        return false;
    }

}
