package com.nomadconnection.broccoli.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level4ActivityAssetStockHistoryDetail;
import com.nomadconnection.broccoli.activity.Level4ActivityAssetStockNewsy;
import com.nomadconnection.broccoli.adapter.AdtListViewAssetStockHistory;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.InvestApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockDetail;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockDetailDaily;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockInfo;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockList;
import com.nomadconnection.broccoli.data.Asset.RequestAssetStockDetail;
import com.nomadconnection.broccoli.data.Asset.ResponseStockDetail;
import com.nomadconnection.broccoli.data.Investment.InvestNewsyStockData;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.utils.ChartUtils;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.CustomYAxisValueFormatter;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.MyMarkerView;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.api.ServiceGenerator.createService;
import static com.nomadconnection.broccoli.config.APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_DELETE;

public class FragmentAssetStockDetail1 extends BaseFragment {
	
	private static String TAG = FragmentAssetStockDetail1.class.getSimpleName();


	/* Flexible Layout */
//	private TextView mSubInfo1;
//	private TextView mSubInfo2;
//	private ImageView mSubInfo3;
//	private TextView mSubInfo4;
	private LinearLayout mFlexibleBG;
	private TextView mSubTitleInfo1;
	private TextView mSubTitleInfo2;
	private ImageView mSubTitleInfo3;
	private TextView mSubTitleInfo4;
	private LinearLayout mInfoBtn1;
	private LinearLayout mInfoBtn2;
	private LinearLayout mInfoBtn3;
	private LinearLayout mInfoBtn4;
	private TextView mSubInfo1;
	private TextView mSubInfo2;
	private TextView mSubInfo3;
	private TextView mSubInfo4;
	private TextView mSubInfo5;
	private TextView mSubInfo6;
	private TextView mSubInfo7;
	private TextView mSubInfo8;
	private TextView mSubInfo9;
	private TextView mSubInfo10;
	private ImageView mNewsyBtn;

	/* Layout */
	private ListView mListView;
	private View mFlexibleArea;
	private AdtListViewAssetStockHistory mAdapter;


	/* Chart */
	private LineChart mChart;
//	private ArrayList<String> mLineXEntries;
//	private ArrayList<Entry> mLineYEntries;
	private ArrayList<String> mLineXEntries1Week;
	private ArrayList<Entry> mLineYEntries1Week;
	private ArrayList<String> mLineXEntries3Month;
	private ArrayList<Entry> mLineYEntries3Month;
	private ArrayList<String> mLineXEntries1Year;
	private ArrayList<Entry> mLineYEntries1Year;
	private ArrayList<String> mLineXEntries3Year;
	private ArrayList<Entry> mLineYEntries3Year;
//	private float mLowValue = 0;
//	private float mHighValue = 0;

	/* Data class */
	private BodyAssetStockInfo mInfoDataHeader;
	private ResponseStockDetail mInfoData;
	private boolean mStockPageState = false;
	private InvestApi mApi;
	private ArrayList<BodyAssetStockList> mStockHistoryList = new ArrayList<>();


	InterfaceEditOrDetail mInterFaceEditOrDetail;

	public FragmentAssetStockDetail1(BodyAssetStockInfo _InfoDataHeader, ResponseStockDetail data) {
		mInfoDataHeader = _InfoDataHeader;
		mInfoData = data;

		mLineXEntries1Week = new ArrayList<String>();
		mLineYEntries1Week = new ArrayList<Entry>();
		mLineXEntries3Month = new ArrayList<String>();
		mLineYEntries3Month = new ArrayList<Entry>();
		mLineXEntries1Year = new ArrayList<String>();
		mLineYEntries1Year = new ArrayList<Entry>();
		mLineXEntries3Year = new ArrayList<String>();
		mLineYEntries3Year = new ArrayList<Entry>();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_asset_stock_detail_1, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_REQUEST_CODE) {
			if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_SUCCESS){
				refreshData();
			} else if (resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_CANCLE){
			} else if (resultCode == ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_DELETE) {
				goBackActivityForResult(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_RESULT_CODE_DELETE);
			}
		}
	}
	



//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			/* getExtra */
			if (getActivity().getIntent().hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
				mStockPageState = true;
			}


		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try {
			/* Layout 선언 */
			mListView = (ListView)_v.findViewById(R.id.lv_fragment_asset_stock_detail_1_listview);
			mAdapter = new AdtListViewAssetStockHistory(getActivity(), mStockHistoryList);

			LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mFlexibleArea = (LinearLayout)inflater.inflate(R.layout.flexible_area_asset_stock_history, null);
			mListView.addHeaderView(mFlexibleArea);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(ItemClickListener);

			/* Flexible Layout */
			mFlexibleBG = (LinearLayout)_v.findViewById(R.id.ll_flexible_asset_stock_history);
			if (mStockPageState) {
				mFlexibleBG.setBackgroundColor(getResources().getColor(R.color.main_3_layout_bg));
			}
			mSubTitleInfo1 = (TextView)_v.findViewById(R.id.tv_flexible_asset_stock_history_subtitle_info_1);
			mSubTitleInfo2 = (TextView)_v.findViewById(R.id.tv_flexible_asset_stock_history_subtitle_info_2);
			mSubTitleInfo3 = (ImageView)_v.findViewById(R.id.iv_flexible_asset_stock_history_subtitle_info_3);
			mSubTitleInfo4 = (TextView)_v.findViewById(R.id.tv_flexible_asset_stock_history_subtitle_info_4);
			mInfoBtn1 = (LinearLayout)_v.findViewById(R.id.ll_flexible_area_asset_stock_history_info_btn_1);
			mInfoBtn2 = (LinearLayout)_v.findViewById(R.id.ll_flexible_area_asset_stock_history_info_btn_2);
			mInfoBtn3 = (LinearLayout)_v.findViewById(R.id.ll_flexible_area_asset_stock_history_info_btn_3);
			mInfoBtn4 = (LinearLayout)_v.findViewById(R.id.ll_flexible_area_asset_stock_history_info_btn_4);
			mInfoBtn1.setOnClickListener(BtnClickListener);
			mInfoBtn2.setOnClickListener(BtnClickListener);
			mInfoBtn3.setOnClickListener(BtnClickListener);
			mInfoBtn4.setOnClickListener(BtnClickListener);
			mSubInfo1 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_1);
			mSubInfo1.setTypeface(FontClass.setFont(getActivity()));
			mSubInfo2 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_2);
			mSubInfo2.setTypeface(FontClass.setFont(getActivity()));
			mSubInfo3 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_3);
			mSubInfo4 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_4);
			mSubInfo5 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_5);
			mSubInfo6 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_6);
			mSubInfo7 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_7);
			mSubInfo8 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_8);
			mSubInfo9 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_9);
			mSubInfo10 = (TextView)_v.findViewById(R.id.tv_flexible_area_asset_stock_history_sub_info_10);
			mNewsyBtn = (ImageView) _v.findViewById(R.id.fl_asset_stock_newsy_btn);
			mNewsyBtn.setOnClickListener(BtnClickListener);

			/* Chart */
			mChart = (LineChart)_v.findViewById(R.id.lc_flexible_area_asset_stock_history_chart);
			mChart.setDescription("");
			mChart.setNoDataTextDescription("");
			mChart.setNoDataText("");
			mChart.setBackgroundColor(Color.TRANSPARENT);
			mChart.setPinchZoom(false);	// scaling can now only be done on x- and y-axis separately
			mChart.setScaleEnabled(false);
			mChart.setDrawGridBackground(false);

//			// draw bars behind lines
//			mChart.setDrawOrder(ChartUtils.mDrawOrder);
			MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view_stock);
			mv.setChart(mChart);
			mChart.setMarkerView(mv);

//			mChart.fitScreen ();
//			mChart.setViewPortOffsets(0f, 0f, 0f, 0f);
//			mChart.setPadding(-20, 0, 20, 0);

			YAxis rightAxis = mChart.getAxisRight();
			rightAxis.setDrawGridLines(false);

			YAxis leftAxis = mChart.getAxisLeft();
//			leftAxis.setDrawAxisLine(true);
//			leftAxis.setAxisLineColor(Color.argb((int) (255 * 1), 125, 147, 255));
			leftAxis.setDrawAxisLine(false);
			leftAxis.setDrawGridLines(true);
//			leftAxis.setAxisLineColor(Color.argb((int) (255 * 1), 125, 147, 255));
			leftAxis.setAxisLineColor(Color.argb((int) (255 * 1), 255, 255, 255));
			leftAxis.setGridColor(Color.argb((int) (255 * 1), 255, 255, 255));
//			leftAxis.enableGridDashedLine(3f, 10f, 0f);
//			leftAxis.setGridLineWidth(1);
			leftAxis.enableGridDashedLine(10f, 10f, 0f);
			leftAxis.setValueFormatter(new CustomYAxisValueFormatter());
			leftAxis.setTextColor(Color.argb((int) (255 * 1), 255, 255, 255));
			leftAxis.setTextSize(9f);
			leftAxis.setStartAtZero(false);
//			leftAxis.setAxisLineWidth(10);
//			leftAxis.setSpaceBottom(100);
//			leftAxis.setSpaceTop(100);
			leftAxis.setXOffset(10);
//			leftAxis.setSpaceTop(30f);

			XAxis xAxis = mChart.getXAxis();
			xAxis.setDrawAxisLine(true);
			xAxis.setDrawGridLines(true);
			xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
			xAxis.setAxisLineColor(Color.argb((int) (255 * 1), 255, 255, 255));
			xAxis.setGridColor(Color.argb((int) (255 * 1), 255, 255, 255));
			xAxis.enableGridDashedLine(10f, 10f, 0f);
			xAxis.setTextColor(Color.argb((int) (255 * 1), 255, 255, 255));
			xAxis.setTextSize(9f);
			xAxis.setYOffset(10);
			xAxis.setLabelsToSkip(0);
			xAxis.setAvoidFirstLastClipping(false);

			// hide
			mChart.getAxisRight().setEnabled(false);
			mChart.getAxisLeft().setEnabled(true);
			mChart.getXAxis().setEnabled(true);
			mChart.getLegend().setEnabled(false);
			mChart.setExtraOffsets(0, 0, 5f, 5f);
			mChart.animateXY(1500, 1500);

			mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries1Week, mLineYEntries1Week, APP.ASSET_STOCK_HISTORY_CHART_USE));
			mChart.invalidate();

			mChart.setOnChartValueSelectedListener(mOnChartValueSelectedListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {

			mSubTitleInfo1.setText(mInfoDataHeader.mBodyAssetStockName);
			mInfoBtn1.setSelected(true);
			setNewBtnBackground(mInfoData.getNewsystock());

//			refreshHeaderView();
//			mSubInfo1.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mAssetStockDetailCount));
//			mSubInfo2.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mAssetStockDetailTotalSum));
//			mSubInfo3.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mAssetStockDetailHighPrice));
//			mSubInfo4.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mAssetStockDetailLowPrice));
//			mSubInfo5.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mAssetStockDetailTradeAmount));
//			long mTempTotalPrice = Long.valueOf(mInfoData.mAssetStockDetailTotalPrice);
//			mSubInfo6.setText(TransFormatUtils.getDecimalFormatRecvString(String.valueOf((long)mTempTotalPrice / 100000000))+"억");
//			mSubInfo7.setText(TransFormatUtils.getDecimalFormatRecvString(mInfoData.mAssetStockDetailTotalAmount));
//			mSubInfo8.setText(String.valueOf(mInfoData.mAssetStockDetailForeignerPercent)+"%");
//			mSubInfo9.setText(String.valueOf(mInfoData.mAssetStockDetailPer));
//			mSubInfo10.setText(String.valueOf(mInfoData.mAssetStockDetailPbr));
			refreshData();
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void setNewBtnBackground(InvestNewsyStockData data) {
		if (data != null && data.getTotal() != null) {
			String total = data.getTotal();
			int value = Integer.parseInt(total);
			if (value <= 19) {
				mNewsyBtn.setImageResource(R.drawable.weather_btn_01);
			} else if (value <= 39) {
				mNewsyBtn.setImageResource(R.drawable.weather_btn_02);
			} else if (value <= 59) {
				mNewsyBtn.setImageResource(R.drawable.weather_btn_03);
			} else if (value <= 79) {
				mNewsyBtn.setImageResource(R.drawable.weather_btn_04);
			} else {
				mNewsyBtn.setImageResource(R.drawable.weather_btn_05);
			}
			mNewsyBtn.setVisibility(View.VISIBLE);
		} else {
			mNewsyBtn.setVisibility(View.GONE);
		}
	}


//===============================================================================//
// Button
//===============================================================================//
	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

				case R.id.ll_flexible_area_asset_stock_history_info_btn_1:
					mInfoBtn1.setSelected(true);
					mInfoBtn2.setSelected(false);
					mInfoBtn3.setSelected(false);
					mInfoBtn4.setSelected(false);
					mChart.getXAxis().setLabelsToSkip(0);
//					mChart.getXAxis().setTextSize(5f);
					mChart.animateXY(1500, 1500);
					mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries1Week, mLineYEntries1Week, APP.ASSET_STOCK_HISTORY_CHART_USE));
					mChart.invalidate();
					break;

				case R.id.ll_flexible_area_asset_stock_history_info_btn_2:
					mInfoBtn1.setSelected(false);
					mInfoBtn2.setSelected(true);
					mInfoBtn3.setSelected(false);
					mInfoBtn4.setSelected(false);
					mChart.getXAxis().setLabelsToSkip((mLineXEntries3Month.size()/3)-2);
//					mChart.getXAxis().setTextSize(7f);
					mChart.animateXY(1500, 1500);
					mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries3Month, mLineYEntries3Month, APP.ASSET_STOCK_HISTORY_CHART_USE));
					mChart.invalidate();
					break;

				case R.id.ll_flexible_area_asset_stock_history_info_btn_3:
					mInfoBtn1.setSelected(false);
					mInfoBtn2.setSelected(false);
					mInfoBtn3.setSelected(true);
					mInfoBtn4.setSelected(false);
					mChart.getXAxis().setLabelsToSkip(180);
//					mChart.getXAxis().setTextSize(9f);
					mChart.animateXY(1500, 1500);
					mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries1Year, mLineYEntries1Year, APP.ASSET_STOCK_HISTORY_CHART_USE));
					mChart.invalidate();
					break;

				case R.id.ll_flexible_area_asset_stock_history_info_btn_4:
					mInfoBtn1.setSelected(false);
					mInfoBtn2.setSelected(false);
					mInfoBtn3.setSelected(false);
					mInfoBtn4.setSelected(true);
					mChart.getXAxis().setLabelsToSkip((mLineXEntries3Year.size()/3)-2);
//					mChart.getXAxis().setTextSize(9f);
					mChart.animateXY(1500, 1500);
					mChart.setData(ChartUtils.generateLineDataFill(mLineXEntries3Year, mLineYEntries3Year, APP.ASSET_STOCK_HISTORY_CHART_USE));
					mChart.invalidate();
					break;
				case R.id.fl_asset_stock_newsy_btn:
					showNewsyStock();
					break;
			default:
				break;
			}

		}
	};

	private void showNewsyStock() {
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.AssetStockDetailNewsy);
		Intent intent = new Intent(getActivity(), Level4ActivityAssetStockNewsy.class);
		intent.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER, mInfoDataHeader);
		if (mStockPageState) {
			intent.putExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND, true);
		}
		getActivity().startActivity(intent);
	}

	/**  Item Click Listener **/
	private AdapterView.OnItemClickListener ItemClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (position == 0 ) return;

			ArrayList<BodyAssetStockList> list = mInfoData.getStock().mAssetStockDetailList;

			if (position != 0 && list.get(position-1) == null) {
				return;
			}

			if(mStockPageState){
				startActivityForResult(new Intent(getActivity(), Level4ActivityAssetStockHistoryDetail.class)
						.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER, mInfoDataHeader)
						.putExtra(APP.INFO_DATA_ASSET_STOCK_HISTORY_PARCEL, list.get(position - 1))
						.putExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND, true)
						, APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_REQUEST_CODE);
			}
			else {
				startActivityForResult(new Intent(getActivity(), Level4ActivityAssetStockHistoryDetail.class)
						.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER, mInfoDataHeader)
						.putExtra(APP.INFO_DATA_ASSET_STOCK_HISTORY_PARCEL, list.get(position - 1))
						, APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_REQUEST_CODE);
			}

		}
	};

	private int mLastXIndex = 0;

	private OnChartValueSelectedListener mOnChartValueSelectedListener = new OnChartValueSelectedListener() {
		@Override
		public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
			if (e == null)
				return;
			mLastXIndex = h.getXIndex();
//			int mTempCount = 0;
//			if (mInfoBtn1.isSelected()) mTempCount = mLineXEntries1Week.size()-1;
//			else if (mInfoBtn2.isSelected()) mTempCount = mLineXEntries3Month.size()-1;
//			else if (mInfoBtn3.isSelected()) mTempCount = mLineXEntries1Year.size()-1;
//			else if (mInfoBtn4.isSelected()) mTempCount = mLineXEntries3Year.size()-1;

//			mChart.getMarkerView().setLayoutParams(new LinearLayout.LayoutParams((int)(mChart.getMarkerView().getChildAt(0).getWidth()*1.5), LinearLayout.LayoutParams.WRAP_CONTENT));
//			if (h.getXIndex() == 0){
//				mChart.getMarkerView().setLayoutParams(new LinearLayout.LayoutParams((int)(mChart.getMarkerView().getChildAt(0).getWidth()*1.5), LinearLayout.LayoutParams.WRAP_CONTENT));
//				mChart.getMarkerView().setGravity(Gravity.RIGHT);
//			}
//			else if (h.getXIndex() == mTempCount){
//				mChart.getMarkerView().setLayoutParams(new LinearLayout.LayoutParams((int)(mChart.getMarkerView().getChildAt(0).getWidth()*1.7), LinearLayout.LayoutParams.WRAP_CONTENT));
//				mChart.getMarkerView().setGravity(Gravity.LEFT);
//			}
//			else{
//				mChart.getMarkerView().setLayoutParams(new LinearLayout.LayoutParams((int)(mChart.getMarkerView().getChildAt(0).getWidth()*1), LinearLayout.LayoutParams.WRAP_CONTENT));
//				mChart.getMarkerView().setGravity(Gravity.CENTER);
//			}

		}

		@Override
		public void onNothingSelected() {
			mChart.highlightValue(mLastXIndex, 0);
		}
	};


//===============================================================================//
// NextStep
//===============================================================================//
	private void refreshData() {
		DialogUtils.showLoading(getActivity());											// 자산 로딩
		Call<ResponseStockDetail> mCall = createService(AssetApi.class).getStockDetail(
				new RequestAssetStockDetail(Session.getInstance(getActivity()).getUserId(),
						mInfoDataHeader.mBodyAssetStockCode)
		);

		mCall.enqueue(new Callback<ResponseStockDetail>() {
			@Override
			public void onResponse(Call<ResponseStockDetail> call, Response<ResponseStockDetail> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						NextStep(response.body());
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<ResponseStockDetail> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ElseUtils.network_error(getActivity());
			}
		});

	}

	/**  NextStep **/
	private void NextStep(ResponseStockDetail result){

		BodyAssetStockDetail detail = result.getStock();
		mInfoData = result;
		mStockHistoryList.clear();
		mStockHistoryList.addAll(detail.mAssetStockDetailList);
		refreshHeaderView();
		mAdapter.notifyDataSetChanged();
	}


//===============================================================================//
// NextStep
//===============================================================================//
	/* Refresh */
	private void refreshHeaderView() {
		setNewBtnBackground(mInfoData.getNewsystock());
		BodyAssetStockDetail detail = mInfoData.getStock();
		mSubTitleInfo2.setText(TransFormatUtils.getDecimalFormatRecvString(detail.mAssetStockDetailNowPrice));
		int mTempNowPrice = Integer.valueOf(detail.mAssetStockDetailNowPrice);
		int mTempPrevPrice = Integer.valueOf(detail.mAssetStockDetailPrevPrice);
		int mTempProfit = mTempNowPrice - mTempPrevPrice;
		if (mTempProfit > 0) {
			mSubTitleInfo3.setVisibility(View.VISIBLE);
			mSubTitleInfo3.setBackgroundResource(R.drawable.up_icon);
		}
		else if (mTempProfit == 0) {
			mSubTitleInfo3.setVisibility(View.GONE);
			mSubTitleInfo3.setBackgroundResource(0);
		}
		else {
			mSubTitleInfo3.setVisibility(View.VISIBLE);
			mSubTitleInfo3.setBackgroundResource(R.drawable.up_icon);
			mSubTitleInfo3.setRotation(180);
		}

		mSubTitleInfo4.setText(TransFormatUtils.getDecimalFormatRecvString(String.valueOf(Math.abs(mTempProfit))) +"  (" + (mTempProfit > 0 ? "+":"-") + String.format("%.1f", ((float)Math.abs(mTempProfit)/mTempPrevPrice)*100) + "%)");

		mSubInfo1.setText(TransFormatUtils.getDecimalFormatRecvString(detail.mAssetStockDetailCount));
		mSubInfo2.setText(TransFormatUtils.getDecimalFormatRecvString(detail.mAssetStockDetailTotalSum));
		mSubInfo3.setText(TransFormatUtils.getDecimalFormatRecvString(detail.mAssetStockDetailHighPrice));
		mSubInfo4.setText(TransFormatUtils.getDecimalFormatRecvString(detail.mAssetStockDetailLowPrice));
		mSubInfo5.setText(TransFormatUtils.getDecimalFormatRecvString(detail.mAssetStockDetailTradeAmount));
		long mTempTotalPrice = Long.valueOf(detail.mAssetStockDetailTotalPrice);
		mSubInfo6.setText(TransFormatUtils.getDecimalFormatRecvString(String.valueOf((long)mTempTotalPrice / 100000000))+"억");
		mSubInfo7.setText(TransFormatUtils.getDecimalFormatRecvString(detail.mAssetStockDetailTotalAmount));
		mSubInfo8.setText(String.valueOf(detail.mAssetStockDetailForeignerPercent)+"%");
		mSubInfo9.setText(String.valueOf(detail.mAssetStockDetailPer));
		mSubInfo10.setText(String.valueOf(detail.mAssetStockDetailPbr));

		refreshChart(detail.mAssetStockDetailListDaily);
	}





//===============================================================================//
// Private - return - up to the depth
//===============================================================================//
	private void goBackActivityForResult(int _resultCod){
		Intent intent = getActivity().getIntent();
		getActivity().setResult(_resultCod, intent);
		getActivity().finish();
	}




//===============================================================================//
// Chart
//===============================================================================//
	/**  Refresh chart **/
	private void refreshChart(ArrayList<BodyAssetStockDetailDaily> _ChartData){
		int Count = _ChartData.size();
		int mSpace = 0;

		mLineXEntries1Week.removeAll(mLineXEntries1Week);
		mLineYEntries1Week.removeAll(mLineYEntries1Week);
		mLineXEntries3Month.removeAll(mLineXEntries3Month);
		mLineYEntries3Month.removeAll(mLineYEntries3Month);
		mLineXEntries1Year.removeAll(mLineXEntries1Year);
		mLineYEntries1Year.removeAll(mLineYEntries1Year);
		mLineXEntries3Year.removeAll(mLineXEntries3Year);
		mLineYEntries3Year.removeAll(mLineYEntries3Year);

		// 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space
//		mLineYEntries1Week.add(0, new Entry(0, 0));
//		mLineYEntries3Month.add(0, new Entry(0,0));
//		mLineYEntries1Year.add(0, new Entry(0, 0));
//		mLineYEntries3Year.add(0, new Entry(0, 0));
		// 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space

		for (int i = 0; i < Count; i++) {
//			if (i ==0) {
//				mLowValue = Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice);
//				mHighValue = Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice);
//			}
//			else{
//				mLowValue = (mLowValue > Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice))?Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice):mLowValue;
//				mHighValue = (mHighValue < Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice))?Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice):mHighValue;
//			}

			if (i >= Count-7){
//				mLineXEntries1Week.add(_ChartData.get(i).mBodyAssetStockDetailDailyDate);
				mLineXEntries1Week.add(TransFormatUtils.transDateFormChart(_ChartData.get(i).mBodyAssetStockDetailDailyDate,"week"));
				mLineYEntries1Week.add(new Entry(Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice), i-((Count - 7))+mSpace));
			}
			if (i >= Count-90){
//				mLineXEntries3Month.add(_ChartData.get(i).mBodyAssetStockDetailDailyDate);
				mLineXEntries3Month.add(TransFormatUtils.transDateFormChart(_ChartData.get(i).mBodyAssetStockDetailDailyDate,"3month"));
				mLineYEntries3Month.add(new Entry(Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice), i-(Count - 90)+mSpace));

			}
			if (i >= Count-365){
//				mLineXEntries1Year.add(_ChartData.get(i).mBodyAssetStockDetailDailyDate);
				mLineXEntries1Year.add(TransFormatUtils.transDateFormChart(_ChartData.get(i).mBodyAssetStockDetailDailyDate,"1year"));
				mLineYEntries1Year.add(new Entry(Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice), i-(Count - 365)+mSpace));
			}
//			mLineXEntries3Year.add(_ChartData.get(i).mBodyAssetStockDetailDailyDate);
			mLineXEntries3Year.add(TransFormatUtils.transDateFormChart(_ChartData.get(i).mBodyAssetStockDetailDailyDate,"3year"));
			mLineYEntries3Year.add(new Entry(Float.valueOf(_ChartData.get(i).mBodyAssetStockDetailDailyPrice), i+mSpace));
		}

		// 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space
//		mLineXEntries1Week.set(0,"");
//		mLineXEntries3Month.set(0,"");
//		mLineXEntries1Year.set(0,"");
//		mLineXEntries3Year.set(0,"");
//		for (int i = 0; i < mSpace*2; i++) {
//			mLineXEntries1Week.add("");
//		}
//		mLineXEntries3Month.add("");
//		mLineXEntries3Month.add("");
//		mLineXEntries1Year.add("");
//		mLineXEntries1Year.add("");
//		mLineXEntries3Year.add("");
//		mLineXEntries3Year.add("");
//		mLineYEntries1Week.remove(0);
//		mLineYEntries3Month.remove(0);
//		mLineYEntries1Year.remove(0);
//		mLineYEntries3Year.remove(0);
		// 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space // 좌우 Space

		if (mInfoBtn1.isSelected()) BtnClickListener.onClick(mInfoBtn1);
		else if (mInfoBtn2.isSelected()) BtnClickListener.onClick(mInfoBtn2);
		else if (mInfoBtn3.isSelected()) BtnClickListener.onClick(mInfoBtn3);
		else if (mInfoBtn4.isSelected()) BtnClickListener.onClick(mInfoBtn4);
	}



}
