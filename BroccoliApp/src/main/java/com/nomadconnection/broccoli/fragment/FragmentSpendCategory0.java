package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.PagerAdapter;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.common.CustomPagerSlidingTabStripSide;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class FragmentSpendCategory0 extends BaseFragment {

	private static String TAG = "FragmentSpendCategory0";

	private static final int MAX_MONTH = 12;

	/* Pager */
	private PagerAdapter mPagerAdapter;
	private ViewPager mViewPager;
	private int mCurrentPageInx;

	/* Pager Title */
	private List<String> mPagerTitle;
	private String[] mTempMonthSpaceBefore;
	private String[] mTempMonthSpaceAfter;

	/* Child Fragment */
	private ArrayList<Fragment> alFragment;

	//private List<BodySpendCategoryList> mAlInfoDataList;
	//private String mSelecDate;
	InterfaceEditOrDetail mInterFaceEditOrDetail;

	public FragmentSpendCategory0() {
		// TODO Auto-generated constructor stub

		/* 넘어온 데이터 날자별 분기(1주일, 1개월, 3개월) */
		// 일단은 그냥 패스
	}
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_spend_category_0, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
	
	
	
	



//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */
			
			/* Layout 선언 */

				/* PagerFragment Tab Items */
			DecimalFormat decimalFormat = new DecimalFormat("00");
			Calendar calendar = Calendar.getInstance();
			int month = calendar.get(Calendar.MONTH)+1;
			set_after_space(); //month space add
			mPagerTitle = new ArrayList<String>();
			mPagerTitle.add(decimalFormat.format(month));
			List<String> targetMonth = new ArrayList<String>();
			targetMonth.add(ElseUtils.getYearMonth(calendar.getTime()));
			for (int i=1; i < MAX_MONTH; i++) {
				calendar.add(Calendar.MONTH, -1);
				month = calendar.get(Calendar.MONTH)+1;
				mPagerTitle.add(decimalFormat.format(month));
				targetMonth.add(ElseUtils.getYearMonth(calendar.getTime()));
			}
			set_before_space(calendar);	//month space add
			Collections.reverse(mPagerTitle);
			Collections.reverse(targetMonth);
			//mPagerTitle = getResources().getStringArray(R.array.pager_fragment_tab_items_spend_month);

			alFragment = new ArrayList<Fragment>();
			for (String target : targetMonth) {
				Bundle bundle = new Bundle();
				bundle.putString(Const.DATA, target);
				FragmentSpendCategory0A fragment = new FragmentSpendCategory0A();
				fragment.setArguments(bundle);
				alFragment.add(fragment);
			}

			String[] title = new String[MAX_MONTH];
			title = mPagerTitle.toArray(title);


			/* Pager */
			mPagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager(), alFragment, title);
			mViewPager = (ViewPager) _v.findViewById(R.id.pager_fragment_spend_category_0_body);
			mViewPager.setAdapter(mPagerAdapter);
			mViewPager.setOffscreenPageLimit(alFragment.size());
			mViewPager.setCurrentItem(alFragment.size() -1);
			mCurrentPageInx = 0;


			CustomPagerSlidingTabStripSide tabs = (CustomPagerSlidingTabStripSide) _v.findViewById(R.id.pager_fragment_spend_category_0_tabs);
			tabs.setSpace(APP.CUSTOM_PAGE_SLIDING_TAB_STRIP_SIDE_TYPE_ELSE, 2, mTempMonthSpaceBefore, mTempMonthSpaceAfter);
			tabs.setShouldExpand(true);
			tabs.setViewPager(mViewPager);
			tabs.setOnPageChangeListener(mPageChangeListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	



//=========================================================================================//
// ToolBar - Image/Text/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
//	public void onBtnClickNaviImgTxtTxtBar(View _view) {
//		try
//		{
//			switch (_view.getId()) {
//
//			case R.id.ll_navi_img_txt_back:
//				getActivity().finish();
//				break;
//
//			default:
//				break;
//			}
//		}
//		catch(Exception e)
//		{
//			if(APP._DEBUG_MODE_)
//				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());
//		}
//	}








//===============================================================================//
// View Pager
//===============================================================================//

	/**  View Page Change Listener **/
	private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {

		@Override
		public void onPageSelected(int _position) {
//			String mSelectTitle = mPagerTitle.get(_position) + getString(R.string.common_month);
//			mPagerTitle.set(_position, mSelectTitle);
//			mPagerAdapter.notifyDataSetChanged();
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	};


	private void set_after_space() {
		int monthly;
		Calendar calendar = Calendar.getInstance();

		mTempMonthSpaceAfter = new String[2];
		DecimalFormat decimalFormat = new DecimalFormat("00");

		for(int s = 0; s < mTempMonthSpaceAfter.length; s++){
			calendar.add(Calendar.MONTH, +1);
			monthly = calendar.get(Calendar.MONTH) + 1;
			mTempMonthSpaceAfter[s] = decimalFormat.format(monthly);
		}
	}

	private void set_before_space(Calendar calendar) {
		int monthly;

		mTempMonthSpaceBefore = new String[2];
		DecimalFormat decimalFormat = new DecimalFormat("00");

		for(int s = 1; s >= 0; s--){
			calendar.add(Calendar.MONTH, -1);
			monthly = calendar.get(Calendar.MONTH)+1;
			mTempMonthSpaceBefore[s] = decimalFormat.format(monthly);
		}
	}


}
