package com.nomadconnection.broccoli.fragment.spend;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.spend.Level3ActivitySpendCardDetail;
import com.nomadconnection.broccoli.adapter.CommonAdapter;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.RequestCardList;
import com.nomadconnection.broccoli.data.Spend.ResponseCardList;
import com.nomadconnection.broccoli.data.Spend.SpendCardUserListData;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by YelloHyunminJang on 2017. 3. 20..
 */

public class FragmentSpendCardList extends BaseFragment {

    private static final int MAX_FAVORITE_COUNT = 2;
    public static final int REQUEST_CODE_DETAIL = 1;
    public static final String ENABLE_COMPANY = "ENABLE_COMPANY";
    public static final String RECOMMEND_YN = "RECOMMEND_YN";

    private ListView mListView;
    private CommonAdapter mAdapter;
    private LayoutInflater mInflater;
    private ArrayList<SpendCardUserListData> mData;
    private ArrayList<String> mEnableCompany;
    private InterfaceEditOrDetail mInterfaceListener;
    private int mRecommendYn = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spend_card_list, null);
        init(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mInterfaceListener = (InterfaceEditOrDetail) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement InterfaceEditOrDetail");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_DETAIL) {
            if (resultCode == RESULT_OK) {
                mInterfaceListener.onRefresh();
            }
        }
    }

    private void init(View view) {
        ((BaseActivity)getActivity()).sendTracker(AnalyticsName.SpendCardInfoList);
        initWidget(view);
        initData();
    }

    private void initWidget(View view) {
        mInflater = LayoutInflater.from(getActivity());
        mListView = (ListView) view.findViewById(R.id.lv_fragment_spend_card_list);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SpendCardUserListData data = mData.get(position);
                if (data.card != null) {
                    Intent intent = new Intent(getActivity(), Level3ActivitySpendCardDetail.class);
                    intent.putExtra(Const.DATA, data);
                    intent.putExtra(ENABLE_COMPANY, mEnableCompany);
                    intent.putExtra(RECOMMEND_YN, mRecommendYn);
                    startActivityForResult(intent, REQUEST_CODE_DETAIL);
                }
            }
        });
        mAdapter = new CommonAdapter(itemCreateListener);
        mListView.setAdapter(mAdapter);
        mAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
            }
        });
    }

    private void initData() {
        Bundle bundle = getArguments();
        mData = (ArrayList<SpendCardUserListData>) bundle.getSerializable(Const.DATA);
        mEnableCompany = (ArrayList<String>) bundle.getSerializable(ENABLE_COMPANY);
        mRecommendYn = bundle.getInt(RECOMMEND_YN, 0);
        setData(mData);
    }

    private void getData() {
        DialogUtils.showLoading(getActivity());
        Call<JsonElement> call = ServiceGenerator.createService(SpendApi.class).getMyCardList();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                DialogUtils.dismissLoading();
                JsonElement element = response.body();
                JsonObject object = element.getAsJsonObject();
                JsonArray array = object.getAsJsonArray("cardList");
                if (array != null) {
                    JsonArray companyList = object.getAsJsonArray("cardCompanyList");
                    Type stringType = new TypeToken<ArrayList<String>>() {}.getType();
                    mEnableCompany = new Gson().fromJson(companyList, stringType);
                    Type listType = new TypeToken<List<SpendCardUserListData>>() {}.getType();
                    mData = new Gson().fromJson(array, listType);
                    setData(mData);
                } else {
                    ErrorUtils.parseError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
    }

    private void setData(List<SpendCardUserListData> list) {
        mAdapter.setData(list);
        mAdapter.notifyDataSetChanged();
    }

    private List<ResponseCardList> mCardInfoList;
    private SpendCardUserListData mSelectCardInfo;

    private void getCardList(String companyCode){
        DialogUtils.showLoading(getActivity());	//소비 로딩 추가
        RequestCardList mRequestCardList = new RequestCardList();
        mRequestCardList.setUserId(Session.getInstance(getActivity()).getUserId());
        mRequestCardList.setCompanyCode(companyCode);

        SpendApi api = ServiceGenerator.createService(SpendApi.class);
        Call<List<ResponseCardList>> call = api.getCardList(mRequestCardList);
        call.enqueue(new Callback<List<ResponseCardList>>() {
            @Override
            public void onResponse(Call<List<ResponseCardList>> call, Response<List<ResponseCardList>> response) {
                DialogUtils.dismissLoading();	//소비 로딩 추가
                mCardInfoList = new ArrayList<ResponseCardList>();
                ResponseCardList data = new ResponseCardList();
                data.setCardCode("");
                data.setCardName(getActivity().getString(R.string.common_text_no));
                mCardInfoList.add(data);
                for (int r = 0; r < response.body().size(); r++) {
                    mCardInfoList.add(response.body().get(r));
                }

                showCardList();//리스트 팝업 부르기
            }

            @Override
            public void onFailure(Call<List<ResponseCardList>> call, Throwable t) {
                DialogUtils.dismissLoading();	//소비 로딩 추가
                ElseUtils.network_error(getActivity());
            }
        });
    }


    private void changeCardMatching(String cardId, String cardCode) {
        DialogUtils.dismissLoading();	//소비 로딩 추가
        HashMap<String, Object> map = new HashMap<>();
        map.put("cardId", Long.valueOf(cardId));
        map.put("cardCode", cardCode);

        SpendApi api = ServiceGenerator.createService(SpendApi.class);
        Call<Result> call = api.setMatchingCard(map);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();	//소비 로딩 추가
                if(response.body().isOk()){
                    if (mInterfaceListener != null) {
                        mInterfaceListener.onRefresh();
                    }
                } else {
                    Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();	//소비 로딩 추가
                ElseUtils.network_error(getActivity());
            }
        });

    }

    private void showCardList(){
        DialogUtils.showDialogCardSelectList(getActivity(), mCardInfoList, null, new ExpandableListView.OnChildClickListener(){

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ResponseCardList mCardList = (ResponseCardList) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);
                String cardId;
                if (id == -1) {
                    cardId = "";
                } else {
                    cardId = String.valueOf(mSelectCardInfo.cardId);
                    changeCardMatching(cardId, mCardList.getCardCode());
                }
                return false;
            }

        }).show();
    }

    private void setFavorite(long cardId, int bookmarkYn) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("cardId", cardId);
        body.put("bookmarkYn", bookmarkYn);
        Call<Result> call = ServiceGenerator.createService(SpendApi.class).setFavoriteCard(body);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body() != null && response.message().equalsIgnoreCase(Const.OK)) {
                    if (!response.body().isOk()) {
                        Toast.makeText(getActivity(), getString(R.string.spend_budget_favorite_category_reg_error), Toast.LENGTH_SHORT).show();
                    } else {
                        if (mInterfaceListener != null) {
                            mInterfaceListener.onRefresh();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    private CommonAdapter.AdapterItemCreateListener itemCreateListener = new CommonAdapter.AdapterItemCreateListener() {

        @Override
        public View getView(List<?> list, int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_list_item_spend_card, null);
                holder = new ViewHolder();
                holder.mLogo = (ImageView) convertView.findViewById(R.id.iv_row_spend_card_logo);
                holder.mFavorite = (ImageView) convertView.findViewById(R.id.iv_row_spend_card_favorite);
                holder.mTitle = (TextView) convertView.findViewById(R.id.tv_row_spend_card_title);
                holder.mMemo = (TextView) convertView.findViewById(R.id.tv_row_spend_card_memo);
                holder.mGraph = (ProgressBar) convertView.findViewById(R.id.progress_spend_card_graph);
                holder.mUsedRecord = (TextView) convertView.findViewById(R.id.tv_row_spend_card_used);
                holder.mRequirement = (TextView) convertView.findViewById(R.id.tv_row_spend_card_requirement);
                holder.mRegBtn = convertView.findViewById(R.id.fl_spend_card_setting);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            SpendCardUserListData userCard = (SpendCardUserListData) mAdapter.getItem(position);

            String title = null;
            holder.mFavorite.setTag(userCard);
            holder.mRegBtn.setTag(userCard);
            long requirement = 0;
            if (userCard.card != null) {
                holder.mLogo.setAlpha(1f);
                holder.mTitle.setTextColor(getResources().getColor(R.color.common_maintext_color));
                holder.mRequirement.setTextColor(getResources().getColor(R.color.common_maintext_color));
                title = userCard.card.cardName;
                holder.mFavorite.setVisibility(View.VISIBLE);
                holder.mRegBtn.setVisibility(View.GONE);
                if (userCard.bookmarkYn > 0) {
                    holder.mFavorite.setSelected(true);
                } else {
                    holder.mFavorite.setSelected(false);
                }
                holder.mFavorite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int count = 0;
                        SpendCardUserListData target = (SpendCardUserListData) v.getTag();
                        for (SpendCardUserListData data : mData) {
                            if (data.bookmarkYn > 0) {
                                count++;
                            }
                        }
                        if (count > MAX_FAVORITE_COUNT || count >= MAX_FAVORITE_COUNT && target.bookmarkYn == 0) {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.spend_card_favorite_reg_fail), Toast.LENGTH_SHORT).show();
                        } else {
                            setFavorite(target.cardId, target.bookmarkYn == 0 ? 1:0);
                        }
                    }
                });
                requirement = userCard.card.requirement;
                holder.mRequirement.setText(holder.mRequirement.getResources().getString(R.string.spend_card_need)+" "+ ElseUtils.getDecimalFormat(requirement));
            } else {
                holder.mLogo.setAlpha(0.25f);
                holder.mTitle.setTextColor(getResources().getColor(R.color.common_subtext_color));
                holder.mRequirement.setTextColor(getResources().getColor(R.color.common_subtext_color));
                title = userCard.cardName;
                holder.mFavorite.setVisibility(View.GONE);
                holder.mRegBtn.setVisibility(View.VISIBLE);
                holder.mRegBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSelectCardInfo = (SpendCardUserListData) v.getTag();
                        getCardList(mSelectCardInfo.cmpnyCode);
                    }
                });
                holder.mRequirement.setText(holder.mRequirement.getResources().getString(R.string.spend_card_need)+" "+0);
                holder.mMemo.setText(userCard.memo);
                holder.mUsedRecord.setText(holder.mUsedRecord.getResources().getString(R.string.spend_card_usage)+" "+ElseUtils.getDecimalFormat(userCard.expenseAmt));
            }
            holder.mLogo.setImageResource(ElseUtils.createCardLogoItem(userCard.cmpnyCode));
            holder.mTitle.setText(title);
            holder.mMemo.setText(userCard.memo);
            holder.mUsedRecord.setText(holder.mUsedRecord.getResources().getString(R.string.spend_card_usage)+" "+ElseUtils.getDecimalFormat(userCard.expenseAmt));
            float value = 0;
            if (requirement != 0) {
                value = (long)( (double)userCard.expenseAmt / (double)requirement * 100.0 );
            } else if (requirement == 0 && userCard.expenseAmt == 0) {
                value = 0;
            } else {
                value = 999;
            }
            if (userCard.card != null) {
                if (0 >= value) {
                    if (userCard.card != null) {
                        holder.mUsedRecord.setTextColor(holder.mUsedRecord.getResources().getColor(R.color.common_subtext_color));
                    } else {
                        holder.mUsedRecord.setTextColor(holder.mUsedRecord.getResources().getColor(R.color.common_subtext_color));
                    }
                    holder.mGraph.setProgress(0);
                } else if (value < 100) {
                    if (userCard.card != null) {
                        holder.mUsedRecord.setTextColor(holder.mUsedRecord.getResources().getColor(R.color.spend_card_progress_font_color));
                    } else {
                        holder.mUsedRecord.setTextColor(holder.mUsedRecord.getResources().getColor(R.color.common_subtext_color));
                    }
                    holder.mUsedRecord.setTextColor(holder.mUsedRecord.getResources().getColor(R.color.spend_card_progress_font_color));
                    holder.mGraph.setProgress(((int) value));
                    Drawable drawable = holder.mGraph.getProgressDrawable();
                    if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
                        shapeDrawable.setColorFilter(holder.mGraph.getResources().getColor(R.color.spend_card_graph_progress_color), PorterDuff.Mode.SRC_IN);
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(holder.mGraph.getResources().getColor(R.color.spend_card_graph_progress_color));
                    }
                } else {
                    if (userCard.card != null) {
                        holder.mUsedRecord.setTextColor(holder.mUsedRecord.getResources().getColor(R.color.spend_card_graph_over_color));
                    } else {
                        holder.mUsedRecord.setTextColor(holder.mUsedRecord.getResources().getColor(R.color.common_subtext_color));
                    }
                    holder.mGraph.setProgress(holder.mGraph.getMax());
                    Drawable drawable = holder.mGraph.getProgressDrawable();
                    if (drawable instanceof LayerDrawable) {
                        LayerDrawable layerDrawable = (LayerDrawable) drawable;
                        ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
                        shapeDrawable.setColorFilter(holder.mGraph.getResources().getColor(R.color.spend_card_graph_over_color), PorterDuff.Mode.SRC_IN);
                    } else if (drawable instanceof GradientDrawable) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        gradientDrawable.setColor(holder.mGraph.getResources().getColor(R.color.spend_card_graph_over_color));
                    }
                }
            } else {
                holder.mUsedRecord.setTextColor(holder.mUsedRecord.getResources().getColor(R.color.common_subtext_color));
                holder.mGraph.setProgress(0);
                Drawable drawable = holder.mGraph.getProgressDrawable();
                if (drawable instanceof LayerDrawable) {
                    LayerDrawable layerDrawable = (LayerDrawable) drawable;
                    ClipDrawable shapeDrawable = (ClipDrawable) layerDrawable.getDrawable(1);
                    shapeDrawable.setColorFilter(holder.mGraph.getResources().getColor(R.color.common_subtext_color), PorterDuff.Mode.SRC_IN);
                } else if (drawable instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                    gradientDrawable.setColor(holder.mGraph.getResources().getColor(R.color.common_subtext_color));
                }
            }

            return convertView;
        }

        class ViewHolder {
            private ImageView mLogo;
            private ImageView mFavorite;
            private TextView mTitle;
            private TextView mMemo;
            private ProgressBar mGraph;
            private TextView mUsedRecord;
            private TextView mRequirement;
            private View mRegBtn;
        }
    };
}
