package com.nomadconnection.broccoli.fragment.demo;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewDemoSpend;
import com.nomadconnection.broccoli.api.DemoApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Demo.TrialExpense;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.interf.OnRefreshListener;
import com.nomadconnection.broccoli.utils.ChartUtils;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.MyMarkerView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDemoSpend extends Fragment implements InterfaceFragmentInteraction {

	/* Layout */
	public ListView mListView;
	/* Flexible Layout */
	private TextView mSpendTitle;
	private TextView mSpendCost;
	private CombinedChart mChart;
	private View mFlexibleArea;
	private AdtListViewDemoSpend mAdapter;
	private List<TrialExpense> mData;
	private int mLastXIndex = 0;
	private CombinedData mCombinedData;
	private ArrayList<Entry> mLineEntries;
	private ArrayList<Entry> mCircleEntries;
	private ArrayList<BarEntry> mBarEntries;
	private int mAge = 20;
	private String mGender = "M";

	private View mInfoBtn1;
	private View mInfoBtn2;
	

	private OnItemClickListener ItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			DialogUtils.showDialogDemoFinish(getActivity());
		}
	};

	private View.OnClickListener mOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			int id = v.getId();
			switch (id) {
				case R.id.fl_flexible_area_main_2_info_btn_1:
					mInfoBtn1.setSelected(true);
					mInfoBtn2.setSelected(false);
					chart_data_set();
					break;

				case R.id.fl_flexible_area_main_2_info_btn_2:
					mInfoBtn1.setSelected(false);
					mInfoBtn2.setSelected(true);
					chart_data_set();
					mOnChartValueSelectedListener.onValueSelected(mLineEntries.get(mLineEntries.size()-1), 0, new Highlight(mLineEntries.size()-1,0));
					break;
			}
		}
	};

	private OnChartValueSelectedListener mOnChartValueSelectedListener = new OnChartValueSelectedListener() {
		@Override
		public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
			if (e == null)
				return;
			mLastXIndex = h.getXIndex();
			mCombinedData.setData(ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_1));
			mChart.setData(mCombinedData);
			mChart.getMarkerView().getChildAt(0).setVisibility(mLineEntries.get(h.getXIndex()).getVal() == 0 ? View.GONE:View.VISIBLE);
			mChart.invalidate();
		}

		@Override
		public void onNothingSelected() {
			mChart.highlightValue(mLastXIndex, 0);
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_main2, null);

		init(view);

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		getSpendDemoData();
	}

	private void init(View view) {
		mData = new ArrayList<TrialExpense>();
		initWidget(view);
		initData();
	}

	private void initData() {

		Bundle bundle = getArguments();
		mAge = bundle.getInt(Const.DEMO_AGE);
		mGender = bundle.getString(Const.DEMO_GENDER);

		mSpendTitle.setText("월 평균 소비");
		getSpendDemoData();
	}

//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private void initWidget(View view) {
		mListView = (ListView)view.findViewById(R.id.lv_fragment_main_2_listview);
		mAdapter = new AdtListViewDemoSpend(getActivity(), null, new OnRefreshListener() {
			@Override
			public void refresh() {
				getSpendDemoData();
			}
		});

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			mListView.setNestedScrollingEnabled(true);
		}

    		/* btn height만큼 채우기 */
		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mFlexibleArea = (LinearLayout)inflater.inflate(R.layout.flexible_area_main_2, null);
		mListView.addHeaderView(mFlexibleArea);
		mInfoBtn1 = mFlexibleArea.findViewById(R.id.fl_flexible_area_main_2_info_btn_1);
		mInfoBtn2 = mFlexibleArea.findViewById(R.id.fl_flexible_area_main_2_info_btn_2);
		mInfoBtn2.setOnClickListener(mOnClickListener);
		mInfoBtn1.setVisibility(View.GONE);

		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(ItemClickListener);

			/* Flexible Layout */
		mSpendTitle = (TextView)view.findViewById(R.id.tv_fragment_main_2_title);
		mSpendCost = (TextView)view.findViewById(R.id.tv_fragment_main_2_cost);
		mSpendCost.setTypeface(FontClass.setFont(getActivity()));

			/* Chart */
		mChart = (CombinedChart) view.findViewById(R.id.chart_bar_and_line_chart);

		mInfoBtn2.setSelected(true);
	}


//===============================================================================//
// reset data
//===============================================================================//
	private void reset_data(){
		mAdapter.notifyDataSetChanged();
		mSpendCost.setText(ElseUtils.getDecimalFormat(mData.get(mData.size()-1).expense));
	}



//===============================================================================//
// chart data set
//===============================================================================//	
	private void chart_data_set() {
		int chartExistLength = -1;
		float budgetValue = 0;
		float totalValue = 0;
		String budgetText = null;
		float averageValue = 0;
		long maxValue = 0;
		String averageText = null;
		String[] chartXValues = new String[0];

		if (mData != null) {
			long monthlySumValue = 0;
			int validCount = 0;
			String[] xValues = new String[mData.size()];
			chartExistLength = mData.size() - 1;
			for (int i=0; i < mData.size(); i++) {
				TrialExpense data = mData.get(i);
				if (data.expense > 0) {
					if (data.expense > maxValue) {
						maxValue = data.expense;
					}
					validCount++;
					monthlySumValue += data.expense;
				}
				String date = data.month;
				if (date != null && !TextUtils.isEmpty(date)) {
					xValues[i] = date.substring(4);
				}
			}
			if (monthlySumValue > 0) {
				monthlySumValue = monthlySumValue/validCount;
			}
			averageValue = monthlySumValue;
			double fixValue = Math.floor(monthlySumValue * 10d) / 10d;
			averageText = String.format(getString(R.string.spend_average), ElseUtils.getDecimalFormat((long) fixValue));
			chartXValues = xValues;
		}

		/* Spend Data */
		mCombinedData = new CombinedData(chartXValues);

		mChart.setDescription("");
		mChart.setNoDataText("");
		mChart.setBackgroundColor(Color.TRANSPARENT);
		mChart.setDrawGridBackground(false);
		mChart.setTouchEnabled(false);
		mChart.setScaleEnabled(false);
		mChart.setDrawBarShadow(false);

		// draw bars behind lines
		mChart.setDrawOrder(ChartUtils.mDrawOrder);

		YAxis rightAxis = mChart.getAxisRight();
		LimitLine ll1 = new LimitLine(averageValue, averageText);
		ll1.setLineWidth(1f);
		ll1.setLineColor(Color.rgb(255, 255, 255));
		ll1.enableDashedLine(3f, 3f, 0f);
		ll1.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
		ll1.setTextColor(Color.rgb(255, 255, 255));
		ll1.setTextSize(13f);

		if(rightAxis.getLimitLines() != null){
			rightAxis.removeAllLimitLines();
		}

		rightAxis.setAxisMaxValue(maxValue*1.1f);

		rightAxis.addLimitLine(ll1);
		rightAxis.setDrawGridLines(false);

		YAxis leftAxis = mChart.getAxisLeft();
		leftAxis.setDrawGridLines(false);
		leftAxis.setAxisMaxValue(maxValue*1.1f);

		XAxis xAxis = mChart.getXAxis();
		xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
		xAxis.setDrawAxisLine(false);
		xAxis.setDrawGridLines(false);
		xAxis.setSpaceBetweenLabels(1);
		xAxis.setTextSize(11f);
		xAxis.setTextColor(Color.argb(90, 255, 255, 255));


		mLineEntries = new ArrayList<>();
		mCircleEntries = new ArrayList<>();
		mBarEntries = new ArrayList<>();

		if(chartExistLength !=  -1){
			for(int i = 0; i < chartXValues.length; i++){
				TrialExpense mChartData = mData.get(i);
				float mSum = Float.valueOf(mChartData.expense);
				mLineEntries.add(new Entry(mSum, i));
				if (chartExistLength == i) {
					mCircleEntries.add(new Entry(mSum, i));
				}
				mBarEntries.add(new BarEntry(mSum, i));
			}

			LineData lineData = null;
			BarData barData = null;
			lineData = ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_1);
			MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
			mv.setChart(mChart);
			mChart.setTouchEnabled(true);
			mChart.setMarkerView(mv);
			mChart.setOnChartValueSelectedListener(mOnChartValueSelectedListener);
			barData = ChartUtils.generateBarData(mBarEntries, APP.MAIN_LIST_CHART_USE_1);
			barData.setHighlightEnabled(true);
			if (mCircleEntries.size() > 0) {
				LineDataSet set = new LineDataSet(mCircleEntries, "Circle DataSet");
				set.setColor(Color.argb((int) (255 * 0), 255, 255, 255));
				set.setLineWidth(1.5f);
				set.setCircleSize(5f);
				set.setCircleColor(Color.argb((int) (255 * 1), 255, 255, 255));
				set.setFillColor(Color.rgb(255, 255, 255));
				set.setCircleColorHole(Color.argb((int) (255 * 1), 203, 124, 229));
				set.setDrawCircleHole(true);
				set.setDrawCircles(true);
				set.setDrawHighlightIndicators(false);
				set.setDrawValues(false);
				lineData.addDataSet(set);
			}
			mCombinedData.setData(lineData);
			mCombinedData.setData(barData);

		}

		// hide
		mChart.getAxisRight().setEnabled(false);
		mChart.getAxisLeft().setEnabled(false);
		mChart.getLegend().setEnabled(false);
		mChart.animateY(1500);

		mChart.setData(mCombinedData);
		mChart.notifyDataSetChanged();
		mChart.invalidate();
	}
	
	
	
//===============================================================================//
// Get Monthly last day
//===============================================================================//
	private int LastDay(){
		Calendar calendar = Calendar.getInstance();

		int year = calendar.get(calendar.YEAR);
		int month = calendar.get(calendar.MONTH);
		calendar.set(year, month, 1);

		int DayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return DayOfMonth;
	}




//===============================================================================//
// Get Spend main check
//===============================================================================//
	private void getSpendDemoData(){
		DialogUtils.showLoading(getActivity());
		Call<JsonElement> call = ServiceGenerator.createServiceForDemo(DemoApi.class).getExpenseTrial(mAge, mGender);
		call.enqueue(new Callback<JsonElement>() {
			@Override
			public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						JsonElement element = response.body();
						JsonObject object = element.getAsJsonObject();
						if (object != null && object.has("expenseList")) {
							JsonArray array = object.getAsJsonArray("expenseList");
							Type listType = new TypeToken<List<TrialExpense>>() {}.getType();
							mData = new Gson().fromJson(array, listType);
							setData(mData);
						} else {
							ErrorUtils.parseError(response);
						}
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<JsonElement> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void setData(List<TrialExpense> list) {
		if (list != null &&  list.size() > 0) {
			mAdapter.setData(list.get(list.size()-1));
			chart_data_set();
			reset_data();
		}
	}

	@Override
	public void reset(boolean flag) {
		if (mListView != null) {
			mListView.setSelection(0);
		}
	}

	@Override
	public boolean canScrollUp() {
		boolean ret = false;
		if (mListView != null) {
			ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
		}
		return ret;
	}

	@Override
	public boolean canScrollDown() {
		return false;
	}
}
