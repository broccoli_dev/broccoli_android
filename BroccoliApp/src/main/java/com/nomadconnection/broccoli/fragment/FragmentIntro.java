package com.nomadconnection.broccoli.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;

import static android.app.Activity.RESULT_OK;

public class FragmentIntro extends BaseFragment {

	private ImageView mImage;
	private Button mBtn;
	
	private int mPage = 0;
	
	/** Constructor **/
	public FragmentIntro() { 
		this(0);
	}
	
	public FragmentIntro(int _page) {
		mPage = _page;
		setRetainInstance(true);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("mPage", mPage);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (savedInstanceState != null)
			mPage = savedInstanceState.getInt("mPage");
		
		View v = inflater.inflate(R.layout.fragment_intro, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
	
	
	
	



//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			mImage = (ImageView)_v.findViewById(R.id.iv_fragment_intro_image);
			mBtn = (Button) _v.findViewById(R.id.btn_confirm);
			mBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					sendResult();
				}
			});
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			switch (mPage) {
			case 0:
//				mTxt1.setText("연결 하다");
//				mTxt2.setText("나의 모든 은행계좌, 카드, 대출, 주식을\n한곳에 연결한다.");
				((BaseActivity)getActivity()).sendTracker(AnalyticsName.IntroAsset);
				mImage.setImageResource(R.drawable.tutorial_01);
				break;
				
			case 1:
//				mTxt1.setText("간편 하다");
//				mTxt2.setText("최초 한 번의 등록으로, 나의 모든 금융 자산을\n간편하게 확인한다.");
				((BaseActivity)getActivity()).sendTracker(AnalyticsName.IntroSpend);
				mImage.setImageResource(R.drawable.tutorial_02);
				break;
				
			case 2:
//				mTxt1.setText("스마트 하다");
//				mTxt2.setText("청구서 관리, 저축목표 달성까지\n자동으로 관리한다.");
				((BaseActivity)getActivity()).sendTracker(AnalyticsName.IntroCard);
				mImage.setImageResource(R.drawable.tutorial_03);
				break;
				
			case 3:
//				mTxt1.setText("세상에 없던 개인자산 모니터");
//				mTxt2.setText("브로콜리를 만나보세요!");
				((BaseActivity)getActivity()).sendTracker(AnalyticsName.IntroEtc);
				mImage.setImageResource(R.drawable.tutorial_04);
				mBtn.setVisibility(View.VISIBLE);
				break;

			default:
				break;
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void sendResult() {
		Intent intent = new Intent();
		getActivity().setResult(RESULT_OK, intent);
		getActivity().finish();
	}


}
