package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivitySpendHistoryDetail;
import com.nomadconnection.broccoli.adapter.AdtListViewSpendCategoryDetail;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Spend.RequestSpendCategoryDetail;
import com.nomadconnection.broccoli.data.Spend.ResponseSpendCategoryDetail;
import com.nomadconnection.broccoli.data.Spend.SpendMonthlyInfo;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.FontClass;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSpendCategoryDetail0 extends BaseFragment implements AdapterView.OnItemClickListener{
	
	private static String TAG = "FragmentSpendCategoryDetail0";

	/* Layout */
	private LinearLayout mSpendCategory;
	private TextView mSpendCategoryName;
	private TextView mSpendCategoryCost;

	private LinearLayout mSpendCategoryBudget;
	private ProgressBar mSpendCategoryBudgetProgress;
	private TextView mSpendCategoryBudgetPercent;
	private TextView mSpendCategorySpendName;
	private TextView mSpendCategorySpendCost;
	private TextView mSpendCategoryBudgetName;
	private TextView mSpendCategoryBudgetCost;

	private LinearLayout mEmpty;
	private ImageView mEmptyIcon;
	private TextView mEmptyTitle;

	private ListView mListView;
	private AdtListViewSpendCategoryDetail mAdapter;
	private List<SpendMonthlyInfo> mAlInfoDataList;
	private ResponseSpendCategoryDetail mResponseSpendCategoryDetail;
	private String mCategoryCode;
	private String mDate;
	
	
	InterfaceEditOrDetail mInterFaceEditOrDetail;

//	public FragmentSpendCategoryDetail0() {
//		// TODO Auto-generated constructor stub
//		mResponseSpendCategoryDetail = _ResponseSpendCategoryDetail;
//		mCategoryCode = _CategoryCode;
//
//		/* 넘어온 데이터 날자별 분기(1주일, 1개월, 3개월) */
//		// 일단은 그냥 패스
//	}
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_spend_category_detail_0, null);
		
		if(parseParam()){
			if(initWidget(v)) {

			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		if (mAlInfoDataList.get(position) == null) {	// test 임시
			return;
		}

		Intent intent = new Intent(getActivity(), Level3ActivitySpendHistoryDetail.class);
		intent.putExtra(Const.SPEND_HISTORY_DETAIL, mAlInfoDataList.get(position));
		startActivity(intent);

	}

	@Override
	public void onResume() {
		super.onResume();
		spend_category_detail();
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getParcelableExtra */
			Intent intent = new Intent(getActivity().getIntent());
			if(intent != null){

			}
			else {
				bResult = false;
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */
			
			/* Layout 선언 */
			mSpendCategory = (LinearLayout)_v.findViewById(R.id.ll_contents_area_inc_type_1_layout);
			mSpendCategory.setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
			mSpendCategoryName = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_1_txt_1);
			mSpendCategoryCost = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_1_txt_2);
			mSpendCategoryCost.setTypeface(FontClass.setFont(getActivity()));

			mSpendCategoryBudget = (LinearLayout)_v.findViewById(R.id.ll_contents_area_inc_type_4_layout);
			mSpendCategoryBudgetProgress = (ProgressBar)_v.findViewById(R.id.progressbar_contents_area_inc_type_4);
			mSpendCategoryBudgetPercent = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_4_percent_value);
			mSpendCategorySpendName = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_4_txt_1);
			mSpendCategorySpendCost = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_4_txt_2);
			mSpendCategorySpendCost.setTypeface(FontClass.setFont(getActivity()));
			mSpendCategoryBudgetName = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_4_txt_3);
			mSpendCategoryBudgetCost = (TextView)_v.findViewById(R.id.tv_contents_area_inc_type_4_txt_4);
			mSpendCategoryBudgetCost.setTypeface(FontClass.setFont(getActivity()));

			mEmpty = (LinearLayout)_v.findViewById(R.id.ll_empty_layout_layout);
			mEmptyIcon = (ImageView)_v.findViewById(R.id.iv_empty_layout_icon);
			mEmptyTitle = (TextView)_v.findViewById(R.id.tv_empty_layout_txt);

			mListView = (ListView)_v.findViewById(R.id.lv_fragment_spend_category_detail_0_listview);
			mAdapter = new AdtListViewSpendCategoryDetail(getActivity(), mAlInfoDataList);	// test  임시
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(this);

			mEmpty.setVisibility(View.GONE);
			mSpendCategory.setVisibility(View.GONE);
			mSpendCategoryBudget.setVisibility(View.GONE);
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */

			int category_int = Integer.valueOf(mCategoryCode);
			long use_budget = Long.valueOf(mResponseSpendCategoryDetail.getExpense());
			long need_budget = Long.valueOf(mResponseSpendCategoryDetail.getBudget());
			int subListSize = mResponseSpendCategoryDetail.getList().size();
			String[] category = getActivity().getResources().getStringArray(R.array.spend_category);
			String TxSpend = String.format(getString(R.string.spend_category_spend_value), category[category_int]);
			String TxBudget = String.format(getString(R.string.spend_category_budget_value), category[category_int]);

			if(mResponseSpendCategoryDetail != null && need_budget != 0){ //mInfoDataAssetCredit.mTxt4 != null
				int value;
				value = (int)( (double)use_budget / (double)need_budget * 100.0 );

				if(value > 100){
					value = 100;
				}

				mSpendCategory.setVisibility(View.GONE);
				mSpendCategoryBudget.setVisibility(View.VISIBLE);
				mSpendCategoryBudgetProgress.setProgress(value);
				mSpendCategoryBudgetPercent.setText(Integer.toString(value));
				mSpendCategorySpendName.setText(TxSpend);
				mSpendCategorySpendCost.setText(ElseUtils.getDecimalFormat(use_budget));
				mSpendCategoryBudgetName.setText(TxBudget);
				mSpendCategoryBudgetCost.setText(ElseUtils.getDecimalFormat(need_budget));
			}
			else{
				mSpendCategory.setVisibility(View.VISIBLE);
				mSpendCategoryBudget.setVisibility(View.GONE);
				mSpendCategoryName.setText(TxSpend);
				mSpendCategoryCost.setText(ElseUtils.getDecimalFormat(use_budget));
			}

			if(subListSize != 0){
				mEmpty.setVisibility(View.GONE);
				mListView.setVisibility(View.VISIBLE);
			}
			else {
				String title = getActivity().getResources().getString(R.string.spend_empty);
				mEmpty.setVisibility(View.VISIBLE);
				mListView.setVisibility(View.GONE);
				mEmptyTitle.setText(title);
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	
	
	
	
	

//=========================================================================================//
// ToolBar - Text/Text/Text
//=========================================================================================//
	/**  ToolBar click event handler **/
	public void onBtnClickNaviTxtTxtTxtBar(View _view) {    	
		try
		{
			switch (_view.getId()) {

			case R.id.ll_navi_txt_txt_txt_back:
//				getActivity().finish();
				break;

			case R.id.ll_navi_txt_txt_txt_else:
//				mInterFaceEditOrDetail.onEdit();
				break;

			default:
				break;
			}
		}
		catch(Exception e)
		{
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "onBtnLevel1Click() : " + e.toString());			
		}
	}



	
	
	

//===============================================================================//
// Button
//===============================================================================//	
	/**  Button Click Listener **/
	private OnClickListener BtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.ll_navi_txt_txt_txt_back:
				getActivity().finish();
				break;

			case R.id.ll_navi_txt_txt_txt_else:
				mInterFaceEditOrDetail.onEdit();
				break;

			default:
				break;
			}

		}
	};



//=========================================================================================//
// Refresh Adapter
//=========================================================================================//
	public void refreshAdt(String _sort){
		mAdapter.notifyDataSetChanged();
	}



//=========================================================================================//
// Spend Category Detail API
//=========================================================================================//
	private void spend_category_detail(){

		Intent intent = new Intent(getActivity().getIntent());
		if (intent != null){
			mDate = intent.getStringExtra(Const.SPEND_CATEGORY_DATE);
			mCategoryCode = intent.getStringExtra(Const.SPEND_CATEGORY_CODE);
		} else {
			return;
		}
		RequestSpendCategoryDetail mCategoryDetail = new RequestSpendCategoryDetail();
		mCategoryDetail.setUserId(Session.getInstance(getActivity()).getUserId());
		mCategoryDetail.setDate(mDate);
		mCategoryDetail.setCategoryId(mCategoryCode);

		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<ResponseSpendCategoryDetail> call = api.getCategoryDetail(mCategoryDetail);
		call.enqueue(new Callback<ResponseSpendCategoryDetail>() {
			@Override
			public void onResponse(Call<ResponseSpendCategoryDetail> call, Response<ResponseSpendCategoryDetail> response) {
				mResponseSpendCategoryDetail = new ResponseSpendCategoryDetail();
				mAlInfoDataList = new ArrayList<SpendMonthlyInfo>();
				mResponseSpendCategoryDetail = response.body();
				mAlInfoDataList =mResponseSpendCategoryDetail.getList();
				mAdapter.setData((ArrayList<SpendMonthlyInfo>) mAlInfoDataList);
				refreshAdt(null);

				if(mAlInfoDataList != null && mAlInfoDataList.size() != 0){
					mListView.setVisibility(View.VISIBLE);
					mEmpty.setVisibility(View.GONE);
					if(!initData()){
						if (getActivity() != null)
							getActivity().finish();
					}
				}
				else {
					String title = getActivity().getResources().getString(R.string.spend_empty);
					mEmpty.setVisibility(View.VISIBLE);
					mListView.setVisibility(View.GONE);
					mEmptyTitle.setText(title);
				}
			}

			@Override
			public void onFailure(Call<ResponseSpendCategoryDetail> call, Throwable t) {
				ElseUtils.network_error(getActivity());
			}
		});
	}
}
