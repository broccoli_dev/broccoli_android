package com.nomadconnection.broccoli.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetStockSearch;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Asset.ResponseSearchAssetStock;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class FragmentAssetStock0 extends BaseFragment {

	public static final String NAME = "NAME";
	public static final String CODE = "CODE";

	private static String TAG = FragmentAssetStock0.class.getSimpleName();

	/* Layout */
	private LinearLayout mStockName;
	private LinearLayout mStockType;
	private LinearLayout mDate;

	public static TextView mInfoName;
	public static TextView mInfoType;
	public static TextView mInfoDate;
	public static EditText mInfoValue;
	public static EditText mInfoAmount;
	public static TextView mInfoCost;
//	public static EditText mInfoCost;
	private String mTextWatcherValueResult = "";
	private String mTextWatcherAmountResult = "";
	private String mTextWatcherCostResult = "";

	private ArrayList<String> mSpinnerStockTypeData;
	private int mSpinnerStockTypePos;

	/* DataClass */
	public static ResponseSearchAssetStock mStockSearchReturnData;

	InterfaceEditOrDetail mInterFaceEditOrDetail;

	public FragmentAssetStock0() {
		mStockSearchReturnData = new ResponseSearchAssetStock("", "");
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_asset_stock_0, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.FragmentAssetStock0);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_REQUEST_CODE) {
			if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_SUCCESS){
				mStockSearchReturnData = (ResponseSearchAssetStock) data.getParcelableExtra(APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_DATA);
				if (mStockSearchReturnData != null){
					mInfoName.setText(mStockSearchReturnData.mStockName);
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			}
			else if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_CANCLE){
			}
		}
	}



//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			Bundle bundle = getArguments();
			if (bundle != null) {
				String name = bundle.getString(NAME);
				String code = bundle.getString(CODE);
				mStockSearchReturnData = new ResponseSearchAssetStock(code, name);
			}
		} catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mStockName = (LinearLayout)_v.findViewById(R.id.ll_asset_stock_inc_button_layout_1);
			mStockType = (LinearLayout)_v.findViewById(R.id.ll_asset_stock_inc_spinner_layout_1);
			mDate = (LinearLayout)_v.findViewById(R.id.ll_asset_stock_inc_spinner_layout_2);
			mInfoName = (TextView)_v.findViewById(R.id.tv_asset_stock_inc_button_text_1);
			mInfoType = (TextView)_v.findViewById(R.id.tv_asset_stock_inc_spinner_text_1);
			mInfoDate = (TextView)_v.findViewById(R.id.tv_asset_stock_inc_spinner_text_2);
			mInfoValue = (EditText)_v.findViewById(R.id.et_asset_stock_inc_edittext_1);
			mInfoAmount = (EditText)_v.findViewById(R.id.et_asset_stock_inc_edittext_2);
			mInfoCost = (TextView)_v.findViewById(R.id.tv_asset_stock_inc_spinner_text_3);
//			mInfoCost = (EditText)_v.findViewById(R.id.et_asset_stock_inc_edittext_3);

			mStockName.setOnClickListener(BtnClickListener);
//			mStockType.setOnClickListener(BtnClickListener);
			mDate.setOnClickListener(BtnClickListener);


			mInfoValue.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (!s.toString().equals(mTextWatcherValueResult)) {
						if (s.toString().length() == 0) mTextWatcherValueResult = "0";
						else
							mTextWatcherValueResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));

						if(mTextWatcherValueResult.equals("0")){
							mInfoValue.setHint("가격입력");
						}
						else {
							mInfoValue.setText(mTextWatcherValueResult);
							mInfoValue.setSelection(mTextWatcherValueResult.length());
						}

						String s1 = mInfoValue.getText().toString().replaceAll(",", "");
						String s2 = mInfoAmount.getText().toString().replaceAll(",", "");
						long m1 = 0;
						if (s1.length() != 0) m1 = Long.valueOf(s1);
						long m2 = 0;
						if (s2.length() != 0) m2 = Long.valueOf(s2);
						String m3 = TransFormatUtils.mDecimalFormat.format(Long.parseLong(String.valueOf(m1 * m2)));
						mInfoCost.setText(m3);
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			});

			mInfoAmount.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (!s.toString().equals(mTextWatcherAmountResult)) {
						if (s.toString().length() == 0) mTextWatcherAmountResult = "0";
						else
							mTextWatcherAmountResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));

						if(mTextWatcherAmountResult.equals("0")){
							mInfoAmount.setHint(getString(R.string.common_input_amount_hint));
						}
						else {
							mInfoAmount.setText(mTextWatcherAmountResult);
							mInfoAmount.setSelection(mTextWatcherAmountResult.length());
						}

						String s1 = mInfoValue.getText().toString().replaceAll(",", "");
						String s2 = mInfoAmount.getText().toString().replaceAll(",", "");
						long m1 = 0;
						if (s1.length() != 0) m1 = Long.valueOf(s1);
						long m2 = 0;
						if (s2.length() != 0) m2 = Long.valueOf(s2);
						String m3 = TransFormatUtils.mDecimalFormat.format(Long.parseLong(String.valueOf(m1 * m2)));
						mInfoCost.setText(m3);
					}
				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
				}
			});

//			mInfoCost.addTextChangedListener(new TextWatcher() {
//				@Override
//				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//				}
//
//				@Override
//				public void onTextChanged(CharSequence s, int start, int before, int count) {
//					if (!s.toString().equals(mTextWatcherCostResult)) {
//						if (s.toString().length() == 0) mTextWatcherCostResult = "0";
//						else
//							mTextWatcherCostResult = TransFormatUtils.mDecimalFormat.format(Long.parseLong(s.toString().replaceAll(",", "")));
//						mInfoCost.setText(mTextWatcherCostResult);
//						mInfoCost.setSelection(mTextWatcherCostResult.length());
//					}
//				}
//
//				@Override
//				public void afterTextChanged(Editable s) {
//				}
//			});

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			mSpinnerStockTypeData = new ArrayList<String>();
			Collections.addAll(mSpinnerStockTypeData, getResources().getStringArray(R.array.spinner_asset_stock_1));
			mSpinnerStockTypePos = 0;

			if (mStockSearchReturnData.mStockName.trim().length() == 0)
				mInfoName.setText("종목 검색");
			else
				mInfoName.setText(mStockSearchReturnData.mStockName);
//			mInfoType.setText("선택");
//			mInfoDate.setHint("2016.04.06");
			mInfoDate.setText(getString(R.string.common_input_date_hint_2));
			mInfoValue.setHint("가격입력");
			mInfoAmount.setHint(getString(R.string.common_input_amount_hint));
			mInfoCost.setHint("0");

//			mInfoCost.setEnabled(false);	// 자동계산

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}






//===============================================================================//
// Listener
//===============================================================================//
	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(final View v) {
//			v.setBackgroundResource(R.drawable.selector_spinner_bg_open);
			mInfoValue.clearFocus();
			mInfoAmount.clearFocus();
			switch (v.getId()) {

				case R.id.ll_asset_stock_inc_button_layout_1:
					Intent intent = new Intent(getActivity(), Level3ActivityAssetStockSearch.class);
					if (getActivity().getIntent().hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
						intent.putExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND, true);
					}
					startActivityForResult(intent, APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_REQUEST_CODE);
					break;

				case R.id.ll_asset_stock_inc_spinner_layout_1:
					int mTempTop1 = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
					int mTempLeft1 = (ElseUtils.getRect(v).left);
					DialogUtils.showDlgSpinnerList(getActivity(), new Dialog.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mSpinnerStockTypePos = which;
							v.setBackgroundResource(R.drawable.selector_spinner_bg);
							v.setSelected(true);
							mInfoType.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
							mInfoType.setText(mSpinnerStockTypeData.get(mSpinnerStockTypePos));
							dialog.dismiss();
						}
					}, mSpinnerStockTypeData, mTempTop1, mTempLeft1, mSpinnerStockTypePos);
					break;

				case R.id.ll_asset_stock_inc_spinner_layout_2:
					DatePickerDialog.OnDateSetListener callBack1 = new DatePickerDialog.OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear,
											  int dayOfMonth) {
							String msg = String.format("%04d.%02d.%02d", year, monthOfYear+1, dayOfMonth);
							mInfoDate.setText(msg);
							send_interface_complete(); //완료버튼 활성화&비활성화 인터페이스 전송
						}
					};
					int mYear, mMonth, mDay = 0;
					String mTempNow = TransFormatUtils.getDataFormatWhich(1);
					Date mTempNowDate = new Date();
					try {
						mTempNowDate = new SimpleDateFormat("yyyy.MM.dd").parse(mTempNow);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					if (mInfoDate.getText().toString().equals(getString(R.string.common_input_date_hint_2))){
						mYear = Integer.valueOf(mTempNow.substring(0,4));
						mMonth = Integer.valueOf(mTempNow.substring(5, 7));
						mDay = Integer.valueOf(mTempNow.substring(8,10));
					}
					else{
						mYear = Integer.valueOf(mInfoDate.getText().toString().substring(0,4));
						mMonth = Integer.valueOf(mInfoDate.getText().toString().substring(5,7));
						mDay = Integer.valueOf(mInfoDate.getText().toString().substring(8,10));
					}

					DatePickerDialog pickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, callBack1, mYear, mMonth-1, mDay);
					pickerDialog.getDatePicker().setMaxDate(mTempNowDate.getTime());		// 처음에 현재 이전 날자 클릭 막아 달라고 했다고, 2016-02-12에 이후 막아달라고 요청
//					pickerDialog.getDatePicker().setMinDate(mTempNowDate.getTime());
//					pickerDialog.getDatePicker().setMinDate(new Date().getTime());
					pickerDialog.show();
//					new DatePickerDialog(getActivity(), callBack1, mYear, mMonth-1, mDay).show();
					break;

				default:
					break;
			}

		}
	};

	private void send_interface_complete(){
		if (empty_check()) {
			mInterFaceEditOrDetail.onEdit();
		}
		else {
			mInterFaceEditOrDetail.onError();
		}
	}

	private boolean empty_check(){
		if(mInfoName.getText().toString().equals("종목 검색") || mInfoDate.getText().toString().equals(getString(R.string.common_input_date_hint_2))
				|| mInfoValue.length() == 0  || mInfoAmount.length() == 0 || mInfoCost.length() == 0
				|| mInfoValue.getText().toString().equals("가격입력") || mInfoAmount.getText().toString().equals(getString(R.string.common_input_amount_hint))){
			return false;
		}
		return true;
	}
}
