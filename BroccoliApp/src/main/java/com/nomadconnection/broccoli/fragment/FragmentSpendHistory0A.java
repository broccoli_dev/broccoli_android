package com.nomadconnection.broccoli.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivitySpendHistoryDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendCardTabList;
import com.nomadconnection.broccoli.adapter.AdtListViewSpendHistoryA;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.CustomExpandableListView;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Spend.RequestDataByDate;
import com.nomadconnection.broccoli.data.Spend.ResponseSpendDetail;
import com.nomadconnection.broccoli.data.Spend.SpendCardBanner;
import com.nomadconnection.broccoli.data.Spend.SpendHistoryHeader;
import com.nomadconnection.broccoli.data.Spend.SpendMonthlyInfo;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSpendHistory0A extends BaseFragment {

	/* Layout */
	private LinearLayout mEmpty;
	private ImageView mEmptyIcon;
	private TextView mEmptyTitle;

	private CustomExpandableListView mListView;
	private String mTargetMonth;
	private AdtListViewSpendHistoryA mAdapter;
	private WrapperExpandableListAdapter mWarpAdapter;
	private ArrayList<SpendMonthlyInfo> mAlInfoDataList;
	private ArrayList<SpendHistoryHeader> mHeaderList;
	private HashMap<Integer, ArrayList<SpendMonthlyInfo>> mAlInfoHashMap;
	private boolean isThisMonth = false;

	public FragmentSpendHistory0A() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//View v = inflater.inflate(R.layout.fragment_asset_credit_detail_1_a, null);
		View v = inflater.inflate(R.layout.fragment_spend_history_0_a, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		requestDataSet();
	}

//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			Bundle bundle = getArguments();
			if (bundle != null) {
				mTargetMonth = bundle.getString(Const.DATA);
			}
			/* getExtra */
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			mEmpty = (LinearLayout)_v.findViewById(R.id.ll_empty_layout_layout);
			mEmptyIcon = (ImageView)_v.findViewById(R.id.iv_empty_layout_icon);
			mEmptyTitle = (TextView)_v.findViewById(R.id.tv_empty_layout_txt);

			mListView = (CustomExpandableListView)_v.findViewById(R.id.lv_fragment_spend_history_0_a_listview);
			mListView.setGroupIndicator(null);
    		mAdapter = new AdtListViewSpendHistoryA(getActivity());
			mWarpAdapter = new WrapperExpandableListAdapter(mAdapter);
    		mListView.setAdapter(mWarpAdapter);
			mListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return false;
				}
			});
			mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
					if (mHeaderList == null || mHeaderList.get(groupPosition) == null) {	// test 임시
						return false;
					}

					Intent intent = new Intent(getActivity(), Level3ActivitySpendHistoryDetail.class);
					intent.putExtra(Const.SPEND_HISTORY_DETAIL, mAlInfoHashMap.get(getHeader(groupPosition)).get(childPosition));
					//intent.putExtra(APP.INFO_DATA_ASSET_CREDIT_PARCEL, mAlInfoDataList.get(position));	// test 임시
					startActivity(intent);
					return false;
				}
			});
			mListView.setOnFloatingGroupChangeListener(new CustomExpandableListView.OnFloatingGroupChangeListener() {
				@Override
				public void onFloatingGroupChangeListener(View current, View next, int scroll) {

				}
			});
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private int getHeader(int groupPosition) {
		int index = 0;
		if (mHeaderList != null) {
			SpendHistoryHeader header = mHeaderList.get(groupPosition);
			index = header.getDay();
		}
		return index;
	}


//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			mEmpty.setVisibility(View.GONE);
			/* Default Value */
		} catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}
	
	
	
//=========================================================================================//
// Refresh Adapter
//=========================================================================================//

	public void requestDataSet() {
		RequestDataByDate userDate = new RequestDataByDate();
		userDate.setUserId(Session.getInstance(getActivity()).getUserId());
		userDate.setDate(mTargetMonth);

		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<ResponseSpendDetail> call = api.getSpendDetail(userDate);
		call.enqueue(new Callback<ResponseSpendDetail>() {
			@Override
			public void onResponse(Call<ResponseSpendDetail> call, Response<ResponseSpendDetail> response) {
				if (getActivity() != null) {
					mAlInfoDataList = new ArrayList<SpendMonthlyInfo>();
					mAlInfoDataList = (ArrayList<SpendMonthlyInfo>) response.body().getList();

					if (mAlInfoDataList != null && mAlInfoDataList.size() != 0) {
						mEmpty.setVisibility(View.GONE);
						mListView.setVisibility(View.VISIBLE);
						mAlInfoHashMap = createList(mAlInfoDataList);
						mHeaderList = createExistHeader(mAlInfoHashMap);
						Collections.sort(mHeaderList, new Comparator<SpendHistoryHeader>() {
							@Override
							public int compare(SpendHistoryHeader lhs, SpendHistoryHeader rhs) {
								int result = 0;
								if (lhs.getDay() < rhs.getDay()) {
									result = 1;
								} else if (lhs.getDay() > rhs.getDay()) {
									result = -1;
								}
								return result;
							}
						});
						mAdapter.setData(mHeaderList, mAlInfoHashMap);
						refreshAdt(null);
					} else {
						String title = getActivity().getResources().getString(R.string.spend_empty);
						mEmpty.setVisibility(View.VISIBLE);
						mListView.setVisibility(View.GONE);
						mEmptyIcon.setImageResource(R.drawable.empty_icon_nohistory);
						mEmptyTitle.setText(title);
					}
				}
			}

			@Override
			public void onFailure(Call<ResponseSpendDetail> call, Throwable t) {
				ElseUtils.network_error(getActivity());
			}
		});
	}

	DateFormat format = new SimpleDateFormat("yyyyMM");

	private ArrayList<SpendHistoryHeader> createHeaderList(String targetMonth) {
		ArrayList<SpendHistoryHeader> headerList = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		Calendar now =  (Calendar) calendar.clone();
		Date date = null;
		try {
			date = format.parse(targetMonth);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);

		int nowYear = now.get(Calendar.YEAR);
		int nowMonth = now.get(Calendar.MONTH);

		int lastDay = 0;
		boolean isThisMonth = false;
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
//		dayOfWeek = dayOfWeek-2;
		if ((year == nowYear) && (month == nowMonth)) {
			// this month
			lastDay = now.get(Calendar.DAY_OF_MONTH);
			isThisMonth = true;
		} else {
			// past month
			lastDay = calendar.getMaximum(Calendar.DAY_OF_MONTH);
		}

		for (int i=0; i < lastDay; i++) {
			int realDay = i+1;
			String day = String.valueOf(realDay);
			String keyDay = String.valueOf(realDay);
			if (realDay < 10) {
				keyDay = "0"+keyDay;
			}
			String dow = null;

			if (isThisMonth && i >= lastDay-2) {
				if (i == lastDay -2) {
					dow = "어제";
				} else if (i == lastDay -1) {
					dow = "오늘";
				}
			} else {
				int temp = (dayOfWeek+i)%7;
				switch (temp) {
					case 0:
						dow = "(일)";
						break;
					case 1:
						dow = "(월)";
						break;
					case 2:
						dow = "(화)";
						break;
					case 3:
						dow = "(수)";
						break;
					case 4:
						dow = "(목)";
						break;
					case 5:
						dow = "(금)";
						break;
					case 6:
						dow = "(토)";
						break;
				}
			}

			String headerDate = String.valueOf(month+1)+"월 "+day+"일 "+dow;
			String keyDate = String.valueOf(month+1)+"."+keyDay;
			SpendHistoryHeader header = new SpendHistoryHeader();
			header.setHeaderDate(headerDate);
			header.setKeyDate(keyDate);
			header.setDay(realDay);
			headerList.add(header);
		}

		return headerList;
	}

	public ArrayList<SpendHistoryHeader> createExistHeader(HashMap<Integer, ArrayList<SpendMonthlyInfo>> map) {
		ArrayList<SpendHistoryHeader> headerList = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		Calendar now =  (Calendar) calendar.clone();
		Date date = null;
		try {
			date = format.parse(mTargetMonth);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);

		int nowYear = now.get(Calendar.YEAR);
		int nowMonth = now.get(Calendar.MONTH);

		int lastDay = 0;
		isThisMonth = false;
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		dayOfWeek = dayOfWeek-2;
		if ((year == nowYear) && (month == nowMonth)) {
			// this month
			lastDay = now.get(Calendar.DAY_OF_MONTH);
			isThisMonth = true;
		} else {
			// past month
			lastDay = calendar.getMaximum(Calendar.DAY_OF_MONTH);
		}

		Set<Integer> keys = map.keySet();
		Iterator<Integer> iterator = keys.iterator();
		while (iterator.hasNext()) {
			int realDay = iterator.next();
			String day = String.valueOf(realDay);
			String keyDay = String.valueOf(realDay);
			if (realDay < 10) {
				keyDay = "0"+keyDay;
			}
			String dow = null;

			if (isThisMonth && (realDay < lastDay+1 && realDay >= lastDay-1)) {
				if (realDay == lastDay -1) {
					dow = "어제";
				} else if (realDay == lastDay) {
					dow = "오늘";
				}
			} else {
				int temp = (dayOfWeek+realDay)%7;
				switch (temp) {
					case 0:
						dow = "(일)";
						break;
					case 1:
						dow = "(월)";
						break;
					case 2:
						dow = "(화)";
						break;
					case 3:
						dow = "(수)";
						break;
					case 4:
						dow = "(목)";
						break;
					case 5:
						dow = "(금)";
						break;
					case 6:
						dow = "(토)";
						break;
				}
			}

			String headerDate = String.valueOf(month+1)+"월 "+day+"일 "+dow;
			String keyDate = String.valueOf(month+1)+"."+keyDay;
			SpendHistoryHeader header = new SpendHistoryHeader();
			header.setHeaderDate(headerDate);
			header.setKeyDate(keyDate);
			header.setDay(realDay);
			headerList.add(header);
		}

		return headerList;
	}

	public HashMap<Integer, ArrayList<SpendMonthlyInfo>> createList(ArrayList<SpendMonthlyInfo> list) {
		HashMap<Integer, ArrayList<SpendMonthlyInfo>> hashMap = new HashMap<>();
		for (int i=0; i < list.size(); i++) {
			String temp = list.get(i).getExpenseDate();
			String expenseDate = ElseUtils.getSpendDateForHeader(temp);
			if (expenseDate != null) {
				String[] values = expenseDate.split("\\.");
				int key = Integer.parseInt(values[1]);
				ArrayList<SpendMonthlyInfo> tempList = hashMap.get(key);
				if (tempList == null) {
					tempList = new ArrayList<>();
					hashMap.put(key, tempList);
				}
				tempList.add(list.get(i));
			}
		}

		return hashMap;
	}

	public void refreshAdt(String sort) {
		showBanner();
		mAdapter.notifyDataSetChanged();
		mWarpAdapter.notifyDataSetChanged();
		for (int i=0; i < mWarpAdapter.getGroupCount(); i++) {
			mListView.expandGroup(i);
		}
	}

	private View mHeaderView;

	private void showBanner() {
		if (isThisMonth) {
			if (mHeaderView != null) {
				mListView.removeHeaderView(mHeaderView);
			}
			Call<SpendCardBanner> call = ServiceGenerator.createScrapingService(SpendApi.class).getRecommendBanner();
			call.enqueue(new Callback<SpendCardBanner>() {
				@Override
				public void onResponse(Call<SpendCardBanner> call, Response<SpendCardBanner> response) {
					if (response != null && response.isSuccessful()) {
						SpendCardBanner banner = response.body();
						if (getActivity() != null && !getActivity().isFinishing()) {
							if (banner != null && banner.recommendYn > 0 && banner.diff > 0) {
								View view = getActivity().getLayoutInflater().inflate(R.layout.row_list_item_card_banner, null);
								mHeaderView = view;
								view.findViewById(R.id.ll_row_spend_card_benefit_overlay_background).setBackgroundColor(getResources().getColor(R.color.common_argb_20p_0_0_0));
								view.findViewById(R.id.ll_row_spend_card_benefit_layout).setBackgroundColor(getResources().getColor(R.color.main_2_layout_bg));
								TextView benefit = (TextView) view.findViewById(R.id.tv_row_spend_card_benefit);
								benefit.setText(ElseUtils.getDecimalFormat(banner.diff));
								TextView benefitUnit = (TextView) view.findViewById(R.id.tv_row_spend_card_benefit_unit);
								if (banner.mileageYn > 0) {
									benefitUnit.setText(R.string.spend_card_recommend_unit_mileage);
								} else {
									benefitUnit.setText(R.string.spend_card_recommend_unit_won);
								}
								view.setTag(banner);
								view.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View view) {
										((BaseActivity)getActivity()).sendTracker(AnalyticsName.SpendHistoryCardBanner);
										SpendCardBanner banner = (SpendCardBanner) view.getTag();
										Intent bannerIntent = new Intent(getActivity(), Level2ActivitySpendCardTabList.class);
										bannerIntent.putExtra(Const.DATA, banner);
										bannerIntent.putExtra(Level2ActivitySpendCardTabList.REQUEST_SHOW_RECOMMEND_CARD, 0);
										startActivity(bannerIntent);
									}
								});
								mListView.addHeaderView(view, null, true);
							}
						}
					}
				}

				@Override
				public void onFailure(Call<SpendCardBanner> call, Throwable t) {

				}
			});
		}
	}


}
