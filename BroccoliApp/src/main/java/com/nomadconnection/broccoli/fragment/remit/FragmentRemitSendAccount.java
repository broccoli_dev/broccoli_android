package com.nomadconnection.broccoli.fragment.remit;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.remit.AdtExpListViewRemitSendAccount;
import com.nomadconnection.broccoli.api.EtcApi;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeBank;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.RemitBankAccount;
import com.nomadconnection.broccoli.data.Remit.RemitBankDetail;
import com.nomadconnection.broccoli.data.Remit.RemitContent;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccountCurrent;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Scrap.RequestDataByUploadId;
import com.nomadconnection.broccoli.dialog.remit.DialogBaseRemitEditButton;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceRemitSend;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentRemitSendAccount extends BaseFragment implements View.OnClickListener{
	
	private static String TAG = "FragmentRemitSendAccount";


	InterfaceRemitSend mInterfaceRemitSend;

	/* Layout */
	private ArrayList<String> mGroupList = null;
	private ArrayList<ArrayList<RemitBankAccount>> mChildList = null;
	private ArrayList<RemitBankAccount> mChildListContent1 = null;
	private ArrayList<RemitBankAccount> mChildListContent2 = null;
	private TextView mRemitBankName;
	private EditText mRemitBankNumber;
	private Button mRemitSearch;
	private ExpandableListView mExpListView;
	private AdtExpListViewRemitSendAccount mAdapter;
	private String[] mBankNameArr;
	private String[] mBankCardArr;
	private int mBankSelect = -1;
	private String mBankCord ="";
	private RemitBankAccount mSelectAccount;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterfaceRemitSend = (InterfaceRemitSend) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_remit_send_account, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		recentlySendAccountList();
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.tv_fragment_remit_send_account_bank_select:
				dialog_bank_list();
				break;
			case R.id.btn_fragment_remit_send_account_search:
				String bankCord = TransFormatUtils.transBankNameToCode(mRemitBankName.getText().toString());
				String bankNumber = mRemitBankNumber.getText().toString();

				RemitBankAccount account = new RemitBankAccount();
				account.setHostName("시티은행");
				account.setAccountNumber(bankNumber);
				account.setCompanyCode(bankCord);
				mSelectAccount = account;
				dialogConfirmAccount(account.getHostName(), bankNumber);
				break;
		}

	}


//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			mGroupList = new ArrayList<String>();
			mChildList = new ArrayList<ArrayList<RemitBankAccount>>();
			mChildListContent1 = new ArrayList<RemitBankAccount>();
			mChildListContent2 = new ArrayList<RemitBankAccount>();
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mRemitBankName = (TextView)_v.findViewById(R.id.tv_fragment_remit_send_account_bank_select);
			mRemitBankName.setOnClickListener(this);
			mRemitBankNumber = (EditText)_v.findViewById(R.id.et_fragment_remit_send_account_number);
			mRemitSearch = (Button)_v.findViewById(R.id.btn_fragment_remit_send_account_search);
			mRemitSearch.setOnClickListener(this);
			mExpListView = (ExpandableListView)_v.findViewById(R.id.elv_fragment_remit_send_account_explistview);
			mAdapter = new AdtExpListViewRemitSendAccount(getActivity(), mGroupList, mChildList);
			mExpListView.setAdapter(mAdapter);

	        /* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return true;
				}
			});

			/* Child list click 시 */
			mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
											int groupPosition, int childPosition, long id) {
					final RemitBankAccount mTemp = mChildList.get(groupPosition).get(childPosition);
					if (mTemp == null) return true;

					mSelectAccount = mTemp;
					dialogConfirmAccount(mTemp.getHostName(), mTemp.getAccountNumber());
					return false;
				}
			});

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			for (int i = 0; i <mGroupList.size() ; i++) {
				mExpListView.expandGroup(i);
			}

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}



//===============================================================================//
// Bank Link get API
//===============================================================================//
	private void recentlySendAccountList(){
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ResponseRemitAccountCurrent> call = api.recentlySendAccountList(mRequestDataByUserId);
		call.enqueue(new Callback<ResponseRemitAccountCurrent>() {
			@Override
			public void onResponse(Call<ResponseRemitAccountCurrent> call, Response<ResponseRemitAccountCurrent> response) {
				DialogUtils.dismissLoading();
				mGroupList.clear();
				mChildList.clear();
				mChildListContent1.clear();
				mChildListContent2.clear();

				mChildListContent1 = response.body().getMyAccountList();
				mChildListContent2 = response.body().getSendAccountList();

				if(mChildListContent1.size() != 0){
					mGroupList.add(getActivity().getString(R.string.remit_send_my_account));
					mChildList.add(mChildListContent1);

					if(mChildListContent2.size() != 0){
						mGroupList.add(getActivity().getString(R.string.remit_send_current_account));
						mChildList.add(mChildListContent2);
					}
				}
				else {

				}

				if(mAdapter != null){
					mAdapter.setData(mGroupList, mChildList);
					mAdapter.notifyDataSetChanged();

					for (int i = 0; i <mGroupList.size() ; i++) {
						mExpListView.expandGroup(i);
					}
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitAccountCurrent> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());

			}
		});
	}

	private void dialog_bank_list(){
		mBankNameArr = getActivity().getResources().getStringArray(R.array.bank_remit_name);
		mBankCardArr = getActivity().getResources().getStringArray(R.array.bank_remit_code);
		ArrayList<String> mListBankName = new ArrayList<String>(Arrays.asList(mBankNameArr));
		String mStr4 = getString(R.string.membership_text_second_cert_bank);
		DialogUtils.showDlgBaseRemitList(getActivity(), new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						mBankSelect = position;
						mRemitBankName.setText(mBankNameArr[position]);
						mBankCord = mBankCardArr[position];
					}
				},
				new InfoDataDialog(APP.DIALOG_BASE_LIST_SMALL, mStr4, getString(R.string.common_cancel)), mBankSelect, mListBankName);
	}


	private void dialogConfirmAccount(String mStr1, String mStr2){
		DialogUtils.showDlgBaseRemitTwoButton(getActivity(), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (which == APP.DIALOG_BASE_TWO_BTN_CONFIRM) {
						mInterfaceRemitSend.onRemitSendAccount(mSelectAccount);
					}

				}
			},
				new InfoDataDialog(R.layout.dialog_inc_remit_case_2, getString(R.string.common_cancel), getString(R.string.common_confirm), mStr1, mStr2));
	}
}
