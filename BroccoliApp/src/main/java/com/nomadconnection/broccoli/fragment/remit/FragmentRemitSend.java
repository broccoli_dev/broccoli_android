package com.nomadconnection.broccoli.fragment.remit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.remit.RemitSendPasswordActivity;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitAccount;
import com.nomadconnection.broccoli.data.Remit.RemitBankAccount;
import com.nomadconnection.broccoli.data.Remit.RemitContent;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendPhone;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccount;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceRemitSend;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.util.ArrayList;

public class FragmentRemitSend extends BaseFragment implements View.OnClickListener{
	
	private static String TAG = "FragmentRemitSend";
	
	
	InterfaceRemitSend mInterfaceRemitSend;

	/* Layout */
	private Bundle mArgument;
	private EditText mRemitCost;
	private TextView mRemitCostText;
	private LinearLayout mRemitOutputAccount;
	private TextView mRemitAccount;
	private LinearLayout mRemitSendType;
	private LinearLayout mRemitSendPhone;
	private LinearLayout mRemitSendAccount;
	private LinearLayout mRemitSender;
	private TextView mRemitSenderName;
	private TextView mRemitSenderNumber;
	private ImageView mRemitSenderDel;
	private Button mRemitSend;
	private String mTextWatcherResult = "";

	private ResponseRemitAccount mRemitMain;
	private RemitAccount mRemitSelectAccount;
	private int mOutputAccountSelect = -1;
	private String mRemitSendKind ="";
	private RemitContent content;
	private RemitBankAccount account;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterfaceRemitSend = (InterfaceRemitSend) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_remit_send, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
//		challenge_info_list_get();
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.ll_fragment_remit_send_output_account:
				dialog_output_bank_list();

				break;
			case R.id.ll_activity_remit_send_phone_number: // 전화 송금
				mInterfaceRemitSend.onRemitSendSet(APP.REMIT_SEND_PHONE, mRemitCost.getText().toString().replaceAll(",", ""));
				break;
			case R.id.ll_activity_remit_send_account_number: //계좌 송금
				mInterfaceRemitSend.onRemitSendSet(APP.REMIT_SEND_ACCOUNT, mRemitCost.getText().toString().replaceAll(",", ""));
				break;
			case R.id.iv_activity_remit_send_sender_del:
				mRemitSend.setEnabled(false);
				mRemitSendType.setVisibility(View.VISIBLE);
				mRemitSender.setVisibility(View.GONE);
				break;
			case R.id.btn_activity_remit_send:
				Intent remit_sending = new Intent(getActivity(), RemitSendPasswordActivity.class);
				if(mArgument.getInt(Const.REMIT_PAGE_STATUS) == APP.REMIT_SEND_PHONE){	//전화송금
					remit_sending.putExtra(Const.REMIT_PAGE_STATUS, APP.REMIT_SEND_PHONE);
					remit_sending.putExtra(Const.DATA, sendPhoneData());
				}
				else {	//계좌송금
					remit_sending.putExtra(Const.REMIT_PAGE_STATUS, APP.REMIT_SEND_ACCOUNT);
					remit_sending.putExtra(Const.DATA, sendAccountData());
				}

				if("A".equals(mRemitSendKind)){
					remit_sending.putExtra(Const.REMIT_ACCOUNT_INFO, mRemitSelectAccount);
				}
				startActivityForResult(remit_sending, 0);
				break;
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			getActivity().finish();
		}
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			mArgument = getArguments();
			Intent intent = new Intent(getActivity().getIntent());
			if(intent.hasExtra(Const.REMIT_MAIN_DATA)){
				mRemitMain = (ResponseRemitAccount)intent.getSerializableExtra(Const.REMIT_MAIN_DATA);
			}
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout 선언 */
			mRemitCost = (EditText)_v.findViewById(R.id.et_activity_remit_send_input_cost);
			mRemitCostText = (TextView)_v.findViewById(R.id.tv_activity_remit_send_text_cost);
			mRemitCostText.setVisibility(View.INVISIBLE);
			mRemitOutputAccount = (LinearLayout) _v.findViewById(R.id.ll_fragment_remit_send_output_account);
			mRemitOutputAccount.setOnClickListener(this);
			mRemitAccount = (TextView)_v.findViewById(R.id.tv_activity_remit_send_account);
			mRemitSendType = (LinearLayout)_v.findViewById(R.id.ll_activity_remit_send_method);
			mRemitSendPhone = (LinearLayout)_v.findViewById(R.id.ll_activity_remit_send_phone_number);
			mRemitSendPhone.setOnClickListener(this);
			mRemitSendAccount = (LinearLayout)_v.findViewById(R.id.ll_activity_remit_send_account_number);
			mRemitSendAccount.setOnClickListener(this);
			mRemitSender = (LinearLayout)_v.findViewById(R.id.ll_activity_remit_send_sender);
			mRemitSenderName = (TextView)_v.findViewById(R.id.tv_activity_remit_send_sender_name);
			mRemitSenderNumber = (TextView)_v.findViewById(R.id.tv_activity_remit_send_sender_account);
			mRemitSenderDel = (ImageView)_v.findViewById(R.id.iv_activity_remit_send_sender_del);
			mRemitSenderDel.setOnClickListener(this);
			mRemitSend = (Button)_v.findViewById(R.id.btn_activity_remit_send);
			mRemitSend.setOnClickListener(this);
			mRemitSend.setEnabled(false);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			if(mRemitMain != null) {
				if(! "0".equals(mRemitMain.getWalletMoney())){
					mRemitAccount.setText(getActivity().getString(R.string.remit_broccoli_money));
					mOutputAccountSelect = 0;
					mRemitSendKind = "W";
				}
				else {
					String tempAccountInfo =  TransFormatUtils.transBankCodeToName(mRemitMain.getAccountInfo().get(0).getCompanyCode())
							+ " " + mRemitMain.getAccountInfo().get(0).getAccountNumber();
					mRemitAccount.setText(tempAccountInfo);
					mOutputAccountSelect = 1;
					mRemitSendKind = "A";
					mRemitSelectAccount = mRemitMain.getAccountInfo().get(mOutputAccountSelect -1);
				}
			}

			if(mArgument.get(Const.DATA) != null){
				mRemitSendType.setVisibility(View.GONE);
				mRemitSender.setVisibility(View.VISIBLE);

				if(! "".equals(mArgument.getString(Const.REMIT_SEND_SUM))){
					mRemitCost.setText(ElseUtils.getStringDecimalFormat(mArgument.getString(Const.REMIT_SEND_SUM)));
				}

				if(mArgument.getInt(Const.REMIT_PAGE_STATUS) == APP.REMIT_SEND_PHONE){
					if(mRemitCost.length() != 0) {
						mRemitSend.setEnabled(true);
					}
					content = (RemitContent) mArgument.getSerializable(Const.DATA);
					mRemitSenderName.setText(content.getHostName());
					mRemitSenderNumber.setText(content.getTelNo());
				}
				else{
					if(mRemitCost.length() != 0) {
						mRemitSend.setEnabled(true);
					}
					account = (RemitBankAccount) mArgument.getSerializable(Const.DATA);
					mRemitSenderName.setText(account.getHostName());
					mRemitSenderNumber.setText(account.getAccountNumber());
				}
			}
			else {
				mRemitSend.setEnabled(false);
				mRemitSendType.setVisibility(View.VISIBLE);
				mRemitSender.setVisibility(View.GONE);
			}

			mRemitCost.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					if (!s.toString().equals(mTextWatcherResult)) {
						if ("".equals(s.toString())) {
							mRemitSend.setEnabled(false);
							mRemitCost.setHint("0");
							mRemitCostText.setVisibility(View.INVISIBLE);
						} else {
							try {
								// The comma in the format specifier does the trick
								mTextWatcherResult = String.format("%,d", Long.parseLong(s.toString().replaceAll(",", "")));
//								Log.d("broccoli", "afterTextChanged() : " + mTextWatcherResult);
							} catch (NumberFormatException e) {
								Log.e("broccoli", "afterTextChanged() : " + e.toString());
							}
							mRemitCostText.setVisibility(View.VISIBLE);
							mRemitCost.setText(mTextWatcherResult);
							mRemitCost.setSelection(mTextWatcherResult.length());
							mRemitCostText.setText(ElseUtils.convertHangul(s.toString().replaceAll(",", "")) + getString(R.string.common_won));
							mRemitSend.setEnabled(true);
						}
					}
				}
			});
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void dialog_output_bank_list(){
		final ArrayList<String> mOutputBankList = new ArrayList<String>();
		mOutputBankList.add(getActivity().getString(R.string.remit_broccoli_money));
		if(mRemitMain != null && mRemitMain.getAccountInfo() != null){
			for(int i = 0; i < mRemitMain.getAccountInfo().size(); i++){
				String tempAccountInfo =  TransFormatUtils.transBankCodeToName(mRemitMain.getAccountInfo().get(i).getCompanyCode())
						+ " " + mRemitMain.getAccountInfo().get(i).getAccountNumber();
				mOutputBankList.add(tempAccountInfo);
			}
		}
		String mStr4 = getString(R.string.remit_account_withdraw_select);
		DialogUtils.showDlgBaseRemitList(getActivity(), new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						mOutputAccountSelect = position;
						mRemitAccount.setText(mOutputBankList.get(position));
						if (mOutputAccountSelect > 0) {
							mRemitSendKind = "A";
							mRemitSelectAccount = mRemitMain.getAccountInfo().get(position - 1);
						} else {
							mRemitSendKind = "W";
						}
					}
				},
				new InfoDataDialog(APP.DIALOG_BASE_LIST_LARGE, mStr4, getString(R.string.common_cancel)), mOutputAccountSelect, mOutputBankList);
	}

	private RequestDataSendAccount sendAccountData() {
		RequestDataSendAccount mSendAccountData = new RequestDataSendAccount();
		mSendAccountData.setUserId(Session.getInstance(getActivity()).getUserId());
		mSendAccountData.setSendKind(mRemitSendKind);
		if("A".equals(mRemitSendKind)){
			mSendAccountData.setAccountId(mRemitSelectAccount.getAccountId());
		}
		else {
			mSendAccountData.setAccountId("");
		}
		mSendAccountData.setSendMoney(mRemitCost.getText().toString().replaceAll(",", ""));
		mSendAccountData.setCompanyCode(account.getCompanyCode());
		mSendAccountData.setAccountNumber(account.getAccountNumber());
		mSendAccountData.setOpponent(account.getHostName());
		return mSendAccountData;
	}

	private RequestDataSendPhone sendPhoneData() {
		RequestDataSendPhone mSendPhoneData = new RequestDataSendPhone();
		mSendPhoneData.setUserId(Session.getInstance(getActivity()).getUserId());
		mSendPhoneData.setSendKind(mRemitSendKind);
		if("A".equals(mRemitSendKind)){
			mSendPhoneData.setAccountId(mRemitSelectAccount.getAccountId());
		}
		else {
			mSendPhoneData.setAccountId("");
		}
		mSendPhoneData.setSendMoney(mRemitCost.getText().toString().replaceAll(",", ""));
		mSendPhoneData.setTelNo(content.getTelNo().replaceAll("-", ""));
		mSendPhoneData.setOpponent(content.getHostName());
		return mSendPhoneData;
	}
}
