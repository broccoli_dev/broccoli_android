package com.nomadconnection.broccoli.fragment.demo;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.AdtListViewMain1;
import com.nomadconnection.broccoli.api.DemoApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Demo.TrialAsset;
import com.nomadconnection.broccoli.data.InfoDataMain1;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.utils.ChartUtils;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;
import com.nomadconnection.broccoli.utils.FontClass;
import com.nomadconnection.broccoli.utils.MyMarkerView;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.lang.reflect.Type;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDemoAsset extends BaseFragment implements InterfaceFragmentInteraction {

	//Comparator 를 만든다.
	private final static Comparator<TrialAsset> myComparator= new Comparator<TrialAsset>()  {
		private final Collator   collator = Collator.getInstance();

		@Override
		public int compare(TrialAsset object1, TrialAsset object2) {
			return collator.compare(object1.month, object2.month);
		}
	};

	public ListView mListView;

	/* Flexible Layout */
	private TextView mInfo1;
	private LinearLayout mInfo2;
	private LinearLayout mInfo3;
	private TextView mInfo5;
	private LinearLayout mInfoBtn1;
	private LinearLayout mInfoBtn2;

	/* Chart */
	private CombinedData mCombinedData;
	private ArrayList<Entry> mLineEntries;
	private ArrayList<BarEntry> mBarEntries;
	private int mLastXIndex = 0;
	private CombinedChart mChart;
	private AdtListViewMain1 mAdapter;
	private ArrayList<InfoDataMain1> mAlInfoDataList;
	private List<TrialAsset> mData;
	private int mAge = 20;
	private String mGender = "M";
	private int mIndex = 0;

	private OnItemClickListener ItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {
			DialogUtils.showDialogDemoFinish(getActivity());
		}
	};

	private OnChartValueSelectedListener mOnChartValueSelectedListener = new OnChartValueSelectedListener() {
		@Override
		public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
			if (e == null)
				return;
			mLastXIndex = h.getXIndex();
			mCombinedData.setData(ChartUtils.generateLineDataCircle(mLineEntries, APP.MAIN_LIST_CHART_USE_1, mLastXIndex));
			mChart.setData(mCombinedData);
			mChart.getMarkerView().getChildAt(0).setVisibility(mLineEntries.get(h.getXIndex()).getVal() == 0 ? View.GONE:View.VISIBLE);
			mChart.invalidate();
		}

		@Override
		public void onNothingSelected() {
			mChart.highlightValue(mLastXIndex, 0);
		}
	};

	/**  Button Click Listener **/
	private View.OnClickListener BtnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

				case R.id.ll_flexible_area_main_1_info_btn_1:
					mInfoBtn1.setSelected(true);
					mInfoBtn2.setSelected(false);

					mInfo1.setText("평균 재산");
					mInfo2.setVisibility(View.VISIBLE);
					mInfo3.setVisibility(View.GONE);
					mInfo5.setText(TransFormatUtils.getDecimalFormat(mData.get(mData.size()-1).deposit));
					refreshChart(mData, false);
					mOnChartValueSelectedListener.onValueSelected(new Entry(0, 0), 0, new Highlight(mLineEntries.size()-1,0));
					break;

				case R.id.ll_flexible_area_main_1_info_btn_2:
					mInfoBtn1.setSelected(false);
					mInfoBtn2.setSelected(true);

					mInfo1.setText("평균 부채");
					mInfo2.setVisibility(View.GONE);
					mInfo3.setVisibility(View.VISIBLE);
					mInfo5.setText(TransFormatUtils.getDecimalFormat(mData.get(mData.size()-1).card+mData.get(mData.size()-1).loan));
					refreshChart(mData, true);
					mOnChartValueSelectedListener.onValueSelected(new Entry(0, 0), 0, new Highlight(mLineEntries.size()-1,0));
					break;

				default:
					break;
			}

		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_main1, null);

		init(view);

		return view;
	}

	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



//===============================================================================//
// Refresh
//===============================================================================//

	@Override
	public void onResume() {
		super.onResume();
		refreshData();
	}

	private void init(View view) {
		mAlInfoDataList = new ArrayList<InfoDataMain1>();
		initWidget(view);
		initData();
	}


//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View view) {
		boolean bResult = true;
		try
		{
			/* Layout */
			mListView = (ListView) view.findViewById(R.id.lv_fragment_main_1_listview);
			mAdapter = new AdtListViewMain1(getActivity(), mAlInfoDataList, ItemClickListener, true);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				mListView.setNestedScrollingEnabled(true);
			}

			LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View headerLayout = inflater.inflate(R.layout.flexible_area_main_1, null);
			mListView.addHeaderView(headerLayout);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(ItemClickListener);


			/* Flexibel Layout */
			mInfo1 = (TextView)headerLayout.findViewById(R.id.tv_flexible_area_main_1_info_1);
			mInfo2 = (LinearLayout)headerLayout.findViewById(R.id.ll_flexible_area_main_1_info_2);
			mInfo3 = (LinearLayout)headerLayout.findViewById(R.id.ll_flexible_area_main_1_info_3);
			mInfo5 = (TextView)headerLayout.findViewById(R.id.tv_flexible_area_main_1_info_5);
			mInfo5.setTypeface(FontClass.setFont(getActivity()));
			mInfoBtn1 = (LinearLayout)headerLayout.findViewById(R.id.ll_flexible_area_main_1_info_btn_1);
			mInfoBtn2 = (LinearLayout)headerLayout.findViewById(R.id.ll_flexible_area_main_1_info_btn_2);
			mInfoBtn1.setOnClickListener(BtnClickListener);
			mInfoBtn2.setOnClickListener(BtnClickListener);


			/* Chart */
			mChart = (CombinedChart) headerLayout.findViewById(R.id.chart_bar_and_line_chart);
			mChart.setDescription("");
			mChart.setNoDataTextDescription("");
			mChart.setNoDataText("");
			mChart.setBackgroundColor(Color.TRANSPARENT);
			mChart.setPinchZoom(false);	// scaling can now only be done on x- and y-axis separately
			mChart.setScaleEnabled(false);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			// draw bars behind lines
			mChart.setDrawOrder(ChartUtils.mDrawOrder);

			MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
			mv.setChart(mChart);
			mChart.setMarkerView(mv);

			YAxis rightAxis = mChart.getAxisRight();
			rightAxis.setDrawGridLines(false);

			YAxis leftAxis = mChart.getAxisLeft();
			leftAxis.setDrawGridLines(false);

			XAxis xAxis = mChart.getXAxis();
			xAxis.setDrawAxisLine(false);
			xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
			xAxis.setDrawGridLines(false);
			xAxis.setTextSize(11f);
			xAxis.setYOffset(10);
			xAxis.setTextColor(Color.argb((int) (255 * 1), 255, 255, 255));

			/* Sample Data */
			mCombinedData = new CombinedData(new String[]{"","","","","",""});
//			mCombinedData = new CombinedData(ChartUtils.getMonth(0, 6));


			mLineEntries = new ArrayList<Entry>();
			mBarEntries = new ArrayList<BarEntry>();

			for (int i = 0; i < 6 ; i++) {
				mLineEntries.add(new Entry(0, i));
				mBarEntries.add(new BarEntry(0, i));
			}

			mCombinedData.setData(ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_1));
			mCombinedData.setData(ChartUtils.generateBarData(mBarEntries, APP.MAIN_LIST_CHART_USE_1));

			// hide
			mChart.getAxisRight().setEnabled(false);
			mChart.getAxisLeft().setEnabled(false);
			mChart.getLegend().setEnabled(false);

			mChart.setData(mCombinedData);
			mChart.invalidate();

			mChart.setOnChartValueSelectedListener(mOnChartValueSelectedListener);

		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}

	private void initData() {

		Bundle bundle = getArguments();
		mAge = bundle.getInt(Const.DEMO_AGE);
		mGender = bundle.getString(Const.DEMO_GENDER);

		mInfoBtn1.setSelected(true);
		refreshData();
	}

	/**  Refresh **/
	private void refreshData(){
		DialogUtils.showLoading(getActivity());
		Call<JsonElement> call = ServiceGenerator.createServiceForDemo(DemoApi.class).getAssetTrial(mAge, mGender);
		call.enqueue(new Callback<JsonElement>() {
			@Override
			public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
				DialogUtils.dismissLoading();
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						JsonElement element = response.body();
						JsonObject object = element.getAsJsonObject();
						if (object != null && object.has("assetList")) {
							JsonArray array = object.getAsJsonArray("assetList");
							Type listType = new TypeToken<List<TrialAsset>>() {}.getType();
							mData = new Gson().fromJson(array, listType);
							setData(mData);
						} else {
							ErrorUtils.parseError(response);
						}
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<JsonElement> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void setData(List<TrialAsset> list) {
		if (mAlInfoDataList != null){
			mAlInfoDataList.removeAll(mAlInfoDataList);
			if (mData != null) {
				Collections.sort(mData, myComparator);
			} else {
				CustomToast.makeText(this.getActivity(), "서버에 통신에 문제가 있습니다.", Toast.LENGTH_SHORT).show();
				return;
			}

			int dataSize = mData.size();
			TrialAsset trialAsset = mData.get(mData.size()-1);

			mAlInfoDataList.add(new InfoDataMain1("예금/적금",
					((dataSize != 0) ? "y" : "n"),
					((dataSize != 0) ? String.valueOf(trialAsset.deposit) : "0"),
					((dataSize != 0) ? String.format("%.2f", trialAsset.depositCount) : ""),
					"", "", "",
					"", "",
					""));

			mAlInfoDataList.add(new InfoDataMain1("월 평균 카드 사용액",
					((dataSize != 0) ? "y" : "n"),
					((dataSize != 0) ? TransFormatUtils.transNullToCost(String.valueOf(trialAsset.card)) : "0"),
					((dataSize != 0) ? String.format("%.2f", trialAsset.cardCount) : "0"),
					"", "", "",
					"", "",
					""));

			if (mAge >= 20) {
				mAlInfoDataList.add(new InfoDataMain1("평균 대출액",
						((dataSize != 0) ? "y" : "n"),
						((dataSize != 0) ? TransFormatUtils.transNullToCost(String.valueOf(trialAsset.loan)) : "0"),
						"",
						"", "", "",
						"", "",
						""));

				mAlInfoDataList.add(new InfoDataMain1("평균 기타재산 항목",
						((dataSize != 0) ? "y" : "n"), "", "",
						((dataSize != 0) ? String.valueOf(trialAsset.realEstate) : "0"),
						((dataSize != 0) ? String.valueOf(trialAsset.car) : "0"),
						"",
						"", "", ""));
			}

			mAdapter.notifyDataSetChanged();

			if (mInfoBtn1.isSelected()) {
				BtnClickListener.onClick(mInfoBtn1);
			}
			else {
				BtnClickListener.onClick(mInfoBtn2);
			}
		}
	}

//===============================================================================//
// Chart
//===============================================================================//


	/**  Refresh chart **/
	private void refreshChart(List<TrialAsset> list, boolean isDebt){
		mLineEntries.removeAll(mLineEntries);
		mBarEntries.removeAll(mBarEntries);
		String[] mMonthP = new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			mMonthP[i] = list.get(i).month.substring(4);
			if (isDebt){
				mLineEntries.add(new Entry(Long.valueOf(list.get(i).card+list.get(i).loan), i));
				mBarEntries.add(new BarEntry(Long.valueOf(list.get(i).card+list.get(i).loan), i));
			} else {
				mLineEntries.add(new Entry(Long.valueOf(list.get(i).deposit), i));
				mBarEntries.add(new BarEntry(Long.valueOf(list.get(i).deposit), i));
			}
		}
		mCombinedData = new CombinedData(mMonthP);
		mCombinedData.setData(ChartUtils.generateLineData(mLineEntries, APP.MAIN_LIST_CHART_USE_1));
		mCombinedData.setData(ChartUtils.generateBarData(mBarEntries, APP.MAIN_LIST_CHART_USE_1));
		mChart.animateY(1500);
		mChart.setData(mCombinedData);
		mChart.invalidate();
		mLastXIndex = mLineEntries.size() - 1;
		mChart.highlightValue(mLastXIndex,0);
	}


	@Override
	public void reset(boolean flag) {
		if (mListView != null) {
			mListView.setSelection(0);
		}
	}

	@Override
	public boolean canScrollUp() {
		boolean ret = false;
		if (mListView != null) {
			ret = ViewCompat.canScrollVertically(mListView, -1) || mListView.getScrollY() > 0;
		}
		return ret;
	}

	@Override
	public boolean canScrollDown() {
		return false;
	}
}
