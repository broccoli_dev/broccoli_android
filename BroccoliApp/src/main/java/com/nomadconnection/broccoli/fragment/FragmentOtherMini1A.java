package com.nomadconnection.broccoli.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivityOtherMiniDetail;
import com.nomadconnection.broccoli.adapter.AdtExpListViewOtherMini;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataOtherMini;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMain1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentOtherMini1A#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentOtherMini1A extends BaseFragment {

	private static String TAG = FragmentOtherMini1A.class.getSimpleName();

	/* Layout */
	private FrameLayout mNoDataLayout;
	private ArrayList<String> mTempGroupList = null;
	private ArrayList<String> mGroupList = null;
	private ArrayList<ArrayList<InfoDataOtherMini>> mChildList = null;
	private ArrayList<InfoDataOtherMini> mChildListContent1 = null;		// 미납
	private ArrayList<InfoDataOtherMini> mChildListContent2 = null;		// 완료
	private ArrayList<InfoDataOtherMini> mChildListContent3 = null;		// 오늘
	private ArrayList<InfoDataOtherMini> mChildListContent4 = null;		// 내일
	private ArrayList<InfoDataOtherMini> mChildListContent5 = null;		// 이번 주
	private ArrayList<InfoDataOtherMini> mChildListContent6 = null;		// 다음 주 이후
	private ExpandableListView mExpListView;
	private ArrayList<InfoDataOtherMini> mAlInfoDataList;

//	InterfaceRefresh mInterfaceRefresh;
//
//	@Override
//	public void onAttach(Activity activity) {
//		super.onAttach(activity);
//
//		// This makes sure that the container activity has implemented
//		// the callback interface. If not, it throws an exception
//		try {
//			mInterfaceRefresh = (InterfaceRefresh) activity;
//		} catch (ClassCastException e) {
//			throw new ClassCastException(activity.toString()
//					+ " must implement InterfaceRefresh");
//		}
//	}

	public FragmentOtherMini1A(ArrayList<InfoDataOtherMini> _AlInfoDataList) {
		// TODO Auto-generated constructor stub
		mAlInfoDataList = _AlInfoDataList;
//		mInterfaceRefresh = (InterfaceRefresh) getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//View v = inflater.inflate(R.layout.fragment_asset_credit_detail_a, null);
		View v = inflater.inflate(R.layout.fragment_other_mini_1_a, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


	@Override
	public void onResume() {
		super.onResume();
	}

	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try
		{
			/* getExtra */
			//				if (getIntent().getExtras() != null) {
			//				}

			mTempGroupList = new ArrayList<String>();
			mGroupList = new ArrayList<String>();
			mChildList = new ArrayList<ArrayList<InfoDataOtherMini>>();
			mChildListContent1 = new ArrayList<InfoDataOtherMini>();
			mChildListContent2 = new ArrayList<InfoDataOtherMini>();
			mChildListContent3 = new ArrayList<InfoDataOtherMini>();
			mChildListContent4 = new ArrayList<InfoDataOtherMini>();
			mChildListContent5 = new ArrayList<InfoDataOtherMini>();
			mChildListContent6 = new ArrayList<InfoDataOtherMini>();

			/* gourp list */
			mTempGroupList.add("미납");
			mTempGroupList.add("완료");
			mTempGroupList.add("오늘");
			mTempGroupList.add("내일");
			mTempGroupList.add("이번 주");
			mTempGroupList.add("다음 주 이후");
			for (int i = 0; i < mTempGroupList.size(); i++) {
				for (int j = 0; j < mAlInfoDataList.size(); j++) {
					if (mTempGroupList.get(i).equals(mAlInfoDataList.get(j).mTxt1)){
						mGroupList.add(mTempGroupList.get(i));
						break;
					}
				}
			}

			for (int i = 0; i < mAlInfoDataList.size(); i++) {
				if (mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(0).toString())) {
					/* gourp 1의 child */
					mChildListContent1.add(mAlInfoDataList.get(i));
				}
				else if (mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(1).toString())) {
					/* gourp 2의 child */
					mChildListContent2.add(mAlInfoDataList.get(i));
				}
				else if (mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(2).toString())) {
					/* gourp 3의 child */
					mChildListContent3.add(mAlInfoDataList.get(i));
				}
				else if (mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(3).toString())) {
					/* gourp 4의 child */
					mChildListContent4.add(mAlInfoDataList.get(i));
				}
				else if (mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(4).toString())) {
					/* gourp 5의 child */
					mChildListContent5.add(mAlInfoDataList.get(i));
				}
				else if (mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(5).toString())) {
					/* gourp 6의 child */
					mChildListContent6.add(mAlInfoDataList.get(i));
				}
			}

			/* child list에 각각의 content 넣기 */
			mChildList.add(mChildListContent1);
			mChildList.add(mChildListContent2);
			mChildList.add(mChildListContent3);
			mChildList.add(mChildListContent4);
			mChildList.add(mChildListContent5);
			mChildList.add(mChildListContent6);
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout */
			mNoDataLayout = (FrameLayout)_v.findViewById(R.id.inc_fragment_other_mini_1_a_empty);
			mExpListView = (ExpandableListView)_v.findViewById(R.id.elv_fragment_other_mini_1_a_explistview);
			mExpListView.setAdapter(new AdtExpListViewOtherMini(getActivity(), mGroupList, mChildList));

	        /* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return false;
				}
			});

			/* Child list click 시 */
			mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
											int groupPosition, int childPosition, long id) {
					InfoDataOtherMini mTemp = null;
					mTemp = mChildList.get(groupPosition).get(childPosition);
					if (mTemp == null) return true;

					startActivityForResult(new Intent(getActivity(), Level3ActivityOtherMiniDetail.class)
							.putExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_2, mTemp)
							, APP.ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_REQUEST_CODE);

//					Intent intent = new Intent(getActivity(), Level3ActivityOtherMiniDetail.class);
//					intent.putExtra(APP.INFO_DATA_OTHER_MINI_DEPTH_2, mTemp);
//					startActivity(intent);
					return false;
				}
			});


		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			for (int i = 0; i <mGroupList.size() ; i++) {
				mExpListView.expandGroup(i);
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// Refresh
//=========================================================================================//
	protected void refreshExpand(){
		refreshData();

		if (mGroupList != null && mExpListView != null)
			for (int i = 0; i <mGroupList.size() ; i++) {
				mExpListView.expandGroup(i);
			}
	}
	protected void refreshData(){
		if (mAlInfoDataList.size() == 0) {
			mNoDataLayout.setVisibility(View.VISIBLE);
			((ImageView)getActivity().findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_nohistory);
			((TextView)getActivity().findViewById(R.id.tv_empty_layout_txt)).setText("사용 내역이 없습니다."); //소유하신 머니캘린더를 입력해주세요
		}
		else {
			mNoDataLayout.setVisibility(View.GONE);
		}

//		Intent intent = getActivity().getIntent();
//		getActivity().setResult(APP.ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_RESULT_CODE_SUCCESS, intent);
//		getActivity().finish();
//		mInterfaceRefresh.onRefresh();
	}


}
