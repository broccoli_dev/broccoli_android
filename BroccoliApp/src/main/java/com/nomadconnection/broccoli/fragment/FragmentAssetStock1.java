package com.nomadconnection.broccoli.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetStockDetail;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.adapter.AdtExpListViewAssetStock;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.Asset.AssetStockInfo;
import com.nomadconnection.broccoli.data.Asset.BodyAssetStockInfo;
import com.nomadconnection.broccoli.data.Asset.ResponseStockDetail;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAssetStock1 extends BaseFragment{
	
	private static String TAG = FragmentAssetStock1.class.getSimpleName();

	/* Layout */
	private ArrayList<String> mGroupList = null;
	private ArrayList<ArrayList<BodyAssetStockInfo>> mChildList = null;
	private ArrayList<BodyAssetStockInfo> mChildListContent1 = null;
	private ArrayList<BodyAssetStockInfo> mChildListContent2 = null;
	private ExpandableListView mExpListView;
	private AdtExpListViewAssetStock mAdapter;

	private boolean mStockPageState = false;

	
	InterfaceEditOrDetail mInterFaceEditOrDetail;

	/* getParcelableExtra */
	private ArrayList<BodyAssetStockInfo> mAlInfoDataList;
	private boolean isFromStock = false;

	public FragmentAssetStock1() {}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_asset_stock_1, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.FragmentAssetStock1);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		if (mAlInfoDataList != null){
//			((LinearLayout)getActivity().findViewById(R.id.ll_navi_motion_else)).callOnClick();
			if (mAlInfoDataList.size() == 0) {
//				((FrameLayout)getActivity().findViewById(R.id.inc_fragment_asset_stock_1_empty)).setVisibility(View.VISIBLE);
//				((ImageView)getActivity().findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_02);
//				((TextView)getActivity().findViewById(R.id.tv_empty_layout_txt)).setText("소유하신 주식을 입력해주세요");
			}
			else ((FrameLayout)getActivity().findViewById(R.id.inc_fragment_asset_stock_1_empty)).setVisibility(View.GONE);
		}
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_REQUEST_CODE) {
			if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_RESULT_CODE_SUCCESS){
				refreshData();
			}
			else if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_RESULT_CODE_CANCLE) {

			}
			else if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_RESULT_CODE_DELETE) {
				checkDataEmpty();
			}
		}
	}





	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			/* getExtra */
			if (getActivity().getIntent().hasExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND)) {
				mStockPageState = true;
			}
			Bundle bundle = getArguments();
			if (bundle != null) {
				mAlInfoDataList = bundle.getParcelableArrayList(Const.DATA);
			}
			mGroupList = new ArrayList<String>();
			mChildList = new ArrayList<ArrayList<BodyAssetStockInfo>>();
			mChildListContent1 = new ArrayList<BodyAssetStockInfo>();
			mChildListContent2 = new ArrayList<BodyAssetStockInfo>();

			/* gourp list */
//			for (int i = 0; i < mAlInfoDataList.size(); i++) {
//				if (!mGroupList.contains(mAlInfoDataList.get(i).mTxt1)) {
//					mGroupList.add(mAlInfoDataList.get(i).mTxt1);
//				}
//			}
			mGroupList.add("보유");
			mGroupList.add("매도");

			for (int i = 0; i < mAlInfoDataList.size(); i++) {
				if (!mAlInfoDataList.get(i).mBodyAssetStockSum.equals("0")) {
					/* gourp 1의 child */
					mChildListContent1.add(mAlInfoDataList.get(i));
				}
				else {
					/* gourp 2의 child */
					mChildListContent2.add(mAlInfoDataList.get(i));
				}
			}

			/* child list에 각각의 content 넣기 */
			mChildList.add(mChildListContent1);
			mChildList.add(mChildListContent2);
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try {
			/* Layout */
			mExpListView = (ExpandableListView)_v.findViewById(R.id.elv_fragment_asset_stock_1_explistview);
			mAdapter = new AdtExpListViewAssetStock(getActivity(), mGroupList, mChildList, mStockPageState);
			mExpListView.setAdapter(mAdapter);

	        /* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return true;
				}
			});

			/* Child list click 시 */
			mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
											int groupPosition, int childPosition, long id) {
					final BodyAssetStockInfo mTemp = mChildList.get(groupPosition).get(childPosition);
					if (mTemp == null) return true;

					NextStep(mTemp,  new ResponseStockDetail());



//					DialogUtils.showLoading(getActivity());											// 자산 로딩
//					Call<BodyAssetStockDetail> mCall = ServiceGenerator.createService(AssetApi.class).getStockDetail(
//							new RequestAssetStockDetail(Session.getInstance(getActivity()).getUserId(),
//									mTemp.mBodyAssetStockCode)
//					);
//
//					mCall.enqueue(new Callback<BodyAssetStockDetail>() {
//						@Override
//						public void onResponse(Response<BodyAssetStockDetail> response) {
//							DialogUtils.dismissLoading();											// 자산 로딩
//							if (APP._SAMPLE_MODE_) NextStep(mTemp, AssetSampleTestDataJson.GetSampleData5());
//							else NextStep(mTemp, response.body());
//						}
//
//						@Override
//						public void onFailure(Throwable t) {
//							DialogUtils.dismissLoading();											// 자산 로딩
//							if (APP._SAMPLE_MODE_) {
//								NextStep(mTemp, AssetSampleTestDataJson.GetSampleData5());
//							}
//							else CustomToast.getInstance(getActivity()).showShort("FragmentAssetStock1 서버 통신 실패 - 주식 상세 내역 조회");
//						}
//					});
					return false;
				}
			});
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			refreshData();
			/* Default Value */
			for (int i = 0; i <mGroupList.size() ; i++) {
				mExpListView.expandGroup(i);
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}


//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep **/
	private void NextStep(BodyAssetStockInfo _Header, ResponseStockDetail _Result){

		if(mStockPageState){
			startActivityForResult(new Intent(getActivity(), Level3ActivityAssetStockDetail.class)
					.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER, _Header)
					.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_BODY, _Result)
					.putExtra(APP.INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND, true)
					, APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_REQUEST_CODE);
		}
		else {
			startActivityForResult(new Intent(getActivity(), Level3ActivityAssetStockDetail.class)
					.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER, _Header)
					.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_BODY, _Result)
					, APP.ACTIVIYT_FOR_RESULT_ASSET_STOCK_REQUEST_CODE);
		}
//		startActivity(new Intent(getActivity(), Level3ActivityAssetStockDetail.class)
//						.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER, _Header)
//						.putExtra(APP.INFO_DATA_ASSET_STOCK_DEPTH_2_BODY, _Result)
//		);

	}


	private void refreshData(){
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		DialogUtils.showLoading(getActivity());											// 자산 로딩
		Call<AssetStockInfo> mCall = ServiceGenerator.createService(AssetApi.class).getStockList(mRequestDataByUserId);
		mCall.enqueue(new Callback<AssetStockInfo>() {
			@Override
			public void onResponse(Call<AssetStockInfo> call, Response<AssetStockInfo> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				AssetStockInfo mTemp;
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						mTemp = response.body();
						mAlInfoDataList.removeAll(mAlInfoDataList);
						mAlInfoDataList.addAll(mTemp.mAssetStockList);
						if (mTemp.mAssetStockList.size() == 0) ((LinearLayout)getActivity().findViewById(R.id.ll_navi_motion_else)).callOnClick();
						else resetData();
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<AssetStockInfo> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				AssetStockInfo mTemp;
				if (APP._SAMPLE_MODE_) {
					mTemp = AssetSampleTestDataJson.GetSampleData14();
					mAlInfoDataList.removeAll(mAlInfoDataList);
					mAlInfoDataList.addAll(mTemp.mAssetStockList);
					if (mTemp.mAssetStockList.size() == 0) ((LinearLayout)getActivity().findViewById(R.id.ll_navi_motion_else)).callOnClick();
					else resetData();
				} else
					ElseUtils.network_error(getActivity());
					//CustomToast.getInstance(getActivity()).showShort("FragmentAssetStock1 서버 통신 실패 : 주식 리스트");
			}
		});

	}

	private void checkDataEmpty() {
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		DialogUtils.showLoading(getActivity());											// 자산 로딩
		Call<AssetStockInfo> mCall = ServiceGenerator.createService(AssetApi.class).getStockList(mRequestDataByUserId);
		mCall.enqueue(new Callback<AssetStockInfo>() {
			@Override
			public void onResponse(Call<AssetStockInfo> call, Response<AssetStockInfo> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				AssetStockInfo mTemp;
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						mTemp = response.body();
						mAlInfoDataList.removeAll(mAlInfoDataList);
						mAlInfoDataList.addAll(mTemp.mAssetStockList);
						if (mTemp.mAssetStockList.size() == 0) {
							getActivity().finish();
						} else resetData();
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<AssetStockInfo> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				AssetStockInfo mTemp;
				if (APP._SAMPLE_MODE_) {
					mTemp = AssetSampleTestDataJson.GetSampleData14();
					mAlInfoDataList.removeAll(mAlInfoDataList);
					mAlInfoDataList.addAll(mTemp.mAssetStockList);
					if (mTemp.mAssetStockList.size() == 0) {
						getActivity().finish();
					} else resetData();
				} else
					ElseUtils.network_error(getActivity());
			}
		});
	}


	private void resetData(){
		if (mAdapter != null) {
			mGroupList.removeAll(mGroupList);
			mChildListContent1.removeAll(mChildListContent1);
			mChildListContent2.removeAll(mChildListContent2);
			mChildList.removeAll(mChildList);
			mGroupList.add("보유");
			mGroupList.add("매도");

			for (int i = 0; i < mAlInfoDataList.size(); i++) {
				if (!mAlInfoDataList.get(i).mBodyAssetStockSum.equals("0")) {
					/* gourp 1의 child */
					mChildListContent1.add(mAlInfoDataList.get(i));
				}
				else {
					/* gourp 2의 child */
					mChildListContent2.add(mAlInfoDataList.get(i));
				}
			}

			/* child list에 각각의 content 넣기 */
			mChildList.add(mChildListContent1);
			mChildList.add(mChildListContent2);

			mAdapter.setData(mGroupList, mChildList);
		}
	}




}
