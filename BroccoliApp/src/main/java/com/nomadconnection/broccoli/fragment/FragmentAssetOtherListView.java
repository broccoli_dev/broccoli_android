package com.nomadconnection.broccoli.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.Level3ActivityAssetOtherDetail;
import com.nomadconnection.broccoli.activity.asset.Level3ActivityAssetOther;
import com.nomadconnection.broccoli.adapter.AdtExpListViewAssetOther;
import com.nomadconnection.broccoli.api.AssetApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Asset.AssetCarInfo;
import com.nomadconnection.broccoli.data.Asset.AssetEstateInfo;
import com.nomadconnection.broccoli.data.Asset.AssetEtcInfo;
import com.nomadconnection.broccoli.data.Asset.AssetOtherInfo;
import com.nomadconnection.broccoli.data.Asset.AssetSampleTestDataJson;
import com.nomadconnection.broccoli.data.InfoDataAssetOther;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAssetOtherListView extends BaseFragment {
	
	private static String TAG = FragmentAssetOtherListView.class.getSimpleName();
	public static final String ESTATE_DATA = "ESTATE_DATA";
	public static final String CAR_DATA = "CAR_DATA";
	public static final String CUSTOM_DATA = "CUSTOM_DATA";

	/* Layout */
	private ArrayList<String> mGroupList = null;
	private ArrayList<ArrayList<InfoDataAssetOther>> mChildList = null;
	private ArrayList<InfoDataAssetOther> mChildListContent1 = null;
	private ArrayList<InfoDataAssetOther> mChildListContent2 = null;
	private ArrayList<InfoDataAssetOther> mChildListContent3 = null;
	private ExpandableListView mExpListView;
	private AdtExpListViewAssetOther mAdapter;

	/* getParcelableExtra */
	private ArrayList<InfoDataAssetOther> mAlInfoDataList;
	private ArrayList<AssetEstateInfo> mAlInfoDataListHouse;
	private ArrayList<AssetCarInfo> mAlInfoDataListCar;
	private ArrayList<AssetEtcInfo> mAlInfoDataListCustom;

	InterfaceEditOrDetail mInterFaceEditOrDetail;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_asset_other_1, null);
		
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onResume() {
		super.onResume();
		if (mAlInfoDataListHouse != null && mAlInfoDataListCar != null && mAlInfoDataListCustom != null){
			if (mAlInfoDataListHouse.size() == 0 && mAlInfoDataListCar.size() == 0 && mAlInfoDataListCustom.size() == 0) {
//				((LinearLayout)getActivity().findViewById(R.id.ll_navi_motion_else)).callOnClick();
//				((FrameLayout)getActivity().findViewById(R.id.inc_fragment_asset_other_1_empty)).setVisibility(View.VISIBLE);
//				((ImageView)getActivity().findViewById(R.id.iv_empty_layout_icon)).setImageResource(R.drawable.empty_icon_03);
//				((TextView)getActivity().findViewById(R.id.tv_empty_layout_txt)).setText("소유하신 기타 재산을 입력해주세요");
			}
			else ((FrameLayout)getActivity().findViewById(R.id.inc_fragment_asset_other_1_empty)).setVisibility(View.GONE);
		}
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_REQUEST_CODE) {
			if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_SUCCESS){
				refreshData();
			}
			else if(resultCode == APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_CANCLE){
			}
		}
	}


	//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
			mAlInfoDataList = new ArrayList<InfoDataAssetOther>();

			/* getExtra */
			mGroupList = new ArrayList<String>();
			mChildList = new ArrayList<ArrayList<InfoDataAssetOther>>();
			mChildListContent1 = new ArrayList<InfoDataAssetOther>();
			mChildListContent2 = new ArrayList<InfoDataAssetOther>();
			mChildListContent3 = new ArrayList<>();

			/* gourp list */
			for (int i = 0; i < mAlInfoDataList.size(); i++) {
				if (!mGroupList.contains(mAlInfoDataList.get(i).mTxt1)) {
					mGroupList.add(mAlInfoDataList.get(i).mTxt1);
				}
			}

			for (int i = 0; i < mAlInfoDataList.size(); i++) {
				if (mGroupList.size() > 0 && mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(0).toString())) {
					/* gourp 1의 child */
					mChildListContent1.add(mAlInfoDataList.get(i));
				}
				else if (mGroupList.size() > 1 && mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(1).toString())) {
					/* gourp 2의 child */
					mChildListContent2.add(mAlInfoDataList.get(i));
				} else {
					mChildListContent3.add(mAlInfoDataList.get(i));
				}
			}

			/* child list에 각각의 content 넣기 */
			mChildList.add(mChildListContent1);
			mChildList.add(mChildListContent2);
			mChildList.add(mChildListContent3);
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout */
			mExpListView = (ExpandableListView)_v.findViewById(R.id.elv_fragment_asset_other_1_explistview);
			mAdapter = new AdtExpListViewAssetOther(getActivity(), mGroupList, mChildList);
			mExpListView.setAdapter(mAdapter);

	        /* group list click 시 */
			mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					return true;
				}
			});

			/* Child list click 시 */
			mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
											int groupPosition, int childPosition, long id) {
					final InfoDataAssetOther mTemp = mChildList.get(groupPosition).get(childPosition);
					if (mTemp == null) return true;

					if (mTemp.mTxt1.equals("부동산")){
						NextStep(mAlInfoDataListHouse.get(childPosition), new AssetCarInfo(), mTemp.mTxt1);
					}
					else if (mTemp.mTxt1.equalsIgnoreCase("기타")) {
						NextStep(mAlInfoDataListCustom.get(childPosition));
					}
					else{	// 차량
						NextStep(new AssetEstateInfo(), mAlInfoDataListCar.get(childPosition), mTemp.mTxt1);
					}
					return false;
				}
			});
			
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try {
			Bundle arguments = getArguments();
			if (arguments != null) {
				mAlInfoDataListHouse = arguments.getParcelableArrayList(ESTATE_DATA);
				mAlInfoDataListCar = arguments.getParcelableArrayList(CAR_DATA);
				mAlInfoDataListCustom = arguments.getParcelableArrayList(CUSTOM_DATA);
			}

			resetData();
			/* Default Value */
			for (int i = 0; i <mGroupList.size() ; i++) {
				mExpListView.expandGroup(i);
			}
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}





//===============================================================================//
// NextStep
//===============================================================================//
	/**  NextStep **/
	private void NextStep(AssetEstateInfo _AssetEstateInfo, AssetCarInfo _AssetCarInfo, String _Which){
		startActivityForResult(new Intent(getActivity(), Level3ActivityAssetOtherDetail.class)
						.putExtra(APP.INFO_DATA_ASSET_OTHER_HOUSE_DEPTH_2, _AssetEstateInfo)
						.putExtra(APP.INFO_DATA_ASSET_OTHER_CAR_DEPTH_2, _AssetCarInfo)
						.putExtra(APP.INFO_DATA_ASSET_OTHER_HOUSE_OR_CAR_DEPTH_2, _Which)
				, APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_REQUEST_CODE);
	}

	private void NextStep(AssetEtcInfo etcInfo) {
		startActivityForResult(new Intent(getActivity(), Level3ActivityAssetOther.class)
						.putExtra(Const.DATA, etcInfo)
						.putExtra(Level3ActivityAssetOther.OTHER_TYPE, Level3ActivityAssetOther.OTHER_CUSTOM)
				, APP.ACTIVIYT_FOR_RESULT_ASSET_OTHER_REQUEST_CODE);
	}


	private void refreshData(){
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		DialogUtils.showLoading(getActivity());											// 자산 로딩
		Call<AssetOtherInfo> mCall = ServiceGenerator.createService(AssetApi.class).getOtherList(mRequestDataByUserId);
		mCall.enqueue(new Callback<AssetOtherInfo>() {
			@Override
			public void onResponse(Call<AssetOtherInfo> call, Response<AssetOtherInfo> response) {
				DialogUtils.dismissLoading();											// 자산 로딩
				AssetOtherInfo mTemp;
				if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
					if (response.body() != null) {
						mTemp = response.body();
						mAlInfoDataListHouse.removeAll(mAlInfoDataListHouse);
						mAlInfoDataListCar.removeAll(mAlInfoDataListCar);
						mAlInfoDataListCustom.removeAll(mAlInfoDataListCustom);
						mAlInfoDataListHouse.addAll(mTemp.mMainAssetRealEstate);
						mAlInfoDataListCar.addAll(mTemp.mMainAssetCar);
						mAlInfoDataListCustom.addAll(mTemp.mMainAssetEtc);
						resetData();
					} else {
						ElseUtils.network_error(getActivity());
					}
				} else {
					ElseUtils.network_error(getActivity());
				}
			}

			@Override
			public void onFailure(Call<AssetOtherInfo> call, Throwable t) {
				DialogUtils.dismissLoading();											// 자산 로딩
				AssetOtherInfo mTemp;
				if (APP._SAMPLE_MODE_) {
					mTemp = AssetSampleTestDataJson.GetSampleData15();
					mAlInfoDataListHouse.removeAll(mAlInfoDataListHouse);
					mAlInfoDataListCar.removeAll(mAlInfoDataListCar);
					mAlInfoDataListCustom.removeAll(mAlInfoDataListCustom);
					mAlInfoDataListHouse.addAll(mTemp.mMainAssetRealEstate);
					mAlInfoDataListCar.addAll(mTemp.mMainAssetCar);
					mAlInfoDataListCustom.addAll(mTemp.mMainAssetEtc);
					resetData();
				}
				else
					ElseUtils.network_error(getActivity());
			}
		});

	}

	private void resetData(){
		if (mAlInfoDataList != null) mAlInfoDataList.removeAll(mAlInfoDataList);

		for (int i = 0; i < mAlInfoDataListHouse.size() ; i++) {
			mAlInfoDataList.add(new InfoDataAssetOther("부동산",
					mAlInfoDataListHouse.get(i).mAssetEstateName,
					mAlInfoDataListHouse.get(i).mAssetEstateDealType,
					mAlInfoDataListHouse.get(i).mAssetEstatePrice,
					mAlInfoDataListHouse.get(i).mAssetEstateId,
					mAlInfoDataListHouse.get(i).mAssetEstateType,
					"", ""));
		}
		for (int i = 0; i < mAlInfoDataListCar.size() ; i++) {
			mAlInfoDataList.add(new InfoDataAssetOther("차량",
					mAlInfoDataListCar.get(i).mAssetCarCode,
					mAlInfoDataListCar.get(i).mAssetCarYear,
					mAlInfoDataListCar.get(i).mAssetCarMarketPrice,
					mAlInfoDataListCar.get(i).mAssetCarId,
					mAlInfoDataListCar.get(i).mAssetCarMakerCode,
					mAlInfoDataListCar.get(i).mAssetCarSubCode,
					""));
		}
		for (int i = 0; i < mAlInfoDataListCustom.size() ; i++) {
			mAlInfoDataList.add(new InfoDataAssetOther("기타",
					mAlInfoDataListCustom.get(i).type,
					mAlInfoDataListCustom.get(i).description,
					mAlInfoDataListCustom.get(i).quantity,
					mAlInfoDataListCustom.get(i).value,
					"",
					"",
					""));
		}
		if (mAdapter != null) {
			mGroupList.removeAll(mGroupList);
			mChildListContent1.removeAll(mChildListContent1);
			mChildListContent2.removeAll(mChildListContent2);
			mChildListContent3.removeAll(mChildListContent3);
			mChildList.removeAll(mChildList);
			/* gourp list */
			for (int i = 0; i < mAlInfoDataList.size(); i++) {
				if (!mGroupList.contains(mAlInfoDataList.get(i).mTxt1)) {
					mGroupList.add(mAlInfoDataList.get(i).mTxt1);
				}
			}

			for (int i = 0; i < mAlInfoDataList.size(); i++) {
				if (mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(0).toString())) {
					/* gourp 1의 child */
					mChildListContent1.add(mAlInfoDataList.get(i));
				} else if (mAlInfoDataList.get(i).mTxt1.equals(mGroupList.get(1).toString())) {
					/* gourp 2의 child */
					mChildListContent2.add(mAlInfoDataList.get(i));
				} else {
					mChildListContent3.add(mAlInfoDataList.get(i));
				}
			}

			/* child list에 각각의 content 넣기 */
			mChildList.add(mChildListContent1);
			mChildList.add(mChildListContent2);
			mChildList.add(mChildListContent3);

			mAdapter.setData(mGroupList, mChildList);
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (mAlInfoDataListHouse.size() == 0 && mAlInfoDataListCar.size() == 0 && mAlInfoDataListCustom.size() == 0)
					((LinearLayout)getActivity().findViewById(R.id.ll_navi_motion_else)).callOnClick();
			}
		}, 200);
	}


}
