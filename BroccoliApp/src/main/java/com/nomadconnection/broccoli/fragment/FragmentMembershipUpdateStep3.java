package com.nomadconnection.broccoli.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.membership.LoginActivity;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.data.Session.OldUserData;
import com.nomadconnection.broccoli.data.Session.ResponseBroccoli;
import com.nomadconnection.broccoli.data.Session.UserData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.BroccoliLoginFilter;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2016. 12. 13..
 */

public class FragmentMembershipUpdateStep3 extends BaseFragment {

    private EditText mEmail;
    private EditText mPwd;
    private EditText mPwdCheck;
    private TextView mEmailResult;
    private TextView mPwdResult;
    private boolean isPwdAvailable = false;
    private boolean isPwdMatched = false;
    private boolean isEmailChecked = false;
    private UserApi mApi;
    private View mBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_membership_update_step3, null);
        ((BaseActivity)getActivity()).sendTracker(AnalyticsName.ExUserEmailRegister);
        init(view);

        return view;
    }

    private void init(View view) {
        mApi = ServiceGenerator.createService(UserApi.class);

        mBtn = view.findViewById(R.id.btn_confirm);
        mBtn.setEnabled(false);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNext();
            }
        });
        mEmail = (EditText) view.findViewById(R.id.et_fragment_membership_email_id);
        mPwd = (EditText) view.findViewById(R.id.et_fragment_membership_password);
        mPwdCheck = (EditText) view.findViewById(R.id.et_fragment_membership_password_check);
        mEmailResult = (TextView) view.findViewById(R.id.tv_fragment_membership_email_result);
        mPwdResult = (TextView) view.findViewById(R.id.tv_fragment_membership_password_result);

        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isEmailChecked = false;
                if (ElseUtils.validateEmail(s.toString())) {
                    isEmailChecked = true;
                    mEmailResult.setText(null);
                    mEmailResult.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mEmailResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                } else {
                    mEmailResult.setText("유효하지 않은 이메일 형식입니다.");
                    mEmailResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                    mEmailResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                }
                checkButtonStatus();
            }
        });

        InputFilter[] filters = new InputFilter[]{new BroccoliLoginFilter(new BroccoliLoginFilter.OnInputListener() {
            @Override
            public void isAllowed(BroccoliLoginFilter.InputResult allowed) {
                isPwdAvailable = false;
                switch (allowed) {
                    case ALLOWED:
                        isPwdAvailable = true;
                        mPwdResult.setText("사용 가능한 비밀번호");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                        break;
                    case NEED_INCLUDE_NUMBER:
                    case NEED_INCLUDE_CHAR:
                        mPwdResult.setText("영문과 숫자를 함께 사용해야 합니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case NOT_ALLOWED_CHARACTER:
                        mPwdResult.setText("!@#$%^&*()만 입력 가능합니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case NOT_ENOUGH_LENGTH:
                        mPwdResult.setText("8~20자리까지 입력 가능합니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                    case TOO_LONG_LENGTH:
                        mPwdResult.setText("8~20자리까지 입력 가능합니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                        break;
                }
            }
        })};
        mPwd.setFilters(filters);

        mPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwdCheck = mPwdCheck.getText().toString();
                isPwdMatched = false;
                if (isPwdAvailable && s.length() != 0 && pwdCheck.length() > 0) {
                    if (pwdCheck.equalsIgnoreCase(s.toString())) {
                        isPwdMatched = true;
                        mPwdResult.setText("비밀번호 일치");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    } else {
                        mPwdResult.setText("비밀번호가 일치하지 않습니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    }
                }
                checkButtonStatus();
            }
        });

        mPwdCheck.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pwd = mPwd.getText().toString();
                isPwdMatched = false;
                if (isPwdAvailable && s.length() != 0) {
                    if (pwd.equalsIgnoreCase(s.toString())) {
                        isPwdMatched = true;
                        mPwdResult.setText("비밀번호 일치");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.tag_d_txt_color));
                    } else {
                        mPwdResult.setText("비밀번호가 일치하지 않습니다.");
                        mPwdResult.setCompoundDrawablesWithIntrinsicBounds(R.drawable.alert_a_icon, 0, 0, 0);
                        mPwdResult.setTextColor(getResources().getColor(R.color.common_rgb_230_57_57));
                    }
                }
                checkButtonStatus();
            }
        });
    }

    private void checkButtonStatus() {
        boolean ret = true;
        if (mEmail.getText().length() <= 5) {
            ret = false;
        }
        if (!isEmailChecked) {
            ret = false;
        }
        if (!isPwdMatched) {
            ret = false;
        }
        if (ret) {
            mBtn.setEnabled(true);
        } else {
            mBtn.setEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onNext() {
        Session.getInstance(getActivity()).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                OldUserData data = new OldUserData();
                LoginData loginData = DbManager.getInstance().getUserInfoDataSet(getActivity(), str, Session.getInstance(getActivity()).getUserId());
                data.setUserId(loginData.getUserId());
                data.setPassword(loginData.getPinNumber());
                checkExistBroccoliMembership(data);
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        memberLeaveComplete();
                        break;
                }
            }

            @Override
            public void fail() {

            }
        });
    }

    private void checkExistBroccoliMembership(final OldUserData data) {
        Call<Result> call = mApi.oldMemberExist(data);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response != null) {
                        Result result = response.body();
                        if (result.isOk() || result.checkUnable()) {
                            upgradeUser(data);
                        } else {
                            Toast.makeText(getActivity(), "브로콜리에 회원정보가 존재하지 않습니다. 새로 회원가입을 해주세요.", Toast.LENGTH_LONG);
                            Session.getInstance(getActivity()).deleteLoginInfo(data.getUserId());
                            getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
            }
        });
    }

    private void addMembership(final long dpid, final String dpToken) {
        DialogUtils.showLoading(getActivity());
        UserData data = new UserData();
        data.setDpId(dpid);
        data.setDpAccessToken(dpToken);
        data.setOsType(1);
        data.setDeviceId(ElseUtils.getDeviceUUID(getActivity()));
        data.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
        data.setAdId(APPSharedPrefer.getInstance(getActivity()).getPrefString(APP.SP_USER_ADID));
        Call<ResponseBroccoli> call = ServiceGenerator.createService(UserApi.class).userAdd(data);
        call.enqueue(new Callback<ResponseBroccoli>() {
            @Override
            public void onResponse(Call<ResponseBroccoli> call, Response<ResponseBroccoli> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    ResponseBroccoli result = response.body();
                    if (result != null) {
                        final String broccoliToken = response.headers().get(Const.TOKEN);
                        final String id = result.userId;
                        final String sign = result.signedRequest;
                        try {
                            Session.getInstance(getActivity()).acquirePublicFromId(id, new Session.OnKeyInterface() {
                                @Override
                                public void acquired(final String str) {
                                    try {
                                        LoginData loginData = DbManager.getInstance().getUserInfoDataSet(getContext(), str, id);
                                        loginData.setUserId(id);
                                        loginData.setPassword(sign);
                                        loginData.setDayliId(dpid);
                                        loginData.setDayliToken(dpToken);
                                        loginData.setPushKey(MainApplication.mAPPShared.getPrefString(APP.SP_USER_GCM));
                                        loginData.setType(1); // android
                                        loginData.setDeviceId(ElseUtils.getDeviceUUID(getContext()));
                                        loginData.setLogin(LoginData.TRUE);
                                        Date current = Calendar.getInstance().getTime();
                                        loginData.setLastLogin(ElseUtils.getSimpleDate(current));
                                        DbManager.getInstance().insertOrUpdateUserInfoData(getContext(), str, loginData);
                                        Session.getInstance(getContext()).setToken(broccoliToken);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    DialogUtils.dismissLoading();
                                }

                                @Override
                                public void error(ErrorMessage errorMessage) {
                                    switch (errorMessage) {
                                        case ERROR_713_NO_SUCH_USER:
                                        case ERROR_10007_SESSION_NOT_FOUND:
                                        case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                                        case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                                        case ERROR_20004_NO_SUCH_USER:
                                            memberLeaveComplete();
                                            break;
                                    }
                                }

                                @Override
                                public void fail() {
                                    DialogUtils.dismissLoading();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    DialogUtils.dismissLoading();
                } else {
                    DialogUtils.dismissLoading();
                    Toast.makeText(getContext(), "서버와 통신 중 문제가 발생하였습니다. 잠시 후 비밀번호를 재입력 해주십시오.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBroccoli> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getActivity());
            }
        });
    }

    private void upgradeUser(final OldUserData data) {
        String email = mEmail.getText().toString();
        String newPwd = mPwd.getText().toString();
        data.setEmail(email);
        data.setNewPassword(newPwd);
        Call<ResponseBroccoli> call = mApi.oldMemberUpgrade(data);
        call.enqueue(new Callback<ResponseBroccoli>() {
            @Override
            public void onResponse(Call<ResponseBroccoli> call, Response<ResponseBroccoli> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    if (response != null) {
                        Result result = response.body();
                        if (result.isOk() || result.checkUnable()) {
                            ResponseBroccoli broccoli = response.body();
                            String token = response.headers().get(Const.TOKEN);
                            upgradeDB(broccoli, token);
                        } else {
                            ErrorMessage message = ErrorUtils.parseError(response);
                            switch (message) {
                                case ERROR_712_EXIST_USER:
                                    Toast.makeText(getActivity(), "이미 회원가입을 하셨습니다. 로그인화면에서 로그인해주세요.", Toast.LENGTH_LONG);
                                    Session.getInstance(getActivity()).deleteLoginInfo(data.getUserId());
                                    getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                                    getActivity().finish();
                                    break;
                                case ERROR_719_QUITE_USER:
                                    Toast.makeText(getActivity(), "탈퇴 회원은 탈퇴일로부터 5일 이후 재가입 가능합니다.", Toast.LENGTH_LONG).show();
                                    Session.getInstance(getActivity()).deleteLoginInfo(data.getUserId());
                                    getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                                    getActivity().finish();
                                    break;
                                default:
                                    Toast.makeText(getActivity(), "에러가 발생하였습니다.\n" + message.name(), Toast.LENGTH_LONG);
                                    break;
                            }
                        }
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<ResponseBroccoli> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getActivity());
            }
        });
    }

    private void upgradeDB(final ResponseBroccoli broccoli, final String token) {
        Session.getInstance(getContext()).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                LoginData data = DbManager.getInstance().getUserInfoDataSet(getActivity(), str, broccoli.userId);
                data.setDayliToken(broccoli.dpAccessToken);
                data.setDayliId(broccoli.dpId);
                data.setUserId(broccoli.userId);
                data.setPassword(broccoli.signedRequest);
                data.setLogin(LoginData.TRUE);
                Date current = Calendar.getInstance().getTime();
                data.setLastLogin(ElseUtils.getSimpleDate(current));
                Session.getInstance(getActivity()).setToken(token);
                try {
                    DbManager.getInstance().updateDb(getActivity(), str, data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                List<BankData> bankDatas = ScrapDao.getInstance().getOldBankData(getActivity(), str);
                ScrapDao.getInstance().removeBankData(getActivity());
                for (BankData bankData : bankDatas) {
                    ScrapDao.getInstance().insertBankData(getActivity(), str, broccoli.userId, bankData);
                }
                terms_agree(broccoli.dpAccessToken); // 약관동의 항목 서버 올리기
                Session.getInstance(getActivity()).setSimpleUserInfo(data, new Session.OnUserInfoUpdateCompleteInterface() {
                    @Override
                    public void complete(UserPersonalInfo info) {
                        startWelcome();
                    }

                    @Override
                    public void error(ErrorMessage message) {
                        switch (message) {
                            case ERROR_713_NO_SUCH_USER:
                            case ERROR_10007_SESSION_NOT_FOUND:
                            case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                            case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                            case ERROR_20004_NO_SUCH_USER:
                                memberLeaveComplete();
                                break;
                        }
                    }
                });
                Session.getInstance(getActivity()).setLock(false);
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        memberLeaveComplete();
                        break;
                }
            }

            @Override
            public void fail() {
                ElseUtils.network_error(getActivity());
            }
        });
    }

    private void terms_agree(String dpToken) {    // 약관동의 항목 서버 올리기
        ArrayList<Integer> mAgrees = getArguments().getIntegerArrayList(Const.TERMS_AGREE);
        Call<Result> call = ServiceGenerator.createServiceDayli(DayliApi.class).termsAgree(dpToken, mAgrees);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                ElseUtils.network_error(getActivity());
            }
        });

    }

    private void nextStep(BaseFragment fragment) {
        try {
            setCurrentFragment(R.id.ll_activity_membership_update_fragment_area, fragment, fragment.getTagName());
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void startWelcome() {
        BaseFragment fragment = new FragmentMembershipUpdateStep4();
        nextStep(fragment);
    }

}
