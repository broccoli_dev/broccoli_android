package com.nomadconnection.broccoli.fragment.remit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.remit.RemitRegisterActivity;
import com.nomadconnection.broccoli.api.RemitApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Remit.RemitBankDetail;
import com.nomadconnection.broccoli.data.Remit.RemitUserInfo;
import com.nomadconnection.broccoli.data.Remit.RequestDataARS;
import com.nomadconnection.broccoli.data.Remit.RequestDataARSCheck;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitARS;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitARSCheck;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentRemitRegisterARS extends BaseFragment implements OnClickListener {

	public static final String TAG = FragmentRemitRegisterARS.class.getSimpleName();

	private Bundle mArgument;
	private RemitBankDetail mRemitBankDetail;
	private RemitUserInfo mRemitUserInfo;
	private FrameLayout mARSGuide;
	private TextView mName;
	private TextView mPhoneNumber;
	private TextView mCerifyNumber;
	private TextView mFallBackTime;
	private Button mRequestBtn;
	private FrameLayout mNextBtn;
	private TextView mNextBtnStr;
	private String mGetAccountId = null;
	private String mGetFailCount = "";
	private CountDownTimer countDownTimer = null;


	public FragmentRemitRegisterARS() {
		// TODO Auto-generated constructor stub
		setTagName(TAG);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_remit_register_ars, null);

		init(view);
//		((BaseActivity)getActivity()).sendTracker(AnalyticsName.FragmentMembershipFirst);
//		Dialog_Notice();//공인인증서 등록 알림 팝업
		
		return view;
	}
	
	void init(View view) {
		mArgument = getArguments();
		mRemitBankDetail = (RemitBankDetail) mArgument.get(Const.DATA);

		mARSGuide = (FrameLayout) view.findViewById(R.id.fl_fragment_remit_register_third_guide);
		mName = (TextView) view.findViewById(R.id.fragment_remit_register_third_name);
		mPhoneNumber = (TextView) view.findViewById(R.id.tv_fragment_remit_register_phone_number);
		mCerifyNumber = (TextView) view.findViewById(R.id.tv_fragment_remit_register_certification_number);
		mFallBackTime = (TextView) view.findViewById(R.id.tv_fragment_remit_register_fall_back_time);

		mRequestBtn = (Button) view.findViewById(R.id.btn_fragment_remit_register_ARS);
		mRequestBtn.setOnClickListener(this);
		mNextBtn = (FrameLayout) view.findViewById(R.id.fl_fragment_remit_register_third_next);
		mNextBtnStr = (TextView) view.findViewById(R.id.tv_fragment_remit_register_third_btn);
		if(mArgument.getBoolean(Const.REMIT_REGISTER)){
			mARSGuide.setVisibility(View.GONE);
			mNextBtnStr.setText(getString(R.string.common_complete));
		}
		else {
			mARSGuide.setVisibility(View.VISIBLE);
			mNextBtnStr.setText(getString(R.string.common_complete));
		}
		mNextBtn.setOnClickListener(this);
		mNextBtn.setEnabled(false);

		ARS_User_Info();
	}
	
	@Override
	public void onStart() {
		super.onStart();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(mArgument.getBoolean(Const.REMIT_REGISTER)){
			((RemitRegisterActivity) getActivity()).setStepGuide(2);
		}

//		if(mGetAccountId != null && !"".equals(mGetAccountId)){
//			ARS_Check();
//		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if(countDownTimer != null){
			countDownTimer.cancel();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if(countDownTimer != null){
			countDownTimer.cancel();
		}
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		int id = view.getId();
		switch (id) {
			case R.id.btn_fragment_remit_register_ARS:
				ARS_Certify();
				break;
			case R.id.fl_fragment_remit_register_third_next:
				ARS_Check();
				break;
		}
	}

	private void ARS_Certify() {
		DialogUtils.showLoading(getActivity());
		RequestDataARS mRequestDataARS = new RequestDataARS();
		mRequestDataARS.setUserId(Session.getInstance(getActivity()).getUserId());
		mRequestDataARS.setCompanyCode(mRemitBankDetail.getCompanyCode());
		mRequestDataARS.setAccountNumber(mRemitBankDetail.getAccountNo());
		mRequestDataARS.setHostName(mRemitUserInfo.getHostName());
		mRequestDataARS.setTelNo(mPhoneNumber.getText().toString().replaceAll("-", ""));
		mRequestDataARS.setInstanceId(mRemitUserInfo.getInstanceId());
		mRequestDataARS.setAccountId(mRemitBankDetail.getAccountID());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ResponseRemitARS> call = api.regArsAndUserAccount(mRequestDataARS);
		call.enqueue(new Callback<ResponseRemitARS>() {
			@Override
			public void onResponse(Call<ResponseRemitARS> call, Response<ResponseRemitARS> response) {
				DialogUtils.dismissLoading();
				if(response.body().getResult() != null && response.body().getResult().equalsIgnoreCase(Const.OK)){
					mNextBtn.setEnabled(true);
					mGetAccountId = response.body().getAccountId();
				}
				else {
					Toast.makeText(getActivity(), response.body().getDesc(), Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitARS> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private void ARS_User_Info(){
		DialogUtils.showLoading(getActivity());
		RequestDataByUserId mRequestDataByUserId = new RequestDataByUserId();
		mRequestDataByUserId.setUserId(Session.getInstance(getActivity()).getUserId());

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<RemitUserInfo> call = api.getSendMoneyBasicInfo(mRequestDataByUserId);
		call.enqueue(new Callback<RemitUserInfo>() {
			@Override
			public void onResponse(Call<RemitUserInfo> call, Response<RemitUserInfo> response) {
				DialogUtils.dismissLoading();
				mRemitUserInfo = response.body();
				mName.setText(mRemitUserInfo.getHostName());
				if(mRemitUserInfo.getPhoneNo().length() != 0){
					mPhoneNumber.setText(ElseUtils.makePhoneNumber(mRemitUserInfo.getPhoneNo()));
				}
				else {
					mPhoneNumber.setText(ElseUtils.makePhoneNumber(getMyPhoneNember()));
				}

				mCerifyNumber.setText(mRemitUserInfo.getRandValue());

				if(mArgument.getBoolean(Const.REMIT_REGISTER)){
					((RemitRegisterActivity) getActivity()).setStepGuide(2);
				}

				startTimer();
			}

			@Override
			public void onFailure(Call<RemitUserInfo> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());

				if(mArgument.getBoolean(Const.REMIT_REGISTER)){
					((RemitRegisterActivity) getActivity()).setStepGuide(2);
				}
			}
		});
	}

	private void ARS_Check(){
		DialogUtils.showLoading(getActivity());
		RequestDataARSCheck mRequestDataARSCheck = new RequestDataARSCheck();
		mRequestDataARSCheck.setUserId(Session.getInstance(getActivity()).getUserId());
		mRequestDataARSCheck.setAccountId(mGetAccountId);
		if(!"".equals(mGetFailCount)){
			mRequestDataARSCheck.setFailCount(mGetFailCount);
		}

		RemitApi api = ServiceGenerator.createRemitService(RemitApi.class);
		Call<ResponseRemitARSCheck> call = api.chkArs(mRequestDataARSCheck);
		call.enqueue(new Callback<ResponseRemitARSCheck>() {
			@Override
			public void onResponse(Call<ResponseRemitARSCheck> call, Response<ResponseRemitARSCheck> response) {
				DialogUtils.dismissLoading();
				if(response.body().getResult() != null && response.body().getResult().equalsIgnoreCase(Const.OK)){
					if(mArgument.getBoolean(Const.REMIT_REGISTER)){
						Intent intent = new Intent();
						getActivity().setResult(getActivity().RESULT_OK, intent);
						getActivity().finish();
					}
					else {
						getActivity().finish();
					}
				}
				else{
					if(response.body().getCode() != null && "801".equals(response.body().getCode())){
						Toast.makeText(getActivity(), getActivity().getString(R.string.remit_register_text_step_ars_guide), Toast.LENGTH_SHORT).show();
					}

					if(!"0".equals(response.body().getFailCount())){
						mGetFailCount = response.body().getFailCount();
						Toast.makeText(getActivity(), getActivity().getString(R.string.remit_register_text_step_ars_fail), Toast.LENGTH_SHORT).show();
					}
				}
			}

			@Override
			public void onFailure(Call<ResponseRemitARSCheck> call, Throwable t) {
				DialogUtils.dismissLoading();
				ElseUtils.network_error(getActivity());
			}
		});
	}

	private String getMyPhoneNember(){
		String phoneNum ="";
		TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
		if(telManager.getLine1Number() != null){
			phoneNum = telManager.getLine1Number().replaceAll("-", "");
		}
		else {
			phoneNum = "01055666734";
		}
		return phoneNum;
	}



	private void Dialog_Notice(){
		DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
				}
			}
		}, new InfoDataDialog(R.layout.dialog_inc_case_8, null, getResources().getString(R.string.dialog_broccoli_init_button)));
	}

	private void startTimer(){
		long tenMin = 60 * 10 * 1000;

		countDownTimer = new CountDownTimer(tenMin, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				long seconds = millisUntilFinished / 1000;
				mFallBackTime.setText(getString(R.string.remit_register_text_step_ars_time_limit) + String.format("%02d", seconds / 60)
						+ getString(R.string.remit_register_text_step_ars_min) + String.format("%02d", seconds % 60)
						+ getString(R.string.remit_register_text_step_ars_second));
			}

			@Override
			public void onFinish() {
				ARS_User_Info();
			}
		}.start();
	}
	
}
