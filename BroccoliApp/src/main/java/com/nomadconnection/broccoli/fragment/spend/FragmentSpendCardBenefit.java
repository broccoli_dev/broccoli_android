package com.nomadconnection.broccoli.fragment.spend;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.adapter.spend.AdtSpendCardBenefitRow;
import com.nomadconnection.broccoli.adapter.spend.RecycleAdtSpendCardBenefit;
import com.nomadconnection.broccoli.constant.BenefitInternalType;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.MinMax;
import com.nomadconnection.broccoli.data.Spend.SpendCardBenefitDetailRowData;
import com.nomadconnection.broccoli.data.Spend.SpendCardBenefitInternalBody;
import com.nomadconnection.broccoli.data.Spend.SpendCardDetailBenefit;
import com.nomadconnection.broccoli.data.Spend.SpendCardDetailBenefitAffiliate;
import com.nomadconnection.broccoli.data.Spend.SpendCardDetailBenefitBody;
import com.nomadconnection.broccoli.data.Spend.SpendCardDetailBenefitInternalTotal;
import com.nomadconnection.broccoli.fragment.BaseFragment;
import com.nomadconnection.broccoli.interf.InterfaceFragmentInteraction;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import static com.nomadconnection.broccoli.constant.BenefitInternalType.RATE;
import static com.nomadconnection.broccoli.constant.MinMax.LESS;


/**
 * Created by YelloHyunminJang on 2017. 3. 27..
 */

public class FragmentSpendCardBenefit extends BaseFragment {

    private RecyclerView mRecyclerView;
    private RecycleAdtSpendCardBenefit mIconAdapter;
    private ListView mListView;
    private AdtSpendCardBenefitRow mListAdapter;

    private SpendCardDetailBenefitInternalTotal mData;
    private TreeMap<Integer, String> mHorizontalListData = new TreeMap<>();
    private TreeMap<Integer, HashMap<String, SpendCardBenefitDetailRowData>> mBenefitListData = new TreeMap<>();
    private int mFirstCategoryIndex = 0;
    private InterfaceFragmentInteraction mInterfaceInteraction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_spend_card_benefit, null);
        init(view);
        return view;
    }

    private void init(View view) {
        initWidget(view);
        initData();
    }

    private void initWidget(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_spend_card_benefit_horizontal_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mIconAdapter = new RecycleAdtSpendCardBenefit(getActivity(), new View.OnClickListener() {

            private View selectedView;

            @Override
            public void onClick(View v) {
                v.setSelected(true);
                if (selectedView != null && selectedView != v) {
                    selectedView.setSelected(false);
                }
                selectedView = v;
                mFirstCategoryIndex = (Integer) v.getTag();
                mIconAdapter.setSelectIndex(mFirstCategoryIndex);
                mListAdapter.setFirstCategoryId(mFirstCategoryIndex);
                mListAdapter.notifyDataSetChanged();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mListView.getLayoutParams();
                if (params != null) {
//                    Object[] array = mBenefitListData.keySet().toArray();
//                    HashMap<String, SpendCardBenefitDetailRowData> map = mBenefitListData.get(array[mFirstCategoryIndex]);
//                    params.height = (int) (map.size() * TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics()));
                    int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
                    int totalHeight = 0;
                    View view = null;
                    if (mListAdapter.getCount() <= 15) {
                        for (int i = 0; i < mListAdapter.getCount(); i++) {
                            view = mListAdapter.getView(i, view, mListView);
                            if (i == 0)
                                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, RelativeLayout.LayoutParams.WRAP_CONTENT));

                            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                            totalHeight += view.getMeasuredHeight();
                        }
                        params.height = totalHeight + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1))+30;
                        mListView.setLayoutParams(params);
                    } else {
                        for (int i = 0; i < 15; i++) {
                            view = mListAdapter.getView(i, view, mListView);
                            if (i == 0)
                                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, RelativeLayout.LayoutParams.WRAP_CONTENT));

                            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                            totalHeight += view.getMeasuredHeight();
                        }
                        params.height = totalHeight + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1))+30;
                        mListView.setLayoutParams(params);
                    }
                }
            }
        });
        mRecyclerView.setAdapter(mIconAdapter);
        mRecyclerView.setLayoutManager(layoutManager);

        mListView = (ListView) view.findViewById(R.id.lv_spend_card_benefit_list);
        mListAdapter = new AdtSpendCardBenefitRow(getActivity());
        mListView.setAdapter(mListAdapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mListView.setNestedScrollingEnabled(true);
        }
    }

    private void initData() {
        Bundle bundle = getArguments();
        mData = (SpendCardDetailBenefitInternalTotal) bundle.getSerializable(Const.DATA);
        createBenefitData(mData.cardBenefitAffiliate, mData.cardBenefitList);
    }

    private void createBenefitData(ArrayList<SpendCardDetailBenefitAffiliate> affiliates, ArrayList<SpendCardDetailBenefit> list) {
        mHorizontalListData.clear();
        mBenefitListData.clear();

        TreeMap<Integer, HashMap<Integer, ArrayList<SpendCardDetailBenefit>>> sortMap = new TreeMap<>();
        if (list != null) {
            for (SpendCardDetailBenefit data : list) {
                HashMap<Integer, ArrayList<SpendCardDetailBenefit>> categoryMap = sortMap.get(data.firstCategoryId);
                if (categoryMap == null) {
                    mHorizontalListData.put(data.firstCategoryId, data.firstCategoryName);
                    categoryMap = new HashMap<>();
                    sortMap.put(data.firstCategoryId, categoryMap);
                }
                ArrayList<SpendCardDetailBenefit> categoryList = categoryMap.get(data.secondCategoryId);
                if (categoryList == null) {
                    categoryList = new ArrayList<>();
                    categoryMap.put(data.secondCategoryId, categoryList);
                }
                categoryList.add(data);
            }
        }

        String defContent = "";

        boolean isDefaultBenefitExist = false;
        if (affiliates != null && affiliates.size() > 0) {
            isDefaultBenefitExist = true;
            mHorizontalListData.put(0, "가맹점");
            HashMap<String, SpendCardBenefitDetailRowData> defaultBenefit = new HashMap<>();
            mBenefitListData.put(0, defaultBenefit);
            for (int i=0; i < affiliates.size(); i++) {
                SpendCardDetailBenefitAffiliate affiliate = affiliates.get(i);
                SpendCardBenefitDetailRowData benefitRowData = defaultBenefit.get(String.valueOf(affiliate.countriesType));
                if (benefitRowData == null) {
                    benefitRowData = new SpendCardBenefitDetailRowData();
                    switch (affiliate.countriesType) {
                        case 1: //internal
                            benefitRowData.benefitName = "국내가맹점";
                            benefitRowData.benefitId = "1";
                            break;
                        case 2: //foreign
                            benefitRowData.benefitName = "해외가맹점";
                            benefitRowData.benefitId = "2";
                            break;
                    }
                }

                if (!defaultBenefit.containsKey(benefitRowData.benefitId)) {
                    defaultBenefit.put(benefitRowData.benefitId, benefitRowData);
                }

                if (!benefitRowData.benefitTypeMap.containsKey(affiliate.benefitType)) {
                    HashMap<BenefitInternalType, SpendCardBenefitInternalBody> row = new HashMap<>();
                    benefitRowData.benefitTypeMap.put(affiliate.benefitType, row);
                }
                HashMap<BenefitInternalType, SpendCardBenefitInternalBody> bodyMap = benefitRowData.benefitTypeMap.get(affiliate.benefitType);

                float rate = affiliate.benefit.benefitRate == null ? 0:affiliate.benefit.benefitRate;
                float amount = affiliate.benefit.benefitAmount == null ? 0:affiliate.benefit.benefitAmount;
                int amountUnit = affiliate.benefit.benefitAmountUnit == null ? 0:affiliate.benefit.benefitAmountUnit;
                int limit = affiliate.benefit.benefitLimit == null ? 0:affiliate.benefit.benefitLimit;

                BenefitInternalType currentBenefitType = RATE;

                float currentAmountUnitRate = 0;
                if (rate == 0 && amountUnit != 0) {
                    currentBenefitType = BenefitInternalType.UNIT;
                    currentAmountUnitRate = amount / amountUnit;
                } else if (amount != 0 && amountUnit == 0 && rate == 0) {
                    currentBenefitType = BenefitInternalType.AMOUNT;
                }

                SpendCardBenefitInternalBody rowBody = bodyMap.get(currentBenefitType);
                if (rowBody == null) {
                    rowBody = new SpendCardBenefitInternalBody();
                    rowBody.type = currentBenefitType;
                    rowBody.affiliateYn = 1;
                    rowBody.isDefaultBenefitExist = true;
                    bodyMap.put(currentBenefitType, rowBody);
                }

                if (currentBenefitType == RATE) {
                    if (rate < 1f) {
                        rate = rate * 100;
                    }
                }

                float bodyAmountUnitRate = 0;
                if (rowBody.benefitAmountUnit != 0) {
                    bodyAmountUnitRate = rowBody.benefitAmount / rowBody.benefitAmountUnit;
                }

                if (currentBenefitType == BenefitInternalType.UNIT) {
                    rowBody.benefitAmount = bodyAmountUnitRate < currentAmountUnitRate ? amount:rowBody.benefitAmount;
                    rowBody.benefitAmountUnit = bodyAmountUnitRate < currentAmountUnitRate ? amountUnit:rowBody.benefitAmountUnit;
                    rowBody.benefitLimit = rowBody.benefitLimit < limit ? limit:rowBody.benefitLimit;
                    rowBody.type = BenefitInternalType.UNIT;
                } else {
                    rowBody.benefitAmount = rowBody.benefitAmount < amount ? amount:rowBody.benefitAmount;
                    rowBody.benefitRate = rowBody.benefitRate < rate ? rate:rowBody.benefitRate;
                    rowBody.benefitLimit = rowBody.benefitLimit < limit ? limit:rowBody.benefitLimit;
                    rowBody.type = currentBenefitType;
                }

                if (affiliate.countriesType == 1) {
                    BenefitInternalType temp = currentBenefitType;
                    String benefit = "";
                    String content = "";

                    switch (affiliate.benefitType) {
                        case 1:
                            switch (temp) {
                                case RATE:
                                    benefit = "할인";
                                    break;
                                case UNIT:
                                    benefit = "원 할인";
                                    break;
                                case AMOUNT:
                                    benefit = "원 할인";
                                    break;
                            }
                            break;
                        case 2:
                            switch (temp) {
                                case RATE:
                                    benefit = "할인";
                                    break;
                                case UNIT:
                                    benefit = "원 할인";
                                    break;
                                case AMOUNT:
                                    benefit = "원 할인";
                                    break;
                            }
                            break;
                        case 3:
                            benefit = "마일 적립";
                            break;
                    }

                    switch (temp) {
                        case RATE:
                            String rowRate = String.format("%.2f", rowBody.benefitRate);
                            String split[] = rowRate.split("\\.");
                            if (split.length > 1) {
                                float value = Float.valueOf(split[1]);
                                if (value == 0f) {
                                    rowRate = split[0];
                                }
                            }
                            content = "기본 " + rowRate+"% "+benefit;
                            break;
                        case UNIT:
                            content = "기본 " + ElseUtils.getDecimalFormat(rowBody.benefitAmountUnit) +"원당 " + ElseUtils.getDecimalFormat((long)((float)rowBody.benefitAmount)) + benefit;
                            break;
                        case AMOUNT:
                            content = "기본 " + ElseUtils.getDecimalFormat((long)((float)rowBody.benefitAmount)) + benefit;
                            break;
                    }

                    defContent = content;
                }
            }
        }

        mFirstCategoryIndex = 0;
        Iterator<Integer> iterator = sortMap.keySet().iterator();
        while (iterator.hasNext()) {
            int firstCategoryId = iterator.next();
            HashMap<String, SpendCardBenefitDetailRowData> benefitMap = new HashMap<>();
            mBenefitListData.put(firstCategoryId, benefitMap);
            HashMap<Integer, ArrayList<SpendCardDetailBenefit>> secondCategoryList = sortMap.get(firstCategoryId);
            Iterator<Integer> secondCategoryId = secondCategoryList.keySet().iterator();
            while (secondCategoryId.hasNext()) {
                int secondId = secondCategoryId.next();
                ArrayList<SpendCardDetailBenefit> data = secondCategoryList.get(secondId);
                for (SpendCardDetailBenefit detailBenefit : data) {
                    boolean isSecond = false;
                    String benefitId = String.valueOf(detailBenefit.secondCategoryId)+"/"+String.valueOf(detailBenefit.thirdCategoryId);
                    String benefitName = detailBenefit.thirdCategoryName;
                    if (detailBenefit.thirdCategoryId == 0) {
                        isSecond = true;
                        benefitName = detailBenefit.secondCategoryName;
                    } else if (detailBenefit.thirdCategoryName == null || TextUtils.isEmpty(detailBenefit.thirdCategoryName)) {
                        isSecond = true;
                        benefitName = detailBenefit.secondCategoryName;
                    }
                    SpendCardBenefitDetailRowData benefitRowData = benefitMap.get(benefitId);
                    if (benefitRowData == null) {
                        benefitRowData = new SpendCardBenefitDetailRowData();
                        benefitMap.put(benefitId, benefitRowData);
                    }

                    benefitRowData.benefitName = benefitName;
                    benefitRowData.benefitId = benefitId;
                    benefitRowData.isSecondCategory = isSecond;

                    if (!benefitRowData.benefitTypeMap.containsKey(detailBenefit.benefitType)) {
                        HashMap<BenefitInternalType, SpendCardBenefitInternalBody> row = new HashMap<>();
                        benefitRowData.benefitTypeMap.put(detailBenefit.benefitType, row);
                    }
                    HashMap<BenefitInternalType, SpendCardBenefitInternalBody> targetMap = benefitRowData.benefitTypeMap.get(detailBenefit.benefitType);

                    float amount = detailBenefit.benefit.benefitAmount == null ? 0:detailBenefit.benefit.benefitAmount;
                    float rate = detailBenefit.benefit.benefitRate == null ? 0:detailBenefit.benefit.benefitRate;
                    int amountUnit = detailBenefit.benefit.benefitAmountUnit == null ? 0:detailBenefit.benefit.benefitAmountUnit;
                    int limit = detailBenefit.benefit.benefitLimit == null ? 0:detailBenefit.benefit.benefitLimit;

                    BenefitInternalType currentBenefitType = RATE;

                    float currentAmountUnitRate = 0;
                    if (rate == 0 && amountUnit != 0) {
                        currentBenefitType = BenefitInternalType.UNIT;
                        currentAmountUnitRate = amount / amountUnit;
                    } else if (amount != 0 && amountUnit == 0 && rate == 0) {
                        currentBenefitType = BenefitInternalType.AMOUNT;
                    }

                    SpendCardBenefitInternalBody rowBody = targetMap.get(currentBenefitType);
                    if (rowBody == null) {
                        rowBody = new SpendCardBenefitInternalBody();
                        rowBody.type = currentBenefitType;
                        rowBody.affiliateYn = detailBenefit.affiliateYn;
                        rowBody.isDefaultBenefitExist = isDefaultBenefitExist;
                        targetMap.put(currentBenefitType, rowBody);
                    } else {
                        rowBody.isMultipleData = true;
                    }

                    if (currentBenefitType == RATE) {
                        if (rate < 1f) {
                            rate = rate * 100;
                        }
                    }

                    float bodyAmountUnitRate = 0;
                    if (rowBody.benefitAmountUnit != 0) {
                        bodyAmountUnitRate = rowBody.benefitAmount / rowBody.benefitAmountUnit;
                    }

                    switch (rowBody.type) {
                        case NONE:
                            break;
                        case RATE:
                            rowBody.benefitAmount = rowBody.benefitAmount < amount ? amount:rowBody.benefitAmount;
                            rowBody.benefitRate = rowBody.benefitRate < rate ? rate:rowBody.benefitRate;
                            rowBody.benefitLimit = rowBody.benefitLimit < limit ? limit:rowBody.benefitLimit;
                            rowBody.type = RATE;
                            break;
                        case UNIT:
                            rowBody.benefitAmount = bodyAmountUnitRate < currentAmountUnitRate ? amount:rowBody.benefitAmount;
                            rowBody.benefitAmountUnit = bodyAmountUnitRate < currentAmountUnitRate ? amountUnit:rowBody.benefitAmountUnit;
                            rowBody.benefitLimit = rowBody.benefitLimit < limit ? limit:rowBody.benefitLimit;
                            rowBody.type = BenefitInternalType.UNIT;
                            break;
                        case AMOUNT:
                            rowBody.benefitAmount = rowBody.benefitAmount < amount ? amount:rowBody.benefitAmount;
                            rowBody.benefitRate = rowBody.benefitRate < rate ? rate:rowBody.benefitRate;
                            rowBody.benefitLimit = rowBody.benefitLimit < limit ? limit:rowBody.benefitLimit;
                            rowBody.type = BenefitInternalType.AMOUNT;
                            break;
                    }

                    rowBody.defaultBenefitContent = defContent;
                }
            }
        }
        mIconAdapter.setData(mHorizontalListData);
        mIconAdapter.notifyDataSetChanged();
        if (mBenefitListData.size() > 0) {
            mListAdapter.setData(mFirstCategoryIndex, mBenefitListData);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mRecyclerView.findViewHolderForAdapterPosition(0).itemView.performClick();
                }
            }, 100);
        }
    }

    private MaxValue getCurrentMaxValue(SpendCardDetailBenefit benefit) {
        MaxValue requirementValue = new MaxValue();
        requirementValue.isRequirement = true;
        MaxValue spendValue = new MaxValue();

        if (benefit.requirement != null) {
            switch (benefit.requirement.maxSignType) {
                case NONE:
                    requirementValue.isMinValue = true;
                    switch (benefit.requirement.minSignType) {
                        case NONE:
                            requirementValue.value = 0;
                            requirementValue.signType = MinMax.NONE;
                            break;
                        case LESS:
                            requirementValue.value = benefit.requirement.min;
                            requirementValue.signType = LESS;
                            break;
                        case LESS_EQUAL:
                        case EQUAL:
                            requirementValue.value = benefit.requirement.min;
                            requirementValue.signType = MinMax.EQUAL;
                            break;
                        case GREATER:
                        case GREATER_EQUAL:
                            requirementValue.value = benefit.requirement.min;
                            requirementValue.signType = MinMax.GREATER;
                            break;
                    }
                    break;
                case LESS:
                    requirementValue.value = benefit.requirement.max;
                    requirementValue.signType = LESS;
                    break;
                case LESS_EQUAL:
                case EQUAL:
                    requirementValue.value = benefit.requirement.max;
                    requirementValue.signType = MinMax.EQUAL;
                    break;
                case GREATER:
                case GREATER_EQUAL:
                    requirementValue.value = benefit.requirement.max;
                    requirementValue.signType = MinMax.GREATER;
                    break;
            }
        }

        if (benefit.spend != null) {
            switch (benefit.spend.maxSignType) {
                case NONE:
                    spendValue.isMinValue = true;
                    switch (benefit.spend.minSignType) {
                        case NONE:
                            spendValue.value = 0;
                            spendValue.signType = MinMax.NONE;
                            break;
                        case LESS:
                            spendValue.value = benefit.spend.min;
                            spendValue.signType = LESS;
                            break;
                        case LESS_EQUAL:
                        case EQUAL:
                            spendValue.value = benefit.spend.min;
                            spendValue.signType = MinMax.EQUAL;
                            break;
                        case GREATER:
                        case GREATER_EQUAL:
                            spendValue.value = benefit.spend.min;
                            spendValue.signType = MinMax.GREATER;
                            break;
                    }
                    break;
                case LESS:
                    spendValue.value = benefit.spend.max;
                    spendValue.signType = LESS;
                    break;
                case LESS_EQUAL:
                case EQUAL:
                    spendValue.value = benefit.spend.max;
                    spendValue.signType = MinMax.EQUAL;
                    break;
                case GREATER:
                case GREATER_EQUAL:
                    spendValue.value = benefit.spend.max;
                    spendValue.signType = MinMax.GREATER;
                    break;
            }
        }

        return requirementValue;
    }

    private MaxValue getBiggerMaxValue(MaxValue requirement, MaxValue spend) {
        MaxValue maxValue = null;

        if (requirement.value > spend.value) {
            maxValue = requirement;
        } else if (requirement.value < spend.value) {
            maxValue = spend;
        } else {
            switch (requirement.signType) {
                case NONE:
                    maxValue = spend;
                    break;
                case LESS:
                    if (spend.signType.ordinal() >= requirement.signType.ordinal()) {
                        maxValue = spend;
                    } else {
                        maxValue = requirement;
                    }
                    break;
                case LESS_EQUAL:
                case EQUAL:
                    if (spend.signType.ordinal() >= requirement.signType.ordinal()) {
                        maxValue = spend;
                    } else {
                        maxValue = requirement;
                    }
                    break;
                case GREATER:
                case GREATER_EQUAL:
                    break;
            }
        }

        return maxValue;
    }

    private class MaxValue {
        boolean isRequirement = false;
        boolean isMinValue = false;
        long value = 0;
        MinMax signType = MinMax.NONE;
        SpendCardDetailBenefitBody body;
    }

    private class BenefitListData {

    }


}
