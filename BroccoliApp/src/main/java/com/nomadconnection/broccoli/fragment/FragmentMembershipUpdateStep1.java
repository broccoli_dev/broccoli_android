package com.nomadconnection.broccoli.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.constant.AnalyticsName;

/**
 * Created by YelloHyunminJang on 2016. 12. 13..
 */

public class FragmentMembershipUpdateStep1 extends BaseFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_membership_update_guide, null);
        ((BaseActivity)getActivity()).sendTracker(AnalyticsName.ExUserInfo);
        init(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init(View view) {
        View btn = view.findViewById(R.id.btn_confirm);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseFragment fragment = new FragmentMembershipUpdateStep2();
                nextStep(fragment);
            }
        });
    }

    private void nextStep(BaseFragment fragment) {
        try {
            setCurrentFragment(R.id.ll_activity_membership_update_fragment_area, fragment, fragment.getTagName());
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
