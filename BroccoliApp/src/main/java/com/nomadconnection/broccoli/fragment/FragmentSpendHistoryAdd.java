package com.nomadconnection.broccoli.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.data.InfoDataAssetCredit;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.EditSpendMonthly;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.interf.InterfaceSpendCallBack;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.TransFormatUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSpendHistoryAdd extends BaseFragment implements View.OnClickListener {

	private static String TAG = "FragmentSpendHistoryAdd";
	InterfaceEditOrDetail mInterFaceEditOrDetail;
	InterfaceSpendCallBack mInterfaceSpendCallBack;
	/* Layout */
	private EditText mSpendSum;
	private EditText mSpendBreakdown;
	private TextView mSpendDateAndTime;
	private TextView mSpendWayText;
	private TextView mSpendWayValue;
	private TextView mSpendCategory;
	private LinearLayout mSpendDateAndTimeSpinner;
	private RelativeLayout mSpendCategoryIconBG;
	private RelativeLayout mSpendCategoryIconRL;
	private ImageView mSpendCategoryIcon;
	private EditText mSpendMemo;
	private int mSpendCategorySelect = -1;
	private String mTextWatcherResult = "";
	private ArrayList<InfoDataAssetCredit> mAlInfoDataList;
	private String mPeriod = null;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mInterFaceEditOrDetail = (InterfaceEditOrDetail) context;
			mInterfaceSpendCallBack = (InterfaceSpendCallBack) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement InterfaceEditOrDetail");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_spend_history_add, null);
		((BaseActivity)getActivity()).sendTracker(AnalyticsName.FragmentSpendHistoryAdd);
		if(parseParam()){
			if(initWidget(v)) {
				if(!initData()){
					getActivity().finish();
				}
			}
			else{
				getActivity().finish();
			}
		}
		else{
			getActivity().finish();
		}
		
		return v;
	}
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.ll_spend_history_inc_date_spinner_layout:
				Calendar calendar = Calendar.getInstance();
				String time = mSpendDateAndTime.getText().toString();
				SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd  a hh:mm");
				Date targetDate = null;
				try {
					targetDate = format.parse(time);
					calendar.setTime(targetDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				final String[] tempPickerDate = new String[]{time.substring(0, 10), time.substring(10)};
				final Dialog dialog = new Dialog(getActivity());
				dialog.setContentView(R.layout.dialog_time_picker);
				final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
				datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
					@Override
					public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						String date = year+"."+ TransFormatUtils.transZeroPadding2(Integer.toString(monthOfYear+1))+"."+ TransFormatUtils.transZeroPadding2(Integer.toString(dayOfMonth));
						tempPickerDate[0] = date;
					}
				});
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
					datePicker.setMaxDate(calendar.getTimeInMillis()+60*1000*60);
				} else {
					datePicker.setMaxDate(calendar.getTimeInMillis()+60*1000*60);
				}
				calendar.add(Calendar.MONTH, -6);
				datePicker.setMinDate(calendar.getTimeInMillis());
				final TimePicker timePicker = (TimePicker) dialog.findViewById(R.id.timePicker);
				timePicker.setIs24HourView(true);
				timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
					@Override
					public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
						String hour = TransFormatUtils.transZeroPadding2(String.valueOf(hourOfDay))+TransFormatUtils.transZeroPadding2(String.valueOf(minute));
						hour = ElseUtils.getHourMinute(hour);
						tempPickerDate[1] = hour;
					}
				});
				dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (datePicker.getVisibility() == View.GONE) {
							datePicker.setVisibility(View.VISIBLE);
							timePicker.setVisibility(View.GONE);
							dialog.findViewById(R.id.btn_next).setVisibility(View.VISIBLE);
							dialog.findViewById(R.id.btn_confirm).setVisibility(View.GONE);
						} else {
							dialog.dismiss();
						}
					}
				});
				dialog.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mSpendDateAndTime.setText(tempPickerDate[0]+tempPickerDate[1]);
						send_interface_complete();
						dialog.dismiss();
					}
				});
				dialog.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (datePicker.getVisibility() == View.VISIBLE) {
							datePicker.setVisibility(View.GONE);
							timePicker.setVisibility(View.VISIBLE);
							v.setVisibility(View.GONE);
							dialog.findViewById(R.id.btn_confirm).setVisibility(View.VISIBLE);
						}
					}
				});
				dialog.show();
//				DatePickerDialog datePickerDialog = new DatePickerDialog(this.getActivity(), AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
//					@Override
//					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//						mSpendDateAndTime.setText(year+"."+ TransFormatUtils.transZeroPadding2(Integer.toString(monthOfYear+1))+"."+ TransFormatUtils.transZeroPadding2(Integer.toString(dayOfMonth)));
//						send_interface_complete();
//					}
//				}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//				calendar.add(Calendar.MONTH, -6);
//				datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
//				datePickerDialog.show();
				break;

			case R.id.rl_spend_history_inc_detail_category_select_bg:
				String mStr4 = getString(R.string.spend_category_select);
				DialogUtils.showDlgBaseGridOrList(getActivity(), new AdapterView.OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
								if (position != -1) {
									if (id == R.id.btn_dialog_base_always) {
										mPeriod = EditSpendMonthly.ALWAYS;
									} else {
										mPeriod = EditSpendMonthly.ONETIME;
									}
									final TypedArray SpendCategoryArrayIcon = getResources().obtainTypedArray(R.array.spend_category_list_img);
									String[] SpendCategoryArray = getResources().getStringArray(R.array.spend_category);
									mSpendCategorySelect = position;
									mSpendCategoryIconRL.setVisibility(View.VISIBLE);
									mSpendCategory.setText(SpendCategoryArray[position + 1]);
									mSpendCategoryIcon.setImageResource(SpendCategoryArrayIcon.getResourceId(position + 1, -1));
									send_interface_complete();
								}
							}
						},
						new InfoDataDialog(APP.DIALOG_BASE_GRID_OR_LIST_G, mStr4, getString(R.string.common_cancel)), mSpendCategorySelect);
				break;
		}

	}

	//complete click
	public void onComplete(){
		if(mSpendBreakdown.length() != 0 && mSpendCategorySelect != -1 && mSpendBreakdown.length() != 0){
			spend_history_add();
		}
		else {
			Toast.makeText(getActivity(), getResources().getString(R.string.common_empty), Toast.LENGTH_SHORT).show();
		}
	}
	



//=========================================================================================//
// onCreate Private method - parseParam()
//=========================================================================================//
	private boolean parseParam(){
		boolean bResult = true;
		try {
		}
		catch (Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "parseParam() : " + e.toString());
			return bResult;
		}
		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initWidget()
//=========================================================================================//
	private boolean initWidget(View _v) {
		boolean bResult = true;
		try
		{
			/* Title */

			/* Layout */
			mSpendSum = (EditText) _v.findViewById(R.id.et_spend_history_inc_detail_sum_value);
			mSpendSum.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					if(!s.toString().equals(mTextWatcherResult)){
						if("".equals(s.toString())){
							mSpendSum.setHint(getString(R.string.common_input_sum_hint));
						}
						else {
							try {
								// The comma in the format specifier does the trick
								mTextWatcherResult = String.format("%,d", Long.parseLong(s.toString().replaceAll(",", "")));
//								Log.d("broccoli", "afterTextChanged() : " + mTextWatcherResult);
							}
							catch (NumberFormatException e) {
								Log.e("broccoli", "afterTextChanged() : " + e.toString());
							}
							mSpendSum.setText(mTextWatcherResult);
							mSpendSum.setSelection(mTextWatcherResult.length());
						}
						send_interface_complete();
					}
				}
			});
			mSpendBreakdown = (EditText) _v.findViewById(R.id.et_spend_history_inc_detail_breakdown);
			mSpendBreakdown.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					send_interface_complete();
				}
			});
			mSpendDateAndTimeSpinner = (LinearLayout)_v.findViewById(R.id.ll_spend_history_inc_date_spinner_layout);
			mSpendDateAndTimeSpinner.setPadding(215, 0, 0, 0);
			mSpendDateAndTimeSpinner.setOnClickListener(this);
			mSpendDateAndTime = (TextView) _v.findViewById(R.id.tv_spend_history_inc_date_spinner_text);

			mSpendWayText = (TextView) _v.findViewById(R.id.tv_spend_history_inc_detail_spend_way_text);
			mSpendWayValue = (TextView) _v.findViewById(R.id.tv_spend_history_inc_detail_spend_way_value);

			mSpendCategory = (TextView) _v.findViewById(R.id.tv_spend_history_inc_detail_category_select);
			mSpendCategoryIconRL = (RelativeLayout) _v.findViewById(R.id.rl_spend_history_inc_detail_icon);
			mSpendCategoryIconBG = (RelativeLayout) _v.findViewById(R.id.rl_spend_history_inc_detail_category_select_bg);
			mSpendCategoryIconBG.setOnClickListener(this);
			mSpendCategoryIcon = (ImageView) _v.findViewById(R.id.imv_spend_history_inc_detail_icon);

			mSpendMemo = (EditText) _v.findViewById(R.id.ed_spend_history_inc_detail_memo);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initWidget() : " + e.toString());
			return bResult;
		}

		return bResult;
	}



//=========================================================================================//
// onCreate Private method - initData()
//=========================================================================================//
	private boolean initData() {
		boolean bResult = true;
		try
		{
			/* Default Value */
			long now = System.currentTimeMillis();
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyy.MM.dd  a hh:mm");
			String[] SpendPayArray = getResources().getStringArray(R.array.spend_pay_type);

			mSpendSum.setHint(getString(R.string.common_input_sum_hint));
			mSpendBreakdown.setHint(getString(R.string.common_content_input));
			mSpendDateAndTime.setText(CurDateFormat.format(calendar.getTime()));
			mSpendWayText.setTextColor(getResources().getColor(R.color.subitem_left_txt_dim_color));
			mSpendWayValue.setTextColor(getResources().getColor(R.color.subitem_left_txt_dim_color));
			mSpendWayValue.setText(SpendPayArray[3]);
			mSpendCategoryIconRL.setVisibility(View.GONE);
		}
		catch(Exception e) {
			bResult = false;
			if(APP._DEBUG_MODE_)
				Log.e("broccoli", "initData() : " + e.toString());
			return bResult;
		}

		return bResult;
	}




//=========================================================================================//
// Spend History Add API
//=========================================================================================//
	private void spend_history_add() {
		DialogUtils.showLoading(getActivity());	 //소비 로딩 추가
		DecimalFormat decimalFormat = new DecimalFormat("00");
		EditSpendMonthly editSpendMonthly = new EditSpendMonthly();
		long now = System.currentTimeMillis();
		Date date = new Date(now);
		SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMddHHmm", Locale.KOREA);
		SimpleDateFormat checkDate = new  SimpleDateFormat("yyyy.MM.dd", Locale.KOREA);
		int category = mSpendCategorySelect + 1;

		editSpendMonthly.setUserId(Session.getInstance(getActivity()).getUserId());
		editSpendMonthly.setExpenseId("0");
		if(checkDate.format(date).equals(mSpendDateAndTime.getText().toString())){
			editSpendMonthly.setExpenseDate(dateFormat.format(date));
		}
		else {
			editSpendMonthly.setExpenseDate(ElseUtils.getServerDate(mSpendDateAndTime.getText().toString()));
		}

		editSpendMonthly.setOpponent(mSpendBreakdown.getText().toString());

		if(category == 18) {
			editSpendMonthly.setCategoryId("25");
		} else {
			editSpendMonthly.setCategoryId(decimalFormat.format(category));
		}

		editSpendMonthly.setPayType("03");
		editSpendMonthly.setEditYn("Y");
		editSpendMonthly.setSum(mSpendSum.getText().toString().replaceAll(",", ""));
		editSpendMonthly.setMemo(mSpendMemo.getText().toString().trim());
		editSpendMonthly.setPeriodType(mPeriod);

		SpendApi api = ServiceGenerator.createService(SpendApi.class);
		Call<Result> call = api.editSpend(editSpendMonthly);
		call.enqueue(new Callback<Result>() {
			@Override
			public void onResponse(Call<Result> call, Response<Result> response) {
				DialogUtils.dismissLoading();	//소비 로딩 추가
				mInterfaceSpendCallBack.onCallBackComplete();
			}

			@Override
			public void onFailure(Call<Result> call, Throwable t) {
				DialogUtils.dismissLoading();	//소비 로딩 추가
				ElseUtils.network_error(getActivity());

			}
		});
		mSpendMemo.clearFocus();
	}



	private void send_interface_complete(){
		if(empty_check()){
			mInterFaceEditOrDetail.onEdit();
		}
		else {
			mInterFaceEditOrDetail.onError();
		}
	}

	private boolean empty_check(){
		if(mSpendSum.length() == 0 || "0".equals(mSpendSum.getText().toString())|| mSpendCategorySelect == -1 || mSpendBreakdown.length() == 0 || mPeriod == null){
			return false;
		}
		return true;
	}

}
