package com.nomadconnection.broccoli.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.MainActivity;
import com.nomadconnection.broccoli.activity.Splash;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.utils.DialogUtils;

public class BaseFragment extends Fragment {

	protected FragmentManager mFm;
	
	private String mTagName = this.getClass().getSimpleName();

	public interface ListFragmentClickListener {
		public void OnClickedItem(int position, Object dataset);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mFm = getFragmentManager();
		Session.getInstance(getActivity());
	}
	
	private void CommitFragment(FragmentTransaction ft, int containView, BaseFragment foreFragment, String tag) {
		ft.addToBackStack(tag);
		ft.replace(containView, foreFragment);
		ft.hide(this);
		ft.commitAllowingStateLoss();
	}
	public void setCurrentFragment(int containerView, BaseFragment foreFragment, String tag) {
		FragmentTransaction ft = mFm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
		CommitFragment(ft, containerView, foreFragment, tag);
	}

	public void setClearCurrentFragment(int containerView, BaseFragment foreFragment, String tag) {
		mFm.popBackStack();
		FragmentTransaction ft = mFm.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
		CommitFragment(ft, containerView, foreFragment, tag);
	}
	
	public void setCurrentFragmentFade(int containerView, BaseFragment foreFragment, String tag) {
		FragmentTransaction ft = mFm.beginTransaction();
		ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
		CommitFragment(ft, containerView, foreFragment, tag);
	}
	
	public void setTagName(String tag) {
		mTagName = tag;
	}
	
	public String getTagName() {
		return mTagName;
	}

	public void memberLeaveComplete() {
		DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == APP.DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK) {
					Session.getInstance(getActivity()).logout();

					Intent intent = new Intent(getActivity(), MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					intent.putExtra(Const.MEMBER_LEAVE, "member_leave");
					startActivity(intent);
				}
			}
		}, new InfoDataDialog(R.layout.dialog_inc_case_1, getString(R.string.membership_text_login_fail_disable_device), "확인"));
	}

	public void sessionLoseLoginAgain() {
		Toast.makeText(getActivity(), "세션이 만료되어 재접속 합니다.", Toast.LENGTH_LONG).show();
		Intent intent = new Intent(getActivity(), Splash.class);
		startActivity(intent);
		getActivity().finish();
	}
}
