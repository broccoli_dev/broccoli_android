package com.nomadconnection.broccoli.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.igaworks.adbrix.IgawAdbrix;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.base.BaseActivity;
import com.nomadconnection.broccoli.activity.membership.LoginActivity;
import com.nomadconnection.broccoli.activity.membership.MembershipActivity;
import com.nomadconnection.broccoli.activity.setting.Level4ActivitySettingCommonTerm;
import com.nomadconnection.broccoli.api.DayliApi;
import com.nomadconnection.broccoli.api.LogApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.UserApi;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.AnalyticsName;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Dayli.ReqAuthRealNameData;
import com.nomadconnection.broccoli.data.Dayli.ReqCertRealNameData;
import com.nomadconnection.broccoli.data.Dayli.ResponseAuthRealName;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfo;
import com.nomadconnection.broccoli.data.InfoDataDialog;
import com.nomadconnection.broccoli.data.Log.LogMsgData;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.LoginData;
import com.nomadconnection.broccoli.data.Session.MembershipData;
import com.nomadconnection.broccoli.database.DbManager;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.CustomToast;
import com.nomadconnection.broccoli.utils.DialogUtils;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ErrorUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nomadconnection.broccoli.data.Log.LogMsgData.LOG_REAL_NAME;

public class FragmentMembershipPersonal extends BaseFragment implements OnClickListener {

    public static final String TAG = FragmentMembershipPersonal.class.getSimpleName();

    private static final String SMS_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private static final int MAX_REQUEST_COUNT = 5;

    private ScrollView mScrollView;
    private EditText mName;
    private EditText mBirth;
    private EditText mGender;
    private LinearLayout mPhoneKind;
    private EditText mPhoneNum;
    private View mNextBtn;
    private EditText mAuthNum;
    private TextView mRequestBtn;
    private TextView mRequestAgain;
    private TextView mRemainTimerTitle;
    private TextView mRemainTimer;
    private String mCurrentAgency;
    private Bundle mArgument;
    private DayliApi mApi;
    private ReqAuthRealNameData mReqAuthRealNameData;
    private ResponseAuthRealName mAuthRealName;
    private boolean isUpdateMode = false;
    private boolean isTimeOut = false;
    private int requestCount = 0;
    private boolean isDelayed = false;
    private View mTermLayout;
    private ImageView mTermIcon;
    private boolean flagFirst, flagSecond, flagThird, flagFourth, flagAll;
    private View checkFirst, checkSecond, checkThird, checkFourth, checkAll;

    public FragmentMembershipPersonal() {
        // TODO Auto-generated constructor stub
        setTagName(TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_membership_step_third, null);
        Bundle bundle = getArguments();
        isUpdateMode = bundle.getBoolean(Const.IS_UPDATE_MODE);
        if (!isUpdateMode) {
            ((BaseActivity) getActivity()).sendTracker(AnalyticsName.UserNameCheck);
        }
        init(view);

        return view;
    }

    void init(View view) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mApi = ServiceGenerator.createServiceDayli(DayliApi.class);
        mArgument = getArguments();
        mScrollView = (ScrollView) view.findViewById(R.id.sv_fragment_membership_top_layout);
        mName = (EditText) view.findViewById(R.id.et_fragment_membership_name);
        mBirth = (EditText) view.findViewById(R.id.tv_fragment_membership_birth);
        mGender = (EditText) view.findViewById(R.id.tv_fragment_membership_gender);
        mPhoneKind = (LinearLayout) view.findViewById(R.id.ll_fragment_membership_phone_kind);
        mPhoneNum = (EditText) view.findViewById(R.id.ed_fragment_membership_phone_number);
        mRequestBtn = (TextView) view.findViewById(R.id.btn_fragment_membership_auth_number);
        mRequestAgain = (TextView) view.findViewById(R.id.btn_fragment_membership_auth_number_again);
        mRemainTimer = (TextView) view.findViewById(R.id.tv_fragment_membership_timer);
        mRemainTimerTitle = (TextView) view.findViewById(R.id.tv_fragment_membership_timer_title);
        mAuthNum = (EditText) view.findViewById(R.id.tv_fragment_membership_auth_number);
        mTermLayout = view.findViewById(R.id.ll_cert_realname_term_layout);
        mTermIcon = (ImageView) view.findViewById(R.id.iv_cert_realname_term_icon);
        mTermIcon.setOnClickListener(this);
        mTermLayout.setVisibility(View.GONE);

        view.findViewById(R.id.rl_cert_realname_term_first).setOnClickListener(this);
        view.findViewById(R.id.rl_cert_realname_term_second).setOnClickListener(this);
        view.findViewById(R.id.rl_cert_realname_term_third).setOnClickListener(this);
        view.findViewById(R.id.rl_cert_realname_term_fourth).setOnClickListener(this);

        checkAll = view.findViewById(R.id.ll_cert_realname_all);
        checkAll.setOnClickListener(this);
        checkFirst = view.findViewById(R.id.ll_cert_realname_first);
        checkFirst.setOnClickListener(this);
        checkSecond = view.findViewById(R.id.ll_cert_realname_second);
        checkSecond.setOnClickListener(this);
        checkThird = view.findViewById(R.id.ll_cert_realname_third);
        checkThird.setOnClickListener(this);
        checkFourth = view.findViewById(R.id.ll_cert_realname_fourth);
        checkFourth.setOnClickListener(this);

        if (isUpdateMode) {
            view.findViewById(R.id.ll_fragment_membership_cert_title_layout).setVisibility(View.GONE);
            mNextBtn = view.findViewById(R.id.fl_fragment_membership_next);
            mNextBtn.setVisibility(View.VISIBLE);
        } else {
            mNextBtn = view.findViewById(R.id.fl_fragment_membership_next);
            mNextBtn.setVisibility(View.VISIBLE);
            if (!APPSharedPrefer.getInstance(this.getActivity()).getPrefBool(APP.SETTING_SENDED_PUSH, false)) {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("osType", 1);
                hashMap.put("pushKey", APPSharedPrefer.getInstance(this.getActivity()).getPrefString(APP.SP_USER_GCM));
                Call<Result> call = ServiceGenerator.createService(UserApi.class).setUserPushkey(hashMap);
                call.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                            if (response.body().isOk() || response.body().checkUnable()) {
                                APPSharedPrefer.getInstance(getActivity()).setPrefBool(APP.SETTING_SENDED_PUSH, true);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {
                    }
                });
            }
        }

        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkButtonStatus();
            }
        });
        mBirth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkButtonStatus();
                if (mBirth.getText().length() == 0) {
                    mBirth.setTextSize(12);
                } else if (mBirth.getText().length() == 1) {
                    mBirth.setTextSize(15);
                }
                if (mBirth.getText().length() >= 6) {
                    mGender.requestFocus();
                }
            }
        });
        mBirth.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                int action = event.getAction();
                switch (keyCode) {
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_NAVIGATE_NEXT:
                        if (action == KeyEvent.ACTION_UP) {

                            mGender.requestFocus();
                        }
                        break;
                    case KeyEvent.KEYCODE_DEL:
                    case KeyEvent.KEYCODE_FORWARD_DEL:
                        break;
                    default:
                        if (action == KeyEvent.ACTION_DOWN) {
                            if (mBirth.getText().length() >= 6) {
                                mGender.requestFocus();
                                mGender.onKeyDown(keyCode, event);
                            }
                        } else if (action == KeyEvent.ACTION_UP) {
                            if (mBirth.getText().length() >= 6) {
                                mGender.onKeyUp(keyCode, event);
                            }
                        }
                        break;
                }
                return false;
            }
        });
        mGender.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkButtonStatus();
            }
        });
        mGender.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                int action = event.getAction();
                switch (keyCode) {
                    case KeyEvent.KEYCODE_ENTER:
                        showPhoneDropDown(mPhoneKind);
                        mPhoneNum.requestFocus();
                        break;
                    case KeyEvent.KEYCODE_DEL:
                        if (mGender.getText().length() == 0 && action == KeyEvent.ACTION_UP) {
                            mBirth.requestFocus();
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });

        mPhoneKind.setOnClickListener(this);
        mPhoneNum.setInputType(InputType.TYPE_CLASS_PHONE);

        String number = getLineNumber();
        mPhoneNum.setText(number);
        mPhoneNum.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            @Override
            public synchronized void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                checkButtonStatus();
            }
        });
        if (number != null && !number.isEmpty()) {
            String checkNum = number.replace("+82", "0");
            mPhoneNum.setText(checkNum);
        }

        mPhoneNum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            Rect rect = new Rect();
                            if (mPhoneNum.getGlobalVisibleRect(rect)) {
                                mScrollView.smoothScrollTo(0, rect.top);
                            }
                        }
                    });
                }
            }
        });

        mRequestBtn.setOnClickListener(this);
        mRequestBtn.setEnabled(false);
        mRequestAgain.setOnClickListener(this);
        mRemainTimerTitle.setVisibility(View.INVISIBLE);
        mRemainTimer.setVisibility(View.INVISIBLE);

        mAuthNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkButtonStatus();
                if (s.length() > 5) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mAuthNum.getWindowToken(), 0);
                }
            }
        });

        mNextBtn.setOnClickListener(this);
        mNextBtn.setEnabled(false);

        if (isUpdateMode) {
            mName.requestFocus();
            UserPersonalInfo info = (UserPersonalInfo) mArgument.getSerializable(Const.DATA);
            if (info != null && info.user != null) {
                mName.setText(info.user.realname);
                String birth = info.user.socialNumber.substring(0, 6);
                String gender = info.user.socialNumber.substring(6);
                mBirth.setText(birth);
                mGender.setText(gender);
            }
            mName.setEnabled(false);
            mBirth.setEnabled(false);
            mGender.setEnabled(false);
        }
        registerSmsListener();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isUpdateMode)
            ((MembershipActivity) getActivity()).setStepGuide(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterSmsListener();
        cancelTimer();
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        Intent intent = null;
        int id = view.getId();
        switch (id) {
            case R.id.fl_fragment_membership_next:
                confirm();
                break;
            case R.id.btn_fragment_membership_auth_number:
                requestAuth();
                break;
            case R.id.ll_fragment_membership_phone_kind:
                showPhoneDropDown(view);
                break;
            case R.id.btn_fragment_membership_auth_number_again:
                requestAuth();
                break;
            case R.id.ll_navi_img_txt_else:
                confirm();
                break;
            case R.id.iv_cert_realname_term_icon:
                if (mTermLayout.getVisibility() == View.VISIBLE) {
                    mTermLayout.setVisibility(View.GONE);
                    mTermIcon.setImageResource(R.drawable.list_down_ic_n);
                } else {
                    mTermLayout.setVisibility(View.VISIBLE);
                    mTermIcon.setImageResource(R.drawable.list_up_ic_n);
                }
                break;
            case R.id.rl_cert_realname_term_first:
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_cert_term_title1));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/name/%ED%86%B5%EC%8B%A0%EC%82%AC%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80.html");
                startActivity(intent);
                break;
            case R.id.rl_cert_realname_term_second:
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_cert_term_title2));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/name/%EA%B0%9C%EC%9D%B8%EC%A0%95%EB%B3%B4%EC%88%98%EC%A7%91%EC%9D%B4%EC%9A%A9%EC%B7%A8%EA%B8%89%EC%9C%84%ED%83%81%EB%8F%99%EC%9D%98.html");
                startActivity(intent);
                break;
            case R.id.rl_cert_realname_term_third:
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_cert_term_title3));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/name/%EA%B3%A0%EC%9C%A0%EC%8B%9D%EB%B3%84%EC%A0%95%EB%B3%B4%EC%B2%98%EB%A6%AC%EB%8F%99%EC%9D%98.html");
                startActivity(intent);
                break;
            case R.id.rl_cert_realname_term_fourth:
                intent = new Intent(getActivity(), Level4ActivitySettingCommonTerm.class);
                intent.putExtra(Const.TITLE, getContext().getResources().getString(R.string.membership_cert_term_title4));
                intent.putExtra(Const.CONTENT, "https://broccoli-cdn.s3.amazonaws.com/terms/name/%EB%B3%B8%EC%9D%B8%ED%99%95%EC%9D%B8%EC%84%9C%EB%B9%84%EC%8A%A4%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80.html");
                startActivity(intent);
                break;
            case R.id.ll_cert_realname_first:
                flagFirst = !flagFirst;
                if (flagFirst) {
                    checkFirst.setSelected(true);
                } else {
                    checkFirst.setSelected(false);
                }
                allCheckFlag();
                break;
            case R.id.ll_cert_realname_second:
                flagSecond = !flagSecond;
                if (flagSecond) {
                    checkSecond.setSelected(true);
                } else {
                    checkSecond.setSelected(false);
                }
                allCheckFlag();
                break;
            case R.id.ll_cert_realname_third:
                flagThird = !flagThird;
                if (flagThird) {
                    checkThird.setSelected(true);
                } else {
                    checkThird.setSelected(false);
                }
                allCheckFlag();
                break;
            case R.id.ll_cert_realname_fourth:
                flagFourth = !flagFourth;
                if (flagFourth) {
                    checkFourth.setSelected(true);
                } else {
                    checkFourth.setSelected(false);
                }
                allCheckFlag();
                break;
            case R.id.ll_cert_realname_all:
                flagAll = !flagAll;
                if (flagAll) {
                    checkAll.setSelected(true);
                } else {
                    checkAll.setSelected(false);
                }
                toggleCheck();
                break;
        }
    }

    private void allCheckFlag() {
        if (flagFirst & flagSecond & flagThird & flagFourth){
            flagAll = true;
            checkAll.setSelected(true);
        }
        else {
            flagAll = false;
            checkAll.setSelected(false);
        }
        checkButtonStatus();
    }

    private void toggleCheck() {
        if (flagFirst & flagSecond & flagThird & flagFourth) {
            flagFirst = flagSecond = flagThird = flagFourth = false;
            checkFirst.setSelected(false);
            checkSecond.setSelected(false);
            checkThird.setSelected(false);
            checkFourth.setSelected(false);
        } else {
            flagFirst = flagSecond = flagThird = flagFourth = true;
            checkFirst.setSelected(true);
            checkSecond.setSelected(true);
            checkThird.setSelected(true);
            checkFourth.setSelected(true);
        }
        checkButtonStatus();
    }

    private void checkButtonStatus() {
        boolean ret = true;
        if (mName.getText().length() <= 1) {
            ret = false;
        }
        if (mBirth.getText().length() <= 5) {
            ret = false;
        }
        if (mGender.getText().length() <= 0) {
            ret = false;
        }
        if (mPhoneNum.getText().length() <= 9) {
            ret = false;
        }
        if (!(flagFirst & flagSecond & flagThird & flagFourth)) {
            ret = false;
        }
        if (ret && !isDelayed) {
            mRequestBtn.setEnabled(true);
            mRequestAgain.setEnabled(true);
        } else {
            mRequestBtn.setEnabled(false);
            mRequestAgain.setEnabled(false);
        }
        if (mAuthNum.getText().length() <= 5) {
            ret = false;
        }
        if (ret) {
            mNextBtn.setEnabled(true);
        } else {
            mNextBtn.setEnabled(false);
        }
    }

    private Handler mHandler = new Handler();

    private void delayedBtn() {
        isDelayed = true;
        mRequestBtn.setEnabled(false);
        mRequestAgain.setEnabled(false);
        mRequestBtn.setText(R.string.membership_text_third_auth_waiting);
        mRequestAgain.setText(R.string.membership_text_third_auth_waiting);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isDelayed = false;
                mRequestBtn.setText(R.string.membership_text_third_auth_request);
                mRequestAgain.setText(R.string.membership_text_third_auth_request_again);
                if (requestCount > 0) {
                    mRequestAgain.setVisibility(View.VISIBLE);
                } else {
                    mRequestAgain.setVisibility(View.GONE);
                }
                mRequestBtn.setEnabled(true);
                mRequestAgain.setEnabled(true);
            }
        }, 10000);
    }

    private void requestAuth() {
        String name = mName.getText().toString();
        String birth = mBirth.getText().toString();
        String gender = mGender.getText().toString();
        String phoneNum = mPhoneNum.getText().toString();
        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(birth) || TextUtils.isEmpty(gender)) {
            Toast.makeText(getActivity(), "입력칸이 비어있습니다. 입력해주세요.", Toast.LENGTH_LONG).show();
            return;
        }
        if (mCurrentAgency == null || TextUtils.isEmpty(mCurrentAgency)) {
            Toast.makeText(getActivity(), "사용하고 계신 통신사를 선택해 주세요.", Toast.LENGTH_LONG).show();
            return;
        }
        if (phoneNum == null || TextUtils.isEmpty(phoneNum)) {
            Toast.makeText(getActivity(), "휴대폰 번호를 입력해주세요.", Toast.LENGTH_LONG).show();
            return;
        }
        DialogUtils.showLoading(getActivity());
        String year = birth;
        String gene = gender;
        phoneNum = phoneNum.replace("-", "");
        mReqAuthRealNameData = new ReqAuthRealNameData();
        mReqAuthRealNameData.setUserName(name);
        mReqAuthRealNameData.setBirthDate(year);
        mReqAuthRealNameData.setGenderCode(gene);
        mReqAuthRealNameData.setPhoneNo(phoneNum);
        mReqAuthRealNameData.setPhoneKind(mCurrentAgency);
        mReqAuthRealNameData.setRqstCausCd("00");
        mReqAuthRealNameData.setDeviceId(ElseUtils.getDeviceUUID(getActivity()));
        Call<ResponseAuthRealName> call = mApi.realNameRequest(mReqAuthRealNameData);
        call.enqueue(new Callback<ResponseAuthRealName>() {
            @Override
            public void onResponse(Call<ResponseAuthRealName> call, Response<ResponseAuthRealName> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    ResponseAuthRealName authRealName = response.body();
                    if ("B000".equalsIgnoreCase(authRealName.getCode())) {
                        int count = authRealName.getReqCount();
                        String warningCountLimit = null;
                        if (count < 5 && count >= 3) {
                            warningCountLimit = "\n\n 남은 1일 요청 가능 횟수 : "+(MAX_REQUEST_COUNT-count);
                        } else if (count >= 5) {
                            warningCountLimit = "\n\n 1일 요청 가능횟수를 모두 사용하셨습니다.";
                        }
                        mAuthRealName = response.body();
                        requestCount = authRealName.getReqCount();
                        delayedBtn();
                        startCountDown();
                        if (count >= 3) {
                            showPopup("인증 번호를 요청하였습니다.\n 문자메시지를 확인해주세요."+(warningCountLimit == null ? "" : warningCountLimit));
                        }
                    } else {
                        int count = 0;
                        if (authRealName != null) {
                            count = authRealName.getReqCount();
                        }
                        String warningCountLimit = null;
                        if (count < 5 && count >= 3) {
                            warningCountLimit = "\n\n 남은 1일 요청 가능 횟수 : "+(MAX_REQUEST_COUNT-count);
                        } else if (count >= 5) {
                            warningCountLimit = "\n\n 1일 요청 가능횟수를 모두 사용하셨습니다.";
                        }
                        showPopup("실명인증 요청 중 문제가 발생하였습니다. 잠시 후에 재요청 부탁드립니다."+("\n"+(warningCountLimit == null ? "" : warningCountLimit)));
                    }
                } else {
                    ErrorMessage message = ErrorUtils.parseError(response);
                    ResponseAuthRealName authRealName = response.body();
                    int count = 0;
                    if (authRealName != null) {
                        count = authRealName.getReqCount();
                    }
                    String warningCountLimit = null;
                    if (count < 5 && count >= 3) {
                        warningCountLimit = "\n\n 남은 1일 요청 가능 횟수 : "+(MAX_REQUEST_COUNT-count);
                    } else if (count >= 5) {
                        warningCountLimit = "\n\n 1일 요청 가능횟수를 모두 사용하셨습니다.";
                    }
                    switch (message) {
                        case ERROR_1013_CERT_UNDER14:
                            showPopup(getString(R.string.membership_text_warning_age_under_14));
                            break;
                        case ERROR_1001_CERT_FAIL_NAME:
                        case ERROR_1002_CERT_FAIL_BIRTHDAY:
                        case ERROR_1003_CERT_FAIL_GENDER:
                        case ERROR_1004_CERT_FAIL_NATION:
                        case ERROR_1005_CERT_FAIL_PHONE_KIND:
                        case ERROR_1006_CERT_FAIL_PHONE_NUM:
                        case ERROR_1009_CERT_FAIL_REQ_CODE:
                            showPopup(getString(R.string.membership_text_warning_not_matched)+(warningCountLimit == null ? "" : warningCountLimit));
                            cancelTimer();
                        case ERROR_1014_CERT_COUNT_OVER:
                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_YEAR, 1);
                            String date = ElseUtils.getMonthDayByCalendar(calendar.getTime());
                            String[] monthDay = date.split("\\.");
                            showPopup(getString(R.string.membership_text_warning_over_request)+"\n"+monthDay[0]+"월 "+monthDay[1]+"일 이후 추가 인증요청이 가능합니다.");
                            break;
                        default:
                            Toast.makeText(getContext(), "실명인증 요청 중 문제가 발생하였습니다. 잠시 후에 재요청 부탁드립니다.", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseAuthRealName> call, Throwable t) {
                DialogUtils.dismissLoading();
                Toast.makeText(getContext(), "실명인증 요청 중 문제가 발생하였습니다. 잠시 후에 재요청 부탁드립니다.", Toast.LENGTH_SHORT).show();
                mNextBtn.setEnabled(false);
            }
        });
    }

    private CountDownTimer mCountDownTimer;
    private StringBuffer mBuffer = new StringBuffer();

    private void startCountDown() {
        cancelTimer();
        isTimeOut = false;
        mRemainTimerTitle.setVisibility(View.VISIBLE);
        mRemainTimer.setVisibility(View.VISIBLE);
        mCountDownTimer = new CountDownTimer(180000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int days = (int) ((millisUntilFinished / 1000) / 86400);
                int hours = (int) (((millisUntilFinished / 1000) - (days
                        * 86400)) / 3600);

                int mins = (int) (((millisUntilFinished / 1000) - ((days
                        * 86400) + (hours * 3600))) / 60);
                int secs = (int) ((millisUntilFinished / 1000) % 60);
                if (mBuffer.length() > 0)
                    mBuffer.delete(0, mBuffer.length());
                if (mins > 0) {
                    mBuffer.append(mins);
                    mBuffer.append("분");
                }
                mBuffer.append(secs);
                mBuffer.append("초");
                mRemainTimer.setText(mBuffer.toString());
            }

            @Override
            public void onFinish() {
                isTimeOut = true;
                mRemainTimerTitle.setVisibility(View.INVISIBLE);
                mRemainTimer.setText(R.string.membership_text_warning_timeout);
                mAuthNum.setText(null);
            }
        };
        mCountDownTimer.start();
    }

    private void cancelTimer() {
        if (mCountDownTimer != null) {
            isTimeOut = false;
            mRemainTimerTitle.setVisibility(View.INVISIBLE);
            mRemainTimer.setVisibility(View.INVISIBLE);
            mCountDownTimer.cancel();
        }
    }

    private void showPopup(String str) {
        DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }, new InfoDataDialog(R.layout.dialog_inc_case_1, str, "확인"));
    }

    private void showPopup(String str, DialogInterface.OnClickListener listener) {
        DialogUtils.showDlgBaseOneButtonNoTitle(getActivity(), listener, new InfoDataDialog(R.layout.dialog_inc_case_1, str, "확인"));
    }

    private void showPhoneDropDown(final View v) {
        v.setBackgroundResource(R.drawable.selector_spinner_border_bg_open);
        int tempTop = (ElseUtils.getRect(v).bottom - ElseUtils.getStatusBar(getActivity()).top);
        int tempLeft = (ElseUtils.getRect(v).left);
        final TextView selected = (TextView) ((ViewGroup) v).getChildAt(0);
        final ArrayList<String> data = new ArrayList<String>();
        Collections.addAll(data, getActivity().getResources().getStringArray(R.array.phone_agency_type));
        String selectedText = selected.getText().toString();
        int selectIndex = -1;
        if (data.contains(selectedText)) {
            selectIndex = data.indexOf(selectedText);
        }
        DialogUtils.showDlgSpinnerList(getActivity(), new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selected.setTextAppearance(getActivity(), R.style.CS_Text_15_898989);
                selected.setText(data.get(which));
                String[] codes = getActivity().getResources().getStringArray(R.array.phone_agency_code);
                mCurrentAgency = codes[which];
                mPhoneNum.requestFocus();
                showKeyboard();
                dialog.dismiss();
            }
        }, data, tempTop, tempLeft, ((selectIndex == -1) ? 0 : selectIndex), new Dialog.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                v.setBackgroundResource(R.drawable.selector_spinner_border_bg);
            }
        });
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void confirm() {
        DialogUtils.showLoading(getActivity());
        String num = mAuthNum.getText().toString();

        if (TextUtils.isEmpty(num)) {
            Toast.makeText(getActivity(), "인증번호를 입력해주세요.", Toast.LENGTH_LONG).show();
            return;
        }

        if (isTimeOut) {
            Toast.makeText(getContext(), getString(R.string.membership_text_warning_timeout), Toast.LENGTH_LONG).show();
            return;
        }

        if (mAuthRealName == null) {
            Toast.makeText(getActivity(), "실명인증을 다시 요청해 주세요.", Toast.LENGTH_LONG).show();
            return;
        }

        final String certNum = mAuthNum.getText().toString();
        checkCert(certNum);
    }

    private void updateMobileNum() {
        final ReqCertRealNameData realNameData = new ReqCertRealNameData();
        realNameData.setDeviceId(ElseUtils.getDeviceUUID(getActivity()));
        Session.getInstance(getActivity()).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                LoginData data = DbManager.getInstance().getUserInfoDataSet(getActivity(), str, Session.getInstance(getActivity()).getUserId());
                Call<Result> call = mApi.updateMobileNum(data.getDayliToken(), realNameData);
                call.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                            Toast.makeText(getActivity(), "전화번호 변경이 완료되었습니다.", Toast.LENGTH_LONG).show();
                            setUpdateComplete();
                        } else {
                            ElseUtils.network_error(getActivity());
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {
                        ElseUtils.network_error(getActivity());
                    }
                });
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        memberLeaveComplete();
                        break;
                }
            }

            @Override
            public void fail() {

            }
        });
    }

    private void setUpdateComplete() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    private void checkCert(String num) {
        ReqCertRealNameData reqCheckAuthentication = new ReqCertRealNameData();
        reqCheckAuthentication.setSmsCertNo(num);
        reqCheckAuthentication.setDeviceId(ElseUtils.getDeviceUUID(getActivity()));
        Call<Result> call = mApi.realNameCert(reqCheckAuthentication);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                DialogUtils.dismissLoading();
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    Result result = response.body();
                    if (result != null) {
                        if (result.getCode().equalsIgnoreCase("B000")) {
                            if (isUpdateMode) {
                                updateMobileNum();
                            } else {
                                checkExistUser();
                            }
                        } else if (result.getCode().equalsIgnoreCase("B131")) {
                            showPopup("입력하신 인증번호가 올바르지 않습니다.\n문자메시지를 확인해주세요.");
                            mAuthNum.setText(null);
                            mAuthNum.requestFocus();
                        } else {
                            showPopup("실명인증에 실패하였습니다.");
                            mAuthNum.setText(null);
                        }
                    } else {
                        ElseUtils.network_error(getActivity());
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                DialogUtils.dismissLoading();
                ElseUtils.network_error(getActivity());
            }
        });
    }

    private void checkExistUser() {
        Call<Result> call = mApi.checkJoined(ElseUtils.getDeviceUUID(getActivity()));
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    Result result = response.body();
                    if (result != null) {
                        passOnData();
                    }
                } else {
                    ErrorMessage message = ErrorUtils.parseError(response);
                    switch (message) {
                        case ERROR_712_EXIST_USER:
                            showPopup("이미 가입하신 사용자입니다. 로그인하여 이용해주세요.", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();
                                }
                            });
                            mAuthNum.setText(null);
                            break;
                        case ERROR_719_QUITE_USER:
                            showPopup("탈퇴회원은 탈퇴일로부터 5일 이후 재가입이 가능합니다.");
                            mAuthNum.setText(null);
                            break;
                        case ERROR_1011_CERT_SESSION_EXPIRE:
                        case ERROR_1012_CERT_NOT_FINISH:
                            showPopup("실명인증 시간이 지났습니다. 실명인증을 다시 해주세요.");
                            mAuthNum.setText(null);
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    private void passOnData() {
        IgawAdbrix.firstTimeExperience(AnalyticsName.NameCheckComplete);
        String name = mReqAuthRealNameData.getUserName();
        String birth = mReqAuthRealNameData.getBirthDate();
        String gender = mReqAuthRealNameData.getGenderCode();
        MembershipData membershipData = new MembershipData();
        membershipData.setName(name);
        membershipData.setBirth(birth);
        membershipData.setGender(gender);
        membershipData.setPhoneNum(mReqAuthRealNameData.getPhoneNo());
        if (mArgument == null) {
            mArgument = new Bundle();
        }
        mArgument.putSerializable(Const.DATA, membershipData);
        BaseFragment fragment = new FragmentMembershipAccount();
        fragment.setArguments(mArgument);
        nextStep(fragment);
    }

    public void setFinish() {
        CustomToast.makeText(getContext(), "사용자 실명정보가 업데이트 되었습니다.", Toast.LENGTH_SHORT).show();
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    private void nextStep(BaseFragment fragment) {
        try {
            setCurrentFragment(R.id.fragment_area, fragment, fragment.getTagName());
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getLineNumber() {
        String number = null;
        try {
            TelephonyManager tMgr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String mPhoneNumber = tMgr.getLine1Number();
            number = mPhoneNumber;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return number;
    }

    private static final String DEFINE_REALNAME_SMS = "[DAYLI Pass]";
    private BroadcastReceiver mSmsReceiver;
    private void registerSmsListener() {
        if (mSmsReceiver == null) {
            mSmsReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent != null) {
                        String action = intent.getAction();
                        if (SMS_ACTION.equalsIgnoreCase(action)) {
                            Bundle bundle = intent.getExtras();
                            if (bundle != null) {
                                Object[] pdus = (Object[]) bundle.get("pdus");
                                if (pdus != null) {
                                    for (int i = 0; i < pdus.length; i++) {
                                        SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdus[i]);
                                        if (msg != null) {
                                            String content = msg.getMessageBody();
                                            if (content != null) {
                                                if (content.contains(DEFINE_REALNAME_SMS)) {
                                                    //mAuthNum
                                                    String number = content.replaceAll("[^0-9]", "");
                                                    mAuthNum.setText(number);
                                                }
                                            }
                                        }
                                    }
                                }
                                // txt.setText(result);
                            }
                        }
                    }
                }
            };
            IntentFilter filter = new IntentFilter();
            filter.addAction(SMS_ACTION);
            getActivity().registerReceiver(mSmsReceiver, filter);
        } else {
            unregisterSmsListener();
            registerSmsListener();
        }
    }

    private void unregisterSmsListener() {
        if (mSmsReceiver != null) {
            getActivity().unregisterReceiver(mSmsReceiver);
            mSmsReceiver = null;
        }
    }

}
