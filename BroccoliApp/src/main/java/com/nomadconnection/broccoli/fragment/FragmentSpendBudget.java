package com.nomadconnection.broccoli.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannedString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.activity.spend.Level2ActivitySpendBudgetSetting;
import com.nomadconnection.broccoli.activity.spend.Level3ActivitySpendBudgetDetail;
import com.nomadconnection.broccoli.adapter.spend.AdtSpendBudgetLookup;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.api.SpendApi;
import com.nomadconnection.broccoli.common.CustomExpandableListView;
import com.nomadconnection.broccoli.common.dialog.BaseVariableDialog;
import com.nomadconnection.broccoli.common.dialog.CommonDialogTextType;
import com.nomadconnection.broccoli.constant.BudgetCategory;
import com.nomadconnection.broccoli.constant.BudgetGroupCategory;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetCategory;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetData;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetDetail;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetLookUpData;
import com.nomadconnection.broccoli.interf.InterfaceEditOrDetail;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YelloHyunminJang on 2017. 3. 10..
 */

public class FragmentSpendBudget extends BaseFragment implements View.OnClickListener {

    public static final String TARGET_POSITION = "TARGET_POSITION";
    private InterfaceEditOrDetail mInterfaceEditOrDetail;
    private View mEmptyLayout;
    private CustomExpandableListView mExpListView;
    private AdtSpendBudgetLookup mAdapter;
    private WrapperExpandableListAdapter mWrapAdapter;
    private Button mBtn;

    private ArrayList<SpendBudgetData> mAllData;
    private SpendBudgetData mData;
    private TreeMap<Integer, ArrayList<SpendBudgetLookUpData>> mMap;
    private String mTargetYearMonth;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mInterfaceEditOrDetail = (InterfaceEditOrDetail) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement InterfaceEditOrDetail");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_spend_budget_monthly_list, null);

        init(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mAllData = (ArrayList<SpendBudgetData>) bundle.getSerializable(Const.DATA);
            int position = bundle.getInt(TARGET_POSITION);
            mData = mAllData.get(position);
            mTargetYearMonth = bundle.getString(Const.BUDGET_TARGET_MONTH);
        }
        initWidget(view);
        initData();
    }

    private void initWidget(View view) {
        mEmptyLayout = view.findViewById(R.id.ll_empty_layout);
        mExpListView = (CustomExpandableListView) view.findViewById(R.id.lv_fragment_spend_budget_monthly);
        mExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                startDetailView(groupPosition, childPosition);
                return false;
            }
        });
        mAdapter = new AdtSpendBudgetLookup(getContext());
        mWrapAdapter = new WrapperExpandableListAdapter(mAdapter);
        mExpListView.setAdapter(mWrapAdapter);
        mBtn = (Button) view.findViewById(R.id.btn_confirm);
        mBtn.setOnClickListener(this);
        mBtn.setVisibility(View.GONE);
        mBtn.setEnabled(true);
    }

    private void initData() {
        Calendar calendar = Calendar.getInstance();
        long currentTime = calendar.getTimeInMillis();
        calendar.set(Integer.valueOf(mTargetYearMonth.substring(0, 4)), Integer.valueOf(mTargetYearMonth.substring(4)), 1, 0, 0);
        long targetTime = calendar.getTimeInMillis();
        boolean isPast = false;
        if (currentTime > targetTime) {
            isPast = true;
        }
        int count = 0;
        int detailCount = 0;
        if (mData.categoryList != null) {
            count = mData.categoryList.size();
        }
        if (mData.detailList != null) {
            detailCount = mData.detailList.size();
        }
        mMap = createDataSet(mData);

        if (isPast) {
            mEmptyLayout.setVisibility(View.GONE);
            mExpListView.setVisibility(View.VISIBLE);
            mAdapter.setData(mMap);
            mAdapter.notifyDataSetChanged();
            mWrapAdapter.notifyDataSetChanged();
            mBtn.setVisibility(View.GONE);
            if (count != 0 || detailCount != 0) {
                mExpListView.post(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < mMap.size() ; i++) {
                            mExpListView.expandGroup(i);
                        }
                    }
                });
            }
        } else {
            if (count == 0) {
                mEmptyLayout.setVisibility(View.VISIBLE);
                mExpListView.setVisibility(View.GONE);
                mBtn.setVisibility(View.VISIBLE);
            } else {
                mBtn.setVisibility(View.GONE);
                mEmptyLayout.setVisibility(View.GONE);
                mExpListView.setVisibility(View.VISIBLE);
                mAdapter.setData(mMap);
                mAdapter.notifyDataSetChanged();
                mWrapAdapter.notifyDataSetChanged();
            }
            if (count != 0) {
                mExpListView.post(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < mMap.size() ; i++) {
                            mExpListView.expandGroup(i);
                        }
                    }
                });
            }
        }
    }

    private TreeMap<Integer, ArrayList<SpendBudgetLookUpData>> createDataSet(SpendBudgetData spendBudgetData) {
        TreeMap<Integer, ArrayList<SpendBudgetLookUpData>> childList = new TreeMap<>();
        ArrayList<SpendBudgetLookUpData> classifiedList = new ArrayList<>();
        ArrayList<SpendBudgetLookUpData> unClassifiedList = new ArrayList<>();
        ArrayList<BudgetGroupCategory> existList = new ArrayList<>();
        long classifiedCategorySum = 0;
        long classifiedDetailExpense = 0;
        if (spendBudgetData.categoryList != null) {
            for (SpendBudgetCategory targetCategory : spendBudgetData.categoryList) {
                classifiedCategorySum += targetCategory.sum;
                classifiedDetailExpense += targetCategory.expense;
                SpendBudgetLookUpData data = new SpendBudgetLookUpData();
                data.budgetId = targetCategory.budgetId;
                data.categoryGroup = targetCategory.categoryGroup;
                data.expense = targetCategory.expense;
                data.sum = targetCategory.sum;
                data.detailList = new ArrayList<>();
                BudgetGroupCategory group = BudgetGroupCategory.values()[targetCategory.categoryGroup];
                existList.add(group);
                BudgetCategory[] categories = group.child();
                ArrayList<SpendBudgetDetail> detailList = new ArrayList<>();
                if (spendBudgetData.detailList != null) {
                    for (SpendBudgetDetail spendBudgetDetail : spendBudgetData.detailList) {
                        int largeId = spendBudgetDetail.largeCategoryId;
                        for (int i=0; i < categories.length; i++) {
                            if (categories[i].ordinal() == largeId) {
                                detailList.add(spendBudgetDetail);
                            }
                        }
                    }
                }
                data.detailList = detailList;
                data.leftBudget = 0;
                data.leftExpense = 0;
                classifiedList.add(data);
            }
        }
        if (classifiedList.size() > 0) {
            Collections.sort(classifiedList, new Comparator<SpendBudgetLookUpData>() {
                @Override
                public int compare(SpendBudgetLookUpData lhs, SpendBudgetLookUpData rhs) {
                    return lhs.getExpense() > rhs.getExpense() ? -1 : lhs.getExpense() < rhs.getExpense() ? 1:0;
                }
            });
            childList.put(0, classifiedList);
        }

        BudgetGroupCategory[] group = BudgetGroupCategory.values();
        for (int index=1; index < group.length; index++) {
            BudgetGroupCategory groupCategory = group[index];
            if (!existList.contains(groupCategory)) {
                SpendBudgetLookUpData data = new SpendBudgetLookUpData();
                data.budgetId = 0;
                data.categoryGroup = groupCategory.ordinal();
                data.expense = 0;
                data.sum = 0;
                data.isUnClassifiedData = true;
                data.leftExpense = spendBudgetData.totalExpense - classifiedDetailExpense;
                data.leftBudget = spendBudgetData.totalSum - classifiedCategorySum;
                ArrayList<SpendBudgetDetail> detailList = new ArrayList<>();
                BudgetCategory[] detail = groupCategory.child();
                for (int i = 0; i < detail.length; i++) {
                    SpendBudgetDetail spendBudgetDetail = new SpendBudgetDetail();
                    if (spendBudgetData.detailList != null) {
                        boolean isExist = false;
                        for (SpendBudgetDetail budgetDetail : spendBudgetData.detailList) {
                            if (detail[i].ordinal() == budgetDetail.largeCategoryId) {
                                spendBudgetDetail.expense = budgetDetail.expense;
                                spendBudgetDetail.sum = budgetDetail.sum;
                                spendBudgetDetail.largeCategoryId = budgetDetail.largeCategoryId;
                                spendBudgetDetail.detailId = budgetDetail.detailId;
                                spendBudgetDetail.budgetId = budgetDetail.budgetId;
                                detailList.add(spendBudgetDetail);
                                isExist = true;
                                data.expense += budgetDetail.expense;
                                data.sum += budgetDetail.sum;
                                break;
                            }
                        }
                        if (!isExist) {
                            spendBudgetDetail.largeCategoryId = detail[i].ordinal();
                            spendBudgetDetail.expense = 0;
                            spendBudgetDetail.sum = 0;
                            detailList.add(spendBudgetDetail);
                        }
                    } else {
                        spendBudgetDetail.largeCategoryId = detail[i].ordinal();
                        spendBudgetDetail.expense = 0;
                        spendBudgetDetail.sum = 0;
                        detailList.add(spendBudgetDetail);
                    }
                }
                data.detailList = detailList;

                unClassifiedList.add(data);
            }
        }

        if (unClassifiedList.size() > 0) {
            Collections.sort(unClassifiedList, new Comparator<SpendBudgetLookUpData>() {
                @Override
                public int compare(SpendBudgetLookUpData lhs, SpendBudgetLookUpData rhs) {
                    return lhs.getExpense() > rhs.getExpense() ? -1 : lhs.getExpense() < rhs.getExpense() ? 1:0;
                }
            });
            childList.put(1, unClassifiedList);
        }
        return childList;
    }

    private void startDetailView(int groupPosition, int position) {
        if (mMap != null) {
            ArrayList<?> list = mMap.get(groupPosition);
            if (list != null) {
                SpendBudgetLookUpData data = mMap.get(groupPosition).get(position);
                Intent intent = new Intent(getActivity(), Level3ActivitySpendBudgetDetail.class);
                intent.putExtra(Const.DATA, data);
                intent.putExtra(Const.BUDGET_TARGET_MONTH, mTargetYearMonth);
                startActivity(intent);
            }
        }
    }

    private void newBudgetSetting() {
        boolean beforeBudgetExist = false;
        Calendar calendar = Calendar.getInstance();
        long currentTime = calendar.getTimeInMillis();
        long targetId = 0;
        for (SpendBudgetData data : mAllData) {
            calendar.set(Integer.valueOf(data.baseMonth.substring(0, 4)), Integer.valueOf(data.baseMonth.substring(4))-1, 1);
            long dataTime = calendar.getTimeInMillis();
            if (dataTime < currentTime) {
                long sum = data.totalSum;
                if (sum != 0) {
                    targetId = data.budgetId;
                    beforeBudgetExist = true;
                }
            }
        }
        if (beforeBudgetExist) {
            CommonDialogTextType dialogTextType = new CommonDialogTextType(getActivity(),
                    SpannedString.valueOf(getString(R.string.spend_budget_list_get_before_budget_title)),
                    new int[] {R.string.spend_budget_list_get_before_budget_add_new, R.string.spend_budget_list_get_before_budget_copy});
            dialogTextType.addTextView(getResources().getString(R.string.spend_budget_list_get_before_budget_content));
            dialogTextType.addTextView(getResources().getString(R.string.spend_budget_list_get_before_budget_content2));
            final long finalTargetId = targetId;
            dialogTextType.setDialogButtonOnClickListener(new BaseVariableDialog.DialogButtonOnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    switch (id) {
                        case R.string.spend_budget_list_get_before_budget_add_new:
                            addNewBudget();
                            break;
                        case R.string.spend_budget_list_get_before_budget_copy:
                            copyBudget(finalTargetId);
                            break;
                    }
                }
            });
            dialogTextType.show();
        } else {
            addNewBudget();
        }
    }

    private void addNewBudget() {
        Intent intent = new Intent(getActivity(), Level2ActivitySpendBudgetSetting.class);
        intent.putExtra(Const.BUDGET_TARGET_MONTH, mTargetYearMonth);
        getActivity().startActivity(intent);
    }

    private void copyBudget(long id) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("targetBudgetId", id);
        map.put("baseMonth", mData.baseMonth);
        Call<Result> call = ServiceGenerator.createService(SpendApi.class).copyBudget(map);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful() || response.message().equalsIgnoreCase(Const.OK)) {
                    Result result = response.body();
                    if (result != null) {
                        if (result.isOk()) {
                            onRefresh();
                        }
                    } else {
                        Toast.makeText(getContext(), "통신 중 에러가 발생하였습니다. 잠시 후 재시도해 주세요.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    ElseUtils.network_error(getActivity());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                ElseUtils.network_error(getActivity());
            }
        });
    }

    private void onRefresh() {
        if (mInterfaceEditOrDetail != null)
            mInterfaceEditOrDetail.onRefresh();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_confirm:
                newBudgetSetting();
                break;
        }
    }
}


