package com.nomadconnection.broccoli.api;

import com.nomadconnection.broccoli.data.Investment.InventMainData;
import com.nomadconnection.broccoli.data.Investment.InvestNewsyStockData;
import com.nomadconnection.broccoli.data.Investment.RequestStockCode;
import com.nomadconnection.broccoli.data.RequestMainData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by YelloHyunminJang on 16. 2. 5..
 */
public interface InvestApi {

    @POST("pam/invest/main")
    Call<InventMainData> getInvestMainData(@Body RequestMainData id);

    @POST("pam/invest/stock/list/stockcode")
    Call<InvestNewsyStockData> getNewsyStockData(@Body RequestStockCode code);

}
