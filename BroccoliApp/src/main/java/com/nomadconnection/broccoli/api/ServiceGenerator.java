package com.nomadconnection.broccoli.api;

import com.google.gson.Gson;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.data.CdnAppInfoData;
import com.nomadconnection.broccoli.data.CdnServerAddress;
import com.nomadconnection.broccoli.session.Session;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 */
public class ServiceGenerator {

    private static final ServiceUrl SERVICE_URL = new ServiceUrl(ServiceUrl.ServerURL.CDN);

    private static OkHttpClient httpClient = null;
    private static Retrofit mRetrofit = null;
    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl(SERVICE_URL.getBaseUrl())
            .addConverterFactory(GsonConverterFactory.create());
    private static OkHttpClient httpClientForScrap = null;
    private static Retrofit.Builder builderForScrap = new Retrofit.Builder().baseUrl(SERVICE_URL.getScrapUrl())
            .addConverterFactory(GsonConverterFactory.create());
    private static OkHttpClient httpClientForRemit = null;
    private static Retrofit.Builder builderForRemit = new Retrofit.Builder().baseUrl("http://10.10.102.200:8080/")
            .addConverterFactory(GsonConverterFactory.create());
    private static OkHttpClient httpClientForDayli = null;
    private static Retrofit.Builder builderForDayli = new Retrofit.Builder().baseUrl(SERVICE_URL.getDayliUrl())
            .addConverterFactory(GsonConverterFactory.create());
    private static OkHttpClient httpClientForDemo = null;
    private static Retrofit.Builder builderForDemo = new Retrofit.Builder().baseUrl(SERVICE_URL.getBaseUrl())
            .addConverterFactory(GsonConverterFactory.create());

    public static Retrofit getRetrofit() {
        if (mRetrofit == null) {
            mRetrofit = builder.build();
        }
        return mRetrofit;
    }

    public static <S> S createService(Class<S> service) {
        if (httpClient == null) {
            httpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept", "application/json")
                            .method(original.method(), original.body());
                    if (Session.TOKEN != null) {
                        requestBuilder.addHeader(Const.TOKEN, Session.TOKEN);
                    }

                    Request request = requestBuilder.build();
//                Response response = chain.proceed(request);
//
//                String rawJson = response.body().string();
//                Response ret = response.newBuilder()
//                        .body(ResponseBody.create(response.body().contentType(), rawJson)).message(rawJson).build();
//                return ret;
                    return chain.proceed(request);
                }
            }).build();
        }
        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(service);
    }

    public static <S> S createScrapingService(Class<S> service) {
        if (httpClientForScrap == null) {
            httpClientForScrap = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept", "application/json")
                            .method(original.method(), original.body());
                    if (Session.TOKEN != null) {
                        requestBuilder.addHeader(Const.TOKEN, Session.TOKEN);
                    }

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            }).build();
        }
        Retrofit retrofit = builderForScrap.client(httpClientForScrap).build();
        return retrofit.create(service);
    }

//    public static <S> S createSecureService(Class<S> service) {
//        BaseUrl baseUrl = service.getAnnotation(BaseUrl.class);
//        Uri uri = new Uri.Builder()
//                .scheme(baseUrl.scheme())
//                .encodedAuthority(baseUrl.host())
//                .appendEncodedPath(baseUrl.path())
//                .build();
//        if (mAdapter == null) {
//            mAdapter = new SecureCallAdapterFactory();
//        }
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(uri.toString())
//                .addCallAdapterFactory(mAdapter)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        return retrofit.create(service);
//    }

    public static <S> S createServiceDayli(Class<S> service) {
        if (httpClientForDayli == null) {
            httpClientForDayli = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            }).build();
        }
        Retrofit retrofit = builderForDayli.client(httpClientForDayli).build();
        return retrofit.create(service);
    }

    public static <S> S createServiceDayliUpload(Class<S> service) {
        if (httpClientForDayli == null) {
            httpClientForDayli = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            }).build();
        }
        Retrofit retrofit = builderForDayli.client(httpClientForDayli).build();
        return retrofit.create(service);
    }

    public static <S> S createRemitService(Class<S> service) {
        if (httpClientForRemit == null) {
            httpClientForRemit = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            }).build();
        }

        Retrofit retrofit = builderForRemit.client(httpClientForRemit).build();
        return retrofit.create(service);
    }

    public static <S> S createServiceForDemo(Class<S> service) {
        if (httpClientForDemo == null) {
            httpClientForDemo = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            }).build();
        }
        Retrofit retrofit = builderForDemo.client(httpClientForDemo).build();
        return retrofit.create(service);
    }

    public static void setToken(final String token) {
        httpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .method(original.method(), original.body());
                if (Session.TOKEN != null) {
                    requestBuilder.addHeader(Const.TOKEN, token);
                }

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        }).build();
        httpClientForScrap = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .method(original.method(), original.body());
                if (Session.TOKEN != null) {
                    requestBuilder.addHeader(Const.TOKEN, token);
                }

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        }).build();
    }

    public static void setAddress(CdnAppInfoData address) {
        if (address != null) {
            String localVersion = MainApplication.getVersionName();
            CdnServerAddress serverAddress = new Gson().fromJson(address.host.get(localVersion), CdnServerAddress.class);
            if (serverAddress != null) {
                SERVICE_URL.setBaseUrl(serverAddress.broccoli_inq);
                SERVICE_URL.setScrapUrl(serverAddress.broccoli_col);
                SERVICE_URL.setDayliUrl(serverAddress.daylipass);
                builder.baseUrl(SERVICE_URL.getBaseUrl());
                builderForScrap.baseUrl(SERVICE_URL.getScrapUrl());
                builderForDayli.baseUrl(SERVICE_URL.getDayliUrl());
                builderForDemo.baseUrl(SERVICE_URL.getBaseUrl());
            }
        }
    }
}
