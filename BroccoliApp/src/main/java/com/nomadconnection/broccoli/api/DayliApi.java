package com.nomadconnection.broccoli.api;

import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.data.Dayli.DayliChangePwdInfo;
import com.nomadconnection.broccoli.data.Dayli.DayliJoinData;
import com.nomadconnection.broccoli.data.Dayli.DayliLoginData;
import com.nomadconnection.broccoli.data.Dayli.ReqAuthRealNameData;
import com.nomadconnection.broccoli.data.Dayli.ReqCertRealNameData;
import com.nomadconnection.broccoli.data.Dayli.ResponseAuthRealName;
import com.nomadconnection.broccoli.data.Dayli.ResponseEmail;
import com.nomadconnection.broccoli.data.Dayli.ResponseJoin;
import com.nomadconnection.broccoli.data.Dayli.ResponseLogin;
import com.nomadconnection.broccoli.data.Dayli.UserPersonalInfoEdit;
import com.nomadconnection.broccoli.data.Dayli.UserProfileUrl;
import com.nomadconnection.broccoli.data.Result;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by YelloHyunminJang on 2016. 12. 27..
 */

public interface DayliApi {

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @POST("api/users/login")
    Call<ResponseLogin> dayliLogin(@Body DayliLoginData login);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @GET("api/users/check/email")
    Call<Result> dayliIdCheck(@Query("email") String email);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @POST("api/verify/email/send")
    Call<Result> dayliEmailCert(@Header("accessToken") String accessToken, @Body HashMap<String, Object> body);


    @POST("api/verify/name/sms")
    Call<ResponseAuthRealName> realNameRequest(@Body ReqAuthRealNameData data);

    @POST("api/verify/name/cert")
    Call<Result> realNameCert(@Body ReqCertRealNameData data);

    @GET("api/users/check/name")
    Call<Result> checkJoined(@Query("deviceId") String deviceId);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @POST("api/users/create/onestep")
    Call<ResponseJoin> dayliPassJoin(@Body DayliJoinData data);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @GET("api/users/find/id")
    Call<ResponseEmail> dayliFindId(@Query("deviceId") String deviceId);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @GET("api/users/find/password/check")
    Call<Result> dayliPasswordRecover(@Query("deviceId") String deviceId, @Query("email") String email);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @PATCH("api/users/find/password/change") //deviceID, newPassword
    Call<Result> dayliPasswordReset(@Body HashMap<String, Object> body);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @GET("api/users/change/password/check") //"userId", "oldPassword"
    Call<Result> dayliPasswordCheck(@Header("accessToken") String token, @Query("userId") long dayliId, @Query("oldPassword") String pwd);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @PATCH("api/users/change/password")
    Call<Result> dayliChangePassword(@Header("accessToken") String accessToken, @Body DayliChangePwdInfo info);


    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @POST("api/terms/agree")
    Call<Result> termsAgree(@Header("accessToken") String dpToken, @Body List<Integer> termsAgree);


    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @GET("api/users/info")
    Call<JsonObject> getUserInfo(@Header("accessToken") String token);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @PATCH("api/users/info/update")
    Call<Result> updateUserInfo(@Header("accessToken") String token, @Body UserPersonalInfoEdit info);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @PATCH("api/users/change/mobilenum")
    Call<Result> updateMobileNum(@Header("accessToken") String token, @Body ReqCertRealNameData data);

    @Headers({
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @Multipart
    @POST("api/users/profile/image")
    Call<UserProfileUrl> uploadProfileImage(@Header("accessToken") String token, @Part MultipartBody.Part file);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "appServiceKey: f00bc453a0634b8ff502553229198aaf663469d478184ed441598b0261b8259c"
    })
    @POST("api/users/coupon/register") // code
    Call<Result> sendCouponCode(@Header("accessToken") String dpToken, @Body HashMap<String, Object> body);
}
