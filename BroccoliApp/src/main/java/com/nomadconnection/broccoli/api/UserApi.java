package com.nomadconnection.broccoli.api;

import com.google.gson.JsonElement;
import com.nomadconnection.broccoli.data.KeyDataSet;
import com.nomadconnection.broccoli.data.MainPopupData;
import com.nomadconnection.broccoli.data.Notice;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Session.OldUserData;
import com.nomadconnection.broccoli.data.Session.ResponseBroccoli;
import com.nomadconnection.broccoli.data.Session.User;
import com.nomadconnection.broccoli.data.Session.UserData;
import com.nomadconnection.broccoli.data.Session.UserDeviceInfo;
import com.nomadconnection.broccoli.data.Session.UserFinancialData;
import com.nomadconnection.broccoli.data.SettingNotice;
import com.nomadconnection.broccoli.data.Spend.Qna;
import com.nomadconnection.broccoli.data.Version;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by YelloHyunminJang on 16. 1. 15..
 */
public interface UserApi {

    @POST("pam/inquiry/version")
    Call<Version> appVersion();

    /**
     * 금융기관 연동에서 금융기관 해제
     * @param data
     * @return
     */
    @POST("pam/user/company/delete")
    Call<Result> userFinancialDelete(@Body UserFinancialData data);

    @POST("pam/user/key")
    Call<KeyDataSet> getSecureKey(@Body RequestDataByUserId id);

    @POST("pam/inquiry/qna")
    Call<List<Qna>> qna();

    @POST("pam/user/realname/info")
    Call<User> requestRealNameInfo(@Body RequestDataByUserId id);

    @POST("pam/inquiry/notice")
    Call<List<Notice>> mainNotice(@Body RequestDataByUserId id);

    @POST("pam/inquiry/innernotice")
    Call<List<SettingNotice>> SettingNotice(@Body RequestDataByUserId id);

    @POST("pam/user/adddevice")
    Call<Result> registerDevice(@Body UserDeviceInfo info);



    //--------------------------------------------------------------------------
    // 아래부터는 email id, pwd 연동하면서 신규, 변동된 api 기존에 안 쓰는 api정리 필요함
    //--------------------------------------------------------------------------

    /**
     * 금융기관 id/pw로 연동 시에 카드사별 다른 pw연동 제약 정보를 확인하는 api
     * @return
     */
    @POST("pam/data/cardLoginLimit")
    Call<JsonElement> getCardLoginLimit();

    /**
     * 로그인 화면에서 신규로 로그인 시에 사용하는 api
     * @param user
     * @return
     */
    @POST("pam/user/login")
    Call<ResponseBroccoli> broccoliLogin(@Body UserData user);

    /**
     * 회원 자동로그인 시에 사용하는 api
     * @param user
     * @return
     */
    @POST("pam/user/autoLogin")
    Call<ResponseBroccoli> autoLogin(@Body UserData user);

    /**
     * 회원 로그아웃 시에 사용하는 api
     * @return
     */
    @GET("pam/user/logout")
    Call<Result> logout();

    /**
     * 신규 회원이 브로콜리에 가입할 때 사용하는 api
     * @param user
     * @return
     */
    @POST("pam/user/add")
    Call<ResponseBroccoli> userAdd(@Body UserData user);

    /**
     * 아이디, 패스워드 이전 버전 사용자가 daylipass 가입을 위해 브로콜리 서버에 요청하는 api
     * @param user
     * @return
     */
    @POST("pam/user/integrate")
    Call<ResponseBroccoli> oldMemberUpgrade(@Body OldUserData user);

    /**
     * 기존 브로콜리 유저가 있는지 체크
     * @param user
     * @return
     */
    @POST("pam/user/check/exist")
    Call<Result> oldMemberExist(@Body OldUserData user);

    /**
     * 사용자 푸시 설정
     * @param body <b>param key(value) : userId(String), pushYn(String)</b>
     * @return
     */
    @POST("pam/user/push")
    Call<Result> setUserPushEnable(@Body HashMap<String, Object> body);

    /**
     * 푸시 키 등록
     * @param body <b>param key(value) : osType(int), pushKey(String)</b>
     * @return
     */
    @POST("pam/user/pushkey")
    Call<Result> setUserPushkey(@Body HashMap<String, Object> body);

    @GET("pam/user")
    Call<Result> getUserInfo();

    /**
     * 회원탈퇴
     * @param body <b>param key(value) : dpAccessToken(String)</b>
     * @return
     */
    @POST("pam/user/quit")
    Call<Result> reqQuit(@Body HashMap<String, Object> body);

    /**
     * 메인화면 공지 팝업
     * @param userId
     * @return
     */
    @POST("pam/inquiry/popup")
    Call<List<MainPopupData>> requestMainNotice(@Body RequestDataByUserId userId);
}
