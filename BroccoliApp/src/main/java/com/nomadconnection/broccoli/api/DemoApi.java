package com.nomadconnection.broccoli.api;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by YelloHyunminJang on 2017. 4. 3..
 */

public interface DemoApi {

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"
    })
    @GET("pam/trial/asset")
    Call<JsonElement> getAssetTrial(@Query("age") int age, @Query("gender") String gender);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"
    })
    @GET("pam/trial/expense")
    Call<JsonElement> getExpenseTrial(@Query("age") int age, @Query("gender") String gender);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"
    })
    @GET("pam/trial/stock")
    Call<JsonElement> getStockTrial(@Query("age") int age, @Query("gender") String gender);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"
    })
    @GET("pam/trial/etc")
    Call<JsonElement> getEtcTrial(@Query("age") int age, @Query("gender") String gender);

}
