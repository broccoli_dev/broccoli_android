package com.nomadconnection.broccoli.api;

import com.nomadconnection.broccoli.data.Log.LogMsgData;
import com.nomadconnection.broccoli.data.Result;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by YelloHyunminJang on 2016. 11. 2..
 */
public interface LogApi {

    @POST("pam/log/join")
    Call<Result> setLogMsg(@Body LogMsgData log);

    @POST("pam/log/cardLoan")
    Call<Result> sendLogCardLoan(@Body String str);

    @POST("pam/log/honestfund")
    Call<Result> sendLogHonestFund(@Body HashMap<String, Object> str);

    @POST("pam/log/cardclick")
    Call<Result> sendLogRecommendListClick(@Body HashMap<String, Object> body);

    @POST("pam/log/applyclick")
    Call<Result> sendLogCardOnlineRequest(@Body HashMap<String, Object> body);
}
