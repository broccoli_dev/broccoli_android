package com.nomadconnection.broccoli.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by YelloHyunminJang on 2017. 1. 25..
 */

public interface FileDownloadService {

    @GET("{type}/config/version.json")
    Call<ResponseBody> downloadFileFromCdn(@Path("type") String type);

    @GET
    Call<ResponseBody> downloadFileWithDynamicUrl(@Url String url);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ServiceUrl.CDN_BASE)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
