package com.nomadconnection.broccoli.api;

/**
 * Created by YelloHyunminJang on 16. 5. 30..
 */
public class ServiceUrl {

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getScrapUrl() {
        return scrapUrl;
    }

    public String getRemitUrl() {
        return remitUrl;
    }

    public String getDayliUrl() {
        return dayliUrl;
    }

    public void setScrapUrl(String url) {
        scrapUrl = url;
    }

    public void setBaseUrl(String url) {
        baseUrl = url;
    }

    public void setDayliUrl(String url) {
        dayliUrl = url;
    }

    public enum ServerURL {
        DEV,
        BETA,
        BIZ,
        CDN
    }

    public static String getCdnVersion() {
        return CDN_LIVE;
    }

    public static final String CDN_BASE = "http://broccoli-cdn.s3.ap-northeast-2.amazonaws.com/";
    public static final String CDN_DEV = "dev";
    public static final String CDN_BETA = "beta";
    public static final String CDN_LIVE = "live";

    public static final String API_TEST = "http://broccoli-dev.yellofg.com:8080/";
    public static final String API_NOMAD_DEV = "http://10.10.102.211:8080/";
    public static final String API_NOMAD_DEV_ORIGIN = "http://10.10.102.200:8080/";
    public static final String API_NOMAD_DEV_ORIGIN_DAYLI = "http://10.10.102.211:8080/";
    public static final String API_DEV_DAYLI = "http://10.10.109.201:8080/";
    public static final String API_DEV_HANSUNG_DAYLI = "http://192.168.12.211:8088/";
    public static final String API_DEV_HANSUNG = "http://192.168.12.211:8080/";
    public static final String API_DEV_TEAM = "http://192.168.12.210:8080/";
    public static final String API_LIVE_TEST = "http://10.10.102.1:7979/";
    public static final String API_DEV_URL = "http://devinq.mybroccoli.co.kr/";
    public static final String API_DEV_URL_FOR_SCRAP = "http://devcol.mybroccoli.co.kr/";
    public static final String API_DEV_URL_DAYLI = "https://dev.daylipass.com/";
    public static final String API_BETA_URL = "https://betainq.mybroccoli.co.kr/";
    public static final String API_BETA_URL_FOR_SCRAP = "https://betacol.mybroccoli.co.kr/";
    public static final String API_BETA_URL_DAYLI = "http://10.10.102.201:8088/";
    public static final String API_REMIT_DEV = "http://10.10.102.200:8080/";//"http://devinq.mybroccoli.co.kr/"; //http://10.10.102.200:8080/";

    public static final String API_BIZ_URL = "https://inq.mybroccoli.co.kr/";
    public static final String API_BIZ_URL_FOR_SCRAP = "https://col.mybroccoli.co.kr/";
    public static final String API_BIZ_URL_DAYLI = "https://daylipass.com/";

    private String baseUrl = null;
    private String scrapUrl = null;
    private String remitUrl = null;
    private String dayliUrl = null;
    private String cdnUrl = null;

    public ServiceUrl(ServerURL url) {
        switch (url) {
            case DEV:
                baseUrl = API_DEV_TEAM;
                remitUrl = API_REMIT_DEV;
                scrapUrl = API_DEV_TEAM;
                dayliUrl = API_DEV_DAYLI;
                break;
            case BETA:
                baseUrl = API_BETA_URL;
                scrapUrl = API_BETA_URL_FOR_SCRAP;
                remitUrl = API_REMIT_DEV;
                dayliUrl = API_BETA_URL_DAYLI;
                break;
            case BIZ:
                baseUrl = API_BIZ_URL;
                scrapUrl = API_BIZ_URL_FOR_SCRAP;
                remitUrl = API_REMIT_DEV;
                dayliUrl = API_BIZ_URL_DAYLI;
                break;
            case CDN:
                baseUrl = API_BIZ_URL;
                scrapUrl = API_BIZ_URL_FOR_SCRAP;
                dayliUrl = API_BIZ_URL_DAYLI;
                break;
        }
    }

}
