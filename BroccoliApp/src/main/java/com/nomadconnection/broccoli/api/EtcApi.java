package com.nomadconnection.broccoli.api;

import com.nomadconnection.broccoli.data.Etc.BodyMoneyInfo;
import com.nomadconnection.broccoli.data.Etc.BodyOrgInfo;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeBank;
import com.nomadconnection.broccoli.data.Etc.EtcChallengeInfo;
import com.nomadconnection.broccoli.data.Etc.EtcMainData;
import com.nomadconnection.broccoli.data.Etc.EtcNoticeInfo;
import com.nomadconnection.broccoli.data.Etc.RequestEtcChallengeBank;
import com.nomadconnection.broccoli.data.Etc.RequestEtcChallengeDelete;
import com.nomadconnection.broccoli.data.Etc.RequestEtcChallengeSave;
import com.nomadconnection.broccoli.data.Etc.RequestEtcMoneyDelete;
import com.nomadconnection.broccoli.data.Etc.RequestEtcMoneyEdit;
import com.nomadconnection.broccoli.data.Etc.RequestEtcMoneyList;
import com.nomadconnection.broccoli.data.Etc.RequestEtcNoticeRead;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.RequestMainData;
import com.nomadconnection.broccoli.data.Result;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by YelloHyunminJang on 16. 1. 18..
 */
public interface EtcApi {

    /**
     * 기타 메인 조회
     * @param userId
     * @return
     */
    @POST("pam/etc/main")
    Call<EtcMainData> getEtcMain(@Body RequestMainData userId);

    /**
     * 머니캘린더 리스트
     * @param object
     * @return
     */
    @POST("pam/etc/calendar/list")
    Call<ArrayList<BodyMoneyInfo>> getCalendarList(@Body RequestEtcMoneyList object);


    /**
     * 청구기관 리스트 조회
     * @param userId
     * @return
     */
    @POST("pam/etc/calendar/transaction")
    Call<ArrayList<BodyOrgInfo>> getOrgList(@Body RequestDataByUserId userId);



    /**
     * 머니캘린더 내역 추가/수정
     * @param object
     * @return
     */
    @POST("pam/etc/calendar/save")
    Call<Result> editMoney(@Body RequestEtcMoneyEdit object);

    /**
     * 머니캘린더 내역 삭제
     * @param object
     * @return
     */
    @POST("pam/etc/calendar/delete")
    Call<Result> deleteMoney(@Body RequestEtcMoneyDelete object);


    /**
     * 챌린지 리스트 조회
     * @param object
     * @return
     */
    @POST("pam/etc/challenge/list")
    Call<List<EtcChallengeInfo>> listChallenge(@Body RequestDataByUserId object);

    /**
     * 챌린지 계좌 조회
     * @param object
     * @return
     */
    @POST("pam/etc/challenge/bank")
    Call<List<EtcChallengeBank>> challengeBankLink(@Body RequestEtcChallengeBank object);

    /**
     * 챌린지 등록/수정
     * @param object
     * @return
     */
    @POST("pam/etc/challenge/save")
    Call<Result> challengeSave(@Body RequestEtcChallengeSave object);


    /**
     * 챌린지 삭제
     * @param object
     * @return
     */
    @POST("pam/etc/challenge/delete")
    Call<Result> challengeDelete(@Body RequestEtcChallengeDelete object);

    /**
     * 알림 읽음으로 수정
     * @param userId
     * @return
     */
    @POST("pam/notification/list")
    Call<List<EtcNoticeInfo>> listNotice(@Body RequestDataByUserId userId);

    /**
     * 알림 내역 조회
     * @param object
     * @return
     */
    @POST("pam/notification/read")
    Call<Result> readNotice(@Body RequestEtcNoticeRead object);

}
