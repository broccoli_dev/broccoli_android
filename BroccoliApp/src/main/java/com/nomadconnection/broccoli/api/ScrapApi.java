package com.nomadconnection.broccoli.api;

import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BankAccountList;
import com.nomadconnection.broccoli.data.Scrap.BankRecordList;
import com.nomadconnection.broccoli.data.Scrap.CardApprovalList;
import com.nomadconnection.broccoli.data.Scrap.CardBillList;
import com.nomadconnection.broccoli.data.Scrap.CardDueList;
import com.nomadconnection.broccoli.data.Scrap.CardLimit;
import com.nomadconnection.broccoli.data.Scrap.CardList;
import com.nomadconnection.broccoli.data.Scrap.CardLoan;
import com.nomadconnection.broccoli.data.Scrap.CashReceiptList;
import com.nomadconnection.broccoli.data.Scrap.RequestDataByUploadId;
import com.nomadconnection.broccoli.data.Scrap.UploadLogMsg;
import com.nomadconnection.broccoli.data.Scrap.UploadStatus;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by YelloHyunminJang on 16. 1. 13..
 */
public interface ScrapApi {
    @POST("pam/hcollect/bank/list")
    Call<Result> sendBankAccountList(@Body BankAccountList list);

    @POST("pam/hcollect/bank/trans")
    Call<Result> sendBankRecordList(@Body BankRecordList list);

    @POST("pam/hcollect/card/approval")
    Call<Result> sendCardApprovalList(@Body CardApprovalList list);

    @POST("pam/hcollect/card/list")
    Call<Result> sendCardList(@Body CardList list);

    @POST("pam/hcollect/card/bill")
    Call<Result> sendCardBillList(@Body CardBillList list);

    @POST("pam/hcollect/card/limit")
    Call<Result> sendCardLimit(@Body CardLimit limit);

    @POST("pam/hcollect/card/schedule")
    Call<Result> sendCardDue(@Body CardDueList list);

    @POST("pam/hcollect/cardloan")
    Call<Result> sendCardLoan(@Body CardLoan data);

    @POST("pam/hcollect/cash")
    Call<Result> sendCashReceiptList(@Body CashReceiptList list);

    @POST("pam/inquiry/success/latest")
    Call<UploadStatus> getUploadStatus(@Body RequestDataByUserId id);

    @POST("pam/inquiry/success")
    Call<Result> getUploadResult(@Body RequestDataByUploadId id);

    @POST("pam/hcollect/error")
    Call<Result> sendErrorReport(@Body UploadLogMsg id);
}
