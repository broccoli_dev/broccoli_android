package com.nomadconnection.broccoli.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by YelloHyunminJang on 16. 5. 10..
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface BaseUrl {
    String scheme() default  "http";
    String host() default "localhost";
    String path() default "";
}
