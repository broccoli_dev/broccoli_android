package com.nomadconnection.broccoli.api;

import com.nomadconnection.broccoli.data.Remit.RemitBank;
import com.nomadconnection.broccoli.data.Remit.RemitBankDetail;
import com.nomadconnection.broccoli.data.Remit.RemitResult;
import com.nomadconnection.broccoli.data.Remit.RemitUserInfo;
import com.nomadconnection.broccoli.data.Remit.RequestDataARS;
import com.nomadconnection.broccoli.data.Remit.RequestDataARSCheck;
import com.nomadconnection.broccoli.data.Remit.RequestDataAccountDel;
import com.nomadconnection.broccoli.data.Remit.RequestDataChangeMoney;
import com.nomadconnection.broccoli.data.Remit.RequestDataPW;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendAccount;
import com.nomadconnection.broccoli.data.Remit.RequestDataSendPhone;
import com.nomadconnection.broccoli.data.Remit.RequestRemitReceivedMoney;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitARS;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitARSCheck;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccount;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccountCurrent;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitAccountEdit;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitChargeMoney;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitPhoneCurrent;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitReceived;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitSend;
import com.nomadconnection.broccoli.data.Remit.ResponseRemitStandByReceivedMoney;
import com.nomadconnection.broccoli.data.RequestDataByUserId;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by YelloHyunminJang on 16. 1. 18..
 */
public interface RemitApi {

    /**
     * 송금 가능 은행 조회
     * @param userId
     * @return
     */
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("sendAccountManage/getPossbleBankList")
    Call<ArrayList<RemitBank>> getRemitBank(@Body RequestDataByUserId userId);

    /**
     * 송금 가능 은행 상세 조회
     * @param userId
     * @return
     */
    @POST("sendAccountManage/getPossbleBankListDetail")
    Call<ArrayList<RemitBankDetail>> getRemitBankDetail(@Body RequestDataByUserId userId);

    /**
     * ARS 를 위한 정보 제공
     * @param userId
     * @return
     */
    @POST("sendAccountManage/getSendMoneyBasicInfo")
    Call<RemitUserInfo> getSendMoneyBasicInfo(@Body RequestDataByUserId userId);

    /**
     * ARS 요청 및 계좌등록
     * @param requestRemitARS
     * @return
     */
    @POST("sendAccountManage/regArsAndUserAccount")
    Call<ResponseRemitARS> regArsAndUserAccount(@Body RequestDataARS requestRemitARS);

    /**
     * ARS 인증
     * @param requestDataARSCheck
     * @return
     */
    @POST("sendAccountManage/chkArs")
    Call<ResponseRemitARSCheck> chkArs(@Body RequestDataARSCheck requestDataARSCheck);

    /**
     * 송금 암호 등록
     * @param requestDataPW
     * @return
     */
    @POST("sendAccountManage/setSendMoneyPW")
    Call<RemitResult> setSendMoneyPW(@Body RequestDataPW requestDataPW);

    /**
     * 송금 암호 수정
     * @param requestDataPW
     * @return
     */
    @POST("sendAccountManage/chgSendPW")
    Call<RemitResult> chgSendPW(@Body RequestDataPW requestDataPW);

    /**
     * 유저가 송금 기능을 사용중인지 체크
     * @param userId
     * @return
     */
    @POST("sendAccountManage/getChkAccountUse")
    Call<RemitResult> getChkAccountUse(@Body RequestDataByUserId userId);


    /**
     * 송금 메인 조회
     * @param userId
     * @return
     */
    @POST("sendAccountManage/mainPage")
    Call<ResponseRemitAccount> getRemitMain(@Body RequestDataByUserId userId);

    /**
     * 송금계좌편집화면
     * @param userId
     * @return
     */
    @POST("sendAccountManage/editMainPage")
    Call<ResponseRemitAccountEdit> editMainPage(@Body RequestDataByUserId userId);

    /**
     * 등록된 계좌 삭제
     * @param requestDataAccountDel
     * @return
     */
    @POST("sendAccountManage/delUserAccount")
    Call<RemitResult> delUserAccount(@Body RequestDataAccountDel requestDataAccountDel);

    /**
     * 최근 계좌 송금 리스트
     * @param userId
     * @return
     */
    @POST("sendMoney/recentlySendAccountList")
    Call<ResponseRemitAccountCurrent> recentlySendAccountList(@Body RequestDataByUserId userId);

    /**
     * 계좌로 송금
     * @param requestDataSendAccount
     * @return
     */
    @POST("sendMoney/sendMoneyAccount")
    Call<ResponseRemitSend> sendMoneyAccount(@Body RequestDataSendAccount requestDataSendAccount);

    /**
     * 최근 전화 송금 리스트
     * @param userId
     * @return
     */
    @POST("sendMoney/recentlySendTelList")
    Call<ResponseRemitPhoneCurrent> recentlySendTelList(@Body RequestDataByUserId userId);

    /**
     * 전화로 송금
     * @param requestDataSendPhone
     * @return
     */
    @POST("sendMoney/sendMoneyPhoneNo")
    Call<ResponseRemitSend> sendMoneyPhoneNo(@Body RequestDataSendPhone requestDataSendPhone);

    /**
     * 충전
     * @param requestDataChangeMoney
     * @return
     */
    @POST("sendMoney/chargeWallet")
    Call<ResponseRemitChargeMoney> chargeWallet(@Body RequestDataChangeMoney requestDataChangeMoney);

    /**
     * 수신대기 중인 전화 송금 리스트
     * @param userId
     * @return
     */
    @POST("sendMoney/userRcvWaitMoneylistForApp")
    Call<ResponseRemitStandByReceivedMoney> checkReceivedMoney(@Body RequestDataByUserId userId);

    /**
     * 송금 수신 완료
     * @param id
     * @return
     */
    @POST("sendMoney/receiveMoneyForApp")
    Call<ResponseRemitReceived> sendReceivedMoney(@Body RequestRemitReceivedMoney id);
}
