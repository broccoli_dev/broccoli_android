package com.nomadconnection.broccoli.api;

import com.google.gson.JsonElement;
import com.nomadconnection.broccoli.data.Asset.AssetMainData;
import com.nomadconnection.broccoli.data.Asset.AssetOtherInfo;
import com.nomadconnection.broccoli.data.Asset.AssetStockInfo;
import com.nomadconnection.broccoli.data.Asset.DeleteAssetCar;
import com.nomadconnection.broccoli.data.Asset.DeleteAssetEstate;
import com.nomadconnection.broccoli.data.Asset.DeleteAssetStock;
import com.nomadconnection.broccoli.data.Asset.EditAssetCar;
import com.nomadconnection.broccoli.data.Asset.EditAssetCustom;
import com.nomadconnection.broccoli.data.Asset.EditAssetEstate;
import com.nomadconnection.broccoli.data.Asset.EditAssetStock;
import com.nomadconnection.broccoli.data.Asset.RequestAssetBankActive;
import com.nomadconnection.broccoli.data.Asset.RequestAssetBankDetail;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarCode;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarMarketPrice;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarSubCode;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCarYear;
import com.nomadconnection.broccoli.data.Asset.RequestAssetCardDetail;
import com.nomadconnection.broccoli.data.Asset.RequestAssetStockDetail;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetBankDetail;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarMakerCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarMarketPrice;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarSubCode;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCarYear;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCardBill;
import com.nomadconnection.broccoli.data.Asset.ResponseAssetCardDetail;
import com.nomadconnection.broccoli.data.Asset.ResponseCardLoanDetail;
import com.nomadconnection.broccoli.data.Asset.ResponseSearchAssetStock;
import com.nomadconnection.broccoli.data.Asset.ResponseStockDetail;
import com.nomadconnection.broccoli.data.Asset.SearchAssetStock;
import com.nomadconnection.broccoli.data.RequestDataByUserId;
import com.nomadconnection.broccoli.data.Result;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by YelloHyunminJang on 16. 1. 18..
 */
public interface AssetApi {

    /**
     * 자사 메인 조회
     * @return
     */
    @GET("pam/asset/main")
    Call<AssetMainData> getAssetMain();

    /**
     * 은행거래 내역 상세 조회
     * @param object
     * @return
     */
    @POST("pam/asset/bank/detail")
    Call<ArrayList<ResponseAssetBankDetail>> getBankDetail(@Body RequestAssetBankDetail object);

    /**
     * 카드 청구서 리스트 조회
     * @param userId
     * @return
     */
    @POST("pam/asset/card/bill")
    Call<ArrayList<ResponseAssetCardBill>> getCardBill(@Body RequestDataByUserId userId);

    /**
     * 주식 리스트 조회
     * @param userId
     * @return
     */
    @POST("pam/asset/stock/list")
    Call<AssetStockInfo> getStockList(@Body RequestDataByUserId userId);

    /**
     * 주식 내역 상세 조회
     * @param object
     * @return
     */
    @POST("pam/asset/stock/detail")
    Call<ResponseStockDetail> getStockDetail(@Body RequestAssetStockDetail object);

    /**
     * 주식 내역 추가/수정
     * @param object
     * @return
     */
    @POST("pam/asset/stock/save")
    Call<Result> editStock(@Body EditAssetStock object);

    /**
     * 주식 내역 삭제
     * @param object
     * @return
     */
    @POST("pam/asset/stock/delete")
    Call<Result> deleteStock(@Body DeleteAssetStock object);

    /**
     * 주식 종목 검색
     * @param object
     * @return
     */
    @POST("pam/inquiry/stock/find")
    Call<ArrayList<ResponseSearchAssetStock>> searchStock(@Body SearchAssetStock object);
//    Call<ResponseSearchAssetStock> searchStock(@Body SearchAssetStock object);

    /**
     * 기타 재산 조회
     * @param userId
     * @return
     */
    @POST("pam/asset/etc")
    Call<AssetOtherInfo> getOtherList(@Body RequestDataByUserId userId);

    /**
     * 부동산 추가/수정
     * @param object
     * @return
     */
    @POST("pam/asset/real_estate/save")
    Call<Result> editEstate(@Body EditAssetEstate object);

    /**
     * 부동산 내역 삭제
     * @param object
     * @return
     */
    @POST("pam/asset/real_estate/delete")
    Call<Result> deleteEstate(@Body DeleteAssetEstate object);

    // document url: car/save/save
    /**
     * 자동차 추가/수정
     * @param object
     * @return
     */
    @POST("pam/asset/car/save")
    Call<Result> editCar(@Body EditAssetCar object);

    /**
     * 자동차 삭제
     * @param object
     * @return
     */
    @POST("pam/asset/car/delete")
    Call<Result> deleteCar(@Body DeleteAssetCar object);

    /**
     * 대출 상세 조회
     * @param object
     * @return
     */
    @POST("pam/asset/bank/detail")
    Call<ArrayList<ResponseAssetBankDetail>> getLoanDetail(@Body RequestAssetBankDetail object);

    /**
     * 카드론 상세 조회
     * @param body <b>param key(value) : userId(String), companyCode(String)</b>
     */
    @POST("pam/asset/cardloan/list")
    Call<ArrayList<ResponseCardLoanDetail>> getCardLoanDetail(@Body HashMap<String, Object> body);

    /**
     * 예/적금, 대출 계좌 활성 상태 편집
     */
    @POST("pam/asset/bank/active")
    Call<Result> setBankActiveRequest(@Body RequestAssetBankActive object);

    /**
     * 차량 제조사 조회
     * @param userId
     * @return
     */
    @POST("pam/inquiry/car/maker")
    Call<ArrayList<ResponseAssetCarMakerCode>> getCarMakerCode(@Body RequestDataByUserId userId);

    /**
     * 차량 대표차명 조회
     * @param object
     * @return
     */
    @POST("pam/inquiry/car/model")
    Call<ArrayList<ResponseAssetCarCode>> getCarCode(@Body RequestAssetCarCode object);

    /**
     * 차량 세부차명 조회
     * @param object
     * @return
     */
    @POST("pam/inquiry/car/submodel")
    Call<ArrayList<ResponseAssetCarSubCode>> getCarSubCode(@Body RequestAssetCarSubCode object);

    /**
     * 차량 연식 조회
     * @param object
     * @return
     */
    @POST("pam/inquiry/car/year")
    Call<ArrayList<ResponseAssetCarYear>> getCarYear(@Body RequestAssetCarYear object);

    /**
     * 차량 시세 조회
     * @param object
     * @return
     */
    @POST("pam/inquiry/car/marketprice")
    Call<ResponseAssetCarMarketPrice> getCarMarketPrice(@Body RequestAssetCarMarketPrice object);

    /**
     * 기타 재산 추가/편집
     * @param body
     * @return
     */
    @POST("pam/asset/etc/save")
    Call<Result> editCustomAsset(@Body EditAssetCustom body);


    /**
     * 기타 재산 삭제
     * @param body <b>param key(value) : userId(String), etcId(int)</b>
     * @return
     */
    @POST("pam/asset/etc/delete")
    Call<Result> deleteCustomAsset(@Body HashMap<String, Object> body);

    /**
     * 카드 청구내역
     */
    @GET("pam/card/bill/list")
    Call<JsonElement> getCardBillList();

    /**
     * 카드 청구내역 상세
     */
    @POST("pam/card/bill/detail")
    Call<ResponseAssetCardDetail> getCardBillDetail(@Body RequestAssetCardDetail body);
}
