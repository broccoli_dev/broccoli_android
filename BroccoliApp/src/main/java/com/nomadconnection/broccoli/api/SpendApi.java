package com.nomadconnection.broccoli.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Spend.BodySpendCategoryList;
import com.nomadconnection.broccoli.data.Spend.DeleteSpendMonthly;
import com.nomadconnection.broccoli.data.Spend.EditSpendBudget;
import com.nomadconnection.broccoli.data.Spend.EditSpendMonthly;
import com.nomadconnection.broccoli.data.Spend.MemoData;
import com.nomadconnection.broccoli.data.Spend.RequestCardList;
import com.nomadconnection.broccoli.data.Spend.RequestCardMap;
import com.nomadconnection.broccoli.data.Spend.RequestDataByDate;
import com.nomadconnection.broccoli.data.Spend.RequestDataByMonth;
import com.nomadconnection.broccoli.data.Spend.RequestSpendCategoryDetail;
import com.nomadconnection.broccoli.data.Spend.ResponseCardList;
import com.nomadconnection.broccoli.data.Spend.ResponseSpendBudget;
import com.nomadconnection.broccoli.data.Spend.ResponseSpendCategoryDetail;
import com.nomadconnection.broccoli.data.Spend.ResponseSpendDetail;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetData;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetDetail;
import com.nomadconnection.broccoli.data.Spend.SpendBudgetLastExpense;
import com.nomadconnection.broccoli.data.Spend.SpendCardBanner;
import com.nomadconnection.broccoli.data.Spend.SpendMainData;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by YelloHyunminJang on 16. 1. 18..
 */
public interface SpendApi {

    /**
     * 소비 메인 조회
     * @return
     */
    @GET("pam/expense/main")
    Call<SpendMainData> getSpendMain();

    /**
     * 소비 상세 조회
     * @param date
     * @return
     */
    @POST("pam/expense/detail")
    Call<ResponseSpendDetail> getSpendDetail(@Body RequestDataByDate date);

    /**
     * 소비 내역 추가/수정
     * @param edit
     * @return
     */
    @POST("pam/expense/save")
    Call<Result> editSpend(@Body EditSpendMonthly edit);

    /**
     * 소비 내역 삭제
     * @param delete
     * @return
     */
    @POST("pam/expense/delete")
    Call<Result> deleteSpend(@Body DeleteSpendMonthly delete);

    /**
     * 예산 상세 조회
     * @param month
     * @return
     */
    @POST("pam/expense/budget/list")
    Call<ResponseSpendBudget> getBudget(@Body RequestDataByMonth month);

    /**
     * 예산 추가/수정
     * @param edit
     * @return
     */
    @POST("pam/expense/budget/save")
    Call<Result> editBudget(@Body EditSpendBudget edit);

    /**
     * 소비 분류 리스트 조회
     * @param date
     * @return
     */
    @POST("pam/expense/category/list")
    Call<List<BodySpendCategoryList>> getCategory(@Body RequestDataByDate date);

    /**
     * 소비 분류 상세 조회
     * @param category
     * @return
     */
    @POST("pam/expense/category/detail")
    Call<ResponseSpendCategoryDetail> getCategoryDetail(@Body RequestSpendCategoryDetail category);

    /**
     * 카드 리스트 검색
     * @param list
     * @return
     */
    @POST("pam/inquiry/card/list")
    Call<List<ResponseCardList>> getCardList(@Body RequestCardList list);


    /**
     * 카드 리스트 매핑
     * @param map
     * @return
     */
    @POST("pam/expense/card/map")
    Call<Result> setCardMap(@Body RequestCardMap map);

    /**
     * 소비이력 메모 작성
     * @param memo
     * @return
     */
    @POST("pam/expense/memo")
    Call<Result> setMemo(@Body MemoData memo);

    /**
     * 개편 예산 편집
     * @return
     */
    @GET("pam/budget")
    Call<SpendBudgetLastExpense> getBudget();

    /**
     * 개편 예산 등록
     * Parameter <p>baseMonth, totalSum, categoryList(categoryGroup, sum)</p>
     * @return
     */
    @POST("pam/budget")
    Call<Result> setBudget(@Body SpendBudgetData data);

    /**
     * 개편 예산 등록 - 복제
     * @param data
     * @return
     */
    @POST("pam/budget/replication")
    Call<Result> copyBudget(@Body HashMap<String, Object> data);

    /**
     * 개편 예산 수정
     * @param data
     * @return
     */
    @POST("pam/budget/update")
    Call<Result> editBudget(@Body SpendBudgetData data);

    /**
     * 개편 예산 조회
     * @return
     */
    @GET("pam/budget/view")
    Call<JsonElement> getBudgetList();

    /**
     * 카테고리별 전월 소비
     * @return {"list":"ListArray(CategoryGroup)"}
     */
    @GET("pam/budget/expense")
    Call<JsonObject> getCategoryExpense();

    /**
     * 즐겨찾기 조회
     * @return
     */
    @GET("pam/budget/bookmark")
    Call<JsonElement> getFavoriteCategory();

    /**
     * 즐겨찾기 등록
     *
     * @param list <b>param key(value) : categoryIds(ListArray(Long))</b>
     */
    @POST("pam/budget/bookmark")
    Call<Result> setFavoriteCategory(@Body HashMap<String, List<Long>> list);

    /**
     * 예산 상세 카테고리 등록
     * @param list
     * @return
     */
    @POST("pam/budget/detail")
    Call<Result> setDetailCategory(@Body List<SpendBudgetDetail> list);

    /**
     * 예산 초기화
     *
     * @param body <b>param key(value) : budgetId(long)</b>
     * @return
     */
    @POST("pam/budget/delete")
    Call<Result> deleteBudget(@Body HashMap<String, Object> body);

    /**
     * 나의 카드 목록
     * @return
     */
    @GET("pam/mycard")
    Call<JsonElement> getMyCardList();

    /**
     * 나의 카드 상세 정보
     * @param cardId
     * @param code
     * @return
     */
    @GET("pam/mycard/{cardId}/{cardCode}")
    Call<JsonElement> getMyCardDetail(@Path("cardId") String cardId, @Path("cardCode") String code);

    /**
     * 카드 매칭
     * @param body {"cardId":long, "cardCode":"String"}
     * @return
     */
    @POST("pam/mycard/match")
    Call<Result> setMatchingCard(@Body HashMap<String, Object> body);

    /**
     * 메모 등록
     * @param body {"cardId":long, "memo":"String"}
     * @return
     */
    @POST("pam/mycard/memo")
    Call<Result> setCardMemo(@Body HashMap<String, Object> body);

    /**
     * 즐겨찾기 등록 및 해제
     * @param body {"cardId":long, "bookmarkYn":int}
     * @return
     */
    @POST("pam/mycard/bookmark")
    Call<Result> setFavoriteCard(@Body HashMap<String, Object> body);

    /**
     * 비교 대상 카드 리스트 (사용자가 보유하고 있는 카드)
     * @return
     */
    @GET("pam/card/recommend/target")
    Call<JsonElement> getOwnCardList();

    /**
     * 추천 카드 리스트
     * @return
     */
    @GET("pam/card/recommend/list")
    Call<JsonElement> getRecommendList(@Query("cardId") long cardId, @Query("cardType") int cardType, @Query("benefitType") int benefitType, @Query("annualFee") int annualFee);

    /**
     * 카드 혜택 목록
     * @param cardCode
     * @return
     */
    @GET("pam/card/benefit/{cardCode}")
    Call<JsonElement> getCardBenefitList(@Path("cardCode") String cardCode);

    /**
     * 카드 추천 배너
     * @return
     */
    @GET("pam/card/recommend/banner")
    Call<SpendCardBanner> getRecommendBanner();
}
