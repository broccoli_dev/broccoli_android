package com.nomadconnection.broccoli.config;

import android.content.Context;

public class DB 
{
	/* DB Name */
	public static final String		NAME					= "broccoli.db";
	
	/* Table Name */
	public static final String		TN_EVENT				= "event";
	public static final String		TN_NOTICE				= "notice";
	public static final String 		TN_TABLE1 				= "broccolitable1";	// table 1
	public static final String 		TN_TABLE2 				= "broccolitable2";	// table 2
	
	public static final String		INSTALL_PATH(Context _context) {
		return _context.getFilesDir().getParentFile().getPath()+"/databases/";
	}	
	
	
	/* Column */
//	public static final String KEY_ROWID 					= "_id";
//	
//	public static final String KEY_IDX	 					= "idx";
//	public static final String KEY_CATEGORY 				= "category";
//	public static final String KEY_TITLE 					= "title";
//	public static final String KEY_CONTENT 					= "content";
//	public static final String KEY_PHONENUM 				= "phonenum";
//	public static final String KEY_GCM 						= "gcm";
	
}
