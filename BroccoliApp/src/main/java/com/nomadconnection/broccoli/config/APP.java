package com.nomadconnection.broccoli.config;

/*
 * APP 사용 변수 모음
*/
public class APP {

	public static final boolean _SAMPLE_MODE_ = false;
	
	/* Debug Mode */
	public static final boolean _DEBUG_MODE_ 	= true;

//=========================================================================================//
// App Step Check
//=========================================================================================//
	public static final int	APP_STEP_CHECK_DEFAULT_TIME	= 2800;
	public static final int	APP_STEP_CHECK_MAX_TIME		= 100000;
	public static final int	APP_STEP_CHECK_RUN			= 77;
	public static final int	APP_STEP_CHECK_TIMEOVER		= 78;


//=========================================================================================//
// App only one check flag
//=========================================================================================//
	public static final String	SP_ONLY_ONE_CHECK_ASSET_CARD_DETAIL_ENTER	= "sp_only_one_check_asset_card_detail_enter";
	public static final String	SP_ONLY_ONE_CHECK_MOBILE_DATA_ENTER	= "sp_only_one_check_mobile_data_enter";
	public static final String	SP_CHECK_SCRAPING_DATA_TIME	= "sp_check_scraping_data_time";
	public static final String	SP_CHECK_MAIN_NOTICE_ID	= "sp_check_main_notice_id";
	public static final String	SP_CHECK_MAIN_NOTICE	= "sp_check_main_notice";
	public static final String	SP_CHECK_MAIN_NOTICE_DATA	= "sp_check_main_notice_data";


//=========================================================================================//
// CustomPageSlicingTabStripSide type
//=========================================================================================//
	public static final String	CUSTOM_PAGE_SLIDING_TAB_STRIP_SIDE_TYPE_CARD	= "custom_page_sliding_tab_strip_side_type_card";
	public static final String	CUSTOM_PAGE_SLIDING_TAB_STRIP_SIDE_TYPE_ELSE	= "custom_page_sliding_tab_strip_side_type_else";

//=========================================================================================//
// Main List Chart Use
//=========================================================================================//
	public static final String	MAIN_LIST_CHART_USE_1	= "main_list_chart_use_1";
	public static final String	MAIN_LIST_CHART_USE_2	= "main_list_chart_use_2";
	public static final String	MAIN_LIST_CHART_USE_3	= "main_list_chart_use_3";
	public static final String	MAIN_LIST_CHART_USE_4	= "main_list_chart_use_4";


//=========================================================================================//
// Chart Use
//=========================================================================================//
	public static final String	ASSET_STOCK_HISTORY_CHART_USE	= "asset_stock_history_chart_use";


//=========================================================================================//
// Main List Card Use
//=========================================================================================//
	public static final String	MAIN_LIST_CARD_USE_1	= "main_list_tab_1";
	public static final String	MAIN_LIST_CARD_USE_2	= "main_list_tab_2";
	public static final String	MAIN_LIST_CARD_USE_3	= "main_list_tab_3";
	public static final String	MAIN_LIST_CARD_USE_4	= "main_list_tab_4";


//=========================================================================================//
// User Info
//=========================================================================================//
	public static final String	SP_USER_ID				= "user_id";	
	public static final String	SP_USER_PW				= "user_pw";
	public static final String	SP_USER_GCM				= "user_gcm";
	public static final String	SP_USER_TOKEN			= "user_token";
	public static final String	SP_USER_ADID			= "user_adid";
	
//=========================================================================================//
// Batch Service
//=========================================================================================//
	public static final long	BATCH_SERVICE_REFRESH_TIME	= 7*1000L;
	public static final String	BATCH_SERVICE_DAILY_RECORD	= "batch_daily";
	
//=========================================================================================//
// Dialog Base
//=========================================================================================//
	public static final int	DIALOG_BASE_ONE_BTN_CLICK	= 20;
	public static final int	DIALOG_BASE_TWO_BTN_CANCEL	= 21;
	public static final int	DIALOG_BASE_TWO_BTN_CONFIRM	= 22;
	public static final int	DIALOG_BASE_GRID_OR_LIST_G	= 23;
	public static final int	DIALOG_BASE_GRID_OR_LIST_L	= 24;
	public static final int	DIALOG_ASSET_OTHER_HOUSE	= 25;
	public static final int	DIALOG_ASSET_OTHER_CAR		= 26;
	public static final int	DIALOG_ASSET_OTHER_CUSTOM	= 27;
	public static final int	DIALOG_SPEND_CARD_INFO_L	= 28;
	public static final int	DIALOG_CHALLENGE_TYPE_L		= 29;
	public static final int	DIALOG_BASE_ONE_BTN_NO_TITLE_CLICK	= 29;
	public static final int	DIALOG_BASE_LIST_LARGE	= 30;
	public static final int	DIALOG_BASE_LIST_SMALL	= 31;


//=========================================================================================//
// Pull to Refresh GetDataTask Handler
//=========================================================================================//
	public static final int PTR_GETDATATASK_HANDLER_SUCCESS		= 1001;
	public static final int PTR_GETDATATASK_HANDLER_FAIL		= 1002;
			
//=========================================================================================//
// Pull to Refresh GetDataTask Handler
//=========================================================================================//
	public static final String INFO_DATA_SETTING_NOTICE_PARCEL = "info_data_setting_notice_parcel";
	
//=========================================================================================//
// PUT Extra
//=========================================================================================//
	public static final String SETTING_CONNECT_FI_DETAIL = "setting_connect_fi_detail";
	public static final String SETTING_CONNECT_FI_REGISTER_STATUS = "setting_connect_fi_register_status";

	public static final String INFO_DATA_ASSET_DAS_DEPTH_1 = "info_data_asset_das_depth_1_parcel";
	public static final String INFO_DATA_ASSET_STOCK_DEPTH_1 = "info_data_asset_stock_depth_1_parcel";
	public static final String INFO_DATA_ASSET_LOAN_DEPTH_1 = "info_data_asset_loan_depth_1_parcel";
	public static final String INFO_DATA_ASSET_OTHER_DEPTH_1 = "info_data_asset_other_depth_1_parcel";
	public static final String INFO_DATA_ASSET_OTHER_HOUSE_DEPTH_1 = "info_data_asset_other_house_depth_1_parcel";
	public static final String INFO_DATA_ASSET_OTHER_CAR_DEPTH_1 = "info_data_asset_other_car_depth_1_parcel";

	public static final String INFO_DATA_ASSET_DAS_DEPTH_2_HEADER = "info_data_asset_das_depth_2_header_parcel";
	public static final String INFO_DATA_ASSET_DAS_DEPTH_2_BODY = "info_data_asset_das_depth_2_body_parcel";
	public static final String INFO_DATA_ASSET_CREDIT_DEPTH_2_HEADER = "info_data_asset_credit_depth_2_header_parcel";
	public static final String INFO_DATA_ASSET_CREDIT_DEPTH_2_BODY = "info_data_asset_credit_depth_2_body_parcel";
	public static final String INFO_DATA_ASSET_STOCK_DEPTH_2_HEADER = "info_data_asset_stock_depth_2_header_parcel";
	public static final String INFO_DATA_ASSET_STOCK_DEPTH_2_BODY = "info_data_asset_stock_depth_2_body_parcel";
	public static final String INFO_DATA_ASSET_LOAN_DEPTH_2_HEADER = "info_data_asset_loan_depth_2_header_parcel";
	public static final String INFO_DATA_ASSET_LOAN_DEPTH_2_BODY = "info_data_asset_loan_depth_2_body_parcel";
	public static final String INFO_DATA_ASSET_OTHER_HOUSE_OR_CAR_DEPTH_2 = "info_data_asset_other_house_or_car_depth_2_parcel";
	public static final String INFO_DATA_ASSET_OTHER_HOUSE_DEPTH_2 = "info_data_asset_other_house_depth_2_parcel";
	public static final String INFO_DATA_ASSET_OTHER_CAR_DEPTH_2 = "info_data_asset_other_car_depth_2_parcel";

	public static final String INFO_DATA_ASSET_DAS_PARCEL = "info_data_asset_das_parcel";
	public static final String INFO_DATA_ASSET_CREDIT_PARCEL = "info_data_asset_credit_parcel";
	public static final String INFO_DATA_ASSET_STOCK_PARCEL = "info_data_asset_stock_parcel";
	public static final String INFO_DATA_ASSET_LOAN_PARCEL = "info_data_asset_loan_parcel";
	public static final String INFO_DATA_ASSET_OTHER_PARCEL = "info_data_asset_other_parcel";
	public static final String INFO_DATA_ASSET_STOCK_HISTORY_PARCEL = "info_data_asset_stock_history_parcel";

	public static final String INFO_DATA_OTHER_MINI_DEPTH_1 = "info_data_other_mini_depth_1_parcel";
	public static final String INFO_DATA_OTHER_MINI_DEPTH_2 = "info_data_other_mini_depth_2_parcel";

	public static final String INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND = "INFO_DATA_INVEST_STOCK_TITLE_BACKGROUND";

//=========================================================================================//
// GCM
//=========================================================================================//
	public static final String	SP_SETTING_1			= "message_receive_on";		// GCM 메시지받기 On/Off
	public static final String	SP_SETTING_2			= "message_alarm_type";		// GCM 메시지 알림음(1:소리,2:진동,기타:둘다)


//=========================================================================================//
// GENERAL SETTING
//=========================================================================================//
	/**
	 * DB Migrations
	 */
	public static final String SETTING_MIGRATION_VER = "setting_migration_ver";

	public static final String SETTING_PASSOWORD_CHECK_COUNT = "SETTING_PASSOWORD_CHECK_COUNT";
	public static final String SETTING_PASSOWORD_DELAY_TIME = "SETTING_PASSOWORD_DELAY_TIME";
	public static final String SETTING_PASSWORD_OVER_TIME = "SETTING_PASSWORD_OVER";

	public static final String SETTING_CERT_CHECK_COUNT = "SETTING_CERT_CHECK_COUNT";
	public static final String SETTING_CERT_OVER_TIME = "SETTING_CERT_OVER_TIME";
	public static final String SETTING_CERT_OVER_COUNT = "SETTING_CERT_OVER_COUNT";

	public static final String SETTING_SENDED_PUSH = "SETTING_SENDED_PUSH";

	/**
	 * public key
	 */
	public static final String PUBLIC_KEY = "PUBLIC_KEY";

	/**
	 * 지문
	 */
	public static final String SETTING_BROCCOLI_FINGERPRINT = "SETTING_BROCCOLI_FINGERPRINT";

//=========================================================================================//
// Activity For Result
//=========================================================================================//
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_REQUEST_CODE 				=  2000;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_SUCCESS			=  2001;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_CODE_CANCLE			=  2002;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_REQUEST_CODE 						=  3000;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_RESULT_CODE_SUCCESS					=  3001;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_RESULT_CODE_CANCLE					=  3002;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_RESULT_CODE_DELETE					=  3003;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_REQUEST_CODE 				=  4000;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_SUCCESS		=  4001;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_CANCLE			=  4002;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_STOCK_HISTORY_RESULT_CODE_DELETE			=  4003;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_OTHER_REQUEST_CODE 						=  5000;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_SUCCESS					=  5001;
	public static final int ACTIVIYT_FOR_RESULT_ASSET_OTHER_RESULT_CODE_CANCLE					=  5002;
	public static final int ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_REQUEST_CODE 				=  6000;
	public static final int ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_RESULT_CODE_SUCCESS 		=  6001;
	public static final int ACTIVIYT_FOR_RESULT_OTHER_MONEY_DETAIL_RESULT_CODE_CANCLE			=  6002;
	public static final String ACTIVIYT_FOR_RESULT_ASSET_STOCK_SEARCH_RESULT_DATA 				= "SearchReturnDate";


	public static final int ACTIVIYT_FOR_RESULT_MAIN_1_REQUEST_CODE 								=  1100;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_1_RESULT_CODE_SUCCESS 						=  1101;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_1_RESULT_CODE_CANCLE						=  1102;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_2_REQUEST_CODE 								=  1200;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_2_RESULT_CODE_SUCCESS 						=  1201;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_2_RESULT_CODE_CANCLE						=  1202;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_3_REQUEST_CODE 								=  1300;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_3_RESULT_CODE_SUCCESS 						=  1301;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_3_RESULT_CODE_CANCLE						=  1302;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_4_REQUEST_CODE 								=  1400;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_4_RESULT_CODE_SUCCESS 						=  1401;
	public static final int ACTIVIYT_FOR_RESULT_MAIN_4_RESULT_CODE_CANCLE						=  1402;

	public static final String APP_CODE = "SEED_CODE_BROCCOLI_2016";
	public static final String PROPERTY_DEVICE_ID = "PROPERTY_DEVICE_ID";

//=========================================================================================//
// Remit page status
//=========================================================================================//
	public static final int REMIT_SEND_PHONE 	=  10;
	public static final int REMIT_SEND_ACCOUNT 	=  11;
	public static final int REMIT_CHANGE_MONEY 	=  12;
}
