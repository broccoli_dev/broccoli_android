package com.nomadconnection.broccoli.collector.data;

public class AccountInfo  {

    public AccountInfo() {
    }

    public AccountInfo(AccountInfo info) {
        fCode = info.fCode;
        cert = info.cert;
        password = info.password;
    }

    private String fCode;

    private String cert;

    private String password;

    public String getFCode() {
        return fCode;
    }

    public void setFCode(String fcode) {
        this.fCode = fcode;
    }

    public String getCert() {
        return cert;
    }

    public void setCert(String cert) {
        this.cert = cert;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "fCode:" + fCode + " cert:" + cert + " password:" + password;
    }
}
