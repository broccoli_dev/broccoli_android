//package com.nomadconnection.broccoli.collector;
//
//import android.app.IntentService;
//import android.content.Intent;
//import android.os.Handler;
//import android.os.Message;
//
//import com.google.gson.FieldNamingStrategy;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.kwic.saib.pub.SmartAIB;
//import com.nomadconnection.broccoli.collector.data.AccountInfo;
//import com.nomadconnection.broccoli.collector.data.SaibRequest;
//
//import java.lang.reflect.Field;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Locale;
//import java.util.Queue;
//import java.util.concurrent.ConcurrentLinkedQueue;
//
//import io.realm.Realm;
//import io.realm.RealmObject;
//import io.realm.RealmResults;
//import io.realm.exceptions.RealmException;
//import io.realm.exceptions.RealmPrimaryKeyConstraintException;
//import io.whylee.whylib.util.Logger;
//
///**
// * Created by whylee on 2015. 10. 26..
// */
//public class CollectorService extends IntentService {
//    private static final String TAG = CollectorService.class.getSimpleName();
//
//    Queue<AccountInfo> queue = new ConcurrentLinkedQueue<>();
//    private SaibClient client;
//
//    public CollectorService() {
//        super(CollectorService.class.getName());
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        Logger.d(TAG, "onStartCommand");
////
//        Realm realm = Realm.getInstance(this);
////
////        realm.beginTransaction();
////        try {
////            AccountInfo accountInfo = realm.createObject(AccountInfo.class);
////            accountInfo.setFCode("MBHN");
////            accountInfo.setCert("CN=이용우(LEE YONG W)008104920140715181000240,OU=HNB,OU=personal4IB,O=yessign,C=kr");
////            accountInfo.setPassword("qrey1436");
////        } catch (RealmPrimaryKeyConstraintException e) {
////            Logger.d(TAG, "onStartCommand", e);
////        }
////
////        realm.commitTransaction();
//
//
////
////        for (AccountInfo info : result) {
////            queue.add(new AccountInfo(info));
////        }
//
////        AccountInfo accountInfo = new AccountInfo();
////        accountInfo.setFCode("MBWR");
////        accountInfo.setCert("CN=이용우(LEE YONG W)008104920140715181000240,OU=HNB,OU=personal4IB,O=yessign,C=kr");
////        accountInfo.setPassword("qrey1436");
////        queue.add(accountInfo);
////
////
////
////        client = new SaibClient.Builder(this)
////                .accountInfo(accountInfo)
////                .from(System.currentTimeMillis() - (60 * 24 * 60 * 60 * 1000))
////                .to(System.currentTimeMillis())
////                .build();
////        client.request();
//
//        Gson gson = new GsonBuilder()
//                .setFieldNamingStrategy(new FieldNamingStrategy() {
//                    @Override
//                    public String translateName(Field f) {
//                        return f.getName().toUpperCase();
//                    }
//                })
//                .create();
//
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
//
//        SaibRequest request = new SaibRequest();
//        request.module = "1,3";
//        request.fCode = "MBWR";
//        request.loginMethod = "0";
//        request.certName = "CN=이용우(LEE YONG W)008104920140715181000240,OU=HNB,OU=personal4IB,O=yessign,C=kr";
//        request.certPwd = "qrey1436";
//        request.number = "5462520100505772";
//        request.startDate = "20151106";
//
//        request.endDate = "20151113";
//
//
//        String requestString = gson.toJson(request);
//        Logger.d(TAG, "Request:" + requestString);
//        new SmartAIB(this, new SmartAIB.OnCompleteListener() {
//            @Override
//            public void onComplete(String s) {
//
//                for (int i = 0; i < Math.ceil(s.length() / 400f); i++) {
//                    Logger.d(TAG, s.substring(i * 400, Math.min((i + 1) * 400, s.length())));
//                }
//            }
//        }).AIB_Execute(requestString);
//
//
//        return super.onStartCommand(intent, flags, startId);
//
//
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//        Logger.d(TAG, "onHandleIntent ");
//
//
////
////        Logger.d(TAG, "onHandleIntent queue.size:" + queue.size());
////
//        AccountInfo info;
////        while ((info = queue.poll()) != null) {
//
//
////        }
//    }
//}
