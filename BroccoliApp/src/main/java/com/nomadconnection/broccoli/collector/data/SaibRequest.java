package com.nomadconnection.broccoli.collector.data;

public class SaibRequest {
    public String module;
    public String fCode;
    public String loginMethod;
    public String number;
    public String regNumber;
    public String certName;
    public String certPwd;
    public String startDate;
    public String endDate;
}
