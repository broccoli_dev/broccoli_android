//package com.nomadconnection.broccoli.collector;
//
//import android.content.Context;
//import android.os.Handler;
//import android.os.Message;
//
//import com.google.gson.FieldNamingStrategy;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.kwic.saib.pub.SmartAIB;
//import com.nomadconnection.broccoli.collector.data.AccountInfo;
//import com.nomadconnection.broccoli.collector.data.SaibRequest;
//
//import java.lang.ref.WeakReference;
//import java.lang.reflect.Field;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Locale;
//import java.util.Map;
//
//import io.whylee.whylib.util.Logger;
//
//public class SaibClient {
//    private static final String TAG = SaibClient.class.getSimpleName();
//
//    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
//    private static final Gson gson = new GsonBuilder()
//            .setFieldNamingStrategy(new UpperNamingStrategy())
//            .create();
//
//    private SmartAIB saib;
//    private final SaibRequest request;
//
//    private SaibClient(Builder builder) {
//        saib = new SmartAIB(builder.context, new ResultHandler(this));
//
//        request = new SaibRequest();
//        request.module = "3";
//        request.fCode = builder.accountInfo.getFCode();
//        request.loginMethod = "0";
//        request.certName = builder.accountInfo.getCert();
//        request.certPwd = builder.accountInfo.getPassword();
//        request.startDate = dateFormat.format(new Date(builder.from));
//        request.endDate = dateFormat.format(new Date(builder.to));
//
//        Logger.d(TAG, "request.certName:" + builder.accountInfo);
//
//    }
//
//
//    public synchronized void request() {
//        String requestString = gson.toJson(request);
//
//        Logger.d(TAG, "request-requestString:" + requestString);
//
//        saib.AIB_Execute(requestString);
////        try {
////            wait();
////        } catch (InterruptedException ignored) {
////        }
//        Logger.d(TAG, "request-end");
//    }
//
//    private synchronized void onResult(Map map) {
//        notify();
//    }
//
//    private static class ResultHandler extends Handler {
//        WeakReference<SaibClient> wrapperRef;
//
//        ResultHandler(SaibClient client) {
//            wrapperRef = new WeakReference<>(client);
//        }
//
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            Logger.d(TAG, "handleMessage");
//
//            SaibClient client = wrapperRef.get();
//            if (client != null) {
//                Map resultMap = (Map) msg.obj;
//                client.onResult(resultMap);
//
//                for (Object key : resultMap.keySet()) {
//                    Logger.d(TAG, "ResultHandler key:" + key + " value:" + resultMap.get(key));
//                }
//
//            }
//        }
//    }
//
//    private static class UpperNamingStrategy implements FieldNamingStrategy {
//
//        @Override
//        public String translateName(Field f) {
//            return f.getName().toUpperCase();
//        }
//    }
//
//    public static class Builder {
//        private Context context;
//        private AccountInfo accountInfo;
//        private long from;
//        private long to;
//
//
//        public Builder(Context context) {
//            this.context = context;
//        }
//
//        public Builder accountInfo(AccountInfo accountInfo) {
//            this.accountInfo = accountInfo;
//            return this;
//        }
//
//        public Builder from(long timeInMilli) {
//            from = timeInMilli;
//            return this;
//        }
//
//        public Builder to(long timeInMilli) {
//            to = timeInMilli;
//            return this;
//        }
//
//        public SaibClient build() {
//            return new SaibClient(this);
//        }
//    }
//
//
//}
