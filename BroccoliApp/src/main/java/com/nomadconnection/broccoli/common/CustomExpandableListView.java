package com.nomadconnection.broccoli.common;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.ReflectionUtils;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;

/**
 * Created by YelloHyunminJang on 2016. 10. 24..
 */

public class CustomExpandableListView extends ExpandableListView {

    private static final int[] EMPTY_STATE_SET = new int[0];
    private static final int[] GROUP_EXPANDED_STATE_SET = new int[]{16842920};
    private static final int[] GROUP_EMPTY_STATE_SET = new int[]{16842921};
    private static final int[] GROUP_EXPANDED_EMPTY_STATE_SET = new int[]{16842920, 16842921};
    private static final int[][] GROUP_STATE_SETS;
    private WrapperExpandableListAdapter mAdapter;
    private DataSetObserver mDataSetObserver;
    private OnScrollListener mOnScrollListener;
    private boolean mFloatingGroupEnabled = true;
    private View mFloatingGroupView;
    private int mFloatingGroupPosition;
    private int mFloatingGroupScrollY;
    private FloatingGroupExpandableListView.OnScrollFloatingGroupListener mOnScrollFloatingGroupListener;
    private OnGroupClickListener mOnGroupClickListener;
    private int mWidthMeasureSpec;
    private Object mViewAttachInfo;
    private boolean mHandledByOnInterceptTouchEvent;
    private boolean mHandledByOnTouchEvent;
    private Runnable mOnClickAction;
    private GestureDetector mGestureDetector;
    private boolean mSelectorEnabled;
    private boolean mShouldPositionSelector;
    private boolean mDrawSelectorOnTop;
    private Drawable mSelector;
    private int mSelectorPosition;
    private final Rect mSelectorRect = new Rect();
    private Runnable mPositionSelectorOnTapAction;
    private Runnable mClearSelectorOnTapAction;
    private final Rect mIndicatorRect = new Rect();
    private OnFloatingGroupChangeListener mOnFloatingGroupChangeListener;

    public interface OnFloatingGroupChangeListener {
        void onFloatingGroupChangeListener(View current, View next, int scroll);
    }

    public CustomExpandableListView(Context context) {
        super(context);
        init();
    }

    public CustomExpandableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomExpandableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        super.setOnScrollListener(new OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if(CustomExpandableListView.this.mOnScrollListener != null) {
                    CustomExpandableListView.this.mOnScrollListener.onScrollStateChanged(view, scrollState);
                }

            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(CustomExpandableListView.this.mOnScrollListener != null) {
                    CustomExpandableListView.this.mOnScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
                }

                if(CustomExpandableListView.this.mFloatingGroupEnabled && CustomExpandableListView.this.mAdapter != null && CustomExpandableListView.this.mAdapter.getGroupCount() > 0 && visibleItemCount > 0) {
                    CustomExpandableListView.this.createFloatingGroupView(firstVisibleItem);
                }

            }
        });
        this.mOnClickAction = new Runnable() {
            public void run() {
                boolean allowSelection = true;
                if(CustomExpandableListView.this.mOnGroupClickListener != null) {
                    allowSelection = !CustomExpandableListView.this.mOnGroupClickListener.onGroupClick(CustomExpandableListView.this, CustomExpandableListView.this.mFloatingGroupView, CustomExpandableListView.this.mFloatingGroupPosition, CustomExpandableListView.this.mAdapter.getGroupId(CustomExpandableListView.this.mFloatingGroupPosition));
                }

                if(allowSelection) {
                    if(CustomExpandableListView.this.mAdapter.isGroupExpanded(CustomExpandableListView.this.mFloatingGroupPosition)) {
                        CustomExpandableListView.this.collapseGroup(CustomExpandableListView.this.mFloatingGroupPosition);
                    } else {
                        CustomExpandableListView.this.expandGroup(CustomExpandableListView.this.mFloatingGroupPosition);
                    }

                    CustomExpandableListView.this.setSelectedGroup(CustomExpandableListView.this.mFloatingGroupPosition);
                }

            }
        };
        this.mPositionSelectorOnTapAction = new Runnable() {
            public void run() {
                CustomExpandableListView.this.positionSelectorOnFloatingGroup();
                CustomExpandableListView.this.setPressed(true);
                if(CustomExpandableListView.this.mFloatingGroupView != null) {
                    CustomExpandableListView.this.mFloatingGroupView.setPressed(true);
                }

            }
        };
        this.mClearSelectorOnTapAction = new Runnable() {
            public void run() {
                CustomExpandableListView.this.setPressed(false);
                if(CustomExpandableListView.this.mFloatingGroupView != null) {
                    CustomExpandableListView.this.mFloatingGroupView.setPressed(false);
                }

                CustomExpandableListView.this.invalidate();
            }
        };
        this.mGestureDetector = new GestureDetector(this.getContext(), new GestureDetector.SimpleOnGestureListener() {
            public void onLongPress(MotionEvent e) {
                if(CustomExpandableListView.this.mFloatingGroupView != null && !CustomExpandableListView.this.mFloatingGroupView.isLongClickable()) {
                    ExpandableListContextMenuInfo contextMenuInfo = new ExpandableListContextMenuInfo(CustomExpandableListView.this.mFloatingGroupView, ExpandableListView.getPackedPositionForGroup(CustomExpandableListView.this.mFloatingGroupPosition), CustomExpandableListView.this.mAdapter.getGroupId(CustomExpandableListView.this.mFloatingGroupPosition));
                    ReflectionUtils.setFieldValue(AbsListView.class, "mContextMenuInfo", CustomExpandableListView.this, contextMenuInfo);
                    CustomExpandableListView.this.showContextMenu();
                }

            }
        });
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(this.mAdapter != null && this.mDataSetObserver != null) {
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
            this.mDataSetObserver = null;
        }

    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.mWidthMeasureSpec = widthMeasureSpec;
    }

    protected void dispatchDraw(Canvas canvas) {
        if(Build.VERSION.SDK_INT >= 14) {
            this.mSelectorPosition = ((Integer)ReflectionUtils.getFieldValue(AbsListView.class, "mSelectorPosition", this)).intValue();
        } else {
            this.mSelectorPosition = ((Integer)ReflectionUtils.getFieldValue(AbsListView.class, "mMotionPosition", this)).intValue();
        }

        this.mSelectorRect.set((Rect)ReflectionUtils.getFieldValue(AbsListView.class, "mSelectorRect", this));
        if(!this.mDrawSelectorOnTop) {
            this.drawDefaultSelector(canvas);
        }

        super.dispatchDraw(canvas);
        if(this.mFloatingGroupEnabled && this.mFloatingGroupView != null) {
            if(!this.mDrawSelectorOnTop) {
                this.drawFloatingGroupSelector(canvas);
            }

            canvas.save();
            canvas.clipRect(this.getPaddingLeft(), this.getPaddingTop(), this.getWidth() - this.getPaddingRight(), this.getHeight() - this.getPaddingBottom());
            if(this.mFloatingGroupView.getVisibility() == View.VISIBLE) {
                this.drawChild(canvas, this.mFloatingGroupView, this.getDrawingTime());
            }

            this.drawFloatingGroupIndicator(canvas);
            canvas.restore();
            if(this.mDrawSelectorOnTop) {
                this.drawDefaultSelector(canvas);
                this.drawFloatingGroupSelector(canvas);
            }
        }

    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getAction() & 255;
        if(action == 0 || action == 3) {
            this.mHandledByOnInterceptTouchEvent = false;
            this.mHandledByOnTouchEvent = false;
            this.mShouldPositionSelector = false;
        }

        if(!this.mHandledByOnInterceptTouchEvent && !this.mHandledByOnTouchEvent && this.mFloatingGroupView != null) {
            int[] screenCoords = new int[2];
            this.getLocationInWindow(screenCoords);
            RectF floatingGroupRect = new RectF((float)(screenCoords[0] + this.mFloatingGroupView.getLeft()), (float)(screenCoords[1] + this.mFloatingGroupView.getTop()), (float)(screenCoords[0] + this.mFloatingGroupView.getRight()), (float)(screenCoords[1] + this.mFloatingGroupView.getBottom()));
            if(floatingGroupRect.contains(ev.getRawX(), ev.getRawY())) {
                if(this.mSelectorEnabled) {
                    switch(action) {
                        case 0:
                            this.mShouldPositionSelector = true;
                            this.removeCallbacks(this.mPositionSelectorOnTapAction);
                            this.postDelayed(this.mPositionSelectorOnTapAction, (long) ViewConfiguration.getTapTimeout());
                            break;
                        case 1:
                            this.positionSelectorOnFloatingGroup();
                            this.setPressed(true);
                            if(this.mFloatingGroupView != null) {
                                this.mFloatingGroupView.setPressed(true);
                            }

                            this.removeCallbacks(this.mClearSelectorOnTapAction);
                            this.postDelayed(this.mClearSelectorOnTapAction, (long)ViewConfiguration.getPressedStateDuration());
                    }
                }

                if(this.mFloatingGroupView.dispatchTouchEvent(ev)) {
                    this.mGestureDetector.onTouchEvent(ev);
                    this.onInterceptTouchEvent(ev);
                    return true;
                }
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        this.mHandledByOnInterceptTouchEvent = super.onInterceptTouchEvent(ev);
        return this.mHandledByOnInterceptTouchEvent;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        this.mHandledByOnTouchEvent = super.onTouchEvent(ev);
        return this.mHandledByOnTouchEvent;
    }

    public void setSelector(Drawable sel) {
        super.setSelector(new ColorDrawable(0));
        if(this.mSelector != null) {
            this.mSelector.setCallback((Drawable.Callback)null);
            this.unscheduleDrawable(this.mSelector);
        }

        this.mSelector = sel;
        this.mSelector.setCallback(this);
    }

    public void setDrawSelectorOnTop(boolean onTop) {
        super.setDrawSelectorOnTop(onTop);
        this.mDrawSelectorOnTop = onTop;
    }

    public void setAdapter(ExpandableListAdapter adapter) {
        if(!(adapter instanceof WrapperExpandableListAdapter)) {
            throw new IllegalArgumentException("The adapter must be an instance of WrapperExpandableListAdapter");
        } else {
            this.setAdapter((WrapperExpandableListAdapter)adapter);
        }
    }

    public void setAdapter(WrapperExpandableListAdapter adapter) {
        super.setAdapter(adapter);
        if(this.mAdapter != null && this.mDataSetObserver != null) {
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
            this.mDataSetObserver = null;
        }

        this.mAdapter = adapter;
        if(this.mAdapter != null && this.mDataSetObserver == null) {
            this.mDataSetObserver = new DataSetObserver() {
                public void onChanged() {
                    CustomExpandableListView.this.mFloatingGroupView = null;
                }

                public void onInvalidated() {
                    CustomExpandableListView.this.mFloatingGroupView = null;
                }
            };
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
        }

    }

    public void setOnScrollListener(OnScrollListener listener) {
        this.mOnScrollListener = listener;
    }

    public void setOnGroupClickListener(OnGroupClickListener onGroupClickListener) {
        super.setOnGroupClickListener(onGroupClickListener);
        this.mOnGroupClickListener = onGroupClickListener;
    }

    public void setFloatingGroupEnabled(boolean floatingGroupEnabled) {
        this.mFloatingGroupEnabled = floatingGroupEnabled;
    }

    public void setOnScrollFloatingGroupListener(FloatingGroupExpandableListView.OnScrollFloatingGroupListener listener) {
        this.mOnScrollFloatingGroupListener = listener;
    }

    public void setOnFloatingGroupChangeListener(OnFloatingGroupChangeListener listener) {
        mOnFloatingGroupChangeListener = listener;
    }

    private void createFloatingGroupView(int position) {
        this.mFloatingGroupView = null;
        this.mFloatingGroupPosition = getPackedPositionGroup(this.getExpandableListPosition(position));

        int floatingGroupFlatPosition;
        for(floatingGroupFlatPosition = 0; floatingGroupFlatPosition < this.getChildCount(); ++floatingGroupFlatPosition) {
            View floatingGroupListPosition = this.getChildAt(floatingGroupFlatPosition);
            Object params = floatingGroupListPosition.getTag(com.diegocarloslima.fgelv.lib.R.id.fgelv_tag_changed_visibility);
            if(params instanceof Boolean) {
                boolean childWidthSpec = ((Boolean)params).booleanValue();
                if(childWidthSpec) {
                    floatingGroupListPosition.setVisibility(View.VISIBLE);
                    floatingGroupListPosition.setTag(com.diegocarloslima.fgelv.lib.R.id.fgelv_tag_changed_visibility, (Object)null);
                }
            }
        }

        if(this.mFloatingGroupEnabled) {
            floatingGroupFlatPosition = this.getFlatListPosition(getPackedPositionForGroup(this.mFloatingGroupPosition));
            int var15 = floatingGroupFlatPosition - position;
            if(var15 >= 0 && var15 < this.getChildCount()) {
                View var16 = this.getChildAt(var15);
                if(var16.getTop() >= this.getPaddingTop()) {
                    return;
                }

                if(var16.getTop() < this.getPaddingTop() && var16.getVisibility() == VISIBLE) {
                    var16.setVisibility(View.INVISIBLE);
                    var16.setTag(com.diegocarloslima.fgelv.lib.R.id.fgelv_tag_changed_visibility, Boolean.valueOf(true));
                }
            }

            if(this.mFloatingGroupPosition >= 0) {
                this.mFloatingGroupView = this.mAdapter.getGroupView(this.mFloatingGroupPosition, this.mAdapter.isGroupExpanded(this.mFloatingGroupPosition), this.mFloatingGroupView, this);
                if(!this.mFloatingGroupView.isClickable()) {
                    this.mSelectorEnabled = true;
                    this.mFloatingGroupView.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            CustomExpandableListView.this.postDelayed(CustomExpandableListView.this.mOnClickAction, (long)ViewConfiguration.getPressedStateDuration());
                        }
                    });
                } else {
                    this.mSelectorEnabled = false;
                }

                this.loadAttachInfo();
                this.setAttachInfo(this.mFloatingGroupView);
            }

            if(this.mFloatingGroupView != null) {
                LayoutParams var17 = (LayoutParams)this.mFloatingGroupView.getLayoutParams();
                if(var17 == null) {
                    var17 = new LayoutParams(-1, -2, 0);
                    this.mFloatingGroupView.setLayoutParams(var17);
                }

                int var18 = ViewGroup.getChildMeasureSpec(this.mWidthMeasureSpec, this.getPaddingLeft() + this.getPaddingRight(), var17.width);
                int paramsHeight = var17.height;
                int childHeightSpec;
                if(paramsHeight > 0) {
                    childHeightSpec = MeasureSpec.makeMeasureSpec(paramsHeight, MeasureSpec.EXACTLY);
                } else {
                    childHeightSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
                }

                this.mFloatingGroupView.measure(var18, childHeightSpec);
                int floatingGroupScrollY = 0;
                int nextGroupFlatPosition = this.getFlatListPosition(getPackedPositionForGroup(this.mFloatingGroupPosition + 1));
                int nextGroupListPosition = nextGroupFlatPosition - position;
                if(nextGroupListPosition >= 0 && nextGroupListPosition < this.getChildCount()) {
                    View left = this.getChildAt(nextGroupListPosition);
                    if(left != null && left.getTop() < this.getPaddingTop() + this.mFloatingGroupView.getMeasuredHeight() + this.getDividerHeight()) {
                        floatingGroupScrollY = left.getTop() - (this.getPaddingTop() + this.mFloatingGroupView.getMeasuredHeight() + this.getDividerHeight());
                        if (mOnFloatingGroupChangeListener != null) {
                            mOnFloatingGroupChangeListener.onFloatingGroupChangeListener(mFloatingGroupView, left, mFloatingGroupScrollY);
                        }
                    }
                }

                int var19 = this.getPaddingLeft();
                int top = this.getPaddingTop() + floatingGroupScrollY;
                int right = var19 + this.mFloatingGroupView.getMeasuredWidth();
                int bottom = top + this.mFloatingGroupView.getMeasuredHeight();
                this.mFloatingGroupView.layout(var19, top, right, bottom);
                this.mFloatingGroupScrollY = floatingGroupScrollY;
                if(this.mOnScrollFloatingGroupListener != null) {
                    this.mOnScrollFloatingGroupListener.onScrollFloatingGroupListener(this.mFloatingGroupView, this.mFloatingGroupScrollY);
                }

            }
        }
    }

    private void loadAttachInfo() {
        if(this.mViewAttachInfo == null) {
            this.mViewAttachInfo = ReflectionUtils.getFieldValue(View.class, "mAttachInfo", this);
        }

    }

    private void setAttachInfo(View v) {
        if(v != null) {
            if(this.mViewAttachInfo != null) {
                ReflectionUtils.setFieldValue(View.class, "mAttachInfo", v, this.mViewAttachInfo);
            }

            if(v instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup)v;

                for(int i = 0; i < viewGroup.getChildCount(); ++i) {
                    this.setAttachInfo(viewGroup.getChildAt(i));
                }
            }

        }
    }

    private void positionSelectorOnFloatingGroup() {
        if(this.mShouldPositionSelector && this.mFloatingGroupView != null) {
            if(Build.VERSION.SDK_INT >= 14) {
                int floatingGroupFlatPosition = this.getFlatListPosition(getPackedPositionForGroup(this.mFloatingGroupPosition));
                ReflectionUtils.invokeMethod(AbsListView.class, "positionSelector", new Class[]{Integer.TYPE, View.class}, this, new Object[]{Integer.valueOf(floatingGroupFlatPosition), this.mFloatingGroupView});
            } else {
                ReflectionUtils.invokeMethod(AbsListView.class, "positionSelector", new Class[]{View.class}, this, new Object[]{this.mFloatingGroupView});
            }

            this.invalidate();
        }

        this.mShouldPositionSelector = false;
        this.removeCallbacks(this.mPositionSelectorOnTapAction);
    }

    private void drawDefaultSelector(Canvas canvas) {
        int selectorListPosition = this.mSelectorPosition - this.getFirstVisiblePosition();
        if(selectorListPosition >= 0 && selectorListPosition < this.getChildCount() && this.mSelectorRect != null && !this.mSelectorRect.isEmpty()) {
            int floatingGroupFlatPosition = this.getFlatListPosition(getPackedPositionForGroup(this.mFloatingGroupPosition));
            if(this.mFloatingGroupView == null || this.mSelectorPosition != floatingGroupFlatPosition) {
                this.drawSelector(canvas);
            }
        }

    }

    private void drawFloatingGroupSelector(Canvas canvas) {
        if(this.mSelectorRect != null && !this.mSelectorRect.isEmpty()) {
            int floatingGroupFlatPosition = this.getFlatListPosition(getPackedPositionForGroup(this.mFloatingGroupPosition));
            if(this.mSelectorPosition == floatingGroupFlatPosition) {
                this.mSelectorRect.set(this.mFloatingGroupView.getLeft(), this.mFloatingGroupView.getTop(), this.mFloatingGroupView.getRight(), this.mFloatingGroupView.getBottom());
                this.drawSelector(canvas);
            }
        }

    }

    private void drawSelector(Canvas canvas) {
        canvas.save();
        canvas.clipRect(this.getPaddingLeft(), this.getPaddingTop(), this.getWidth() - this.getPaddingRight(), this.getHeight() - this.getPaddingBottom());
        if(this.isPressed()) {
            this.mSelector.setState(this.getDrawableState());
        } else {
            this.mSelector.setState(EMPTY_STATE_SET);
        }

        this.mSelector.setBounds(this.mSelectorRect);
        this.mSelector.draw(canvas);
        canvas.restore();
    }

    private void drawFloatingGroupIndicator(Canvas canvas) {
        Drawable groupIndicator = (Drawable)ReflectionUtils.getFieldValue(ExpandableListView.class, "mGroupIndicator", this);
        if(groupIndicator != null) {
            int stateSetIndex = (this.mAdapter.isGroupExpanded(this.mFloatingGroupPosition)?1:0) | (this.mAdapter.getChildrenCount(this.mFloatingGroupPosition) > 0?2:0);
            groupIndicator.setState(GROUP_STATE_SETS[stateSetIndex]);
            int indicatorLeft = ((Integer)ReflectionUtils.getFieldValue(ExpandableListView.class, "mIndicatorLeft", this)).intValue();
            int indicatorRight = ((Integer)ReflectionUtils.getFieldValue(ExpandableListView.class, "mIndicatorRight", this)).intValue();
            if(Build.VERSION.SDK_INT >= 14) {
                this.mIndicatorRect.set(indicatorLeft + this.getPaddingLeft(), this.mFloatingGroupView.getTop(), indicatorRight + this.getPaddingLeft(), this.mFloatingGroupView.getBottom());
            } else {
                this.mIndicatorRect.set(indicatorLeft, this.mFloatingGroupView.getTop(), indicatorRight, this.mFloatingGroupView.getBottom());
            }

            groupIndicator.setBounds(this.mIndicatorRect);
            groupIndicator.draw(canvas);
        }

    }

    static {
        GROUP_STATE_SETS = new int[][]{EMPTY_STATE_SET, GROUP_EXPANDED_STATE_SET, GROUP_EMPTY_STATE_SET, GROUP_EXPANDED_EMPTY_STATE_SET};
    }

    public interface OnScrollFloatingGroupListener {
        void onScrollFloatingGroupListener(View var1, int var2);
    }
}
