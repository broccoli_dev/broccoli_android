package com.nomadconnection.broccoli.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.hyunmin.materialrefresh.MaterialRefreshLayout;
import com.nomadconnection.broccoli.interf.InterfaceScrollable;
import com.nomadconnection.broccoli.utils.ElseUtils;

import java.lang.reflect.Field;

/**
 * Created by YelloHyunminJang on 16. 6. 21..
 */
public class CustomSwipeRefresh extends MaterialRefreshLayout {

    private View mHeader;

    private InterfaceScrollable interfaceScrollable;

    public CustomSwipeRefresh(Context context) {
        super(context, null);
    }

    public CustomSwipeRefresh(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setInterfaceFragmentReset(InterfaceScrollable listener) {
        interfaceScrollable = listener;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        init();
    }

    private void init() {
        Field field = null;
        try {
            field = MaterialRefreshLayout.class.getDeclaredField("mMaterialHeaderView");
            field.setAccessible(true);
            try {
                mHeader = (View) field.get(this);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mHeader.post(new Runnable() {
            @Override
            public void run() {
                LayoutParams params = (LayoutParams) mHeader.getLayoutParams();
                params.setMargins(0, ElseUtils.convertDp2Px(getContext(), 80f), 0, 0);
                mHeader.setLayoutParams(params);
            }
        });
    }

    @Override
    public boolean canChildScrollUp() {
        boolean ret = false;
        if (interfaceScrollable != null) {
            ret = interfaceScrollable.canScroll();
        } else {
            ret = super.canChildScrollUp();
        }
        return ret;
    }
}
