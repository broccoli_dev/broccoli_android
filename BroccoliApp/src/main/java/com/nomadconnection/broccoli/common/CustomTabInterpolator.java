package com.nomadconnection.broccoli.common;

/**
 * Created by YelloHyunminJang on 16. 6. 22..
**/

import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

public abstract class CustomTabInterpolator {

    public static final CustomTabInterpolator SMART = new SmartIndicationInterpolator();
    public static final CustomTabInterpolator LINEAR = new LinearIndicationInterpolator();
    public static final CustomTabInterpolator BROCCOLI = new BroccoliIndicationInterpolator();

    static final int ID_SMART = 0;
    static final int ID_LINEAR = 1;
    static final int ID_BROCCOLI = 2;

    public static CustomTabInterpolator of(int id) {
        switch (id) {
            case ID_SMART:
                return SMART;
            case ID_LINEAR:
                return LINEAR;
            case ID_BROCCOLI:
                return BROCCOLI;
            default:
                throw new IllegalArgumentException("Unknown id: " + id);
        }
    }

    public abstract float getLeftEdge(float offset);

    public abstract float getRightEdge(float offset);

    public float getThickness(float offset) {
        return 1f; //Always the same thickness by default
    }

    public int getColor(float offset) {
        return 0xff;
    }

    public static class SmartIndicationInterpolator extends CustomTabInterpolator {

        private static final float DEFAULT_INDICATOR_INTERPOLATION_FACTOR = 3.0f;

        private final Interpolator leftEdgeInterpolator;
        private final Interpolator rightEdgeInterpolator;

        public SmartIndicationInterpolator() {
            this(DEFAULT_INDICATOR_INTERPOLATION_FACTOR);
        }

        public SmartIndicationInterpolator(float factor) {
            leftEdgeInterpolator = new AccelerateInterpolator(factor);
            rightEdgeInterpolator = new DecelerateInterpolator(factor);
        }

        @Override
        public float getLeftEdge(float offset) {
            return leftEdgeInterpolator.getInterpolation(offset);
        }

        @Override
        public float getRightEdge(float offset) {
            return rightEdgeInterpolator.getInterpolation(offset);
        }

        @Override
        public float getThickness(float offset) {
            return 1f / (1.0f - getLeftEdge(offset) + getRightEdge(offset));
        }

    }

    public static class LinearIndicationInterpolator extends CustomTabInterpolator {

        @Override
        public float getLeftEdge(float offset) {
            return offset;
        }

        @Override
        public float getRightEdge(float offset) {
            return offset;
        }

    }

    public static class BroccoliIndicationInterpolator extends SmartIndicationInterpolator {

        @Override
        public float getThickness(float offset) {
            return 1f;
        }

        @Override
        public int getColor(float offset) {
            return (int) (0x80 + 0x80 * (1.0f - getRightEdge(offset)));
        }
    }
}
