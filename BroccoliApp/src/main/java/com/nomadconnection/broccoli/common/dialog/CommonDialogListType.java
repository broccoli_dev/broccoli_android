package com.nomadconnection.broccoli.common.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.nomadconnection.broccoli.R;

import java.util.ArrayList;


/**
 * List형태의 팝업이다.
 * @author Jang
 *
 */
public class CommonDialogListType extends BaseVariableDialog {

	/**
	 * 눌려진 버튼의 id값과 현재 눌려진 리스트의 item 위치값을 돌려준다.
	 * @author Jang
	 *
	 */
	public static interface DialogListTypeListener {
		public void onButtonClick(DialogInterface dialog, int stringResId);
		public void onItemClick(DialogInterface dialog, int itemPosition);
	}
	
	private ArrayList<String> mList;
	private ListView mListView;
	private CommonDialogListAdapter mAdapter;
	private DialogListTypeListener mOnClickListener;

	/**
	 * 생성자
	 * @param context
	 * @param title
	 * @param buttons_resid
	 * @param list
	 */
	public CommonDialogListType(Context context, String title, int[] buttons_resid, ArrayList<String> list) {
		super(context, title, buttons_resid);
		if (list != null) {
			init(list);
		}
	}
	
	/**
	 * 생성자
	 * @param context
	 * @param title
	 * @param subtitle
	 * @param buttons_resid
	 * @param list
	 */
	public CommonDialogListType(Context context, String title, String subtitle, int[] buttons_resid, ArrayList<String> list) {
		super(context, title, buttons_resid);
		if (list != null) {
			init(list);
		}
	}
	
	/**
	 * 초기화 작업 <br>
	 * List의 item에 두 종류의 text가 추가되어야 될 시 list2가 필요하다.
	 * @param list
	 */
	private void init(ArrayList<String> list) {
		this.createBody(BodyType.ListType);
		
		mListView = (ListView) this.findViewById(R.id.dialog_bodytype_list_listview);
		setList(list);
	}
	
	/**
	 * 추가된 data에 따라 body를 visibility와 line의 vislbility를 조절한다.
	 * @param list
	 */
	public void setList(ArrayList<String> list) {
		if (null != mAdapter){
			mAdapter.clear();
			mAdapter = null;
		}
		
		if (null != mList && !mList.isEmpty()) {
			mList.clear();
		} else if (mList == null) {
			mList = new ArrayList<String>();
		}
		mList.addAll(list);
		
		if ((list.size() < 1)) {
			getBodyLayer().setVisibility(View.GONE);
		} else if (list.size() > 0) {
			getBodyLayer().setVisibility(View.VISIBLE);
		}
		
		View line = this.findViewById(R.id.dialog_bodytype_list_top_line);
		if (line != null) {
			if ((list.size() < 1)) {
				line.setVisibility(View.GONE);
			} else {
				line.setVisibility(View.VISIBLE);
			}
		}
		mAdapter = new CommonDialogListAdapter(this.getContext(), 0, mList);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(mOnItemClickListener);
	}
	
	/**
	 * 추가된 이중 data에 따라 body를 visibility와 line의 vislbility를 조절한다.
	 * @param list
	 */
	public void setList(ArrayList<String> list, ArrayList<String> list2) {
		if (null != mAdapter){
			mAdapter.clear();
			mAdapter = null;
		}
		
		if (null != mList && !mList.isEmpty()) {
			mList.clear();
		} else if (mList == null) {
			mList = new ArrayList<String>();
		}
		mList.addAll(list);
		
		if ((list.size() < 1)) {
			getBodyLayer().setVisibility(View.GONE);
		} else if (list.size() > 0) {
			getBodyLayer().setVisibility(View.VISIBLE);
		}
		
		View line = this.findViewById(R.id.dialog_bodytype_list_top_line);
		if (line != null) {
			if ((list.size() < 1)) {
				line.setVisibility(View.GONE);
			} else {
				line.setVisibility(View.VISIBLE);
			}
		}
		mAdapter = new CommonDialogListAdapter(this.getContext(), 0, mList, list2);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(mOnItemClickListener);
	}
	
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position,
								long arg3) {
			if (mOnClickListener != null) {
				mOnClickListener.onItemClick(CommonDialogListType.this, position);
				//dismiss();
			}
		}
	};
	
	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		if (mAdapter != null) {
			mAdapter.clear();
			mAdapter = null;
		}
		if (mList != null) {
			if (!mList.isEmpty()) {
				mList.clear();
			}
			mList = null;
		}
	}
	
	@Override
	protected void OnClick(int id) {
		// TODO Auto-generated method stub
		if (mOnClickListener != null) {
			mOnClickListener.onButtonClick(this, id);
			dismiss();
		}
	}
	
	/**
	 * {@link CommonDialogListType}를 등록한다.
	 * @param listener
	 */
	public void setDialogListener(DialogListTypeListener listener) {
		mOnClickListener = listener;
	}
}
