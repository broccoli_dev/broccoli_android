package com.nomadconnection.broccoli.common.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.widget.EditText;

import com.nomadconnection.broccoli.R;

/**
 * EditText가 있는 입력 팝업이다.
 * @author Jang
 *
 */
public class CommonDialogInputType extends BaseVariableDialog {

	/**
	 * 눌려진 버튼의 id값과 현재 입력창에 입력된 text를 들고온다.
	 * @author Jang
	 *
	 */
	public interface DialogInputTypeListener {
		void onClick(DialogInterface dialog, int stringresid, String text);
	}
	
	private EditText mEditText;
	private DialogInputTypeListener mOnClickListener;
	
	/**
	 * 입력 팝업의 생성자
	 * @param context
	 * @param title_resid
	 * @param buttons
	 */
	public CommonDialogInputType(Context context, int title_resid, int[] buttons) {
		super(context, title_resid, buttons);
		init();
	}
	
	/**
	 * 입력 팝업의 생성자
	 * @param context
	 * @param title
	 * @param buttons
	 */
	public CommonDialogInputType(Context context, String title, int[] buttons) {
		super(context, title, buttons);
		init();
	}

	/**
	 * 초기화 작업
	 */
	private void init() {
		this.createBody(BodyType.InputType);
		
		mEditText = (EditText) getBodyLayout().findViewById(R.id.dialog_bodytype_input_edittext);
	}
	
	/**
	 * {@link CommonDialogInputType}를 등록한다.
	 * @param listener
	 */
	public void setDialogListener(DialogInputTypeListener listener){
		mOnClickListener = listener;
	}
	
	/**
	 * 현재 입력창 객체를 가져온다.
	 * @return EditText
	 */
	public EditText getEditText() {
		return mEditText;
	}
	
	/**
	 * 입력창에 현재 입력되어 있는 text를 가져온다.
	 * @return String
	 */
	public String getText() {
		return getText(false);
	}
	
	/**
	 * 입력창에 현재 입력되어 잇는 text를 가져온다.<br>
	 * 입력창에 입력이 없고 getHint가 true라면 힌트를 들고온다.
	 * @param getHint
	 * @return String
	 */
	public String getText(boolean getHint){
		String ret = new String();
		if (mEditText != null) {
			ret = mEditText.getText().toString();
			if(getHint && TextUtils.isEmpty(ret)){
				CharSequence cs = mEditText.getHint();
				if(null != cs){
					ret = cs.toString();
				}
			}
		}
		return ret;
	}
	
	/**
	 * 입력창에 힌트를 등록한다.
	 * @param hint_resid
	 */
	public void setHint(int hint_resid) {
		String shint = getStringResouce(hint_resid);
		mEditText.setHint(shint);
	}
	
	/**
	 * 입력창에 힌트를 등록한다.
	 * @param hint
	 */
	public void setHint(String hint) {
		mEditText.setHint(hint);
	}
	
	@Override
	protected void OnClick(int id) {
		// TODO Auto-generated method stub
		if (mOnClickListener != null) {
			mOnClickListener.onClick(CommonDialogInputType.this, id, getText());
		}
	}
	
}
