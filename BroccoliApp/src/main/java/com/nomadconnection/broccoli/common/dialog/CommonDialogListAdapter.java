package com.nomadconnection.broccoli.common.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

import java.util.ArrayList;

/**
 * 리스트형태의 팝업에 사용되는 Adpater
 * @author Jang
 *
 */
public class CommonDialogListAdapter extends ArrayAdapter<String> {
	
	private LayoutInflater mInflater;
	private ArrayList<String> mList2 = null;
	
	public CommonDialogListAdapter(Context context, int textViewResourceId, ArrayList<String> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub 
		mInflater = LayoutInflater.from(context);
		mList2 = null;
	}
	
	public CommonDialogListAdapter(Context context, int textViewResourceId, ArrayList<String> objects, ArrayList<String> list2) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub 
		mInflater = LayoutInflater.from(context);
		mList2 = list2;
	}
	
	@Override
	public void add(String object) {
		// TODO Auto-generated method stub
		super.add(object);
	}
	
	@Override
	public void remove(String object) {
		// TODO Auto-generated method stub
		super.remove(object);
	}
	
	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
		if (mList2 != null) {
			mList2.clear();
			mList2 = null;
		}
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return super.getCount();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return super.getItem(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view;
		if (convertView != null) {
			view = convertView;
		} else {
			view = mInflater.inflate(R.layout.common_dialog_bodytype_list_item, null);
		}
		
		TextView textview = (TextView) view.findViewById(R.id.dialog_list_item_textview1);
		textview.setText(this.getItem(position));
		if (mList2 != null) {
			TextView textview2 = (TextView) view.findViewById(R.id.dialog_list_item_textview2);
			textview2.setVisibility(View.VISIBLE);
			textview2.setText(mList2.get(position));
		}
		
		return view;
	}

}
