package com.nomadconnection.broccoli.common;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

/**
 * Created by YelloHyunminJang on 16. 6. 23..
 */
public class CustomTabView extends FrameLayout {

    private TextView mTextView;
    private String mText;
    private boolean isDisable = false;

    public CustomTabView(Context context) {
        super(context);
        init();
    }

    public CustomTabView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTabView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

    }

    public void createTabView(ViewGroup container, int position, android.support.v4.view.PagerAdapter adapter) {
        mTextView = new TextView(this.getContext());
        mTextView.setGravity(Gravity.CENTER);
        mTextView.setText(adapter.getPageTitle(position));
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f);
        mTextView.setTextAppearance(this.getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        int[][] states = new int[][] {new int[] {android.R.attr.state_pressed}, new int[] {android.R.attr.state_enabled},
                new int[] {-android.R.attr.state_pressed}, new int[] {-android.R.attr.state_enabled}, new int[] {android.R.attr.state_selected}
        ,new int[] {-android.R.attr.state_selected}};
        int[] colors = new int[] {this.getContext().getResources().getColor(R.color.tabbar_title_text_color_press),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_press),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_nor),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_nor),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_sel),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_nor)};
        ColorStateList colorStateList = new ColorStateList(states, colors);
        mTextView.setTextColor(colorStateList);
        Typeface tapFont = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
        mTextView.setTypeface(tapFont);
        mTextView.setEnabled(false);
        addView();
    }

    public void createNormalTabView(String title) {
        mText = title;
        mTextView = new TextView(this.getContext());
        mTextView.setGravity(Gravity.CENTER);
        mTextView.setText(title);
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        mTextView.setTextAppearance(this.getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        int[][] states = new int[][] {new int[] {android.R.attr.state_pressed}, new int[] {android.R.attr.state_enabled},
                new int[] {-android.R.attr.state_pressed}, new int[] {-android.R.attr.state_enabled}, new int[] {android.R.attr.state_selected}
                ,new int[] {-android.R.attr.state_selected}};
        int[] colors = new int[] {this.getContext().getResources().getColor(R.color.tabbar_title_text_color_sel),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_sel),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_nor),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_nor),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_sel),
                this.getContext().getResources().getColor(R.color.tabbar_title_text_color_nor)};
        ColorStateList colorStateList = new ColorStateList(states, colors);
        mTextView.setTextColor(colorStateList);
        Typeface tapFont = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
        mTextView.setTypeface(tapFont);
        mTextView.setEnabled(false);
        addView();
    }

    public void createDisableTabView(String title) {
        mText = title;
        mTextView = new TextView(this.getContext());
        mTextView.setGravity(Gravity.CENTER);
        mTextView.setText(title);
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        mTextView.setTextAppearance(this.getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        mTextView.setTextColor(0x88000000);
        Typeface tapFont = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
        mTextView.setTypeface(tapFont);
        mTextView.setEnabled(false);
        addView();
    }

    private void addView() {
        if (this.getChildCount() > 0) {
            this.removeView(this.getChildAt(0));
        }
        this.addView(mTextView);
    }

    public TextView getTabView() {
        return mTextView;
    }

    public String getBackupText() {
        return mText;
    }

    public boolean isDisable() {
        return isDisable;
    }

    public void setDisable(boolean disable) {
        isDisable = disable;
    }
}
