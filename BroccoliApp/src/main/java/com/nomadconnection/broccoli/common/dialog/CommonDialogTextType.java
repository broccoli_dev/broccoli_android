package com.nomadconnection.broccoli.common.dialog;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

/**
 * 일반적인 Text를 출력해주는 팝업이다.
 *
 * @author Jang
 */
public class CommonDialogTextType extends BaseVariableDialog {

    final static int DEFAULT = -1;

    /**
     * Text를 등록 시에 color와 size, style등을 정할 수 있다.
     *
     * @author Jang
     */
    public static class TextData {

        public String mText = null;
        public SpannableStringBuilder mSpannableText = null;
        public int mTextColor = -1;
        public int mTextSize = -1;
        public float mLineSpace = -1;
        public int mTextStyle = -1;
        public int mGravity = Gravity.CENTER;
        public int mLeftIcon = -1;
        public int mMargin = 15;

        public TextData(String text) {
            mText = text;
        }

        public TextData(Context context, int text_resid) {
            mText = context.getString(text_resid);
        }

        public TextData(Context context, int text_resid, int color_resid, int sp_size,
                        int text_style) {
            mText = context.getString(text_resid);
            if (color_resid > 0)
                mTextColor = color_resid;//getColorResource(color_resid);
            if (sp_size > 0)
                mTextSize = sp_size;
            if (text_style > 0)
                mTextStyle = text_style;
        }

        public TextData(SpannableStringBuilder text, int sp_size,
                        int text_style) {
            mSpannableText = text;
            if (sp_size > 0)
                mTextSize = sp_size;
            if (text_style > 0)
                mTextStyle = text_style;
        }

        public TextData(String text, int color_resid, int sp_size,
                        int text_style) {
            mText = text;
            if (color_resid > 0)
                mTextColor = color_resid;//getColorResource(color_resid);
            if (sp_size > 0)
                mTextSize = sp_size;
            if (text_style > 0)
                mTextStyle = text_style;
        }

        public TextView setText(TextView textview) {
            if (mSpannableText != null) {
                textview.setText(mSpannableText);
            } else {
                textview.setText(mText);
            }
            return textview;
        }

    }

    private LinearLayout mBody;

    public CommonDialogTextType(Context context, String[] buttons) {
        super(context, null, buttons);
        init();
    }

    public CommonDialogTextType(Context context, int[] buttons) {
        super(context, (String) null, buttons);
        init();
    }

    public CommonDialogTextType(Context context, int title_resid,
                                int[] buttons_resid) {
        super(context, title_resid, buttons_resid);
        init();
    }

    public CommonDialogTextType(Context context, String title, String[] buttons) {
        super(context, title, buttons);
        init();
    }

    public CommonDialogTextType(Context context, String title,
                                int[] buttons_resid) {
        super(context, title, buttons_resid);
        init();
    }

    public CommonDialogTextType(Context context, Spanned title,
                                int[] buttons_resid) {
        super(context, title, buttons_resid);
        init();
    }

    public CommonDialogTextType(Context context) {
        super(context);
        init();
    }

    /**
     * 초기화 작업
     */
    private void init() {
        this.createBody(BodyType.TextType);

        mBody = (LinearLayout) getBodyLayout().findViewById(
                R.id.dialog_bodytype_text_body);
    }

    public void addAdjustMarginTextView(int text_res, int color, int size, int style, int margin, int gravity) {
        TextData data = new TextData(this.getContext(), text_res, color, size,
                style);
        data.mMargin = margin;
        data.mGravity = gravity;
        addTextView(data);
    }

    public void addAdjustMarginTextView(String text, int color, int size, int style, int margin, int gravity) {
        TextData data = new TextData(text, color, size,
                style);
        data.mMargin = margin;
        data.mGravity = gravity;
        addTextView(data);
    }

    /**
     * Text를 추가한다.
     *
     * @param text_resid
     * @param color_resid
     * @param sp_size
     * @param text_style
     */
    public void addTextView(int text_resid, int color_resid, int sp_size,
                            int text_style) {
        TextData data = new TextData(this.getContext(), text_resid, color_resid, sp_size,
                text_style);
        addTextView(data);
    }

    /**
     * Text를 추가한다.
     *
     * @param text_resid
     * @param color_resid
     * @param sp_size
     */
    public void addTextView(int text_resid, int color_resid, int sp_size) {
        addTextView(text_resid, color_resid, sp_size, DEFAULT);
    }

    /**
     * Text를 추가한다.
     *
     * @param text_resid
     */
    public void addTextView(int text_resid) {
        addTextView(text_resid, DEFAULT, DEFAULT);
    }

    /**
     * Text를 추가한다.
     *
     * @param text
     * @param sp_size
     * @param text_style
     */
    public void addTextView(SpannableStringBuilder text, int sp_size,
                            int text_style) {
        TextData data = new TextData(text, sp_size, text_style);
        TextView textview = createTextView(data);
        mBody.addView(textview);
    }

    /**
     * Text를 추가한다.
     *
     * @param text
     * @param sp_size
     */
    public void addTextView(SpannableStringBuilder text, int sp_size) {
        addTextView(text, sp_size, DEFAULT);
    }

    /**
     * Text를 추가한다.
     *
     * @param text
     */
    public void addTextView(SpannableStringBuilder text) {
        addTextView(text, DEFAULT);
    }

    /**
     * Text를 추가한다.
     *
     * @param text
     * @param color_resid
     * @param sp_size
     * @param text_style
     */
    public void addTextView(String text, int color_resid, int sp_size,
                            int text_style) {
        TextData data = new TextData(text, color_resid, sp_size, text_style);
        addTextView(data);
    }

    /**
     * Text를 추가한다.
     *
     * @param text
     * @param color_resid
     * @param sp_size
     */
    public void addTextView(String text, int color_resid, int sp_size) {
        addTextView(text, color_resid, sp_size, DEFAULT);
    }

    /**
     * Text를 추가한다.
     *
     * @param text
     */
    public void addTextView(String text) {
        addTextView(text, DEFAULT, DEFAULT);
    }

    public void addTextView(TextData data) {
        TextView textview = createTextView(data);
        mBody.addView(textview);
    }

    /**
     * Text를 추가한다.
     *
     * @param textview
     */
    public void addTextView(TextView textview) {
        if (textview != null)
            mBody.addView(textview);
    }

    /**
     * Text를 추가한다.
     *
     * @param textview
     */
    public void addTextLayout(View textview) {
        if (textview != null)
            mBody.addView(textview);
    }

    /**
     * 특정 position의 text를 지운다.
     *
     * @param position
     */
    public void removeTextView(int position) {
        mBody.removeViewAt(position);
    }

    /**
     * 모든 text를 지운다.
     */
    public void removeAllTextView() {
        mBody.removeAllViews();
    }

    /**
     * 특정 position의 text를 편집힌다.
     *
     * @param position
     * @param text_resid
     * @param color_resid
     * @param sp_size
     * @param text_style
     */
    public void editTextView(int position, int text_resid, int color_resid,
                             int sp_size, int text_style) {
        TextView view = (TextView) mBody.getChildAt(position);
        if (view != null) {
            if (color_resid > 0)
                view.setTextColor(getColorResource(color_resid));
            if (sp_size > 0)
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, sp_size);
            if (text_style > 0) {
                view.setTypeface(null, text_style);
                if (text_style == Typeface.BOLD
                        || text_style == Typeface.BOLD_ITALIC)
                    view.setPaintFlags(view.getPaintFlags()
                            | Paint.FAKE_BOLD_TEXT_FLAG);
            }
            if (text_resid > 0)
                view.setText(getStringResouce(text_resid));
        }
    }

    public void editTextView(int position, SpannableStringBuilder text,
                             int color_resid, int sp_size, int text_style) {
        TextView view = (TextView) mBody.getChildAt(position);
        if (view != null) {
            if (color_resid > 0)
                view.setTextColor(getColorResource(color_resid));
            if (sp_size > 0)
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, sp_size);
            if (text_style > 0) {
                view.setTypeface(null, text_style);
                if (text_style == Typeface.BOLD
                        || text_style == Typeface.BOLD_ITALIC)
                    view.setPaintFlags(view.getPaintFlags()
                            | Paint.FAKE_BOLD_TEXT_FLAG);
            }
            view.setText(text);
        }
    }

    public void editTextView(int position, String text, int color_resid,
                             int sp_size, int text_style) {
        TextView view = (TextView) mBody.getChildAt(position);
        if (view != null) {
            if (color_resid > 0)
                view.setTextColor(getColorResource(color_resid));
            if (sp_size > 0)
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, sp_size);
            if (text_style > 0) {
                view.setTypeface(null, text_style);
                if (text_style == Typeface.BOLD
                        || text_style == Typeface.BOLD_ITALIC)
                    view.setPaintFlags(view.getPaintFlags()
                            | Paint.FAKE_BOLD_TEXT_FLAG);
            }
            view.setText(text);
        }
    }

    /**
     * text를 textview에 set하고 생성된 textview 객체를 돌려준다.
     *
     * @param data
     * @return TextView
     */
    private TextView createTextView(TextData data) {
        TextView textview = new TextView(this.getContext());
        LinearLayout.LayoutParams lParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        if (mBody.getChildCount() != 0) {
            lParam.topMargin = data.mMargin;
        }
        lParam.gravity = data.mGravity;
        textview.setLayoutParams(lParam);
        textview = data.setText(textview);
        if (data.mTextColor > 0)
            textview.setTextColor(getContext().getResources().getColor(data.mTextColor));
        else {
            textview.setTextColor(getContext().getResources().getColor(R.color.common_rgb_89_89_89));
        }
        if (data.mTextSize > 0) {
            textview.setTextSize(TypedValue.COMPLEX_UNIT_SP, data.mTextSize);
        } else {
            int textSize = 14;
            textview.setTextSize(textSize);
        }
        if (data.mLineSpace > 0) {
            textview.setLineSpacing(data.mLineSpace, 1.0f);
        }
        if (data.mTextStyle > 0) {
            textview.setTypeface(null, data.mTextStyle);
            if (data.mTextStyle == Typeface.BOLD
                    || data.mTextStyle == Typeface.BOLD_ITALIC)
                textview.setPaintFlags(textview.getPaintFlags()
                        | Paint.FAKE_BOLD_TEXT_FLAG);

        }
        if (data.mLeftIcon > 0) {
            textview.setCompoundDrawablesWithIntrinsicBounds(data.mLeftIcon, 0, 0, 0);
            textview.setCompoundDrawablePadding(10);
        }
        textview.setGravity(data.mGravity);

        return textview;
    }

    /**
     * 특정 position의 TextView를 가져온다.
     *
     * @param position
     * @return
     */
    public TextView getTextView(int position) {
        return (TextView) mBody.getChildAt(position);
    }

    /**
     * 현재 추가된 TextView의 개수를 가져온다.
     *
     * @return
     */
    public int getTotalTextViewCount() {
        return mBody.getChildCount();
    }

    /**
     * Text를 추가한다.
     *
     * @param message_resid
     */
    public void setMessage(int message_resid) {
        addTextView(message_resid);
    }

    /**
     * Text를 추가한다.
     *
     * @param message
     */
    public void setMessage(String message) {
        addTextView(message);
    }

    @Override
    protected void OnClick(int id) {
        // TODO Auto-generated method stub
        super.OnClick(id);
        dismiss();
    }

    @Override
    public void dismiss() {
        // TODO Auto-generated method stub
        super.dismiss();
        removeAllTextView();
    }
}
