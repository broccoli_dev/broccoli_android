package com.nomadconnection.broccoli.common.dialog;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;

import java.util.ArrayList;


/**
 * 모든 팝업의 Base가 되는 팝업이다.<br>
 * 하는 작업은 아래와 같다.<br>
 * <li>자동 버튼 생성 <li>자동 높이 지정 <li>자동 스크롤 생성
 * 
 * @author Jang
 * 
 */
public class BaseVariableDialog extends Dialog {

	public static final int BUTTON_SINGLE = 0;
	public static final int BUTTON_LEFT = 0;
	public static final int BUTTON_RIGHT = 1;

	public static final int DENSITY_XH = 320;
	public static final int DENSITY_XXH = 480;
	public static final int DENSITY_XXXH = 640;
	private static final int MAGINO = 100;
	protected LayoutInflater mInflater;
	private BodyType mBodyType = null;
	private LinearLayout mDialog;
	private FrameLayout mTitleLayer;
	private LinearLayout mBodyLayer;
	private FrameLayout mButtonLayer;
	private ScrollView mBodyScrollWrapper;
	private LinearLayout mBodyLayout;
	private LinearLayout mVariableBody;
	private LinearLayout mButtonLayout;
	private ImageView mBodyLayerBackground;
	private boolean isFullScreenMode = false;
	/**
	 * 화면에 따라 팝업의 화면 높이를 자동 조절하고<br>
	 * 자동으로 스크롤될 수 있도록 높이를 고정해주는 기능이 있다.
	 */
	Runnable setDialogBodyHeight = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Rect CheckRect = new Rect();
			Window window = getWindow();
			window.getDecorView().getWindowVisibleDisplayFrame(CheckRect);
			int StatusBarHeight = CheckRect.top;
			// int contentTop=
			// window.findViewById(Window.ID_ANDROID_CONTENT).getTop();

			Display display = ((WindowManager) BaseVariableDialog.this
					.getContext().getSystemService(Context.WINDOW_SERVICE))
					.getDefaultDisplay();
			int deviceHeight = display.getHeight() - StatusBarHeight;
			int dialogHeight = mDialog.getHeight();
			int bodyHeight = 0;

			if (mBodyType == BodyType.ListType) {
				bodyHeight = getTotalHeightofListView((ListView) BaseVariableDialog.this
						.findViewById(R.id.dialog_bodytype_list_listview));
			} else if (mBodyType == BodyType.Custom) {
				bodyHeight = mCustomBody.getHeight();
			} else {
				if (mVariableBody != null)
					bodyHeight = mVariableBody.getHeight();
			}

			int realDialogHeight = bodyHeight + mTitleLayer.getHeight()
					+ mButtonLayer.getHeight();
			if (dialogHeight < realDialogHeight) {
				dialogHeight = realDialogHeight;
			}

			int height = 0;

			if (isFullScreenMode) {
				height = deviceHeight - mTitleLayer.getHeight()
						- mButtonLayer.getHeight();
//				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//				mBodyScrollWrapper.setLayoutParams(params);
			} else {
				if (deviceHeight - MAGINO < dialogHeight) {
					height = deviceHeight - mTitleLayer.getHeight()
							- mButtonLayer.getHeight() - MAGINO;
				} else {
					height = mBodyLayer.getHeight();
				}
			}

			FrameLayout.LayoutParams bodylayerParam = (FrameLayout.LayoutParams) mBodyLayer
					.getLayoutParams();
			if (bodylayerParam == null) {
				bodylayerParam = new FrameLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			}
			FrameLayout.LayoutParams backgroundParam = (FrameLayout.LayoutParams) mBodyLayerBackground
					.getLayoutParams();
			if (backgroundParam == null) {
				backgroundParam = new FrameLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			}

			bodylayerParam.height = height;
			backgroundParam.height = height;

			mBodyLayer.setLayoutParams(bodylayerParam);
			mBodyLayerBackground.setLayoutParams(backgroundParam);
			mBodyLayer.invalidate();
		}
	};

	private TextView mTitleText;
	private Button[] mButtons;
	private View.OnClickListener mButtonClickListener;
	private DialogButtonOnClickListener mDialogClickListener;
	private DialogKeyListener mDialogKeyListener;
	private int[] mButtonResource;
	private int mAniStyle = R.style.Animation_AppCompat_DropDownUp;
	/**
	 * 가로세로 회전을 감지하기 위해 등록한 receiver를 해제한다.
	 */
	private boolean unregistered = false;
	private BroadcastReceiver mRotateReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (mBodyLayout != null)
				mBodyLayout.post(setDialogBodyHeight);
		}

	};
	View.OnClickListener mOnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			if (id >= 0 && mButtons.length > id) {
				if (mButtonResource != null) {
					int resid = mButtonResource[id];
					OnClick(resid);
				} else {
					OnClick(id);
				}
			}
		}
	};
	private View mCustomBody;
	private View mCustomTitle;
	private View mCustomButtonLayout;

	/**
	 * <s>생성자로 Activity의 Context와 팝업의 Title, 버튼의 개수와 이름을 정하는 int 배열이 필요다.
	 *
	 * @param context
	 * @param title
	 * @param buttons
	 */
	public BaseVariableDialog(Context context, String title, int[] buttons) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		String[] names = getStringArrayFromResource(buttons);
		mButtonResource = buttons;
		init(title, names);
	}

	/**
	 * 생성자로 Activity의 Context와 팝업의 Title, 버튼의 개수와 이름을 정하는 int 배열이 필요다.
	 *
	 * @param context
	 * @param title
	 * @param buttons
	 */
	public BaseVariableDialog(Context context, Spanned title, int[] buttons) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		String[] names = getStringArrayFromResource(buttons);
		mButtonResource = buttons;
		init(title, names);
	}

	/**
	 * <s>생성자로 Activity의 Context와 팝업의 Title, 버튼의 개수와 이름을 정하는 String 배열이 필요다.
	 *
	 * @param context
	 * @param title
	 *            title이 있다면 넣고 없다면 0
	 * @param buttons
	 *            리소스 id가 들어있는 배열 버튼이 없다면 null
	 */
	public BaseVariableDialog(Context context, int title, int[] buttons) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		String titlename = null;
		if (title < 0) {
			titlename = null;
		} else {
			titlename = getStringResouce(title);
		}

		String[] names = getStringArrayFromResource(buttons);
		mButtonResource = buttons;
		init(titlename, names);
	}

	public BaseVariableDialog(Context context, String title, String[] buttons) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		init(title, buttons);
	}

	public BaseVariableDialog(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		init(null, null);
	}

	/**
	 * 초기화 작업을 한다.<br>
	 * 레이아웃과 초기입력 값에 따라 서브타이틀 버튼 등을 생성한다.
	 *
	 * @param title
	 *            입력받은 title
	 * @param buttons
	 *            입력받은 StringArray
	 */
	protected void init(Object title, String[] buttons) {
		WindowManager.LayoutParams winLayoutParam = new WindowManager.LayoutParams();
		winLayoutParam.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		winLayoutParam.dimAmount = 0.5f;
		winLayoutParam.windowAnimations = mAniStyle;
		getWindow().setAttributes(winLayoutParam);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

		setContentView(R.layout.common_dialog_layout);

		mInflater = LayoutInflater.from(this.getContext());

		mDialog = (LinearLayout) this.findViewById(R.id.common_dialog_top_layer);
		mTitleLayer = (FrameLayout) this.findViewById(R.id.common_dialog_title_layer);
		mTitleText = (TextView) this.findViewById(R.id.common_dialog_title);
		mBodyLayer = (LinearLayout) this.findViewById(R.id.common_dialog_body_layer);
		mBodyLayerBackground = (ImageView) this.findViewById(R.id.common_dialog_body_layer_background);
		mBodyScrollWrapper = (ScrollView) this.findViewById(R.id.common_dialog_body_scroll_wrapper);
		mBodyLayout = (LinearLayout) this.findViewById(R.id.common_dialog_body_scroll);

		mButtonLayer = (FrameLayout) this.findViewById(R.id.common_dialog_button_layer);
		mButtonLayout = (LinearLayout) this.findViewById(R.id.common_dialog_button_linearlayout);

		if (title != null) {
			if (title instanceof Spanned) {
				setTitle((Spanned) title);
			} else if (title instanceof String) {
				setTitle((String) title);
			}
			setTitleLayerVisibility(View.VISIBLE);
		} else {
			//round처리된 background
			//setBodyBackground(getContext().getResources().getDrawable(R.drawable.dialog_rounded_title_background));
			findViewById(R.id.common_dialog_top_layer_divider).setVisibility(View.GONE);
			setTitleLayerVisibility(View.GONE);
		}

		setBodyLayoutOrientation(LinearLayout.VERTICAL);

		setButton(buttons);

		setOnClickListener(mOnClickListener);
		registerConfigurationChanged();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
	}

	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
	}

	@Override
	public void onWindowAttributesChanged(WindowManager.LayoutParams params) {
		// TODO Auto-generated method stub
		super.onWindowAttributesChanged(params);
	}

	@Override
	public void onContentChanged() {
		// TODO Auto-generated method stub
		super.onContentChanged();
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		if (mBodyLayout != null)
			mBodyLayout.post(setDialogBodyHeight);
		super.show();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (mDialogKeyListener != null) {
			return mDialogKeyListener.onKeyDown(BaseVariableDialog.this,
					keyCode, event);
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (mDialogKeyListener != null) {
			return mDialogKeyListener.onKeyUp(BaseVariableDialog.this, keyCode,
					event);
		} else {
			return super.onKeyUp(keyCode, event);
		}
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		super.hide();
	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		super.cancel();
	}

	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		unregisterConfigurationChanged();
	}

	/**
	 * 가로세로 회전을 감지하기 위해 receiver를 등록한다.
	 */
	private void registerConfigurationChanged() {
		IntentFilter intent = new IntentFilter(
				Intent.ACTION_CONFIGURATION_CHANGED);
		this.getContext().registerReceiver(mRotateReceiver, intent);
	}

	private void unregisterConfigurationChanged() {
		if (!unregistered) {
			unregistered = true;
			this.getContext().unregisterReceiver(mRotateReceiver);
		}
	}

	/**
	 * 팝업을 layout을 재구성한다.
	 */
	public void refreshBackground() {
		mBodyLayout.post(setDialogBodyHeight);
	}

	public int getdpi() {
		int dpi = 0;
		DisplayMetrics metrics = new DisplayMetrics();
		getWindow().getWindowManager().getDefaultDisplay().getMetrics(metrics);

		switch (metrics.densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
		case DisplayMetrics.DENSITY_MEDIUM:
		case DisplayMetrics.DENSITY_HIGH:
			dpi = metrics.densityDpi;
			break;
		default:
			if (metrics.densityDpi < DENSITY_XXH
					&& metrics.densityDpi > DisplayMetrics.DENSITY_HIGH) {
				dpi = DENSITY_XH;
			} else if (metrics.densityDpi < DENSITY_XXXH
					&& metrics.densityDpi > DENSITY_XH) {
				dpi = DENSITY_XXH;
			} else if (metrics.densityDpi > DENSITY_XXH) {
				dpi = DENSITY_XXXH;
			} else {
				dpi = metrics.densityDpi;
			}
			break;
		}

		return dpi;
	}

	/**
	 * 리소스 값에 해당하는 String을 가져온다.
	 *
	 * @param resid
	 *            리소스 id
	 * @return
	 */
	protected String getStringResouce(int resid) {
		String ret = null;

		if (resid > 0) {
			ret = this.getContext().getResources().getString(resid);
		}

		return ret;
	}

	/**
	 * 리소스 값에 해당하는 color값을 가져온다.
	 *
	 * @param resid
	 * @return
	 */
	protected int getColorResource(int resid) {
		int color = 0;

		if (resid > 0) {
			color = this.getContext().getResources().getColor(resid);
		}

		return color;
	}

	/**
	 * 리소스배열을 각 리소스에 해당하는 String배열로 변경한다.
	 *
	 * @param resid
	 *            리소스 id가 들어있는 int 배열
	 * @return
	 */
	protected String[] getStringArrayFromResource(int[] resid) {
		String names[] = null;
		if (resid != null) {
			names = new String[resid.length];
			for (int i = 0; i < resid.length; i++) {
				names[i] = getStringResouce(resid[i]);
			}
		}

		return names;
	}

	/**
	 * 입력받은 String Array가 null인지 체크하고 null이라면 Button영역을 지운다.<br>
	 * null이 아니라면 버튼을 추가하는 함수로 String Array를 전달한다.
	 *
	 * @param buttons
	 *            입력받은 StringArray
	 */
	public void setButton(String[] buttons) {
		if (buttons != null) {
			switch (buttons.length) {
			case 0:
				//mButtonLayer.setVisibility(View.GONE);
				break;
			default:
				mButtonLayer.setVisibility(View.VISIBLE);
				addButton(buttons);
				break;
			}
		} else {
			//mButtonLayer.setVisibility(View.GONE);
		}
	}
	
	/**
	 * 입력받은 String 어레이만큼 버튼을 생성하고 ButtonLayout에 추가<br>
	 * 만약 이미 추가된 버튼이 있다면 모두 지우고 새로 추가한다.
	 *
	 * @param buttons
	 *            입력받은 StringArray
	 */
	void addButton(String[] buttons) {
		if (mButtonLayout.getChildCount() != 0) {
			mButtonLayout.removeAllViews();
		}
		int button_count = 0;
		for (int i = 0; i < buttons.length; i++) {
			if (buttons[i] != null && !buttons[i].equals("")) {
				button_count++;
			}
		}
		mButtons = new Button[button_count];
		int temp_count = 0;
		for (int i = 0; i < buttons.length; i++) {
			if (buttons[i] != null && !buttons[i].equals("")) {
				mButtons[temp_count] = createButton(buttons[i], temp_count,
						button_count);
				mButtonLayout.addView(mButtons[temp_count]);
				if (i != button_count-1) {
					View line = createLine();
					mButtonLayout.addView(line);
				}

				temp_count++;
			}
		}
	}

	/**
	 * 버튼을 생성한다.
	 *
	 * @param name
	 *            버튼의 이름
	 * @param id
	 *            버튼의 id
	 * @param totalCount
	 *            버튼의 총 개수
	 * @return 새성된 버튼
	 */
	Button createButton(String name, int id, int totalCount) {
		Button button = new Button(this.getContext());
		LinearLayout.LayoutParams lParam = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		lParam.gravity = Gravity.CENTER_VERTICAL;
		lParam.weight = 1;
//		if (id < totalCount - 1) {
//			lParam.rightMargin = 2;
//		}
		if(totalCount == 2 && id == totalCount - 2) {
			button.setLayoutParams(lParam);
			button.setId(id);
			button.setText(name);
			int textSize = (int) getContext().getResources().getDimension(
					R.dimen.dialog_base_btn_txt_size);
			button.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			button.setTextColor(getContext().getResources().getColor(R.color.common_argb_50p_89_89_89));
//			button.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//			button.setPaintFlags(button.getPaintFlags() | Paint.FAKE_BOLD_TEXT_FLAG);
			button.setBackground(getContext().getResources().getDrawable(R.drawable.selector_button_popup));
		} else {
			button.setLayoutParams(lParam);
			button.setId(id);
			button.setText(name);
			int textSize = (int) getContext().getResources().getDimension(
					R.dimen.dialog_base_btn_txt_size);
			button.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			button.setTextColor(getContext().getResources().getColor(R.color.common_rgb_89_89_89));
//			button.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//			button.setPaintFlags(button.getPaintFlags() | Paint.FAKE_BOLD_TEXT_FLAG);
			button.setBackground(getContext().getResources().getDrawable(R.drawable.selector_button_popup));
		}
		return button;
	}

	View createLine() {
		View view = new View(getContext());
		LayoutParams lParam = new LayoutParams(1,
				LayoutParams.MATCH_PARENT);
		lParam.gravity = Gravity.CENTER_VERTICAL;
		view.setLayoutParams(lParam);
		view.setBackgroundResource(R.color.common_white);
		return view;
	}

	/**
	 * inflater로 각 xml에서 바디를 가져온다.<br>
	 * 리스트형태의 바디는 다른 곳에 bodylayer에 추가되어야 한다.
	 *
	 * @param bodytype
	 */
	protected void createBody(BodyType bodytype) {
		mBodyType = bodytype;
		switch (bodytype) {
		case InputType:
			mInflater.inflate(R.layout.common_dialog_bodytype_input,
					mBodyLayout);
			mVariableBody = (LinearLayout) mBodyLayout.getChildAt(0);
			break;
		case ListType:
			mInflater.inflate(R.layout.common_dialog_bodytype_list, mBodyLayer);
			mVariableBody = (LinearLayout) mBodyLayer.getChildAt(2);
			break;
		case TextType:
			mInflater
					.inflate(R.layout.common_dialog_bodytype_text, mBodyLayout);
			mVariableBody = (LinearLayout) mBodyLayout.getChildAt(0);
			break;
		case CheckBox:
			mInflater.inflate(R.layout.common_dialog_bodytype_checkbox,
					mBodyLayout);
			mVariableBody = (LinearLayout) mBodyLayout.getChildAt(0);
			break;
		case RadioListType:
			mInflater.inflate(R.layout.common_dialog_bodytype_radio,
					mBodyLayout);
			mVariableBody = (LinearLayout) mBodyLayout.getChildAt(0);
			break;
		case LoadingType:
			mInflater.inflate(R.layout.common_dialog_bodytype_loading, mBodyLayout);
			mVariableBody = (LinearLayout) mBodyLayout.getChildAt(0);
			break;
		default:
			mInflater.inflate(R.layout.common_dialog_bodytype_custom,
					mBodyLayout);
			mVariableBody = (LinearLayout) mBodyLayout.getChildAt(0);
			break;
		}

	}

	/**
	 * Body에 CustomType의 View를 추가한다. 기존 Body의 Gone처리가 필요하면 따로 해줘야한다.
	 *
	 * @param customBody
	 *            추가할 View
	 */
	public void addCustomBody(View customBody) {
		if (customBody != null) {
			mCustomBody = customBody;
			mBodyLayout.addView(mCustomBody);
		}
	}

	public void addCustomBodyTypeList(View customBody) {
		if (customBody != null) {
			mCustomBody = customBody;
			mBodyLayer.addView(mCustomBody);
		}
	}

	/**
	 * Title에 CustomType의 View를 추가한다 (ex ImageView) <br>
	 * 자동으로 기존의 타이틀은 Gone처리된다.
	 * 
	 * @param customTitle
	 */
	public void addCustomTitle(View customTitle) {
		if (customTitle != null) {
			mCustomTitle = customTitle;
			mTitleLayer.addView(mCustomTitle);
			setTitleVisibility(View.GONE);
		}
	}

	/**
	 * Body에 CustomType의 View를 추가한다. 기존 Body의 Gone처리가 필요하면 따로 해줘야한다.
	 *
	 */
	public void addCustomButtonLayout(View customButton) {
		if (customButton != null) {
			mCustomButtonLayout = customButton;
			mButtonLayer.addView(mCustomButtonLayout);
		}
	}

	/**
	 * Body에 추가한 CustomType의 View를 삭제한다.
	 */
	public void removeCustomBody() {
		if (mCustomBody != null) {
			mBodyLayout.removeView(mCustomBody);
		}
	}

	/**
	 * CustomBody를 가져올 수 있다.
	 *
	 * @return mCustomBody
	 */
	public View getCustomBody() {
		return mCustomBody;
	}

	/**
	 * customButtonlayout을 가져온다.
	 *
	 * @return
	 */
	public View getCustomButtonLayout() {
		return mCustomButtonLayout;
	}

	/**
	 * 기존 Body의 Visibility를 조절한다.
	 *
	 * @param visibility
	 */
	public void setVariableBodyVisibility(int visibility) {
		if (mVariableBody != null)
			mVariableBody.setVisibility(visibility);
	}

	/**
	 * 기존 Button들의 Visibility를 조절한다. visibility 변경해도 버튼 영역 자체는 남아있다.
	 *
	 * @param visibility
	 */
	public void setVariableButtonLayoutVisibility(int visibility) {
		if (mButtonLayout != null)
			mButtonLayout.setVisibility(visibility);
	}

	/**
	 * 기존 Button영역 자체의 visibility를 조절한다. visibility 변경하면 버튼 영역 자체가 사라진다.
	 *
	 * @param visibility
	 */
	public void setButtonLayerVisibility(int visibility) {
		if (mButtonLayer != null) {
			mButtonLayer.setVisibility(visibility);
		}
		View view = this.findViewById(R.id.common_dialog_body_divider);
		if (view != null) {
			view.setVisibility(visibility);
		}
	}

	/**
	 * 기존 Body를 가져올 수 있다.
	 *
	 * @return
	 */
	public LinearLayout getVariableBody() {
		return mVariableBody;
	}

	/**
	 * LinearLayout.HORIZONTAL or LinearLayout.VERTICAL
	 *
	 * @param orientation
	 */
	public void setBodyLayoutOrientation(int orientation) {
		mBodyLayout.setOrientation(orientation);
	}

	/**
	 * 타이틀이 들어가 있는 레이아웃을 가져올 수 있다.
	 *
	 * @return
	 */
	protected FrameLayout getTitleLayer() {
		return mTitleLayer;
	}

	/**
	 * 타이틀 TextView를 가져올 수 있다.
	 *
	 * @return TextView mTitleText;
	 */
	TextView getTitleTextView() {
		return mTitleText;
	}

	/**
	 * 팝업의 바디가 들어가 있는 레이아웃을 가져올 수 있다.
	 *
	 * @return LinearLayout mBody
	 */
	protected LinearLayout getBodyLayout() {
		return mBodyLayout;
	}

	/**
	 * 버튼들과 배경이 들어가 있는 레이아웃을 가져올 수 있다.
	 *
	 * @return FrameLayout mButtonLayer
	 */
	protected FrameLayout getButtonLayer() {
		return mButtonLayer;
	}

	/**
	 * 생성된 버튼들이 들어있는 배열을 돌려준다.
	 *
	 * @return Button[] mButtons
	 *
	 */
	public Button[] getButtons() {
		return mButtons;
	}

	public void setButtonBackgroundRes(int drawres) {
		if (mButtons != null) {
			for (int i = 0; i < mButtons.length; i++) {
				mButtons[i].setBackgroundResource(drawres);
			}
		}
	}

	/**
	 * 추가한 버튼을 리스너에 등록해 준다. Button의 ID는 등록된 array 순서대로 전달된다. ex) {확인, 취소} 라면
	 * Button id는 0, 1이다.
	 *
	 * @param onClickListener
	 */
	public void setOnClickListener(View.OnClickListener onClickListener) {
		if (null != onClickListener) {
			mButtonClickListener = onClickListener;
			if (mButtons != null) {
				for (int i = 0; i < mButtons.length; i++) {
					mButtons[i].setOnClickListener(mButtonClickListener);
				}
			}
		}
	}
	
	/**
	 * 버튼의 활성화/비활성화를 조절할 수 있다.
	 *
	 * @param enabled
	 */
	public void setBtnEnable(boolean enabled) {
		if (mButtons != null) {
			for (int i = 0; i < mButtons.length; i++) {
				mButtons[i].setEnabled(enabled);
			}
		}
	}

	/**
	 * 리턴 받을 결과값이 버튼 뿐일 시에 버튼이 눌린 결과 값을 전달해 줄 interface를 등록한다.
	 *
	 * @param dialogClickListener
	 *            버튼이 눌린 결과값을 전달해 줄 Interface
	 */
	public void setDialogButtonOnClickListener(
			DialogButtonOnClickListener dialogClickListener) {
		this.mDialogClickListener = dialogClickListener;
	}

	/**
	 * 팝업에서 출력된 상태에서 모든 key를 필터링하고자 할 때 등록하는 listener이다.
	 *
	 * @param dialogkeylistener
	 */
	public void setDialogKeyListener(DialogKeyListener dialogkeylistener) {
		mDialogKeyListener = dialogkeylistener;
	}

	/**
	 * 타이틀을 설정해준다.
	 *
	 * @param title
	 *            설정할 타이틀명
	 */
	@Override
	public void setTitle(CharSequence title) {
		// TODO Auto-generated method stub
		// super.setTitle(title);
		mTitleText.setText(title);
		if (title != null) {
			if (mTitleText.getVisibility() != View.VISIBLE) {
				setTitleVisibility(View.VISIBLE);
			}
		} else {
			setTitleVisibility(View.GONE);
		}

	}

	/**
	 * 타이틀을 설정해준다.
	 *
	 * @param title
	 *            설정할 타이틀명
	 */
	public void setTitle(String title) {
		mTitleText.setText(title);
		if (title != null) {
			if (mTitleText.getVisibility() != View.VISIBLE) {
				setTitleVisibility(View.VISIBLE);
			}
		} else {
			setTitleVisibility(View.GONE);
		}
	}

	/**
	 * 타이틀을 설정해준다.
	 *
	 * @param title
	 *            설정할 타이틀명
	 */
	public void setTitle(Spanned title) {
		mTitleText.setText(title);
		if (title != null) {
			if (mTitleText.getVisibility() != View.VISIBLE) {
				setTitleVisibility(View.VISIBLE);
			}
		} else {
			setTitleVisibility(View.GONE);
		}
	}

	/**
	 * 타이틀을 설정해준다.
	 *
	 * @param title
	 *            설정할 타이틀명
	 */
	public void setTitle(SpannableStringBuilder title) {
		mTitleText.setText(title);
	}

	/**
	 * 기존 타이틀의 Visibility를 조정한다.
	 *
	 * @param visibility
	 */
	public void setTitleVisibility(int visibility) {
		mTitleText.setVisibility(visibility);
	}

	/**
	 * 기존 타이틀 영역에 Visibility를 조정한다.
	 *
	 * @param visibility
	 */
	public void setTitleLayerVisibility(int visibility) {
		mTitleLayer.setVisibility(visibility);
	}

	@SuppressWarnings("deprecation")
	public void setBodyBackground(Drawable draw) {
		mBodyLayerBackground.setBackgroundDrawable(draw);
	}

	/**
	 * bodylayer를 가져 온다.
	 *
	 * @return
	 */
	public LinearLayout getBodyLayer() {
		return mBodyLayer;
	}

	/**
	 * 팝업의 최상위 layout을 가져온다.
	 *
	 * @return
	 */
	public View getDialog() {
		return mDialog;
	}

	/**
	 * 버튼을 클릭하면 해당 함수가 호출된다.<br>
	 * 이 함수를 override시에 super에 값을 전달하지 않으면 DialogButtonOnClickListener를 등록해도 사용할
	 * 수 없다.
	 *
	 * @param id
	 */
	protected void OnClick(int id) {
		if (mDialogClickListener != null) {
			mDialogClickListener.onClick(BaseVariableDialog.this, id);
			dismiss();
		}
	}

	public int getBiggestRecord(ArrayList<Integer> list) {
		int biggest = 0;
		for (int i = 0; i < list.size(); i++) {
			int temp = list.get(i);
			if (biggest < temp) {
				biggest = temp;
			}
		}
		return biggest;
	}

	public int getTotalHeightofListView(ListView listView) {
		if (listView == null) {
			return 0;
		}

		ListAdapter mAdapter = listView.getAdapter();

		int totalHeight = 0;

		if (mAdapter.getCount() > 10) {
			int averageCount = (int) Math.floor(mAdapter.getCount() / 3);
			int tempHeight = 0;
			for (int i = 0; i < 3; i++) {
				View mView = mAdapter.getView(averageCount + averageCount * i
						- 1, null, listView);
				mView.measure(
						MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
						MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
				tempHeight += mView.getMeasuredHeight();
			}
			int averageHeight = (int) Math.floor(tempHeight / 3);
			totalHeight = averageHeight * mAdapter.getCount();
		} else {
			for (int i = 0; i < mAdapter.getCount(); i++) {
				View mView = mAdapter.getView(i, null, listView);

				mView.measure(
						MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
						MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

				totalHeight += mView.getMeasuredHeight();
			}
		}

		return totalHeight;
	}

	public void setBodyMargin(int left, int top, int right, int bottom) {
		FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mDialog.getLayoutParams();
		if (params != null) {
			params.setMargins(left, top ,right, bottom);
		}
		mDialog.setLayoutParams(params);
	}

	public void setFullScreenMode(boolean mode) {
		isFullScreenMode = mode;
		if (isFullScreenMode) {
			mBodyScrollWrapper.setFillViewport(true);
		}
	}

	/**
	 * <b>바디의 형태를 정하는 Enum </b><br>
	 * -ListType 바디가 ListType이라면 사용한다. (Radio팝업 제외)<br>
	 * -DuplicateType 중복체크할 때 사용한다.<br>
	 * -InformationType 파일이나 폴더의 정보를 볼 때 사용한다.<br>
	 * -RadioListType 리스트에 RadioButton 달려있을 시에 사용한다.<br>
	 * -TextType 단순 Text나 TextView가 두개 있을 시에 사용한다.<br>
	 *
	 * @author Jang
	 *
	 */
	public enum BodyType {
		ListType, InputType, TextType, CheckBox, RadioListType, LoadingType, Custom
	}

	/**
	 * 팝업에서 생성된 버튼이 눌려지는 걸 감지하기 위해 사용하는 Listener<br>
	 * 몇몇 팝업에서는 해당 Listener를 등록하여도 사용하지 못하는 경우가 있다.
	 *
	 * @author Jang
	 *
	 */
	public interface DialogButtonOnClickListener {
		void onClick(DialogInterface dialog, int id);
	}

	/**
	 * 팝업이 호출된 상태에서 눌려지는 모든 key를 전달받기 위한 Listener
	 *
	 * @author Jang
	 *
	 */
	public interface DialogKeyListener {
		boolean onKeyDown(DialogInterface dialog, int keyCode,
								 KeyEvent event);

		boolean onKeyUp(DialogInterface dialog, int keyCode,
							   KeyEvent event);
	}

}
