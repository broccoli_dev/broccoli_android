package com.nomadconnection.broccoli.common.dialog;

import android.content.Context;

/**
 * Created by YelloHyunminJang on 2016. 12. 6..
 */

public class CommonDialogCustomType extends BaseVariableDialog {

    /**
     * 입력 팝업의 생성자
     * @param context
     * @param title_resid
     * @param buttons
     */
    public CommonDialogCustomType(Context context, int title_resid, int[] buttons) {
        super(context, title_resid, buttons);
        init();
    }

    /**
     * 입력 팝업의 생성자
     * @param context
     * @param title
     * @param buttons
     */
    public CommonDialogCustomType(Context context, String title, int[] buttons) {
        super(context, title, buttons);
        init();
    }

    /**
     * 초기화 작업
     */
    private void init() {
        this.createBody(BodyType.Custom);
    }

}
