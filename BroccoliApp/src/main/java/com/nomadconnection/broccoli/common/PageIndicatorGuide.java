package com.nomadconnection.broccoli.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.nomadconnection.broccoli.R;

public class PageIndicatorGuide extends RadioGroup {
	
	private int mCount = 0;
	private int mCurrentButtonResId = 0;
	private int mGuideMargin = 0;
	
	public PageIndicatorGuide(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(null);
	}
	
	public PageIndicatorGuide(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(attrs);
	}
	
	@Override
	protected Parcelable onSaveInstanceState() {
		// TODO Auto-generated method stub
		return super.onSaveInstanceState();
	}
	
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(state);
	}

	void init(AttributeSet attrs) {
		//this.setBackgroundColor(getResources().getColor(android.R.color.transparent));
		this.setOrientation(RadioGroup.HORIZONTAL);

		if (attrs != null) {
			TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.PageIndicatorGuide);
			mCurrentButtonResId = ta.getResourceId(R.styleable.PageIndicatorGuide_indicatorDrawable, -1);
			mGuideMargin = ta.getDimensionPixelSize(R.styleable.PageIndicatorGuide_indicatorMargin, 0);
			ta.recycle();
		}
	}

	public void setButtonDrawable(int res) {
		mCurrentButtonResId = res;
		updatePageCount();
	}
	
	public void setPageTotalCount(int count, int margin) {
		mCount = count;
		mGuideMargin = margin;
		for (int i=0; i < count; i++) {
			addPageCount(i);
		}
	}
	
	public void setPageTotalCount(int count) {
		mCount = count;
		for (int i=0; i < count; i++) {
			addPageCount(i);
		}
	}
	
	public void updatePageCount() {
		this.removeAllViews();
		for (int i=0; i < mCount; i++) {
			addPageCount(i);
		}
	}
	
	private void addPageCount(int index) {
		RadioButton button = createRadioButton(this.getContext(), index);
		this.addView(button);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		params.weight = 1;
		if (mGuideMargin != 0) {
			params.rightMargin = mGuideMargin;
		}
		button.setLayoutParams(params);
		if (mCurrentButtonResId > 0) {
			button.setButtonDrawable(mCurrentButtonResId);
		}
	}
	
	private RadioButton createRadioButton(Context context, int index) {
		RadioButton button = new RadioButton(context);
		button.setId(index);
		button.setGravity(Gravity.CENTER);
		return button;
	}
	
	public void setEntirePageCountBackgroundDrawable(int resid) {
		for (int i=0; i < this.getChildCount(); i++) {
			RadioButton button = (RadioButton) this.getChildAt(i);
			button.setBackgroundResource(resid);
		}
	}
	
	public void setEntirePageCountButtonDrawable(int resid) {
		mCurrentButtonResId = resid;
		for (int i=0; i < this.getChildCount(); i++) {
			RadioButton button = (RadioButton) this.getChildAt(i);
			button.setButtonDrawable(resid);
		}
	}
	
	public void setRadioButtonDrawable(int resid) {
		mCurrentButtonResId = resid;
		setEntirePageCountButtonDrawable(mCurrentButtonResId);
	}
	
	public void setGuideMargin(int margin) {
		mGuideMargin = margin;
		updatePageCount();
	}
	
	public void setPageCount(int count) {
		mCount = count;
		updatePageCount();
	}
	
	@Override
	public void check(int id) {
		// TODO Auto-generated method stub
		if (mCount >= id) {
			super.check(id);
		}
	}
	
	@Override
	protected void detachAllViewsFromParent() {
		// TODO Auto-generated method stub
		super.detachAllViewsFromParent();
	}
	
}
