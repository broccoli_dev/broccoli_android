package com.nomadconnection.broccoli.common.dialog;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;

import com.nomadconnection.broccoli.R;

public class CommonDialogLoadingType extends BaseVariableDialog {
	
	private ImageView mLoadingImage;

	public CommonDialogLoadingType(Context context) {
		super(context);
		init();
	}

	private void init() {
		this.createBody(BodyType.LoadingType);

		mLoadingImage		= (ImageView)findViewById(R.id.dialog_loading_img);

		AnimationDrawable frameAnimation = (AnimationDrawable) mLoadingImage.getDrawable();
		frameAnimation.setCallback(mLoadingImage);
		frameAnimation.setVisible(true, true);
		frameAnimation.start();
	}
	
}
