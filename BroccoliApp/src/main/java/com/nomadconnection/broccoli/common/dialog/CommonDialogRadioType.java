package com.nomadconnection.broccoli.common.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.LayoutParams;
import android.widget.RadioGroup.OnCheckedChangeListener;
import com.nomadconnection.broccoli.R;

import java.util.ArrayList;


/**
 * RadioGroup안에 라이오버튼들이 존재하는 팝업이다.
 * @author Jang
 * @param <T>
 *
 */
public class CommonDialogRadioType<T> extends BaseVariableDialog {
	
	/**
	 * 현재 check된 라이오버튼의 위치값과 id값을 받을 수 잇는 Listener
	 * @author Jang
	 *
	 */
	public static interface DialogRadioTypeListener {
		public void onClick(DialogInterface dialog, int checkedresid, int position, Object select);
	}

	private static final int DIVIDER1_COLOR = 255;

	private static final int DIVIDER2_COLOR = 202;
	
	private RadioGroup mRadioGroup;
	private DialogRadioTypeListener mListener;
	private RadioButton[] mRadioButton;
	
	private int[] mRadioButtonRes = null;
	private ArrayList<T> mRadioButtonResText = null;
	private boolean isDisableButton = false;
	

	public CommonDialogRadioType(Context context, int title, int[] radiobuttons) {
		super(context, title, new int[] {R.string.common_confirm, R.string.common_cancel});
		init(radiobuttons);
	}
	
	public CommonDialogRadioType(Context context, String title, ArrayList<String> radiobuttons) {
		super(context, title, new int[] {R.string.common_cancel});
		isDisableButton = true;
		init(radiobuttons);
	}
	
	public CommonDialogRadioType(Context context, String title, String[] radiobuttons) {
		super(context, title, new int[] {R.string.common_cancel});
		isDisableButton = true;
		
		ArrayList<String> strArray = new ArrayList<String>();
		if(radiobuttons != null) {
			for(int i = 0; i < radiobuttons.length; i++) {
				strArray.add(radiobuttons[i]);
			}
		}
		init(strArray);
	}

	/**
	 * 초기화 작업
	 * @param radiobuttons
	 */
	private void init(int[] radiobuttons) {
		this.createBody(BodyType.RadioListType);
		
		mRadioGroup = (RadioGroup) getBodyLayout().findViewById(R.id.dialog_bodytype_radio);
		mRadioGroup.setOnCheckedChangeListener(onCheckChangeListener);
		setRadioButtons(radiobuttons);
	}
	
	/**
	 * 초기화 작업
	 * @param radiobuttons
	 */
	private void init(ArrayList<String> radiobuttons) {
		this.createBody(BodyType.RadioListType);
		
		mRadioGroup = (RadioGroup) getBodyLayout().findViewById(R.id.dialog_bodytype_radio);
		mRadioGroup.setOnCheckedChangeListener(onCheckChangeListener);
		setRadioButtons(radiobuttons);
	}
	
	/**
	 * ArrayList<String>을 받아서 라디오버튼 생성
	 * @param radiobuttons
	 */
	@SuppressWarnings("unchecked")
	public void setRadioButtons(ArrayList<String> radiobuttons) {
		if (radiobuttons != null) {
			mRadioButtonResText = (ArrayList<T>) radiobuttons;
			mRadioButton = new RadioButton[radiobuttons.size()];
			for (int i=0; i < radiobuttons.size(); i++) {
				addRadioButton(radiobuttons.get(i), i, radiobuttons.size());
			}
			mRadioGroup.clearCheck();
		}
	}
	
	/**
	 * 버튼을 생성하고 라디오그룹에 추가
	 * @param text
	 * @param id
	 * @param total
	 */
	private void addRadioButton(String text, int id, int total) {
		mRadioButton[id] = createRadioButton(text, id);
		if (id > 0 && id < total) 
		{
			mRadioGroup.addView(createLine(Color.rgb(DIVIDER1_COLOR, DIVIDER1_COLOR, DIVIDER1_COLOR)));
			mRadioGroup.addView(createLine(Color.rgb(DIVIDER2_COLOR, DIVIDER2_COLOR, DIVIDER2_COLOR)));
		}
		mRadioGroup.addView(mRadioButton[id]);
	}
	
	/**
	 * int array를 받아서 라디오버튼 생성
	 * @param radiobuttons
	 */
	public void setRadioButtons(int[] radiobuttons) {
		if (radiobuttons != null) {
			mRadioButtonRes = radiobuttons;
			mRadioButton = new RadioButton[radiobuttons.length];
			for (int i=0; i < radiobuttons.length; i++) {
				addRadioButton(radiobuttons[i], i, radiobuttons.length);
			}
		} else {
			isDisableButton = true;
		}
	}
	
	/**
	 * 버튼을 생성하고 라디오그룹에 추가
	 * @param text_res
	 * @param id
	 * @param total
	 */
	private void addRadioButton(int text_res, int id, int total) {
		String text = null;
		if (text_res > 0) {
			text = this.getStringResouce(text_res);
		}
		mRadioButton[id] = createRadioButton(text, id);
		if (id > 0 && id < total) 
		{
			mRadioGroup.addView(createLine(Color.rgb(DIVIDER1_COLOR, DIVIDER1_COLOR, DIVIDER1_COLOR)));
			mRadioGroup.addView(createLine(Color.rgb(DIVIDER2_COLOR, DIVIDER2_COLOR, DIVIDER2_COLOR)));
		}
		mRadioGroup.addView(mRadioButton[id]);
	}
	
	/**
	 * 라디오버튼 생성
	 * @param text
	 * @param id
	 * @return
	 */
	private RadioButton createRadioButton(String text, int id) {
		RadioButton button = (RadioButton) mInflater.inflate(
				R.layout.common_dialog_bodytype_radio_item, null);
		RadioGroup.LayoutParams lParam = new RadioGroup.LayoutParams(
				LayoutParams.MATCH_PARENT, (int) getContext().getResources().getDimension(R.dimen.common_list_height));
		button.setEllipsize(TruncateAt.MARQUEE);
		button.setSingleLine(true);
		button.setLayoutParams(lParam);
		int textSize = 15;
		if (getdpi() == DisplayMetrics.DENSITY_MEDIUM) {
			textSize = 24;
		}
		button.setTextSize(textSize);
		button.setText(text);
		button.setId(id);
		button.setChecked(false);
		return button;
	}

	/**
	 * 구분자 생성
	 * @param color
	 * @return
	 */
	private View createLine(int color) {
		View line = new View(this.getContext());
		line.setLayoutParams(new RadioGroup.LayoutParams(LayoutParams.MATCH_PARENT, 1));
		line.setBackgroundColor(color);
		return line;
	}
	
	/**
	 * 확인 버튼 텍스트 변경용
	 * @param text
	 */
	public void setConfirmBtn(String text) {
		if(getButtons() != null){
			Button[] okBtn = getButtons();
			if(okBtn.length > 1){
				okBtn[0].setText(text);
			}
		}
	}
	
	/**
	 * 라디오그룹에서 check할 라디오버튼을 선택한다.
	 * @param position
	 * @param check
	 */
	public void setCheck(int position, boolean check) {
		if (mRadioButton != null) {
			mRadioButton[position].setChecked(check);
		}
	}
	
	/**
	 * {@link CommonDialogRadioType}를 등록한다.
	 * @param listener
	 */
	public void setDialogListener(DialogRadioTypeListener listener) {
		mListener = listener;
	}
	
	/**
	 * 특정 위치에 추가된 라이오버튼을 가져온다.
	 * @param position
	 * @return
	 */
	public RadioButton getRadioButton(int position) {
		RadioButton button = null;
		if (mRadioButton != null)
		{
			if (position >= mRadioButton.length) {
				button = null;
			} else {
				button = mRadioButton[position];
			}
		}
		return button;
	}
	
	private OnCheckedChangeListener onCheckChangeListener =  new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedid) {
			// TODO Auto-generated method stub
			if (isDisableButton) {
				if (mListener != null) {
					if (checkedid >= 0) {
						int checked = checkedid;
						Object select = null;
						if(mRadioButtonRes == null || mRadioButtonRes.length ==0){
							checked = checkedid;
							select = mRadioButtonResText.get(checkedid);
						} else {
							checked = mRadioButtonRes[checkedid];
						}
						mListener.onClick(CommonDialogRadioType.this, checked, checkedid, select);
						dismiss();
					}
				}
			}
		}
	};
	
	@Override
	protected void OnClick(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case R.string.common_confirm:
			if (mListener != null) {
				int checkedid = mRadioGroup.getCheckedRadioButtonId();
				if (checkedid >= 0) {
					int checked = 0;
					Object select = null;
					if(mRadioButtonRes == null || mRadioButtonRes.length ==0){
						checked = checkedid;
						select = mRadioButtonResText.get(checkedid);
					} else {
						checked = mRadioButtonRes[checkedid];
					}
					mListener.onClick(CommonDialogRadioType.this, checked, checkedid, select);
				}
			}
			dismiss();
			break;
		case R.string.common_cancel:
			// do nothing
			dismiss();
			break;
		}
	}

}
