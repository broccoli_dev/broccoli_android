package com.nomadconnection.broccoli.common;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hyunmin.materialrefresh.MaterialHeadListener;
import com.hyunmin.materialrefresh.MaterialRefreshLayout;
import com.nomadconnection.broccoli.R;

/**
 * Created by YelloHyunminJang on 16. 6. 13..
 */
public class MainRefreshView extends LinearLayout implements MaterialHeadListener {

    private ViewGroup mContainer;
    private Animation mRotateUpAnim;
    private Animation mRotateDownAnim;
    private final int ROTATE_ANIM_DURATION = 180;
    private Animation.AnimationListener animationListener;
    private TextView mMainTextView;
    private ImageView mImageView;

    public MainRefreshView(Context context) {
        super(context);
        init();
    }

    public MainRefreshView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MainRefreshView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setWillNotDraw(false);
        ViewGroup.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mContainer = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.main_refresh_view, null);
        addView(mContainer, layoutParams);
        setGravity(Gravity.BOTTOM);
        mImageView = (ImageView) findViewById(R.id.default_header_arrow);
        mImageView.setVisibility(View.GONE);
        mMainTextView = (TextView) findViewById(R.id.default_header_textview);

        animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        setupAnimation();
    }

    public void setupAnimation() {

        mRotateUpAnim = new RotateAnimation(0.0f, -180.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mRotateUpAnim.setAnimationListener(animationListener);
        mRotateUpAnim.setDuration(ROTATE_ANIM_DURATION);
        mRotateUpAnim.setFillAfter(true);

        mRotateDownAnim = new RotateAnimation(-180.0f, 0.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mRotateDownAnim.setDuration(ROTATE_ANIM_DURATION);
        mRotateDownAnim.setFillAfter(true);
    }

    public void updateData() {

    }

    public void startAnimation() {
        AnimationDrawable frameAnimation = (AnimationDrawable) mImageView.getDrawable();
        frameAnimation.setCallback(mImageView);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
    }

    public void stopAnimation() {
        AnimationDrawable frameAnimation = (AnimationDrawable) mImageView.getDrawable();
        frameAnimation.setCallback(mImageView);
        frameAnimation.setVisible(true, true);
        frameAnimation.stop();
    }

    public void setTitle(String title) {
        this.mMainTextView.setText(title);
    }

    @Override
    public void onComplete(MaterialRefreshLayout materialRefreshLayout) {
        stopAnimation();
        updateData();
        mMainTextView.setText(R.string.refresh_text_state_complete);
    }

    @Override
    public void onBegin(MaterialRefreshLayout materialRefreshLayout) {
        mImageView.setVisibility(View.GONE);
        mMainTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPull(MaterialRefreshLayout materialRefreshLayout, float fraction) {
        mMainTextView.setText(R.string.refresh_text_state_refresh);
    }

    @Override
    public void onRelease(MaterialRefreshLayout materialRefreshLayout, float fraction) {
        mMainTextView.setText(R.string.refresh_text_state_refresh);
        mImageView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefreshing(MaterialRefreshLayout materialRefreshLayout) {
        mMainTextView.setVisibility(View.GONE);
        mMainTextView.setText(R.string.refresh_text_state_refresh);
        mImageView.setVisibility(View.VISIBLE);
        startAnimation();
    }
}
