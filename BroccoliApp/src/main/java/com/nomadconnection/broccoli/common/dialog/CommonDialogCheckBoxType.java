package com.nomadconnection.broccoli.common.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioGroup.LayoutParams;

import com.nomadconnection.broccoli.R;

import java.util.ArrayList;


/**
 * 팝업에 CheckBox가 복수로 들어갈 수 있는 형태의 팝업 
 * @author Jang
 *
 */
public class CommonDialogCheckBoxType extends BaseVariableDialog {
	
	/**
	 * 팝업에서 확인 누를 시에 현재 check되어 결과값을 전달 받는 Listener
	 * @author Jang
	 *
	 */
	public static interface DialogCheckTypeListener {
		public void onSingleCheck(DialogInterface dialog, int checkedresid, int poistion);
		public void onMulitCheck(DialogInterface dialog, int[] checkedresid, int[] position);
	}

	private static final int DIVIDER1_COLOR = 255;

	private static final int DIVIDER2_COLOR = 202;
	
	private LinearLayout mLinearLayout;
	private DialogCheckTypeListener mListener;

	private boolean isMultiCheck = false;
	private ArrayList<String> mCheckbox = new ArrayList<String >();
	private ArrayList<CheckBox> mList = new ArrayList<CheckBox>();
	private ArrayList<CheckBox> mCheckList = null;

	/**
	 * CheckBox 형태의 팝업 생성자
	 * @param context
	 * @param title
	 * @param checkbox - 생성할 체크박스 resid array
	 * @param multiCheck - multicheck가 가능한지 설정
	 */
	public CommonDialogCheckBoxType(Context context, int title, int[] checkbox, boolean multiCheck) {
		super(context, title, new int[] {R.string.common_confirm, R.string.common_cancel});
		isMultiCheck = multiCheck;
		ArrayList<String> list = new ArrayList<String>();
		for (int i=0; i < checkbox.length; i++) {
			list.add(getStringResouce(checkbox[i]));
		}
		init(list);
	}

	/**
	 * CheckBox 형태의 팝업 생성자
	 * @param context
	 * @param title
	 * @param checkbox - 생성할 체크박스 resid array
	 * @param multiCheck - multicheck가 가능한지 설정
	 */
	public CommonDialogCheckBoxType(Context context, String title, ArrayList<String> checkbox, boolean multiCheck) {
		super(context, title, new int[] {R.string.common_confirm, R.string.common_cancel});
		isMultiCheck = multiCheck;
		init(checkbox);
	}
	
	/**
	 * 초기화 작업
	 * @param checkbox
	 */
	private void init(ArrayList<String> checkbox) {
		this.createBody(BodyType.CheckBox);
		
		mLinearLayout = (LinearLayout) getBodyLayout().findViewById(R.id.dialog_bodytype_checkbox);
		setCheckBox(checkbox);
	}
	
	/**
	 * CheckBox array 개수만큼 CheckBox 생성명령을 내린다. 
	 * @param checkbox
	 */
	private void setCheckBox(ArrayList<String> checkbox) {
		if (checkbox != null) {
			mCheckbox = checkbox;
			for (int i=0; i < checkbox.size(); i++) {
				addCheckBox(checkbox.get(i), i, checkbox.size());
			}
		}
	}
	
	/**
	 * CheckBox를 하나씩 생성하고 생성된 CheckBox를 자동으로 추가해 준다.
	 * @param id
	 * @param total
	 */
	private void addCheckBox(String text, int id, int total) {
		CheckBox checkbox = createCheckBox(text, id);
		checkbox.setOnClickListener(onClickListener);
		mList.add(checkbox);
		if (id > 0 && id < total) {
			mLinearLayout.addView(createLine(Color.rgb(DIVIDER1_COLOR, DIVIDER1_COLOR, DIVIDER1_COLOR)));
			mLinearLayout.addView(createLine(Color.rgb(DIVIDER2_COLOR, DIVIDER2_COLOR, DIVIDER2_COLOR)));
		}
		mLinearLayout.addView(checkbox);
	}
	
	/**
	 * CheckBox를 xml에서 가져와서 생성한다.
	 * @param text
	 * @param id
	 * @return CheckBox
	 */
	private CheckBox createCheckBox(String text, int id) {
		CheckBox box = (CheckBox) mInflater.inflate(R.layout.common_dialog_bodytype_checkbox_item, null);
		box.setText(text);
		box.setId(id);
		return box;
	}
	
	/**
	 * Divider를 생성한다.
	 * @param color
	 * @return View
	 */
	private View createLine(int color) {
		View line = new View(this.getContext());
		line.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
		line.setBackgroundColor(color);
		return line;
	}
	
	/**
	 * 초기에 선택된 값을 set하거나 UI에 반응에 따라 자동으로 check상태를 set한다.
	 * @param position
	 * @param check
	 */
	public void setCheck(int position, boolean check) {
		if (mList != null)
			mList.get(position).setChecked(check);
	}
	
	/**
	 * 복수 check가 불가능할 시에 호출되는 함수이다.
	 * @param id
	 */
	public void checkOnlyOne(int id) {
		if (mList != null) {
			for (int i=0; i < mList.size(); i++) {
				if (mList.get(i).getId() != id) { 
					mList.get(i).setChecked(false);
				}
			}
		}
	}
	
	/**
	 * 현재 check된 Item의 id를 가져온다.
	 * @return
	 */
	public ArrayList<CheckBox> getCheckedItemId() {
		ArrayList<CheckBox> list = new ArrayList<CheckBox>();
		if (mList != null) {
			for (int i=0; i < mList.size(); i++) {
				if (mList.get(i).isChecked()) { 
					list.add(mList.get(i));
				}
			}
		}
		return list;
	}
	
	View.OnClickListener onClickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (!isMultiCheck) {
				checkOnlyOne(v.getId());
			}
		}
	};
	
	/**
	 * {@link DialogCheckTypeListener}를 등록한다.
	 * @param listener
	 */
	public void setDialogListener(DialogCheckTypeListener listener) {
		mListener = listener;
	}
	
	@Override
	protected void OnClick(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case R.string.common_confirm:
			if (mListener != null) {
				mCheckList = getCheckedItemId();
				if (isMultiCheck) {
					int[] checkid = new int[mCheckList.size()];
					int[] checkposition = new int[mCheckList.size()];
					for (int i=0; i < checkid.length; i++) {
						checkposition[i] = mCheckList.get(i).getId();
					}
					mListener.onMulitCheck(CommonDialogCheckBoxType.this, checkid, checkposition);
				} else {
					if (mCheckList.size() > 0) {
						int checkid = mCheckList.get(0).getId();
						mListener.onSingleCheck(CommonDialogCheckBoxType.this, checkid, checkid);
					}
				}
			}
			break;
		case R.string.common_cancel:
			dismiss();
			// do nothing
			break;
		}
	}
	
	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		if (null != mList) {
			mList.clear();
			mList = null;
		}
		if (null != mCheckList) {
			mCheckList.clear();
			mCheckList = null;
		}
	}

}
