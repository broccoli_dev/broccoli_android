package com.nomadconnection.broccoli.http;


import com.google.gson.annotations.SerializedName;

public class Repo {

    @SerializedName("id")
    public String mId;

    @SerializedName("name")
    public String mName;

    @SerializedName("full_name")
    public String mFullName;

    public Repo(String id, String name, String full_name ) {
        this.mId = id;
        this.mName = name;
        this.mFullName = name;
    }
}
