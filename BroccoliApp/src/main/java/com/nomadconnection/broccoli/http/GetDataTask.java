package com.nomadconnection.broccoli.http;

import android.os.AsyncTask;
import android.os.Handler;

import com.nomadconnection.broccoli.config.APP;

public class GetDataTask  extends AsyncTask<Void, Void, Boolean> {

	private Handler mHandler = null;

	public GetDataTask(Handler _handler){
		mHandler = _handler;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// Simulates a background job.
		boolean isTrue = false;
		try {
			Thread.sleep(2000);
			isTrue = true;
		} catch (InterruptedException e) {
			isTrue = false;
		}
		return isTrue;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// Do some stuff here

		// Call onRefreshComplete when the list has been refreshed.
		if(result){
			mHandler.sendEmptyMessage(APP.PTR_GETDATATASK_HANDLER_SUCCESS);	// Success 여부
		}
		else{
			mHandler.sendEmptyMessage(APP.PTR_GETDATATASK_HANDLER_FAIL);	// Fail 여부
		}

		super.onPostExecute(result);
	}
}


