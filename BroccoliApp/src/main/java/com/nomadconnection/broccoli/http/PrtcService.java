package com.nomadconnection.broccoli.http;

import com.nomadconnection.broccoli.data.InfoDataAppVersion;

import java.util.List;

import okhttp3.Response;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface  PrtcService {

	/* Test */
	@GET("/users/{user}/repos")
	Call<List<Repo>> listRepos1(@Path("user") String user);


	/* App Version Check */
	@POST("/app/version/new")
	Call<InfoDataAppVersion> createUser(@Body InfoDataAppVersion user);

}



//	@GET("/users/{username}")
//  Call<User> getUser(@Path("username") String username);
//
//  @GET("/group/{id}/users")
//  Call<List<User>> groupList(@Path("id") int groupId, @Query("sort") String sort);
//
//  @POST("/users/new")
//  Call<User> createUser(@Body User user);
//
//	@POST("/login")
//	public void login(@Field("username") String username, @Field("password") String password, Callback<JSONObject> callback);