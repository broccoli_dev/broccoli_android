package com.nomadconnection.broccoli.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.data.Spend.SpendCardBenefitDetail;
import com.nomadconnection.broccoli.data.Spend.SpendCardBenefitList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static android.widget.LinearLayout.LayoutParams.MATCH_PARENT;
import static android.widget.LinearLayout.LayoutParams.WRAP_CONTENT;

/**
 * Created by YelloHyunminJang on 2016. 10. 10..
 */

public class RowSpendCardBenefitDetail extends LinearLayout {

    private TextView mTextView;
    private LinearLayout mBody;
    private View mShowMore;
    private SpendCardBenefitList mBenefitData;
    private HashMap<Integer, View> mDetailMap = new HashMap<>();

    public RowSpendCardBenefitDetail(Context context) {
        super(context);
    }

    public RowSpendCardBenefitDetail(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RowSpendCardBenefitDetail(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
        if (mBenefitData != null) {
            refreshData();
        }
    }

    private void init() {
        mTextView = (TextView) findViewById(R.id.tv_row_card_benefit_title_text);
        mBody = (LinearLayout) findViewById(R.id.ll_row_card_benefit_body_layout);
        mShowMore = findViewById(R.id.ll_row_card_benefit_show_more);
        mShowMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showMore();
            }
        });
    }

    public void setTitle(String title) {
        mTextView.setText(title);
    }

    private void showMore() {
        mShowMore.setVisibility(View.GONE);
        if (mDetailMap != null) {
            Set<Integer> keys = mDetailMap.keySet();
            Iterator<Integer> iterator = keys.iterator();
            while (iterator.hasNext()) {
                int index = iterator.next();
                View view = mDetailMap.get(index);
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    private void createBody(List<SpendCardBenefitDetail> list) {
        if (mBody != null) {
            if (mBody.getChildCount() > 0) {
                mBody.removeAllViews();
            }
            if (mDetailMap != null) {
                mDetailMap.clear();
            }
            for (int i = 0; i < list.size(); i++) {
                SpendCardBenefitDetail detail = list.get(i);
                LinearLayout layout = createSubCategory(i, list.size()-1, detail);
                mBody.addView(layout);
            }
        }
    }

    private LinearLayout createSubCategory(int index, int lastIndex, SpendCardBenefitDetail data) {
        LinearLayout body = new LinearLayout(this.getContext());
        LayoutParams bodyParam = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        if (index == lastIndex) {
            bodyParam.setMargins(0, 0, 0, 0);
        } else {
            bodyParam.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen.common_dimens_10));
        }
        body.setLayoutParams(bodyParam);
        body.setOrientation(LinearLayout.VERTICAL);

        LinearLayout titleLayout = new LinearLayout(this.getContext());
        LayoutParams titleLayoutParam = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        titleLayout.setLayoutParams(titleLayoutParam);
        titleLayout.setOrientation(LinearLayout.HORIZONTAL);
        body.addView(titleLayout);

        TextView title = new TextView(this.getContext());
        LinearLayout.LayoutParams titleParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        titleParam.gravity = Gravity.LEFT|Gravity.TOP;
        titleParam.setMargins(0, 0, (int) getResources().getDimension(R.dimen.common_dimens_10), 0);
        title.setLayoutParams(titleParam);
        title.setGravity(Gravity.LEFT|Gravity.TOP);
        int width = (int) getResources().getDimension(R.dimen.common_dimens_90);
        title.setMinWidth(width);
        title.setMaxWidth(width);
        title.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
        title.setText(data.getBenefitCategoryCodeName());
        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setSingleLine(true);
        titleLayout.addView(title);

        TextView sub = new TextView(this.getContext());
        LinearLayout.LayoutParams subParam = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        subParam.weight = 1;
        subParam.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        sub.setLayoutParams(subParam);
        sub.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
        sub.setTextColor(getResources().getColor(R.color.common_rgb_27_179_166));
        sub.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
        sub.setText(data.getSummary());
        titleLayout.addView(sub);

        TextView detail = new TextView(this.getContext());
        LayoutParams detailParam = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        if (index == lastIndex) {
            detailParam.setMargins((int) getResources().getDimension(R.dimen.common_dimens_10),
                    (int) getResources().getDimension(R.dimen.common_dimens_20),
                    (int) getResources().getDimension(R.dimen.common_dimens_10),
                    0);
        } else {
            detailParam.setMargins((int) getResources().getDimension(R.dimen.common_dimens_10),
                    (int) getResources().getDimension(R.dimen.common_dimens_20),
                    (int) getResources().getDimension(R.dimen.common_dimens_10),
                    (int) getResources().getDimension(R.dimen.common_dimens_20));
        }
        detail.setLayoutParams(detailParam);
        detail.setGravity(Gravity.LEFT);
        detail.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
        detail.setTextColor(getResources().getColor(R.color.common_rgb_89_89_89));
        detail.setText(data.getDetail());
        body.addView(detail);
        detail.setVisibility(View.GONE);
        mDetailMap.put(index, detail);

        return body;
    }

    public void setData(SpendCardBenefitList spendCardBenefitList) {
        mBenefitData = spendCardBenefitList;
        refreshData();
    }

    public void refreshData() {
        if (mBenefitData != null) {
            String title = mBenefitData.getBenefitLargeCategoryCodeName();
            List<SpendCardBenefitDetail> list = mBenefitData.getBenefitCategoryList();

            setTitle(title);
            createBody(list);
        }
    }

}
