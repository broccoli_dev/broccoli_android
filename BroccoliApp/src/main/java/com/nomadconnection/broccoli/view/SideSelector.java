package com.nomadconnection.broccoli.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.SectionIndexer;

import com.nomadconnection.broccoli.R;

/**
 * Created by YelloHyunminJang on 2017. 2. 10..
 */

public class SideSelector extends View {
    private static String TAG = SideSelector.class.getCanonicalName();

    public static final char[] HEADER = new char[]{'ㄱ', 'ㄴ', 'ㄷ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅅ', 'ㅇ', 'ㅈ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ', 'A', 'F', 'K', 'P', 'U', 'Z', '#'};
    public static final int BOTTOM_PADDING = 10;

    private SectionIndexer selectionIndexer = null;
    private ListView list;
    private Paint paint;
    private String[] sections;
    private OnSideSelectorListener onSideSelectorListener;
    private boolean isDragging = false;

    public interface OnSideSelectorListener {
        void onPressed();
        void onMoved();
        void onRelease();
    }

    public SideSelector(Context context) {
        super(context);
        init();
    }

    public SideSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SideSelector(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setBackgroundColor(0x20FFFFFF);
        paint = new Paint();
        paint.setColor(getResources().getColor(R.color.common_subtext_color));
        paint.setTextSize(40);
        paint.setTextAlign(Paint.Align.CENTER);
    }

    public void setListView(ListView _list, BaseExpandableListAdapter adapter) {
        list = _list;
        BaseExpandableListAdapter targetAdapter = adapter;

        selectionIndexer = (SectionIndexer) targetAdapter;

        Object[] sectionsArr = selectionIndexer.getSections();
        sections = new String[sectionsArr.length];
        for (int i = 0; i < sectionsArr.length; i++) {
            sections[i] = sectionsArr[i].toString();
        }

    }

    private int mLastAction = -1;
    private int mPastIndex = 0;

    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        int y = (int) event.getY();
        float selectedIndex = ((float) y / (float) getPaddedHeight()) * HEADER.length;

        if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
            isDragging = true;
            if (selectionIndexer == null) {
                selectionIndexer = (SectionIndexer) list.getAdapter();
            }

            if (selectedIndex > HEADER.length-1) {
                selectedIndex = HEADER.length-1;
            } else if (selectedIndex < 0) {
                selectedIndex = 0;
            }
            char target = HEADER[(int) selectedIndex];
            int minTarget = Integer.MAX_VALUE;
            //char nearTarget = 0;
            int nearIndex = mPastIndex;

            for (int i=0; i< sections.length; i++) {
                char sectionTarget = sections[i].charAt(0);
                int temp =  Math.abs(sectionTarget-target);
                if (minTarget > temp) {
                    minTarget = temp;
                    //nearTarget = sections[i].charAt(0); near target char
                    nearIndex = i;
                    mPastIndex = i;
                }
            }

            int position = selectionIndexer.getPositionForSection(nearIndex);
            if (position == -1) {
                return true;
            }
            if (list instanceof ExpandableListView) {
                ExpandableListView listView = (ExpandableListView) list;
                listView.setSelectedGroup(position);
            } else {
                list.setSelection(position);
            }
            if (onSideSelectorListener != null && mLastAction != event.getAction())
                onSideSelectorListener.onPressed();
        } else {
            isDragging = false;
            if (onSideSelectorListener != null && mLastAction != event.getAction())
                onSideSelectorListener.onRelease();
        }
        mLastAction = event.getAction();
        return true;
    }

    public void setOnSideSelectorListener(OnSideSelectorListener listener) {
        onSideSelectorListener = listener;
    }

    protected void onDraw(Canvas canvas) {

        int viewHeight = getPaddedHeight();
        float charHeight = ((float) viewHeight) / (float) HEADER.length;

        float widthCenter = getMeasuredWidth() / 2;
        for (int i = 0; i < HEADER.length; i++) {
            canvas.drawText(String.valueOf(HEADER[i]), widthCenter, charHeight + (i * charHeight), paint);
        }
        super.onDraw(canvas);
    }

    private int getPaddedHeight() {
        return getHeight() - BOTTOM_PADDING;
    }

    public boolean isDragging() {
        return isDragging;
    }
}
