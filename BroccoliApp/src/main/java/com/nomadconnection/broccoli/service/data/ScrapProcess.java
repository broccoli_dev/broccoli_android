package com.nomadconnection.broccoli.service.data;

import com.nomadconnection.broccoliespider.data.BankRequestData;

/**
 * Created by YelloHyunminJang on 16. 8. 8..
 */
public class ScrapProcess {

    private String type;
    private String job;
    private String code;
    private String result;
    private String error;
    private String account;
    private BankRequestData data;
    private boolean isFinish = false;

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setRequestData(BankRequestData data) {
        this.data = data;
    }

    public BankRequestData getData() {
        return data;
    }

    public void setData(BankRequestData data) {
        this.data = data;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finished) {
        isFinish = finished;
    }
}
