package com.nomadconnection.broccoli.service;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.igaworks.IgawReceiver;

/**
 * Created by youngmoon on 2016-08-30.
 */
public class MultipleInstallReceiver extends CampaignTrackingReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        IgawReceiver igawReceiver = new IgawReceiver();
        igawReceiver.onReceive(context, intent);
    }
}
