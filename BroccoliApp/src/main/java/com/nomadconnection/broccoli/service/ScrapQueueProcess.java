package com.nomadconnection.broccoli.service;

import android.os.Looper;

import com.nomadconnection.broccoli.service.data.ScrapRequester;
import com.nomadconnection.broccoli.service.mode.ProcessMode;

import java.util.Queue;

/**
 * Created by YelloHyunminJang on 16. 3. 4..
 */
public class ScrapQueueProcess extends Thread {

    private final Queue<ScrapRequester> queue;
    private AutoUploadMgr.OnScrapRequested onRequestedListener;
    private final Object mLocker;

    public ScrapQueueProcess(Queue<ScrapRequester> linkedQueue, AutoUploadMgr.OnScrapRequested onRequestedListener, Object locker) {
        this.queue = linkedQueue;
        this.onRequestedListener = onRequestedListener;
        this.mLocker = locker;
        Looper.getMainLooper();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            process();
            try {
                synchronized (queue) {
                    queue.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void process() {
        while (!queue.isEmpty()) {
            ScrapRequester requester = queue.poll();
            if (requester != null) {
                String temp = requester.getMode();
                ProcessMode mode = ProcessMode.valueOf(temp);
                onRequestedListener.OnRequested(mode, requester);
                try {
                    synchronized (mLocker) {
                        mLocker.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void interrupt() {
        super.interrupt();
        synchronized (queue) {
            this.queue.notifyAll();
        }
    }
}
