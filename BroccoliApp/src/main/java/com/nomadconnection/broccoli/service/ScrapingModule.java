package com.nomadconnection.broccoli.service;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.heenam.espider.Engine;
import com.heenam.espider.EngineInterface;
import com.nomadconnection.broccoli.MainApplication;
import com.nomadconnection.broccoli.R;
import com.nomadconnection.broccoli.api.LogApi;
import com.nomadconnection.broccoli.api.ScrapApi;
import com.nomadconnection.broccoli.api.ServiceGenerator;
import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoli.data.Result;
import com.nomadconnection.broccoli.data.Scrap.BankAccountList;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.data.Scrap.BankRecordList;
import com.nomadconnection.broccoli.data.Scrap.CardApprovalList;
import com.nomadconnection.broccoli.data.Scrap.CardBillList;
import com.nomadconnection.broccoli.data.Scrap.CardDueList;
import com.nomadconnection.broccoli.data.Scrap.CardLimit;
import com.nomadconnection.broccoli.data.Scrap.CardList;
import com.nomadconnection.broccoli.data.Scrap.CardLoan;
import com.nomadconnection.broccoli.data.Scrap.CashReceiptList;
import com.nomadconnection.broccoli.data.Scrap.Common;
import com.nomadconnection.broccoli.data.Scrap.ServiceRecord;
import com.nomadconnection.broccoli.data.Scrap.UploadLogMsg;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.database.ServiceRecordDao;
import com.nomadconnection.broccoli.service.data.ScrapProcess;
import com.nomadconnection.broccoli.service.data.ScrapRequester;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.service.mode.ScrapResult;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoliespider.License;
import com.nomadconnection.broccoliespider.data.BankCode;
import com.nomadconnection.broccoliespider.data.BankRequestData;
import com.nomadconnection.broccoliespider.data.CardCode;
import com.nomadconnection.broccoliespider.data.CashCode;
import com.nomadconnection.broccoliespider.data.CommonCode;
import com.nomadconnection.broccoliespider.data.CountryCode;
import com.nomadconnection.broccoliespider.parameter.BankTransactionParameter;
import com.nomadconnection.broccoliespider.parameter.CardApprovalParameter;
import com.nomadconnection.broccoliespider.parameter.CardIdLoginParameter;
import com.nomadconnection.broccoliespider.parameter.CashParameter;
import com.nomadconnection.broccoliespider.parameter.LoginParameter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.nomadconnection.broccoliespider.data.BaseRequestData.VALUE_METHOD_ID;

/**
 * Created by YelloHyunminJang on 16. 3. 4..
 */
public class ScrapingModule implements EngineInterface {

    private final static String TAG = ScrapingModule.class.getSimpleName();

    public static final int STATUS_COMPLETE = 0;
    public static final int STATUS_REQUEST = 1;
    public static final int STATUS_SCRAPING = 2;
    public static final int STATUS_UPLOADING = 3;
    public static final int STATUS_PREPARE = 4;
    public static final int STATUS_ERROR = 5;
    public static final int STATUS_RETRY = 6;

    private static final String BROCCOLI_PATH = "/Broccoli";
    private static final String BROCCOLI_FILE = "BroccoliLog.txt";
    private static final String DIVIDER = "-------------------------------------------\n";
    private static final String RESULT_MSG = "SERVER_RESULT : ";

    private final static int DEFAULT_TIMEOUT = 3 * 60 * 1000;
    private Object mLock = new Object();
    private Gson mGson = new Gson();
    private ScrapApi mScrapApi;
    private Context mContext;
    private int mCurrentStatus = STATUS_COMPLETE;
    private ProcessMode mMode = ProcessMode.COLLECT_IDLE;
    private AutoUploadMgr.UploadStatusListener mListener;
    private List<BankRequestData> mRequestList;
    private ServiceRecordDao mRecordDao;
    private ScrapRequester mCurrentScrapRequester;
    private String mUserId;
    private String mScrapTime;
    private Engine mEngine;
    private ArrayList<ScrapProcess> mJobProcessList;	//모듈조회시 결과 데이터
    private ArrayList<HashMap<String, HashMap<String, String>>> mJobList; // 스크래핑 될 jobList
    private ArrayList<ScrapProcess> mSecondProcessList;

    private Handler mTimeOutHandler = new Handler();
    private Runnable mTimeOutCheckRunnable = new Runnable() {
        @Override
        public void run() {
            mEngine.cancelAllForce();
        }
    };

    private final byte[] SecureKey = { 'M', 'o', 'b', 'i', 'l', 'e', 'T', 'r', 'a', 'n', 's', 'K', 'e', 'y', '1', '0' };

    public ScrapingModule(Context context, AutoUploadMgr.UploadStatusListener listener) {
        mScrapApi = ServiceGenerator.createScrapingService(ScrapApi.class);
        mContext = context;
        mListener = listener;
        mRecordDao = ServiceRecordDao.getInstance();
        mJobList = new ArrayList<HashMap<String, HashMap<String, String>>>(); // 실행될 모듈 정보 List
        mJobProcessList = new ArrayList<>();
        mSecondProcessList = new ArrayList<>();
        initEngine();
    }

    private void initEngine() {
        mEngine = Engine.getInstatnce(mContext.getApplicationContext()); //엔진의 Instatnce를 가져옵니다.
        mEngine.setInterface(this); //엔진 실행시 콜백 받을 Class
        mEngine.setLicenseKey(License.LICENSE_KEY); //엔진 라이센스키 TODO : 전달받은 라이센스키로 적용
        mEngine.setAutoStop(true);
    }

    public byte[] toByteArray(String hexStr) {
        int len = hexStr.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexStr.charAt(i), 16) << 4) + Character
                    .digit(hexStr.charAt(i + 1), 16));
        }
        return data;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public String toHexString(byte[] bytes) {
        char[] hexChar = new char[bytes.length *2];
        for (int i=0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            hexChar[i * 2] = hexArray[v >>> 4];
            hexChar[i * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChar);
    }

    public void setScrapData(ScrapRequester requester) {
        if (requester != null) {
            initEngine();
            mUserId = Session.getInstance(mContext).getUserId();
            mScrapApi = ServiceGenerator.createScrapingService(ScrapApi.class);
            mCurrentScrapRequester = requester;
            String temp = requester.getMode();
            mRequestList = requester.getData();
            mMode = ProcessMode.valueOf(temp);
            mListener.start(mMode);
            Calendar calendar = Calendar.getInstance();
            mScrapTime = ElseUtils.getSimpleDate(calendar.getTime());
            if (mRequestList != null && mRequestList.size() > 0) {
                mEngine.setOptionsRaon(true,
                        APPSharedPrefer.getInstance(mContext).getPrefString(APP.PUBLIC_KEY),
                        toHexString(SecureKey),
                        false,
                        Engine.CipherAlgorism.SE_CIPHER_SEED_CBC,
                        Engine.CipherDataType.SE_CIPHER_TYPE_HEXSTRING); //보안 키보드 동기화 파라메터
            }
            switch (mMode) {
                case COLLECT_FIRST_THIS_MONTH:
                    if (mRequestList != null && mRequestList.size() > 0) {
                        startScrapingDataMonthly();
                    } else {
                        scrapComplete();
                    }
                    break;
                case COLLECT_FIRST_PAST_MONTH:
                    break;
                case COLLECT_DAILY:
                    if (mRequestList != null && mRequestList.size() > 0) {
                        APPSharedPrefer.getInstance(mContext).setPrefString(APP.BATCH_SERVICE_DAILY_RECORD, ElseUtils.getSimpleDate(new Date()));
                        startScrapingDataDaily();
                    } else {
                        scrapComplete();
                    }
                    break;
                case COLLECT_INDIVIDUAL:
                    if (mRequestList != null && mRequestList.size() > 0) {
                        startScrapingDataMonthly();
                    } else {
                        scrapComplete();
                    }
                    break;
                case COLLECT_RETRY:
                    if (mRequestList != null && mRequestList.size() > 0) {
                        startScrapingDataMonthly();
                    } else {
                        scrapComplete();
                    }
                    break;
                case COLLECT_LOGIN:
                    if (mRequestList != null && mRequestList.size() > 0) {
                        startLogin();
                    } else {
                        scrapComplete();
                    }
                    break;
                case COLLECT_IDLE:
                    break;
            }
        }
    }

    private void checkScarpResult() {
        set_synchronize_time(); //main display time set
        final ProcessMode mode = mMode;
        List<ServiceRecord> list = ServiceRecordDao.getInstance().getSelectedModeData(mContext, mode.name());
        final HashMap<Integer, Boolean> checkMap = new HashMap<>();
        int errorCount = 0;

        final List<ServiceRecord> successList = new ArrayList<>();
        final List<ServiceRecord> tryAgainList = new ArrayList<>();
        final List<ServiceRecord> networkFailList = new ArrayList<>();
        final List<ServiceRecord> authFailList = new ArrayList<>();
        final List<ServiceRecord> siteRenewList = new ArrayList<>();
        final List<ServiceRecord> loginFailList = new ArrayList<>();
        if (list != null) {
            for (ServiceRecord record : list) {
                String time = record.getTime();
                if (time != null && !time.isEmpty()) {
                    if (time.equals(mScrapTime)) {
//                        String count = record.getCount();
//                        int displayTime = Integer.parseInt(count);
//                        if (displayTime == 0) {
//                            if (mode == ProcessMode.COLLECT_INDIVIDUAL) {
//                                displayTime++;
//                                record.setCount(String.valueOf(displayTime));
//                                ServiceRecordDao.getInstance().updateData(mContext, record);
//                            }
//                        }
                        switch (record.getResult()) {
                            case ScrapResult.COMPLETE:
                                successList.add(record);
                                break;
                            case ScrapResult.FAIL:
                            case ScrapResult.FAIL_TIMEOUT:
                            case ScrapResult.FAIL_MISSING:
                            case ScrapResult.FAIL_UNKNOWN:
                                tryAgainList.add(record);
                                checkMap.put(ScrapResult.FAIL, true);
                                errorCount++;
                                break;
                            case ScrapResult.FAIL_AUTH:
                                authFailList.add(record);
                                checkMap.put(ScrapResult.FAIL_AUTH, true);
                                errorCount++;
                                break;
                            case ScrapResult.FAIL_RENEWAL:
                                siteRenewList.add(record);
                                checkMap.put(ScrapResult.FAIL_RENEWAL, true);
                                errorCount++;
                                break;
                            case ScrapResult.FAIL_NETWORK:
                                networkFailList.add(record);
                                checkMap.put(ScrapResult.FAIL_NETWORK, true);
                                errorCount++;
                                break;
                            case ScrapResult.FAIL_IDPWD_ERROR:
                                loginFailList.add(record);
                                checkMap.put(ScrapResult.FAIL_IDPWD_ERROR, true);
                                errorCount++;
                                break;
                        }
                    }
                }
            }
        }


//        if (mode == ProcessMode.COLLECT_INDIVIDUAL) {
//            if (checkMap.size() > 1) {
//                String title = mContext.getString(R.string.dialog_scrap_error_title);
//                String str = "총 "+errorCount+"개의 오류가 발견되었습니다.";
//                DialogUtils.ServiceDialogShow(mContext, 100, title, str, AlertDialogActivity.TYPE_FAIL_MULTI, mode.ordinal(), null);
//            }
//        }

        Session.getInstance(mContext).acquirePublic(new Session.OnKeyInterface() {
            @Override
            public void acquired(String str) {
                int requestCode = 100;
                if (successList.size() > 0) {
                    ScrapDao scrapDao = ScrapDao.getInstance();
                    for (ServiceRecord record:successList) {
                        BankData bankData = scrapDao.getBankDataByName(mContext, str, record.getName(), Session.getInstance(getApplicationContext()).getUserId());
                        bankData.setStatus(BankData.NONE);
                        scrapDao.updateBankData(mContext, str, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                    }
                }
                if (authFailList.size() > 0) {
                    ScrapDao scrapDao = ScrapDao.getInstance();
                    for (ServiceRecord record:authFailList) {
                        BankData bankData = scrapDao.getBankDataByName(mContext, str, record.getName(), Session.getInstance(getApplicationContext()).getUserId());
                        bankData.setStatus(BankData.FAIL_AUTH);
                        scrapDao.updateBankData(mContext, str, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                    }
                }
                if (siteRenewList.size() > 0) {
                    ScrapDao scrapDao = ScrapDao.getInstance();
                    for (ServiceRecord record:siteRenewList) {
                        BankData bankData = scrapDao.getBankDataByName(mContext, str, record.getName(), Session.getInstance(getApplicationContext()).getUserId());
                        bankData.setStatus(BankData.FAIL_SITERENEW);
                        scrapDao.updateBankData(mContext, str, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                    }
                }
                if (tryAgainList.size() > 0) {
                    ScrapDao scrapDao = ScrapDao.getInstance();
                    for (ServiceRecord record:tryAgainList) {
                        BankData bankData = scrapDao.getBankDataByName(mContext, str, record.getName(), Session.getInstance(getApplicationContext()).getUserId());
                        bankData.setStatus(BankData.FAIL);
                        scrapDao.updateBankData(mContext, str, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                    }
                }
                if (networkFailList.size() > 0) {
                    ScrapDao scrapDao = ScrapDao.getInstance();
                    for (ServiceRecord record:networkFailList) {
                        BankData bankData = scrapDao.getBankDataByName(mContext, str, record.getName(), Session.getInstance(getApplicationContext()).getUserId());
                        bankData.setStatus(BankData.FAIL_NETWORK);
                        scrapDao.updateBankData(mContext, str, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                    }
                }
                if (loginFailList.size() > 0) {
                    ScrapDao scrapDao = ScrapDao.getInstance();
                    for (ServiceRecord record:loginFailList) {
                        BankData bankData = scrapDao.getBankDataByName(mContext, str, record.getName(), Session.getInstance(getApplicationContext()).getUserId());
                        bankData.setStatus(BankData.FAIL_PASSWORD);
                        scrapDao.updateBankData(mContext, str, Session.getInstance(getApplicationContext()).getUserId(), bankData);
                    }
                }
//                if (checkMap.size() <= 1 && mode == ProcessMode.COLLECT_INDIVIDUAL) {
//                    if (authFailList.size() > 0) {
//                        String title = mContext.getString(R.string.dialog_scrap_error_auth_fail_title);
//                        String content = mContext.getString(R.string.common_warning_auth_not_reg);
//                        DialogUtils.ServiceDialogShow(mContext, requestCode++, title, content, AlertDialogActivity.TYPE_AUTH_FAIL, mode.ordinal(), (ArrayList<ServiceRecord>) authFailList);
//                    }
//                    if (siteRenewList.size() > 0) {
//                        String title = mContext.getString(R.string.dialog_scrap_error_site_renew_title);
//                        String content = mContext.getString(R.string.common_warning_site_renew);
//                        DialogUtils.ServiceDialogShow(mContext, requestCode++, title, content, AlertDialogActivity.TYPE_SITE_RENEW, mode.ordinal(), (ArrayList<ServiceRecord>) siteRenewList);
//                    }
//                    if (tryAgainList.size() > 0) {
//                        String content = "브로콜리에서 은행 데이터를 조회 중 "+tryAgainList.size()+"개의 조회가 실패하였습니다.\n" +
//                                "연동 금융 기관 페이지에서 다시 한 번 시도하여 주세요.";
//                        DialogUtils.ServiceDialogShow(mContext, requestCode++, null, content, AlertDialogActivity.TYPE_FAIL_RETRY, mode.ordinal(), (ArrayList<ServiceRecord>) tryAgainList);
//                    }
//                    if (networkFailList.size() > 0) {
//                        String content = "네트워크 연결이 되지 않아 계좌조회에 실패 하였습니다. 연동 금융 기관 페이지에서 다시 한 번 시도하여 주세요.";
//                        DialogUtils.ServiceDialogShow(mContext, requestCode++, null, content, AlertDialogActivity.TYPE_FAIL_RETRY, mode.ordinal(), (ArrayList<ServiceRecord>) networkFailList);
//                    }
//                }
            }

            @Override
            public void error(ErrorMessage errorMessage) {
                switch (errorMessage) {
                    case ERROR_713_NO_SUCH_USER:
                    case ERROR_10007_SESSION_NOT_FOUND:
                    case ERROR_10008_PWD_CHANGED_SESSION_EXPIRED:
                    case ERROR_20003_MISMATCH_ACCESS_TOKEN:
                    case ERROR_20004_NO_SUCH_USER:
                        break;
                }
            }

            @Override
            public void fail() {

            }
        });
    }

    private void scrapComplete() {
        mCurrentStatus = STATUS_COMPLETE;
        mMode = ProcessMode.COLLECT_IDLE;
        mCurrentScrapRequester = null;
        mListener.complete();
        mScrapTime = null;
        synchronized (getLock()) {
            getLock().notifyAll();
        }
    }

    public Object getLock() {
        return mLock;
    }

    private void addBankInquiry(BankRequestData data, String module, String account) {
        try {
            ScrapProcess process = new ScrapProcess();
            HashMap<String, String> moduleInfo = new HashMap<String, String>();		// 실행 할 모듈정보
            HashMap<String, String> loginInfo = new HashMap<String, String>();		// Login 정보 Parameter
            HashMap<String, String> paramInfo = new HashMap<String, String>();		// 업무에 필요한 추가정보 Parameter
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<String, HashMap<String, String>>();	// 실행할 모듈 전체정보

            // 모듈 정보
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, CountryCode.KOREA);			// 국가코드
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, BankCode.TYPE);		// O2CODE
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, data.getOriginalBankCode());	// S4CODE
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, module);		// 기업은행(개인)-IE 전계좌조회


            String loginMethod = data.getLoginMethod();
            if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                // 로그인 정보
                loginInfo.put(LoginParameter.LOGIN_ID, data.getLoginId());
                loginInfo.put(LoginParameter.LOGIN_PASS, data.getLoginPwd());					// 인증서 패스워드
            } else {
                // 로그인 정보
                loginInfo.put(LoginParameter.CERT_DER_FILEPATH, data.getCertName() + "/signCert.der");		// 조회할 인증서 signCert.der파일경로
                loginInfo.put(LoginParameter.CERT_KEY_FILEPATH, data.getCertName() + "/signPri.key");		// 조회할 인증서 signPri.key파일경로
                loginInfo.put(LoginParameter.CERT_PASSWORD, data.getPassword());					// 인증서 패스워드
            }

            if (BankTransactionParameter.VALUE_MODULE_BANK_TRANSACTION.equalsIgnoreCase(module) ||
                    BankTransactionParameter.VALUE_MODULE_BANK_SAVING_TRANSACTION.equalsIgnoreCase(module) ||
                    BankTransactionParameter.VALUE_MODULE_BANK_LOAN_TRANSACTION.equalsIgnoreCase(module)) {
                // 파라메터 정보
                paramInfo.put(BankTransactionParameter.KEY_START_DATE, data.getStartDate());								// paramInfo
                paramInfo.put(BankTransactionParameter.KEY_END_DATE, data.getEndDate());
                paramInfo.put(BankTransactionParameter.KEY_TIME_SORT, BankTransactionParameter.VALUE_TIME_SORT_NEWEST);
                paramInfo.put(BankTransactionParameter.KEY_ACCOUNT, account);
            }

            // 모듈 실행될 전체 데이터
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
			executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);

            process.setRequestData(data);
            process.setAccount(account);
            process.setCode(data.getOriginalBankCode());
            process.setJob(module);
            process.setType(data.getOriginalBankType());
            mJobProcessList.add(process);
            mJobList.add(executeModule);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCardInquiry(BankRequestData data, String module) {
        try {
            ScrapProcess process = new ScrapProcess();
            HashMap<String, String> moduleInfo = new HashMap<String, String>();		// 실행 할 모듈정보
            HashMap<String, String> loginInfo = new HashMap<String, String>();		// Login 정보 Parameter
            HashMap<String, String> paramInfo = new HashMap<String, String>();		// 업무에 필요한 추가정보 Parameter
            HashMap<String, String> paramExtInfo = new HashMap<String, String>();		// 업무에 필요한 추가정보 Parameter
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<String, HashMap<String, String>>();	// 실행할 모듈 전체정보

            // 모듈 정보
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, CountryCode.KOREA);			// 국가코드
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, CardCode.TYPE);		// O2CODE
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, data.getOriginalBankCode());	// S4CODE
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, module);		// 기업은행(개인)-IE 전계좌조회

            String loginMethod = data.getLoginMethod();
            if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                // 로그인 정보
                loginInfo.put(LoginParameter.LOGIN_ID, data.getLoginId());
                loginInfo.put(LoginParameter.LOGIN_PASS, data.getLoginPwd());					// 인증서 패스워드

                if (!CardIdLoginParameter.VALUE_MODULE_CARD_LIST.equalsIgnoreCase(module) &&
                        !CardIdLoginParameter.VALUE_MODULE_CARD_LIMIT.equalsIgnoreCase(module)) {
                    // 파라메터 정보
                    if (CardIdLoginParameter.VALUE_MODULE_CARD_APPROVAL.equalsIgnoreCase(module)) {
                        paramInfo.put(CardIdLoginParameter.KEY_START_DATE, data.getStartDate());
                        paramInfo.put(CardIdLoginParameter.KEY_SEARCH_SECTION, CardIdLoginParameter.VALUE_SEARCH_SECTION_TOTAL);
                        paramExtInfo.put(CardIdLoginParameter.KEY_MEMBERSHIP_STORE, CardIdLoginParameter.VALUE_MEMBERSHIP_STORE_EXCLUDE);
                    } else if (CardIdLoginParameter.VALUE_MODULE_CARD_BILL.equalsIgnoreCase(module)) {
                        paramInfo.put(CardIdLoginParameter.KEY_START_DATE, data.getBillDate());
                    }
                    if (CardIdLoginParameter.VALUE_MODULE_CARD_APPROVAL.equalsIgnoreCase(module)) {
                        paramInfo.put(CardIdLoginParameter.KEY_END_DATE, data.getEndDate());								// paramInfo
                    }
                    if (CardIdLoginParameter.VALUE_MODULE_CARD_APPROVAL.equalsIgnoreCase(module)
                            || CardIdLoginParameter.VALUE_MODULE_CARD_DUE.equalsIgnoreCase(module)) {
                        paramInfo.put(CardIdLoginParameter.KEY_TIME_SORT, CardIdLoginParameter.VALUE_TIME_SORT_NEWEST);								// paramInfo
                    }
                    if (CardIdLoginParameter.VALUE_MODULE_CARD_LOAN.equalsIgnoreCase(module)) {
                        //do nothing
                    }
                }
            } else {
                // 로그인 정보
                loginInfo.put(LoginParameter.CERT_DER_FILEPATH, data.getCertName() + "/signCert.der");		// 조회할 인증서 signCert.der파일경로
                loginInfo.put(LoginParameter.CERT_KEY_FILEPATH, data.getCertName() + "/signPri.key");		// 조회할 인증서 signPri.key파일경로
                loginInfo.put(LoginParameter.CERT_PASSWORD, data.getPassword());					// 인증서 패스워드

                if (!CardApprovalParameter.VALUE_MODULE_CARD_LIST.equalsIgnoreCase(module) &&
                        !CardApprovalParameter.VALUE_MODULE_CARD_LIMIT.equalsIgnoreCase(module)) {
                    // 파라메터 정보
                    if (CardApprovalParameter.VALUE_MODULE_CARD_APPROVAL.equalsIgnoreCase(module)) {
                        paramInfo.put(CardApprovalParameter.KEY_START_DATE, data.getStartDate());
                        paramInfo.put(CardApprovalParameter.KEY_SEARCH_SECTION, CardApprovalParameter.VALUE_SEARCH_SECTION_TOTAL);
                        paramExtInfo.put(CardApprovalParameter.KEY_MEMBERSHIP_STORE, CardApprovalParameter.VALUE_MEMBERSHIP_STORE_EXCLUDE);
                    } else if (CardApprovalParameter.VALUE_MODULE_CARD_BILL.equalsIgnoreCase(module)) {
                        paramInfo.put(CardApprovalParameter.KEY_START_DATE, data.getBillDate());
                    }
                    if (CardApprovalParameter.VALUE_MODULE_CARD_APPROVAL.equalsIgnoreCase(module)) {
                        paramInfo.put(CardApprovalParameter.KEY_END_DATE, data.getEndDate());								// paramInfo
                    }
                    if (CardApprovalParameter.VALUE_MODULE_CARD_APPROVAL.equalsIgnoreCase(module)
                            || CardApprovalParameter.VALUE_MODULE_CARD_DUE.equalsIgnoreCase(module)) {
                        paramInfo.put(CardApprovalParameter.KEY_TIME_SORT, CardApprovalParameter.VALUE_TIME_SORT_NEWEST);								// paramInfo
                    }
                    if (CardApprovalParameter.VALUE_MODULE_CARD_LOAN.equalsIgnoreCase(module)) {
                        //do nothing
                    }
                }
            }

            // 모듈 실행될 전체 데이터
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
			executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, paramExtInfo);

            process.setRequestData(data);
            process.setCode(data.getOriginalBankCode());
            process.setJob(module);
            process.setType(data.getOriginalBankType());
            mJobProcessList.add(process);
            mJobList.add(executeModule);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCashInquiry(BankRequestData data, String module) {
        try {
            ScrapProcess process = new ScrapProcess();
            HashMap<String, String> moduleInfo = new HashMap<String, String>();		// 실행 할 모듈정보
            HashMap<String, String> loginInfo = new HashMap<String, String>();		// Login 정보 Parameter
            HashMap<String, String> paramInfo = new HashMap<String, String>();		// 업무에 필요한 추가정보 Parameter
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<String, HashMap<String, String>>();	// 실행할 모듈 전체정보

            // 모듈 정보
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, CountryCode.KOREA);			// 국가코드
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, CashCode.TYPE);		// O2CODE
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, data.getOriginalBankCode());	// S4CODE
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, module);		// 기업은행(개인)-IE 전계좌조회

            String loginMethod = data.getLoginMethod();
            if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                // 로그인 정보
                loginInfo.put(LoginParameter.LOGIN_ID, data.getLoginId());
                loginInfo.put(LoginParameter.LOGIN_PASS, data.getLoginPwd());					// 인증서 패스워드
            } else {
                // 로그인 정보
                loginInfo.put(LoginParameter.CERT_DER_FILEPATH, data.getCertName() + "/signCert.der");		// 조회할 인증서 signCert.der파일경로
                loginInfo.put(LoginParameter.CERT_KEY_FILEPATH, data.getCertName() + "/signPri.key");		// 조회할 인증서 signPri.key파일경로
                loginInfo.put(LoginParameter.CERT_PASSWORD, data.getPassword());					// 인증서 패스워드
            }

            // 파라메터 정보
            paramInfo.put(CashParameter.KEY_START_DATE, data.getStartDate());								// paramInfo
            paramInfo.put(CashParameter.KEY_END_DATE, data.getEndDate());								// paramInfo
            paramInfo.put(CashParameter.KEY_TIME_SORT, CashParameter.VALUE_TIME_SORT_NEWEST);								// paramInfo
            paramInfo.put(CashParameter.KEY_SEARCH_SECTION, CashParameter.VALUE_SEARCH_SECTION_TOTAL);								// paramInfo
            paramInfo.put(CashParameter.KEY_ISSUE_TYPE, CashParameter.VALUE_ISSUE_TYPE_TOTAL);

            // 모듈 실행될 전체 데이터
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);

            process.setRequestData(data);
            process.setCode(data.getOriginalBankCode());
            process.setJob(module);
            process.setType(data.getOriginalBankType());
            mJobProcessList.add(process);
            mJobList.add(executeModule);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startScraping() {
        mEngine.stopEngine();		// 기존 엔진이 실행중이면, 중단
        mEngine.setThreadCount(4);	// 스레드의 개수 설정
        mEngine.setAutoStop(true);	// 엔진 자동중단 실행 여부
        mEngine.startEngine();		// 엔진 기동
        mEngine.startJob();			// 추가된 모듈리스트 조회
    }

    private void startLogin() {
        for (BankRequestData bank : mRequestList) {
            mListener.progress(bank.getOriginalBankName() + " " + "로그인 확인 중입니다.");
            if (BankData.TYPE_BANK.equalsIgnoreCase(bank.getOriginalBankType())) {
                addBankInquiry(bank, BankTransactionParameter.VALUE_MODULE_BANK_LIST, null);
            } else if (BankData.TYPE_CARD.equalsIgnoreCase(bank.getOriginalBankType())) {
                addCardInquiry(bank, CardIdLoginParameter.VALUE_MODULE_CARD_LIST);
            } else if (BankData.TYPE_CASH.equalsIgnoreCase(bank.getOriginalBankType())) {
                addCashInquiry(bank, CashParameter.VALUE_MODULE_ID_LOGIN_CASH);
            }
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                startScraping();
            }
        });
    }

    private void startScrapingDataDaily() {
        int count = 0;
        for (BankRequestData bank : mRequestList) {
            mListener.progress(bank.getOriginalBankName() + " " + mContext.getString(R.string.loading_status_str));
            String loginMethod = bank.getLoginMethod();
            if (BankData.TYPE_BANK.equals(bank.getOriginalBankType())) {
                if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                    addBankInquiry(bank, BankTransactionParameter.VALUE_MODULE_BANK_LIST, null);
                } else {
                    addBankInquiry(bank, BankTransactionParameter.VALUE_MODULE_BANK_LIST, null);
                }
            } else if (BankData.TYPE_CARD.equals(bank.getOriginalBankType())) {
                if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                    addCardInquiry(bank, CardIdLoginParameter.VALUE_MODULE_CARD_LIST);
                } else {
                    addCardInquiry(bank, CardApprovalParameter.VALUE_MODULE_CARD_LIST);
                }
            } else if (BankData.TYPE_CASH.equals(bank.getOriginalBankType())) {
                if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                    addCashInquiry(bank, CashParameter.VALUE_MODULE_ID_LOGIN_CASH);
                } else {
                    addCashInquiry(bank, CashParameter.VALUE_MODULE_CASH);
                }
            }
            count++;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                startScraping();
            }
        });
    }

    public void startScrapingDataMonthly() {
        mCurrentStatus = STATUS_SCRAPING;
        int count = 0;
        for (BankRequestData bank : mRequestList) {
            mListener.progress(bank.getOriginalBankName() + " " + mContext.getString(R.string.loading_status_str));
            String loginMethod = bank.getLoginMethod();
            if (BankData.TYPE_BANK.equals(bank.getOriginalBankType())) {
                if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                    addBankInquiry(bank, BankTransactionParameter.VALUE_MODULE_BANK_LIST, null);
                } else {
                    addBankInquiry(bank, BankTransactionParameter.VALUE_MODULE_BANK_LIST, null);
                }
            } else if (BankData.TYPE_CARD.equals(bank.getOriginalBankType())) {
                if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                    addCardInquiry(bank, CardIdLoginParameter.VALUE_MODULE_CARD_LIST);
                } else {
                    addCardInquiry(bank, CardApprovalParameter.VALUE_MODULE_CARD_LIST);
                }
            } else if (BankData.TYPE_CASH.equals(bank.getOriginalBankType())) {
                if (VALUE_METHOD_ID.equalsIgnoreCase(loginMethod)) {
                    addCashInquiry(bank, CashParameter.VALUE_MODULE_ID_LOGIN_CASH);
                } else {
                    addCashInquiry(bank, CashParameter.VALUE_MODULE_CASH);
                }
            }
            count++;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                startScraping();
            }
        });
    }

    private synchronized void insertScrapResult(int result, String jsonData, int jobIndex, boolean isSubModule) {
        ScrapProcess process = mJobProcessList.get(jobIndex);
        String name = process.getData().getOriginalBankName();
        List<ServiceRecord> list = mRecordDao.getSelectedBankData(mContext, mMode.name() ,name);
        if (list.size() > 0) {
            ServiceRecord record = list.get(0);
            record.setId(mMode.name() + name);
            record.setMode(mMode.name());
            record.setName(name);
            record.setType(process.getType());
            if (result != ScrapResult.COMPLETE) {
                record.setStatus(jsonData);
            }
            if (!isSubModule) {
                record.setResult(result);
            } else {
                if (result != ScrapResult.COMPLETE) {
                    record.setResult(result);
                }
            }
            record.setTime(mScrapTime);
            record.setCount("0");
            record.setJob(process.getJob());
            mRecordDao.insertOrUpdateData(mContext, record);
        } else {
            ServiceRecord record = new ServiceRecord();
            record.setId(mMode.name() + name);
            record.setMode(mMode.name());
            record.setName(name);
            record.setType(process.getType());
            if (result != ScrapResult.COMPLETE) {
                JSONObject object = null;
                try {
                    object = new JSONObject(jsonData);
                    String errMsg = object.toString();
                    record.setStatus(errMsg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            record.setResult(result);
            record.setTime(mScrapTime);
            record.setCount("0");
            record.setJob(process.getJob());
            mRecordDao.insertOrUpdateData(mContext, record);
        }
    }

    private void uploadLogMsg(int result, String errorMessage, int jobIndex) {
        uploadLogMsg(result, "100000", errorMessage, "broccoli - process error", jobIndex);
    }

    private void uploadLogMsg(int result, String errorCode, String errorMessage, String etc, int jobIndex) {
        if (result != ScrapResult.COMPLETE) {
            BankRequestData data = mJobProcessList.get(jobIndex).getData();
            UploadLogMsg msg = new UploadLogMsg();
            msg.setUserId(Session.getInstance(mContext).getUserId());
            if (BankData.TYPE_CASH.equalsIgnoreCase(data.getOriginalBankType())) {
                msg.setCompanyCode("1003");
            } else {
                msg.setCompanyCode(data.getOriginalBankCode());
            }
            msg.setErrorCode(errorCode);
            msg.setErrorMsg(errorMessage);
            msg.setEtc(etc);
            Call<Result> call = mScrapApi.sendErrorReport(msg);
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {

                }
            });
        }
    }

    private void showRequestCount(List<BankRequestData> list, boolean end) {
        int bank = 0;
        int card = 0;
        int cash = 0;
        for (BankRequestData data : list) {
            if (data != null) {
                if (BankData.TYPE_BANK.equals(data.getOriginalBankType())) {
                    bank++;
                } else if (BankData.TYPE_CARD.equals(data.getOriginalBankType())) {
                    card++;
                } else if (BankData.TYPE_CASH.equals(data.getOriginalBankType())) {
                    cash++;
                }
            }
        }

        if (bank > 0 || card > 0 || cash > 0) {
            if (end) {
                ElseUtils.showNotification(mContext, mContext.getString(R.string.app_name), "최신정보 업데이트 완료");
            } else {

            }
        }
//        String msg = new String();
//        if (bank > 0) {
//            msg += "은행 "+bank+"개";
//        }
//        if (card > 0) {
//            if (bank > 0) {
//                msg += "/";
//            }
//            msg += "카드 "+card+"개";
//        }
//        if (!msg.isEmpty()) {
//            if (end) {
//                ElseUtils.showNotification(mContext, mContext.getString(R.string.app_name), "최신정보 업데이트 완료");
//            } else {
//
//            }
//        }
    }

    private void startTimeOutChecker() {
        mTimeOutHandler.postDelayed(mTimeOutCheckRunnable, DEFAULT_TIMEOUT);
    }

    private void clearTimeOutChecker() {
        mTimeOutHandler.removeCallbacks(mTimeOutCheckRunnable);
    }

    private int processBankList(String jsonData, int jobIndex) {
        int result = ScrapResult.COMPLETE;
        Calendar current = Calendar.getInstance();
        mCurrentStatus = STATUS_UPLOADING;
        JSONObject jSub = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            jSub = jsonObject.getJSONObject(CommonCode.RESULT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jSub.has(CommonCode.KEY_LIST)) {
            BankAccountList data = new BankAccountList();
            data.setUSERID(mUserId);
            data.setTYPE(Common.TYPE_BANK);
            data.setCMPNYCODE(mJobProcessList.get(jobIndex).getCode());
            String uploadId = Common.TYPE_BANK + "_" + jobIndex + "_" + ElseUtils.getSimpleDate(current.getTime());
            data.setUPLOADID(uploadId);
            JsonObject dataResult = new JsonObject();
            dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
            data.setResult(dataResult);
            Call<Result> call = mScrapApi.sendBankAccountList(data);
            try {
                Response response = call.execute();
                writeFile(RESULT_MSG + response.raw(), jobIndex);
            } catch (IOException e) {
                e.printStackTrace();
                writeFile(RESULT_MSG + e.getMessage(), jobIndex);
            }
            try {
                JSONArray array = jSub.getJSONArray(CommonCode.KEY_LIST);
                if (array.length() > 0) {
                    for (int i=0; i < array.length(); i++) {
                        JSONObject deposit = array.getJSONObject(i);
                        //전체계좌가 아닌 예적금만 조회하도록 수정 대신 계좌리스트는 전체를 보낸다.
//                        Iterator<String> keys = deposit.keys();
//                        while (keys.hasNext()) {
//                            String key = keys.next();
//                            JSONArray bankArray = deposit.getJSONArray(key);
//                            for (int j=0; j < bankArray.length(); j++) {
//                                String account = bankArray.getJSONObject(j).getString("resAccount");
//                                if (account != null && !TextUtils.isEmpty(account)) {
//                                    ScrapProcess process = new ScrapProcess();
//                                    BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
//                                    process.setData(bankRequestData);
//                                    process.setJob(BankTransactionParameter.VALUE_MODULE_BANK_TRANSACTION);
//                                    process.setAccount(account);
//                                    process.setCode(bankRequestData.getOriginalBankCode());
//                                    process.setType(bankRequestData.getOriginalBankType());
//                                    mSecondProcessList.add(process);
//                                }
//                            }
//                        }
                        JSONArray bankArray = deposit.getJSONArray("resDepositTrust");
                        for (int j=0; j < bankArray.length(); j++) {
                            String type = bankArray.getJSONObject(j).getString(CommonCode.KEY_ACCOUNT_TYPE);
                            if (CommonCode.VALUE_TYPE_DEPOSIT.equalsIgnoreCase(type) || CommonCode.VALUE_TYPE_MMDA.equalsIgnoreCase(type)) {
                                String account = bankArray.getJSONObject(j).getString(CommonCode.KEY_ACCOUNT);
                                if (account != null && !TextUtils.isEmpty(account)) {
                                    ScrapProcess process = new ScrapProcess();
                                    BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
                                    process.setData(bankRequestData);
                                    process.setJob(BankTransactionParameter.VALUE_MODULE_BANK_TRANSACTION);
                                    process.setAccount(account);
                                    process.setCode(bankRequestData.getOriginalBankCode());
                                    process.setType(bankRequestData.getOriginalBankType());
                                    mSecondProcessList.add(process);
                                }
                            } else if (CommonCode.VALUE_TYPE_SAVING.equalsIgnoreCase(type) || CommonCode.VALUE_TYPE_TRUST.equalsIgnoreCase(type)) {
                                String account = bankArray.getJSONObject(j).getString(CommonCode.KEY_ACCOUNT);
                                if (account != null && !TextUtils.isEmpty(account)) {
                                    ScrapProcess process = new ScrapProcess();
                                    BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
                                    process.setData(bankRequestData);
                                    process.setJob(BankTransactionParameter.VALUE_MODULE_BANK_SAVING_TRANSACTION);
                                    process.setAccount(account);
                                    process.setCode(bankRequestData.getOriginalBankCode());
                                    process.setType(bankRequestData.getOriginalBankType());
                                    mSecondProcessList.add(process);
                                }
                            } else if (CommonCode.VALUE_TYPE_LOAN.equalsIgnoreCase(type)) {
                                String account = bankArray.getJSONObject(j).getString(CommonCode.KEY_ACCOUNT);
                                if (account != null && !TextUtils.isEmpty(account)) {
                                    ScrapProcess process = new ScrapProcess();
                                    BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
                                    process.setData(bankRequestData);
                                    process.setJob(BankTransactionParameter.VALUE_MODULE_BANK_LOAN_TRANSACTION);
                                    process.setAccount(account);
                                    process.setCode(bankRequestData.getOriginalBankCode());
                                    process.setType(bankRequestData.getOriginalBankType());
                                    mSecondProcessList.add(process);
                                }
                            }
                        }
                    }
                } else {
                    BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
                    mListener.bankComplete(bankRequestData.getOriginalBankName(), bankRequestData.getOriginalBankCode());
                    result = ScrapResult.FAIL_EMPTY;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                result = ScrapResult.FAIL_UNKNOWN;
            }
        } else {
            result = ScrapResult.FAIL;
        }
        insertScrapResult(result, jsonData, jobIndex, false);
        uploadLogMsg(result, jsonData, jobIndex);

        return result;
    }

    private int processBankDealList(String jsonData, String number, int jobIndex) {
        int result = ScrapResult.COMPLETE;
        Calendar current = Calendar.getInstance();
        mCurrentStatus = STATUS_UPLOADING;
        JSONObject jSub = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            jSub = jsonObject.getJSONObject(CommonCode.RESULT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jSub != null && jSub.has(CommonCode.KEY_LIST)) {
            BankRecordList data = new BankRecordList();
            data.setUSERID(mUserId);
            data.setTYPE(Common.TYPE_BANK_DEAL);
            data.setCMPNYCODE(mJobProcessList.get(jobIndex).getCode());
            data.setReqAccount(number);
            String uploadId = Common.TYPE_BANK_DEAL + "_"+ jobIndex + "_" + ElseUtils.getSimpleDate(current.getTime());
            data.setUPLOADID(uploadId);
            JsonObject dataResult = new JsonObject();
            dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
            data.setResult(dataResult);
            Call<Result> call = mScrapApi.sendBankRecordList(data);
            try {
                Response response = call.execute();
                writeFile(RESULT_MSG + response.raw(), jobIndex);
            } catch (IOException e) {
                e.printStackTrace();
                writeFile(RESULT_MSG + e.getMessage(), jobIndex);
            }
        } else {
            result = ScrapResult.FAIL;
        }
        insertScrapResult(result, jsonData, jobIndex, true);
        BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
        mListener.bankComplete(bankRequestData.getOriginalBankName(), bankRequestData.getOriginalBankCode());
        uploadLogMsg(result, jsonData, jobIndex);

        return result;
    }

    private int processCardList(String jsonData, int jobIndex) {
        int result = ScrapResult.COMPLETE;
        Calendar calendar = Calendar.getInstance();
        mCurrentStatus = STATUS_UPLOADING;

        if (mMode == ProcessMode.COLLECT_LOGIN) {
            BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
            mListener.loginComplete(bankRequestData, true, 0, "", "");
        } else {
            JSONObject jSub = null;
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                jSub = jsonObject.getJSONObject(CommonCode.RESULT);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jSub != null && jSub.has(CommonCode.KEY_LIST)) {
                CardList data = new CardList();
                data.setUSERID(mUserId);
                data.setTYPE(Common.TYPE_CARD_LIST);
                data.setCMPNYCODE(mJobProcessList.get(jobIndex).getCode());
                String uploadId = Common.TYPE_CARD_DUE + "_"+ jobIndex + "_" + ElseUtils.getSimpleDate(calendar.getTime());
                data.setUPLOADID(uploadId);
                JsonObject dataResult = new JsonObject();
                dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
                data.setResult(dataResult);
                Call<Result> call = mScrapApi.sendCardList(data);
                try {
                    Response response = call.execute();
                    writeFile(RESULT_MSG+response.raw(), jobIndex);
                    BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
                    if (!bankRequestData.isDaily()) {
                        for (int i=0; i<7; i++) {
                            ScrapProcess process = new ScrapProcess();
                            process.setData(bankRequestData);
                            switch (i) {
                                case 0:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_APPROVAL);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_APPROVAL);
                                    }
                                    break;
                                case 1:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_BILL);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_BILL);
                                    }
                                    break;
                                case 2:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_DUE);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_DUE);
                                    }
                                    break;
                                case 3:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_LIMIT);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_LIMIT);
                                    }
                                    break;
                                case 4:
                                    BankRequestData bill = bankRequestData.getClone();
                                    Calendar billCalendar = Calendar.getInstance();
                                    billCalendar.add(Calendar.MONTH, -1);
                                    bill.setBillDate(ElseUtils.getYearMonth(billCalendar.getTime()));
                                    process.setData(bill);
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_BILL);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_BILL);
                                    }
                                    break;
                                case 5:
                                    BankRequestData bill2 = bankRequestData.getClone();
                                    Calendar billCalendar2 = Calendar.getInstance();
                                    billCalendar2.add(Calendar.MONTH, 1);
                                    bill2.setBillDate(ElseUtils.getYearMonth(billCalendar2.getTime()));
                                    process.setData(bill2);
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_BILL);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_BILL);
                                    }
                                    break;
                                case 6:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_LOAN);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_LOAN);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            process.setCode(bankRequestData.getOriginalBankCode());
                            process.setType(bankRequestData.getOriginalBankType());
                            mSecondProcessList.add(process);
                        }
                    } else {
                        for (int i=0; i<4; i++) {
                            ScrapProcess process = new ScrapProcess();

                            process.setData(bankRequestData);
                            switch (i) {
                                case 0:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_APPROVAL);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_APPROVAL);
                                    }
                                    break;
                                case 1:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_BILL);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_BILL);
                                    }
                                    break;
                                case 2:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_DUE);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_DUE);
                                    }
                                    break;
                                case 3:
                                    if (VALUE_METHOD_ID.equalsIgnoreCase(process.getData().getLoginMethod())) {
                                        process.setJob(CardIdLoginParameter.VALUE_MODULE_CARD_LIMIT);
                                    } else {
                                        process.setJob(CardApprovalParameter.VALUE_MODULE_CARD_LIMIT);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            process.setCode(bankRequestData.getOriginalBankCode());
                            process.setType(bankRequestData.getOriginalBankType());
                            mSecondProcessList.add(process);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    writeFile(RESULT_MSG + e.getMessage(), jobIndex);
                }
            } else {
                BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
                mListener.bankComplete(bankRequestData.getOriginalBankName(), bankRequestData.getOriginalBankCode());
                result = ScrapResult.FAIL_EMPTY;
            }
            insertScrapResult(result, jsonData, jobIndex, false);
            uploadLogMsg(result, jsonData, jobIndex);
        }

        return result;
    }

    private int processCardDue(String jsonData, int jobIndex) {
        int result = ScrapResult.COMPLETE;
        Calendar calendar = Calendar.getInstance();
        mCurrentStatus = STATUS_UPLOADING;

        JSONObject jSub = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            jSub = jsonObject.getJSONObject(CommonCode.RESULT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jSub != null && jSub.has(CommonCode.KEY_LIST)) {
            CardDueList data = new CardDueList();
            data.setUSERID(mUserId);
            data.setTYPE(Common.TYPE_CARD_DUE);
            data.setCMPNYCODE(mJobProcessList.get(jobIndex).getCode());
            String uploadId = Common.TYPE_CARD_DUE + "_"+ jobIndex + "_" + ElseUtils.getSimpleDate(calendar.getTime());
            data.setUPLOADID(uploadId);
            JsonObject dataResult = new JsonObject();
            dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
            data.setResult(dataResult);
            Call<Result> call = mScrapApi.sendCardDue(data);
            try {
                Response response = call.execute();
                writeFile(RESULT_MSG+response.raw(), jobIndex);
            } catch (IOException e) {
                e.printStackTrace();
                writeFile(RESULT_MSG + e.getMessage(), jobIndex);
            }
        } else {
            result = ScrapResult.FAIL;
        }
        insertScrapResult(result, jsonData, jobIndex, true);
        uploadLogMsg(result, jsonData, jobIndex);
        return result;
    }

    private int processCardApproval(String jsonData, int jobIndex) {
        int result = ScrapResult.COMPLETE;
        Calendar calendar = Calendar.getInstance();
        mCurrentStatus = STATUS_UPLOADING;

        JSONObject jSub = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            jSub = jsonObject.getJSONObject(CommonCode.RESULT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jSub != null && jSub.has(CommonCode.KEY_LIST)) {
            CardApprovalList data = new CardApprovalList();
            data.setUSERID(mUserId);
            data.setTYPE(Common.TYPE_CARD_APPROVAL);
            data.setCMPNYCODE(mJobProcessList.get(jobIndex).getCode());
            String uploadId = Common.TYPE_CARD_APPROVAL + "_"+ jobIndex + "_" + ElseUtils.getSimpleDate(calendar.getTime());
            data.setUPLOADID(uploadId);
            JsonObject dataResult = new JsonObject();
            dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
            data.setResult(dataResult);
            Call<Result> call = mScrapApi.sendCardApprovalList(data);
            try {
                Response response = call.execute();
                writeFile(RESULT_MSG + response.raw(), jobIndex);
            } catch (IOException e) {
                e.printStackTrace();
                writeFile(RESULT_MSG + e.getMessage(), jobIndex);
            }
        } else {
            result = ScrapResult.FAIL;
        }
        insertScrapResult(result, jsonData, jobIndex, true);
        uploadLogMsg(result, jsonData, jobIndex);
        return result;
    }

    private int processCardBill(String jsonData, int jobIndex) {
        int result = ScrapResult.COMPLETE;
        Calendar calendar = Calendar.getInstance();
        mCurrentStatus = STATUS_UPLOADING;
        JSONObject jSub = null;

        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            jSub = jsonObject.getJSONObject(CommonCode.RESULT);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if (jSub != null && jSub.has(CommonCode.KEY_LIST)) {
            CardBillList data = new CardBillList();
            data.setUSERID(mUserId);
            data.setTYPE(Common.TYPE_CARD_BILL);
            data.setCMPNYCODE(mJobProcessList.get(jobIndex).getCode());
            String uploadId = Common.TYPE_CARD_BILL + "_"+ jobIndex + "_" + ElseUtils.getSimpleDate(calendar.getTime());
            data.setUPLOADID(uploadId);
            JsonObject dataResult = new JsonObject();
            dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
            data.setResult(dataResult);
            Call<Result> call = mScrapApi.sendCardBillList(data);
            try {
                Response response = call.execute();
                writeFile(RESULT_MSG + response.raw(), jobIndex);
            } catch (IOException e) {
                e.printStackTrace();
                writeFile(RESULT_MSG + e.getMessage(), jobIndex);
            }
        } else {
            result = ScrapResult.FAIL;
        }
        insertScrapResult(result, jsonData, jobIndex, true);
        uploadLogMsg(result, jsonData, jobIndex);
        return result;
    }

    private int processCardLoan(String jsonData, int jobIndex) {
        int result = ScrapResult.COMPLETE;
        mCurrentStatus = STATUS_UPLOADING;

        JSONObject jSub = null;
        Calendar calendar = Calendar.getInstance();

        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            jSub = jsonObject.getJSONObject(CommonCode.RESULT);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if (jSub != null && jSub.has(CommonCode.KEY_LIST)) {
            CardLoan data = new CardLoan();
            data.setUSERID(mUserId);
            data.setTYPE(Common.TYPE_CARD_LOAN);
            data.setCMPNYCODE(mJobProcessList.get(jobIndex).getCode());
            String uploadId = Common.TYPE_CARD_LOAN + "_"+ jobIndex + "_" + ElseUtils.getSimpleDate(calendar.getTime());
            data.setUPLOADID(uploadId);
            JsonObject dataResult = new JsonObject();
            dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
            data.setResult(dataResult);
            Call<Result> call = mScrapApi.sendCardLoan(data);
            try {
                Response response = call.execute();
                writeFile(RESULT_MSG + response.raw(), jobIndex);
            } catch (IOException e) {
                e.printStackTrace();
                writeFile(RESULT_MSG + e.getMessage(), jobIndex);
            }
        } else {
            result = ScrapResult.FAIL;
        }

        Call<Result> call = ServiceGenerator.createService(LogApi.class).sendLogCardLoan(jsonData);
        try {
            Response response = call.execute();
            writeFile(RESULT_MSG+response.raw(), jobIndex);
        } catch (IOException e) {
            e.printStackTrace();
            writeFile(RESULT_MSG + e.getMessage(), jobIndex);
        }
        insertScrapResult(result, jsonData, jobIndex, true);
        uploadLogMsg(result, jsonData, jobIndex);
        return result;
    }

    private int processCardLimit(String jsonData, int jobIndex) {
        int result = ScrapResult.COMPLETE;
        Calendar calendar = Calendar.getInstance();

        JSONObject jSub = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            jSub = jsonObject.getJSONObject(CommonCode.RESULT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jSub != null && jSub.has(CommonCode.KEY_LIST)) {
            CardLimit data = new CardLimit();
            data.setUSERID(mUserId);
            data.setTYPE(Common.TYPE_CARD_LIMIT);
            data.setCMPNYCODE(mJobProcessList.get(jobIndex).getCode());
            String uploadId = Common.TYPE_CARD_LIMIT + "_"+ jobIndex + "_" + ElseUtils.getSimpleDate(calendar.getTime());
            data.setUPLOADID(uploadId);
            JsonObject dataResult = new JsonObject();
            dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
            data.setResult(dataResult);
            Call<Result> call = mScrapApi.sendCardLimit(data);
            try {
                Response response = call.execute();
                writeFile(RESULT_MSG + response.raw(), jobIndex);
            } catch (IOException e) {
                e.printStackTrace();
                writeFile(RESULT_MSG + e.getMessage(), jobIndex);
            }
        } else {
            result = ScrapResult.FAIL;
        }
        insertScrapResult(result, jsonData, jobIndex, true);
        uploadLogMsg(result, jsonData, jobIndex);
        return result;
    }

    private int processCash(String jsonData, int jobIndex) {
        int result = ScrapResult.COMPLETE;

        if (mMode == ProcessMode.COLLECT_LOGIN) {
            BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
            mListener.loginComplete(bankRequestData, true, 0, "", "");
        } else {
            Calendar calendar = Calendar.getInstance();
            JSONObject jSub = null;
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                jSub = jsonObject.getJSONObject(CommonCode.RESULT);
            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            if (jSub != null && jSub.has(CommonCode.KEY_LIST)) {
                CashReceiptList data = new CashReceiptList();
                data.setUSERID(mUserId);
                data.setTYPE(Common.TYPE_CASH);
                String uploadId = Common.TYPE_CASH + "_"+ jobIndex + "_" + ElseUtils.getSimpleDate(calendar.getTime());
                data.setUPLOADID(uploadId);
                JsonObject dataResult = new JsonObject();
                dataResult = mGson.fromJson(jSub.toString(), JsonObject.class);
                data.setResult(dataResult);
                Call<Result> call = mScrapApi.sendCashReceiptList(data);
                try {
                    Response response = call.execute();
                    writeFile(RESULT_MSG + response.raw(), jobIndex);
                } catch (IOException e) {
                    e.printStackTrace();
                    writeFile(RESULT_MSG + e.getMessage(), jobIndex);
                }
            } else {
                result = ScrapResult.FAIL;
            }
            insertScrapResult(result, jsonData, jobIndex, false);
            uploadLogMsg(result, jsonData, jobIndex);
            BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
            mListener.bankComplete(bankRequestData.getOriginalBankName(), bankRequestData.getOriginalBankCode());
        }

        return result;
    }

    private int checkErrorCode(int jobIndex, int errorCode, String userError, String errorMessage, String data) {
        int result = 0;
        int convertError = (errorCode & 0xFFFF);
        int errorCount = 0;
        String type = mJobProcessList.get(jobIndex).getType();
        switch (convertError) {
            case 2300:
            case 2301:
            case 2302: // cert p/w error
                // cert error
                result = ScrapResult.FAIL_AUTH;
                break;
            case 2001:
            case 2200:
                // network error
                result = ScrapResult.FAIL_NETWORK;
                break;
            case 2003:
            case 2701:
            case 2702:
            case 2710:
                // site renew
                result = ScrapResult.FAIL_RENEWAL;
                break;
            case 2800:
                result = ScrapResult.FAIL_IDPWD_ERROR;
                break;
            case 2801: // password error
                result = ScrapResult.FAIL_IDPWD_ERROR;
                if (userError != null && !TextUtils.isEmpty(userError)) {
                    errorCount = Integer.valueOf(userError);
                }
                break;
            case 2802: // password error count over
                result = ScrapResult.FAIL_IDPWD_ERROR;
                break;
            case 2803: // id or password error
                result = ScrapResult.FAIL_IDPWD_ERROR;
                break;
            case 2040:
                // do nothing
                break;
            case 3002:
                result = ScrapResult.FAIL_IDPWD_ERROR;
                break;
            default:
                if (convertError == 2100) {
                    if ("WFCO90302".equalsIgnoreCase(userError)) {
                      result = ScrapResult.COMPLETE;
                    } else if (errorMessage.contains(CommonCode.ERROR_NOT_REG)) {
                        result = ScrapResult.FAIL_AUTH;
                    } else if (errorMessage.equals(CommonCode.ERROR_IGNORE_1)
                            || errorMessage.equals(CommonCode.ERROR_IGNORE_2)
                            || errorMessage.equals(CommonCode.ERROR_IGNORE_3)
                            || errorMessage.equals(CommonCode.ERROR_IGNORE_4)
                            || errorMessage.equals(CommonCode.ERROR_IGNORE_5)
                            || errorMessage.equals(CommonCode.ERROR_IGNORE_6)
                            || errorMessage.equals(CommonCode.ERROR_IGNORE_7)) {
                        result = ScrapResult.COMPLETE;

                        BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
                        UploadLogMsg msg = new UploadLogMsg();
                        msg.setUserId(Session.getInstance(mContext).getUserId());
                        if (BankData.TYPE_CASH.equalsIgnoreCase(bankRequestData.getOriginalBankType())) {
                            msg.setCompanyCode("1003");
                        } else {
                            msg.setCompanyCode(bankRequestData.getOriginalBankCode());
                        }
                        msg.setErrorCode(String.valueOf(convertError));
                        msg.setErrorMsg(errorMessage);
                        msg.setEtc(data);
                        Call<Result> call = mScrapApi.sendErrorReport(msg);
                        call.enqueue(new Callback<Result>() {
                            @Override
                            public void onResponse(Call<Result> call, Response<Result> response) {

                            }

                            @Override
                            public void onFailure(Call<Result> call, Throwable t) {

                            }
                        });
                    } else {
                        result = ScrapResult.FAIL;
                    }
                } else {
                    result = ScrapResult.FAIL;
                }
                break;
        }
        if (mMode != ProcessMode.COLLECT_LOGIN) {
            if (BankData.TYPE_BANK.equals(type)) {
                writeBankLogFile(errorMessage, mJobProcessList.get(jobIndex).getJob(), convertError, true, jobIndex);
            } else if (BankData.TYPE_CARD.equals(type)) {
                writeCardLogFile(errorMessage, mJobProcessList.get(jobIndex).getJob(), convertError, true, jobIndex);
            } else {
                writeBankLogFile(errorMessage, mJobProcessList.get(jobIndex).getJob(), convertError, true, jobIndex);
            }
            String errorData = "errorCode : "+errorCode+", \n"+
                    "errorMessage : "+errorMessage+", \n"+
                    "errorData : "+data;
            insertScrapResult(result, errorData, jobIndex, false);
            uploadLogMsg(result, String.valueOf(convertError), errorMessage, data, jobIndex);
        } else {
            BankRequestData bankRequestData = mJobProcessList.get(jobIndex).getData();
            mListener.loginComplete(bankRequestData, false, convertError, userError, errorMessage);
        }
        return result;
    }

    private void writeFile(String msg, int jobIndex) {
        File dir = new File(Environment.getExternalStorageDirectory() + BROCCOLI_PATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            String bankName = "미확인";
            bankName = mJobProcessList.get(jobIndex).getData().getOriginalBankName();
            String str =
                    DIVIDER+
                            "Name : "+bankName+"\n"+
                            "Time : "+new SimpleDateFormat("yyMMddHHmmss").format(new Date())+"\n"+
                            "Msg : "+msg+"\n";

            FileOutputStream fos = new FileOutputStream(
                    String.format("%s/"+BROCCOLI_FILE,
                            dir.getPath()
                    )
                    , true);
            fos.write(str.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e) {
        }
    }

    private void writeFile(String msg, int result, int jobIndex) {
        File dir = new File(Environment.getExternalStorageDirectory() + BROCCOLI_PATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String resultMsg = null;
        switch (result) {
            case ScrapResult.COMPLETE:
                resultMsg = "SUCCESS";
                break;
            case ScrapResult.FAIL:
                resultMsg = "FAIL";
                break;
            case ScrapResult.FAIL_AUTH:
                resultMsg = "FAIL_AUTH";
                break;
            case ScrapResult.FAIL_RENEWAL:
                resultMsg = "FAIL_RENEWAL";
                break;
            case ScrapResult.FAIL_NETWORK:
                resultMsg = "FAIL_NETWORK";
                break;
            case ScrapResult.FAIL_TIMEOUT:
                resultMsg = "FAIL_TIMEOUT";
                break;
            case ScrapResult.FAIL_IDPWD_ERROR:
                resultMsg = "FAIL_IDPWD_ERROR";
                break;
            default:
                resultMsg = "FAIL_UNKNOWN";
                break;
        }
        try {
            Date date = new Date();
            FileOutputStream fos = null;
            String bankName = "미확인";
            bankName = mJobProcessList.get(jobIndex).getData().getOriginalBankName();
            String str =
                    DIVIDER+
                    "BankName : "+bankName+"\n"+
                    "Time : "+new SimpleDateFormat("yyMMddHHmmss").format(date)+"\n"+
                    "Result : "+resultMsg+"\n"+
                    "Msg : "+(result == ScrapResult.COMPLETE ? resultMsg:msg)+"\n";
            fos = new FileOutputStream(
                    String.format("%s/"+BROCCOLI_FILE,
                            dir.getPath()
                    )
                    , true);
            fos.write(str.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e) {
        }
    }

    private void writeBankLogFile(String msg, String type, int result, boolean isDivider, int jobIndex) {
        File dir = new File(Environment.getExternalStorageDirectory() + BROCCOLI_PATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String resultMsg = null;
        switch (result) {
            case ScrapResult.COMPLETE:
                resultMsg = "SUCCESS";
                break;
            case ScrapResult.FAIL:
                resultMsg = "FAIL";
                break;
            case ScrapResult.FAIL_AUTH:
                resultMsg = "FAIL_AUTH";
                break;
            case ScrapResult.FAIL_RENEWAL:
                resultMsg = "FAIL_RENEWAL";
                break;
            case ScrapResult.FAIL_NETWORK:
                resultMsg = "FAIL_NETWORK";
                break;
            case ScrapResult.FAIL_TIMEOUT:
                resultMsg = "FAIL_TIMEOUT";
                break;
            default:
                resultMsg = "FAIL_UNKNOWN";
                break;
        }
        try {
            FileOutputStream fos = null;
            String bankName = "미확인";
            bankName = mJobProcessList.get(jobIndex).getData().getOriginalBankName();
            String str =
                    (isDivider ? DIVIDER:"")+
                    "BankName : "+bankName+"\n"+
                    "Time : "+new SimpleDateFormat("yyMMddHHmmss").format(new Date())+"\n"+
                    "Result : "+resultMsg+"\n"+
                    "Type : "+type+"\n"+
                    "Msg : "+(result == ScrapResult.COMPLETE ? resultMsg:msg)+"\n";
            fos = new FileOutputStream(
                    String.format("%s/"+BROCCOLI_FILE,
                            dir.getPath()
                    )
                    , true);
            fos.write(str.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e) {
        }
    }

    private void writeCardLogFile(String msg, String type, int result, boolean isDivider, int jobIndex) {
        File dir = new File(Environment.getExternalStorageDirectory() + BROCCOLI_PATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String resultMsg = null;
        switch (result) {
            case ScrapResult.COMPLETE:
                resultMsg = "SUCCESS";
                break;
            case ScrapResult.FAIL:
                resultMsg = "FAIL";
                break;
            case ScrapResult.FAIL_AUTH:
                resultMsg = "FAIL_AUTH";
                break;
            case ScrapResult.FAIL_RENEWAL:
                resultMsg = "FAIL_RENEWAL";
                break;
            case ScrapResult.FAIL_NETWORK:
                resultMsg = "FAIL_NETWORK";
                break;
            case ScrapResult.FAIL_TIMEOUT:
                resultMsg = "FAIL_TIMEOUT";
                break;
            case ScrapResult.FAIL_IDPWD_ERROR:
                resultMsg = "FAIL_IDPWD_ERROR";
                break;
            default:
                resultMsg = "FAIL_UNKNOWN";
                break;
        }
        try {
            FileOutputStream fos = null;
            String bankName = "미확인";
            bankName = mJobProcessList.get(jobIndex).getData().getOriginalBankName();
            String str =
                    (isDivider ? DIVIDER:"")+
                    "CardName : "+bankName+"\n"+
                    "Time : "+new SimpleDateFormat("yyMMddHHmmss").format(new Date())+"\n"+
                    "Result : "+resultMsg+"\n"+
                    "Type : "+type+"\n"+
                    "Msg : "+(result == ScrapResult.COMPLETE ? resultMsg:msg)+"\n";
            fos = new FileOutputStream(
                    String.format("%s/"+BROCCOLI_FILE,
                            dir.getPath()
                    )
                    , true);
            fos.write(str.getBytes());
            fos.flush();
            fos.close();
        } catch (IOException e) {
        }
    }

    public int getStatus() {
        return mCurrentStatus;
    }

    public void setStatus(int status) {
        mCurrentStatus = status;
    }

    public ProcessMode getMode() {
        return mMode;
    }

    public String getStatusMsg() {
        String msg = null;
        switch (mCurrentStatus) {
            case STATUS_COMPLETE:
                msg = mContext.getString(R.string.loading_status_complete);
                break;
            case STATUS_SCRAPING:
            case STATUS_UPLOADING:
                msg = mContext.getString(R.string.loading_status_str);
                break;
        }
        return msg;
    }

    private void set_synchronize_time(){
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy.MM.dd a hh:mm");
        String strNow = sdfNow.format(date);
        MainApplication.mAPPShared.setPrefString(APP.SP_CHECK_SCRAPING_DATA_TIME, strNow);
    }

    public ScrapRequester getCurrentRequester() {
        return mCurrentScrapRequester;
    }

    /**
     * 엔진에 실행중인 모듈List의 갯수를 엔진에 전달하는 함수.
     */
    @Override
    public int numberOfJobInEngine(Engine engine) {
        return mJobList.size();
    }

    /**
     * 실행할 모듈정보(moduleInfo)를 엔진에 전달하는 Callback 함수
     */
    @Override
    public HashMap<String, String> engineGetJob(Engine engine, int jobIndex) {
        if (mJobList.size() <= jobIndex)
            return null;

        HashMap<String, HashMap<String, String>> element = mJobList.get(jobIndex);
        if (element.containsKey(Engine.ENGINE_JOB_MODULE_KEY)) {
            return element.get(Engine.ENGINE_JOB_MODULE_KEY);
        }

        return null;
    }

    /**
     * 실행할 로그인정보(loginInfo) 및 파라메터(paramInfo)를 엔진에 전달하는 Callback 함수
     */
    @Override
    public String engineGetParam(Engine engine, int threadIndex, int jobIndex, String requireJSONString, boolean bSynchronous) {
        try {
            JSONObject requireJson = new JSONObject(requireJSONString);
            Iterator<String> itr = requireJson.keys();
            while(itr.hasNext()) {
                String key = (String) itr.next();

                JSONObject reqJobItem = (JSONObject) requireJson.get(key);
                HashMap<String, String> jobSourceItem = (HashMap<String, String>) ((HashMap<String, HashMap<String, String>>) mJobList.get(jobIndex)).get(key);

                Iterator<String> itrItem = jobSourceItem.keySet().iterator();
                while (itrItem.hasNext()) {
                    String keyItem = (String) itrItem.next();
                    reqJobItem.put(keyItem, jobSourceItem.get(keyItem));
                }
            }

            String retString = requireJson.toString();
            startTimeOutChecker();
            return retString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return requireJSONString;
    }

    /**
     * 엔진이 모듈을 실행 후 결과를 가져올 때 호출됩니다.(각 조회된 데이터를 처리합니다.)
     */
    @Override
    public void engineResult(Engine engine, int threadIndex, int jobIndex, int error, String userError, String errorMessage, String resultJsonString) {
        clearTimeOutChecker();
        if (error != 0) { // Error == 0 이면 정상, 아니면 Error처리 합니다.
            ScrapProcess process = mJobProcessList.get(jobIndex);
            if (!errorMessage.equals("")) { //Error 메세지가 공백으로 내려왔을 경우
                process.setError(process.getCode() + "\n(" + errorMessage + ")\n"); //모듈 실행중 에러가 발생했을 시 내려온에러 메세지를 Array에 담습니다.(오류내용을 Alert에 표시 해주기 위해)
                process.setResult(process.getCode() + "\n(" + errorMessage + ")\n");
            } else { //Error 메세지가 내려왔을 경우
                process.setError(process.getCode() + "]\n[" + "errorCode =" + String.valueOf(error & 0xFFFF) + "]\n"); //에러 일 때 내려온 에러메세지가 없다면, 에러 코드를 Array에 담습니다.( 정상처리된 모듈들의 조회 내용과 함께 표시하기 위해 )
                process.setResult(process.getCode() + "]\n[" + "errorCode =" + String.valueOf(error & 0xFFFF) + "]\n");
            }

            String type = process.getType();
            String module = process.getJob();

            /**
             * 은행 적금 상세내역 및 대출 상세내역 row ErrorCode 안 보내도록 예외처리
             */
            if (!(BankData.TYPE_BANK.equalsIgnoreCase(type) && (BankTransactionParameter.VALUE_MODULE_BANK_LOAN_TRANSACTION.equalsIgnoreCase(module) ||
                    BankTransactionParameter.VALUE_MODULE_BANK_SAVING_TRANSACTION.equalsIgnoreCase(module)))) {
                checkErrorCode(jobIndex, error, userError, errorMessage, resultJsonString);
            }

            process.setFinish(true);
        } else {
            ScrapProcess process = mJobProcessList.get(jobIndex);
            process.setResult(resultJsonString); //모듈이 에러없이 정상 처리되었을 때의 데이터를 조회결과Array에 담습니다.
            process.setFinish(true);
            String type = process.getType();
            String module = process.getJob();
            if (BankData.TYPE_BANK.equals(type)) {
                if (BankTransactionParameter.VALUE_MODULE_BANK_LIST.equals(module)) {
                    processBankList(resultJsonString, jobIndex);
                } else if (BankTransactionParameter.VALUE_MODULE_BANK_TRANSACTION.equalsIgnoreCase(module)) {
                    processBankDealList(resultJsonString, process.getAccount(), jobIndex);
                }
            } else if (BankData.TYPE_CARD.equals(type)) {
                if (CardApprovalParameter.VALUE_MODULE_CARD_LIST.equals(module) || CardIdLoginParameter.VALUE_MODULE_CARD_LIST.equalsIgnoreCase(module)) {
                    processCardList(resultJsonString, jobIndex);
                } else if (CardApprovalParameter.VALUE_MODULE_CARD_APPROVAL.equals(module) || CardIdLoginParameter.VALUE_MODULE_CARD_APPROVAL.equalsIgnoreCase(module)) {
                    processCardApproval(resultJsonString, jobIndex);
                } else if (CardApprovalParameter.VALUE_MODULE_CARD_BILL.equals(module) || CardIdLoginParameter.VALUE_MODULE_CARD_BILL.equalsIgnoreCase(module)) {
                    processCardBill(resultJsonString, jobIndex);
                } else if (CardApprovalParameter.VALUE_MODULE_CARD_LIMIT.equals(module) || CardIdLoginParameter.VALUE_MODULE_CARD_LIMIT.equalsIgnoreCase(module)) {
                    processCardLimit(resultJsonString, jobIndex);
                } else if (CardApprovalParameter.VALUE_MODULE_CARD_DUE.equals(module) || CardIdLoginParameter.VALUE_MODULE_CARD_DUE.equalsIgnoreCase(module)) {
                    processCardDue(resultJsonString, jobIndex);
                } else if (CardApprovalParameter.VALUE_MODULE_CARD_LOAN.equals(module) || CardIdLoginParameter.VALUE_MODULE_CARD_LOAN.equalsIgnoreCase(module)) {
                    processCardLoan(resultJsonString, jobIndex);
                }
            } else if (BankData.TYPE_CASH.equals(type)) {
                processCash(resultJsonString, jobIndex);
            }
        }
    }

    /**
     * 엔진시스템의 Error가 발생시호출됩니다.
     */
    @Override
    public void engineSystemError(Engine engine, int error, String errorMessage) {
        UploadLogMsg msg = new UploadLogMsg();
        msg.setUserId(Session.getInstance(mContext).getUserId());
        msg.setErrorCode(String.valueOf(error));
        msg.setErrorMsg(errorMessage);
        Call<Result> call = mScrapApi.sendErrorReport(msg);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }

    /**
     * 엔진의 상태를 가져옵니다. (status 값이 0 이면 모든 작업이 완료상태)
     */
    @Override
    public void engineStatus(Engine engine, int status) {
        switch (status) {
            case 0: //idle
                if (mSecondProcessList.size() > 0) {
                    mJobList.clear();
                    mJobProcessList.clear();
                    mJobList = new ArrayList<>();
                    mJobProcessList = new ArrayList<>();
                    for (ScrapProcess process : mSecondProcessList) {
                        String type = process.getType();
                        if (BankData.TYPE_BANK.equals(type)) {
                            addBankInquiry(process.getData(), process.getJob(), process.getAccount());
                        } else if (BankData.TYPE_CARD.equals(type)) {
                            addCardInquiry(process.getData(), process.getJob());
                        } else if (BankData.TYPE_CASH.equals(type)) {
                            addCashInquiry(process.getData(), process.getJob());
                        }
                    }
                    mSecondProcessList.clear();
                    mSecondProcessList = new ArrayList<>();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            startScraping();
                        }
                    });
                } else {
                    handler.postDelayed(delayFinish, 3000);
                }
                break;
            default:
                break;
        }
    }

    Handler handler = new Handler();
    Runnable delayFinish = new Runnable() {
        @Override
        public void run() {
            mJobList.clear();
            mJobProcessList.clear();
            mJobList = new ArrayList<>();
            mJobProcessList = new ArrayList<>();
            mSecondProcessList.clear();
            mSecondProcessList = new ArrayList<>();
            checkScarpResult();
            if (mMode != ProcessMode.COLLECT_LOGIN)
                showRequestCount(mRequestList, true);
            scrapComplete();
        }
    };

    /**
     * 실행중인 모듈의 진행중인 상태를 status로 확인할 수 있습니다.(시작, 진행중, 완료 등등)
     */
    @Override
    public void engineJobStatus(Engine engine, int threadIndex, int jobIndex, int status) {

    }


    int mJobsize = 0;
    /**
     * 실행중인 모듈의 진행상태를 percent로 확인할 수 있습니다.(Progress를 처리 가능.)
     */
    @Override
    public void engineJobPercent(Engine engine, int threadIndex, int jobIndex, int percent) {
        if (percent == 100) {
            int jobSize = 100 / mJobProcessList.size();
            mJobsize = mJobsize + jobSize;
            if (mJobsize == 33) { // 홀수일 경우 임시처리
                mJobsize = 34;
            }
            if (mJobsize >= 100) {
                mJobsize = 0;

            }
        }
    }
}
