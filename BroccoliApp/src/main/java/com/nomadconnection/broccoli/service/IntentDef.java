package com.nomadconnection.broccoli.service;

/**
 * Created by YelloHyunminJang on 16. 1. 22..
 */
public class IntentDef {

    public static final String BASE = "com.nomadconnection.";

    /**
     * 자동 업로드 ALRAM INTENT
     */
    public static final String INTENT_ACTION_UPLOAD_START = BASE + "INTENT_ACTION_UPLOAD_START";

    public static final String INTENT_ACTION_UPLOAD_FIRST = BASE + "INTENT_ACTION_UPLOAD_FIRST";

    public static final String INTENT_ACTION_UPLOAD_REFRESH = BASE + "INTENT_ACTION_UPLOAD_REFRESH";

    /**
     * 개별 업로드
     */
    public static final String INTENT_ACTION_UPLOAD_INDIVIDUAL = BASE + "INTENT_ACTION_UPLOAD_INDIVIDUAL";

    /**
     * 로그인
     */
    public static final String INTENT_ACTION_LOGIN = BASE + "INTENT_ACTION_LOGIN";

    /**
     * 재업로드
     */
    public static final String INTENT_ACTION_UPLOAD_RETRY = BASE + "INTENT_ACTION_UPLOAD_RETRY";

    /**
     * 로그인 되었는지 체크하는 Intent
     */
    public static final String INTENT_ACTION_INTERNAL_LOGIN_CHECK = BASE+"INTENT_ACTION_INTERNAL_LOGIN_CHECK";

    public static final String REGISTRATION_GENERATING = BASE+"REGISTRATION_GENERATING";
    public static final String REGISTRATION_COMPLETE = BASE+"REGISTRATION_COMPLETE";
}
