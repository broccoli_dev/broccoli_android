package com.nomadconnection.broccoli.service.mode;

/**
 * Created by YelloHyunminJang on 16. 3. 23..
 */
public class ScrapResult {
    public static final int COMPLETE = 0;
    public static final int FAIL = 1;
    public static final int FAIL_TIMEOUT = 2;  // 라이브러리에서 응답없음
    public static final int FAIL_NETWORK = 3;
    public static final int FAIL_RETRY   = 4;
    public static final int FAIL_RENEWAL = 5;
    public static final int FAIL_AUTH    = 6;
    public static final int FAIL_MISSING = 7;
    public static final int FAIL_UNKNOWN = 8;
    public static final int FAIL_EMPTY = 9;
    public static final int FAIL_IDPWD_ERROR = 10;
}
