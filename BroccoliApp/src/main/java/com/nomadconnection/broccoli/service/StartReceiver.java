package com.nomadconnection.broccoli.service;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.nomadconnection.broccoli.utils.ElseUtils;

/**
 * Created by YelloHyunminJang on 16. 4. 28..
 */
public class StartReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction() != null) {

            if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                ComponentName compName = new ComponentName(context.getPackageName(), BatchService.class.getName());
                context.startService(new Intent().setComponent(compName));
                return;
            } else if(intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {		// 강제 종료 되었을때 power 꽂혔을때 Service 살았는지 검사하여 재시작...
                new CheckServiceRunning(context).execute();
            } else if(intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED)) {
                if(intent.getData() == null) return;
                final String packageName = intent.getData().getSchemeSpecificPart();
                if(packageName == null || !packageName.equals(context.getPackageName())) return;

                ComponentName compName = new ComponentName(context.getPackageName(), BatchService.class.getName());
                context.startService(new Intent().setComponent(compName));
            }
        }
    }

    private class CheckServiceRunning extends AsyncTask<Object, Integer, Boolean> {

        private Context mContext;

        public CheckServiceRunning(Context context) {
            mContext = context;
        }

        @Override
        protected Boolean doInBackground(Object... params) {
            // TODO Auto-generated method stub
            return ElseUtils.isServiceRunning(mContext, BatchService.class.getName());
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            if (!result) {
                ComponentName compName = new ComponentName(mContext.getPackageName(), BatchService.class.getName());
                mContext.startService(new Intent().setComponent(compName));
            }
        }

    }
}
