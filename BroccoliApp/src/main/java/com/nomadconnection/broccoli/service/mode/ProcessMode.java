package com.nomadconnection.broccoli.service.mode;

/**
 * Created by YelloHyunminJang on 16. 3. 4..
 */
public enum  ProcessMode {
    COLLECT_FIRST_THIS_MONTH,
    COLLECT_FIRST_PAST_MONTH,
    COLLECT_DAILY,
    COLLECT_INDIVIDUAL,
    COLLECT_RETRY,
    COLLECT_LOGIN,
    COLLECT_IDLE
}
