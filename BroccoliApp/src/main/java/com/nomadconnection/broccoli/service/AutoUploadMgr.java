package com.nomadconnection.broccoli.service;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.database.ScrapDao;
import com.nomadconnection.broccoli.service.data.ScrapRequester;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.utils.APPSharedPrefer;
import com.nomadconnection.broccoli.utils.ElseUtils;
import com.nomadconnection.broccoli.utils.ScrapUtils;
import com.nomadconnection.broccoliespider.data.BankRequestData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class AutoUploadMgr {

    private static final String TAG = AutoUploadMgr.class.getSimpleName();
    private static final long AN_HOUR = 1000*60*60;
    private static final int SCRAPING_MAX_DAY = 87;
    private static final int SCRAPING_MIDDLE = 60;

    private final Queue<ScrapRequester> scheduleQueue = new ConcurrentLinkedQueue<>();
    private Context mContext;

    private AlarmNotification mAlarmNotification;

    private ScrapQueueProcess mScrapProcess;
    private ScrapingModule mModule;
    private boolean isFirst = false;

    public interface UploadStatusListener {
        void start(ProcessMode mode);
        void progress(String msg);
        void bankComplete(String name, String code);
        void loginComplete(BankRequestData data, boolean success, int errorCode, String errorCount, String ErrorMsg);
        void complete();
    }

    public interface OnScrapRequested {
        void OnRequested(ProcessMode mode, ScrapRequester requester);
    }

    public AutoUploadMgr(Context context, UploadStatusListener listener) {
        mContext = context;
        mAlarmNotification = new AlarmNotification(context);
        mModule = new ScrapingModule(context, listener);
        if (mScrapProcess != null && mScrapProcess.isAlive()) {
            mScrapProcess.interrupt();
        }
        mScrapProcess = new ScrapQueueProcess(scheduleQueue, onScrapRequested, mModule.getLock());
        mScrapProcess.start();
    }

    private OnScrapRequested onScrapRequested = new OnScrapRequested() {
        @Override
        public void OnRequested(ProcessMode mode, ScrapRequester requester) {
            Message message = new Message();
            message.obj = requester;
            mHandler.sendMessage(message);
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg != null) {
                ScrapRequester requester = (ScrapRequester) msg.obj;
                if (requester != null) {
                    String temp = requester.getMode();
                    ProcessMode mode = ProcessMode.valueOf(temp);
                    switch (mode) {
                        case COLLECT_FIRST_THIS_MONTH:
                            mModule.setScrapData(requester);
                            break;
                        case COLLECT_FIRST_PAST_MONTH:
                            break;
                        case COLLECT_DAILY:
                            mModule.setScrapData(requester);
                            break;
                        case COLLECT_INDIVIDUAL:
                            mModule.setScrapData(requester);
                            break;
                        case COLLECT_RETRY:
                            mModule.setScrapData(requester);
                            break;
                        case COLLECT_LOGIN:
                            mModule.setScrapData(requester);
                            break;
                        case COLLECT_IDLE:
                            break;
                    }
                }
            }
        }
    };

    public int getUploadStatus() {
        return mModule.getStatus();
    }

    public void setUploadStatus(int status) {
        mModule.setStatus(status);
    }

    public String getCurrentStatusMsg() { return mModule.getStatusMsg(); }

    public ProcessMode getUploadMode() {
        ProcessMode mode = ProcessMode.COLLECT_IDLE;
        if (mModule != null) {
            mode = mModule.getMode();
        }
        return mode;
    }

    public void startServiceMonth(String key) {
        if (Session.getInstance(mContext).isAutoLoginAvailable()) {
            ProcessMode mode  = ProcessMode.COLLECT_FIRST_THIS_MONTH;
            mAlarmNotification.registerAutoUpload();
            List<BankData> list = ScrapDao.getInstance().getValidBankData(mContext, key, Session.getInstance(mContext).getUserId());
            List<BankRequestData> requestList = new ArrayList<BankRequestData>();
            for (BankData data : list) {
                if (data != null) {
                    Calendar calendar = Calendar.getInstance();
                    Date time = calendar.getTime();
                    String endDate = ElseUtils.getYearMonthDay(time);
                    String billDate = ElseUtils.getYearMonth(time);
                    Calendar startCal = Calendar.getInstance();
                    startCal.add(Calendar.MONTH, -3);
                    String startDate = ElseUtils.getYearMonthDay(startCal.getTime());
                    if (BankData.TYPE_BANK.equals(data.getType())) {
                        BankRequestData temp = ScrapUtils.convertBankRequestData(data, startDate, endDate, false);
                        requestList.add(temp);
                    } else if (BankData.TYPE_CARD.equals(data.getType())) {
                        requestList.add(ScrapUtils.convertCardRequestData("", data, startDate, endDate, billDate, false));
                    } else if (BankData.TYPE_CASH.equals(data.getType())) {
                        requestList.add(ScrapUtils.convertCashRequestData("", data, startDate, endDate, null, false));
                    }
                }
            }

            ScrapRequester requester = new ScrapRequester();
            requester.setMode(mode.name());
            requester.setData(requestList);
            requester.setTime(new Date().toString());
            generateQueue(requester);
        }
    }

    private boolean isAvailableTime() {
        boolean available = true;
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        Date start = calendar.getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        Date end = calendar.getTime();

        if (date.compareTo(start) >= 0 && date.compareTo(end) < 0) {
            available = false;
        }
        return available;
    }

    public void startServiceOneShot(String key, int day) {
        String date = APPSharedPrefer.getInstance(mContext).getPrefString(APP.BATCH_SERVICE_DAILY_RECORD);
        boolean isAvailable = true;
        if (!isAvailableTime()) {
            isAvailable = false;
        }
        if (isAvailable && date != null && !date.isEmpty()) {
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            Date currentTime = Calendar.getInstance().getTime();
            try {
                String parseDate = ElseUtils.getYearMonthDay(currentTime);
                String parseStoreDate = null;
                if (date.length() <= 8) {
                    parseStoreDate = date;
                } else {
                    parseStoreDate = ElseUtils.getSimpleDateToYearMonthDay(date);
                }

                Date lastActiveTime = format.parse(parseStoreDate);
                Date current = format.parse(parseDate);
                int compare = current.compareTo(lastActiveTime);
                if (compare <= 0) {
                    isAvailable = false;
                }
                // 테스트용이다 나중에 반드시 지우자 한 시간마다 테스트 가능하도록 수정
//                long differTime = current.getTime() - lastActiveTime.getTime();
//                if (differTime > AN_HOUR) {
//                    isAvailable = true;
//                } else {
//                    isAvailable = false;
//                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (Session.getInstance(mContext).isAutoLoginAvailable() && isAvailable) {
            ProcessMode mode  = ProcessMode.COLLECT_DAILY;
            mAlarmNotification.registerAutoUpload();
            List<BankData> list = ScrapDao.getInstance().getValidBankData(mContext, key, Session.getInstance(mContext).getUserId());
            List<BankRequestData> requestList = new ArrayList<BankRequestData>();
            if (day >= SCRAPING_MAX_DAY) {
                mode = ProcessMode.COLLECT_INDIVIDUAL;
                day = SCRAPING_MAX_DAY;
            } else if (day >= SCRAPING_MIDDLE) {
                mode = ProcessMode.COLLECT_INDIVIDUAL;
            } else if (day <= 3) {
                day = 3;
            }
            for (BankData data : list) {
                if (data != null) {
                    Calendar calendar = Calendar.getInstance();
                    Date time = calendar.getTime();
                    String endDate = ElseUtils.getYearMonthDay(time);
                    String billDate = ElseUtils.getYearMonth(time);
                    Calendar startCal = Calendar.getInstance();
                    startCal.add(Calendar.DAY_OF_MONTH, day * -1);
                    String startDate = ElseUtils.getYearMonthDay(startCal.getTime());
                    if (BankData.TYPE_BANK.equals(data.getType())) {
                        BankRequestData temp = ScrapUtils.convertBankRequestData(data, startDate, endDate, true);
                        requestList.add(temp);
                    } else if (BankData.TYPE_CARD.equals(data.getType())) {
                        requestList.add(ScrapUtils.convertCardRequestData("", data, startDate, endDate, billDate, false));
                    } else if (BankData.TYPE_CASH.equals(data.getType())) {
                        requestList.add(ScrapUtils.convertCashRequestData("", data, startDate, endDate, null, true));
                    }
                }
            }
            ScrapRequester requester = new ScrapRequester();
            requester.setMode(mode.name());
            requester.setData(requestList);
            requester.setTime(new Date().toString());
            generateQueue(requester);
        } else {
            setUploadStatus(ScrapingModule.STATUS_COMPLETE);
        }
    }

    public void startServiceDaily(String key) {
        boolean isAvailable = true;
        if (Session.getInstance(mContext).isAutoLoginAvailable() && isAvailable) {
            ProcessMode mode  = ProcessMode.COLLECT_DAILY;
            mAlarmNotification.registerAutoUpload();
            List<BankData> list = ScrapDao.getInstance().getValidBankData(mContext, key, Session.getInstance(mContext).getUserId());
            List<BankRequestData> requestList = new ArrayList<BankRequestData>();
            for (BankData data : list) {
                if (data != null) {
                    Calendar calendar = Calendar.getInstance();
                    Date time = calendar.getTime();
                    String endDate = ElseUtils.getYearMonthDay(time);
                    String billDate = ElseUtils.getYearMonth(time);
                    Calendar startCal = Calendar.getInstance();
                    startCal.add(Calendar.DAY_OF_MONTH, -3);
                    String startDate = ElseUtils.getYearMonthDay(startCal.getTime());
                    if (BankData.TYPE_BANK.equals(data.getType())) {
                        BankRequestData temp = ScrapUtils.convertBankRequestData(data, startDate, endDate, true);
                        requestList.add(temp);
                    } else if (BankData.TYPE_CARD.equals(data.getType())) {
                        requestList.add(ScrapUtils.convertCardRequestData("", data, startDate, endDate, billDate, false));
                    } else if (BankData.TYPE_CASH.equals(data.getType())) {
                        requestList.add(ScrapUtils.convertCashRequestData("", data, startDate, endDate, null, true));
                    }
                }
            }
            ScrapRequester requester = new ScrapRequester();
            requester.setMode(mode.name());
            requester.setData(requestList);
            requester.setTime(new Date().toString());
            generateQueue(requester);
        }
    }

    public void startServiceIndividual(ArrayList<BankData> list) {
        if (Session.getInstance(mContext).isAutoLoginAvailable()) {
            ProcessMode mode  = ProcessMode.COLLECT_INDIVIDUAL;
            mAlarmNotification.registerAutoUpload();
            List<BankRequestData> requestList = new ArrayList<BankRequestData>();
            for (BankData data : list) {
                if (data != null) {
                    Calendar calendar = Calendar.getInstance();
                    Date time = calendar.getTime();
                    String endDate = ElseUtils.getYearMonthDay(time);
                    String billDate = ElseUtils.getYearMonth(time);
                    Calendar startCal = Calendar.getInstance();
                    startCal.add(Calendar.MONTH, -3);
                    String startDate = ElseUtils.getYearMonthDay(startCal.getTime());
                    if (BankData.TYPE_BANK.equals(data.getType())) {
                        BankRequestData temp = ScrapUtils.convertBankRequestData(data, startDate, endDate, false);
                        requestList.add(temp);
                    } else if (BankData.TYPE_CARD.equals(data.getType())) {
                        requestList.add(ScrapUtils.convertCardRequestData("", data, startDate, endDate, billDate, false));
                    } else if (BankData.TYPE_CASH.equals(data.getType())) {
                        requestList.add(ScrapUtils.convertCashRequestData("", data, startDate, endDate, null, false));
                    }
                }
            }
            ScrapRequester requester = new ScrapRequester();
            requester.setMode(mode.name());
            requester.setData(requestList);
            requester.setTime(new Date().toString());
            generateQueue(requester);
        }
    }

    public void loginService(ArrayList<BankData> list) {
        ProcessMode mode  = ProcessMode.COLLECT_LOGIN;
        mAlarmNotification.registerAutoUpload();
        List<BankRequestData> requestList = new ArrayList<BankRequestData>();
        for (BankData data : list) {
            if (data != null) {
                Calendar calendar = Calendar.getInstance();
                Date time = calendar.getTime();
                String endDate = ElseUtils.getYearMonthDay(time);
                String billDate = ElseUtils.getYearMonth(time);
                Calendar startCal = Calendar.getInstance();
                startCal.add(Calendar.MONTH, -3);
                String startDate = ElseUtils.getYearMonthDay(startCal.getTime());
                if (BankData.TYPE_BANK.equals(data.getType())) {
                    BankRequestData temp = ScrapUtils.convertBankRequestData(data, startDate, endDate, false);
                    requestList.add(temp);
                } else if (BankData.TYPE_CARD.equals(data.getType())) {
                    requestList.add(ScrapUtils.convertCardRequestData("", data, startDate, endDate, billDate, false));
                } else if (BankData.TYPE_CASH.equals(data.getType())) {
                    requestList.add(ScrapUtils.convertCashRequestData("", data, startDate, endDate, null, false));
                }
            }
        }
        ScrapRequester requester = new ScrapRequester();
        requester.setMode(mode.name());
        requester.setData(requestList);
        requester.setTime(new Date().toString());
        generateQueue(requester);
    }

    public void startServiceRetry(ArrayList<BankRequestData> list) {
        if (Session.getInstance(mContext).isAutoLoginAvailable()) {
            ProcessMode mode  = ProcessMode.COLLECT_RETRY;
            mAlarmNotification.registerAutoUpload();
            List<BankRequestData> requestList = list;
//            for (BankData data : list) {
//                if (data != null) {
//                    Calendar calendar = Calendar.getInstance();
//                    String time = calendar.getTime().toString();
//                    String endDate = ElseUtils.getYearMonthDay(time);
//                    Calendar startCal = Calendar.getInstance();
//                    startCal.add(Calendar.MONTH, -1);
//                    String startDate = ElseUtils.getYearMonthDay(startCal.getTime().toString());
//                    if (BankData.TYPE_BANK.equals(data.getType())) {
//                        BankRequestData temp = convertBankRequestData(data, startDate, endDate);
//                        requestList.add(temp);
//                    } else if (BankData.TYPE_CARD.equals(data.getType())) {
//                        String billDate = ElseUtils.getYearMonth(time);
//                        requestList.add(convertCardRequestData(BankRequestData.VALUE_MODULE_CARD_DUE + "," +
//                                BankRequestData.VALUE_MODULE_CARD_APPROVAL + "," +
//                                BankRequestData.VALUE_MODULE_CARD_BILL + "," +
//                                BankRequestData.VALUE_MODULE_CARD_LIMIT, data, startDate, endDate, billDate));
//                    } else if (BankData.TYPE_CASH.equals(data.getType())) {
//                        requestList.add(convertCashRequestData(BankRequestData.VALUE_MODULE_CASH, data, startDate, endDate, null));
//                    }
//                }
//            }
            ScrapRequester requester = new ScrapRequester();
            requester.setMode(mode.name());
            requester.setData(requestList);
            requester.setTime(new Date().toString());
            generateQueue(requester);
        }
    }

    public void generateQueue(ScrapRequester requester) {
        scheduleQueue.offer(requester);
        synchronized (scheduleQueue) {
            scheduleQueue.notifyAll();
        }
    }

    public ArrayList<ScrapRequester> getCurrentQueue() {
        ArrayList<ScrapRequester> list = new ArrayList<>();
        Iterator<ScrapRequester> iterator = null;
        synchronized (scheduleQueue) {
            iterator = scheduleQueue.iterator();
        }
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        if (mModule.getCurrentRequester() != null) {
            list.add(mModule.getCurrentRequester());
        }
        return list;
    }
}
