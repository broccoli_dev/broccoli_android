package com.nomadconnection.broccoli.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;

import com.nomadconnection.broccoli.config.APP;
import com.nomadconnection.broccoli.data.FinancialLogin;
import com.nomadconnection.broccoli.data.Scrap.BankData;
import com.nomadconnection.broccoli.service.data.ScrapRequester;
import com.nomadconnection.broccoli.service.mode.ProcessMode;
import com.nomadconnection.broccoli.session.Session;
import com.nomadconnection.broccoli.constant.Const;
import com.nomadconnection.broccoli.constant.ErrorMessage;
import com.nomadconnection.broccoliespider.data.BankRequestData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class BatchService extends Service {
	public static final String TAG = BatchService.class.getSimpleName();
	public static final int ALARM_SERVICE_START = 50000;        // 서비스 재시작
	public static final int MSG_UNREGISTER_CLIENT = 1;
	public static final int MSG_REGISTER_CLIENT = 2;
	public static final int MSG_UPLOAD_STATUS = 3;
	public static final int MSG_UPLOAD_MODE = 4;
	public static final int MSG_UPLOAD_COMPLETE = 5;
	public static final int MSG_REQUEST_QUEUE = 6;
	public static final int MSG_BANK_COMPLETE = 7;
	public static final int MSG_LOGIN_COMPLETE = 8;
	private static final String SERVICE_RESTART_INTENT = "com.nomadconnection.intent.action.SERVICE_RESTART";
	private static final long SERVICE_RESTART_TIME_DELAY = 10 * 1000;
	final Messenger mMessenger = new Messenger(new messageHandler());
	ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	private Handler mHandler = new Handler();
	private AutoUploadMgr mAutoUploader;
	private WakefulBroadcastReceiver mAlarmReceiver;
	private BroadcastReceiver mAppConfigReceiver;
	private PowerManager.WakeLock mWakeLock;
	private WifiManager.WifiLock mWifiLock;

	private void sendCompleteMsg() {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				mClients.get(i).send(Message.obtain(null,
						MSG_UPLOAD_COMPLETE, MSG_UPLOAD_COMPLETE, 0));
			} catch (RemoteException e) {
				// The client is dead.  Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}

	private void sendProcessModeMessage(ProcessMode mode) {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				mClients.get(i).send(Message.obtain(null,
						MSG_UPLOAD_MODE, mode.ordinal(), 0));
			} catch (RemoteException e) {
				// The client is dead.  Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}

	private void sendStatusMessage(int status) {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				Message msg = Message.obtain(null,
						MSG_UPLOAD_STATUS, status, 0);
				if (status == ScrapingModule.STATUS_SCRAPING
						|| status == ScrapingModule.STATUS_UPLOADING) {
					msg.obj = mAutoUploader.getCurrentStatusMsg();
				}
				mClients.get(i).send(msg);
			} catch (RemoteException e) {
				// The client is dead.  Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}

	private void sendScrapMessage(String msg) {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				Message message = Message.obtain(null,
						MSG_UPLOAD_STATUS, ScrapingModule.STATUS_SCRAPING, 0);
				message.obj = msg;
				mClients.get(i).send(message);
			} catch (RemoteException e) {
				// The client is dead.  Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}

	private void sendQueueList(ArrayList<ScrapRequester> list) {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				Message message = Message.obtain(null,
						MSG_REQUEST_QUEUE, 0, 0);
				message.obj = list;
				mClients.get(i).send(message);
			} catch (RemoteException e) {
				// The client is dead.  Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}

	private void sendBankCompleteMsg(ArrayList<ScrapRequester> list) {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				Message message = Message.obtain(null,
						MSG_BANK_COMPLETE, 0, 0);
				message.obj = list;
				mClients.get(i).send(message);
			} catch (RemoteException e) {
				// The client is dead.  Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}

	private void sendLoginCompleteMsg(FinancialLogin data) {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				Message message = Message.obtain(null,
						MSG_LOGIN_COMPLETE, 0, 0);
				message.obj = data;
				mClients.get(i).send(message);
			} catch (RemoteException e) {
				// The client is dead.  Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}

	@Override
    public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
    }

	
//=============================================================================================//
// 	override method
//=============================================================================================//

	@Override
	public void onCreate() {
        super.onCreate();
        showMessage();
	}
     
	@Override
	public void onRebind(Intent intent) {
		super.onRebind(intent);
	}

	@Override
    public boolean onUnbind(Intent intent) {
    	return super.onUnbind(intent);
    }

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			if (SERVICE_RESTART_INTENT.equals(intent.getAction())) {

			}
		}
		startAlarmManager();
//		setWakeLock();
//		setWifiLock();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		restartService(null);
		clearRegister();
		clearWakeLock();
		clearWifiLock();
	}

	private void startAlarmManager() {
		if(mAutoUploader == null) {
			AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			alarm.cancel(PendingIntent.getService(this, ALARM_SERVICE_START,
					new Intent(this, this.getClass()), PendingIntent.FLAG_UPDATE_CURRENT));

			mAutoUploader = new AutoUploadMgr(getBaseContext(), new AutoUploadMgr.UploadStatusListener() {

				@Override
				public void start(ProcessMode mode) {
					ArrayList<ScrapRequester> list = mAutoUploader.getCurrentQueue();
					sendQueueList(list);
					acquireWakeLock();
					acquireWifiLock();
				}

				@Override
				public void progress(String msg) {
					sendScrapMessage(msg);
				}

				@Override
				public void bankComplete(String name, String code) {
					ArrayList<ScrapRequester> list = mAutoUploader.getCurrentQueue();
					sendBankCompleteMsg(list);
				}

				@Override
				public void loginComplete(BankRequestData requestData, boolean success, int errorCode, String userError, String errorMsg) {
					FinancialLogin data = new FinancialLogin();
					data.setRequestData(requestData);
					data.setSuccess(success);
					data.setErrorCode(errorCode);
					data.setUserError(userError);
					data.setErrorMsg(errorMsg);
					sendLoginCompleteMsg(data);
				}

				@Override
				public void complete() {
					sendCompleteMsg();
					ArrayList<ScrapRequester> list = mAutoUploader.getCurrentQueue();
					sendBankCompleteMsg(list);
					releaseWakeLock();
					releaseWifiLock();
				}
			});
			AlarmReceiver();
			//ConfigReceiver();
		}
	}

//=============================================================================================//
// 	private method
//=============================================================================================//

    public void showMessage() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				showMessage();
			}
		}, APP.BATCH_SERVICE_REFRESH_TIME);
    }

	/**
	 * 서비스 재시작
	 */
	private void restartService(String token) {
		Intent intent = new Intent(this, getClass());
		if (!TextUtils.isEmpty(token)) {
			intent.setAction(SERVICE_RESTART_INTENT);
		}
		PendingIntent pending = PendingIntent.getService(this, ALARM_SERVICE_START,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		long triggerTime = System.currentTimeMillis() + SERVICE_RESTART_TIME_DELAY;
		alarm.set(AlarmManager.RTC_WAKEUP, triggerTime, pending);
	}

	private void setWakeLock() {
		if (mWakeLock == null) {
			PowerManager powerManager = (PowerManager) getBaseContext().getSystemService(Context.POWER_SERVICE);
			mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "wakelock");
		}
	}

	private void acquireWakeLock() {
		if (mWakeLock != null) {
			mWakeLock.acquire();
		}
	}

	private void releaseWakeLock() {
		if (mWakeLock != null) {
			try {
				if (mWakeLock.isHeld())
					mWakeLock.release();
			} catch (Throwable throwable) {

			}
		}
	}

	private void clearWakeLock() {
		if (mWakeLock != null) {
			try {
				if (mWakeLock.isHeld())
					mWakeLock.release();
			} catch (Throwable throwable) {

			}
			mWakeLock = null;
		}
	}

	private void setWifiLock() {
		WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		mWifiLock = wifiManager.createWifiLock("wifilock");
		mWifiLock.setReferenceCounted(true);
	}

	public void acquireWifiLock() {
		if (mWifiLock != null) {
			mWifiLock.acquire();
		}
	}

	public void releaseWifiLock() {
		if (mWifiLock != null) {
			try {
				if (mWifiLock.isHeld())
					mWifiLock.release();
			} catch (Throwable throwable) {

			}
		}
	}

	private void clearWifiLock() {
		if (mWifiLock != null) {
			if (mWifiLock.isHeld())
				mWifiLock.release();
			mWifiLock = null;
		}
	}

	private void AlarmReceiver() {

		if(mAlarmReceiver != null) return;
		mAlarmReceiver = new WakefulBroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {

				String action = intent.getAction();

				if(action == null) return;

				if(action.equals(IntentDef.INTENT_ACTION_INTERNAL_LOGIN_CHECK)) {
					if (Session.getInstance(getBaseContext()).isLogin()) {

					}
				}

				//writeFile(action, action);

				if(action.equals(IntentDef.INTENT_ACTION_UPLOAD_START)) {
					acquireWakeLock();
					mAutoUploader.setUploadStatus(ScrapingModule.STATUS_PREPARE);
					final int day = intent.getIntExtra(Const.SCRAPING_DAY, 3);
					Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
						@Override
						public void acquired(String str) {
							mAutoUploader.startServiceOneShot(str, day);
						}

						@Override
						public void error(ErrorMessage errorMessage) {

						}

						@Override
						public void fail() {

						}
					});
				} else if (action.equals(IntentDef.INTENT_ACTION_UPLOAD_REFRESH)) {
					acquireWakeLock();
					mAutoUploader.setUploadStatus(ScrapingModule.STATUS_PREPARE);
					Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
						@Override
						public void acquired(String str) {
							mAutoUploader.startServiceDaily(str);
						}

						@Override
						public void error(ErrorMessage errorMessage) {

						}

						@Override
						public void fail() {

						}
					});
				} else if (action.equals(IntentDef.INTENT_ACTION_UPLOAD_FIRST)) {
					acquireWakeLock();
					mAutoUploader.setUploadStatus(ScrapingModule.STATUS_PREPARE);
					Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
						@Override
						public void acquired(String str) {
							mAutoUploader.startServiceMonth(str);
						}

						@Override
						public void error(ErrorMessage errorMessage) {

						}

						@Override
						public void fail() {

						}
					});
				} else if (action.equals(IntentDef.INTENT_ACTION_LOGIN)) {
					acquireWakeLock();
					ArrayList<BankData> list = (ArrayList<BankData>) intent.getSerializableExtra(Const.DATA);
					mAutoUploader.setUploadStatus(ScrapingModule.STATUS_PREPARE);
					mAutoUploader.loginService(list);
				} else if (action.equals(IntentDef.INTENT_ACTION_UPLOAD_INDIVIDUAL)) {
					acquireWakeLock();
					ArrayList<BankData> list = (ArrayList<BankData>) intent.getSerializableExtra(Const.DATA);
					mAutoUploader.setUploadStatus(ScrapingModule.STATUS_PREPARE);
					mAutoUploader.startServiceIndividual(list);
				} else if (action.equals(IntentDef.INTENT_ACTION_UPLOAD_RETRY)) {
					acquireWakeLock();
					ArrayList<BankRequestData> list = (ArrayList<BankRequestData>) intent.getSerializableExtra(Const.DATA);
					mAutoUploader.setUploadStatus(ScrapingModule.STATUS_PREPARE);
					mAutoUploader.startServiceRetry(list);
				}
			}
		};

		IntentFilter filter = new IntentFilter();
		filter.addAction(IntentDef.INTENT_ACTION_INTERNAL_LOGIN_CHECK);
		filter.addAction(IntentDef.INTENT_ACTION_UPLOAD_START);
		filter.addAction(IntentDef.INTENT_ACTION_UPLOAD_FIRST);
		filter.addAction(IntentDef.INTENT_ACTION_UPLOAD_INDIVIDUAL);
		filter.addAction(IntentDef.INTENT_ACTION_LOGIN);
		filter.addAction(IntentDef.INTENT_ACTION_UPLOAD_RETRY);
		filter.addAction(IntentDef.INTENT_ACTION_UPLOAD_REFRESH);
		getBaseContext().registerReceiver(mAlarmReceiver, filter);
	}

	private void ConfigReceiver() {
		if (mAppConfigReceiver != null) return;
		mAppConfigReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent != null) {
					String action = intent.getAction();
					if (Intent.ACTION_SCREEN_OFF.equals(action)) {

					} else if (Intent.ACTION_SCREEN_ON.equals(action)) {
						acquireWakeLock();
						Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
							@Override
							public void acquired(String str) {
								mAutoUploader.startServiceDaily(str);
							}

							@Override
							public void error(ErrorMessage errorMessage) {

							}

							@Override
							public void fail() {

							}
						});
					} else if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
						acquireWakeLock();
						Session.getInstance(getApplicationContext()).acquirePublic(new Session.OnKeyInterface() {
							@Override
							public void acquired(String str) {
								mAutoUploader.startServiceDaily(str);
							}

							@Override
							public void error(ErrorMessage errorMessage) {

							}

							@Override
							public void fail() {

							}
						});
					}
				}
			}
		};
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		filter.addAction(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_BOOT_COMPLETED);
		filter.addAction(Intent.ACTION_USER_PRESENT);
		getBaseContext().registerReceiver(mAppConfigReceiver, filter);
	}

	private void clearRegister() {
		if (mAlarmReceiver != null)
			getBaseContext().unregisterReceiver(mAlarmReceiver);
		if (mAppConfigReceiver != null)
			getBaseContext().unregisterReceiver(mAppConfigReceiver);
	}

	private void writeFile(String msg, String sub) {
		File dir = new File(Environment.getExternalStorageDirectory() + "/Broccoli");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		try {
			FileOutputStream fos = new FileOutputStream(
					String.format("%s/%s-%s-%s.txt",
							dir.getPath(),
							"AutoAlarm",
							sub,
							new SimpleDateFormat("yyMMddHHmmss").format(new Date())
					)
			);
			fos.write(msg.getBytes());
			fos.flush();
			fos.close();
		} catch (IOException e) {
		}
	}

	class messageHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_REGISTER_CLIENT:
					mClients.add(msg.replyTo);
					if (mAutoUploader != null) {
						sendProcessModeMessage(mAutoUploader.getUploadMode());
					}
					break;
				case MSG_UNREGISTER_CLIENT:
					mClients.remove(msg.replyTo);
					break;
				case MSG_UPLOAD_STATUS:
					if (mAutoUploader != null) {
						int status = mAutoUploader.getUploadStatus();
						sendStatusMessage(status);
					}
					break;
				case MSG_UPLOAD_MODE:
					break;
				case MSG_REQUEST_QUEUE:
					if (mAutoUploader != null) {
						ArrayList<ScrapRequester> list = mAutoUploader.getCurrentQueue();
						sendQueueList(list);
					}
					break;
				default:
					super.handleMessage(msg);
			}
		}
	}
}

