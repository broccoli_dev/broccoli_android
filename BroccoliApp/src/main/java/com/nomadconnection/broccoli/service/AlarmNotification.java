package com.nomadconnection.broccoli.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by YelloHyunminJang on 16. 1. 21..
 */
public class AlarmNotification {

    public static final int ALARM_ACTION_AUTO_UPLOAD = 100;
    private static long UPLOAD_START_TIME						= AlarmManager.INTERVAL_HOUR/20;//AlarmManager.INTERVAL_DAY;
    private Context mContext;
    private BroadcastReceiver mAlarmReceiver = null;
    private Random mRandom = new Random();

    public AlarmNotification(Context context) {
        mContext = context;
    }

    public static void registerAlarm(Context context, int id, Intent intent, long time) {
        AlarmManager manager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent sender = PendingIntent.getBroadcast(context, id, new Intent(intent), PendingIntent.FLAG_CANCEL_CURRENT);
        manager.set(AlarmManager.RTC_WAKEUP, time, sender);
    }

    /*
	 * 알람 해제
	 */
    private static void unregisterAlarm(Context context, int id, String intent) {
        AlarmManager manager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(PendingIntent.getBroadcast(context, id, new Intent(intent), PendingIntent.FLAG_UPDATE_CURRENT));
    }

    /**
     * 매일 화면 켜졌을 때만 스크래핑 하기로 함
     */
    public void registerAutoUpload() {
//        registerAlarm(mContext, ALARM_ACTION_AUTO_UPLOAD, IntentDef.INTENT_ACTION_UPLOAD_START,
//                setTriggerTime(), UPLOAD_START_TIME);
    }

    public void unregisterAutoUpload() {
        unregisterAlarm(mContext, ALARM_ACTION_AUTO_UPLOAD, IntentDef.INTENT_ACTION_UPLOAD_START);
    }

    // trigger 시간이 현재 시간보다 작을때는 바로 알람 발생하고 현재 시간에서 trigger 시간 후에 다시 알리고 그다음부터 interval대로..
    // tirgger 시간보다 클때는 trigger 시간에서 현재시간을 뺀 시간에 알리고 그담부터는 interval대로
	/*
	 * 알람 등록
	 */
    private void registerAlarm(Context context, int id, String intent, long triggerTime, long interval) {
        PendingIntent sender = PendingIntent.getBroadcast(context, id, new Intent(intent), PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager manager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, triggerTime, interval, sender);
    }

    private void registerAlarm(Context context, int id, Intent intent, long triggerTime, long interval) {
        PendingIntent sender = PendingIntent.getBroadcast(context, id, new Intent(intent), PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager manager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, triggerTime, interval, sender);
    }

    private long setTriggerTime() {
        long currentTime = System.currentTimeMillis();
        Calendar time = Calendar.getInstance();
        //time.set(Calendar.HOUR_OF_DAY, getRandom(2, 22));
        time.add(Calendar.MINUTE, 5);
        long btTime = time.getTimeInMillis();
        long triggerTime = btTime;
//        Log.d(AlarmNotification.class.getSimpleName(), "triggerDate = "+time.getTime());
//        Log.d(AlarmNotification.class.getSimpleName(), "currentTime = "+currentTime);
//        Log.d(AlarmNotification.class.getSimpleName(), "triggerTime = "+time.getTimeInMillis());
        if (currentTime > btTime) {
            triggerTime += 1000 * 60 * 60 * 24;
        }
        return triggerTime;
    }

    public int getRandom(int max, int offset) {
        int ret = mRandom.nextInt(max)+offset;
        return ret;
    }

}
