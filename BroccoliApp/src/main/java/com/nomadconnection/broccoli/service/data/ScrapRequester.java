package com.nomadconnection.broccoli.service.data;


import com.nomadconnection.broccoliespider.data.BankRequestData;

import java.util.List;

/**
 * Created by YelloHyunminJang on 16. 3. 4..
 */
public class ScrapRequester {
    private String mode;
    private String time;
    private List<BankRequestData> data;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<BankRequestData> getData() {
        return data;
    }

    public void setData(List<BankRequestData> data) {
        this.data = data;
    }
}
