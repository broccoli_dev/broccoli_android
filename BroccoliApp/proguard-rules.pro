# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/whylee/dev/android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes Exceptions, Signature, InnerClasses
-dontwarn org.spongycastle.**
-dontwarn okio.**
-dontwarn com.loopj.android.http.**
-dontwarn retrofit2.Platform$Java8
-dontwarn com.nomadconnection.nsecurity.**
-dontwarn sun.security.ssl.**
-dontwarn java.net.HttpURLConnection
-dontwarn com.github.mikephil.**

## proguard espider heenam
-keepclasseswithmembernames class * { native <methods>;
}
-dontwarn com.heenam.espider.**
-keep class com.heenam.espider.** { *;
}

-keep class com.lumensoft.**
-keep class com.signkorea.**
-keep class com.raonsecure.**
-keep class com.softsecurity.**
-dontwarn com.lumensoft.**
-dontwarn com.signkorea.**
-dontwarn com.raonsecure.**
-dontwarn com.softsecurity.**

##---------------Begin: proguard configuration for Igaworks Common  ----------
-keep class com.igaworks.** { *; }
-dontwarn com.igaworks.**
##---------------End: proguard configuration for Igaworks Common   ----------

-libraryjars libs
-keep class com.crashlytics.** { *; }
#-keepattributes SourceFile,LineNumberTable
#-renamesourcefileattribute SourceFile

-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class android.support.v7.app.** { *; }
-keep interface android.support.v7.app.** { *; }

# Keep the pojos used by GSON or Jackson
-keep class com.futurice.project.models.pojo.** { *; }

# Keep GSON stuff
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.** { *; }

# Keep Jackson stuff
-keep class org.codehaus.** { *; }
-keep class com.fasterxml.jackson.annotation.** { *; }

# Keep these for GSON and Jackson
-keepattributes Signature
-keepattributes *Annotation*
-keepattributes EnclosingMethod

# Keep Retrofit
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.** *;
}
-keepclassmembers class * {
    @retrofit.** *;
}

-keepclassmembers enum * { *; }
-keepclassmembers enum com.nomadconnection.broccoli.** { *; }

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-keep class org.spongycastle.** { *; }
-keep class com.nomadconnection.broccoli.api.** { *; }
-keep class com.nomadconnection.broccoli.data.** { *; }
-keep class com.nomadconnection.broccoli.constant.** { *; }
-keep class org.acra.** { *; }
-keep class com.nomadconnection.nsecurity.** { *; }
-keep class sun.security.ssl.** { *; }
-keep class com.github.mikephil.** { *; }

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}