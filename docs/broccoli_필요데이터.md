은행 계좌
--
**level 1**

사용자가 등록한 계좌의 잔액의 
합계와 최근 거래 확인


```
스크래핑 
summary {
	account_count number
	total_amount number
	latest_transaction {
		time date
		type [+,-]
		amount number
		context string
		method string
	}
}
```

**level 2**

사용자가 등록한 계좌별로 
잔액확인과 최근 거래 확인

```
스크래핑 
account[] {
	bank_name string
	name string
	number string
	balance number
	latest_transaction {
		time date
		type [+,-]
		amount number
		context string
		method string
	}
}
```

**level 3**

사용자의 등록한 계좌의 입출금 내역

```
스크래핑 
filter {date_range, type}
transaction[] {
	time date
	type [+,-]
	amount number
	context string
	method string
}
```

신용 카드
--


**level 1**

사용자가 등록한 신용카드의 결제 예정 금액의 합계와 최근 거래 확인

```
스크래핑 
summary {
	card_count number
	total_amount number
	latest_transaction {
		time date
		amount number
		context string
		method [일시불,할부]
	}
}
```

**level 2**

사용자 등록한 신용카드별로 
결제 예정 금액 확인과 최근 거래 확인

```
스크래핑 
card[] {
	card_company string
	name string
	number string
	payment_date date
	payment_amount number
	total_amount number
	latest_transaction {
		time date
		amount number
		context string
		method [일시불,할부]
	}
}
```

**level 3**

사용자가 등록한 신용카드의 이용 내역 확인

```
스크래핑 
filter {date_range, method}
transaction[] {
	time date
	amount number
	context string
	method string
	approval boolean
}
```

소비
--

사용자의 거래 내용 중 소비에 해당하는 항목을 통합하여 확인

```
스크래핑 
filter {date_range}
transaction[] {
	time date
	amount number
	context string
	method string
	category string
}
  
```

대출
--

**level 1**

잔여 대출의 총합 확인

```
스크래핑 
summary {
	account_count number
	redemption_ratio real (제공여부 ?)
	total_amount number (제공여부 ?)
}
```


**level 2**

금융기관 별 대출 현황 조회

```
스크래핑 
account[] {
	bank_name string
	account_name string
	redemption_ratio real (제공여부 ?)
	amount number (제공여부 ?)
	interest_rate real (제공하지 않음)
}
```

**level 3**

개별 대출별 잔여 금액과 이자비용

```
스크래핑 
account {
	bank_name string
	account_name string
	account_type string (제공하지 않음)
	redemption_ratio real (제공여부 ?)
	amount number (제공여부 ?)
	interest_rate real (제공하지 않음)
	expiration_date date 
	next_interest_payment_date date (제공하지 않음)
	next_interest_amount number (제공하지 않음)
}
```

챌린지
--
 - 월간 예산을 설정해 현명한 소비를 하고자한다
 - 적금 등의 재무목표를 세우고 달성현황을 확인

```
수동기입
budget {
	total_amount number
	total_spend number
	category[] {
		type string
		name string
		amount number
		spend number
	}
}

수동기입
challenge[] {
	title string
	type string
	start_date date
	end_date date
	goal number
	monthly number
	current number
	account account
}
```

빌리마인더
--
청구서를 통합관리하고 알림기능을 이용한다
개별 청구서의 정보를 확인할 수 있다.

 - 자동 등록 : 일자가 정해진 자동이체로 지불되는 정기 청구금액(통신요금, 카드대금, 정기구독 요금 등)
 - 수동 기입 : 
월간 납부임에도 사용자가 직접 이체하여 특정 일자에 자동이체가 되지 않는 정기 청구금액

```
스크래핑,수동기입
bill[] {
	title string
	category string
	type string
	payment_amount number
	monthly_payment_amount number
	monthly_payment_date date
}

```

비금융자산
--

**level 1**

비금융자산을 기록한다

```
수동기입
summary[] {
	housung_property_amount number
	car_property_amount number
}

```

**level 2**

소유한 비금융자산을 기록해놓는다. 

```
수동기입
housing[] {
	name string
	type string
	address string
	price number
}

수동기입
car[] {
	manufacturer string
	model string
	year date
	price number
}

```

