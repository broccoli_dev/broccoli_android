package com.nomadconnection.broccoliespider.data;

/**
 * Created by YelloHyunminJang on 16. 7. 27..
 */
public class BankCode {

    public static final String TYPE = "BK";


    /**
     * 한국은행
     */
    public static final String HK           = "0001";

    /**
     * 산업은행
     */
    public static final String KDB          = "0002";

    /**
     * 기업은행
     */
    public static final String IBK          = "0003";

    /**
     * 국민
     */
    public static final String KB           = "0004";

    /**
     * 수협
     */
    public static final String SUHYUP       = "0007";

    /**
     * 농협
     */
    public static final String NH           = "0011";

    /**
     * 우리
     */
    public static final String WOORI        = "0020";

    /**
     * SC제일은행
     */
    public static final String SC           = "0023";

    /**
     * 시티
     */
    public static final String CITY         = "0027";

    /**
     * 대구
     */
    public static final String DAEGU        = "0031";

    /**
     * 부산
     */
    public static final String BUSAN        = "0032";

    /**
     * 광주
     */
    public static final String KWANGJU      = "0034";

    /**
     * 제주
     */
    public static final String JEJU         = "0035";

    /**
     * 전북
     */
    public static final String JEONBUK      = "0037";

    /**
     * 경남은행
     */
    public static final String KYUNGNAM     = "0039";

    /**
     * 새마을
     */
    public static final String SAEMAUL      = "0045";

    /**
     * 신협
     */
    public static final String SINHYUP      = "0048";

    /**
     * 상호저축
     */
    public static final String SBC          = "0050";

    /**
     * HSBC
     */
    public static final String HSBC         = "0054";

    /**
     * 도이치
     */
    public static final String DEUTSCH      = "0055";

    /**
     * ABN암로
     */
    public static final String ABN          = "0056";

    /**
     * JP모건
     */
    public static final String JPMOGAN      = "0057";

    /**
     * 우체국
     */
    public static final String POST         = "0071";

    /**
     * 하나
     */
    public static final String HANA         = "0081";

    /**
     * 신한
     */
    public static final String SHINHAN      = "0088";

    /**
     * 케이뱅크
     */
    public static final String KBANK        = "0089";

    /**
     * 카카오뱅크
     */
    public static final String KAKAOBANK    = "0090";
}
