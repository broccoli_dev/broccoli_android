package com.nomadconnection.broccoliespider.data;

/**
 * Created by YelloHyunminJang on 16. 2. 11..
 */
public class CommonCode {


    public static final String KEY_BANK1 = "BANK_1";
    public static final String KEY_BANK2 = "BANK_2";
    public static final String KEY_BANK3 = "BANK_3";
    public static final String KEY_ACCNAME = "ACCTNM";

    public static final String KEY_CARD1 = "CARD_1";
    public static final String KEY_CARD2 = "CARD_2";
    public static final String KEY_CARD3 = "CARD_3";
    public static final String KEY_CARD4 = "CARD_4";

    public static final String RESULT = "result";
    public static final String KEY_LIST = "lists";

    public static final String KEY_ACCOUNT = "resAccount";

    public static final String KEY_ACCOUNT_TYPE = "resAccountDeposit";
    public static final String VALUE_TYPE_DEPOSIT = "10";
    public static final String VALUE_TYPE_MMDA = "11";
    public static final String VALUE_TYPE_SAVING = "12";
    public static final String VALUE_TYPE_TRUST = "13";
    public static final String VALUE_TYPE_ETC = "14";
    public static final String VALUE_TYPE_LOAN = "40";

    public static final String ERROR_NOT_REG = "인증서";

    public static final String ERROR_IGNORE_1 = "조회가능한 예금종류가 아닙니다.";
    public static final String ERROR_IGNORE_2 = "해당 서비스가 불가한 계좌이거나 잘못된 계좌번호이오니";
    public static final String ERROR_IGNORE_3 = "결제예정금액은 해당 은행으로 문의하여";
    public static final String ERROR_IGNORE_4 = "메뉴선택이 잘못 되었습니다. 확인 후 이용해 주십시오.";
    public static final String ERROR_IGNORE_5 = "다음달 결제하실 내역이 존재하지 않습니다.";
    public static final String ERROR_IGNORE_6 = "청구예정금액 조회 중 시스템 장애가 발생하였습니다.";
    public static final String ERROR_IGNORE_7 = "청구내역이 없습니다";
}
