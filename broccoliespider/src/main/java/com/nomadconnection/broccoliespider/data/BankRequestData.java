package com.nomadconnection.broccoliespider.data;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by YelloHyunminJang on 16. 1. 8..
 */
public class BankRequestData extends BaseRequestData implements Cloneable, Serializable {

    private String org;
    private String certKey;
    private String module;
    private String fCode;
    private String loginMethod;
    private String cusKind;
    private String certName;
    private String certPwd;
    private String loginId;
    private String loginPwd;
    private String number;
    private String password;
    private String currcd;
    private String startDate;
    private String endDate;
    private String regNumber;
    private String cvc;
    private String validDate;
    private String appHistory;
    private String billDate;
    private String originalBankCode;
    private String originalBankType;
    private String originalBankName;

    private boolean isComplete = true;
    private boolean isDaily = true;

    public BankRequestData(Builder builder) throws IllegalStateException {
        if (builder != null) {
            this.org = builder.org;
            this.certKey = builder.certKey;
            this.module = builder.module;
            this.fCode = builder.fCode;
            this.loginMethod = builder.loginMethod;
            this.cusKind = builder.cusKind;
            this.certName = builder.certName;
            this.certPwd = builder.certPwd;
            this.loginId = builder.loginId;
            this.loginPwd = builder.loginPwd;
            this.number = builder.number;
            this.password = builder.password;
            this.currcd = builder.currcd;
            this.startDate = builder.startDate;
            this.endDate = builder.endDate;
            this.regNumber = builder.regNumber;
            this.cvc = builder.cvc;
            this.validDate = builder.validDate;
            this.appHistory = builder.appHistory;
            this.billDate = builder.billDate;
        }

        if (this.module == null || this.loginMethod == null
                || this.startDate == null || this.endDate == null) {
            isComplete = false;

        }
        if (!isComplete) {
            throw new IllegalStateException("Lack of essential information");
        }
        if (this.loginMethod != null) {
            if (this.loginMethod.equals(VALUE_METHOD_AUTH)) {
                if (this.certName == null || this.certPwd == null) {
                    isComplete = false;
                }
            } else if (this.loginMethod.equals(VALUE_METHOD_ID)) {
                if (this.loginId == null || this.loginPwd == null) {
                    isComplete = false;
                }
            }
        }
        if (!isComplete) {
            throw new IllegalStateException("Lack of personal information");
        }
    }

    public JSONObject getJSONObject() throws IllegalStateException, JSONException {
        if (!isComplete) {
            throw new IllegalStateException("Lack of essential information");
        }
        JSONObject object = new JSONObject();
        try {
            object.put(MODULE, this.module);
            object.put(FCODE, this.fCode);
            object.put(LOGIN, this.loginMethod);
            if (this.loginMethod.equals(VALUE_METHOD_AUTH)) {
                object.put(CERTNAME, this.certName);
                object.put(CERTPWD, this.certPwd);
            } else if (this.loginMethod.equals(VALUE_METHOD_ID)) {
                object.put(LOGINID, this.loginId);
                object.put(LOGINPWD, this.loginPwd);
            }
            object.put(STARTDATE, this.startDate);
            object.put(ENDDATE, this.endDate);
            if (this.org != null) {
                object.put(ORG, this.org);
            }
            if (this.certKey != null) {
                object.put(CERTKEY, this.certKey);
            }
            if (this.cusKind != null) {
                object.put(CUS_KIND, this.cusKind);
            }
            if (this.number != null) {
                object.put(NUMBER, this.number);
            }
            if (this.password != null) {
                object.put(PASSWORD, this.password);
            }
            if (this.currcd != null) {
                object.put(CURRCD, this.currcd);
            }
            if (this.regNumber != null) {
                object.put(REGNUMBER, this.regNumber);
            }
            if (this.cvc != null) {
                object.put(CVC, this.cvc);
            }
            if (this.validDate != null) {
                object.put(VALIDDATE, this.validDate);
            }
            if (this.billDate != null) {
                object.put(BILLDATE, this.billDate);
            }
            if (this.appHistory != null) {
                object.put(APPHISTORYVIEW, this.appHistory);
            }
        } catch (JSONException e) {
            throw e;
        }
        return object;
    }

    public String getCertName() {
        return certName;
    }

    public String getPassword() {
        return certPwd;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public static class Builder {
        private String org;
        private String certKey;
        private String module;
        private String fCode;
        private String loginMethod;
        private String cusKind;
        private String certName;
        private String certPwd;
        private String loginId;
        private String loginPwd;
        private String number;
        private String password;
        private String currcd;
        private String startDate;
        private String endDate;
        private String regNumber;
        private String cvc;
        private String validDate;
        private String appHistory;
        private String billDate;

        public Builder() {

        }

        public Builder(String module, String fCode, String certName, String certPwd, String startDate, String endDate) {
            setModule(module);
            setFCode(fCode);
            setAuth(certName, certPwd);
            setStartDate(startDate);
            setEndDate(endDate);
        }

        public Builder(String module, String fCode, String id, String pwd, String startDate, String endDate, String nothing) {
            setModule(module);
            setFCode(fCode);
            setIDAndPW(id, pwd);
            setStartDate(startDate);
            setEndDate(endDate);
        }

        /**
         * <p><b>Require</b></p>
         * 서비스구분코드
         * <p>1:전계좌조회</p>
         * <p>2:잔액조회</p>
         * <p>3:거래내역조회</p>
         * <p>1,3:전계좌+거래내역조회</p>
         *
         * @param module
         */
        public Builder setModule(String module) {
            this.module = module;
            return this;
        }

        /**
         * <p><b>Require</b></p>
         * 기관코드
         * <p>using AiBUtils.getFSByCode(banknumbercode.trim())</p>
         * <p>ex)setFcode(AibUtils.getFSByCode(BankCode.KB)</p>
         *
         * @param code
         * @return
         */
        public Builder setFCode(String code) {
            this.fCode = code;
            return this;
        }

        /**
         * 로그인방법
         * setIDAndPw나 setAuth 시에 자동으로 로그인방법 설정함.
         * <p>0:인증서 - VALUE_METHOD_AUTH</p>
         * <p>1:아이디 - VALUE_METHOD_ID</p>
         *
         * @param method
         * @return
         */
        public Builder setLoginMethod(String method) {
            this.loginMethod = method;
            return this;
        }

        /**
         * <p><b>Require</b></p>
         * 아이디로 로그인
         *
         * @param id
         * @param pw
         * @return
         */
        public Builder setIDAndPW(String id, String pw) {
            this.loginId = id;
            this.loginPwd = pw;
            setLoginMethod(VALUE_METHOD_ID);
            return this;
        }

        /**
         * <p><b>Require</b></p>
         * 인증서로 로그인
         *
         * @param name
         * @param pwd
         * @return
         */
        public Builder setAuth(String name, String pwd) {
            this.certName = name;
            this.certPwd = pwd;
            setLoginMethod(VALUE_METHOD_AUTH);
            return this;
        }

        /**
         * <p><b>Require</b></p>
         * 조회시작날짜
         * form yyyyMMdd
         *
         * @param date
         * @return
         */
        public Builder setStartDate(String date) {
            this.startDate = date;
            return this;
        }

        /**
         * <p><b>Require</b></p>
         * 조회종료날짜
         * form yyyyMMdd
         *
         * @param date
         * @return
         */
        public Builder setEndDate(String date) {
            this.endDate = date;
            return this;
        }

        /**
         * <p><b>Optional</b></p>
         *
         * @param key
         * @return
         */
        public Builder setCertKey(String key) {
            this.certKey = key;
            return this;
        }

        /**
         * 서비스분류코드
         *
         * @param org
         * @return
         */
        public Builder setOrg(String org) {
            this.org = org;
            return this;
        }

        /**
         * 고객구분 - 현재 라이브러리에서 개인 외에는 서비스하고 있지 않음
         * <p>0:개인</p>
         * <p>1:기업</p>
         * <p>2:프리미엄</p>
         *
         * @param customer
         * @return
         */
        public Builder setCustomerKind(String customer) {
            this.cusKind = customer;
            return this;
        }

        /**
         * 계좌번호
         *
         * @param number
         * @return
         */
        public Builder setNumber(String number) {
            this.number = number;
            return this;
        }

        /**
         * 계좌비밀번호
         *
         * @param pwd
         * @return
         */
        public Builder setPassword(String pwd) {
            this.password = pwd;
            return this;
        }

        /**
         * 통화코드
         *
         * @param cd
         * @return
         */
        public Builder setCurrentCD(String cd) {
            this.currcd = cd;
            return this;
        }

        /**
         * 주민/사업자번호
         *
         * @param number
         * @return
         */
        public Builder setRegNumber(String number) {
            this.regNumber = number;
            return this;
        }

        /**
         * 카드 CVC
         *
         * @param cvc
         * @return
         */
        public Builder setCVC(String cvc) {
            this.cvc = cvc;
            return this;
        }

        /**
         * 카드유효기간
         * form yyyyMM
         *
         * @param date
         * @return
         */
        public Builder setValidDate(String date) {
            this.validDate = date;
            return this;
        }

        public Builder setBillDate(String billData) {
            this.billDate = billData;
            return this;
        }

        public Builder setAppHistoryView(String history) {
            this.appHistory = history;
            return this;
        }

        public BankRequestData build() {
            return new BankRequestData(this);
        }
    }


    public boolean isDaily() {
        return isDaily;
    }

    public void setIsDaily(boolean isDaily) {
        this.isDaily = isDaily;
    }

    public String getModule() {
        return module;
    }

    public String getFCode() {
        return fCode;
    }

    public String getOriginalBankCode() {
        return originalBankCode;
    }

    public void setOriginalBankCode(String originalInputCode) {
        this.originalBankCode = originalInputCode;
    }

    public String getOriginalBankType() {
        return originalBankType;
    }

    public void setOriginalBankType(String type) {
        originalBankType = type;
    }

    public String getOriginalBankName() {
        return originalBankName;
    }

    public void setOriginalBankName(String originalBankName) {
        this.originalBankName = originalBankName;
    }

    public BankRequestData getClone() {
        BankRequestData data = null;
        try {
             data = (BankRequestData) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return data;
    }

    public void passwordClear() {
        certPwd = "personalData";
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getBillDate() {
        return billDate;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public String getLoginMethod() {
        return loginMethod;
    }
}
