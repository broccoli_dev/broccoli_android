package com.nomadconnection.broccoliespider.data;

/**
 * Created by YelloHyunminJang on 16. 7. 27..
 */
public class CardCode {

    public static final String TYPE = "CD";

    /**
     * 국민카드
     */
    public static final String KB       = "0301";

    /**
     * 현대카드
     */
    public static final String HYUNDAE  = "0302";

    /**
     * 삼성카드
     */
    public static final String SAMSUNG  = "0303";

    /**
     * 농협
     */
    public static final String NH       = "0304";

    /**
     * BC카드
     */
    public static final String BC       = "0305";

    /**
     * 신한카드
     */
    public static final String SHINHAN  = "0306";

    /**
     * 씨티카드
     */
    public static final String CITY     = "0307";

    /**
     * 우리카드
     */
    public static final String WOORI    = "0309";

    /**
     * 롯데카드
     */
    public static final String LOTTE    = "0311";

    /**
     * 하나카드
     */
    public static final String HANA     = "0313";
}
