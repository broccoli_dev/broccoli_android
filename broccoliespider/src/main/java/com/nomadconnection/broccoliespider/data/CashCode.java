package com.nomadconnection.broccoliespider.data;

/**
 * Created by YelloHyunminJang on 16. 7. 27..
 */
public class CashCode {

    public static final String TYPE = "NT";

    /**
     * 현금영수증
     */
    public static final String CODE       = "0003";

}
