package com.nomadconnection.broccoliespider.parameter;

/**
 * Created by YelloHyunminJang on 16. 7. 27..
 */
public class LoginParameter {

    public static final String CERT_DER_FILEPATH = "reqCertFile";
    public static final String CERT_KEY_FILEPATH = "reqKeyFile";
    public static final String CERT_PASSWORD = "reqCertPass";

    public static final String LOGIN_ID = "reqUserId";
    public static final String LOGIN_PASS = "reqUserPass";
}
