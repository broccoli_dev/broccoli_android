package com.nomadconnection.broccoliespider.parameter;

/**
 * Created by YelloHyunminJang on 16. 7. 27..
 */
public class CardApprovalParameter {

    /**
     * 시작일자
     */
    public static final String KEY_START_DATE = "commStartDate";

    /**
     * 종료일자
     */
    public static final String KEY_END_DATE = "commEndDate";

    /**
     * 일자 정렬 순서
     */
    public static final String KEY_TIME_SORT = "reqSEQOrder";

    /**
     * 일자 최신순
     */
    public static final String VALUE_TIME_SORT_NEWEST = "0";

    /**
     * 일자 과거순
     */
    public static final String VALUE_TIME_SORT_OLDEST = "1";

    /**
     * 조회구분
     */
    public static final String KEY_SEARCH_SECTION = "reqSearchGbn";

    /**
     * 카드별 조회
     */
    public static final String VALUE_SEARCH_SECTION_INDIVIDUAL = "0";

    /**
     * 전체 조회
     */
    public static final String VALUE_SEARCH_SECTION_TOTAL = "1";

    /**
     * 카드번호
     * <p>카드별 조회 시에는 필수 옵션<p/>
     */
    public static final String KEY_CARD_NUMBER = "reqCardNo";

    /**
     * 가맹점정보 포함 여부
     */
    public static final String KEY_MEMBERSHIP_STORE = "reqMemberStoreInfoYN";

    /**
     * 가맹점정보 미포함
     */
    public static final String VALUE_MEMBERSHIP_STORE_EXCLUDE = "0";

    /**
     * 가맹점정보 포함
     */
    public static final String VALUE_MEMBERSHIP_STORE_INCLUDE = "1";

    /**
     * 카드 보유카드 조회 코드
     */
    public static final String VALUE_MODULE_CARD_LIST = "010040";

    /**
     * 카드 승인내역 조회 코드
     */
    public static final String VALUE_MODULE_CARD_APPROVAL = "010010";

    /**
     * 카드 명세서 조회 코드
     */
    public static final String VALUE_MODULE_CARD_BILL = "010020";

    /**
     * 카드 보유카드 조회 코드
     */
    public static final String VALUE_MODULE_CARD_LIMIT = "010030";

    /**
     * 카드 결제예정 조회 코드
     */
    public static final String VALUE_MODULE_CARD_DUE = "010050";

    /**
     * 카드 장기카드대출
     */
    public static final String VALUE_MODULE_CARD_LOAN = "010070";
}
