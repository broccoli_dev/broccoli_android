package com.nomadconnection.broccoliespider.parameter;

/**
 * Created by YelloHyunminJang on 16. 8. 8..
 */
public class CashParameter {

    /**
     * 시작일자
     */
    public static final String KEY_START_DATE = "commStartDate";

    /**
     * 종료일자
     */
    public static final String KEY_END_DATE = "commEndDate";

    /**
     * 일자 정렬 순서
     */
    public static final String KEY_TIME_SORT = "reqSEQOrder";

    /**
     * 일자 최신순
     */
    public static final String VALUE_TIME_SORT_NEWEST = "0";

    /**
     * 일자 과거순
     */
    public static final String VALUE_TIME_SORT_OLDEST = "1";

    /**
     * 조회구분
     */
    public static final String KEY_SEARCH_SECTION = "reqSearchGbn";

    /**
     * 전체
     */
    public static final String VALUE_SEARCH_SECTION_TOTAL = "";

    /**
     * 전문직 등 사업자와 거래
     */
    public static final String VALUE_SEARCH_SECTION_BUSINESS = "0";

    /**
     * 사업자 지출증빙용
     */
    public static final String VALUE_SEARCH_SECTION_VOUCHERS = "1";

    /**
     * 발행구분
     */
    public static final String KEY_ISSUE_TYPE = "reqIssueType";

    /**
     * 전체
     */
    public static final String VALUE_ISSUE_TYPE_TOTAL = "";

    /**
     * 일반
     */
    public static final String VALUE_ISSUE_TYPE_NORMAL = "1";

    /**
     * 전통시장
     */
    public static final String VALUE_ISSUE_TYPE_TRADITION_MARKET = "2";

    /**
     * 교통
     */
    public static final String VALUE_ISSUE_TYPE_PUBLIC_TRANSPORTER = "3";

    /**
     * 현금영수증 개인서비스 현금영수증
     */
    public static final String VALUE_MODULE_CASH = "000010";

    public static final String VALUE_MODULE_ID_LOGIN_CASH = "001010";

}
