package com.nomadconnection.broccoliespider.parameter;

/**
 * Created by YelloHyunminJang on 16. 7. 27..
 */
public class BankTransactionParameter {

    /**
     * 계좌번호
     */
    public static final String KEY_ACCOUNT = "reqAccount";

    /**
     * 시작일자
     */
    public static final String KEY_START_DATE = "commStartDate";

    /**
     * 종료일자
     */
    public static final String KEY_END_DATE = "commEndDate";

    /**
     * 일자 정렬 순서
     */
    public static final String KEY_TIME_SORT = "reqSEQOrder";

    /**
     * 일자 최신순
     */
    public static final String VALUE_TIME_SORT_NEWEST = "0";

    /**
     * 일자 과거순
     */
    public static final String VALUE_TIME_SORT_OLDEST = "1";

    /**
     * 은행 전계좌 조회 코드
     */
    public static final String VALUE_MODULE_BANK_LIST = "010001";

    /**
     * 은행 거래내역 조회 코드
     */
    public static final String VALUE_MODULE_BANK_TRANSACTION = "010003";

    /**
     * 은행 개인 적금 거래내역 조회 코드
     */
    public static final String VALUE_MODULE_BANK_SAVING_TRANSACTION = "010013";

    /**
     * 은행 개인 대출 거래내역 조회 코드
     */
    public static final String VALUE_MODULE_BANK_LOAN_TRANSACTION = "010043";
}
