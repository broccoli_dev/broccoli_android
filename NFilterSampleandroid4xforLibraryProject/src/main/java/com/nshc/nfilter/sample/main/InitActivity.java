package com.nshc.nfilter.sample.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.nshc.nfilter.sample.activity.app.NativeCodeWayActivityTest;
import com.nshc.nfilter.sample.activity.app.WebWayActivityTest;
import com.nshc.nfilter.sample.view.app.NativeCodeWayViewTest;
import com.nshc.nfilter.sample.view.app.WebWayViewTest;

public class InitActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfilter_sample_main);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnViewWebview:
				startActivity( new Intent(this, WebWayViewTest.class) );
				break;
			case R.id.btnViewNative:
				startActivity( new Intent(this, NativeCodeWayViewTest.class) );
				break;	
			case R.id.btnActivityWebview:
				startActivity( new Intent(this, WebWayActivityTest.class) );
				break;						
			case R.id.btnActivityNative:
				startActivity( new Intent(this, NativeCodeWayActivityTest.class) );
				break;		
		}
	}
	
}
