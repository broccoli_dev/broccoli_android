package com.nshc.nfilter.sample.main;

import android.app.ProgressDialog;
import android.content.Context;

public class NFilterProgressDialog extends ProgressDialog {

	private static NFilterProgressDialog instance;
	private static Context context;
	
	private NFilterProgressDialog(Context context) {
		super(context);
	}
	
	public static synchronized NFilterProgressDialog getInstance(Context con){
		if(instance == null){
			instance = new NFilterProgressDialog(con);
			context = con;
		}
		
		return instance;
	}
	
	public void show(String title, String message){
		show(context, title, message);
	}
	
	public void dismiss(){
		dismiss();
	}
}
