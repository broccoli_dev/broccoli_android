package com.nshc.nfilter.sample.activity.app;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nshc.nfilter.NFilter;
import com.nshc.nfilter.sample.main.R;
import com.nshc.nfilter.util.NFilterLOG;
import com.nshc.nfilter.util.NFilterUtils;

public class WebWayActivityTest extends Activity{

	String publickey;
	NFilter nfilter = null;
	private WebView webview;
	HttpClient httpclient =  new DefaultHttpClient();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		setContentView(R.layout.nfilter_sample_webview_view);

		//nFilter 초기화
		 NFilterLOG.debug = true;
		nfilter = new NFilter(this);
		nfilter.setXmlURL("http://demo.nshc.net:8088/nfilter/static/keys/npublickey.xml");
		nfilter.setNoPadding(true);
		nfilter.setPlainDataEnable(true);
		initWebView( "http://demo.nshc.net:8088/nfilter/static/view.html" );

	}

	public void initWebView(String url){
		
		webview = (WebView) findViewById(R.id.webview);
		webview.setFocusable(true);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.loadUrl(url);
		webview.setWebViewClient(new WebViewClient(){
			/**
			 * 키패드 사용을 위해  shouldOverrideUrlLoading를 Override함
			 */
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url){
				/*
				 * Parameter
				 *  - WebView view 
				 *  - String url
				 *  - int requestCode: onActivityResult에서 리턴값으로 받을 requestCode
				 */
				if( nfilter.setOnNFilter(view, url, 1) )
					return true;

				return super.shouldOverrideUrlLoading(view, url); // NFilter가 올라오도록 셋팅함..
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
			}
		});
	}	

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		nfilter.configurationChanged();
		super.onConfigurationChanged(newConfig);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(resultCode == RESULT_OK)
		{
			String encdata = data.getStringExtra("encdata");		// 암호화된 스트링 (Base64로 인코딩되어있음)
			int plndatalength = data.getIntExtra("plaintxtlength", 0); // 입력값의 길이
			String dummydata =data.getStringExtra("dummydata");
			String fieldName = data.getStringExtra("name");

			String plaindata = data.getStringExtra("plaintxt");
			byte[] plainDataByte = NFilterUtils.getInstance().decrypt( plaindata );

			webview.loadUrl("javascript:(function() { document.getElementsByName('"
					+ fieldName + "')[0].value = '"
					+ encdata
					+ "'; document.getElementsByName('"
					+ fieldName
					+ "_p')[0].value = '" + new String( plainDataByte ) + "'; })()");

			// 평문데이터 초기화
			for (int i = 0; i < plainDataByte.length; i++) {
				plainDataByte[ i ] = 0;
			}
		}
	}

	public void onResume() {
		super.onResume();
	}	

	public void onPause() {
		super.onPause();
	}

	protected void onDestroy() {
		webview.clearCache(true);
		super.onDestroy();
	}	

	public String setSyncCookie( String publicUrl ){
		try {

			HttpPost post = new HttpPost( publicUrl );
			HttpResponse response = null;
			String endResult = null;

			response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			InputStream instream = entity.getContent();

			endResult = convertStreamToString(instream);

			return endResult;
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "";
	}	
	private static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	private Bitmap getImageFromURL(String strImageURL) 
	{
		Bitmap imgBitmap = null;
		
		try {
			URL url = new URL(strImageURL);
			URLConnection conn = url.openConnection();
			conn.connect();
			int nSize = conn.getContentLength();
			BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), nSize);
			imgBitmap = BitmapFactory.decodeStream(bis);

			bis.close();
		} catch(Exception e){
			e.printStackTrace();
		}

		return imgBitmap;
	}

	class publicKeyAsyncTask extends AsyncTask<String, Void, String>{

		@Override
		protected void onPostExecute(String result) {
			nfilter.setPublicKey(result);
			super.onPostExecute(result);
		}

		@Override
		protected String doInBackground(String... params) {
			String publicKey = setSyncCookie(params[0]);
			return publicKey;
		}
	}
}