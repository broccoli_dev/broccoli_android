package com.nshc.nfilter.sample.activity.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nshc.nfilter.NFilter;
import com.nshc.nfilter.sample.main.R;
import com.nshc.nfilter.util.NFilterLOG;
import com.nshc.nfilter.util.NFilterUtils;

public class NativeCodeWayActivityTest extends Activity{
	NFilter nfilter = null;
	EditText et1,et2, et3 = null;
	Button btn1, btn2, btn3 = null;
	TextView tv = null;
	ProgressDialog dialog = null;
	HttpClient httpclient = getThreadSafeClient();

	String encdata_num = null;
	int plndatalength_num = 0;
	String dummydata_num = null;
	String plainNormalData_num = null;
	String plaindata_num = null;
	byte[] plainDataByte_num = null;

	String encdata_char = null;
	int plndatalength_char = 0;
	String dummydata_char = null;
	String plainNormalData_char = null;
	String plaindata_char = null;
	byte[] plainDataByte_char = null;

	public static final int DIALOG_CHAR = 33;
	public static final int DIALOG_NUM = 43;
	public static final int DIALOG_SERVER = 53;

	private Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch(msg.what){
			case DIALOG_CHAR :
				showDialog(DIALOG_CHAR);
				//handler.sendEmptyMessageDelayed(DIALOG_CHAR, 1000);
				break;
			case DIALOG_NUM :
				showDialog(DIALOG_NUM);
				//handler.sendEmptyMessageDelayed(DIALOG_NUM, 1000);
				break;
			case DIALOG_SERVER :
				showDialog(DIALOG_SERVER);
				//handler.sendEmptyMessageDelayed(DIALOG_SERVER, 1000);
				break;
			default :
				break;
			}
			super.handleMessage(msg);
		}

	};

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfilter_sample_native_view);
		
		nfilter = new NFilter(this);
		NFilterLOG.debug = true;
		nfilter.setXmlURL("http://demo.nshc.net:8088/nfilter/static/keys/npublickey.xml");
		nfilter.setPlainDataEnable(true);
		nfilter.setNoPadding(true);
		nfilter.registerReceiver();
		et1 = (EditText) findViewById(R.id.charEditText);
		et1.setInputType(0);
		et1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				nfilter.setDesc("Character KeyPad");
				nfilter.onNFilter(v, NFilter.KEYPADCHAR, 1);
			}
		}); 

		et2 = (EditText) findViewById(R.id.numEditText);
		et2.setInputType(0);
		et2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				nfilter.setDesc("Number KeyPad");
				nfilter.onNFilter(v, NFilter.KEYPADNUM, 2);
			}
		});    

		et3 = (EditText) findViewById(R.id.serialNumEditText);
		et3.setInputType(0);
		et3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				nfilter.setDesc("Serial Number KeyPad");
				nfilter.onNFilter(v, NFilter.KEYPADSERIALNUM, 3);
			}
		}); 
		
		btn1 = (Button)findViewById(R.id.num_conf);
		btn1.setOnClickListener(onClickListener);
		btn2 = (Button)findViewById(R.id.char_conf);
		btn2.setOnClickListener(onClickListener);
		btn3 = (Button)findViewById(R.id.server_conf);
		btn3.setOnClickListener(onClickListener);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = getLayoutInflater();
		View view = inflater.inflate(R.layout.custom_dialog, null);
		builder.setView(view);
		switch(id){
		case DIALOG_CHAR :
			builder.setTitle("문자 입력값 정보");
			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					removeDialog(DIALOG_CHAR);
					handler.removeMessages(DIALOG_CHAR);
				}
			});

			handler.sendEmptyMessageDelayed(DIALOG_CHAR, 1000);
			return builder.create();

		case DIALOG_NUM :
			builder.setTitle("숫자 입력값 정보");
			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					removeDialog(DIALOG_NUM);
					handler.removeMessages(DIALOG_NUM);
				}
			});

			handler.sendEmptyMessageDelayed(DIALOG_NUM, 1000);
			return builder.create();

		case DIALOG_SERVER :
			builder.setTitle("서버 복호화 정보");
			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					removeDialog(DIALOG_SERVER);
					handler.removeMessages(DIALOG_SERVER);
				}
			});

			handler.sendEmptyMessageDelayed(DIALOG_SERVER, 1000);
			return builder.create();

		default:
			break;
		}
		return super.onCreateDialog(id);
	}
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		// TODO Auto-generated method stub
		tv = (TextView)dialog.findViewById(R.id.message);
		switch(id){
		case DIALOG_CHAR :
			if(plndatalength_char != 0){
				tv.setText("\r\n" +
						"문자 암호화 데이터 :: " + encdata_char + "\r\n\r\n" +
						"문자 평문 데이터 :: " + plaindata_char + "\r\n\r\n" +
						"문자 사용자 입력 데이터 :: " + new String(plainDataByte_char) + "\r\n" );
			}else{
				tv.setText("null");
			}
			break;
		case DIALOG_NUM :
			if(plndatalength_num != 0){
				tv.setText("\r\n" + 
						"숫자 암호화 데이터 :: " + encdata_num + "\r\n\r\n" +
						"숫자 평문 데이터 :: " + plaindata_num + "\r\n\r\n" +
						"숫자 사용자 입력 데이터 :: " + new String(plainDataByte_num) + "\r\n" );	
			}else{
				tv.setText("null");
			}

			break;
		case DIALOG_SERVER :
			if(plndatalength_num != 0 && plndatalength_char != 0) {
				new DecryptAsyncTask().execute("http://demo.nshc.net:8088/nfilter/static/nfilter_prop.jsp");	
			} else {
				tv.setText("문자/숫자 데이터를 입력해 주세요");
			}

			break;
		default:
			break;
		}
		super.onPrepareDialog(id, dialog);
	}

	public String getDecryptData(String URL){
		try {
			HttpPost post = new HttpPost( URL );
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			//			params.add(new BasicNameValuePair("mode", "n"));
			//			params.add(new BasicNameValuePair("encData", encdata_num));
			//			params.add(new BasicNameValuePair("mode", "a"));
			//			params.add(new BasicNameValuePair("encData", encdata_char));
			params.add(new BasicNameValuePair("encdata1", encdata_num));
			params.add(new BasicNameValuePair("encdata2", encdata_char));
			//			Log.e("getDecryptData", "parameters : "+Arrays.toString(params.toArray()));
			post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			HttpResponse response = null;
			String endResult = null;
			response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			InputStream instream = entity.getContent();

			endResult = convertStreamToString(instream);
			return endResult;
		} catch (HttpResponseException e) {
			e.printStackTrace();
			return "HttpResponseException";
		} catch (IOException e) {
			e.printStackTrace();
			return "IOException";
		} catch (Exception ex) {
			ex.printStackTrace();
			return "Exception";
		}
	}

	public DefaultHttpClient getThreadSafeClient()  {

		DefaultHttpClient client = new DefaultHttpClient();
		ClientConnectionManager mgr = client.getConnectionManager();
		HttpParams params = client.getParams();
		client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, mgr.getSchemeRegistry()), params);
		return client;
	}

	OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch(v.getId()){
			case R.id.num_conf :
				showDialog(DIALOG_NUM);
				break;

			case R.id.char_conf :
				showDialog(DIALOG_CHAR);
				break;

			case R.id.server_conf :
				if(plndatalength_char != 0 && plndatalength_num != 0)dialog = ProgressDialog.show(NativeCodeWayActivityTest.this, "로딩 중...", "잠시만 기다려주세요", false);
				showDialog(DIALOG_SERVER);
				break;

			default :
				break;
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(resultCode == RESULT_OK)
		{
			if( requestCode == 1 ){

				encdata_char = data.getStringExtra("encdata");		// 암호화된 스트링 (Base64로 인코딩되어있음)
				plndatalength_char = data.getIntExtra("plaintxtlength", 0); // 입력값의 길이
				dummydata_char =data.getStringExtra("dummydata");

				plainNormalData_char = data.getStringExtra("plainNormalTxt");
				//가상키보드 암호화 데이터
				Log.e("onActivityResult", "AES Enc Data : " + data.getStringExtra("aesencdata"));
				plaindata_char = data.getStringExtra("plaintxt");
				plainDataByte_char = NFilterUtils.getInstance().decrypt(plaindata_char);
				et1.setText(dummydata_char);
				
				String clientEncData = data.getStringExtra("clientencdata");
				
				Log.e("nFilter", "nfilter client encrypt data : " + clientEncData);
				byte[] ss = NFilterUtils.getInstance().nSaferDecrypt(clientEncData);
				Log.e("nFilter", "nfilter client encrypt data : " + new String(ss));
				
			}else if( requestCode == 2 ){

				encdata_num = data.getStringExtra("encdata");		// 암호화된 스트링 (Base64로 인코딩되어있음)
				plndatalength_num = data.getIntExtra("plaintxtlength", 0); // 입력값의 길이
				dummydata_num =data.getStringExtra("dummydata");

				plainNormalData_num = data.getStringExtra("plainNormalTxt");
				//가상키보드 암호화 데이터
				plaindata_num = data.getStringExtra("plaintxt");
				plainDataByte_num = NFilterUtils.getInstance().decrypt(plaindata_num);
				Log.e("onActivityResult", "AES Enc Data : " + data.getStringExtra("aesencdata"));
				et2.setText(dummydata_num);
				
				String clientEncData = data.getStringExtra("clientencdata");
				
				Log.e("nFilter", "nfilter client encrypt data : " + clientEncData);
				byte[] ss = NFilterUtils.getInstance().nSaferDecrypt(clientEncData);
				Log.e("nFilter", "nfilter client encrypt data : " + new String(ss));
			}

		}

	}


	public String setSyncCookie( String publicUrl ){
		try {

			HttpPost post = new HttpPost( publicUrl );
			HttpResponse response = null;
			String endResult = null;

			response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			InputStream instream = entity.getContent();

			endResult = convertStreamToString(instream);

			return endResult;
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "";
	}	
	private static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}	

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		nfilter.configurationChanged();
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		removeDialog(DIALOG_CHAR);
		removeDialog(DIALOG_NUM);
		removeDialog(DIALOG_SERVER);
		nfilter.unregisterReceiver();
		handler.removeCallbacksAndMessages(null);
	}

	class DecryptAsyncTask extends AsyncTask<String, Void, String>{
		@Override
		protected void onPostExecute(String result) {
			Log.i("DecryptAsyncTask", "result = " + result);
			tv.setText(result);
			dialog.dismiss();
			super.onPostExecute(result);
		}

		@Override
		protected String doInBackground(String... params) {
			String temp = getDecryptData(params[0]);
			String data = temp.replaceAll("<br>", "\r\n");
			return data;
		}
	}

	class publicKeyAsyncTask extends AsyncTask<String, Void, String>{

		@Override
		protected void onPostExecute(String result) {
			nfilter.setPublicKey(result);
			super.onPostExecute(result);
		}

		@Override
		protected String doInBackground(String... params) {

			String publicKey = setSyncCookie(params[0]);
			Log.e("publicKeyAsyncTask", "Server publicKey : " + publicKey);
			return publicKey;
		}
	}
}