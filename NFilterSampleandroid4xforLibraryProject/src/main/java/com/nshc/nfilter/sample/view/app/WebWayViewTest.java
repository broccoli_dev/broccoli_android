package com.nshc.nfilter.sample.view.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.nshc.nfilter.NFilter;
import com.nshc.nfilter.command.view.NFilterOnClickListener;
import com.nshc.nfilter.command.view.NFilterTO;
import com.nshc.nfilter.sample.main.R;
import com.nshc.nfilter.util.NFilterUtils;

public class WebWayViewTest extends Activity implements NFilterOnClickListener{

	NFilter nfilter = null;
	private WebView webview;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//NFilterProgressDialog.getInstance(WebWayViewTest.this).show("로딩중...", "공개키 세팅 중...");
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		nfilter = new NFilter(this);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		setContentView(R.layout.nfilter_sample_webview_view);
		//nfilter.setXmlURL("http://demo.nshc.net:8088/nfilter/static/keys/npublickey.xml");
		String publickey = null;
		System.out.println("test1");
		try {
			publickey = new publicKeyAsyncTask().execute("https://tmpin.ok-name.co.kr:5443/sys/nsafe/npublickey.jsp").get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		System.out.println("test2");
		//NFilterProgressDialog.getInstance(WebWayViewTest.this).dismiss();
		nfilter.setPublicKey(publickey);
		nfilter.setNoPadding(true);
		nfilter.setPlainDataEnable(true);
		initWebView( "http://demo.nshc.net:8088/nfilter/static/view.html" );    	
	}

	public void initWebView(String url){
		webview = (WebView) findViewById(R.id.webview);

		webview.setFocusable(true);
		webview.getSettings().setJavaScriptEnabled(true);

		webview.loadUrl(url);

		webview.setWebViewClient(new WebViewClient(){
			/**
			 * 키패드 사용을 위해  shouldOverrideUrlLoading를 Override함
			 */
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url){
				/*
				 * Parameter
				 *  - WebView view 
				 *  - String url
				 *  - int requestCode: onActivityResult에서 리턴값으로 받을 requestCode
				 */
				if(nfilter.setOnViewNFilter(view, url, 1))
					return true;
				return super.shouldOverrideUrlLoading(view, url); // NFilter가 올라오도록 셋팅함..
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
			}
		});
	}	


	@Override
	public void onNFilterClick(NFilterTO nFilterTO) {
		Log.i("nFilter", "nFilterTO.getFieldName() >>> " + new String(nFilterTO.getFieldName()));

		if( nFilterTO.getFocus() == NFilter.NEXTFOCUS ){ //다음 이벤트 호출
			TextView tmpEditText = (TextView) findViewById(R.id.nf_char_tmp_editText);
			tmpEditText.setText( "" );
			tmpEditText = (TextView) findViewById(R.id.nf_num_tmp_editText);
			tmpEditText.setText( "" );
			Log.i("nFilter" , "[focus] NEXTFOCUS" + nFilterTO.getFocus() );
			nfilter.nFilterClose(View.GONE);

		}else if( nFilterTO.getFocus() == NFilter.PREFOCUS ){ //이전 이벤트 호출
			TextView tmpEditText = (TextView) findViewById(R.id.nf_char_tmp_editText);
			tmpEditText.setText( "" );
			tmpEditText = (TextView) findViewById(R.id.nf_num_tmp_editText);
			tmpEditText.setText( "" );
			Log.i("nFilter" , "[focus] NEXTFOCUS" + nFilterTO.getFocus() );
			nfilter.nFilterClose(View.GONE);

		}else if( nFilterTO.getFocus() == NFilter.DONEFOCUS ){ //닫기 이벤트 호출
			TextView tmpEditText = (TextView) findViewById(R.id.nf_char_tmp_editText);
			tmpEditText.setText( "" );
			tmpEditText = (TextView) findViewById(R.id.nf_num_tmp_editText);
			tmpEditText.setText( "" );
			nfilter.nFilterClose(View.GONE);

		}else{

			if( nFilterTO.getPlainData() != null ){

				webview.loadUrl("javascript:(function() { document.getElementsByName('"
						+ new String(nFilterTO.getFieldName()) + "')[0].value = '"
						+ new String(nFilterTO.getEncData())
						+ "'; document.getElementsByName('"
						+ new String(nFilterTO.getFieldName())
						+ "_p')[0].value = '" + nFilterTO.getDummyData() + "'; })()");

				//리턴 값을 해당 TextView에 넣는다.
				if( new String(  nFilterTO.getFieldName() ).equals("encdata2") ){
					// 입력필드가 가상키보드에 가려서 보이지 않을 경우 
					// 임시로 값을 보여주는 editText
					// nfilter_char_key_view.xml 32라인에서 직접 수정 가능
					TextView tmpEditText = (TextView) findViewById(R.id.nf_char_tmp_editText);
					tmpEditText.setText( new String( nFilterTO.getDummyData() ) );				
					// ================================================================ //

				}else if( new String(  nFilterTO.getFieldName() ).equals("encdata1") ){
					// 입력필드가 가상키보드에 가려서 보이지 않을 경우 
					// 임시로 값을 보여주는 editText
					// nfilter_num_key_view.xml 32라인에서 직접 수정 가능
					TextView tmpEditText = (TextView) findViewById(R.id.nf_num_tmp_editText);
					tmpEditText.setText( new String( nFilterTO.getDummyData() ) );
					// ================================================================ //

				}

				byte[] plainDataByte = NFilterUtils.getInstance().decrypt( nFilterTO.getPlainData()  );

				// 평문데이터 초기화
				for (int i = 0; i < plainDataByte.length; i++) {
					plainDataByte[ i ] = 0;
				}

			}else{
				Log.i( "nFilter" , " nFilterTO.getPlainData()  null" );

				webview.loadUrl("javascript:(function() { document.getElementsByName('"
						+ new String(nFilterTO.getFieldName()) + "')[0].value = '"
						+ ""
						+ "'; document.getElementsByName('"
						+ new String(nFilterTO.getFieldName())
						+ "_p')[0].value = '" + "" + "'; })()");

				//리턴 값을 해당 TextView에 넣는다.
				if( new String(  nFilterTO.getFieldName() ).equals("encdata2") ){
					// 입력필드가 가상키보드에 가려서 보이지 않을 경우 
					// 임시로 값을 보여주는 editText
					// nfilter_char_key_view.xml 32라인에서 직접 수정 가능
					TextView tmpEditText = (TextView) findViewById(R.id.nf_char_tmp_editText);
					tmpEditText.setText( "" );				
					// ================================================================ //

				}else if( new String(  nFilterTO.getFieldName() ).equals("encdata1") ){
					// 입력필드가 가상키보드에 가려서 보이지 않을 경우 
					// 임시로 값을 보여주는 editText
					// nfilter_num_key_view.xml 32라인에서 직접 수정 가능
					TextView tmpEditText = (TextView) findViewById(R.id.nf_num_tmp_editText);
					tmpEditText.setText( "" );
					// ================================================================ //

				}
			}
		}
	}
	/*	
	public String maskingData(NFilterTO nFilterTO){
		String maskingData = "";
		String dot = "";
		int len = nFilterTO.getPlainLength();
		for(int i = 0; i < (len - 1); i++){
			dot += "*";
		}
		NFilterLOG.i("maskingData", "dot >>>>>>>>>>>>>>>>>>>>> " + dot);
		byte[] plainDataByte = NFilterUtils.getInstance().decrypt( nFilterTO.getPlainData()  );
		maskingData = dot +  new String( plainDataByte ).substring(len-1, len);
		NFilterLOG.i("maskingData", "maskingData >>>>>>>>>>>>>>>>>>>>> " + maskingData);
		return maskingData;
	}
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		nfilter.configurationChanged();
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if( keyCode == KeyEvent.KEYCODE_BACK){
			if( !nfilter.nFilterClose(View.GONE) ) return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void onResume() {
		super.onResume();
	}	

	public void onPause() {
		super.onPause();
	}

	protected void onDestroy() {
		webview.clearCache(true);
		super.onDestroy();
	}	

	HttpClient httpclient =  new DefaultHttpClient();
	public String setSyncCookie( String publicUrl ){
		try {

			HttpPost post = new HttpPost( publicUrl );
			HttpResponse response = null;
			String endResult = null;

			response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			InputStream instream = entity.getContent();

			endResult = convertStreamToString(instream);

			return endResult;
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "";
	}	
	private static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	class publicKeyAsyncTask extends AsyncTask<String, Void, String>{
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		}

		@Override
		protected String doInBackground(String... params) {
			String publicKey = setSyncCookie(params[0]);
			return publicKey;
		}
	}
}