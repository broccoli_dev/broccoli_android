package com.nomadconnection.broccoliraonsecure;

import com.lumensoft.ks.KSCertificate;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class KSW_CertItem extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	public static final String POLICY = "policy";
	public static final String SUBJECTNAME = "subjectName";
	public static final String EXPIREDTIME = "expiredTime";
	public static final String ISSUERNAME = "issuerName";
	public static final String EXPIREDIMG = "expiredImg";
	public static final String PATH = "ksw_data";
	public KSCertificate userCert;
	boolean selectable = true;
	boolean containRealName = false;

	public KSW_CertItem(KSCertificate userCert)
			throws UnsupportedEncodingException {
		this.userCert = userCert;
		this.put(PATH, userCert.getPath());
		this.put(POLICY, userCert.getPolicy());
		this.put(SUBJECTNAME, userCert.getSubjectName());
		this.put(EXPIREDTIME, "만료일 : " + userCert.getExpiredTime());
		this.put(ISSUERNAME, userCert.getIssuerName());
		int expiredT = userCert.isExpiredTime();
		this.put(EXPIREDIMG, Integer.valueOf(expiredT));
	}

	public KSW_CertItem() {

	}

	public KSCertificate getCertItem() {
		return userCert;
	}

	public boolean isExpired() {
		boolean expired = false;
		if (userCert != null) {
			expired = this.userCert.isExpired();
		}
		return expired;
	}

	public boolean isAvailable() {
		boolean ret =  true;
		if (userCert == null || userCert.isExpired() || !userCert.isKeyFileExist() || !userCert.isValidFormat()) {
			ret = false;
		}
		return ret;
	}

	public boolean isContainRealName() {
		return containRealName;
	}

	public void setContainRealName(boolean isRealName) {
		containRealName = isRealName;
	}

	public void setCertificate(KSCertificate certicate) {
		this.userCert = certicate;
	}
}
