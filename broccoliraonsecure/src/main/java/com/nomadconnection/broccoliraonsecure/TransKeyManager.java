package com.nomadconnection.broccoliraonsecure;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.softsecurity.transkey.Global;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCtrl;

/**
 * Created by YelloHyunminJang on 16. 8. 3..
 */
public class TransKeyManager {

    public static TransKeyManager mInstance;


    public static TransKeyManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new TransKeyManager(context.getApplicationContext());
        }
        return mInstance;
    }

    public TransKeyManager(Context context) {

    }

    public TransKeyCtrl initTransKeyPad(Context context, int index, int keyPadType, int textType, String label, String hint,
                                        int maxLength, String maxLengthMessage, int line3Padding, boolean bReArrange,
                                        FrameLayout keyPadView, EditText editView, HorizontalScrollView scrollView,
                                        LinearLayout inputView, ImageButton clearView, RelativeLayout ballonView, boolean bUseAtmMode,
                                        ITransKeyActionListener listener, ITransKeyActionListenerEx listenerEx) {
        TransKeyCtrl transKeyCtrl = null;
        try {
            transKeyCtrl = new TransKeyCtrl(context);
            //Activity 로 처리할 때 처럼 파라미터를 인텐트에 넣어서 처리.
            Intent newIntent = getIntentParam(context, keyPadType, textType, label, hint, maxLength, maxLengthMessage, line3Padding, 40,bUseAtmMode);
            transKeyCtrl.init(newIntent, keyPadView, editView, scrollView, inputView, clearView, ballonView);
            transKeyCtrl.setReArrangeKeapad(bReArrange);
            transKeyCtrl.setTransKeyListener(listener);
            transKeyCtrl.setTransKeyListenerEx(listenerEx);

        } catch (Exception e) {
            if(Global.debug) Log.d("STACKTRACE", e.getStackTrace().toString());
        }
        return transKeyCtrl;
    }

    public TransKeyCtrl initTransKeyPad(Context context, int index, int keyPadType, int textType, String label, String hint,
                                int maxLength, String maxLengthMessage, int line3Padding, boolean bReArrange,
                                FrameLayout keyPadView, EditText editView, HorizontalScrollView scrollView,
                                LinearLayout inputView, ImageButton clearView, RelativeLayout ballonView, ImageView lastInput, boolean bUseAtmMode,
                                ITransKeyActionListener listener, ITransKeyActionListenerEx listenerEx) {
        TransKeyCtrl transKeyCtrl = null;
        try {
            transKeyCtrl = new TransKeyCtrl(context);
            //Activity 로 처리할 때 처럼 파라미터를 인텐트에 넣어서 처리.
            Intent newIntent = getIntentParam(context, keyPadType, textType, label, hint, maxLength, maxLengthMessage, line3Padding, 40, bUseAtmMode);
            transKeyCtrl.init(newIntent, keyPadView, editView, scrollView, inputView, clearView, ballonView, lastInput);
            transKeyCtrl.setReArrangeKeapad(bReArrange);
            transKeyCtrl.setTransKeyListener(listener);
            transKeyCtrl.setTransKeyListenerEx(listenerEx);
        } catch (Exception e) {
            if(Global.debug) Log.d("STACKTRACE", e.getStackTrace().toString());
        }
        return transKeyCtrl;
    }

    public Intent getIntentParam(Context context, int keyPadType, int textType, String label, String hint,
                                 int maxLength, String maxLengthMessage, int line3Padding, int reduceRate,boolean useAtmMode) {
        Intent newIntent = new Intent(context, TransKeyActivity.class);

        //public static final int mTK_TYPE_KEYPAD_NUMBER				//숫자전용
        //public static final int mTK_TYPE_KEYPAD_QWERTY_LOWER		//소문자 쿼티
        //public static final int mTK_TYPE_KEYPAD_QWERTY_UPPER		//대문자 쿼티
        //public static final int mTK_TYPE_KEYPAD_ABCD_LOWER			//소문자 순열자판
        //public static final int mTK_TYPE_KEYPAD_ABCD_UPPER			//대문자 순열자판
        //public static final int mTK_TYPE_KEYPAD_SYMBOL				//심벌자판

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_KEYPAD_TYPE, keyPadType);

        //키보드가 입력되는 형태
        //TransKeyActivity.mTK_TYPE_TEXT_IMAGE 					- 보안 텍스트 입력
        //TransKeyActivity.mTK_TYPE_TEXT_PASSWORD 				- 패스워드 입력
        //TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_EX 			- 마지막 글자 보여주는 패스워드 입력
        //TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_LAST_IMAGE	- 마지막 글자를 제외한 나머지를 *표시.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_INPUT_TYPE, textType);

        //키패드입력화면의 입력 라벨
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_NAME_LABEL, label);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_DISABLE_SPACE, false);

        //최대 입력값 설정 1 - 16
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_INPUT_MAXLENGTH, maxLength);

        //인터페이스 - maxLength시에 메시지 박스 보여주기. 기본은 메시지 안나옴.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_MAX_LENGTH_MESSAGE, maxLengthMessage);

        //--> SERVER 연동테스트
        if(true){
            byte[] SecureKey = { 'M', 'o', 'b', 'i', 'l', 'e', 'T', 'r', 'a', 'n', 's', 'K', 'e', 'y', '1', '0' };
            newIntent.putExtra(TransKeyActivity.mTK_PARAM_CRYPT_TYPE, TransKeyActivity.mTK_TYPE_CRYPT_SERVER);
            newIntent.putExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY, SecureKey);
        }
        else{
//		newIntent.putExtra(TransKeyActivity.mTK_PARAM_CRYPT_TYPE, TransKeyActivity.mTK_TYPE_CRYPT_LOCAL);
        }
        //<-- SERVER 연동테스트

        //해당 Hint 메시지를 보여준다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SET_HINT, hint);

        //Hint 테스트 사이즈를 설정한다.(단위 dip, 0이면 디폴트 크기로 보여준다.)
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SET_HINT_TEXT_SIZE, 0);

        //커서를 보여준다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SHOW_CURSOR, true);

        //에디트 박스안의 글자 크기를 조절한다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_EDIT_CHAR_REDUCE_RATE, reduceRate);

        //심볼 변환 버튼을 비활성화 시킨다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_DISABLE_SYMBOL, false);

        //심볼 변환 버튼을 비활성화 시킬 경우 팝업 메시지를 설정한다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_DISABLE_SYMBOL_MESSAGE, "심볼키는 사용할 수 없습니다.");

        //////////////////////////////////////////////////////////////////////////////
        //인터페이스 - line3 padding 값 설정 가능 인자. 기본은 0 값이면 아무 설정 안했을 시 원래 transkey에서 제공하던 값을 제공한다..
//        newIntent.putExtra(TransKeyActivity.mTK_PARAM_KEYPAD_MARGIN, line3Padding);
//        newIntent.putExtra(TransKeyActivity.mTK_PARAM_KEYPAD_LEFT_RIGHT_MARGIN, 0);
//        newIntent.putExtra(TransKeyActivity.mTK_PARAM_KEYPAD_HIGHEST_TOP_MARGIN, 2);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_TALKBACK, true);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SUPPORT_ACCESSIBILITY_SPEAK_PASSWORD, true);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_PREVENT_CAPTURE, false);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_HIDE_TIMER_DELAY, 6);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_KEYPAD_ANIMATION, true);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_ATM_MODE, useAtmMode);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SAMEKEY_ENCRYPT_ENABLE, false);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_NUMPAD_USE_CANCEL_BUTTON, true);

//
        String originalPassword = "acc4af609fad57f1cd870a5d94092a24be5fd974";
        byte[] pbkdfKey = toByteArray(originalPassword);
        byte[] PBKDF2_SALT = {1, 6, 0, 7, 4, 4, 4, 4, 8, 8, 7, 1, 4, 3, 3, 3, 3, 3, 3, 3};
        int PBKDF2_IT = 512;

        newIntent.putExtra(TransKeyActivity. mTK_PARAM_PBKDF2_RANDKEY, pbkdfKey);
        newIntent.putExtra(TransKeyActivity. mTK_PARAM_PBKDF2_SALT, PBKDF2_SALT);
        newIntent.putExtra(TransKeyActivity. mTK_PARAM_PBKDF2_IT, PBKDF2_IT);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_MIN_LENGTH_MESSAGE, "최소 글자 8글자 미만입니다");
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_INPUT_MINLENGTH, 8);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_ALERTDIALOG_TITLE, "mTranskey alert");

        return newIntent;
    }

    public byte[] toByteArray(String hexStr) {
        int len = hexStr.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexStr.charAt(i), 16) << 4) + Character
                    .digit(hexStr.charAt(i + 1), 16));
        }
        return data;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public String toHexString(byte[] bytes) {
        char[] hexChar = new char[bytes.length *2];
        for (int i=0; i < bytes.length; i++) {
            int v = bytes[i] & 0xFF;
            hexChar[i * 2] = hexArray[v >>> 4];
            hexChar[i * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChar);
    }

}
