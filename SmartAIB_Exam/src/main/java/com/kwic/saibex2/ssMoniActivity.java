package com.kwic.saibex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;


import com.kwic.saib.pub.CertAPI;
import com.kwic.saib.pub.SmartAIB;

public class ssMoniActivity extends Activity implements SmartAIB.OnCompleteListener{
	
	ListView list;
    
	ArrayList<Account> m_acctInfo = new ArrayList<Account>();
	
    ProgressDialog dialog;
	TextView ipt_st_date, ipt_et_date;
	
    private AtomicInteger Step;
    private int totalSteps = 0;
	private int currStep = 0;
    private void requestAccount(final Account acctInfo) {
    	
    	String module= "";
    	if ("CARD".equals(acctInfo.cardBank)) {
    		module = "6,1";
    	} else {
    		module = "1,3";
    	}
		JSONObject reqObj = new JSONObject();
		try {
            reqObj.put("HANDLE", 		"HWND");		    // HWND 고정값
            reqObj.put("CERTKEY", 		"");			    // 빈문자열 / 고정값
            reqObj.put("ORG", 			"1002");  	// 1003 - 인터넷뱅킹
			reqObj.put("MODULE",	 	module);         
			reqObj.put("FCODE", 		AiBUtils.getFSByCode(acctInfo.Code)); 
			reqObj.put("LOGINMETHOD", 	acctInfo.LoginMethod); 
			reqObj.put("CERTNAME", 		acctInfo.UID);
			reqObj.put("CERTPWD", 		acctInfo.UPW);
			reqObj.put("LOGINID", 		acctInfo.UID);  		 
			reqObj.put("LOGINPWD", 		acctInfo.UPW);  	
			reqObj.put("NUMBER", 		"12345678");  	
			reqObj.put("STARTDATE",		ipt_st_date.getText().toString().replaceAll("-", ""));  		
			reqObj.put("ENDDATE",		ipt_et_date.getText().toString().replaceAll("-", ""));  		
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		SmartAIB smartAIB = new SmartAIB(ssMoniActivity.this ,this);
		smartAIB.AIB_Execute(reqObj.toString());	
		/*
		APIRequestHandler thisHandler;
		thisHandler = new APIRequestHandler(ssMoniActivity.this,module,acctInfo.Code, 
		new APIRequestCallback() {
			@Override
			public void onRequestComplete(boolean status, String errMsg) {
				
				checkCurrentSteps(status, errMsg, m_acctInfo.get(currStep).Code);
			}
		});
		callScrappingAPI(thisHandler, reqObj.toString());
		*/
    }

	@Override
	public void onComplete(String jsonData) {
		// TODO Auto-generated method stub
        String gbData = jsonData;
        Toast.makeText(ssMoniActivity.this, gbData, Toast.LENGTH_LONG).show();
	}
	
	private void callScrappingAPI(final APIRequestHandler thisHandler, final String reqStr) {
		try {
			Handler mTimeoutHandler = new Handler(Looper.getMainLooper());
			mTimeoutHandler.postDelayed(new Runnable() {
			public void run() {
				try {
					SmartAIB ssa = new SmartAIB(ssMoniActivity.this, thisHandler);
					ssa.AIB_Execute(reqStr);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}}, 100);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkCurrentSteps(boolean status, String errMsg, String FCode) {
		int thisStep = Step.incrementAndGet();
		currStep = thisStep;
		if (thisStep >= totalSteps) {
			Toast.makeText(ssMoniActivity.this, "완료", Toast.LENGTH_LONG).show();
		} else {
			requestAccount(m_acctInfo.get(thisStep));
		}
		//callUpdateProgress(1, thisStep, totalInitialSteps, status, errMsg, UIUtils.getCompanyNameByFCode(FCode));
		int curPercentage = (int) (((float) thisStep / (float) totalSteps) * 100);
	}    
    
	private void readCertInfo()
	{
		AssetManager assetManager = getAssets();

	    String str = "";
        StringBuffer buf = new StringBuffer();            
        InputStream is = null;
		try {
			is = assetManager.open("cert_info.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            if (is != null) {                            
                try {
                	int idx = 0;
					while ((str = reader.readLine()) != null) {    
					    buf.append(str + "\n" );
					    String[] bf = str.split("\\|");
				        Account p1 = new Account(idx++,bf[0],bf[1],bf[2],bf[3],bf[4],bf[5]); 
					    m_acctInfo.add(p1);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}                
            }
        } finally {
            try { is.close(); } catch (Throwable ignore) {}
        }
	}
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ss_moni_activity);
        list = (ListView ) findViewById(R.id.list);

        readCertInfo();
        
        final AcctAdapter m_adapter = new AcctAdapter(this, R.layout.acct_row, m_acctInfo); // 어댑터를 생성합니다.
        list.setAdapter(m_adapter); // 

        
		ipt_st_date = (TextView ) findViewById(R.id.ipt_st_date);
		ipt_et_date = (TextView ) findViewById(R.id.ipt_et_date);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_YEAR, -7);
		Date stDate = calendar.getTime();
		Date etDate = new Date();
		
		ipt_st_date.setText(new SimpleDateFormat("yyyy-MM-dd").format(stDate));
		ipt_et_date.setText(new SimpleDateFormat("yyyy-MM-dd").format(etDate));
		ipt_st_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){

            //////////////////////// 안드로이드 달력
            final Calendar c = Calendar.getInstance();
            
        	int myYear = c.get(Calendar.YEAR);
        	int myMonth = c.get(Calendar.MONTH);
        	int myDay = c.get(Calendar.DAY_OF_MONTH);
  
             Dialog dlgDate = new DatePickerDialog(ssMoniActivity.this, new DatePickerDialog.OnDateSetListener() {
	         	    public void onDateSet(DatePicker view, int year, int monthOfYear,
	    	            int dayOfMonth) {
	         	    	ipt_st_date.setText(String.valueOf(year)+"-"+ String.format("%02d", monthOfYear + 1)+"-"+String.format("%02d", dayOfMonth));
	    	    }
	    	}, myYear, myMonth, myDay);
             
            dlgDate.show();
            ////////////////////////                
        }});		
		ipt_et_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){

            //////////////////////// 안드로이드 달력
            final Calendar c = Calendar.getInstance();
            
        	int myYear = c.get(Calendar.YEAR);
        	int myMonth = c.get(Calendar.MONTH);
        	int myDay = c.get(Calendar.DAY_OF_MONTH);
  
             Dialog dlgDate = new DatePickerDialog(ssMoniActivity.this, new DatePickerDialog.OnDateSetListener() {
	         	    public void onDateSet(DatePicker view, int year, int monthOfYear,
	    	            int dayOfMonth) {
	         	    	ipt_et_date.setText(String.valueOf(year)+"-"+ String.format("%02d", monthOfYear + 1)+"-"+String.format("%02d", dayOfMonth));
	    	    }
	    	}, myYear, myMonth, myDay);
             
            dlgDate.show();
            ////////////////////////                
        }});	    		
        
        dialog = new ProgressDialog(this);

        ImageButton btnClose = (ImageButton ) findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
				finish();              	
        }});
        
        Button btnSearch = (Button ) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
                Step = new AtomicInteger(0);
                totalSteps = m_adapter.getCount();
    			requestAccount(m_acctInfo.get(0));
            }});	        
    }
} 
