package com.kwic.saibex2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;


import com.kwic.saib.pub.CertAPI;
import com.kwic.saib.pub.SmartAIB;
import com.kwic.saibex2.BankAcctListAdapter.BankTotalAdapterCallback;

public class ssBankActivity extends Activity implements  BankTotalAdapterCallback{
		
		String[] arr_name;
		String[] arr_code;
		
		String   sel_code = "004";
		String   sel_name;
		String   sel_mod;
		String   gbData = "";
		
		ListView list;
	    BankAcctListAdapter totalAdapter;

        ProgressDialog dialog;
    	TextView ipt_st_date, ipt_et_date;

		/* create by androkim */
//		private String mLoginMethod;

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ss_bank_activity);

			/* create by androkim */
//			mLoginMethod = getIntent().getExtras().getString("login_method");
            
    		ipt_st_date = (TextView ) findViewById(R.id.ipt_st_date);
    		ipt_et_date = (TextView ) findViewById(R.id.ipt_et_date);
            
    		Calendar calendar = Calendar.getInstance();
    		calendar.setTime(new Date());
    		calendar.add(Calendar.DAY_OF_YEAR, -7);
    		Date stDate = calendar.getTime();
    		Date etDate = new Date();
    		
    		ipt_st_date.setText(new SimpleDateFormat("yyyy-MM-dd").format(stDate));
    		ipt_et_date.setText(new SimpleDateFormat("yyyy-MM-dd").format(etDate));
    		ipt_st_date.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View v){

                //////////////////////// 안드로이드 달력
                final Calendar c = Calendar.getInstance();
                
            	int myYear = c.get(Calendar.YEAR);
            	int myMonth = c.get(Calendar.MONTH);
            	int myDay = c.get(Calendar.DAY_OF_MONTH);
      
                 Dialog dlgDate = new DatePickerDialog(ssBankActivity.this, new DatePickerDialog.OnDateSetListener() {
    	         	    public void onDateSet(DatePicker view, int year, int monthOfYear,
    	    	            int dayOfMonth) {
    	         	    	ipt_st_date.setText(String.valueOf(year)+"-"+ String.format("%02d", monthOfYear + 1)+"-"+String.format("%02d", dayOfMonth));
    	    	    }
    	    	}, myYear, myMonth, myDay);
                 
                dlgDate.show();
                ////////////////////////                
            }});		
    		ipt_et_date.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View v){

                //////////////////////// 안드로이드 달력
                final Calendar c = Calendar.getInstance();
                
            	int myYear = c.get(Calendar.YEAR);
            	int myMonth = c.get(Calendar.MONTH);
            	int myDay = c.get(Calendar.DAY_OF_MONTH);
      
                 Dialog dlgDate = new DatePickerDialog(ssBankActivity.this, new DatePickerDialog.OnDateSetListener() {
    	         	    public void onDateSet(DatePicker view, int year, int monthOfYear,
    	    	            int dayOfMonth) {
    	         	    	ipt_et_date.setText(String.valueOf(year)+"-"+ String.format("%02d", monthOfYear + 1)+"-"+String.format("%02d", dayOfMonth));
    	    	    }
    	    	}, myYear, myMonth, myDay);
                 
                dlgDate.show();
                ////////////////////////                
            }});	    		
            
            dialog = new ProgressDialog(this);

            ImageButton btnClose = (ImageButton ) findViewById(R.id.btn_close);
            btnClose.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View v){
    				finish();              	
            }});
            
            Button btnSearch = (Button ) findViewById(R.id.btnSearch);
            btnSearch.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick (View v){
    				Intent intent = new Intent(getBaseContext(), CertListActivity.class);
    				startActivityForResult(intent, 0);                	
            }});	        
	        
			arr_name = (String[])getResources().getStringArray(R.array.BANK_NAME);
			arr_code = (String[])getResources().getStringArray(R.array.BANK_FILE);

			final Spinner spinnerDropDown =(Spinner)findViewById(R.id.ipt_name);
			SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.ss_acct_row ,arr_name, arr_code);
			spinnerDropDown.setAdapter(adapter);
			spinnerDropDown.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					// Get select item
					int sid=spinnerDropDown.getSelectedItemPosition();
					sel_code = arr_code[sid];
					sel_name = arr_name[sid];
				}
				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub		
				}
			});            
        }

    	public void showData(String sModule, String jsonData, String AccountNumber) {
    	    dialog.dismiss();
    		JSONObject jObj = null;
    		try {
    			jObj = new JSONObject(jsonData);
        		String retVal = "";
        		String errMag = "";
        		try {
        			retVal = jObj.getString("RESULT");
        			errMag = jObj.getString("ERRMSG");
        		} catch (JSONException e2) {
        			// TODO Auto-generated catch block
        			e2.printStackTrace();
        		}
        		
        		if ("FAIL".equals(retVal)) {
        			Toast.makeText(ssBankActivity.this, errMag, Toast.LENGTH_LONG).show();
        			return ;
        		}    
    		} catch (JSONException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    			
    		JSONObject bankList1 = null;
    		JSONObject bankList3 = null;
    		String jSubData = "";
    		try {
    			 switch (Integer.parseInt(sModule)) {
    				case 1: 
    	    			bankList1 = jObj.getJSONObject("BANK_1");
    	    			jSubData = bankList1.toString(); 
    					break;
    				case 3: 
    	    			bankList3 = jObj.getJSONObject("BANK_3");
    	    			jSubData = bankList3.getJSONObject(AccountNumber).toString();
    					break;
    			 }
    		} catch (JSONException e) {
    		    dialog.dismiss();
    			return;
    		}

    		// 전계좌 조회
    		JSONObject jSub = null;
    		JSONArray jArray = null;
    		try {
    			jSub = new  JSONObject(jSubData);

        		String retVal = "";
        		String errMag = "";
        		try {
        			retVal = jSub.getString("RESULT");
        			errMag = jSub.getString("ERRMSG");
        		} catch (JSONException e2) {
        			// TODO Auto-generated catch block
        			e2.printStackTrace();
        		}
        		
        		if ("FAIL".equals(retVal)) {
        			Toast.makeText(ssBankActivity.this, errMag, Toast.LENGTH_LONG).show();
        			return ;
        		}
    			jArray = jSub.getJSONArray("LIST");
    		} catch (JSONException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
    		if ("1".equals(sModule)) {
    			showBankList(jArray);	
    		} else if ("3".equals(sModule) && jArray != null) {
    			String jData = jArray.toString();
    			if ("[]".equals(jData)) {
        			Toast.makeText(ssBankActivity.this, "거래내역이 없습니다.", Toast.LENGTH_LONG).show();
    			} else {
	    			Intent i = new Intent(ssBankActivity.this, ssBSubActivity.class);   
	    			i.putExtra("mod", sModule );	
	    			i.putExtra("code", sel_code );	
	    			i.putExtra("data", jData );	
	    			startActivity(i);
    			}
    		}    		
    	}
    	
    	public void showBankList(JSONArray json){
    		ArrayList<HashMap<String, String>> dealList = new ArrayList<HashMap<String, String>>();
    		
    		//NUMBERS = "";
            for (int i = 0; i < json.length(); i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                try {
                    map.put("FCODE", sel_code);
                    map.put("MODULE",sel_code);
                    JSONObject c = (JSONObject) json.get(i);
                    //Fill map
                    Iterator iter = c.keys();
                    while(iter.hasNext())   {
                        String currentKey = (String) iter.next();
                        map.put(currentKey, c.getString(currentKey));
                    }
                    dealList.add(map);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            };				
    		list=(ListView)findViewById(R.id.list);
    		// Getting adapter by passing xml data ArrayList
    		totalAdapter=new BankAcctListAdapter(ssBankActivity.this, dealList);  
    		totalAdapter.setCallback(ssBankActivity.this);	
            list.setAdapter(totalAdapter);			
    	}
    	
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			super.onActivityResult(requestCode, resultCode, data);
			switch (resultCode) {
			   case RESULT_OK:
				    String passwd = data.getStringExtra("passwd");
				    String certnm = data.getStringExtra("certnm"); 
				    //Toast.makeText(this, "passwd:"+passwd, Toast.LENGTH_LONG).show();
					
				    CertAPI c = new CertAPI();
				    if (c.PKI_CheckPassword(certnm, passwd) == true) {
						Toast.makeText(ssBankActivity.this, "인증서 비밀번호가 확인 되었습니다.", Toast.LENGTH_LONG).show();
				    } else {
						Toast.makeText(ssBankActivity.this, "인증서 비밀번호가 틀렸습니다.", Toast.LENGTH_LONG).show();
						return;
				    }
				    
					dialog.setTitle("조회중");
					dialog.setMessage("조회중입니다. 잠시만 기다려 주세요.");
					dialog.setIndeterminate(true);
					dialog.setCancelable(true);
					dialog.show();

					JSONObject obj1 = new JSONObject();
					try {
						obj1.put("MODULE","1,3");  				// 1,3 (전계좌 , 거래내역)
						obj1.put("FCODE",AiBUtils.getFSByCode(sel_code));  			// 기관코드
						obj1.put("LOGINMETHOD","0");  			// 0 - 인증서 / 1 아이디							
						obj1.put("NUMBER","");  				// 계좌번호
						obj1.put("REGNUMBER","");  				// 계좌소유주
						obj1.put("CERTNAME",certnm);  			// 인증서명
						obj1.put("CERTPWD",passwd);  			// 인증서비림번호

						obj1.put("STARTDATE",ipt_st_date.getText().toString().replaceAll("-", ""));  		// 조회일자
						obj1.put("ENDDATE",ipt_et_date.getText().toString().replaceAll("-", ""));  		// 조회일자
	
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		

				   Log.d("smartaib", "request:"+obj1.toString());
					new SmartAIB(ssBankActivity.this ,new Handler() {
					    public void handleMessage(Message msg) {


					        boolean status = false;
					        HashMap map = (HashMap)msg.obj;

							Log.d("smartaib", "result");
							for(Object key : map.keySet()){
								Log.d("smartaib", "\t"+key+" "+map.get(key));
							}

					        try {
					        	dialog.dismiss();
								String rtnMsg = (String) map.get("rtnMsg");
								File dir = new File("/sdcard/Broccoi");
								dir.mkdirs();
								try {
									FileOutputStream fos = new FileOutputStream(
											String.format("%s/%s-%s.json",
													dir.getPath(),
													AiBUtils.getFSByCode(sel_code),
													new SimpleDateFormat("yyMMddHHmmss").format(new Date())
											)
									);
									fos.write(rtnMsg.getBytes());
									fos.flush();
									fos.close();
								} catch (IOException e) {
								}
								JSONObject jRet = new JSONObject(rtnMsg);
					            gbData = jRet.toString();
					            showData("1", gbData, "");
					            //Toast.makeText(ssMainActivity.this, jRet.toString(), Toast.LENGTH_LONG).show();
					        } catch (JSONException e) {
					            e.printStackTrace();
					        }
					    }
					}).AIB_Execute(obj1.toString());					    

				   break;
				default:
				   break;
			}
		}

		@Override
		public void deal_list(int position, String AcctNum) {
			// TODO Auto-generated method stub
            showData("3", gbData.replace("-", ""), AcctNum.replace("-", ""));
		}

		@Override
		public void balance(int position, String AcctNum) {
			//Intent intent = new Intent(getBaseContext(), CertActivity.class);
			//startActivityForResult(intent, 0);              	
		}				
} 
