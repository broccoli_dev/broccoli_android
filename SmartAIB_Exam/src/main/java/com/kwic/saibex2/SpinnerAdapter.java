package com.kwic.saibex2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SpinnerAdapter extends ArrayAdapter<String> {

    private Context ctx;
    private String[] contentArray;
    private String[] imageArray;

    public SpinnerAdapter(Context context, int resource, String[] objects,
            String[] imageArray) {
        super(context,  R.layout.ss_acct_row, R.id.spText, objects);
        this.ctx = context;
        this.contentArray 	= objects;
        this.imageArray 	= imageArray;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.ss_acct_row, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.spText);
        textView.setText(contentArray[position]);

        ImageView imageView = (ImageView)row.findViewById(R.id.spIcon);
        try {
            String resName = "@drawable/ico_"+ imageArray[position].toLowerCase();
            int id = ctx.getResources().getIdentifier(resName, "drawable", ctx.getPackageName());
        	imageView.setImageResource(id);
        } catch ( Exception e) {
    	}
        return row;    
    }    
}