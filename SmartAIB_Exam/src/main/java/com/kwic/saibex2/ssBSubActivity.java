package com.kwic.saibex2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;


public class ssBSubActivity extends Activity  {

	ListView list;
    BankDealAdapter dealAdapter;

	public void getDealList(String mData, String code, String mod) throws JSONException{
		ArrayList<HashMap<String, String>> dealList = new ArrayList<HashMap<String, String>>();
		
        if ("2".equals(mod)) {
            HashMap<String, String> map = new HashMap<String, String>();
            JSONObject jobj = new  JSONObject(mData);
            
            try {
                map.put("FCODE", code);
                map.put("MODULE",mod);

                Iterator iter = jobj.keys();
                while(iter.hasNext())   {
                    String currentKey = (String) iter.next();
                    map.put(currentKey, jobj.getString(currentKey));
                }
                dealList.add(map);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }        	
        } else { 
			JSONArray json = new JSONArray(mData);
			
	        for (int i = 0; i < json.length(); i++) {
	            HashMap<String, String> map = new HashMap<String, String>();
	            try {
	                map.put("FCODE", code);
	                map.put("MODULE",mod);
	                JSONObject c = (JSONObject) json.get(i);
	                //Fill map
	                Iterator iter = c.keys();
	                while(iter.hasNext())   {
	                    String currentKey = (String) iter.next();
	                    map.put(currentKey, c.getString(currentKey));
	                }
	                dealList.add(map);
	            }
	            catch (JSONException e) {
	                e.printStackTrace();
	            }
	
	        };	
        }
		list=(ListView)findViewById(R.id.list);
		// Getting adapter by passing xml data ArrayList
		dealAdapter=new BankDealAdapter(ssBSubActivity.this, dealList);  
        list.setAdapter(dealAdapter);			
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ss_bsub_activity);
		Bundle bundle = getIntent().getExtras();

        ImageButton btnClose = (ImageButton ) findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
				finish();              	
        }});
        
        if(bundle.getString("data")!= null)
        {
    		try {
				getDealList(bundle.getString("data"), bundle.getString("code"), bundle.getString("mod"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }		
    }
 
	
}

