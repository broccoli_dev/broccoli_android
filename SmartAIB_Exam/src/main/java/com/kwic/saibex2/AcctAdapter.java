package com.kwic.saibex2;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AcctAdapter  extends ArrayAdapter<Account> {
    
    private ArrayList<Account> items;
    private Context mContext;
    public AcctAdapter(Context context, int textViewResourceId, ArrayList<Account> items) {
            super(context, textViewResourceId, items);
            mContext = context;
            this.items = items;
    }
	
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.acct_row, null);
            }
            Account p = items.get(position);
            if (p != null) {
	            TextView title = (TextView) v.findViewById(R.id.title);
	            ImageView thumb_image=(ImageView)v.findViewById(R.id.list_image); 	// thumb image

	            title.setText(p.Name);            
	            
	            String resName = "@drawable/ico_"+ p.Code;
	            int id = v.getResources().getIdentifier(resName, "drawable", mContext.getPackageName());
	            thumb_image.setImageResource(id);
	            
            }
            return v;
    }
}