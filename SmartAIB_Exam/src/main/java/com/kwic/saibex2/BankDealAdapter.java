package com.kwic.saibex2;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/*
 * 은행 거래 리스트 어뎁터
 * 
 */
public class BankDealAdapter extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    
    public BankDealAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    protected String makeStringComma(String str) {
    	try {
	  	  if (str.length() == 0)
	  	   return "";
	  	  long value = Long.parseLong(str.replaceAll(",",""));
	  	  DecimalFormat format = new DecimalFormat("###,###");
	  	  return format.format(value);
    	} catch (Exception e) {
    		return str ;
    	}
  	 }
    
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.bank_deal_row, null);

        TextView title 	= (TextView)vi.findViewById(R.id.title); 		// title
        TextView sub 	= (TextView)vi.findViewById(R.id.sub); 			// title
        TextView ddate 	= (TextView)vi.findViewById(R.id.date); 		// date
        TextView amt 	= (TextView)vi.findViewById(R.id.amt); 			// amt
        TextView inout 	= (TextView)vi.findViewById(R.id.inout); 		// amt
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.list_image); 	// thumb image
        
        HashMap<String, String> deal = new HashMap<String, String>();
        deal = data.get(position);

        if ("2".equals(deal.get("MODULE"))) {
            title.setText(deal.get("ACCTNM").trim()+":"+deal.get("NUMBER").trim());
            ddate.setText(deal.get("OPENDATE"));
            sub.setText(deal.get("ACCTKIND"));
            amt.setTextColor(Color.DKGRAY);
            amt.setText("현재잔액:"+makeStringComma(deal.get("CURBAL"))+"원");
        } else if ("3".equals(deal.get("MODULE"))) {
            title.setText(deal.get("TRANDES"));
            ddate.setText(deal.get("TRANDATE"));
            sub.setText(deal.get("JUKYO"));
            if ("".equals(deal.get("INBAL")) || "0".equals(deal.get("INBAL"))) {
            	inout.setTextColor(Color.RED);
                amt.setText("잔액:"+makeStringComma(deal.get("TRANABAL"))+"원");
                inout.setText("출금:"+makeStringComma(deal.get("OUTBAL"))+"원");
            } else {
                inout.setTextColor(Color.BLUE);
                amt.setText("잔액:"+makeStringComma(deal.get("TRANABAL"))+"원");
                inout.setText("입금:"+makeStringComma(deal.get("INBAL"))+"원");
            }
        }        
        String resName = "@drawable/ico_"+ deal.get("FCODE").toLowerCase();
        int id = vi.getResources().getIdentifier(resName, "drawable", activity.getPackageName());
        thumb_image.setImageResource(id);
        return vi;
    }
}