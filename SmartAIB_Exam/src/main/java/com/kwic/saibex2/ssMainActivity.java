package com.kwic.saibex2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kwic.saib.pub.SmartAIB;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;


public class ssMainActivity extends Activity  implements SmartAIB.OnCompleteListener{

    /* create by androkim */
    private SharedPreferences mSharedPreferences;
    private RadioGroup mRadioGroup;
    private LinearLayout mOriginLayout;
    private LinearLayout mElseLayout;

    private LinearLayout mIdPwLayout1;
    private LinearLayout mIdPwLayout2;
    private LinearLayout mIdPwLayout3;
    private LinearLayout mIdPwLayout4;
    private LinearLayout mIdPwLayout5;
    private LinearLayout mIdPwLayout6;
    private LinearLayout mIdPwLayout7;
    private LinearLayout mIdPwLayout8;

    private Button mSearch1;
    private Button mSearch2;
    private Button mSearch3;
    private Button mSearch4;
    private Button mSearch5;
    private Button mSearch6;
    private Button mSearch7;
    private Button mSearch8;
    private Button mSearch9;

    private EditText mId1;
    private EditText mId2;
    private EditText mId3;
    private EditText mId4;
    private EditText mId5;
    private EditText mId6;
    private EditText mId7;
    private EditText mId8;
    private EditText mId9;

    private EditText mPw1;
    private EditText mPw2;
    private EditText mPw3;
    private EditText mPw4;
    private EditText mPw5;
    private EditText mPw6;
    private EditText mPw7;
    private EditText mPw8;
    private EditText mPw9;

    private TextView mReturn1;
    private TextView mReturn2;
    private TextView mReturn3;
    private TextView mReturn4;
    private TextView mReturn5;
    private TextView mReturn6;
    private TextView mReturn7;
    private TextView mReturn8;
    private TextView mReturn9;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ss_main_activity);

        Log.i("", "////////// SamrtAIB Start : ");

        mSharedPreferences  = getSharedPreferences(this.getClass().getSimpleName(), Activity.MODE_PRIVATE);

        /* create by androkim */
        mOriginLayout = (LinearLayout)findViewById(R.id.ll_ss_main_activity_origin_layout);
        mElseLayout = (LinearLayout)findViewById(R.id.ll_ss_main_activity_else_layout);
        mIdPwLayout1 = (LinearLayout)findViewById(R.id.ll_ss_main_activity_id_pw_layout_1);
        mIdPwLayout2 = (LinearLayout)findViewById(R.id.ll_ss_main_activity_id_pw_layout_2);
        mIdPwLayout3 = (LinearLayout)findViewById(R.id.ll_ss_main_activity_id_pw_layout_3);
        mIdPwLayout4 = (LinearLayout)findViewById(R.id.ll_ss_main_activity_id_pw_layout_4);
        mIdPwLayout5 = (LinearLayout)findViewById(R.id.ll_ss_main_activity_id_pw_layout_5);
        mIdPwLayout6 = (LinearLayout)findViewById(R.id.ll_ss_main_activity_id_pw_layout_6);
        mIdPwLayout7 = (LinearLayout)findViewById(R.id.ll_ss_main_activity_id_pw_layout_7);
        mIdPwLayout8 = (LinearLayout)findViewById(R.id.ll_ss_main_activity_id_pw_layout_8);

        mSearch1 = (Button)findViewById(R.id.btn_ss_main_activity_search_1);
        mSearch2 = (Button)findViewById(R.id.btn_ss_main_activity_search_2);
        mSearch3 = (Button)findViewById(R.id.btn_ss_main_activity_search_3);
        mSearch4 = (Button)findViewById(R.id.btn_ss_main_activity_search_4);
        mSearch5 = (Button)findViewById(R.id.btn_ss_main_activity_search_5);
        mSearch6 = (Button)findViewById(R.id.btn_ss_main_activity_search_6);
        mSearch7 = (Button)findViewById(R.id.btn_ss_main_activity_search_7);
        mSearch8 = (Button)findViewById(R.id.btn_ss_main_activity_search_8);
        mSearch9 = (Button)findViewById(R.id.btn_ss_main_activity_search_9);

        mSearch1.setOnClickListener(btnClickListener);
        mSearch2.setOnClickListener(btnClickListener);
        mSearch3.setOnClickListener(btnClickListener);
        mSearch4.setOnClickListener(btnClickListener);
        mSearch5.setOnClickListener(btnClickListener);
        mSearch6.setOnClickListener(btnClickListener);
        mSearch7.setOnClickListener(btnClickListener);
        mSearch8.setOnClickListener(btnClickListener);
        mSearch9.setOnClickListener(btnClickListener);

        mId1 = (EditText)findViewById(R.id.et_ss_main_activity_id_1);
        mId2 = (EditText)findViewById(R.id.et_ss_main_activity_id_2);
        mId3 = (EditText)findViewById(R.id.et_ss_main_activity_id_3);
        mId4 = (EditText)findViewById(R.id.et_ss_main_activity_id_4);
        mId5 = (EditText)findViewById(R.id.et_ss_main_activity_id_5);
        mId6 = (EditText)findViewById(R.id.et_ss_main_activity_id_6);
        mId7 = (EditText)findViewById(R.id.et_ss_main_activity_id_7);
        mId8 = (EditText)findViewById(R.id.et_ss_main_activity_id_8);
        mId9 = (EditText)findViewById(R.id.et_ss_main_activity_id_9);

        mPw1 = (EditText)findViewById(R.id.et_ss_main_activity_pw_1);
        mPw2 = (EditText)findViewById(R.id.et_ss_main_activity_pw_2);
        mPw3 = (EditText)findViewById(R.id.et_ss_main_activity_pw_3);
        mPw4 = (EditText)findViewById(R.id.et_ss_main_activity_pw_4);
        mPw5 = (EditText)findViewById(R.id.et_ss_main_activity_pw_5);
        mPw6 = (EditText)findViewById(R.id.et_ss_main_activity_pw_6);
        mPw7 = (EditText)findViewById(R.id.et_ss_main_activity_pw_7);
        mPw8 = (EditText)findViewById(R.id.et_ss_main_activity_pw_8);
        mPw9 = (EditText)findViewById(R.id.et_ss_main_activity_pw_9);

        mReturn1 = (TextView)findViewById(R.id.tv_ss_main_activity_return_1);
        mReturn2 = (TextView)findViewById(R.id.tv_ss_main_activity_return_2);
        mReturn3 = (TextView)findViewById(R.id.tv_ss_main_activity_return_3);
        mReturn4 = (TextView)findViewById(R.id.tv_ss_main_activity_return_4);
        mReturn5 = (TextView)findViewById(R.id.tv_ss_main_activity_return_5);
        mReturn6 = (TextView)findViewById(R.id.tv_ss_main_activity_return_6);
        mReturn7 = (TextView)findViewById(R.id.tv_ss_main_activity_return_7);
        mReturn8 = (TextView)findViewById(R.id.tv_ss_main_activity_return_8);
        mReturn9 = (TextView)findViewById(R.id.tv_ss_main_activity_return_9);



        mRadioGroup = (RadioGroup)findViewById(R.id.rg_ss_main_activity_group);
        mRadioGroup.setOnCheckedChangeListener(RadioBtnCheckedChangeListener);

        /* InitSetting */
        mRadioGroup.check(R.id.rb__ss_main_activity_1);								// default
        mOriginLayout.setVisibility(View.VISIBLE);
        mElseLayout.setVisibility(View.GONE);

        mId1.setText(getPrefString("id_1"));
        mId2.setText(getPrefString("id_2"));
        mId3.setText(getPrefString("id_3"));
        mId4.setText(getPrefString("id_4"));
        mId5.setText(getPrefString("id_5"));
        mId6.setText(getPrefString("id_6"));
        mId7.setText(getPrefString("id_7"));
        mId8.setText(getPrefString("id_8"));
        mId9.setText(getPrefString("id_9"));

        mPw1.setText(getPrefString("pw_1"));
        mPw2.setText(getPrefString("pw_2"));
        mPw3.setText(getPrefString("pw_3"));
        mPw4.setText(getPrefString("pw_4"));
        mPw5.setText(getPrefString("pw_5"));
        mPw6.setText(getPrefString("pw_6"));
        mPw7.setText(getPrefString("pw_7"));
        mPw8.setText(getPrefString("pw_8"));
        mPw9.setText(getPrefString("pw_9"));


		final JSONObject obj1 = new JSONObject();
		try {
			obj1.put("MODULE","1");  				
			obj1.put("FCODE","MBHN");  					// 기관코드			
			obj1.put("LOGINMETHOD","1");  									
			obj1.put("LOGINID","pcfriend2010");  			// 로그인아이디
			obj1.put("LOGINPWD","xapi1004");  		// 로그인비번

			obj1.put("STARTDATE","20151001");  			// 조회일자
			obj1.put("ENDDATE","20151031");  			// 조회일자

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		/*
		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
            	SmartAIB ssAib = new SmartAIB(ssMainActivity.this, ssMainActivity.this);
            	ssAib.AIB_Execute(obj1.toString());		
		        Toast.makeText(ssMainActivity.this, "조회를 시작합니다.", Toast.LENGTH_LONG).show();	            	
            }
		}, 100);
		*/
//    	SmartAIB ssAib = new SmartAIB(ssMainActivity.this, ssMainActivity.this);
//    	ssAib.AIB_Execute(obj1.toString());
//        Toast.makeText(ssMainActivity.this, "조회를 시작합니다.", Toast.LENGTH_LONG).show();


        ImageButton btnClose = (ImageButton ) findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
				finish();              	
        }});	
        //은행예제
        Button btnBank = (Button ) findViewById(R.id.btnBank);
        btnBank.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
				Intent intent = new Intent(getBaseContext(), ssBankActivity.class);
                /* create by androkim */
                if (((RadioButton)findViewById(R.id.rb__ss_main_activity_1)).isSelected()){
                    intent.putExtra("login_method", "certi");
                }
                else if (((RadioButton)findViewById(R.id.rb__ss_main_activity_2)).isSelected()){
                    intent.putExtra("login_method", "idpw");
                }
				startActivity(intent);
        }});	    
        
        //카드 샘플
        Button btnCard = (Button ) findViewById(R.id.btnCard);
        btnCard.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
				Intent intent = new Intent(getBaseContext(), ssCardActivity.class);
                /* create by androkim */
                if (((RadioButton)findViewById(R.id.rb__ss_main_activity_1)).isSelected()){
                    intent.putExtra("login_method", "certi");
                }
                else if (((RadioButton)findViewById(R.id.rb__ss_main_activity_2)).isSelected()){
                    intent.putExtra("login_method", "idpw");
                }
				startActivity(intent);                	
        }});
        //현금영수증
        Button btnCash = (Button ) findViewById(R.id.btnCash);
        btnCash.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
				Intent intent = new Intent(getBaseContext(), ssCashActivity.class);
				startActivity(intent);                	
        }});
        //모니터링
        Button btnMon = (Button ) findViewById(R.id.btnMon);
        btnMon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
				//Intent intent = new Intent(getBaseContext(), ssMoniActivity.class);
				//startActivity(intent);           
            	SmartAIB ssAib = new SmartAIB(ssMainActivity.this, ssMainActivity.this);
            	ssAib.AIB_Execute(obj1.toString());
		        Toast.makeText(ssMainActivity.this, "조회를 시작합니다.", Toast.LENGTH_LONG).show();	            	
        }});
    }

	@Override
	public void onComplete(String jsonData) {
		// TODO Auto-generated method stub
        Toast.makeText(ssMainActivity.this, jsonData, Toast.LENGTH_LONG).show();
	}





    /** create by androkim **/
    private android.widget.RadioGroup.OnCheckedChangeListener RadioBtnCheckedChangeListener = new android.widget.RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.rb__ss_main_activity_1:
                    mOriginLayout.setVisibility(View.VISIBLE);
                    mElseLayout.setVisibility(View.GONE);
                    break;

                case R.id.rb__ss_main_activity_2:
                    mOriginLayout.setVisibility(View.GONE);
                    mElseLayout.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };



    /* create by androkim */
    private View.OnClickListener btnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View mView) {

            switch (mView.getId()) {
                case R.id.btn_ss_main_activity_search_1:
                    // 우리은행
                    if (mId1.getText().toString().length() == 0 || mPw1.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "우리은행  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_1", mId1.getText().toString());
                        setPrefString("pw_1", mPw1.getText().toString());
                    }
                    sendJsonData("3", mId1.getText().toString(), mPw1.getText().toString(), "020", mCompleteListener1);
//                    if (writeToFile("test.txt", "ABCDEFGHIJK..."))  Toast.makeText(ssMainActivity.this, "생성완료", Toast.LENGTH_SHORT).show();
//                    writeData("test.txt", "ABCDEFGHIJK...");
//                    readFromFile("test.txt");
                    break;

                case R.id.btn_ss_main_activity_search_2:
                    // 신한은행
                    if (mId2.getText().toString().length() == 0 || mPw2.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "신한은행  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_2", mId2.getText().toString());
                        setPrefString("pw_2", mPw2.getText().toString());
                    }
                    sendJsonData("1,3", mId2.getText().toString(), mPw2.getText().toString(), "088", mCompleteListener2);
//                    Toast.makeText(ssMainActivity.this, getFileUri("test.txt").toString(), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(ssMainActivity.this, readFromFile("test.txt"), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(ssMainActivity.this, writeData2("test2.txt", "").toString(), Toast.LENGTH_SHORT).show();
//                    sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "신한은행", "", "jsonBankSinhan.txt");
                    break;

                case R.id.btn_ss_main_activity_search_3:
                    // 국민은행
                    if (mId3.getText().toString().length() == 0 || mPw3.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "국민은행  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_3", mId3.getText().toString());
                        setPrefString("pw_3", mPw3.getText().toString());
                    }
                    sendJsonData("1,3", mId3.getText().toString(), mPw3.getText().toString(), "004", mCompleteListener3);
                    break;

                case R.id.btn_ss_main_activity_search_4:
                    // 기업은행
                    if (mId4.getText().toString().length() == 0 || mPw4.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "기업은행  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_4", mId4.getText().toString());
                        setPrefString("pw_4", mPw4.getText().toString());
                    }
                    sendJsonData("1,3", mId4.getText().toString(), mPw4.getText().toString(), "003", mCompleteListener4);
                    break;

                case R.id.btn_ss_main_activity_search_5:
                    // 하나은행
                    if (mId5.getText().toString().length() == 0 || mPw5.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "하나은행  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_5", mId5.getText().toString());
                        setPrefString("pw_5", mPw5.getText().toString());
                    }
                    sendJsonData("1,3", mId5.getText().toString(), mPw5.getText().toString(), "081", mCompleteListener5);
                    break;

                case R.id.btn_ss_main_activity_search_6:
                    // 현대카드
                    if (mId6.getText().toString().length() == 0 || mPw6.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "현대카드  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_6", mId6.getText().toString());
                        setPrefString("pw_6", mPw6.getText().toString());
                    }
//                    sendJsonData("2,3", mId6.getText().toString(), mPw6.getText().toString(), "505", mCompleteListener6);
                    sendJsonData("2,3", mId6.getText().toString(), mPw6.getText().toString(), "505", mCompleteListener6);
                    break;

                case R.id.btn_ss_main_activity_search_7:
                    // 신한카드
                    if (mId7.getText().toString().length() == 0 || mPw7.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "신한카드  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_7", mId7.getText().toString());
                        setPrefString("pw_7", mPw7.getText().toString());
                    }
//                    sendJsonData("2,3", mId7.getText().toString(), mPw7.getText().toString(), "506", mCompleteListener7);
                    sendJsonData("2,3", mId7.getText().toString(), mPw7.getText().toString(), "506", mCompleteListener7);
                    break;

                case R.id.btn_ss_main_activity_search_8:
                    // 삼성카드
                    if (mId8.getText().toString().length() == 0 || mPw8.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "삼성카드  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_8", mId8.getText().toString());
                        setPrefString("pw_8", mPw8.getText().toString());
                    }
//                    sendJsonData("2,3", mId8.getText().toString(), mPw8.getText().toString(), "504", mCompleteListener8);
                    sendJsonData("2", mId8.getText().toString(), mPw8.getText().toString(), "504", mCompleteListener8);
                    break;

                case R.id.btn_ss_main_activity_search_9:
                    // 삼성카드
                    if (mId9.getText().toString().length() == 0 || mPw9.getText().toString().length() == 0){
                        Toast.makeText(ssMainActivity.this, "BC  ID / PW 공백", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        setPrefString("id_9", mId9.getText().toString());
                        setPrefString("pw_9", mPw9.getText().toString());
                    }
//                    sendJsonData("2,3", mId8.getText().toString(), mPw8.getText().toString(), "504", mCompleteListener8);
                    sendJsonData("2", mId9.getText().toString(), mPw9.getText().toString(), "503", mCompleteListener9);
                    break;
            }
        }
    };

    /* create by androkim */
    private JSONObject sendJsonData(String _module, String _id, String _pw, String _code, SmartAIB.OnCompleteListener _CompleteListener){
        Toast.makeText(ssMainActivity.this, "조회 중..", Toast.LENGTH_LONG).show();
        final JSONObject obj = new JSONObject();
        try {

            obj.put("MODULE",_module);
            obj.put("FCODE",AiBUtils.getFSByCode(_code.trim()));  			// 기관코드
//            obj.put("FCODE",_cqaode.trim());  					// 기관코드
            obj.put("LOGINMETHOD","1");
            obj.put("LOGINID",_id.trim());  			// 로그인아이a
            obj.put("LOGINPWD",_pw.trim());  		         // 로그인비번

            obj.put("STARTDATE","20151201");  			// 조회일자
            obj.put("ENDDATE","20151231");       			// 조회일자
            if (_code.equals("504") || _code.equals("505") || _code.equals("506")) {
                //obj.put("APPHISTORYVIEW","1");       			// 가맹정보
                //obj.put("AppHistoryView","1");       			// 가맹정보
                obj.put("ORG","1002");
                obj.put("CERTKEY", "");
                obj.put("REGNUMBER", "");
                obj.put("CVC", "");
                obj.put("YYYYMM", "");
            } else {
                obj.put("ORG","1001");
                obj.put("NUMBER", "1002346031436");
            }
            obj.put("CUS_KIND", "0");
//            obj.put("STARTDATE","20151001");  			// 조회일자
//            obj.put("ENDDATE","20151130");       			// 조회일자


//            obj.put("ORG","1001");
//            obj.put("CERTKEY","");
//            obj.put("CUS_KIND","");
//            obj.put("CERTNAME","");
//            obj.put("CERTPWD","");
//            obj.put("NUMBER","");
//            obj.put("PASSWORD","");
//            obj.put("CURRCD","");
//            obj.put("REGNUMBER","");
//            obj.put("CVC","YYYYMM");


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new SmartAIB(ssMainActivity.this, _CompleteListener).AIB_Execute(obj.toString());

        return obj;
    }





    /* create by androkim */
    private SmartAIB.OnCompleteListener mCompleteListener1 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn1.setText(jsonData);
////            if (writeToFile("jsonBankUri.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "우리은행", jsonData, "jsonBankUri.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "우리은행", jsonData, "jsonBankUri.txt");
//            Log.d("", "////////// mReturn1 : " + jsonData);
        }
    };

    private SmartAIB.OnCompleteListener mCompleteListener2 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn2.setText(jsonData);
////            if (writeToFile("jsonBankSinhan.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "신한은행", jsonData, "jsonBankSinhan.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "신한은행", jsonData, "jsonBankSinhan.txt");
//            Log.d("", "////////// mReturn2 : " + jsonData);
        }
    };

    private SmartAIB.OnCompleteListener mCompleteListener3 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn3.setText(jsonData);
////            if (writeToFile("jsonBankKb.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "국민은행", jsonData, "jsonBankKb.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "국민은행", jsonData, "jsonBankKb.txt");
//            Log.d("", "////////// mReturn3 : " + jsonData);
        }
    };

    private SmartAIB.OnCompleteListener mCompleteListener4 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn4.setText(jsonData);
////            if (writeToFile("jsonBankGiup.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "기업은행", jsonData, "jsonBankGiup.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "기업은행", jsonData, "jsonBankGiup.txt");
//            Log.d("", "////////// mReturn4 : " + jsonData);
        }
    };

    private SmartAIB.OnCompleteListener mCompleteListener5 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn5.setText(jsonData);
////            if (writeToFile("jsonBankHana.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "하나은행", jsonData, "jsonBankHana.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "하나은행", jsonData, "jsonBankHana.txt");
//            Log.d("", "////////// mReturn5 : " + jsonData);
        }
    };

    private SmartAIB.OnCompleteListener mCompleteListener6 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn6.setText(jsonData);
////            if (writeToFile("jsonCardHyundae.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "현대카드", jsonData, "jsonCardHyundae.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "현대카드", jsonData, "jsonCardHyundae.txt");
            sendToEmail(ssMainActivity.this, "juhn0512@nomadconnection.com", "현대카드", jsonData, "jsonCardHyundae.txt");
//            Log.d("", "////////// mReturn6 : " + jsonData);
        }
    };

    private SmartAIB.OnCompleteListener mCompleteListener7 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn7.setText(jsonData);
////            if (writeToFile("jsonCardSinhan.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "신한카드", jsonData, "jsonCardSinhan.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "신한카드", jsonData, "jsonCardSinhan.txt");
            sendToEmail(ssMainActivity.this, "juhn0512@nomadconnection.com", "신한카드", jsonData, "jsonCardSinhan.txt");
//            Log.d("", "////////// mReturn7 : " + jsonData);
        }
    };

    private SmartAIB.OnCompleteListener mCompleteListener8 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn8.setText(jsonData);
////            if (writeToFile("jsonCardSamsung.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "삼성카드", jsonData, "jsonCardSamsung.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "삼성카드", jsonData, "jsonCardSamsung.txt");
            sendToEmail(ssMainActivity.this, "juhn0512@nomadconnection.com", "삼성카드", jsonData, "jsonCardSamsung.txt");
//            Log.d("", "////////// mReturn8 : " + jsonData);
        }
    };

    private SmartAIB.OnCompleteListener mCompleteListener9 = new SmartAIB.OnCompleteListener(){
        @Override
        public void onComplete(String jsonData) {
            Toast.makeText(ssMainActivity.this, "조회 완료", Toast.LENGTH_SHORT).show();
            mReturn9.setText(jsonData);
////            if (writeToFile("jsonCardSamsung.txt", jsonData)) sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "삼성카드", jsonData, "jsonCardSamsung.txt");
//            sendToEmail(ssMainActivity.this, "rlagkdrbs@naver.com", "삼성카드", jsonData, "jsonCardSamsung.txt");
//            Log.d("", "////////// mReturn8 : " + jsonData);
        }
    };

    public void setPrefString(String strName, String strValue) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(strName, strValue);
        edit.commit();
    }
    public String getPrefString(String strName) {
        String strRet = mSharedPreferences.getString(strName, "");
        return strRet;
    }

    /** Text File 만들기 **/
    private boolean writeToFile(final String _txtName, final String _data) {
        boolean bResult = true;

        try{
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getBaseContext().openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(_data);
            outputStreamWriter.close();
        } catch(IOException e){
            Toast.makeText(this, "파일이 File write failed.", Toast.LENGTH_SHORT).show();
//            Log.e("Exception", "File write failed: " + e.toString());
            bResult = false;
        }
        finally {

        }

        return bResult;
    }

    public Uri writeData2 ( String _fileName, String _data ) {
        File file = new File(ssMainActivity.this.getFilesDir(), _fileName);
//        String filename = "myfile";
//        String string = "Hello world!";
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(_fileName, Context.MODE_PRIVATE);
            outputStream.write(_data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Uri.fromFile(file);
    }

//    public void writeData ( String _txtName, String _data ) {
//        try {
//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(_txtName, Context.MODE_PRIVATE));
//            outputStreamWriter.write(_data);
//            outputStreamWriter.close();
//        }
//        catch (IOException e) {
//            Log.e("", "File write failed: " + e.toString());
//        }
//    }
//
//    private String readFromFile( String _txtName) {
//
//        String ret = "";
//
//        try {
//            InputStream inputStream = openFileInput(_txtName);
//
//            if ( inputStream != null ) {
//                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                String receiveString = "";
//                StringBuilder stringBuilder = new StringBuilder();
//
//                while ( (receiveString = bufferedReader.readLine()) != null ) {
//                    stringBuilder.append(receiveString);
//                }
//
//                inputStream.close();
//                ret = stringBuilder.toString();
//            }
//        }
//        catch (FileNotFoundException e) {
////            Log.e(TAG, "File not found: " + e.toString());
//        } catch (IOException e) {
////            Log.e(TAG, "Can not read file: " + e.toString());
//        }
//
//        return ret;
//    }



    /** Email 보내기 **/
    /** Email 보내기 **/
    private void sendToEmail(Activity _Activity, String _To, String _Title, String _Body, String _fileName){
        final Intent email = new Intent(android.content.Intent.ACTION_SENDTO);
        String uriText = "mailto:" + _To +
                "?subject=" + URLEncoder.encode(_Title);
        email.setData(Uri.parse(uriText));
//        email.putExtra(Intent.EXTRA_TEXT, _Title);
        email.putExtra(Intent.EXTRA_TEXT, _Body);

//        email.putExtra(Intent.EXTRA_STREAM, getFileUri(_fileName));
        email.putExtra(Intent.EXTRA_STREAM, writeData2(_fileName, _Body));
//        Toast.makeText(ssMainActivity.this, writeData2("test2.txt", "").toString(), Toast.LENGTH_SHORT).show();

        try {
            _Activity.startActivity(email);
        } catch (Exception e) {
        }
    }

    private Uri getFileUri(String _fileName){
        // 전송할 파일의 경로
//        String szSendFilePath = Environment.getExternalStorageDirectory()
//                .getAbsolutePath() + _fileName;
//        File f = new File(szSendFilePath);
//        String szSendFilePath = Environment.getRootDirectory().getAbsolutePath() + _fileName;
        String szSendFilePath = "/"+_fileName;
        File f = new File(szSendFilePath);
        if (!f.exists()) {
            Toast.makeText(this, "파일이 없습니다.", Toast.LENGTH_SHORT).show();
        }

        // File객체로부터 Uri값 생성
        final Uri fileUri = Uri.fromFile(f);
        return fileUri;
    }

}
