package com.kwic.saibex2;

import android.content.Context;

import com.kwic.saib.pub.SmartAIB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class APIRequestHandler implements SmartAIB.OnCompleteListener {

    private Context context;

    public static final int TYPE_BANK 			= 1003;
    public static final int TYPE_CARD 			= 1002;

    public static final String SUB_BANK_ALL 	= "1";
    public static final String SUB_BANK_BALANCE = "2";
    public static final String SUB_BANK_TRAN 	= "3";

    public static final String SUB_CARD_EST 	= "1";
    public static final String SUB_CARD_APPS 	= "2";
    public static final String SUB_CARD_BILL 	= "3";
    public static final String SUB_CARD_LIMIT 	= "4";
    public static final String SUB_CARD_POINT 	= "5";
    public static final String SUB_CARD_ALL 	= "6";

    private Runnable processingDataSaving;
    private String responseJson;

    private String module;
    private String fcode;
    
    private String pay_date = "";
    private String pay_amt  = "";

    public APIRequestHandler() {}
    private APIRequestCallback callback;

    public APIRequestHandler(Context context, String module, String fcode) {
        this.context = context;
        this.module = module;
        this.fcode = fcode;
        init();
    }

    public APIRequestHandler(Context context, String module,
                             String fcode, APIRequestCallback callback) {
        this(context, module, fcode);
        this.callback = callback;
    }


    private ArrayList<HashMap<String, String>> getJasonToArrayList(JSONArray jArray) {
        ArrayList<HashMap<String, String>> tempList = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < jArray.length(); i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("FCODE", fcode);
            try {
                if (jArray.get(i) != null && !jArray.get(i).toString().equalsIgnoreCase("null")) {
                    JSONObject c = (JSONObject) jArray.get(i);
                    Iterator iter = c.keys();
                    while (iter.hasNext()) {
                        String currentKey = (String) iter.next();
                        map.put(currentKey, AiBUtils.cleanse(c.getString(currentKey)));
                    }
                    tempList.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return tempList;
    }

    private void init() {
        processingDataSaving = new Runnable() {
            @Override
            public void run() {

                try {
	                String[] Msubtype = module.split("\\,");
	                JSONObject MjObj = new JSONObject(responseJson);
	                boolean isSuccess = false;
	                String errMsg = "";
	                if (callback != null) {
	                    callback.onRequestComplete(isSuccess, errMsg);
	                }
                } catch (Exception e) {
                    if (callback != null) {
                        callback.onRequestComplete(false, " 다시 시도해 주십시오(실패).");
                    }
                }
            }
        };
    }

    @Override
    public void onComplete(String jsonData) {
        responseJson = jsonData;
        new Thread(processingDataSaving).start();
    }
}
