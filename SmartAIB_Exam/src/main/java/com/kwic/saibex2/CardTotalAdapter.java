package com.kwic.saibex2;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CardTotalAdapter extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    
    private CardTotalAdapterCallback callback;
    
    public CardTotalAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    protected String makeStringComma(String str) {
    	try {
  	  	  if (str.length() == 0)
  	  	   return "";
  	  	  long value = Long.parseLong(str.replaceAll(",",""));
  	  	  DecimalFormat format = new DecimalFormat("###,###");
  	  	  return format.format(value);
      	} catch (Exception e) {
      		return str ;
      	}
    }
    
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.card_total_row, null);
        
        TextView title 	= (TextView)vi.findViewById(R.id.title); 			// title
        TextView amt 	= (TextView)vi.findViewById(R.id.total_amt); 				// amt
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.list_image); 	// thumb image
        
        HashMap<String, String> deal = new HashMap<String, String>();
        deal = data.get(position);
        
        // Setting all values in listview
        final String AccountNum = "";
		if ("6".equals(deal.get("MODULE"))) {
	        title.setText(deal.get("CARDNUM"));
	        amt.setText(deal.get("CARDNAME"));
	        //AccountNum = deal.get("CARDNUM");
		} else if ("5".equals(deal.get("MODULE"))) {
	        title.setText(deal.get("POINTNAME"));
	        amt.setText(makeStringComma(deal.get("POINTAMT"))+"포인트");
		} else if ("4".equals(deal.get("MODULE"))) {
	        title.setText(deal.get("USEDIS")+":"+(deal.get("USELIMIT"))+"원");
	        amt.setText((deal.get("USEAMT")) +"원/"+(deal.get("USEFULAMT"))+"원" );
		} else if ("1".equals(deal.get("MODULE"))) {
	        title.setText(deal.get("ESTDATE"));
	        amt.setText(makeStringComma(deal.get("ESTAMT"))+"원" );
		}
        String resName = "@drawable/ico_"+ deal.get("FCODE").toLowerCase();
        int id = vi.getResources().getIdentifier(resName, "drawable", activity.getPackageName());
        thumb_image.setImageResource(id);
       
        return vi;
    }
    
    public void setCallback(CardTotalAdapterCallback callback){

        this.callback = callback;
    }


    public interface CardTotalAdapterCallback {
        public void deal_list(int position, String AcctNum);
        public void balance(int position, String AcctNum);
    }    
}