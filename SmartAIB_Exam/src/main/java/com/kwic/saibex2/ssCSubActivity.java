package com.kwic.saibex2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;


public class ssCSubActivity extends Activity  {

	ListView list;
    CardDealAdapter dealAdapter;

	ListView list2;
    CardDealAdapter dealAdapter2;  
    
	public void getDealList(String mData, String mData2, String code, String mod) throws JSONException{
		ArrayList<HashMap<String, String>> dealList = new ArrayList<HashMap<String, String>>();
		ArrayList<HashMap<String, String>> dealList2 = new ArrayList<HashMap<String, String>>();

		JSONArray json = new JSONArray(mData);
        for (int i = 0; i < json.length(); i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            try {
                map.put("FCODE", code);
                map.put("MODULE",mod);
                JSONObject c = (JSONObject) json.get(i);
                //Fill map
                Iterator iter = c.keys();
                while(iter.hasNext())   {
                    String currentKey = (String) iter.next();
                    map.put(currentKey, c.getString(currentKey));
                }
                dealList.add(map);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        };	

        if (!"".equals(mData2)) {
			JSONArray json2 = new JSONArray(mData2);
	        for (int i = 0; i < json2.length(); i++) {
	            HashMap<String, String> map = new HashMap<String, String>();
	            try {
	                map.put("FCODE", code);
	                map.put("MODULE","9");
	                JSONObject c = (JSONObject) json2.get(i);
	                //Fill map
	                Iterator iter = c.keys();
	                while(iter.hasNext())   {
	                    String currentKey = (String) iter.next();
	                    map.put(currentKey, c.getString(currentKey));
	                }
	                dealList2.add(map);
	            }
	            catch (JSONException e) {
	                e.printStackTrace();
	            }
	        };	
        }
        
		list=(ListView)findViewById(R.id.list);
		// Getting adapter by passing xml data ArrayList
		dealAdapter=new CardDealAdapter(ssCSubActivity.this, dealList);  
        list.setAdapter(dealAdapter);	
        
		list2=(ListView)findViewById(R.id.list2);
		// Getting adapter by passing xml data ArrayList
		dealAdapter2=new CardDealAdapter(ssCSubActivity.this, dealList2);  
        list2.setAdapter(dealAdapter2);	        
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ss_csub_activity);
		Bundle bundle = getIntent().getExtras();
        ImageButton btnClose = (ImageButton ) findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
				finish();              	
        }});
        if(bundle.getString("data")!= null)
        {
    		try {
				getDealList(bundle.getString("data"), bundle.getString("data2"), bundle.getString("code"), bundle.getString("mod"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }		
    }
 
	
}

