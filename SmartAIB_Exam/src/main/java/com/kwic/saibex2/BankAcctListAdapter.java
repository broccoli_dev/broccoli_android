package com.kwic.saibex2;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
/*
 * 은행 계좌 리스트 어뎁터
 * 
 */
public class BankAcctListAdapter extends BaseAdapter {
    
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    
    private BankTotalAdapterCallback callback;
    
    public BankAcctListAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    protected String makeStringComma(String str) {
    	try {
  	  	  if (str.length() == 0)
  	  	   return "";
  	  	  long value = Long.parseLong(str.replaceAll(",",""));
  	  	  DecimalFormat format = new DecimalFormat("###,###");
  	  	  return format.format(value);
      	} catch (Exception e) {
      		return str ;
      	}
    }
    
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.bank_acct_row, null);
        
        TextView title 	= (TextView)vi.findViewById(R.id.title); 			// title
        //TextView ddate 	= (TextView)vi.findViewById(R.id.date); 			// date
        TextView amt 	= (TextView)vi.findViewById(R.id.total_amt); 				// amt
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.list_image); 	// thumb image
        
        HashMap<String, String> deal = new HashMap<String, String>();
        deal = data.get(position);
        
        // Setting all values in listview
        title.setText(deal.get("ACCTKIND").trim()+":"+deal.get("NUMBER").trim());
        amt.setText(makeStringComma(deal.get("CURBAL"))+"원");
        String resName = "@drawable/ico_"+ deal.get("FCODE").toLowerCase();
        int id = vi.getResources().getIdentifier(resName, "drawable", activity.getPackageName());
        
        thumb_image.setImageResource(id);
        final String AccountNum = deal.get("NUMBER").trim();
        Button btn_deal_list  = (Button)vi.findViewById(R.id.btn_deal_list);
        btn_deal_list.setFocusable(false);
        btn_deal_list.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
            	if(callback != null) {
                    callback.deal_list(position, AccountNum);
                 }            	
            }
        });
        return vi;
    }
    
    public void setCallback(BankTotalAdapterCallback callback){

        this.callback = callback;
    }


    public interface BankTotalAdapterCallback {
        public void deal_list(int position, String AcctNum);
        public void balance(int position, String AcctNum);
    }    
}