package com.kwic.saibex2;

public interface APIRequestCallback {
    void onRequestComplete(boolean status, String errMsg);
}
