package com.kwic.saibex2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import android.annotation.SuppressLint;


public class AiBUtils {
	
    public static boolean checkValid(String str) {
        return str != null && str.length() > 0;
    }
    
    public static String cleanse(String input) {
        if (checkValid(input)) {
            input = input.replace("\\n", "");
            input = input.replace("\\t", "");
            input = input.replaceAll("\\s+", " ");
            input = input.trim();
            return input;

        } else {
            return "";
        }
    }
    
	public static String getCertType(String oid)
	{
		// 금결원
		if("1.2.410.200005.1.1.1".compareTo(oid) == 0)
			return "범용(개인)";
		
		if("1.2.410.200005.1.1.2".compareTo(oid) == 0)
			return "은행/신용카드/보험(법인)";
		
		if("1.2.410.200005.1.1.4".compareTo(oid) == 0)
			return "은행/신용카드/보험(개인)";

		if("1.2.410.200005.1.1.5".compareTo(oid) == 0)
			return "범용(법인)";

		
		// 정보인증
		if("1.2.410.200004.5.2.1.1".compareTo(oid) == 0)
			return "1등급 법인";
		
		if("1.2.410.200004.5.2.1.2".compareTo(oid) == 0)
			return "1등급 개인";
		
		if("1.2.410.200004.5.2.1.7.1".compareTo(oid) == 0)
			return "은행/신용카드/보험용";
		
		// 코스콤
		if("1.2.410.200004.5.1.1.5".compareTo(oid) == 0)
			return "범용(개인)";
		if("1.2.410.200004.5.1.1.7".compareTo(oid) == 0)
			return "범용(법인)";
		if("1.2.410.200004.5.1.1.9".compareTo(oid) == 0)
			return "증권/보험(개인)";
		if("1.2.410.200004.5.1.1.10".compareTo(oid) == 0)
			return "증권/보험용(개인)";
		if("1.2.410.200004.5.1.1.10.1".compareTo(oid) == 0)
			return "은행/보험용(개인)";
		
		// 전자인증
		if("1.2.410.200004.5.4.1.1".compareTo(oid) == 0)
			return "범용(개인)";
		if("1.2.410.200004.5.4.1.2".compareTo(oid) == 0)
			return "범용(법인)";
		if("1.2.410.200004.5.4.1.101".compareTo(oid) == 0)
			return "은행/보험용";
		
		// 무역정보통신
		if("1.2.410.200012.1.1.1".compareTo(oid) == 0)
			return "범용(개인)";
		if("1.2.410.200012.1.1.3".compareTo(oid) == 0)
			return "범용(법인)";
		if("1.2.410.200012.1.1.101".compareTo(oid) == 0)
			return "은행/보험용";
		
		return "인증서";
	}
	
	public static String getOID(String inOID) 
	{	
		String oidV = "";
		oidV += "1.2.410.200004.5.2.1.1:법인 범용|";
		oidV += "1.2.410.200004.5.2.1.2:개인 범용|";
		oidV += "1.2.410.200004.5.1.1.7:법인 범용|";
		oidV += "1.2.410.200004.5.1.1.5:개인 범용|";
		oidV += "1.2.410.200005.1.1.5: 법인 범용|";
		oidV += "1.2.410.200005.1.1.1:개인 범용|";
		oidV += "1.2.410.200004.5.4.1.2:법인 범용|";
		oidV += "1.2.410.200004.5.4.1.1:개인 범용|";
		oidV += "1.2.410.200012.1.1.3:법인 범용|";
		oidV += "1.2.410.200012.1.1.1:개인 범용|";
		oidV += "1.2.410.200004.5.2.1.7.1:은행거래용/보험용|";
		oidV += "1.2.410.200004.5.2.1.7.2:증권거래용/보험용|";
		oidV += "1.2.410.200004.5.2.1.7.3:신용카드용|";
		oidV += "1.2.410.200004.5.1.1.9:용도제한용|";
		oidV += "1.2.410.200005.1.1.4:은행/보험용|";
		oidV += "1.2.410.200005.1.1.6.2:신용카드용|";
		oidV += "1.2.410.200004.5.4.1.101:인터넷뱅킹용|";
		oidV += "1.2.410.200004.5.4.1.102:증권거래용|";
		oidV += "1.2.410.200004.5.4.1.103:인터넷보험용|";
		oidV += "1.2.410.200004.5.4.1.104:신용카드용|";
		oidV += "1.2.410.200004.5.4.1.105:전자민원용|";
		oidV += "1.2.410.200004.5.4.1.106:인터넷뱅킹용/전자민원용|";
		oidV += "1.2.410.200004.5.4.1.107:증권거래용/전자민원용|";
		oidV += "1.2.410.200004.5.4.1.108:인터넷보험용/전자민원용|";
		oidV += "1.2.410.200004.5.4.1.109:신용카드용/전자민원용|";
		oidV += "1.2.410.200012.11.31:은행거래용(서명용)|";
		oidV += "1.2.410.200012.11.32:은행거래용(암호용)|";
		oidV += "1.2.410.200012.11.35:증권거래용(서명용)|";
		oidV += "1.2.410.200012.11.36:증권거래용(암호용)|";
		oidV += "1.2.410.200012.11.39:보험거래용(서명용)|";
		oidV += "1.2.410.200012.11.40:보험거래용(암호용)|";
		oidV += "1.2.410.200012.11.43:신용카드용(서명용)|";
		oidV += "1.2.410.200012.11.44:신용카드용(암호용)|";
		String outOID = "미확인";
		String [] oidArr = oidV.split("\\|");
		int m = Arrays.asList(oidArr).indexOf(inOID);
		for (int i = 0; i < oidArr.length; i++) {
			String [] tmOid = oidArr[i].split("\\:");
			if  (tmOid[0].contains(inOID)) {
				outOID = tmOid[1];
				break;
			}
		}
		System.out.println(inOID+":"+outOID);
		return outOID;
	}
	
	
	public static String getFSByCode(String fCode)
	{
		String bc = "004,088,011,005,023,020,045,031,032,002,007,039,048,027,034,071,037,003,035,081,512,502,514,508,504,510,506,501,511,505,503,509,054,";
		String bf = "MBKB,MBSH,MBNH,MBKE,MBSC,MBWR,MBKF,MBDB,MBPS,MBKD,MBSU,MBKN,MBCU,MBCT,MBKJ,MBEP,MBJB,MBIB,MBJJ,MBHN,MCKJ,MCKB,MCNH,MCLT,MCSS,MCSU,MCSH,MCCT,MCWR,MCHD,MCBC,MCHN,MLHT,";
		
		String [] bC = bc.split(",");
		String [] bF = bf.split(",");

		int m = Arrays.asList(bC).indexOf(fCode);
		return bF[m];
	}
	
 	@SuppressLint("SimpleDateFormat")
	public static String getCurrentDate()
	{
		Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd");
		String strReturn = formatter.format(today);		
		return strReturn;
	}

	@SuppressLint("SimpleDateFormat")
	public static String getCurrentDateTime()
	{
		Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		String strReturn = formatter.format(today);
		return strReturn;
	}
	
	public static String convert_issuer(String IssuerO)
	{
		String issuerName = IssuerO;
		if("yessign".compareToIgnoreCase(IssuerO) == 0)
			issuerName = "금융결제원";
		else if("kica".compareToIgnoreCase(IssuerO) == 0)
			issuerName = "한국정보인증";
		else if("signkorea".compareToIgnoreCase(IssuerO) == 0)
			issuerName = "코스콤(증권전산)";
		else if("crosscert".compareToIgnoreCase(IssuerO) == 0)
			issuerName = "한국전자인증";
		else if("tradesign".compareToIgnoreCase(IssuerO) == 0)
			issuerName = "한국무역정보통신";
		else if("ncasign".compareToIgnoreCase(IssuerO) == 0)
			issuerName = "한국전산원";
		
		return issuerName;
	}	
	
	public static String getWithOnlyDigit(String data)
	{
		if(data == null)
			return "";
		
		return data.replaceAll("[^\\d]", "");
	}

	public static int getCertIcon(String date) 
	{
		// 만료시
		if(getDiffOfDate(date) < 0)
			return R.drawable.certificate_unuseably;
		// 만료기간이 30일 이하일경우
		else if(getDiffOfDate(date) < 30)
			return R.drawable.certificate_30;
		// 유효
		else
			return R.drawable.certificate_use;
	}
	
	public static long getDiffOfDate(String compareDate) {
		
		long diffDays = 0;

		// 입력일자에서 숫자만 가져온다.
		String comDate = getWithOnlyDigit(compareDate);

		// 8자리보다 작으면 0으로 리턴한다.
		if(comDate.length() < 8)
			return 0;
		
		try {
			SimpleDateFormat formatter = new SimpleDateFormat ("yyyyMMdd", Locale.ENGLISH);
			
			comDate = comDate.substring(0, 8);
			Date beginDate = formatter.parse(getCurrentDate());
			Date endDate = formatter.parse(comDate);
			
			long diff = endDate.getTime() - beginDate.getTime();
			diffDays = diff / (24*60*60*1000);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return diffDays;
	}	    
}
