package com.kwic.saibex2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.TextUtils;


/**
 * 메세지 처리용 Class
 */
public class InputPassword {

	
	public static final int ALERT_OK = 0;
	public static final int ALERT_CANCEL = 1;
	public static final int ALERT_OKANDCANCEL = 2;
	public static final int ALERT_USERDEFINE = 3;
	public static final int ALERT_BACK = 4;
	
	public static String btn_1 = "확인";
	public static String btn_2 = "취소";
	
	public static void setButtonText(int type, String btnText) {
		switch(type) {
		case ALERT_OK:
			btn_1 = btnText;
			break;
		case ALERT_CANCEL:
			btn_2 = btnText;
			break;
		}
	}
	
	
	private static InputPassword fsbDialog = null;
	public static InputPassword sharedApplication() {
		synchronized (InputPassword.class) {
			if (fsbDialog == null) {
				fsbDialog = new InputPassword();
			}
			return fsbDialog;
		}
	}
	
	
	
	private static AlertDialog dialog =  null;
	/**
	 * 다이얼로그 요청 메소드 
	 * @param mContext
	 * @param title
	 * @param msg
	 * @param type
	 * @param callback
	 */
	public static void showAlertMsg(Activity mContext, String title, CharSequence msg, int type, final CallBack callback) {
    
    	if(mContext instanceof Activity) {
    		if(((Activity) mContext).isFinishing()) {
    			return;
    		}
    	}
		
    	
//    	if( dialog != null ){
////    		dialog.dismiss();
////    		dialog = null;
//    		return;
//    	}
		final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
		alert.setCancelable(false);
		alert.setTitle(title);
		alert.setMessage(msg);
		switch (type) {
		case ALERT_OKANDCANCEL:
			alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface d, int w) {
					if(callback != null) {
						callback.onPressButton(ALERT_OK);
					}
					d.dismiss();
				}
			});
			alert.setNegativeButton("취소", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface d, int w) {
					if(callback != null) {
						callback.onPressButton(ALERT_CANCEL);
					}
					d.dismiss();
				}
			});
			break;
		case ALERT_USERDEFINE:
			if(!TextUtils.isEmpty(btn_1)) {
				alert.setPositiveButton(btn_1, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface d, int w) {
						if(callback != null) {
							callback.onPressButton(ALERT_OK);
						}
						d.dismiss();
					}
				});
			}
			if(!TextUtils.isEmpty(btn_2)) {
				alert.setNegativeButton(btn_2, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface d, int w) {
						if(callback != null) {
							callback.onPressButton(ALERT_CANCEL);
						}
						d.dismiss();
					}
				});
			}
			break;
		case ALERT_OK:
			alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface d, int w) {
					if(callback != null) {
						callback.onPressButton(ALERT_OK);
					}
					d.dismiss();
				}
			});
			break;
		case ALERT_CANCEL:
			alert.setPositiveButton("취소", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface d, int w) {
					if(callback != null) {
						callback.onPressButton(ALERT_CANCEL);
					}
					d.dismiss();
				}
			});
			break;
		case ALERT_BACK:
			alert.setPositiveButton("뒤로", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface d, int w) {
					if(callback != null) {
						callback.onPressButton(ALERT_BACK);
					}
					d.dismiss();
				}
			});
			break;
		default:
			break;
		}
		dialog = alert.create();
		dialog.show();

	}
	
	
	/**
	 * 다이얼로그 요청 메소드 
	 * @param mContext
	 * @param title
	 * @param msg
	 * @param type
	 */
	public static void showAlertMsg(Activity mContext, String title, CharSequence msg, int type) {
		showAlertMsg(mContext, title, msg, type, null);
	}
	
	
	/**
	 * 다이얼로그 요청 메소드 
	 * @param mContext
	 * @param msg
	 * @param type
	 */
	public static void showAlertMsg(Activity mContext, CharSequence msg, int type) {
		showAlertMsg(mContext, "알림", msg, type);
	}
	
	
	/**
	 * CallBack interface  
	 */
	public static interface CallBack {
		public void onPressButton(int btnIndex);
	}
	
	
	public void clear(){
		if( dialog != null ){
			dialog.dismiss();
			dialog = null;
		}
	}
}
