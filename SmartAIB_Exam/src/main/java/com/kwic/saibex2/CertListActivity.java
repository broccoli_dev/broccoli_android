package com.kwic.saibex2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kwic.saib.pub.CertAPI;
import com.kwic.saib.pub.IBLCertInfo;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CertListActivity extends Activity {

	final Context context = this;
	//인증정보 리스트
	ArrayList<IBLCertInfo> mCertList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ss_cert_activity);
		
		LinearLayout llCertList = (LinearLayout)findViewById(R.id.ll_CertList);

		final CertAPI certAPI = new CertAPI();
		final String jsonData = certAPI.PKI_List();
		
		JSONArray jArr = null;
		try {
			jArr = new JSONArray(jsonData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(jArr.length()> 0) {
			// 가져온 인증서를 Layout 에 추가
			for(int i = 0; i < jArr.length(); i++)
			{
				View view = LayoutInflater.from(CertListActivity.this).inflate(R.layout.ss_cert_row, null);
				
				ImageView ivCertStatus = (ImageView)view.findViewById(R.id.iv_CertStatus);
				TextView tvCertInfo1 = (TextView)view.findViewById(R.id.tv_CertInfo1);
				TextView tvCertInfo2 = (TextView)view.findViewById(R.id.tv_CertInfo2);
				TextView tvCertInfo3 = (TextView)view.findViewById(R.id.tv_CertInfo3);
				TextView tvCertInfo4 = (TextView)view.findViewById(R.id.tv_CertInfo4);
				
				// 가져온 인증서 정보로 Row를 구성한다.
				JSONObject jObj = null;
				try {
					jObj = jArr.getJSONObject(i);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					String subjectName = jObj.getString("subjectName");
					ivCertStatus.setImageResource(AiBUtils.getCertIcon(jObj.getString("notAfter")));
					tvCertInfo1.setText(subjectName);
					tvCertInfo2.setText(AiBUtils.getOID(jObj.getString("oid")));	// OID로 변환하는 것이 필요하다.
					tvCertInfo3.setText(AiBUtils.convert_issuer(jObj.getString("issuerName")));
					tvCertInfo4.setText("만료일 : " +jObj.getString("notAfter"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				llCertList.addView(view);
				view.setTag(i);
				view.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						// POPUP Alert 을 통해서 인증서 암호를 입력받는다.
						LayoutInflater li = LayoutInflater.from(context);
						View promptsView = li.inflate(R.layout.in_popup_prompt, null);
						final int index = (Integer)v.getTag();
						
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setView(promptsView);
						final EditText userInput = (EditText) promptsView
								.findViewById(R.id.editTextDialogUserInput);
	
						userInput.setOnClickListener(new OnClickListener() {
							   
							 public void onClick(View v) {
							  // Set keyboard
							  InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
							    // Lets soft keyboard trigger only if no physical keyboard present
							    imm.showSoftInput(userInput, InputMethodManager.SHOW_IMPLICIT);
							    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
							   }
							  });
						
						alertDialogBuilder
							.setCancelable(false)
							.setPositiveButton("OK",
							  new DialogInterface.OnClickListener() {
							    public void onClick(DialogInterface dialog,int id) {
							    	
									JSONArray mArr = null;
									JSONObject jO = null;
									try {
										mArr = new JSONArray(jsonData);
										jO = mArr.getJSONObject(index);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									String certNM = null;
									try {
										certNM = jO.getString("subjectDN");
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									String certPW = userInput.getText().toString();
									Intent intent = new Intent();
									intent.putExtra("passwd", certPW);
									intent.putExtra("certnm", certNM);
									setResult(RESULT_OK, intent);
									finish();
							    }
							  })
							.setNegativeButton("Cancel",
							  new DialogInterface.OnClickListener() {
							    public void onClick(DialogInterface dialog,int id) {
							    	dialog.cancel();
							    }
							  });						
						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();
						// show it
						alertDialog.show();
						//비밀번호 입력 팝업
					}
				});
			}
		}
	}
}
